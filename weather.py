from pyowm import OWM
from pyowm.utils import config
from pyowm.utils import timestamps
from pyowm.utils.config import get_default_config
from rich import print
config_dict = get_default_config()
config_dict['language'] = 'ru'

owm = OWM('e29b6b7eedafcaddb10d824cc8f13c64')
mgr = owm.weather_manager()

place = input("Укажите город: ")

observation = mgr.weather_at_place(place)
w = observation.weather
wdet = w.detailed_status
humidity = w.humidity
wind = w.wind()["speed"]
tempmax = w.temperature('celsius')["temp_max"]
tempmin = w.temperature('celsius')["temp_min"]

print(f"В городе {place} сегодня {wdet}")
print(f"Ветер: {str(wind)} м/с")
print(f"Температура: от {str(tempmin)} °C до {str(tempmax)} °C")
print(f"Влажность воздуха: {str(humidity)}%")

input()

