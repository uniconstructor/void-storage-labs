# -*- coding: utf-8 -*-

# Learn more: https://github.com/kennethreitz/setup.py
# https://github.com/ella/setuptools-dummy/blob/master/setup.py
# https://setuptools.pypa.io/en/latest/userguide/declarative_config.html

from setuptools import setup, find_packages


setup(
    name='void-storage-labs',
    version='0.1.0',
    description='',
    #author='',
    #author_email='',
    url='https://example.com',
    # license=license,
    packages=find_packages(exclude=(
        "*.tests", "*.tests.*", "tests.*", "tests",
        'docs', 
        'data', 
        'src/data', 
        '.vscode', 
        '.pytest_cache', 
        '__pycache__', 
        'hashlife',
        'README.md',
        '*.ipynb'
        ))
)