# https://stackoverflow.com/questions/38340808/recursive-typing-in-python-3-5
from __future__ import annotations
from rich import print
from rich.pretty import pprint
from collections import Counter
from dataclasses import dataclass, field
from bitarray import bitarray
from bitarray.util import zeros, huffman_code, ba2int, int2ba
from typing import Union, List, Tuple
import heapq as _heapq
from operator import itemgetter as _itemgetter
from sortedcontainers import SortedSet
import math
from tqdm import tqdm

DEFAULT_ENDIAN = 'little'

class CustomCounter(Counter):

    def least_common(self, n: int=None) -> list:
        '''List the n least common elements and their counts from the most
        common to the least.    If n is None, then list all element counts.

        >>> Counter('abracadabra').least_common(3)
        [('r', 2), ('b', 2), ('a', 5)]

        '''
        # Emulate Bag.sortedByCount from Smalltalk
        if n is None:
                return sorted(self.items(), key=_itemgetter(1), reverse=False)
        return _heapq.nsmallest(n, self.items(), key=_itemgetter(1))
    
    def top_items(self, n: int=None) -> CustomCounter:
        return CustomCounter(dict(self.most_common(n)))
    
    def bottom_items(self, n: int=None) -> CustomCounter:
        return CustomCounter(dict(self.least_common(n)))
    
    def order_by_keys(self, max_items: int=None, reverse=False):
        if max_items is None:
                return sorted(self.items(), key=_itemgetter(0), reverse=reverse)
        if (reverse):
            return _heapq.nsmallest(max_items, self.items(), key=_itemgetter(0))
        else:
            return _heapq.nlargest(max_items, self.items(), key=_itemgetter(0))
    
    ### ITEM COUNT FILTERING ###
    
    def with_count(self, target_count: int) -> CustomCounter:
        result = CustomCounter()
        for item_key, item_count in self.items():
            if (item_count == target_count):
                result.update({ item_key: item_count })
        return result

    def with_count_above(self, target_count: int) -> CustomCounter:
        result = CustomCounter()
        for item_key, item_count in self.items():
            if (item_count >= target_count):
                result.update({ item_key: item_count })
        return result
    
    def with_count_below(self, target_count: int) -> CustomCounter:
        result = CustomCounter()
        for item_key, item_count in self.items():
            if (item_count <= target_count):
                result.update({ item_key: item_count })
        return result
    
    def most_common_above(self, target_count: int, max_items: int=None) -> CustomCounter:
        result = CustomCounter()
        for item_key, item_count in self.most_common(max_items):
            if (item_count >= target_count):
                result.update({ item_key: item_count })
        return result
    
    def most_common_below(self, target_count: int, max_items: int=None) -> CustomCounter:
        result = CustomCounter()
        for item_key, item_count in self.most_common(max_items):
            if (item_count <= target_count):
                result.update({ item_key: item_count })
        return result
    
    def least_common_above(self, target_count: int, max_items: int=None) -> CustomCounter:
        result = CustomCounter()
        for item_key, item_count in self.least_common(max_items):
            if (item_count >= target_count):
                result.update({ item_key: item_count })
        return result
    
    def least_common_below(self, target_count: int, max_items: int=None) -> CustomCounter:
        result = CustomCounter()
        for item_key, item_count in self.least_common(max_items):
            if (item_count <= target_count):
                result.update({ item_key: item_count })
        return result
    
    ### KEY FILTERING ###

    def max_keys(self, max_items: int=None) -> list:
        keys = sorted(list(self.keys()), reverse=True)
        if (max_items is not None):
            return keys[0:max_items]
        return keys
    
    def min_keys(self, max_items: int=None) -> list:
        keys = sorted(list(self.keys()))
        if (max_items is not None):
            return keys[0:max_items]
        return keys
    
    def first_items(self, max_items: int=None) -> list:
        """
        First n counts: same as most_common(), but order by key
        """
        return self.order_by_keys(max_items=max_items, reverse=False)
    
    def last_items(self, max_items: int=None) -> list:
        """
        Last n counts: same as most_common(), but order by key
        """
        return self.order_by_keys(max_items=max_items, reverse=True)
    
    def with_key(self, target_key: Union[int, str, tuple, bool]) -> CustomCounter:
        result = CustomCounter()
        for item_key, item_count in self.items():
            if (type(item_key) != type(target_key)):
                raise Exception(f"Type of the item key '{item_key}' ({type(item_key)}) is not equal to target key '{target_key}' ({type(target_key)})")
            if (item_key == target_key):
                result.update({ item_key: item_count })
        return result
    
    def with_keys(self, target_keys: Union[List[int], List[str], List[tuple]]) -> CustomCounter:
        result        = CustomCounter()
        item_keys = self.keys()
        for target_key in target_keys:
            if (target_key in item_keys):
                item_count = self.get(target_key)
                result.update({ target_key: item_count })
        return result
    
    def with_key_above(self, target_key: int) -> CustomCounter:
        result = CustomCounter()
        if (type(target_key) is not int):
            raise Exception(f"Target key '{target_key}' is not integer ({type(target_key)} given)")
        for item_key, item_count in self.items():
            if (type(item_key) is not int):
                raise Exception(f"Item key '{item_key}' is not integer ({type(item_key)} given)")
            if (item_key >= target_key):
                result.update({ item_key: item_count })
        return result
    
    def with_key_below(self, target_key: int) -> CustomCounter:
        result = CustomCounter()
        if (type(target_key) != 'int'):
            raise Exception(f"Target key '{target_key}' is not integer ({type(target_key)} given)")
        for item_key, item_count in self.items():
            if (type(item_key) != 'int'):
                raise Exception(f"Item key '{item_key}' is not integer ({type(item_key)} given)")
            if (item_key <= target_key):
                result.update({ item_key: item_count })
        return result

    ### COUNTS ###
    
    def aggregated_counts(self) -> CustomCounter:
        result = CustomCounter()
        for item_key, item_count in self.items():
            result.update({ item_count: 1 })
        return result
    
    def max_count_values(self, max_items: int=None) -> List[int]:
        return self.aggregated_counts().max_keys(max_items=max_items)
    
    def min_count_values(self, max_items: int=None) -> List[int]:
        return self.aggregated_counts().min_keys(max_items=max_items)
    
    def max_counts(self, max_items: int=None) -> List[int]:
        aggregated_counts = self.aggregated_counts()
        count_values      = aggregated_counts.max_keys(max_items=max_items)
        return aggregated_counts.with_keys(count_values)
    
    def min_counts(self, max_items: int=None) -> List[int]:
        aggregated_counts = self.aggregated_counts()
        count_values      = aggregated_counts.min_keys(max_items=max_items)
        return aggregated_counts.with_keys(count_values)
    
    def most_used_counts(self, max_items: int=None) -> CustomCounter:
        return self.aggregated_counts().most_common(max_items)
    
    def least_used_counts(self, max_items: int=None) -> CustomCounter:
        return self.aggregated_counts().least_common(max_items)

    ### DEPRECATED ###

    def items_above(self, max_count: int) -> CustomCounter:
        return self.with_count_above(target_count=max_count)
    
    def items_below(self, max_count: int) -> CustomCounter:
        return self.with_count_below(target_count=max_count)

###########################
### OTHER MODIFICATIONS ###
###########################

DEFAULT_BLOCK_SIZE = 256

def init_byte_counter(value: int=0) -> CustomCounter:
    return CustomCounter(dict([(x, value) for x in range(DEFAULT_BLOCK_SIZE)]))

@dataclass
class ConsumableCounter:
    value_counts     : CustomCounter = field(default=None)
    value_bits       : bitarray      = field(default=None)
    values           : List[int]     = field(default=None, repr=False)
    bit_length       : int           = field(default=0, init=False)
    remaining_counts : CustomCounter = field(default_factory=Counter, repr=False, init=False)
    remaining_values : List[int]     = field(default_factory=list, repr=False, init=False)
    endian           : str           = field(default=DEFAULT_ENDIAN, init=False, repr=False)

    def __init__(
            self, values: List[int]=None, value_counts: CustomCounter=None, value_bits: bitarray=None, endian: str=DEFAULT_ENDIAN
        ):
        self.remaining_values = list()
        self.remaining_counts = CustomCounter()
        self.endian           = endian
        if (values is None) and (value_counts is None) and (value_bits is None):
            raise Exception(f"values, value_counts or value_bits must be specified")
        if (values is not None) and (value_bits is not None):
            raise Exception(f"You can init object either with values or with value bits, but not both")
        if (value_bits is not None) and (value_counts is None):
            raise Exception(f"value_bits cannot be decoded without a value_counts (value_counts=None received)")
        
        # define what action should be prepared
        if (values is not None):
            # encoding: init counter with values - they will be counted and encoded to binary
            self.values           = values.copy()
            self.value_counts     = CustomCounter(self.values)
            self.remaining_values = self.values.copy()
            self.remaining_counts = self.value_counts.copy()
            self.value_bits       = bitarray('', endian=self.endian)
            self.bit_length       = 0
        elif (value_bits is not None):
            # decoding: init counter with bitarray - it will be restored to original values
            self.value_bits   = value_bits.copy()
            self.bit_length   = len(self.value_bits)
            self.value_counts = value_counts.copy()
            for value, value_count in value_counts.copy().items():
                for i in range(value_count):
                    self.remaining_values.append(value)
                    self.remaining_counts.update({ value: 1 })
            self.values = list()

    def consume(self, max_values: int=None) -> bitarray:
        if (max_values is None):
            max_values = len(self.values)
            #self.remaining_values = self.values.copy()
        huffman_counts = self.remaining_counts.copy()
        codes          = huffman_code(huffman_counts)
        progress       = tqdm(range(0, max_values), mininterval=0.5, smoothing=0)
        encoded_length = 0
        code_lengths   = CustomCounter()
        for v, vc in codes.items():
            code_lengths.update({ v: len(vc) })
        
        for i in progress:
            value = self.values[i]
            code  = codes[value]
            self.value_bits += code
            self.remaining_counts.update({ value: -1 })
            self.remaining_values.remove(value)
            if (self.remaining_counts[value] == 0):
                if (len(huffman_counts) > 1):
                    del huffman_counts[value]
                codes        = huffman_code(huffman_counts)
                code_lengths = CustomCounter()
                for v, vc in codes.items():
                    code_lengths.update({ v: len(vc) })
                progress.set_postfix_str(f"code_lengths={code_lengths.aggregated_counts().last_items()}", refresh=False)
            encoded_length += len(code)
            progress.set_description_str(f"encoded {encoded_length} bits ({encoded_length // 8} bytes)", refresh=False)
        self.bit_length = len(self.value_bits)
        
        return self.value_bits
    
    def restore(self, max_values: int=None) -> List[int]:
        if (max_values is None):
            max_values = len(self.remaining_values)
            #self.remaining_values = self.values.copy()
        huffman_counts = self.remaining_counts.copy()
        codes          = huffman_code(huffman_counts)
        for i in range(0, max_values):
            code_lengths = CustomCounter()
            for v, vc in codes.items():
                code_lengths.update({ v: len(vc) })
            # start from shortest codes
            decoded_value = None
            for value, code_length in code_lengths.least_common():
                code         = codes[value]
                encoded_bits = self.value_bits[0:code_length]
                if (code == encoded_bits) and (value in self.remaining_values):#(self.remaining_counts[value] > 0):
                    decoded_value   = value
                    self.value_bits = self.value_bits[code_length:len(self.value_bits)]
                    self.bit_length = len(self.value_bits)
                    self.values.append(decoded_value)
                    self.remaining_values.remove(decoded_value)
                    self.remaining_counts.update({ decoded_value: -1 })
                    if (self.remaining_counts[decoded_value] == 0):
                        if (len(self.remaining_counts) > 1):
                            del huffman_counts[decoded_value]
                        codes        = huffman_code(huffman_counts)
                        code_lengths = CustomCounter()
                        for v, vc in codes.items():
                            code_lengths.update({ v: len(vc) })
                    break
            if (decoded_value is None):
                raise Exception(f"Step {i} ({max_values}): No value found for huffman code (encoded_bits={encoded_bits.to01()}, ")
            #else:
                #print(f"{i}: value={decoded_value}, code={code.to01()}, lvb={len(self.value_bits)}")
        return self.values

############################
### ENCODING AND PACKING ###
############################

@dataclass
class CountsBitPack:
    """
    Ultra-compact binary representation of frequencies for each byte inside 256-byte block
    """
    # frequencies of each byte in block: counter always have length=256 values, 
    # because counter include bytes with 0 occurrences
    value_counts     : CustomCounter               = field(default=None)
    # encoded value_counts
    value_bits       : bitarray                    = field(default=None)
    # number of unique values that have 2 or more occurrences in 256-byte block
    # length: 7 bit (because there is only 128 maximum pairs for 256 byte values)
    duplicated_count : int                         = field(default=None, init=False)
    # all byte frequencies, used in this block (starting from 2)
    # item format: is_used (bool), item_count (int), bit_length (int)
    # length: dynamic ("is_used" value used as continuation bit)
    frequencies      : List[Tuple[bool, int, int]] = field(default=None, init=False)
    # number of values, excluded from this block (bytes with 0 occurrences) 
    excluded_count   : int                         = field(default=None, init=False)
    # 256 - total_duplicates - excluded_count
    unique_count     : int                         = field(default=None, init=False)
    # unique_count + unique_count
    included_count   : int                         = field(default=None, init=False)
    # length of encoded binary representation
    bit_length       : int                         = field(default=0, init=False)

    def __init__(self, value_counts: CustomCounter=None, value_bits: bitarray=None):
        if (value_counts is not None):
            if (sum(value_counts.values()) != DEFAULT_BLOCK_SIZE):
                raise Exception(f"incorrect byte frequencies={sum(value_counts.values())} for block size={DEFAULT_BLOCK_SIZE}")
            self.value_counts     = value_counts.copy()
            self.duplicated_count = sum(self.value_counts.with_key_above(2).values())
            self.excluded_count   = self.value_counts[0]
            self.unique_count     = self.value_counts[1] #DEFAULT_BLOCK_SIZE - self.duplicated_count - self.excluded_count
            self.included_count   = self.duplicated_count + self.unique_count
            print(f"dc={self.duplicated_count}, ec={self.excluded_count}, uc={self.unique_count}")
        elif (value_bits is not None):
            self.value_bits = value_bits.copy()
            self.bit_length = len(value_bits)
        else:
            raise Exception(f"value_counts or value_bits must be provided")
        
        # set up frequencies
        self.frequencies          = list()
        processed_duplicate_count = 0
        remaining_duplicate_count = self.duplicated_count
        for item_count in range(2, 255):
            block_items_with_count = self.value_counts[item_count]
            #print(f"block_items_with_count={block_items_with_count}")
            if (block_items_with_count > 0):
                #max_items_with_count       = math.ceil(DEFAULT_BLOCK_SIZE / item_count) - processed_duplicate_count
                items_count_bits           = (remaining_duplicate_count-1).bit_length() #max_items_with_count.bit_length() #remaining_duplicate_count.bit_length()
                frequency                  = (True, block_items_with_count, items_count_bits + 1)
                processed_duplicate_count += block_items_with_count
                remaining_duplicate_count -= block_items_with_count
            else:
                frequency = (False, None, 1)
            self.frequencies.append(frequency)
            #print(f"item_count={item_count}: f={frequency}, processed: {processed_duplicate_count} of {self.duplicated_count} ({remaining_duplicate_count} left), bit_length={self.bit_length}")
            if (remaining_duplicate_count == 0):
                break
        if (remaining_duplicate_count > 0):
            raise Exception(f"provided value_counts is incorrect (remaining_duplicate_count={remaining_duplicate_count} after full scan)")
        self.bit_length = sum([f[2] for f in self.frequencies])
    
    def encode(self) -> bitarray:
        self.value_bits  = bitarray('', endian=DEFAULT_ENDIAN)
        self.value_bits += int2ba(self.duplicated_count, length=7, signed=False, endian=DEFAULT_ENDIAN)
        for frequency in self.frequencies:
            count_length = 0
            if (frequency[0] is False):
                encoded_frequency = bitarray('0', endian=DEFAULT_ENDIAN)
                self.value_bits += encoded_frequency
            else:
                count_length      = frequency[2] - 1
                encoded_frequency = bitarray('1', endian=DEFAULT_ENDIAN)
                if (count_length > 0):
                     encoded_frequency += int2ba(frequency[1], length=count_length, signed=False, endian=DEFAULT_ENDIAN)
                self.value_bits += encoded_frequency
            #print(f"ef={encoded_frequency.to01()}, l={len(encoded_frequency)}")
        self.bit_length = len(self.value_bits)
        return self.value_bits
    
    def decode(self) -> CustomCounter:
        pass

        

            


            

