# https://github.com/Textualize/rich
from rich import print, inspect
# https://rich.readthedocs.io/en/latest/pretty.html
from rich.pretty import pprint
# https://rich.readthedocs.io/en/latest/traceback.html
from rich.traceback import install
install(show_locals=True)
# https://rich.readthedocs.io/en/latest/logging.html
import logging
from rich.logging import RichHandler
logging.basicConfig(
    level="INFO",
    format="%(message)s",
    datefmt="[%X]",
    handlers=[RichHandler(rich_tracebacks=True, locals_max_length=16)]
)
log = logging.getLogger("rich")
# https://github.com/tqdm/tqdm#nested-progress-bars
from tqdm import tqdm as tqdm_notebook
#from tqdm.notebook import tqdm as tqdm_notebook, trange

import shutil
# https://docs.python.org/3/library/itertools.html
# https://more-itertools.readthedocs.io/en/stable/api.html
from itertools import count, accumulate
# https://more-itertools.readthedocs.io/en/stable/api.html#more_itertools.windowed
from more_itertools import adjacent, map_reduce, mark_ends, pairwise, windowed, seekable, peekable, spy, \
    bucket, split_when, split_before, split_at, roundrobin, \
    consecutive_groups, run_length, first_true, islice_extended, first, last, rstrip, tail, \
    unique_everseen, locate, replace, time_limited
# https://bitstring.readthedocs.io/en/latest/index.html
from bitstring import Bits, BitArray, BitStream, ConstBitStream
# https://pythongeeks.org/ordereddict-in-python/
# https://www.geeksforgeeks.org/counters-in-python-set-1/
# https://docs.python.org/3/library/collections.html#counter-objects
from collections import OrderedDict, Counter, defaultdict, namedtuple, deque
# https://docs.python.org/3/library/typing.html
from typing import List, Dict, Set, Tuple, Optional, Union
# https://docs.python.org/3.6/library/random.html#module-random
import random
# https://www.geeksforgeeks.org/floor-ceil-function-python/?ref=lbp
import math
import operator
# https://stackoverflow.com/questions/55212014/python-count-copies-in-dictionary
# https://docs.python.org/3/library/copyreg.html#module-copyreg
# https://docs.python.org/3/library/shelve.html#module-shelve
# https://docs.python.org/3/library/pickle.html#persistence-of-external-objects
import copy, pickle, json
# https://github.com/mohanson/leb128
# https://en.wikipedia.org/wiki/LEB128
import io, leb128
import os, sys
if not 'workbookDir' in globals():
    workbookDir = os.getcwd()
os.chdir(workbookDir)
# функции для работы с хеш-пространством
from hash_space_utils import HashItemAddress, HashItemPosition, HashSegmentAddress, PositionSeed, \
    DEFAULT_VALUE_STEP, DEFAULT_POSITION_STEP, HASH_DIGEST_BITS, \
    split_data, count_segment_items, count_split_values, collect_split_positions, \
    get_min_bit_length, get_aligned_bit_length, get_aligned_byte_length, \
    bytes_at_position, read_hash_item, read_hash_segment, \
    get_item_based_data_length_from_number, \
    find_value_in_segment
from hash_item_tiers import get_max_position_tier
import vlq

# https://www.attrs.org/en/stable/
# https://www.attrs.org/en/latest/api.html#attr.ib
import attr
# from attr import field, fields, define, make_class
# https://realpython.com/python-data-classes/
# https://realpython.com/python-data-classes/#advanced-default-values
from dataclasses import dataclass, make_dataclass
# https://docs.mongoengine.org/
from mongoengine import *

connect(db='blib', host='127.0.0.1', port=27017, maxPoolSize=300)
# https://docs.mongoengine.org/guide/querying.html#advanced-queries
from mongoengine.queryset.visitor import Q
# https://docs.mongoengine.org/guide/connecting.html#switch-collection
from mongoengine.context_managers import switch_collection
# https://pymongo.readthedocs.io/en/stable/api/bson/objectid.html
from bson import ObjectId
# https://realpython.com/lru-cache-python/
from functools import lru_cache
#from pygtrie import PrefixSet

# maximum value length for in-memory lookup when filling database
MAX_CACHED_LENGTH    = 4
# default byte order when converting dats or hash item byte values to int
DEFAULT_BYTE_ORDER   = 'big'
# ObjectId length in bytes
OBJECTID_LENGTH      = 12
# total ObjectId bytes for position in 
MAX_POSITION_LENGTH  = 6
# total ObjectId bytes for position in 
MAX_VALUE_LENGTH     = OBJECTID_LENGTH - MAX_POSITION_LENGTH
# number of seeds scanned at once during single allocation step 
# (see allocate_tier_values() and scan_new_seeds_for_tier() for details)
NEW_SEEDS_PER_TIER   = 100_000
# maximum items per 1 query when searching new best seeds (find_best_seeds_for_tier())
MAX_RESULTS_FOR_BEST_SEED_LOOKUP = 1000
# maximum seeds in query searching new best seeds (find_best_seeds_for_tier())
MAX_SEEDS_FOR_BEST_SEED_LOOKUP   = 1_000_000 #8000000 #500000 #50000
# max LRU cache size used for db lookup operations by objectID (see has_item_hash_value() for details)
# (2**(7*1)): all possible positions (0-127) encoded by 1 byte LEB128
# (2**(8*2)): all possible 2-byte values at each position
MAX_CACHE_SIZE_FOR_OBJECTID_LOOKUP = (2**(7*1)) * (2**(8*2))

# result generated by create_contant_based_split()
ContentBasedSplit = namedtuple('ContentBasedSplit', [
    # split result (list of items of different length)
    'split_items',
    # unique elements only (data dictionary)
    'data_values',
    # Bits() object with all processed data
    'encoded_data',
    # Bits() object with remaining data 
    'remaining_data',
    # this flag is True when split process was not finished because all tier dictionaries are full
    'capacity_overflow',
    # total number of number of unique values, grouped by length
    'length_counts',
    # number of usages of each dict value
    'byte_counts',
    # length of the result list
    'encoded_items_count',
    # number of unique values in dict
    'unique_values_count',
    # data.bytepos property value after split 
    # (required to continue split process with several iterations, used when capacity_overflow=True)
    'bytepos',
])

# result of allocation of the single tier values
TierVauesAllocation = namedtuple('TierVauesAllocation', [
    # allocated tier (common for all allocation values)
    'tier',
    # number of unique seeds, used to allocate values of this tier
    'total_seeds',
    # all seeds, used by this allocation
    'seeds',
    # all data item values for this tier, used for this allocation
    'values',
    # what seed should we use for each position to decode our data?
    'position_seeds',
    # position->value (direct mapping, used for verification of encoded data)
    'position_values',
    # value->position (inversed mapping, used to encode data items to data item positions)
    'value_positions',
    # number of positions that each seed uses for this allocation
    'seed_positions_counter',
])

class HashItemValue(Document):
    position        = LongField(min_value=0, max_value=2**(8*8))
    value           = LongField(min_value=0, max_value=2**(8*8))
    seed            = LongField(min_value=0, max_value=2**(8*8))
    position_length = IntField(min_value=1, max_value=8)
    value_length    = IntField(min_value=2, max_value=8)
    seed_length     = IntField(min_value=1, max_value=8)
    meta = {
        'index_background'  : False,
        'auto_create_index' : True,
        'indexes' : [
            'position',
            'value',
            'seed',
            'position_length',
            'seed_length',
            'value_length'
        ]
    }

SEED_VALUE_BYTE_ORDER = 'little'

class SeedValue(Document):
    value               = BinaryField(max_bytes=256, unique=True) #primary_key=True, null=False, unique=True, db_field='value'
    position            = LongField(min_value=0, max_value=2**(8*8))
    value_length        = IntField(min_value=1, max_value=256)
    #extra_byte          = BinaryField(max_bytes=1)
    parent_value        = BinaryField(max_bytes=256)
    children_count      = IntField(min_value=0, max_value=256)
    #parent_position     = LongField(min_value=0, max_value=2**(8*8))
    #parent_value_length = IntField(min_value=1, max_value=32)
    meta = {
        #'collection' : 'seed_0_value',
        #'index_background'  : False,
        #'auto_create_index' : True,
        'indexes' : [
            #'value',
            'position',
            'value_length',
            #'extra_byte',
            'parent_value',
            'children_count',
            #'parent_position',
            #'parent_value_length',
        ]
    }

    #@queryset_manager
    #def childrenFor(doc_cls, queryset, target_value: Union[bytes, bytearray]):
    #    return queryset.filter(parent_value=target_value)

    #@queryset_manager
    #def parentFor(doc_cls, queryset, target_value: Union[bytes, bytearray]):
    #    parent_value = target_value[0:len(target_value)-1]
    #    return queryset.filter(value=parent_value)
# switch_collection(SeedValue, collection_name='seed_values_0')

class SeedValueQuerySet(QuerySet):
    def get_last_position(self) -> int:
        last_item = self.filter().order_by('-position').first()
        if (last_item is None):
            return 0
        return last_item.position
    
    def get_last_value_length(self) -> int:
        last_item = self.filter().order_by('-value_length').first()
        if (last_item is None):
            return 0
        return last_item.value_length
    
    def get_min_value_length(self) -> int:
        """
        Shortest value length to start search iteration
        """
        for value_length in range(2, 16):
            max_values   = (256 ** value_length)
            saved_values = self.filter(value_length=value_length).count()
            #print(saved_values, max_values)
            if (saved_values < max_values):
                return value_length
        raise Exception('Error: get_min_value_length() - too long value')
    
    def get_all_values(self) -> QuerySet:
        return self.order_by('position')
    
class SeedValueZero(Document):
    position            = LongField(min_value=0, max_value=2**(8*8), primary_key=True)
    value               = BinaryField(max_bytes=256, unique=True)
    parent_value        = BinaryField(max_bytes=256)
    value_length        = IntField(min_value=1, max_value=256)
    meta = {
        'queryset_class': SeedValueQuerySet,
        'indexes' : [
            # 'position',
            'value',
            'parent_value',
            'value_length',
        ]
    }
    _pv_cache = defaultdict(set) #PrefixSet()

    #def __init__(self):
    #    self._pv_cache = PrefixSet()

    def get_pv_cache(self):
        return type(self)._pv_cache

    def set_pv_cache(self,val):
        type(self)._pv_cache = val

    pv_cache = property(get_pv_cache, set_pv_cache)

    def get_parent_item(self, value: bytes, value_length: int) -> Document:
        #value_length = len(value)
        if (value_length <= 2):
            return None
        parent_values = list()
        for parent_value_length in range(2, value_length):
            parent_value = value[0:parent_value_length]
            parent_values.append(parent_value)
        # get longest parent
        last_parent = self.objects(value__in=parent_values).order_by('-value_length').first()
        if (last_parent is not None):
            prev_parent = self.get_parent_item(self, value=last_parent.value, value_length=value_length)
            if (prev_parent is not None):
                self._pv_cache[value_length].add(prev_parent.value)
            else:
                self._pv_cache[value_length].add(last_parent.value)
        return last_parent
    
    def has_parent_value(self, value: bytes, value_length: int) -> bool:
        #value_length = len(value)
        if value in self._pv_cache:
            return True
        for parent_value_length in range(2, value_length):
            parent_value = value[0:parent_value_length]
            if (parent_value in self._pv_cache[value_length]):
                return True
        parent_item = self.get_parent_item(self, value=value, value_length=value_length)
        if (parent_item is not None):
            return True
        return False
    
    def cache_item_value(self, value: bytes, value_length: int):
        #value_length = len(value)
        self._pv_cache[value_length].add(value)
    
    def has_cached_value(self, value: bytes, value_length: int) -> bool:
        #value_length = len(value)
        return (value in self._pv_cache[value_length])

def encode_to_leb128(number: int) -> bytearray:
  return leb128.u.encode(number)

def decode_from_leb128(value: bytearray) -> int:
  return leb128.u.decode(value)

def get_address_length_from_number(number: int) -> int:
    """
    >>> get_address_length_from_number(127)
    1
    >>> get_address_length_from_number(128)
    2
    >>> get_address_length_from_number(0)
    1
    >>> get_address_length_from_number(2**14-1)
    2
    >>> get_address_length_from_number(2**14)
    3
    """
    return len(leb128.u.encode(number))

def get_value_length_from_number(number: int) -> int:
    return get_address_length_from_number(number) + 1

def address_length_to_offset(address_length: int) -> int:
    """
    >>> address_length_to_offset(0)
    0
    >>> address_length_to_offset(1)
    0
    >>> address_length_to_offset(2)
    128
    >>> address_length_to_offset(2) == (2**7)
    True
    >>> address_length_to_offset(3) == (2**7 + 2**14)
    True
    >>> address_length_to_offset(4) == (2**7 + 2**14 + 2**21)
    True
    """
    if (address_length == 0):
        return 0
    if (address_length == 1):
        return 0
    prev_address_length = address_length - 1
    prev_address_offset = address_length_to_offset(prev_address_length)
    return prev_address_offset + (2 ** (7 * (address_length - 1)))

def address_length_to_capacity(address_length: int) -> int:
    """
    >>> address_length_to_capacity(0)
    0
    >>> address_length_to_capacity(1)
    128
    >>> address_length_to_capacity(2)
    16256
    >>> address_length_to_capacity(3)
    2080640
    """
    if (address_length == 0):
        return 0
    total_address_capacity = (2 ** (7 * address_length))
    address_offset         = address_length_to_offset(address_length)
    return total_address_capacity - address_offset

def address_length_to_extended_capacity(address_length: int) -> int:
    """
    >>> address_length_to_extended_capacity(0)
    0
    >>> address_length_to_extended_capacity(1)
    128
    >>> address_length_to_extended_capacity(2)
    16512
    >>> address_length_to_extended_capacity(3)
    2113664
    """
    if (address_length == 0):
        return 0
    total_address_capacity = (2 ** (7 * address_length))
    address_offset         = address_length_to_offset(address_length)
    return total_address_capacity + address_offset

def position_to_id(position: int, address_length: int) -> int:
    offset = address_length_to_offset(address_length)
    return (offset + position)

def position_to_address_length(position: int) -> int:
    return get_address_length_from_number(position)
  
def seed_to_address_length(seed: int) -> int:
    return get_address_length_from_number(seed)

def id_to_position(id: int, address_length: int) -> int:
    offset = address_length_to_offset(address_length)
    return (id - offset)

def id_to_address_length(id: int) -> int:
    return get_address_length_from_number(id)

def address_length_to_value_length(address_length: int) -> int:
    return (address_length + 1)

def value_length_to_address_length(value_length: int) -> int:
    if (value_length == 0):
        return 0
    return (value_length - 1)

def create_length_limits(min_value_length: int = 2, max_value_length: int = 4) -> dict:
    """
    >>> create_length_limits(2, 3)
    {2: 128, 3: 16256}
    >>> create_length_limits(2, 4)
    {2: 128, 3: 16256, 4: 2080640}
    """
    max_items_by_length = dict()
    for value_length in range(min_value_length, max_value_length + 1):
        address_length        = value_length - 1
        value_length_capacity = address_length_to_capacity(address_length)
        max_items_by_length[value_length] = value_length_capacity
    return max_items_by_length

def get_last_seed(value_length: int=None, target_values: Optional[Set[int]]=None, target_positions: Optional[Set[int]]=None) -> int:
    """
    Получить максимальный созданный seed полученный при генерации базы значений
    """
    condition = Q(value_length__gt=0)
    if (target_values is not None) and (len(target_values) > 0):
        condition = condition & Q(value__in=target_values)
    if (target_positions is not None) and (len(target_positions) > 0):
        condition = condition & Q(position__in=target_positions)
    if (value_length is not None):
        condition = condition & Q(value_length=value_length)
    value = HashItemValue.objects(condition).order_by('-seed').first()
    if (value is None):
        return 0
    return value.seed

def create_number_ranges(numbers: List[int]) -> List[range]:
    """
    >>> create_number_ranges([1, 3, 4, 8, 11])
    [range(1, 2), range(3, 5), range(8, 9), range(11, 12)]
    """
    groups  = []
    numbers = sorted(numbers)
    for group in consecutive_groups(numbers):
        group = list(group)
        groups.append(range(min(group), max(group) + 1))
    return groups

def create_content_based_split(data: ConstBitStream, min_value_length: int=2, max_value_length: int=4) -> ContentBasedSplit:
    """
    Split data to short items (several byte each), each item value should be unique.
    Process goes from short to values. Number of items of each length is defined separatley (based on leb_128 encoding). 
    """
    # sets to True when all dictionaries are full, but there is still more unique data items to process
    capacity_overflow   = False
    # number of usages for each byte value
    byte_counts         = Counter()
    # unique fragments of data
    data_values         = set()
    # oridinal data, splitted into items according our dictionary
    split_items         = list()
    # set up maximum values of each length
    max_items_by_length = create_length_limits(min_value_length, max_value_length)
    # combined usage of dictionary values, grouped by item length
    length_counts       = Counter()
    # read data using variable length items
    while (True):
        item_value = None
        # length of next value depends on current dictionary state: short values collected first,
        # new values will be added only if we don't meet them previously
        for value_length in range(min_value_length, max_value_length + 1):
            # try to read and use short values first, use long values only if we dont have short ones
            scan_value = data.peek(f"bits:{value_length * 8}")
            if (scan_value not in data_values):
                # new (unique) value recieved
                if length_counts[value_length] >= max_items_by_length[value_length]:
                    # reached maximum capacity for values with given length
                    if (value_length == max_value_length):
                        # maximum unique values recieved - dictionary overflow
                        capacity_overflow = True
                        break
                    else:
                        # use capacity from next length tier
                        continue
                else:
                    # add new value to dictionary, updating item counter (and decreace tier capacity by 1 value)
                    length_counts.update({value_length : 1})
                    item_value = data.read(f"bits:{value_length * 8}")
                    break
            else:
                # existing (not unique) value recieved - do not modify our dictionary, but save item value to split result
                item_value = data.read(f"bits:{value_length * 8}")
                break
        if capacity_overflow == True:
            # input max_value_length is not enough to create a dictionary
            # raise Exception(f"Length capacity reached: {length_counts} (items_processed={len(split_items)}, max_value_length={max_value_length})")
            break
        # update usage counter of the dictionary value
        byte_counts.update({item_value.hex : 1})
        # append item value to final result
        split_items.append(item_value)
        # add item value to data dictionary
        data_values.add(item_value)
        if ((len(data) - data.bitpos) <= (min_value_length * 8)):
            # all data processed:
            # last item will be saved and returned in 'remaining_data' field inside result dict (if any)
            break
    # final result
    return ContentBasedSplit(
        split_items         = tuple(split_items),
        data_values         = data_values,
        encoded_data        = data[0:data.bitpos],
        remaining_data      = data[data.bitpos:len(data)],
        capacity_overflow   = capacity_overflow,
        length_counts       = length_counts,
        byte_counts         = byte_counts,
        encoded_items_count = len(split_items),
        unique_values_count = len(data_values),
        bytepos             = data.bytepos,
    )

def int_value_to_object_id(value: int, format: str='hex') -> str:
    value_length   = get_value_length_from_number(value)
    length_bytes   = (value_length).to_bytes(value_length * 8, byteorder=DEFAULT_BYTE_ORDER, signed=False)
    value_bytes    = (value).to_bytes(value_length, byteorder=DEFAULT_BYTE_ORDER, signed=False)
    padding_length = 11 - value_length
    padding_bytes  = (bytes.fromhex("00") * padding_length)
    object_id      = padding_bytes.hex() + length_bytes.hex() + value_bytes.hex()
    return object_id

def hash_item_to_object_id(value_length: int, position: int, value: int, seed: int) -> str:
    max_position_length = 5
    max_value_length    = 6
    max_seed_length     = 0
    max_length_bytes    = 1
    length_bytes        = (value_length * 8).to_bytes(max_length_bytes, byteorder=DEFAULT_BYTE_ORDER, signed=False)
    position_bytes      = (position).to_bytes(max_position_length, byteorder=DEFAULT_BYTE_ORDER, signed=False)
    value_bytes         = (value).to_bytes(max_value_length, byteorder=DEFAULT_BYTE_ORDER, signed=False)
    #seed_bytes          = (seed).to_bytes(max_seed_length, byteorder=DEFAULT_BYTE_ORDER, signed=False)
    #item_id_hex         = position_bytes.hex() + value_bytes.hex() + seed_bytes.hex() + length_bytes.hex()
    item_id_hex         = length_bytes.hex() + position_bytes.hex() + value_bytes.hex()
    return item_id_hex

def init_hash_item(value_length: int, position: int, value: int, seed: int) -> HashItemValue:
    #address_length  = position_to_address_length(position)
    #value_length    = address_length + 1
    address_length  = value_length - 1
    #position_length = get_aligned_byte_length(position)
    seed_length     = get_aligned_byte_length(seed)
    item_id_hex     = hash_item_to_object_id(value_length, position, value, seed)
    hash_item = HashItemValue(
        id=item_id_hex,
        position=position,
        value=value,
        seed=seed,
        position_length=address_length,
        value_length=value_length,
        seed_length=seed_length,
    )
    return hash_item

#@lru_cache(maxsize=MAX_CACHE_SIZE_FOR_OBJECTID_LOOKUP, typed=True)
def has_item_hash_value(value_length: int, position: int, value: int, seed: int) -> bool:
    #address_length = position_to_address_length(position)
    #address_length   = value_length - 1
    #item_id_hex    = hash_item_to_object_id(value_length, position, value, seed)
    #item_id        = ObjectId(oid=item_id_hex)
    # item           = HashItemValue.objects.with_id(item_id)
    item = HashItemValue.objects(
        value_length=value_length,
        position=position,
        value=value,
        seed=seed,
    ).first()
    if (item and item.id):
        return True
    return False
    

def save_new_hash_item_value(value_length: int, position: int, value: int, seed: int, check_existing: bool=True) -> bool:
    """
    Save unique combination of position/value for a given value_length
    If combination of position/value already exists in database - new value will not be created
    Returns True if new value has been created and False if combination of given value_length/position/value
    already exists
    Id of the new record is a 12-byte combination of item position (bytes 0-5) and item value (bytes 6-11),
    converted to bytes as unsigned int using DEFAULT_BYTE_ORDER (currently big endian)
    """
    # saving new value to database only if we don't have this value at given position
    if (check_existing):
        item_value_exists = has_item_hash_value(value_length, position, value, seed)
        if item_value_exists is True:
            return False
    hash_item = init_hash_item(value_length, position, value, seed)
    hash_item.save(force_insert=True)
    # clear LRU cache every time when new value is created
    #has_item_hash_value.cache_clear()
    return True

def save_new_hash_item_values(hash_item_values: List[HashItemValue], value_length: int) -> List[ObjectId]:
    object_values    = set()
    object_positions = set()
    object_ids       = set()
    #value_positions = defaultdict(set)
    item_list       = list()
    # save items with unique id only
    for hash_item_value in hash_item_values:
        if hash_item_value.id not in object_ids:
            #object_values.add(hash_item_value.value)
            #object_positions.add(hash_item_value.position)
            #object_ids.add(hash_item_value.id)
            #value_positions[hash_item_value.value].add(hash_item_value.position)
            item_list.append(hash_item_value)
            object_ids.add(hash_item_value.id)
    # check database before saving: remove items with existing id to prevent primary key duplication
    existing_items = HashItemValue.objects(
        #Q(value__in=object_values) & Q(position__in=object_positions) & Q(value_length=value_length) |
        Q(id__in=object_ids)
    )
    #existing_values = set([existing_item.value for existing_item in existing_items])
    #existing_value_positions = defaultdict(set)
    existing_ids = set()
    for existing_item in existing_items:
        #existing_value_positions[existing_item.value].add(existing_item.position)
        existing_ids.add(existing_item.id)
    new_items = list()
    for hash_item in item_list:
        #if (hash_item.position in existing_value_positions[hash_item.value]):
        #    continue
        if (len(existing_ids) > 0) and (hash_item.id in existing_ids):
            continue
        new_items.append(hash_item)
    if len(new_items) == 0:
        return []
    # create all items in a batch
    new_item_ids = QuerySet(HashItemValue, collection=HashItemValue._get_collection()).insert(new_items, load_bulk=False)
    return new_item_ids

def scan_new_seeds_for_tier(address_length: int, 
        input_values: Set[int], skip_values: Set[int]=set(), 
        input_positions: Set[int]=None, skip_positions: Set[int]=set(),
        input_seeds: Set[int]=None, skip_seeds: Set[int]=set(), 
        start_seed: int=None, end_seed: int=None, 
        start_position: int=None, end_position: int=None, 
        accumulate_positions: bool=False, accumulate_values: bool=False, 
        stop_after_minimum_allocation: bool=True, cache_value_position_pairs: bool=True, check_existing: bool=True, 
        bar_position: int=0,
        use_all_tier_positions: bool=True, 
        min_values_per_seed: int=1,
    ) -> dict:
    """
    Scan hash space segment, defined by seed range (start_seed, end_seed) and position range (start_position, end_position)
    All values must have same length (value length is defined in bytes, currently as address_length + 1)
    """
    value_length  = address_length + 1
    seed_counts   = Counter()
    seed_items    = defaultdict(dict)
    # all values that need to be found
    values        = input_values.copy()
    # same values, but represented as bytes
    byte_values = set()
    for iv in values:
        bv = int.to_bytes(iv, value_length, byteorder=DEFAULT_BYTE_ORDER, signed=False)
        byte_values.add(bv)
    # seed positions to scan
    positions = None
    if input_positions is not None:
        positions = input_positions.copy()
        
    # selected seeds to scan
    seeds = None
    if input_seeds is not None:
        seeds = input_seeds.copy()

    # all new values, located during this scan
    located_values       = set()
    located_values_count = 0
    # total writes to database (creation of new values only)
    saved_values_count   = 0
    # total reads from hash space
    scanned_values_count = 0
    # maximum number of different positions of a single value inside all seeds (accumulated during this scan)
    max_positions_count  = 0
    # maximum number of unique position/value pairs located inside one seed
    max_seed_items_count = 0
    # total unique position/value pairs stored in cache during this scan
    cached_values_count  = 0
    # total number of hits for cache discribed above 
    # (e. g. number of times when memory lookup performed instead of database lookup)
    cache_hits_count     = 0
    position_skips_count = 0
    value_skips_count    = 0
    accumulated_positions_count = 0
    accumulated_values_count    = 0
    # number of scanned values from last located value
    scans_per_value          = 0
    max_scans_per_value      = 0
    prev_max_scans_per_value = 0
    max_scans_update_count   = 0
    # this flag interrups search
    stop_search           = False
    # all possible positions for each value saved in db
    value_positions       = defaultdict(set)
    value_position_counts = Counter()
    
    if positions is None:
        positions = set()
        # start of search range inside single seed
        if (start_position is None):
            start_position = address_length_to_offset(address_length)
        # end of search range inside single seed
        if (end_position is None):
            if (use_all_tier_positions is True):
                # use all available positions
                end_position = address_length_to_offset(address_length + 1)
            else:
                # use minimum seed positions (only first positions at tier start)
                end_position = start_position + len(values)
        # create base scan range for seed positions
        for p in range(start_position, end_position):
            positions.add(p)
    else:
        if (start_position is not None) or (end_position is not None):
            raise Exception("You cannot provide start_position/end_position AND position list")
        # positions provided as set
        start_position = min(positions)
        end_position   = max(positions)
    # exclude posotopns from search if we need to do so
    for skip_position in skip_positions:
        if (skip_position not in positions):
            continue
        # we count all skipped positions as "accumulated" to keep counter up to date
        if accumulate_positions:
            accumulated_positions_count += 1
        positions.discard(skip_position)
    # define number total number of value/position pairs
    tier_capacity = address_length_to_offset(address_length + 1) - address_length_to_offset(address_length)
    
    # filter value list - discard all skipped values from the search
    if (len(skip_values) > 0):
        # exclude values from search if needed
        for sv in skip_values:
            bv = int.to_bytes(sv, value_length, byteorder=DEFAULT_BYTE_ORDER, signed=False)
            # discard from int and byte value dict
            values.discard(sv)
            byte_values.discard(bv)
            # we count all skipped values as "accumulated" to keep accumulated values' counter up to date
            if (accumulate_values):
                accumulated_values_count += 1
            # and we also count all skipped values as "already located" to keep the same "stop search" conditions below
            located_values.add(sv)
            located_values_count += 1

    # this flag indicates that we need to update available positions list for all further seeds
    update_seed_positions = True
    refresh_search_stats  = True
    seed_positions        = sorted(list(positions))
    # all positions accumulated during scan of this seed
    discarded_seed_positions = set()
    # all values located during scan of this seed 
    located_seed_values      = set()
    # list of hash value items that will be saved to mongodb when seed scan will be completed
    prepared_seed_values     = list()
    # int values of minimum and maximum target value - used for fast check values diring search
    min_value = min(values)
    max_value = max(values)
    # scanning seeds
    seed_progress = tqdm_notebook(range(start_seed, end_seed), mininterval=2, position=bar_position) #, smoothing=1)
    #seed_progress = range(start_seed, end_seed)
    for seed in seed_progress:
        seed_saved_values_count   = 0
        seed_scanned_values_count = 0
        seed_located_values_count = 0
        # refresh list of discarded positions for every seed
        discarded_seed_positions.clear()
        located_seed_values.clear()
        prepared_seed_values.clear()
        # starting scan seed positions within tier range
        for position in seed_positions:
            # read bytes from hash space of current seed
            value_bytes = bytes_at_position(position * 8, value_length * 8, seed, use_bytearray=False)
            # count performed scan operations (for seed / total)
            scanned_values_count      += 1
            seed_scanned_values_count += 1
            scans_per_value           += 1
            # go to the next position if there is not target value
            if value_bytes not in byte_values:
                value_skips_count += 1
                continue
            # normalize byte value - convertion to unsigned int (doing it after check above to speed up search a bit)
            value_int = int.from_bytes(value_bytes, byteorder=DEFAULT_BYTE_ORDER, signed=False)
            # each value should present only once per seed 
            if value_int in located_seed_values:
                # (occures rarely, but needs to be checked - to avoid incorrect counters behaviour)
                value_skips_count += 1
                continue
            # checking that value/position pair is uniqne: it must not be used by any other value before,
            # first we check value/position cache instead of quering db
            if (cache_value_position_pairs) and (position not in value_positions[value_int]): #and (value_positions_count < (2 ** (7 * address_length))):
                # this position/value pair is already exists in another seed - do not persist it to db in this case
                cache_hits_count += 1
                continue

            # NEW ITEM LOCATED: init db object for mongo
            new_item = init_hash_item(value_length, position, value_int, seed)
            # collect all items during seed scan - we save them to db later, when all seed positions will be scanned
            prepared_seed_values.append(new_item)
            # ignore same values if we meet them inside this seed again 
            located_seed_values.add(value_int)
            seed_located_values_count += 1
            # update "maximum values per seed" option
            max_positions_count = max(max_positions_count, seed_located_values_count)
        #### END OF POSITON PROCESSING (HOT LOOP) ###

        # TODO: save last best seed result with all located values
        # TODO: adjust min values per seed during scan
        
        # update "too many scans per seed" parameter
        max_scans_per_value_updated = False
        if (scans_per_value > max_scans_per_value):
            prev_max_scans_per_value    = max_scans_per_value
            max_scans_per_value         = scans_per_value
            max_scans_update_count     += 1
            max_scans_per_value_updated = True
        # check of "seed saturation": save value only if there is a minimum number of values per seed
        # or we are searching too long time
        if (seed_located_values_count < min_values_per_seed): # or (max_scans_per_value_updated is True):
            continue

        #### SAVING NEW ITEMS ####
        for saved_hash_item in prepared_seed_values:
            # saving unique position/value pairs to database
            new_item_saved = save_new_hash_item_value(
                saved_hash_item.value_length, 
                saved_hash_item.position, 
                saved_hash_item.value, 
                saved_hash_item.seed, 
                check_existing
            )
            # add saved pair position/value to scan cache: each unique pair should be mentioned only once
            if (cache_value_position_pairs):
                value_positions[value_int].add(saved_hash_item.position)
                value_positions_count = len(value_positions[saved_hash_item.value])
                cached_values_count += 1
            else:
                value_position_counts.update({ saved_hash_item.value : 1 })
            value_positions_count = value_position_counts[saved_hash_item.value]
            # TODO: check that all possible positions for this value has been found and move this value to the skip list
            # and calculate statistics (new vs existing items)
            max_positions_count = max(max_positions_count, value_positions_count)
            # update number of items that has been created (during all scan / during seed scan)
            if new_item_saved:
                saved_values_count      += 1
                seed_saved_values_count += 1
        
            # exclude this position from further search (if accumulate_positions is enabled)
            if accumulate_positions:
                # update global skip positions list
                skip_positions.add(saved_hash_item.position)
                # remove position from search
                positions.discard(saved_hash_item.position)
                # update save list of positions discarded during active seed scan
                discarded_seed_positions.add(saved_hash_item.position)
                # request update of iterated position list for next seed
                update_seed_positions = True
                accumulated_positions_count += 1
                # display message when search of the last seed is started
                if (len(positions) == 1):
                    final_values = values.copy()
                    final_values.discard(saved_hash_item.value)
                    #print(f"[accumulate_positions]: last seed position: {saved_hash_item.position} seed={saved_hash_item.seed}, values: {final_values}")
                    seed_progress.write(f"[accumulate_positions]: last seed position: {saved_hash_item.position} seed={saved_hash_item.seed}, values: {final_values}")
                # stop search if all value possitions found
                if (len(positions) == 0) and (stop_search is False):
                    print(f"[accumulate_positions]: (len(positions) == 0)")
                    stop_search = True
            else:
                refresh_search_stats = True
            
            # exclude this value from further search
            if accumulate_values:
                # update global skip value list
                skip_values.add(saved_hash_item.value)
                located_seed_values.add(saved_hash_item.value)
                # remove value from search
                values.discard(saved_hash_item.value)
                # ... twice (set of bytes is required for speed)
                byte_values.discard(int.to_bytes(saved_hash_item.value, saved_hash_item.value_length, byteorder=DEFAULT_BYTE_ORDER, signed=False))
                accumulated_values_count += 1
                # refresh min and max target value
                min_value = min(values)
                max_value = max(values)
                # stop search if all value possitions found
                if (len(values) == 0) and (stop_search is False):
                    print(f"[accumulate_values]: len(values) == 0")
                    stop_search = True
            else:
                refresh_search_stats = True
                # if found values are not accumulated - different strategy can be used: save any value from 
                # target list and stop when each value will be found at least once 
                # (each value can be saved many times ad different positions in this case)
                if (len(located_values) == len(values)):
                    # all target values located (have at least 1 position)
                    if stop_after_minimum_allocation and (stop_search is False):
                        seed_progress.write(f"stop_after_minimum_allocation=True: len(located_values) == len(values)")
                        #print(f"stop_after_minimum_allocation=True: len(located_values) == len(values)")
                        #print(f"{sorted(located_values)}")
                        #print(f"{sorted(values)}")
                        stop_search = True

            # save located position/value pairs for final result
            seed_items[saved_hash_item.seed][saved_hash_item.position] = saved_hash_item.value
            seed_counts.update({ saved_hash_item.seed: 1 })
            # collect located values
            if (saved_hash_item.value not in located_values):
                located_values.add(saved_hash_item.value)
                located_values_count += 1
        #### END OF SAVING NEW VALUES ###    

        # each seed will iterate only through non-excluded positions
        if (refresh_search_stats is True) or (update_seed_positions is True):
            # remove all discarded positions from search (this makes search inside next seed faster)
            for discarded_seed_position in discarded_seed_positions:
                seed_positions.remove(discarded_seed_position)
            # refresh min and max target value
            min_value = min(values)
            max_value = max(values)
            # mark position list updated
            update_seed_positions = False
            refresh_search_stats  = False
            # display current scan status
            # stats: maximum number if items found inside one seed
            if len(seed_counts) > 0:
                max_seed_items_count = seed_counts.most_common(1)[0][1]
            # stats: total number of unique values found during this scan
            if (cache_value_position_pairs):
                unique_values_count  = len(value_positions)
                positions_per_unique = cached_values_count / (unique_values_count + 1) # avoid division to zero
            else:
                unique_values_count  = len(value_position_counts)
                positions_per_unique = saved_values_count / (unique_values_count + 1)
            # remaining values
            values_count    = len(values)
            positions_count = len(positions)
            seed_progress.set_description_str(f"seed:{seed}/{end_seed}")
            seed_progress.set_postfix({
                "located[seed](left)"   : f"{located_values_count}[{seed_located_values_count}]]({values_count})",
                "scanned[seed]"         : f"{scanned_values_count}[{seed_scanned_values_count}]",
                "saved[seed]"           : f"{saved_values_count}[{seed_saved_values_count:<3}]",
                "skipped[seed]"         : f"{scanned_values_count-saved_values_count}[{seed_scanned_values_count-seed_saved_values_count:<3}]",
                "positions[capacity]"   : f"{start_position}-{end_position}[{values_count}->{located_values_count}/{tier_capacity}]",
                "max_vp[max_si]"        : f"{max_positions_count}[{max_seed_items_count}]",
                "unique"                : f"{unique_values_count}",
                "cached[hits]"          : f"{cached_values_count}[{cache_hits_count}]",
                "positions_per_unique"  : f"{positions_per_unique:8.2f}",
                "acc_positions[skips]"  : f"{accumulated_positions_count}/{positions_count}[{position_skips_count}]",
                "acc_values[skips]"     : f"{accumulated_values_count}[{value_skips_count}]",
                "min_value/max_value"   : f"{min_value}/{max_value}",
                "scans_per_value(max)"  : f"{scans_per_value}({max_scans_per_value})",
            })
            # all positions are found
            if (positions_count == 0):
                seed_progress.write("(positions_count == 0)")
                stop_search = True
            if (values_count == 0):
                seed_progress.write("(values_count == 0)")
                stop_search = True
            # refresh "scans per value counter"
            scans_per_value = 0
        
        if stop_search is False:
            # do not start check other "stop_serarch" conditions if we don't have a marker
            continue
        #### END OF SEED PROCESSING (ANOTHER HOT LOOP) ####

        # finish: all done, stop search
        if (cache_value_position_pairs):
            unique_values_count  = len(value_positions)
            positions_per_unique = cached_values_count / (unique_values_count + 1) # avoid division to zero
        else:
            unique_values_count  = len(value_position_counts)
            positions_per_unique = saved_values_count / (unique_values_count + 1)
        # remaining values
        values_count = len(values)
        # display current scan status one last time
        seed_progress.set_description_str(f"seed:{seed}/{end_seed}")
        seed_progress.set_postfix({
            "located[seed](left)"   : f"{located_values_count}[{seed_located_values_count}]]({values_count})",
            "scanned[seed]"         : f"{scanned_values_count}[{seed_scanned_values_count}]",
            "saved[seed]"           : f"{saved_values_count}[{seed_saved_values_count:<3}]",
            "skipped[seed]"         : f"{scanned_values_count-saved_values_count}[{seed_scanned_values_count-seed_saved_values_count:<3}]",
            "positions[capacity]"   : f"{start_position}-{end_position}[{values_count}->{located_values_count}/{tier_capacity}]",
            "max_vp[max_si]"        : f"{max_positions_count}[{max_seed_items_count}]",
            "unique"                : f"{unique_values_count}",
            "cached[hits]"          : f"{cached_values_count}[{cache_hits_count}]",
            "positions_per_unique"  : f"{positions_per_unique:8.2f}",
            "acc_positions[skips]"  : f"{accumulated_positions_count}/{len(positions)}[{position_skips_count}]",
            "acc_values[skips]"     : f"{accumulated_values_count}[{value_skips_count}]",
            "min_value/max_value"   : f"{min_value}/{max_value}",
            "scans_per_value(max)"  : f"{scans_per_value}({max_scans_per_value})",
        })
        break
    # clear search progress indication to avoid tqdm glitches
    seed_progress.clear()
    return {
        # last scanned_seed
        "last_seed"            : seed,
        # search results: seed_items[seed][position] = value
        "seed_items"           : seed_items,
        # popularity of each seed
        "seed_counts"          : seed_counts,
        # haw many values has been found
        "located_values"       : located_values,
        "skip_positions"       : skip_positions,
        # total number of new values saved to db (only actually created values counted)
        "saved_values_count"   : saved_values_count,
        # total number of scanned values
        "scanned_values_count" : scanned_values_count,
    }

def find_best_seeds_for_tier(address_length: int, values: Set[int], 
        skip_positions: Set[int]=set(), skip_values: Set[int]=set(), 
        target_seeds: Set[int]=set(), 
        min_position: int=None, max_position: int=None,
        use_all_tier_positions: bool=True) -> Counter:
    """
    Find seed values containing maximum values from condition, excluding values that already been found (skip values),
    and exclude positions that already beeb used (skip_positions), maintaining "one seed - one position" correspondence
    """
    # number of values found inside each seed
    seed_counts = Counter()
    # seed_values = defaultdict(lambda : defaultdict(set))
    if (min_position is None):
        min_position = address_length_to_offset(address_length)
    if (max_position is None):
        max_position = address_length_to_offset(address_length + 1)
    value_length     = address_length + 1
    # values that nedd to be found at any target position
    target_values    = None
    target_values    = sorted(list(values.copy()))
    # allowed value positions (excluding already used)
    #target_positions = None
    #target_positions = list()

    # 1) position query part
    position_condition = None
    position_condition = Q(position__gte=min_position, position__lte=max_position)
    if (len(skip_positions) > 0):
        position_condition = position_condition & Q(position__nin=sorted(skip_positions))
    #print(f"position_ranges_condition: {position_ranges_condition}")
    #print(f"position_items_condition: {position_items_condition}")
    
    # 2) seed query part
    seed_condition = None
    if (len(target_seeds) == 0):
        # target seed not set - create "empty condition"
        seed_condition = Q(seed__gte=0)
    elif (len(target_seeds) <= 1024):
        # number of seeds is not so large - mention them all
        seed_condition = Q(
            seed__gte=min(target_seeds), 
            seed__lte=max(target_seeds),
            seed__in=target_seeds
        )
    else:
        # large number of target seeds: use only min and max value to avoid query perfomance issues
        seed_condition = Q(
            seed__gte=min(target_seeds), 
            seed__lte=max(target_seeds)
        )
    
    # 3) value query part
    value_condition = None
    value_condition = Q(
        value__gte=min(target_values), 
        value__lte=max(target_values), 
        value__in=target_values
    )

    # final condition: all queries combined
    # items_condition = Q(value_length=value_length) & position_condition & (seed_condition & value_condition)
    # get max available seed for this query
    last_seed = get_last_seed(value_length=value_length, target_values=target_values.copy()) #, target_positions=target_positions.copy())
    # split search by chunks
    total_seed_chunks = (last_seed // (MAX_SEEDS_FOR_BEST_SEED_LOOKUP + min_position)) + 1
    
    # final result: collect mentions of each seed
    if (len(target_seeds) == 0):
        # display progress only for long queries (means "only when seed list is refresing")
        seed_chunk_progress = range(0, total_seed_chunks) #tqdm_notebook(range(0, total_seed_chunks), position=0)
    else:
        seed_chunk_progress = range(0, total_seed_chunks)
    # query results divided into chunks to avoid problems with MaxDocumentSize: 
    # https://www.mongodb.com/community/forums/t/size-19103932-is-larger-than-maxdocumentsize-16777216/111619/5
    for seed_chunk_number in seed_chunk_progress:
        # start of seed chunk
        first_chunk_seed           = seed_chunk_number * MAX_SEEDS_FOR_BEST_SEED_LOOKUP
        # end of seed chunk
        last_chunk_seed            = first_chunk_seed + MAX_SEEDS_FOR_BEST_SEED_LOOKUP
        chunk_seed_condition       = None
        seed_chunk_items_condition = None
        # manually check that result will not be empty (before quereing database)
        if (total_seed_chunks > 1) and (len(target_seeds) > 0):
            chunk_has_target_seed = False
            if (max(target_seeds) < last_chunk_seed):
                last_chunk_seed = max(target_seeds) + 1
                chunk_has_target_seed = True
            if chunk_has_target_seed is False:
                for target_seed in target_seeds:
                    # check that currently scanning seed chunk contains at least one target seed
                    if target_seed in range(first_chunk_seed, last_chunk_seed):
                        chunk_has_target_seed = True
                        break
                # do not perform database query when we have limited set of target seeds and current chunk 
                # does not contain any seed from this list
                if chunk_has_target_seed is False:
                    continue
        # condition to scan one part of total available seeds
        chunk_seed_condition       = Q(seed__gte=first_chunk_seed, seed__lt=last_chunk_seed) & seed_condition
        #seed_chunk_items_condition = items_condition & chunk_seed_condition
        seed_chunk_items_condition = Q(value_length=value_length) & position_condition & (chunk_seed_condition & value_condition)
        # print(f"{seed_chunk_number}/{total_seed_chunks} [{first_chunk_seed}-{last_chunk_seed}]: db_items_count={db_items_count}")
        # find seeds containing maximum target values 
        #db_items = HashItemValue.objects(items_condition)[start_item:end_item].only('seed').item_frequencies('seed')
        #db_items = HashItemValue.objects(seed_chunk_items_condition)[start_item:end_item].only('seed').item_frequencies('seed')
        db_items = HashItemValue.objects(seed_chunk_items_condition)\
            .limit(MAX_RESULTS_FOR_BEST_SEED_LOOKUP)\
            .only('seed')\
            .item_frequencies('seed')
        # update seed counter
        if (len(db_items) > 0):
            seed_counts.update(db_items)
        #print(f"{item_chunk_number}/{total_result_chunks} [{start_item}-{end_item}/{total_items_count}]: seed_counts.most_common(8): {seed_counts.most_common(8)}")
        #print(f"{item_chunk_number}/{total_result_chunks} [{start_item}-{end_item}/{total_items_count}]: db_items_counter.most_common(8): {db_items_counter.most_common(8)}")
    #if (len(target_seeds) == 0):
    #    seed_chunk_progress.clear()
    # return result as Counter
    return Counter(dict(seed_counts.most_common(256)))

def allocate_tier_values(address_length: int, values: Set[int], 
        priority_seeds: Set[int]=None, skip_seeds: Set[int]=None,
        min_position: int=None, max_position: int=None, last_scanned_seed: int=None,
        use_all_tier_positions: bool=True) -> TierVauesAllocation:
    """
    Find seed and position for every value from given set. All values must have same byte length.
    All values should be located within single continuous position range (range starts at the start of the tier)
    """
    # number of values that needs to be allocated
    target_values       = values.copy()
    target_value_count  = len(target_values)
    value_length        = address_length + 1
    #min_position        = address_length_to_offset(address_length)
    #max_position        = address_length_to_offset(address_length + 1)
    if (min_position is None):
        min_position = address_length_to_offset(address_length)
    if (max_position is None):
        if (use_all_tier_positions is True):
            # use all available positions
            max_position = address_length_to_offset(address_length + 1)
        else:
            # use minimum seed positions (only first positions at tier start)
            max_position = min_position + len(values)
    
    # use selected seeds only
    if (priority_seeds is None):
        target_seeds = set()
    else:
        # init target seeds from the priority seeds list (if any)
        target_seeds = priority_seeds.copy()
        # discard some of the target seeds if we need to
        if (skip_seeds is not None):
            for skip_seed in skip_seeds:
                target_seeds.discard(skip_seed)

    # use selected positions only
    target_positions = set()
    if (use_all_tier_positions is False):
      for position in range(min_position, max_position):
          target_positions.add(position)
    
    # item positions and item values, grouped by seed
    seed_positions      = defaultdict(set)
    seed_values         = defaultdict(set)
    # number of positions, used by each seed
    seed_positions_counter = Counter()
    # mapping: absolute position -> seed
    position_seeds      = dict()
    # mapping: absolute position -> data item value (unique)
    position_values     = dict()
    # mapping: data item value (unique) -> absolute position (it's a reversed "position_values" mapping)
    value_positions     = dict()
    # mapping: data item value (unique) -> seed
    value_seeds         = dict()
    # values and positions as sets, without grouping
    allocated_positions = set()
    allocated_values    = set()
    # seed values used in final allocation
    allocation_seeds    = set()
    # seed values excluded from search (containing duplicate values)
    excluded_seeds      = set()
    progress            = tqdm_notebook(total=target_value_count, position=0)
    last_top_seed_max_values = 0
    #last_scanned_seed        = None
    refresh_target_seeds     = True
    # allocate values: find seed value for every tier value, having address (position number) length shorter than value length
    while (len(allocated_values) < target_value_count):
        # find best seeds for remaining set of values, excluding already used positions:
        best_seeds = Counter()
        # clear outdated target seeds list periodically to load updated list from db
        if refresh_target_seeds:
            #print(f"Refreshing target seeds...")
            target_seeds.clear()
        
        # starting search from database values
        current_best_seeds = find_best_seeds_for_tier(
            address_length, 
            target_values.copy(), 
            allocated_positions.copy(), 
            allocated_values.copy(), 
            target_seeds=target_seeds.copy(),
            min_position=min_position, 
            max_position=max_position
        )
        # combine seeds from DB and seeds from hash space scan
        best_seeds.update(current_best_seeds)
        
        # scan new seeds only when we run out of current_best_seeds
        if (len(current_best_seeds) == 0):
            # allways start from seed 0 in order to maximize usage of already allocated seeds
            # and explore more values for most popular seeds
            if (last_scanned_seed is None):
                last_scanned_seed = 0 # get_last_seed(value_length, target_values=values.copy())
            # search additional seeds in hash space (adding more values to database during this process)
            print(f"Starting additional search for this allocation from start_seed={last_scanned_seed}...")
            # hide current progress (because 2 tqdm bars can conflict)
            progress.clear()
            new_best_seeds = scan_new_seeds_for_tier(
                address_length, 
                input_values=values.copy(), 
                skip_positions=allocated_positions.copy(), 
                skip_values=allocated_values.copy(), 
                start_seed=last_scanned_seed,
                end_seed=2**40,
                accumulate_positions=True,
                accumulate_values=True,
                cache_value_position_pairs=False, 
                stop_after_minimum_allocation=True, 
                check_existing=True,
                bar_position=0,
                use_all_tier_positions=use_all_tier_positions,
            )
            progress.display()
            # save last scanned seed from last search (next search should be started there)
            last_scanned_seed    = new_best_seeds['last_seed']
            # trigger target seed list refresh on next iteration
            refresh_target_seeds = True
            # trying to find new seeds again, with updated db
            print(f"Search done, resuming allocation...")
            #best_seeds.update(new_best_seeds['seed_counts'])
            continue
        # partially refresh current best seeds list 
        # (new target seeds is a part of previous target seeds, no global refresh)
        target_seeds.clear()
        for target_seed, _ in best_seeds.most_common(100):
            target_seeds.add(target_seed)
        # remove seeds containing incorrect values
        for excluded_seed in excluded_seeds:
            if excluded_seed in best_seeds:
                del best_seeds[excluded_seed]
                del current_best_seeds[excluded_seed]
                target_seeds.discard(excluded_seed)
        # check database to ensure that we have enough values and can continue search
        if (len(list(best_seeds.items())) == 0):
            #pprint(values, max_length=8)
            #print(len(allocated_positions), f"{allocated_positions[0:32]}")
            #print(len(allocated_values), f"{allocated_values[0:32]}")
            #raise Exception(f"Value database is too small for this tier")
            raise Exception(f"(len(list(best_seeds.items())) == 0):")
            continue
            #break
        # get best available seed
        # we know how many values we can get, so we can use limit to speed up query
        for top_seed, current_top_seed_max_values in best_seeds.most_common(100):
            if (top_seed not in excluded_seeds):
                break
        # max values per seed for current target seed list
        # check: is is time to refresh our target seed list?
        if (current_top_seed_max_values < last_top_seed_max_values) or (len(target_seeds) <= 4): # and (len(target_seeds) <= 32)) or (len(target_seeds) <= 32): # or (len(target_seeds) <= 32):
            # all "rich" seeds are used - it is time to update our target seed list before using
            # seeds with less values per item
            refresh_target_seeds = True
        else:
            refresh_target_seeds = False
        # save number of values, located inside the top seed - we will use it in next iteration
        last_top_seed_max_values = current_top_seed_max_values
        # create condition to find all values using same seed
        # exclude allocated values and positions from search condition
        #for skip_value in allocated_values:
        #    target_values.discard(skip_value)
        #for skip_position in allocated_positions:
        #    target_positions.discard(skip_position)
        # extract item positions and values
        top_seed_positions = set()
        top_seed_values    = set()
        # find values sharing same seed
        top_seed_items = HashItemValue.objects(
            Q(position__gte=min_position) & Q(position__lt=max_position) & 
            Q(position__nin=allocated_positions) &
            #Q(position__in=target_positions) &
            Q(value__in=target_values) & 
            Q(value_length=value_length) &
            Q(seed=top_seed)
        )
        for top_seed_item in top_seed_items:
            if top_seed_item.position in allocated_positions:
                #excluded_seeds.add(top_seed_item.seed)
                target_positions.discard(top_seed_item.position)
                continue
            if top_seed_item.value in allocated_values:
                #excluded_seeds.add(top_seed_item.seed)
                target_values.discard(top_seed_item.value)
                continue
            # update allocated positions and values lists
            top_seed_positions.add(top_seed_item.position)
            top_seed_values.add(top_seed_item.value)
            allocated_positions.add(top_seed_item.position)
            allocated_values.add(top_seed_item.value)
            target_positions.discard(top_seed_item.position)
            target_values.discard(top_seed_item.value)
            # update mappings
            position_values[top_seed_item.position] = top_seed_item.value
            value_positions[top_seed_item.value] = top_seed_item.position
            # update seed positions counter
            seed_positions_counter.update({ top_seed_item.seed: 1 })
        if len(top_seed_values) == 0:
            # exclude seeds that has no new values for this allocation
            excluded_seeds.add(top_seed)
            target_seeds.discard(top_seed)
            del best_seeds[top_seed]
            del current_best_seeds[top_seed]
        if (top_seed not in excluded_seeds):
            # save allocated positions and values
            seed_positions[top_seed].update(top_seed_positions)
            seed_values[top_seed].update(top_seed_values)
            allocation_seeds.add(top_seed)
            target_seeds.discard(top_seed)
            del best_seeds[top_seed]
            del current_best_seeds[top_seed]
        #else:
            #print(f"top_seed (EXCLUDED) : {top_seed} {best_seeds.most_common(5)}")
        progress.set_description(f"[{len(allocation_seeds)} seeds used]")
        progress.set_postfix({
            "current_seed"         : f"{top_seed} (+{len(top_seed_values)} values)",
            "available_seeds"      : f"{len(best_seeds)}",
            "next_seeds(5)"        : f"{best_seeds.most_common(5)}",
            "refresh_target_seeds" : f"{refresh_target_seeds}",
        })
        progress.update(len(top_seed_values))
    # create indexes
    for seed, positions in seed_positions.items():
        for position in positions:
            position_seeds[position] = seed
    for seed, values in seed_values.items():
        for value in values:
            value_seeds[value] = seed
    # return typed result
    return TierVauesAllocation(
        # allocated tier (common for all allocation values)
        tier                   = address_length,
        # number of unique seeds, used to allocate values of this tier
        total_seeds            = len(allocation_seeds),
        # all seeds, used by this allocation
        seeds                  = sorted(allocation_seeds),
        # all data item values for this tier, used for this allocation
        values                 = sorted(allocated_values),
        # what seed should we use for each position to decode our data?
        position_seeds         = dict(sorted(position_seeds.items())),
        # position->value (direct mapping, used for verification of encoded data)
        position_values        = dict(sorted(position_values.items())),
        # value->position (inversed mapping, used to encode data items to data item positions)
        value_positions        = dict(sorted(value_positions.items())),
        # number of positions that each seed uses for this allocation
        seed_positions_counter = seed_positions_counter,
    )

def collect_tier_values(target_value_length: int, data_values: Union[Set[Bits], List[Bits]]) -> List[int]:
    """
    Pick unique data values by length, (input list created by create_content_based_split())
    """
    tier_values = set()
    for data_value in data_values:
        item_byte_value = data_value.tobytes()
        value_length    = len(item_byte_value)
        if (value_length != target_value_length):
            continue
        item_int_value = int.from_bytes(item_byte_value, byteorder=DEFAULT_BYTE_ORDER, signed=False)
        tier_values.add(item_int_value)
    return tier_values

def create_seed_dict(split_result: ContentBasedSplit) -> List[int]:
    # define max used value tier for future allocation
    max_tier = get_max_position_tier(total_data_items=split_result.unique_values_count)




""""""
# TODO: create one common prefix for all values:
# count occurences of each prefix byte, 
# count remaining (not found yet) unique values for each prefix byte
# read more bytes from hash space only if next value length is a prefix/target for at least 1 remaining value
# make several reads when getting bytes from hash space: read maximim bytes from one position, 
# then append bytes until prefix contain values (several value checks from same one hash result)
# store all prefix values and value byte counts separatly (as trie)
# discard non-used values from prefix every time when new value found (when accumulation of values is enabled)
# TODO: multi-thread search with hierarchy:
# - spread work across processes using common byte prefix
# - each process works has unique, non-overlapping set of values, each set of values is a complete prefix subtree,
#   starting from common single byte
# TODO: multi-thread search - divide by position range
# TODO: multi-thread search - reuse each hash calculation result:
# - combine search for different value lengths
# - base search on prefix
# - run new value filtration as a tree, based on a common bytes prefix
# - filtration tree memoization: cache result of the filtration at each level for each byte value
# TODO: use values that has already been found for any search session
# - store all known values as prefix tree
# - save all occurences of each value 
#   (also we can store in database only unique values, but save links to every parent value for every new value)
# - during new value search: 
#   - build common bytes prefix
#   - find all prefix tree values in local database, starting from longest
#   - if prefix tree value is a target value - save it
#   - if prefix tree value is a prefix - read remaining bytes from the same position (to complete the value) 
#     and then check it. Save "values-prefixes" as well as data values. 
#     If value is not unique - create a link to it,
#   - when all long values with common prefix are finished
# - scan only positions containing values from common prefix
# TODO: start search from values with only one possible position, 
# try to find seed with several values only if all values have at least 2 different positions in any seed
# TODO: ability to continue scan for every position/value/seed:
# - continue value scan at single position for any seed
# - continue value search for a given prefix of any length
# TODO: links to short values created before can be used to build an efficent scan plan
# - get prefix tree
# - get all values from prefix tree, longest first
# - find longest common prefix for each prefix value (for all lengths)
# - find all positions of each value in prefix tree
# - read prefix value from each position
# - read and append extra bytes from same position, 
#   incrementally building new value until max_value_length is reached (e. g. until prefix will have no matches)
# - save all unique prefixes generated during search process
# - each position from db should contain value of maximum length reached during all previous searches:
#   if we get a position of the value from common prefixes tree - we get maximum available length (next bytes are unknown)
# - short refix values are more common and each value in db must have link to parent value position in the same seed
# - search starts from 1-byte values, collects only unique values, each value must be appended to existing value,
#   each new value triggers update of seed's common prefix tree, update all previous values, 
#   creation links to every common prefix value, update of all child values (if any)
# - seed positions for scan can be chosen arbitrarily
# - fresh database filling starts from search of all positions of 1-byte values
# - each 1-byte value position then passed to next-level processing: we read 2-byte value from same position
# - we read new bytes from same position until we have an unique value here
# - unique value is saved to db, links to all existing prefixes added automatically
# - new value now have a longest possible unique parent and can be used as parent for new values in future
# - using byte-aligned length for our values, each value can have 1 parent and 256 unique children
# - all occurences of each unique value is stored as links in a 3nf-table
# TODO: content-addressable values: ObjectId of each db value is a value itself, presented in hex, prefixed by 0
# TODO: layered value mapping for seed: tree, created from 1-byte values - level 0 contains bitmap with value positions
# each value can have mapping for each seed space
# each seed have one mapping for every unique value
# bitmaps of same position intervals in different can be combined/intersected to find combination of seeds 
# containing all required values at proper positions
# combining 2 bitmaps of 2 different 1-byte values inside same seed can help locate all positions of 2-byte values
# produced by (in any order)
# long values will have sparse position mapping, long values will create their mappings from stort value mappings:
# mapping of long value is allways a result of intersection of two short value mappings
# 
# Bitmap tree build process starts from level 0 and goes up until all value bits will contain "1"
# bitmaps containing 0 in all position is not stored (at any level)
# New values can be added to database only through bitmap scan process:
# worker read each position from previx value bitmap and get value from this position 
# (using proper value length: +1 byte from current longest value at this position)
# when all bitmap positions scanned - worker switches to 1-byte values creation from last available position
# diring this process worker will search new positions of existing values, but saves only values that never been seen
# in this seed before: each position inside any seed can contain only one, unique value 
# (but must have links back to every parent value with length n-1, n-2, n-3 and so on, 
# until we arrive to one of 256 inital 1-byte values, auto-created when their seed was added)
# 
# TODO: BitmapLevel(id, (level_)value_id)
#       # MappingValue(id, level_value_id, value_id, value_length_id)
# TODO: Bitmap(id, bitmap_position,
#             bitmap_level_id, int_bitmap_value, byte_bitmap_value, bitmap_value_id, 
#             start_position_id, end_position_id, nested_value_count, own_value_count) # 
#       # MappingLevelValue(id, mapping_level_id, mappingId, valueId)
# TODO: SeedBitmap(id, seed_id, bitmap_id)
# TODO: SeedValueBitmap(id, seed_id, value_id, bitmap_id, value_length_id) # +seed_bitmap_id, seed_value_id, items_count, first_position, last_position, 
# TODO: SeedValueBitmapProjection(id, head_seed_value_bitmap_id, tail_seed_value_bitmap_id, result_value_bitmap_id)
#
# TODO: ability to use one Stratum V2 header-only mining 
# to create hash space from header data: https://ru.braiins.com/stratum-v2#header 
# Hash space size:  2^(NONCE_BITS + VERSION_ROLLING_BITS) = ~280Th, where NONCE_BITS = 32 and VERSION_ROLLING_BITS = 16
# use different job type to combine mining with value search
# SolutionStructure: https://docs.google.com/document/d/1FadCWj-57dvhxsnFM_7X806qyvhR0u3i85607bGHxvg/edit#heading=h.7v5lyc8gzbdz
# NewMiningJob: https://docs.google.com/document/d/1FadCWj-57dvhxsnFM_7X806qyvhR0u3i85607bGHxvg/edit#heading=h.nwqxi1avxr2i
# SubmitShares: https://docs.google.com/document/d/1FadCWj-57dvhxsnFM_7X806qyvhR0u3i85607bGHxvg/edit#heading=h.v679s11hovb8
# TODO: use cyclic bit shift to extract all possible values from single hash
# TODO: content-addressable bitmap value (objectId, 12 bytes): 
# - 0: bitmap header - defines witch bytes will be used for prefix path and witch is used for children values
#      all empty value bytes is used for path prefix
#      children value bytes: 1-8
#      path value bytes: 2-9
# - 1: bitmap level (in bytes): 0-256, [bitmap_length = (8 ** bitmap_level)]
# - 2-5: bitmap position, in bits (based on bitmap level: bit_position = ((8 ** bitmap_level) * bitmap_position)), 4 bytes
# - 6..11: full path as prefix (each byte defines one of 256 options in each level, byte position defines tree level)
#          6-byte path (and value) is maximum (6-level tree)
# Bytes read as big-endian, first byte containing all zeroes terminates path length
# (because first byte of the path must have at least one bit in "true" state)
# TODO: try to use entire ObjectId as mapping tree path (last byte stores target bitmap index)
# TODO: all children bitmaps linked to parent via parentId
# TODO: separate bitmap for scanned/skipped seed/value positions or required sequentual position scan
#       for level 0 and restriction for parent bitmaps to use any positions other than provided by children bitmaps
# new parent bitmap can be created only from fully scanned (finished) non-interupting sequence of bitmaps on previous level
# if sequence of 8 bitmaps have only "11111111" values it is ok to not combine them into next-level bitmap
# if bitmap has at least one bit set to "0" - it must not have any children bitmap at this position
# TODO: add absolute position to bitmap data: value 0-7, stores absolute position of the bitmap inside children list
# TODO: search for a repeating pattern inside endire value bitmap, reference to them via links to past bitmap segments
# TODO: combine self-referencing sequence search with unique value search
# TODO: link bitmap subtrees with sequentual hash space ranges, map each bitmap subtree to its own hash space segment,
#       add new bytes to bitmap only as links to the current seed hash space
# Combine search of the unique values with bitmap creation:
# - each seed hash space initilazes with 256 unique 1-byte values
# - one bitmap must be created for each 1-byte value: this bitmap contains "0" in all positions 
# except first position of the target value
# - scan of every new bitmap can utilize bitmaps created earlier for other values: 
#   for example: seed hash space init function can set "0" to all positions containing other values
#   at this position inside any other value bitmap
# - last value bitmap can be computed without hany hash calculation at all, because 
#   at this moment every position at the start can be in 2 states:
#   1) it was scanned before, when we searched for previous values, this position contains one 
#      of 255 previously mapped values, mark it as "0" (this position does not contain value fron our mapping)
#   2) this position has no value associated with it - the only available option 
#      for this position is our last value - mark this position as "1" (this position contains our value)
# - during scan we get bytes from hash space as blocks, compare each byte of this block with 
#   256 mappings, updating any of them if nessesary
# - values at same position with same length inside same seed exclude each other 
#   (hash space can have only one value at each position)
# - high-level bitmaps can only be created/updated via composing two low-level bitmaps
# - new bitmap for new value can only be created as combination of two of 256 inital bitmaps
# - new unique seed value can be created only by appending one of inital 256 1-byte 
#   values to any existing (or inital) value
# - new value bytes is a concatenation of head_value_bytes and tail_value_bytes
# - new value bitmap is created automatically, without hash space scanning:
#   this can be achieved by scanning "tail value" bitmap and performing bitwise 
#   check of "tail value" bitmap in all positions going next after "tail value" positions vith "1"
#   Resulting bitmap will contain all hash space positions containing "tail+head" value in next 2 bytes 
#   (in case when we combine two 1-byte values)
# - bitmap segment can be described as a link to the previously discovered value 
#   (do not allow use of values that has been discovered AFTER current bitmap segment was created)
#   new seed hash space will allways contain all 255 possible byte values - it is enough to describe
#   any furture bitmap
# - direct read from hash space is used only when creating 1-byte, low-level bitmaps
#   direct read from hash space allways should use all 256 bitmaps in order to
#   maximize hashing power utilization: each located value can be used to set "1" to
#   located value at this position and "0" to all 255 remaining bitmaps
# - "0" at any byte position can be reliable verified and proved by providing "1" 
#   for any other value with same seed, length and position
# Conclusion: instead of storing "0" for any value it is better to store "1" for any other value 
# at the same position. Scan should run for all base values at the same position in the same time.
# No ability to exclude base values from scanning (scan and update bitmap for all or not update bitmap at all)
# All auto-created 1-byte values share the same "last scanned position" value: this value
# stored as 1 of 256 values that has currently contain last scanned position
# TODO: if we have common "last position" value, we can also store chain of the new values in order of creation:
# this chain can be used to more quickly check all previously scanned positions, 
# allowing at any position to know:
# - what bitmap should be checked for a value at this position
# - what bitmap should be checked for previous/next position value
# TODO: https://www.mongodb.com/docs/manual/tutorial/query-a-2d-index/#exact-matches-on-a-flat-surface
# TODO: https://www.mongodb.com/docs/manual/reference/operator/query-bitwise/
# TODO: https://www.mongodb.com/docs/manual/reference/operator/query/polygon/



# TODO: BitLength(id, (bit_length_)value_id)
# TODO: ByteLength(id, (byte_length_)value_id)
# TODO: Length(id, bit_length_id, byte_length_id) # bit_length_value_id, byte_length_value_id
#       # == LengthValue(id, value_id, length_id, # int_bit_length_value, int_byte_length_value,
#         # +byte_bit_length_value, byte_byte_length_value, # bit_length_id, byte_length_id,) 
#         # +(default_)value_id, # ValueLength(id, ((value_)length_)value_id), 
#         # +length_value_id # + bit_length_value_id, byte_length_value_id,
# TODO: Value(id, 
#             int_value, byte_value, 
#             bit_value_length, byte_value_length, 
#             value_length_id,
#             parent_int_value, parent_byte_value, 
#             parent_value_id,
#             parent_bit_value_length, parent_byte_value_length, parent_value_length_id,
#       ), 
#       # +value_length_id, bit_value_length == int_value_length
#       # +value_tier_id
#       ValueLength(id, value_id, length_id)
#       ValueTier(id, (value_tier_)value_id)
#       ValueLengthTier(id, value_length_id, value_tier_id)
# TODO: BitPosition(id, (bit_position)value_id)
# TODO: BytePosition(id, (byte_position)value_id)
# TODO: Position(id, (position_)value_id), # , position_length_id # +((bit_)position_)value_id, ((byte_)position_)value_id 
#       PositionTier(id, (position_tier)_value_id)
#       PositionValue(id, position_id, value_id) # == ValuePosition, # ValuePosition(id, value_id, position_id), # , value_length_id, position_length_id == PositionValue
#       PositionLength(id, position_id, length_id) # PositionLength(id, (((position_)length_)value_id) # == LengthPosition, 
# TODO: Seed(id, (seed_)value_id), # , seed_length_id, Seed.value_id == Seed.seed_value_id
#       SeedLength(id, seed_id, length_id)
#       SeedPosition(id, seed_id, position_id), # , seed_length_id, position_length_id == PositionSeed
#       SeedValue(id, seed_id, value_id) # == ValueSeed
#       SeedPositionValue(id, seed_id, position_id, value_id) # +seed_position_id, seed_value_id, position_value_id
#       # == SeedPositionValue == PositionValueSeed == PositionSeedValue == ValueSeedPosition == ValuePositionSeed
# TODO: Address(id, (address_)value_id) # Address(id, (address_)value_id), # position_id, address_length_id, value_length_id, tier_id) # , value_length_id, address_length_id
#       AddressTier(id, (address_tier)_value_id)
#       AddressLength(id, address_id, length_id) # AddressLength(id, ((address_)length_value_id) # == LengthAddress, # , int_value, byte_value 
#       AddressLengthTier(id, length_address_id, address_tier_id)
#       AddressPosition(id, address_id, position_id)  # PositionAddress(id, position_id, address_id) # PositionAddress(id, position_id, address_value_id)
#       AddressPositionTier(id, address_position_id, position_tier_id)
#       AddressLengthPosition(id, address_id, length_id, position_id) # +address_length_id, address_position_id, position_length_id
#       AddressSeed(id, address_id, seed_id) # SeedAddress(id, seed_id, address_id)
#       AddressSeedTier(id, address_seed_id, address_tier_id)
#       AddressValue(id, address_id, value_id) # == ValueAddress 
#       AddressValueTier(id, address_value_id, value_tier_id)
# TODO: Score(id, (score_)value_id)
# TODO: Tier(id, address_length_id, value_length_id) # , tier_score_id
#       TierAddressLength(id, tier_id, address_length_id)
#       TierValueLength(id, tier_id, value_length_id)
#       TierSeed(id, seed_id, tier_id) # == SeedTier
#       TierAddress(id, tier_id, address_id) # == AddressTier
#       TierValue(id, tier_id, value_id) # == ValueTier
#       TierValueSeed(id, tier_id, value_id, seed_id) 
#       TierScore(id, tier_id, score_id) # +score=value_length - address_length # tier_address_length_id, tier_value_length_id
#       # == TierSeedValue == SeedTierValue == ValueTierSeed == ValueSeedTier == SeedValueTier
# TODO: Item(id, seed_id, position_id, value_id) # seed_position_id, seed_value_id, position_value_id
#       ItemValue(id, item_id, value_id)
#       ItemPosition(id, item_id, position_id)
#       ItemSeed(id, item_id, seed_id)
#       ItemAddress(id, item_id, address_id)
#       ItemValueLength(id, item_id, value_id, length_id) # +item_value_id, item_address_id, value_length_id
#       ItemAddressLength(id, item_id, address_id, length_id) # +item_value_id, item_address_id, address_length_id 
#       ItemTier(id, item_id, tier_id)
# TODO: ParentValue(id, parent_value_id, child_value_id) # , parent_value_length_id, value_length_id
#       ParentLevel(id, (parent_level_)value_id)
#       ParentValueLength(id, parent_value_id, child_value_id, value_length_id) # 
#       ParentValueAddress(id, parent_value_id, address_id)
#       ParentTier(id, parent_value_length_id, child_value_length_id)
#       ParentTierScore(id, parent_value_tier_id, score_id) # +score=value_length - parent_value_length
#       ParentValueTierValue()
