# https://stackoverflow.com/questions/38340808/recursive-typing-in-python-3-5
from __future__ import annotations
from rich import print
from rich.pretty import pprint
from custom_counter import CustomCounter as Counter
#from tqdm import tqdm
from bitarray.util import ba2int, int2ba, canonical_huffman, huffman_code
from bitarray import bitarray, frozenbitarray
from collections import defaultdict, namedtuple
from dataclasses import dataclass, field
from copy import deepcopy, copy
from typing import List, Set, Dict, Tuple, Optional, Union, Iterable, Generator, Generic, Optional, Protocol,\
    TypeVar, FrozenSet, Hashable, Sequence
#from functools import lru_cache
#import xxhash
#import math
#from bitarrayset.binaryarrayset import BinaryArraySet
from enum import Enum, IntEnum
from sortedcontainers import SortedSet, SortedDict, SortedList
# pip install git+https://github.com/djbpitt/pyskiplist.git@dumpNodes 
from pyskiplist import SkipList

from fib_encoder import from_c1_bits, to_c1_bits, get_encoded_c1_length, get_encoded_c1_bits
from custom_varint import DEFAULT_PREFIX_LENGTH, encode_varint_number, decode_varint_number
from hash_range_iterator import int_bits_from_nounce, last_int_bits_from_nounce, int_from_nounce,\
    last_ba_bits_from_nounce, last_ba_bits_from_digest, last_fba_bits_from_digest, last_int_bits_from_digest
from custom_varint import encode_varint_number, decode_varint_number, get_number_range,\
    create_number_ranges

# размер множества с потенциальными значениями для COnsumableSet
DEFAULT_SET_BATCH_SIZE     = 32
# количество бит для указания длины varint-числа в битах при использовании префиксного кодирования чисел
DEFAULT_VARINT_PREFIX_BITS = 4

# max 65534 options for remaining_number_id in seed tree
SEED_VARINT_PREFIX_BITS   = 3
# max 254 options for remaining_number_id in each nounce tree
NOUNCE_VARINT_PREFIX_BITS = 3

DEFAULT_NUMBER_ID_OFFSET  = 1
DEFAULT_HASH_ITEM_SCORE   = 1
DEFAULT_ENDIAN            = 'little'
DEFAULT_TARGET_LENGTH     = 16
MAX_DATA_ITEM_CODE_LENGTH = 32

@dataclass()
class VarintNumberRange:
    start          : int = field()
    end            : int = field()
    prefix_length  : int = field()
    number_length  : int = field()
    encoded_length : int = field(default=None, init=False)

    def __post_init__(self):
        self.encoded_length = self.prefix_length + self.number_length

def create_number_ranges_for_prefix_length(prefix_length: int, offset: int=0) -> Dict[int, VarintNumberRange]:
    ranges    = SortedDict()
    max_value = 0
    for number_length in range(0, 2**prefix_length):
        min_value = max_value
        max_value = min_value + 2**number_length
        number_range = VarintNumberRange(
            start         = min_value + offset,
            end           = max_value + offset,
            prefix_length = prefix_length,
            number_length = number_length,
        )
        ranges[number_range.encoded_length] = number_range
    return ranges

class SkipDict(SkipList):
    """
    Modification of original SkipList: https://github.com/djbpitt/pyskiplist.git
    Duplicate key-value pairs are not allowed
    """

    def has_value(self, value) -> bool:
        return (value in set(self.values()))
    
    def has_key(self, key) -> bool:
        return (key in set(self.keys()))
    
    def has_index(self, index) -> bool:
        return (index < self.__len__())

    def insert(self, key, value):
        """
        Insert a key-value pair in the list.
        Restrict insertion of existing key/value pairs
        """
        if (self.__contains__(key=key) is True):
            current_value = self.search(key=key)
            if (current_value == value):
                # item already in set
                return
            if (current_value is not None):
                raise Exception(f"Cannot add new item: key={key}, value={value}: this key already contains value={current_value}")
        super().insert(key=key, value=value)

@dataclass
class ConsumableSet:
    """
    Поглощаемый (или расходуемый) диапазон целых чисел: диапазон значений из которого каждое число можно использовать только 1 раз
    Поддерживает две отдельные нумерации для использованных чисел и для оставшихся: оба списка чисел нумеруются 
    от меньшего к большему
    
    Восстановление использованных чисел (то есть возврат их обратно в список "неиспользованных") не поддерживается

    Число с id=0 всегда присутствует в списке оставшихся значений (self.numbers.remaining_values) и всегда содержит 0
    
    TODO: попробовать BinaryArraySet для хранения использованных/оставшихся значений и посмотреть будет ли эффективнее
    """
    # список изначальных чисел без изменений
    remaining_dict       : SkipDict       = field(default_factory=SkipDict)
    # список использованных значений
    consumed_values      : Dict[int, int] = field(default_factory=SortedDict)
    # список оставшихся чисел (которые еще не были использованы)
    remaining_values     : Dict[int, int] = field(default_factory=SortedDict)
    # автоматически пополнять список оставшихся чисел при использовании нового числа
    # (необходимо чтобы список оставшихся чисел не заканчивался)
    refill_on_consume    : bool           = field(default=False)
    # число начиная с которого нужно продолжить заполнение диапазона возможных значений 
    # (если класс используется для выбора значений из непрерывного множества)
    start_batch_value    : int            = field(default=1)

    def __post_init__(self):
        self.remaining_dict.insert(key=0, value=0)
        self.consumed_values[0] = 0

    def get_next_consumed_value_id(self) -> int:
        return len(self.consumed_values)
    
    def get_next_remaining_value_id(self) -> int:
        return len(self.remaining_values)
    
    def get_next_remaining_dict_id(self) -> int:
        return len(self.remaining_dict)

    def has_consumed_value(self, value: int) -> bool:
        return (value in self.consumed_values.values())
    
    def has_consumed_value_id(self, value_id: int) -> bool:
        return (value_id in self.consumed_values)
    
    def has_remaining_value(self, value: int) -> bool:
        return (value in self.remaining_values.values())
    
    def has_remaining_value_id(self, value_id: int) -> bool:
        return (value_id in self.remaining_values)
    
    def has_remaining_dict_id(self, dict_id: int) -> bool:
        return self.remaining_dict.has_key(key=dict_id)
    
    def get_max_remaining_dict_id(self) -> int:
        next_dict_id = self.get_next_remaining_dict_id()
        max_dict_id  = next_dict_id - 1
        return max_dict_id
    
    def get_max_remaining_value_id(self) -> int:
        next_value_id = self.get_next_remaining_value_id()
        max_value_id  = next_value_id - 1
        return max_value_id
    
    def get_max_remaining_value(self) -> int:
        max_remaining_value_id = self.get_max_remaining_value_id()
        self.remaining_values[max_remaining_value_id]
    
    def get_remaining_values(self) -> Dict[int, int]:
        remaining_values    = SortedDict()
        # 0 is a required system element: "no_id" marker
        remaining_values[0] = 0
        remaining_value_id  = 1
        consumed_values     = SortedSet(self.consumed_values.values())
        for _dict_index, item in enumerate(self.remaining_dict.items()):
            _dict_key = item[0]
            value     = item[1]
            if (value in consumed_values):
                continue
            # print(f"id={remaining_value_id}, value={value}")
            remaining_values[remaining_value_id] = value
            remaining_value_id += 1
        return remaining_values
    
    def get_consumed_values(self) -> Iterable[int]:
        return self.consumed_values.values()
    
    def update_remaining_values(self) -> Dict[int, int]:
        self.remaining_values = self.get_remaining_values()
        return self.remaining_values
    
    def get_consumed_value_id(self, value: int) -> int:
        if (self.has_consumed_value(value=value) is False):
            raise Exception(f"consumed value={value} not found")
        for consumed_id, consumed_value in self.consumed_values.items():
            if (consumed_value == value):
                return consumed_id
        raise Exception(f"consumed value_id not found for value={value}")
    
    def get_remaining_value_id(self, value: int) -> int:
        if (self.has_remaining_value(value=value) is False):
            raise Exception(f"remaining value id not found for value={value}")
        for remaining_id, remaining_value in self.remaining_values.items():
            if (remaining_value == value):
                return remaining_id
        raise Exception(f"remaining value id not found for value={value}")
    
    def add_consumed_value(self, value: int) -> int:
        if (self.has_consumed_value(value=value)):
            raise Exception(f"consumed value={value} already exists")
        value_id = self.get_next_consumed_value_id()
        self.consumed_values[value_id] = value
        return value_id
    
    def add_remaining_value(self, value: int) -> int:
        if (self.has_remaining_value(value=value)):
            raise Exception(f"remaining value={value} already exists")
        if (self.has_consumed_value(value=value)):
            raise Exception(f"remaining value={value} already exists, and has been consumed")
        dict_id  = self.get_next_remaining_dict_id()
        value_id = self.get_next_remaining_value_id()
        self.remaining_dict.insert(key=dict_id, value=value)
        self.update_remaining_values()
        return value_id
    
    def refill_remaining_dict(self, consumed_id: int, consumed_value: int) -> int:
        #next_dict_value = self.start_batch_value
        #next_dict_id    = self.get_next_remaining_dict_id()
        #self.remaining_dict.insert(key=next_dict_id, value=next_dict_value)
        #self.start_batch_value += 1
        max_remaining_value = self.get_max_remaining_value()
        if (max_remaining_value == consumed_value):
            return consumed_id
        next_dict_id    = self.get_next_remaining_dict_id()
        max_dict_id     = next_dict_id - 1
        max_dict_value  = self.remaining_dict[max_dict_id]
        next_dict_value = max_dict_value[1] + 1
        self.remaining_dict.insert(key=next_dict_id, value=next_dict_value)
        return next_dict_id
    
    def consume_value(self, value: int) -> int:
        if (self.has_remaining_value(value=value) is False):
            raise Exception(f"remaining value={value} not exists")
        if (self.has_consumed_value(value=value)):
            raise Exception(f"consumed value={value} already exists")
        consumed_id = self.add_consumed_value(value=value)
        # auto-refill remaining value list
        if (self.refill_on_consume):
            self.refill_remaining_dict(consumed_id=consumed_id, consumed_value=value)
        # update remaining values with new refreshed dict
        self.update_remaining_values()
        return consumed_id
    
    def add_remaining_values(self, values: List[int]) -> None:
        unique_values = SortedSet(values)
        if (len(unique_values) != len(values)):
            raise Exception(f"provided values is not unique")
        # first loop only perform validation, without data change
        for value in values:
            if (self.has_remaining_value(value=value)):
                raise Exception(f"remaining value={value} already exists")
            if (self.has_consumed_value(value=value)):
                raise Exception(f"remaining value={value} already exists, and has been consumed")
        # second loop only performs insertions on validated data: no rollback needed for incorrect input
        for value in values:
            dict_id = self.get_next_remaining_dict_id()
            self.remaining_dict.insert(key=dict_id, value=value)
        self.update_remaining_values()
    
    def add_remaining_values_batch(self, batch_size: int=DEFAULT_SET_BATCH_SIZE) -> None:
        start_batch  = self.start_batch_value
        end_batch    = start_batch + batch_size
        batch_numbers = list(range(start_batch, end_batch))
        self.add_remaining_values(values=batch_numbers)
        self.start_batch_value = end_batch

####################
### NUMBERS TREE ###
####################

class NumberType(int, Enum):
    CONSUMED  : int = 0
    REMAINING : int = 1

class NumberChangeType(str, Enum):
    NUMBER_ID      : str = 'NUMBER_ID'
    NUMBER_ID_CODE : str = 'NUMBER_ID_CODE'
    ID_CODE_LENGTH : str = 'ID_CODE_LENGTH'

@dataclass
class NumberChangeData:
    change_type : NumberChangeType                 = field()
    old_value   : Union[int, frozenbitarray, None] = field()
    new_value   : Union[int, frozenbitarray, None] = field()

@dataclass
class NumberUpdate:
    number         : int                    = field()
    number_id      : int                    = field()
    number_id_code : frozenbitarray         = field()
    changes        : List[NumberChangeData] = field(default_factory=list)

class TreeActionType(str, Enum):
    REGISTER_PREFIX_ITEM     : str = 'REGISTER_PREFIX_ITEM'
    REGISTER_UNIQUE_VALUE    : str = 'REGISTER_UNIQUE_VALUE'
    REGISTER_REMAINING_VALUE : str = 'REGISTER_REMAINING_VALUE'
    UPDATE_VALUE_ID_COUNTS   : str = 'UPDATE_VALUE_ID_COUNTS'
    UPDATE_VALUE_ID_CODES    : str = 'UPDATE_VALUE_ID_CODES'
    ADD_SORTED_VALUE         : str = 'ADD_SORTED_VALUE'
    CONSUME_REMAINING_VALUE  : str = 'CONSUME_REMAINING_VALUE'

@dataclass
class ValueTreeAction:
    type    : TreeActionType = field()
    payload : Dict           = field(default_factory=dict)

@dataclass
class SavedNumber:
    number            : int                              = field(default=None)
    number_id         : int                              = field(default=None)
    instance_id       : int                              = field(default=None)
    number_type       : NumberType                       = field(default=None)
    encoded_number    : bitarray                         = field(default_factory=bitarray)
    decoded_number    : int                              = field(default_factory=bitarray)
    encoded_length    : int                              = field(default=None)
    is_new_number     : bool                             = field(default=None)
    # actions, executed to save save this number
    executed_actions  : List[ValueTreeAction]            = field(default_factory=list)
    # result of each executed action
    action_results    : List[Tuple[TreeActionType, int]] = field(default_factory=list, repr=False)
    # all changes. performed inside tree after executing saving this number (may include several changed numbers)
    tree_updates      : List[NumberUpdate]               = field(default_factory=list)
    
@dataclass
class NumbersTree:
    """
    Дерево Хаффмана с ограниченным набором уникальных значений (целых чисел). Наиболее часто используемые
    значения имеют самые короткие коды. Перестраивается каждый раз после добавления нового значения.
    Это дерево запоминает и хранит все числовые значения которые были использованы хотя бы 1 раз
    для указания значений всегда используются их id в ConsumableSet: 
    - `consumed_id`: номер ранее добавленного значения
    - `remaining_id`: номер номер значения из списка оставшихся
    """
    # все уникальные значения чисел в этом дереве в виде исчерпаемого множества 
    # (все добавленные значения хранятся в numbers.consumed_values)
    numbers               : ConsumableSet               = field(default_factory=ConsumableSet, repr=False)
    # alias для numbers: все уникальные значения чисел в этом дереве как множество 
    # (значения автоматически сортируются по возрастанию)
    sorted_numbers        : Set[int]                    = field(default_factory=SortedSet)
    # изначальный размер диапазона для numbers
    init_batch_size       : int                         = field(default=DEFAULT_SET_BATCH_SIZE)
    # режим отладки (в этом режиме класс автоматически декодирует и проверяет каждый элемент при генерации кодов)
    debug                 : bool                        = field(default=True)
    # порядковый номер добавленного числа (в отличии от number_id считаются все добавленные числа а не только уникальные)
    number_instance_id    : int                         = field(default=1)
    # количество использований id каждого значения 
    # (это важно - мы считаем не количество использований самого значения а количество использований его id)
    id_counts             : Counter                     = field(default_factory=Counter, init=False)
    # коды Хаффмана для id чисел
    id_codes              : Dict[int, bitarray]         = field(default_factory=SortedDict, init=False)
    # reverse code/id mapping
    code_ids              : Dict[bitarray, int]         = field(default_factory=SortedDict, init=False)
    # mapping для определения длины кода для каждого id (ключ - id числа, значение - длина кода для этого id)
    id_code_lengths       : Dict[int, int]              = field(default_factory=SortedDict, init=False)
    # все используемые варианты длины кода id
    code_lengths          : Set[int]                    = field(default_factory=SortedSet, init=False)
    # все коды Хаффмана, сгруппированные по длине кода
    id_codes_by_length    : Dict[int, Set[bitarray]]    = field(default_factory=lambda: defaultdict(SortedSet), init=False)
    # все id значений, сгруппированные по длине кода
    ids_by_length         : Dict[int, Set[int]]         = field(default_factory=lambda: defaultdict(SortedSet), init=False)
    # значение нулевого элемента (специальный маркер всегда равен 0)
    zero_element          : int                         = field(default=0, init=False)
    # значение id нулевого элемента (специальный маркер,всегда равен 0)
    zero_element_id       : int                         = field(default=0, init=False)
    # код id нулевого элемента (может меняться по мере роста дерева и добавления новых чисел)
    zero_element_code     : bitarray                    = field(default=None, init=False)
    # длина кода id нулевого элемента (в битах)
    zero_element_length   : int                         = field(default=1, init=False)
    # счетчик количества кодов разной длины в дереве: обнуляется и пересчитывается при каждом перестроении дерева
    # поэтому считает количество элементов каждой длинны в текущем дереве а не общее количество использования 
    # всех элементов каждой длины
    code_length_counts    : Counter                     = field(default_factory=Counter, init=False)
    # список незавершенных операций для перестроения дерева после должен быть пустым после добавления нового элемента
    pending_actions       : List[ValueTreeAction]       = field(default_factory=list)
    # порядок чтения/записи двоичных данных при генерации кодов Хаффмана и varint-кодов чисел
    default_endian        : str                         = field(default=DEFAULT_ENDIAN, init=False, repr=False)
    # количество префиксных бит при указании длины числа в varint-формате
    varint_prefix_length  : int                         = field(default=DEFAULT_VARINT_PREFIX_BITS, repr=False)

    def __post_init__(self):
        # заполняем изначальный набор возможных значений
        self.numbers = ConsumableSet(refill_on_consume=True)
        self.numbers.add_remaining_values_batch(batch_size=self.init_batch_size)
        # создаём стартовое дерево Хаффмана
        self.id_counts.update({ self.zero_element_id: 0 })
        self.update_id_codes()
    
    ### CHANGE MANAGEMENT API ###

    def is_empty_tree(self) -> bool:
        if (self.id_counts[self.zero_element_id] == 0):
            return True
        return False

    def register_prefix_item(self):
        self.id_counts.update({ self.zero_element_id : 1 })

    def register_unique_value(self, value: int) -> int:
        if (self.has_consumed_number(number=value) is True):
            raise Exception(f"unique number={value} already registered")
        consumed_number_id = self.numbers.consume_value(value=value)
        #self.id_counts.update({ consumed_number_id: 1 })
        self.id_counts.update({ consumed_number_id: 0 })
        self.sorted_numbers.add(value)
        return consumed_number_id

    def save_number(self, number: int, update_tree: bool=True):
        number_type = self.get_number_type(number=number)
        number_id   = None
        
        if (number_type == NumberType.CONSUMED):
            number_id     = self.get_consumed_number_id(number=number)
            is_new_number = False
            self.add_pending_action(ValueTreeAction(
                type    = TreeActionType.UPDATE_VALUE_ID_COUNTS,
                payload = dict(id_counts=Counter({ number_id: 1 }))
            ))
        elif (number_type == NumberType.REMAINING):
            is_new_number = True
            self.add_pending_action(ValueTreeAction(
                type    = TreeActionType.REGISTER_PREFIX_ITEM,
                payload = None,
            ))
            self.add_pending_action(ValueTreeAction(
                type    = TreeActionType.REGISTER_UNIQUE_VALUE,
                payload = dict(value=number)
            ))
        else:
            raise Exception(f"unknown number_type={number_type} for number={number}")

        encoded_number = self.encode_number(number=number)
        saved_number   = SavedNumber(
            number         = number,
            number_id      = number_id,
            instance_id    = self.number_instance_id,
            number_type    = number_type,
            encoded_number = encoded_number,
            encoded_length = len(encoded_number),
            is_new_number  = is_new_number,
        )
        if (self.debug):
            saved_number.decoded_number = self.decode_number(encoded_number=encoded_number)
        # update tree just after adding new number
        if (update_tree):
            saved_number.executed_actions = self.pending_actions.copy()
            saved_number.action_results   = self.run_pending_actions()
            saved_number.tree_updates     = self.update_id_codes()
            if (saved_number.number_id is None):
                # add number id assigned after saving (it is None for new numbers)
                saved_number.number_id = self.get_consumed_number_id(number=number)
        # count all saved number instances (even non-unique)
        self.number_instance_id += 1

        return saved_number
    
    def execute_tree_action(self, action_type: TreeActionType, payload: Dict):
        if (action_type == TreeActionType.REGISTER_PREFIX_ITEM):
            return self.register_prefix_item()
        
        if (action_type == TreeActionType.REGISTER_UNIQUE_VALUE):
            if (payload is None) or (payload.get("value") is None):
                raise Exception(f"You must provide payload.value for this action (current payload={payload})")
            return self.register_unique_value(value=payload.get("value"))

        if (action_type == TreeActionType.REGISTER_REMAINING_VALUE):
            if (payload is None) or (payload.get("value") is None):
                raise Exception(f"You must provide payload.value for this action (current payload={payload})")
            return self.numbers.add_remaining_value(value=payload.get("value"))
        
        if (action_type == TreeActionType.UPDATE_VALUE_ID_COUNTS):
            if (payload is None) or (payload.get("id_counts") is None):
                raise Exception(f"You must provide payload.id_counts for this action (current payload={payload})")
            return self.id_counts.update(payload.get("id_counts"))
        
        if (action_type == TreeActionType.UPDATE_VALUE_ID_CODES):
            return self.update_id_codes()
        
        if (action_type == TreeActionType.CONSUME_REMAINING_VALUE):
            if (payload is None) or (payload.get("value") is None):
                raise Exception(f"You must provide payload.value for this action (current payload={payload})")
            return self.numbers.consume_value(value=payload.get("value"))
        
        if (action_type == TreeActionType.ADD_SORTED_VALUE):
            if (payload is None) or (payload.get("value") is None):
                raise Exception(f"You must provide payload.value for this action (current payload={payload})")
            return self.sorted_numbers.add(payload.get("value"))
        
        raise Exception(f"Incorrect action type: action_type={action_type}, payload={payload}")

    def add_pending_action(self, action: ValueTreeAction) -> None:
        self.pending_actions.append(action)
    
    def has_pending_actions(self) -> bool:
        return (len(self.pending_actions) > 0)
    
    def run_pending_actions(self) -> List[Tuple[TreeActionType, int]]:
        results = list()
        for action in self.pending_actions:
            result = self.execute_tree_action(action_type=action.type, payload=action.payload)
            results.append((action.type.name, result))
        self.pending_actions.clear()
        #return results
        return None
    
    def update_id_codes(self) -> List[NumberUpdate]:
        """
        Rebuild Huffman tree and update seed_id codes
        """
        # saving old tree metadata to detect diffs
        old_id_codes        = self.id_codes.copy()
        old_id_code_lengths = self.id_code_lengths.copy()
        old_ids             = set(old_id_codes.keys())
        # clearing old tree metadata
        self.id_codes.clear()
        self.code_ids.clear()
        self.id_code_lengths.clear()
        self.code_lengths.clear()
        self.code_length_counts.clear()
        self.id_codes_by_length.clear()
        self.ids_by_length.clear()
        # main update: rebuild Huffman tree
        self.id_codes = SortedDict(huffman_code(self.id_counts, endian=self.default_endian).items())
        
        # preparing update list
        updates = list()
        changes = list()
        # all other updates derived from new Huffman tree
        for number_id, number_id_code in self.id_codes.items():
            number_id_code                  = frozenbitarray(number_id_code)
            code_length                     = len(number_id_code)
            self.code_ids[number_id_code]   = number_id
            self.id_code_lengths[number_id] = code_length
            self.code_lengths.add(code_length)
            self.code_length_counts.update({ code_length : 1 })
            self.id_codes_by_length[code_length].add(number_id_code)
            self.ids_by_length[code_length].add(number_id)
            # detect changes
            changes = list()
            if (number_id not in old_ids):
                # new seed_id added
                changes.append(NumberChangeData(
                    change_type = NumberChangeType.NUMBER_ID,
                    old_value   = None,
                    new_value   = number_id,
                ))
                changes.append(NumberChangeData(
                    change_type = NumberChangeType.NUMBER_ID_CODE,
                    old_value   = None,
                    new_value   = number_id_code,
                ))
                changes.append(NumberChangeData(
                    change_type = NumberChangeType.ID_CODE_LENGTH,
                    old_value   = None,
                    new_value   = code_length,
                ))
            else:
                # existing seed_id updated
                old_id_code    = old_id_codes[number_id]
                if (old_id_code != number_id_code):
                    changes.append(NumberChangeData(
                        change_type = NumberChangeType.NUMBER_ID_CODE,
                        old_value   = frozenbitarray(old_id_code),
                        new_value   = number_id_code,
                    ))
                old_code_length = old_id_code_lengths[number_id]
                if (old_code_length != code_length):
                    changes.append(NumberChangeData(
                        change_type = NumberChangeType.ID_CODE_LENGTH,
                        old_value   = old_code_length,
                        new_value   = code_length,
                    ))
            # update seed_id if we have at least one change
            if (len(changes) > 0):
                updates.append(NumberUpdate(
                    number         = self.get_consumed_number(number_id=number_id),
                    number_id      = number_id,
                    number_id_code = number_id_code,
                    changes        = changes.copy(),
                ))
        # update zero element 
        self.zero_element_code   = self.get_zero_element_code()
        self.zero_element_length = len(self.get_zero_element_code())
        # return all performed updates
        return updates
    
    #def commit_changes(self) -> List[NumberUpdates]:
    #    if (self.has_pending_actions() is False):
    #        return []
    #    results = self.run_pending_actions()
    #    # update Huffman tree only when encoding completed: this is required 
    #    # to keep encoding and decoding trees identical on each step
    #    updates = self.update_id_codes()
    #    return updates
    
    ### CORE NUMBERS API ###
    
    def has_consumed_number(self, number: int) -> bool:
        return self.numbers.has_consumed_value(value=number)
    
    def has_consumed_number_id(self, number_id: int) -> bool:
        return self.numbers.has_consumed_value_id(value_id=number_id)
    
    def get_consumed_number(self, number_id: int) -> int:
        if (self.has_consumed_number_id(number_id=number_id) is False):
            raise Exception(f"consumed number_id={number_id} not found in this tree")
        return self.numbers.consumed_values[number_id]
    
    def get_consumed_numbers(self) -> Iterable[int]:
        return self.numbers.get_consumed_values()

    def get_consumed_number_id(self, number: int) -> int:
        if (self.has_consumed_number(number=number) is False):
            raise Exception(f"consumed number={number} not found in this tree")
        number_id = self.numbers.get_consumed_value_id(value=number)
        if (self.has_consumed_number_id(number_id=number_id) is False):
            raise Exception(f"cannot get consumed number_id for number={number} (number_id={number_id})")
        return number_id
    
    def has_remaining_number(self, number: int) -> bool:
        return self.numbers.has_remaining_value(value=number)
    
    def has_remaining_number_id(self, number_id: int) -> bool:
        return self.numbers.has_remaining_value_id(value_id=number_id)
    
    def get_remaining_number(self, number_id: int) -> int:
        if (self.has_remaining_number_id(number_id=number_id) is False):
            raise Exception(f"remaining number_id={number_id} not found in this tree")
        return self.numbers.remaining_values[number_id]
    
    def get_remaining_number_id(self, number: int) -> int:
        if (self.has_remaining_number(number=number) is False):
            raise Exception(f"remaining number={number} not found in this tree")
        number_id = self.numbers.get_remaining_value_id(value=number)
        if (self.has_remaining_number_id(number_id=number_id) is False):
            raise Exception(f"cannot get remaining number_id for number={number} (number_id={number_id})")
        return number_id
    
    def get_number_type(self, number: int, create_missing_numbers: bool=False) -> NumberType:
        if (self.is_empty_tree()):
            # empty tree does not require prefix symbol, and will always contain number_id from remaining list
            return NumberType.REMAINING
        if (self.has_consumed_number(number=number) is True):
            return NumberType.CONSUMED
        if (self.has_remaining_number(number=number) is True):
            return NumberType.REMAINING
        if (create_missing_numbers):
            # добавляем недостающий вариант значения в список доступных для выбора
            self.numbers.add_remaining_value(value=number)
            return NumberType.REMAINING
        raise Exception(f"Unknown type for seed={number}")
    
    def has_number_id_code(self, number_id: int) -> bool:
        return (number_id in self.id_codes)

    def get_number_id_code(self, number_id: int) -> frozenbitarray:
        if (self.has_consumed_number_id(number_id=number_id) is False):
            raise Exception(f"No consumed number_id={number_id}")
        if (self.has_number_id_code(number_id=number_id) is False):
            raise Exception(f"No Huffman code for number_id={number_id}")
        number_code = frozenbitarray(self.id_codes[number_id])
        return number_code
    
    def has_code_number_id(self, number_code: frozenbitarray) -> bool:
        number_code = frozenbitarray(number_code)
        return (number_code in self.code_ids)
    
    def get_code_number_id(self, number_code: bitarray) -> int:
        number_code = frozenbitarray(number_code)
        if (self.has_code_number_id(number_code=number_code) is False):
            raise Exception(f"No number_id for Huffman code: number_code={number_code}")
        number_id = self.code_ids[number_code]
        if (self.has_consumed_number_id(number_id=number_id) is False):
            raise Exception(f"No consumed number_id={number_id}")
        return number_id
    
    def get_zero_element_code(self) -> bitarray:
        return self.id_codes[self.zero_element_id]
    
    def get_next_code_length(self, number_id: int=None) -> int:
        if (number_id is None):
            number_id = self.numbers.get_next_consumed_value_id()
        next_id_counts = self.id_counts.copy()
        next_id_counts.update({ self.zero_element_id : 1 })
        next_id_counts.update({ number_id : 1 })
        next_codes = SortedDict(huffman_code(next_id_counts, endian=self.default_endian).items())
        return len(next_codes[number_id])
    
    def get_code_length(self, number_id: int) -> int:
        if (number_id in self.numbers.has_consumed_value_id(value_id=number_id)):
            id_code_length = self.id_code_lengths[number_id]
        else:
            id_code_length = self.get_next_code_length()
        return id_code_length
    
    def get_min_code_length(self) -> int:
        return min(self.code_lengths)
    
    def get_max_code_length(self) -> int:
        return max(self.code_lengths)
    
    def get_next_number_id_value(self) -> int:
        return self.numbers.get_next_consumed_value_id() + 1
    
    def get_number_prefix(self, number_type: NumberType) -> bitarray:
        if (self.is_empty_tree()):
            return bitarray('', endian=self.default_endian)
        if (number_type == NumberType.CONSUMED):
            return bitarray('', endian=self.default_endian)
        elif (number_type == NumberType.REMAINING):
            return self.get_zero_element_code()
        else:
            raise Exception(f"unknown number_id_type={number_type}")
    
    def get_prefix_length(self, number_type: NumberType) -> int:
        if (self.is_empty_tree()):
            # empty tree does never starts from a prefix element
            return 0
        if (number_type == NumberType.CONSUMED):
            return 0
        if (number_type == NumberType.REMAINING):
            return self.zero_element_length
        raise Exception(f"unknown number_type={number_type}")
    
    ### ENCODING/DECODING ###
    
    def encode_raw_number(self, number: int) -> bitarray:
        # we will never encode 0 as raw number, so we will shift all encoded values to 1
        if (number == 0):
            raise Exception(f"Cannot encode raw number=0")
        varint_number = number - 1
        return encode_varint_number(number=varint_number, prefix_length=self.varint_prefix_length, endian=self.default_endian)
    
    def encode_number_prefix(self, number_type: NumberType) -> bitarray:
        if (self.is_empty_tree()):
            # empty tree does not use a prefix
            return bitarray('', endian=self.default_endian)
        if (number_type == NumberType.CONSUMED):
            return bitarray('', endian=self.default_endian)
        if (number_type == NumberType.REMAINING):
            return self.get_zero_element_code()
        raise Exception(f"Unknown number_type={number_type}")

    def encode_number_id(self, number_id: int, number_type: NumberType) -> bitarray:
        if (number_type == NumberType.CONSUMED):
            return self.get_number_id_code(number_id=number_id)
        if (number_type == NumberType.REMAINING):
            return self.encode_raw_number_id(number_id=number_id)
        raise Exception(f"unknown number_id_type={number_type}")
    
    def encode_raw_number_id(self, number_id: int) -> bitarray:
        return self.encode_raw_number(number=number_id)
    
    def encode_number(self, number: int, create_missing_numbers: bool=False) -> bitarray:
        number_type = self.get_number_type(number=number, create_missing_numbers=create_missing_numbers)
        if (number_type == NumberType.CONSUMED):
            number_id = self.get_consumed_number_id(number=number)
        elif (number_type == NumberType.REMAINING):
            number_id = self.get_remaining_number_id(number=number)
        else:
            raise Exception(f"Unknown number_type={number_type}")
        # encode value parts
        encoded_prefix    = self.encode_number_prefix(number_type=number_type)
        encoded_number_id = self.encode_number_id(number_id=number_id, number_type=number_type)
        encoded_number    = encoded_prefix + encoded_number_id

        if (self.debug is True):
            # decode seed with same SeedTree state and check the result (for testing)
            decoded_number = self.decode_number(encoded_number=encoded_number)
            if (decoded_number != number):
                decoded_number_type     = self.decode_number_prefix(encoded_number=encoded_number)
                decoded_number_id       = self.decode_number_id(encoded_number=encoded_number, number_type=decoded_number_type)
                decoded_prefix_bits     = self.decode_number_prefix_bits(encoded_number=encoded_number)
                decoded_number_id_bits  = self.decode_number_id_bits(encoded_number=encoded_number, number_type=decoded_number_type)
                msg  = "ENCODED:\n"
                msg += f"number_id={number_id}, number_type={number_type.name}\n"
                msg += f"encoded_number_prefix={encoded_prefix}, encoded_number_id={encoded_number_id},\n"
                msg += f"number={number} ({encoded_number})\n"
                msg += "\nDECODED:\n"
                msg += f"decoded_number_prefix={decoded_prefix_bits}, decoded_number_id={decoded_number_id_bits},\n"
                msg += f"number_id={decoded_number_id}, number_type={decoded_number_type.name}\n"
                msg += f"number={decoded_number}\n"
                raise Exception(msg)
        return encoded_number
    
    def decode_number(self, encoded_number: bitarray) -> int:
        if (self.is_empty_tree()):
            # empty tree can only contain remaining number id as its first element
            number_type = NumberType.REMAINING
            number_id   = self.decode_raw_number_id(raw_number_id_bits=encoded_number)
        else:
            number_type = self.decode_number_prefix(encoded_number=encoded_number)
            number_id   = self.decode_number_id(encoded_number=encoded_number, number_type=number_type)
        if (number_type == NumberType.CONSUMED):
            number = self.get_consumed_number(number_id=number_id)
        if (number_type == NumberType.REMAINING):
            number = self.get_remaining_number(number_id=number_id)
        return number
    
    def decode_number_prefix_bits(self, encoded_number: bitarray) -> bitarray:
        return encoded_number[0:self.zero_element_length]

    def decode_number_prefix(self, encoded_number: bitarray) -> NumberType:
        if (self.is_empty_tree()):
            # empty tree can only contain remaining number id as its first element
            return NumberType.REMAINING
        prefix_bits = self.decode_number_prefix_bits(encoded_number=encoded_number)
        if (prefix_bits == self.zero_element_code):
            return NumberType.REMAINING
        else:
            return NumberType.CONSUMED
    
    def decode_number_id_bits(self, encoded_number: bitarray, number_type: NumberType) -> bitarray:
        prefix_length  = self.get_prefix_length(number_type=number_type)
        number_id_bits = encoded_number[prefix_length:len(encoded_number)]
        return number_id_bits

    def decode_number_id(self, encoded_number: bitarray, number_type: NumberType) -> int:
        if (self.is_empty_tree()):
            # empty tree can only contain remaining number id as its first element
            return self.decode_raw_number_id(raw_number_id_bits=encoded_number)
        number_id_bits = self.decode_number_id_bits(encoded_number=encoded_number, number_type=number_type)
        if (number_type == NumberType.CONSUMED):
            return self.decode_huffman_number_id(encoded_number_id_bits=number_id_bits)
        if (number_type == NumberType.REMAINING):
            return self.decode_raw_number_id(raw_number_id_bits=number_id_bits)
        raise Exception(f"unknown number_type={number_type}")
    
    def decode_huffman_number_id(self, encoded_number_id_bits: bitarray) -> int:
        for code_length in self.code_lengths:
            encoded_bits = encoded_number_id_bits[0:code_length]
            length_codes = self.id_codes_by_length[code_length]
            for number_code in length_codes:
                #print(f"l={code_length}, seed_code={seed_code}, bits={encoded_bits}")
                if (number_code == encoded_bits):
                    number_id = self.code_ids[number_code]
                    return number_id
        raise Exception(f"No Huffman code for encoded_number_id_bits={encoded_number_id_bits}")

    def decode_raw_number_id(self, raw_number_id_bits: bitarray) -> int:
        varint_number = decode_varint_number(encoded_number=raw_number_id_bits, prefix_length=self.varint_prefix_length)
        # we will never encode 0 as raw number, so we will shift all encoded values to 1
        varint_number = varint_number + 1
        if (varint_number == 0):
            raise Exception(f"Incorrect raw number decoded: decoded raw number must be greater than 0")
        return varint_number
    
    def encode_to_bitarray(self, numbers: List[int]) -> bitarray:
        encoded_bits = bitarray('', endian=self.default_endian)
        for number in numbers:
            saved_number = self.save_number(number=number)
            #number_codes.append(saved_number.encoded_number)
            encoded_bits += saved_number.encoded_number
        return encoded_bits
    
    def decode_from_bitarray(self, data: bitarray) -> List[SavedNumber]:
        saved_numbers  = list()
        remaining_data = data
        while True:
            decoded_number = self.decode_number(encoded_number=remaining_data)
            saved_number   = self.save_number(number=decoded_number)
            saved_numbers.append(saved_number)
            data_start     = saved_number.encoded_length
            data_end       = len(remaining_data)
            remaining_data = remaining_data[data_start:data_end]
            if (len(remaining_data) == 0):
                break
        return saved_numbers

#####################
### HASH IDS TREE ###
#####################

def init_batch_size_from_prefix_size(prefix_size: int) -> int:
    return (2**(2**prefix_size)) - 1

# initial seed "remaining_items" size based on SEED_VARINT_PREFIX_BITS
#SEED_INIT_BATCH_SIZE      = (2**(2**SEED_VARINT_PREFIX_BITS)) - 1
# initial nounce "remaining_items" size based on NOUNCE_VARINT_PREFIX_BITS
#NOUNCE_INIT_BATCH_SIZE    = (2**(2**NOUNCE_VARINT_PREFIX_BITS)) - 1

# составной id хеш-значения - состоит из 2 чисел: seed (номер хеш-пространства) и nounce (позиция в хеш-пространстве)
HashId = namedtuple("HashId", ['seed', 'nounce'])

class HashIdNumberType(str, Enum):
    SEED   : str = 'SEED'
    NOUNCE : str = 'NOUNCE'

class HashIdTreeType(str, Enum):
    SEED   : str = 'SEED'
    NOUNCE : str = 'NOUNCE'

class HashIdsTreeActionType(str, Enum):
    REGISTER_UNIQUE_HASH_ID    : str = 'REGISTER_UNIQUE_HASH_ID'
    REGISTER_UNIQUE_SEED       : str = 'REGISTER_UNIQUE_SEED'
    REGISTER_SEED_NOUNCES_TREE : str = 'REGISTER_SEED_NOUNCES_TREE'
    UPDATE_HASH_ID_COUNTS      : str = 'UPDATE_HASH_ID_COUNTS'
    UPDATE_SEEDS_TREE          : str = 'UPDATE_SEEDS_TREE'
    UPDATE_NOUNCES_TREE        : str = 'UPDATE_NOUNCES_TREE'
    LOAD_SEEDS_TREE            : str = 'LOAD_SEEDS_TREE'
    LOAD_NOUNCES_TREE          : str = 'LOAD_NOUNCES_TREE'

@dataclass
class HashIdsTreeAction:
    type    : HashIdsTreeActionType = field()
    seed    : int                   = field(default=None)
    nounce  : int                   = field(default=None)
    payload : Dict                  = field(default_factory=dict)

class HashIdChangeType(str, Enum):
    HASH_ID        : str = 'HASH_ID'
    HASH_ID_CODE   : str = 'HASH_ID_CODE'
    ID_CODE_LENGTH : str = 'ID_CODE_LENGTH'

@dataclass
class HashIdChangeData:
    change_type : HashIdChangeType                         = field()
    old_value   : Union[HashId, int, frozenbitarray, None] = field()
    new_value   : Union[HashId, int, frozenbitarray, None] = field()

@dataclass
class HashIdUpdate:
    hash_id        : HashId                 = field()
    is_new_hash_id : bool                   = field()
    changes        : List[HashIdChangeData] = field(default_factory=list)

@dataclass
class SavedHashId:
    id                 : HashId      = field(default=None, init=False)
    seed               : int         = field()
    nounce             : int         = field()
    hash_instance_id   : int         = field(default=None)
    seed_instance_id   : int         = field(default=None)
    nounce_instance_id : int         = field(default=None)
    seed_number        : SavedNumber = field(default=None, init=False)
    nounce_number      : SavedNumber = field(default=None, init=False)
    encoded_hash_id    : bitarray    = field(default=None)
    decoded_hash_id    : bitarray    = field(default=None)
    encoded_length     : int         = field(default=None)
    is_new_hash_id     : bool        = field(default=True)

    def __post_init__(self):
        self.id = HashId(seed=self.seed, nounce=self.nounce)
    
    def set_saved_seed(self, saved_seed: SavedNumber):
        self.seed_number = saved_seed
        if (saved_seed.is_new_number):
            self.is_new_hash_id = True
        elif (self.nounce_number is not None):
            if (self.nounce_number.is_new_number is False) and (saved_seed.is_new_number is False):
                self.is_new_hash_id = False
        else:
            self.is_new_hash_id = True
    
    def set_saved_nounce(self, saved_nounce: SavedNumber):
        self.nounce_number = saved_nounce
        if (saved_nounce.is_new_number):
            self.is_new_hash_id = True
        elif (self.seed_number is not None):
            if (self.nounce_number.is_new_number is False) and (self.seed_number.is_new_number is False):
                self.is_new_hash_id = False
        else:
            self.is_new_hash_id = True

@dataclass
class HashIdsTree:
    seeds_tree            : NumbersTree                = field(default_factory=NumbersTree)
    nounces_trees         : Dict[int, NumbersTree]     = field(default_factory=dict)
    hash_instance_id      : int                        = field(default=0)
    number_instance_id    : int                        = field(default=0)
    hash_id_counts        : Counter                    = field(default_factory=Counter)
    saved_hash_ids        : Dict[HashId, SavedHashId]  = field(default_factory=SortedDict, repr=False)
    hash_ids_by_length    : Dict[int, Set[HashId]]     = field(default_factory=lambda: defaultdict(SortedSet))
    hash_id_lengths       : Set[int]                   = field(default_factory=SortedSet)
    hash_id_length_counts : Counter                    = field(default_factory=Counter)
    current_hash_id       : HashId                     = field(default=None)
    updated_hash_ids      : Set[HashId]                = field(default_factory=SortedSet)
    hash_id_updates       : Dict[HashId, HashIdUpdate] = field(default_factory=SortedDict)
    seed_prefix_length    : int                        = field(default=SEED_VARINT_PREFIX_BITS)
    nounce_prefix_length  : int                        = field(default=NOUNCE_VARINT_PREFIX_BITS)
    default_endian        : str                        = field(default=DEFAULT_ENDIAN, init=False, repr=False)
    debug                 : bool                       = field(default=True)

    def __post_init__(self):
        self.seeds_tree = NumbersTree(
            init_batch_size      = init_batch_size_from_prefix_size(prefix_size=self.seed_prefix_length),
            varint_prefix_length = self.seed_prefix_length,
        )
    
    def has_seed(self, seed: int) -> bool:
        return self.seeds_tree.has_consumed_number(number=seed)
    
    def get_seed(self, seed_id: int) -> int:
        if (self.has_seed_id(seed_id=seed_id) is False):
            raise Exception(f"seed_id={seed_id} not registered")
        return self.seeds_tree.get_consumed_number(number_id=seed_id)
    
    def has_seed_id(self, seed_id: int) -> bool:
        return self.seeds_tree.has_consumed_number_id(number_id=seed_id)

    def get_seed_id(self, seed: int) -> int:
        if (self.has_seed(seed=seed) is False):
            raise Exception(f"seed={seed} not registered")
        return self.seeds_tree.get_consumed_number_id(number=seed)
    
    def has_seed_nounces_tree(self, seed: int) -> bool:
        return (seed in self.nounces_trees)
    
    def get_seed_nounces_tree(self, seed: int, init_missing: bool=True) -> NumbersTree:
        if (self.has_seed_nounces_tree(seed=seed) is False):
            if (init_missing is False):
                raise Exception(f"missing nounces_tree for seed={seed}")
            self.init_seed_nounces_tree(seed=seed)
        return self.nounces_trees[seed]
    
    def init_seed_nounces_tree(self, seed: int) -> NumbersTree:
        if (self.has_seed_nounces_tree(seed=seed) is False):
            self.nounces_trees[seed] = NumbersTree(
                init_batch_size      = init_batch_size_from_prefix_size(prefix_size=self.nounce_prefix_length),
                varint_prefix_length = self.nounce_prefix_length,
            )
        return self.nounces_trees[seed]
    
    def get_nounce_id(self, seed: int, nounce: int) -> int:
        if (self.has_seed_nounces_tree(seed=seed) is False):
            raise Exception(f"cannot get nounce_id: missing nounces_tree for seed={seed}")
        return self.nounces_trees[seed].get_consumed_number_id(number=nounce)
    
    def has_saved_hash_id(self, hash_id: HashId) -> bool:
        return (hash_id in self.saved_hash_ids)
    
    def is_new_hash_id(self, hash_id: HashId) -> bool:
        return (hash_id not in self.saved_hash_ids)
    
    def process_updated_hash_ids(self):
        """
        Must run before new hash_id is saved
        """
        old_hash_ids = self.saved_hash_ids.copy()
        # clear updates from previous step
        self.hash_id_updates.clear()
        
        #for hash_id in self.updated_hash_ids:
        for hash_id in SortedSet(self.saved_hash_ids.keys()): #self.updated_hash_ids:
            new_hash_id_code     = self.encode_hash_id(seed=hash_id.seed, nounce=hash_id.nounce).copy()
            new_decoded_hash_id  = self.decode_hash_id(new_hash_id_code)
            new_hash_id_length   = len(new_hash_id_code)
            if (hash_id != new_decoded_hash_id):
                raise Exception(f"Encoded and decoded hash_id didn't match: hash_id={hash_id}, new_encoded_hash_id={new_hash_id_code}, new_decoded_hash_id={new_decoded_hash_id}")
            #hash_id_update = HashIdUpdate(
            #    hash_id        = hash_id,
            #    is_new_hash_id = self.is_new_hash_id(hash_id=hash_id),
            #    changes        = [],
            #)
            if (hash_id not in self.hash_id_updates):
                self.hash_id_updates[hash_id] = HashIdUpdate(
                    hash_id        = hash_id,
                    is_new_hash_id = self.is_new_hash_id(hash_id=hash_id),
                    changes        = [],
                )
            if (self.hash_id_updates[hash_id].is_new_hash_id):
                # add new hash id
                if (len(self.hash_id_updates[hash_id].changes) == 0):
                    self.hash_id_updates[hash_id].changes.append(HashIdChangeData(
                        change_type = HashIdChangeType.HASH_ID_CODE,
                        old_value   = None,
                        new_value   = new_hash_id_code.copy(),
                    ))
                    self.hash_id_updates[hash_id].changes.append(HashIdChangeData(
                        change_type = HashIdChangeType.ID_CODE_LENGTH,
                        old_value   = None,
                        new_value   = new_hash_id_length,
                    ))
            else:
                # update existing hash_id
                if (len(self.hash_id_updates[hash_id].changes) == 0):
                    old_hash_id_code = old_hash_ids[hash_id].encoded_hash_id
                    if (old_hash_id_code != new_hash_id_code):
                        self.hash_id_updates[hash_id].changes.append(HashIdChangeData(
                            change_type = HashIdChangeType.HASH_ID_CODE,
                            old_value   = old_hash_id_code.copy(),
                            new_value   = new_hash_id_code.copy(),
                        ))
                    old_hash_id_length = old_hash_ids[hash_id].encoded_length
                    if (old_hash_id_length != new_hash_id_length):
                        self.hash_id_updates[hash_id].changes.append(HashIdChangeData(
                            change_type = HashIdChangeType.ID_CODE_LENGTH,
                            old_value   = old_hash_id_length,
                            new_value   = new_hash_id_length,
                        ))
            #self.hash_id_updates.append(hash_id_update)
        # clear processed hash_id list
        self.updated_hash_ids.clear()
    
    def process_hash_id_updates(self):
        """
        Must run after new hash_id is saved
        """
        for hash_id, update in self.hash_id_updates.items():
            if (hash_id == self.current_hash_id):
                continue
            new_hash_id_code     = self.encode_hash_id(seed=hash_id.seed, nounce=hash_id.nounce).copy()
            new_decoded_hash_id  = self.decode_hash_id(new_hash_id_code)
            new_hash_id_length   = len(new_hash_id_code)
            old_hash_id_code     = self.saved_hash_ids[update.hash_id].encoded_hash_id.copy()
            old_hash_id_length   = self.saved_hash_ids[update.hash_id].encoded_length
            
            assert(hash_id == new_decoded_hash_id)

            if (new_hash_id_code != old_hash_id_code):
                self.saved_hash_ids[update.hash_id].encoded_hash_id = new_hash_id_code
                self.saved_hash_ids[update.hash_id].decoded_hash_id = new_decoded_hash_id
            if (new_hash_id_length != old_hash_id_length):
                self.saved_hash_ids[update.hash_id].encoded_length = new_hash_id_length

        # TODO: incremental update instead of full rebuild
        self.hash_ids_by_length.clear()
        self.hash_id_length_counts.clear()
        for hash_id, saved_hash in self.saved_hash_ids.items():
            self.hash_ids_by_length[saved_hash.encoded_length].add(hash_id)
            self.hash_id_length_counts.update({ saved_hash.encoded_length: 1 })
        
    def save_hash_id(self, seed: int, nounce: int, update_seeds_tree: bool=True, update_nounces_tree: bool=True) -> SavedHashId:
        self.current_hash_id = HashId(seed=seed, nounce=nounce)
        encoded_hash_id  = self.encode_hash_id(seed=seed, nounce=nounce)
        decoded_hash_id  = self.decode_hash_id(encoded_hash_id=encoded_hash_id.copy())
        if (decoded_hash_id != self.current_hash_id):
            raise Exception(f"Encoded and decoded hash_id are not match: self.new_hash_id={self.current_hash_id}, encoded_hash_id={encoded_hash_id}, decoded_hash_id={decoded_hash_id}")
        
        saved_hash_id = SavedHashId(
            seed             = seed,
            nounce           = nounce,
            hash_instance_id = self.hash_instance_id,
            encoded_hash_id  = encoded_hash_id.copy(),
            decoded_hash_id  = decoded_hash_id,
            encoded_length   = len(encoded_hash_id),
        )
        self.hash_instance_id += 1

        # updating seeds
        saved_seed                     = self.save_seed(seed=seed, update_tree=update_seeds_tree)
        saved_hash_id.set_saved_seed(saved_seed=saved_seed)
        saved_hash_id.seed_instance_id = self.number_instance_id
        self.number_instance_id += 1
        
        # updating_nounces 
        saved_nounce                     = self.save_nounce(seed=seed, nounce=nounce, update_tree=update_nounces_tree)
        saved_hash_id.set_saved_nounce(saved_nounce=saved_nounce)
        saved_hash_id.nounce_instance_id = self.number_instance_id
        self.number_instance_id += 1

        #self.updated_hash_ids.add(self.current_hash_id)
        # processing tree updates, generating hash_id changes
        #print(saved_hash_id.id, saved_hash_id.is_new_hash_id, self.updated_hash_ids)
        self.process_updated_hash_ids()
        
        #if (self.is_new_hash_id(hash_id=self.current_hash_id)):
            # saving new hash_id data
        self.saved_hash_ids[saved_hash_id.id] = saved_hash_id
        
        # processing hash_id changes
        #print(self.hash_id_counts, self.hash_ids_by_length)
        self.process_hash_id_updates()
        #print(saved_hash_id.id, saved_hash_id.is_new_hash_id, self.hash_id_updates)
        #print(f"\n--------\n")
        
        return saved_hash_id

    def collect_updated_hash_ids_from_saved_seed(self, saved_seed: SavedNumber):
        for seed_update in saved_seed.tree_updates:
            if (len(seed_update.changes) > 0):
                if (seed_update.number == 0):
                    continue
                seed    = seed_update.number
                nounces = self.nounces_trees[seed].get_consumed_numbers()
                for nounce in nounces:
                    if (nounce == 0):
                        continue
                    self.updated_hash_ids.add(HashId(seed=seed, nounce=nounce))
    
    def save_seed(self, seed: int, update_tree: bool=True) -> SavedNumber:
        if (self.has_seed_nounces_tree(seed=seed) is False):
            self.init_seed_nounces_tree(seed=seed)
        saved_seed = self.seeds_tree.save_number(number=seed, update_tree=update_tree)
        self.collect_updated_hash_ids_from_saved_seed(saved_seed=saved_seed)
        return saved_seed

    def collect_updated_hash_ids_from_saved_nounce(self, seed: int, saved_nounce: SavedNumber):
        for nounce_update in saved_nounce.tree_updates:
            if (len(nounce_update.changes) > 0):
                if (nounce_update.number == 0):
                    continue
                nounce = nounce_update.number
                self.updated_hash_ids.add(HashId(seed=seed, nounce=nounce))
    
    def save_nounce(self, seed: int, nounce: int, update_tree: bool=True) -> SavedNumber:
        if (self.has_seed_nounces_tree(seed=seed) is False):
            self.init_seed_nounces_tree(seed=seed)
        saved_nounce = self.nounces_trees[seed].save_number(number=nounce, update_tree=update_tree)
        self.collect_updated_hash_ids_from_saved_nounce(seed=seed, saved_nounce=saved_nounce)
        return saved_nounce

    def execute_tree_action(self, action_type: HashIdsTreeAction, payload: Dict):
        if (action_type == HashIdsTreeActionType.UPDATE_SEEDS_TREE):
            results = self.seeds_tree.run_pending_actions()
            updates = self.seeds_tree.update_id_codes()
            return (results, updates)
        
        if (action_type == HashIdsTreeActionType.UPDATE_NOUNCES_TREE):
            if (payload is None) or (payload.get("hash_id") is None):
                raise Exception(f"You must provide payload.hash_id for this action (current payload={payload})")
            seed    = payload.get("hash_id").seed
            results = self.nounces_trees[seed].run_pending_actions()
            updates = self.nounces_trees[seed].update_id_codes()
            return (results, updates)
        
        if (action_type == HashIdsTreeActionType.UPDATE_HASH_ID_COUNTS):
            if (payload is None) or (payload.get("id_counts") is None):
                raise Exception(f"You must provide payload.hash_id_counts for this action (current payload={payload})")
            return self.hash_id_counts.update(payload.get("hash_id_counts"))
        
        raise Exception(f"Incorrect action type: action_type={action_type}, payload={payload}")

    def add_pending_action(self, action: HashIdsTreeAction) -> None:
        self.pending_actions.append(action)
    
    def has_pending_actions(self) -> bool:
        return (len(self.pending_actions) > 0)
    
    def run_pending_actions(self) -> List[Tuple[HashIdsTreeAction, int]]:
        results = list()
        for action in self.pending_actions:
            result = self.execute_tree_action(action_type=action.type, payload=action.payload)
            results.append((action.type.name, result))
        self.pending_actions.clear()
        return results
    
    def encode_seed(self, seed: int) -> bitarray:
        return self.seeds_tree.encode_number(number=seed)

    def encode_nounce(self, seed: int, nounce: int) -> bitarray:
        if (self.has_seed_nounces_tree(seed=seed) is False):
            self.init_seed_nounces_tree(seed=seed)
        return self.nounces_trees[seed].encode_number(number=nounce)

    def encode_hash_id(self, seed: int, nounce: int) -> bitarray:
        encoded_seed    = self.encode_seed(seed=seed)
        encoded_nounce  = self.encode_nounce(seed=seed, nounce=nounce)
        encoded_hash_id = encoded_seed + encoded_nounce
        return encoded_hash_id

    def decode_hash_id(self, encoded_hash_id: bitarray) -> HashId:
        seed                = self.decode_seed(encoded_seed=encoded_hash_id.copy())
        encoded_seed        = self.encode_seed(seed=seed)
        encoded_seed_length = len(encoded_seed)
        encoded_seed_bits   = encoded_hash_id[0:encoded_seed_length]
        if (encoded_seed_bits != encoded_seed):
            raise Exception(f"Encoded and decoded seed values are different: encoded_seed={encoded_seed}, encoded_seed_bits={encoded_seed_bits}, encoded_hash_id={encoded_hash_id}")
        
        remaining_bits        = encoded_hash_id[encoded_seed_length:]
        nounce                = self.decode_nounce(seed=seed, encoded_nounce=remaining_bits)
        encoded_nounce        = self.encode_nounce(seed=seed, nounce=nounce)
        encoded_nounce_length = len(encoded_nounce)
        encoded_nounce_bits   = encoded_hash_id[encoded_seed_length:encoded_seed_length+encoded_nounce_length]
        if (encoded_nounce_bits != encoded_nounce):
            raise Exception(f"Encoded and decoded nounce values are different: encoded_seed={encoded_seed}, encoded_nounce={encoded_nounce}, encoded_nounce_bits={encoded_nounce_bits}, encoded_hash_id={encoded_hash_id}")
        
        return HashId(seed=seed, nounce=nounce)

    def decode_seed(self, encoded_seed: bitarray) -> int:
        return self.seeds_tree.decode_number(encoded_number=encoded_seed)

    def decode_nounce(self, seed: int, encoded_nounce: bitarray) -> int:
        if (self.has_seed_nounces_tree(seed=seed) is False):
            self.init_seed_nounces_tree(seed=seed)
        return self.nounces_trees[seed].decode_number(encoded_number=encoded_nounce)
    
    def encode_to_bitarray(self, hash_ids: List[HashId]) -> bitarray:
        encoded_bits = bitarray('', endian=self.default_endian)
        item_id      = 0
        for hash_id in hash_ids:
            if (self.debug is True):
                encoded_hash_id = self.encode_hash_id(seed=hash_id.seed, nounce=hash_id.nounce)
                decoded_hash_id = self.decode_hash_id(encoded_hash_id=encoded_hash_id)
                print(f"{item_id}: {decoded_hash_id}, code={encoded_hash_id.to01()}, l={len(encoded_hash_id)}")
            saved_hash_id = self.save_hash_id(seed=hash_id.seed, nounce=hash_id.nounce)
            encoded_bits += saved_hash_id.encoded_hash_id.copy()
            item_id      += 1
        return encoded_bits
    
    def decode_from_bitarray(self, data: bitarray) -> List[SavedNumber]:
        saved_hash_ids = list()
        remaining_data = data
        item_id        = 0
        while True:
            decoded_hash_id = self.decode_hash_id(encoded_hash_id=remaining_data)
            if (self.debug is True):
                encoded_hash_id = self.encode_hash_id(seed=decoded_hash_id.seed, nounce=decoded_hash_id.nounce)
                print(f"{item_id}: {decoded_hash_id}, code={encoded_hash_id.to01()}, l={len(encoded_hash_id)}")
            saved_hash_id   = self.save_hash_id(seed=decoded_hash_id.seed, nounce=decoded_hash_id.nounce)
            saved_hash_ids.append(deepcopy(saved_hash_id))
            data_start      = saved_hash_id.encoded_length
            data_end        = len(remaining_data)
            remaining_data  = remaining_data[data_start:data_end]
            item_id += 1
            if (len(remaining_data) == 0):
                break
        return saved_hash_ids

#######################
### DATA ITEMS TREE ###
#######################

HashRangeId = namedtuple("HashRangeId", ["type", "seeds", "nounces"])

def ba2tuple(data: bitarray, signed: bool=False) -> Tuple[int, int]:
    return (len(data), ba2int(data, signed=signed))

def tuple2ba(data: Tuple[int, int], endian: str=DEFAULT_ENDIAN, signed: bool=False) -> bitarray:
    bit_length = data[0]
    value      = data[1]
    return int2ba(value, length=bit_length, endian=endian, signed=signed)

class SeedIdType(int, Enum):
    CONSUMED  : int = 0
    REMAINING : int = 1

class HashIdType(str, Enum):
    # saved hash id (seed/nounce pair)
    OLD : str = 'OLD'
    # new hash id (seed/nounce pair)
    NEW : str = 'NEW'

@dataclass
class HashIdRange:
    id               : HashRangeId       = field(default=None, init=False)
    seed_id_type     : SeedIdType        = field()
    seed_ids         : Iterable[int]     = field()
    nounce_ids       : Iterable[int]     = field()
    seed_id_length   : int               = field()
    nounce_id_length : int               = field()
    default_score    : int               = field(default=1)
    hash_id_length   : int               = field(default=None, init=False)
    data_item_length : int               = field(default=None, init=False)

    def __post_init__(self):
        self.hash_id_length   = self.seed_id_length + self.nounce_id_length
        self.data_item_length = self.hash_id_length + self.default_score
        if (self.seed_id_type == SeedIdType.CONSUMED):
            seeds = (self.seed_ids[0])
        elif (self.seed_id_type == SeedIdType.REMAINING):
            #seeds = (min(self.seed_ids), max(self.seed_ids))
            seeds = (self.seed_ids.start, self.seed_ids.stop)
        else:
            raise Exception(f"Incorrect seed_id type: {self.seed_id_type}")
        nounces = (self.nounce_ids.start, self.nounce_ids.stop)
        self.id = HashRangeId(type=self.seed_id_type.value, seeds=seeds, nounces=nounces)
    
    def get_seed(self, seeds_tree: NumbersTree, seed_id: int) -> int:
        if (self.seed_id_type == SeedIdType.CONSUMED):
            return seeds_tree.get_consumed_number(number_id=seed_id)
        if (self.seed_id_type == SeedIdType.REMAINING):
            return seeds_tree.get_remaining_number(number_id=seed_id)
        raise Exception(f"Incorrect seed_id type: {self.seed_id_type}")

    def get_nounce(self, nounce_id: int, nounces_tree: NumbersTree=None) -> int:
        if (self.seed_id_type == SeedIdType.REMAINING):
            # для новых seed-значений первые значений nounce всегда совпадают с id, их не нужно преобразовывать 
            return nounce_id
        if (self.seed_id_type == SeedIdType.CONSUMED):
            if (nounces_tree is None):
                raise Exception(f"nounces_tree not set (nounce_id={nounce_id})")
            return nounces_tree.get_remaining_number(number_id=nounce_id)
        raise Exception(f"Incorrect seed_id type: {self.seed_id_type}")

@dataclass
class EncodedDataItem:
    hash_id          : HashId         = field()
    hash_id_type     : HashIdType     = field()
    hash_item_code   : frozenbitarray = field()
    data_item_code   : frozenbitarray = field()
    hash_digest      : int            = field()
    hash_id_length   : int            = field(default=None, init=False)
    data_item_length : int            = field(default=None, init=False)
    score            : int            = field(default=None, init=False)

    def __post_init__(self):
        self.hash_id_length   = len(self.hash_item_code)
        self.data_item_length = len(self.data_item_code)
        self.score            = self.data_item_length - self.hash_id_length

# TODO: minimum default scan offset position
@dataclass
class ScannedHashItem:
    hash_id            : HashId          = field()
    data_item_code     : frozenbitarray  = field()
    data_item_code_id  : Tuple[int, int] = field()
    offset             : int             = field()
    hash_digest        : int             = field()
    target_item_length : int             = field()
    is_target_item     : bool            = field(default=False)
    data_item_length   : int             = field(default=None)

    def __post_init__(self):
        if (self.data_item_code is not None):
            self.data_item_length = len(self.data_item_code)
        elif (self.data_item_code_id is not None):
            self.data_item_length = self.data_item_code_id[0]
    
    def set_data_item_code(self, data_item_code: bitarray):
        self.data_item_code   = data_item_code
        self.data_item_length = len(self.data_item_code)
    
@dataclass
class InitHashItem:
    hash_id          : HashId         = field()
    hash_id_code     : frozenbitarray = field()
    data_item_code   : frozenbitarray = field()
    #offset           : int            = field()
    hash_digest      : int            = field()
    hash_id_length   : int            = field(default=None, init=False)
    data_item_length : int            = field(default=None, init=False)
    score            : int            = field(default=None, init=False)

    def __post_init__(self):
        self.hash_id_length   = len(self.hash_id_code)
        self.data_item_length = len(self.data_item_code)
        self.score            = self.data_item_length - self.hash_id_length

@dataclass
class SavedHashItem:
    hash_id          : HashId         = field()
    hash_id_code     : frozenbitarray = field()
    data_item_code   : frozenbitarray = field()
    hash_digest      : int            = field()
    hash_id_type     : HashIdType     = field(default=HashIdType.OLD)
    total_score      : int            = field(default=0)
    total_instances  : int            = field(default=0)
    hash_id_length   : int            = field(default=None, init=False)
    data_item_length : int            = field(default=None, init=False)
    score            : int            = field(default=None, init=False)

    def __post_init__(self):
        self.hash_id_length   = len(self.hash_id_code)
        self.data_item_length = len(self.data_item_code)
        self.score            = self.data_item_length - self.hash_id_length
    
    def update_hash_id_code(self, hash_id_code: Union[frozenbitarray, bitarray]):
        if (self.hash_id_code == hash_id_code) and (self.score >= DEFAULT_HASH_ITEM_SCORE):
            return
        self.hash_id_code     = None
        self.hash_id_code     = hash_id_code.copy()
        self.hash_id_length   = len(self.hash_id_code)
        self.data_item_length = self.hash_id_length + DEFAULT_HASH_ITEM_SCORE
        if (self.data_item_length > MAX_DATA_ITEM_CODE_LENGTH):
            raise Exception(f"{self.hash_id}: too long data_item_length={self.data_item_length}, hash_id_code={self.hash_id_code}, hash_id_length={self.hash_id_length}")
        self.data_item_code   = None
        self.data_item_code   = last_fba_bits_from_digest(digest=self.hash_digest, bit_length=self.data_item_length, endian=DEFAULT_ENDIAN, signed=False).copy()
        self.score            = self.data_item_length - self.hash_id_length
        
@dataclass
class DataLayerPrefix:
    pass

@dataclass
class DataItemsTree:
    layer_id                       : int                                = field(default=0)
    hash_ids_tree                  : HashIdsTree                        = field(default_factory=HashIdsTree)
    sorted_hash_ids                : Set[HashId]                        = field(default_factory=SortedSet)
    saved_data_items               : List[EncodedDataItem]              = field(default_factory=list)
    data_item_codes_by_length      : Dict[int, Set[frozenbitarray]]     = field(default_factory=lambda: defaultdict(SortedSet))
    data_item_code_ids_by_length   : Dict[int, Set[Tuple[int, int]]]    = field(default_factory=lambda: defaultdict(SortedSet))
    # все уникальные фрагменты данных полученные при поиске текущего элемента во время сканирования
    # (обнуляется при поиске каждого следующего элемента, сгруппированы по длине)
    scanned_item_codes_by_length   : Dict[int, Set[frozenbitarray]]     = field(default_factory=lambda: defaultdict(SortedSet))
    data_item_code_lengths         : Set[int]                           = field(default_factory=SortedSet)
    #scanned_item_code_lengths      : Set[int]                           = field(default_factory=SortedSet)
    # mapping соответствия фрагментов данных и id хешей для их генерации
    data_code_hash_ids             : Dict[frozenbitarray, HashId]       = field(default_factory=SortedDict)
    hash_id_data_codes             : Dict[HashId, frozenbitarray]       = field(default_factory=SortedDict)
    hash_id_digests                : Dict[HashId, int]                  = field(default_factory=SortedDict)
    hash_id_init_items             : Dict[HashId, InitHashItem]         = field(default_factory=SortedDict)
    saved_hash_items               : Dict[HashId, SavedHashItem]        = field(default_factory=SortedDict)
    # диапазоны поиска значений, сгруппированные по длине элемента данных
    hash_id_ranges_by_data_length  : Dict[int, List[HashIdRange]]       = field(default_factory=SortedDict)
    # последний фрагмент данных хранится отдельно (чтобы было проще кодировать последний элемент)
    # если данных для кодирования остаётся меньше чем 32 бита - то они все записываются в data_tail
    data_tail                      : bitarray                           = field(default=None)
    data_tail_length               : int                                = field(default=0)
    # количество бит на которое адрес должен быть короче значения
    default_score                  : int                                = field(default=1)
    minimal_score                  : int                                = field(default=1)
    max_target_item_length         : int                                = field(default=DEFAULT_TARGET_LENGTH)
    max_hash_item_length           : int                                = field(default=31)
    max_data_item_length           : int                                = field(default=MAX_DATA_ITEM_CODE_LENGTH)
    seed_prefix_length             : int                                = field(default=SEED_VARINT_PREFIX_BITS)
    nounce_prefix_length           : int                                = field(default=NOUNCE_VARINT_PREFIX_BITS)
    encoded_bitarray               : bitarray                           = field(default=None, init=False)
    default_endian                 : str                                = field(default=DEFAULT_ENDIAN, init=False, repr=False)
    debug                          : bool                               = field(default=True)

    def __post_init__(self):
        self.hash_ids_tree = HashIdsTree(
            seed_prefix_length   = self.seed_prefix_length,
            nounce_prefix_length = self.nounce_prefix_length,
        )
        self.encoded_bitarray = bitarray('', endian=self.default_endian)
    
    def create_seed_id_ranges(self) -> Dict[int, VarintNumberRange]:
        return create_number_ranges_for_prefix_length(prefix_length=self.seed_prefix_length, offset=DEFAULT_NUMBER_ID_OFFSET)
    
    def create_nounce_id_ranges(self) -> Dict[int, VarintNumberRange]:
        return create_number_ranges_for_prefix_length(prefix_length=self.nounce_prefix_length, offset=DEFAULT_NUMBER_ID_OFFSET)
    
    def collect_consumed_seed_ranges(self, seed_ranges: Dict[int, Dict[HashRangeId, HashIdRange]]) -> Dict[int, Dict[HashRangeId, HashIdRange]]:
        for seed_id_length in self.hash_ids_tree.seeds_tree.code_lengths:
            seeds_ids = self.hash_ids_tree.seeds_tree.ids_by_length[seed_id_length]
            for seed_id in seeds_ids:
                if (seed_id == 0):
                    continue
                seed                    = self.hash_ids_tree.get_seed(seed_id=seed_id)
                nounce_id_prefix_length = self.hash_ids_tree.get_seed_nounces_tree(seed=seed).zero_element_length
                for range_id_length, nounce_range in self.create_nounce_id_ranges().items():
                    nounce_id_length = range_id_length + nounce_id_prefix_length
                    seed_range = HashIdRange(
                        seed_id_type     = SeedIdType.CONSUMED,
                        seed_ids         = [seed_id],
                        nounce_ids       = range(nounce_range.start, nounce_range.end),
                        seed_id_length   = seed_id_length,
                        nounce_id_length = nounce_id_length,
                    )
                    seed_ranges[seed_range.data_item_length][seed_range.id] = seed_range
        return seed_ranges

    def collect_remaining_seed_ranges(self, seed_ranges: Dict[int, Dict[HashRangeId, HashIdRange]]) ->Dict[int, Dict[HashRangeId, HashIdRange]]:
        if (self.hash_ids_tree.seeds_tree.is_empty_tree()):
            # new seed tree will not require a prefix (works only once)
            seed_id_prefix_length = 0
        else:
            seed_id_prefix_length = self.hash_ids_tree.seeds_tree.zero_element_length
        # new nounce tree will not require a prefix (works for every new seed)
        nounce_id_prefix_length = 0
        for seed_range_id_length, seed_id_range in self.create_seed_id_ranges().items():
            seed_id_length = seed_id_prefix_length + seed_range_id_length
            for nounce_range_id_length, nounce_range in self.create_nounce_id_ranges().items():
                nounce_id_length = nounce_id_prefix_length + nounce_range_id_length
                seed_range = HashIdRange(
                    seed_id_type     = SeedIdType.REMAINING,
                    seed_ids         = range(seed_id_range.start, seed_id_range.end),
                    nounce_ids       = range(nounce_range.start, nounce_range.end),
                    seed_id_length   = seed_id_length,
                    nounce_id_length = nounce_id_length,
                )
                if (seed_range.data_item_length > self.max_data_item_length):
                    continue
                seed_ranges[seed_range.data_item_length][seed_range.id] = seed_range
        return seed_ranges
    
    def collect_scan_ranges(self) -> Dict[int, Dict[HashRangeId, HashIdRange]]:
        scan_ranges = defaultdict(SortedDict)
        scan_ranges = self.collect_consumed_seed_ranges(seed_ranges=scan_ranges)
        scan_ranges = self.collect_remaining_seed_ranges(seed_ranges=scan_ranges)
        return scan_ranges
    
    def has_data_item_code(self, data_item: frozenbitarray, data_item_length: int) -> bool:
        return (data_item in self.data_item_codes_by_length[data_item_length])
    
    def has_data_item_code_id(self, data_item_code_id: Tuple[int, int]) -> bool:
        data_item_code_length = data_item_code_id[0]
        return (data_item_code_id in self.data_item_code_ids_by_length[data_item_code_length])
    
    def has_scanned_data_item_code(self, data_item: frozenbitarray, data_item_length: int) -> bool:
        return (data_item in self.scanned_item_codes_by_length[data_item_length])
    
    def scan_hash_item(self, hash_id: HashId, bit_length: int, prev_lengths: Set[int], target_item_id: Tuple[int, int]=None) -> ScannedHashItem:
        offset = 0
        while True:
            seed          = hash_id.seed + offset
            hash_digest   = int_from_nounce(nounce=hash_id.nounce, seed=seed)
            #data_item     = last_fba_bits_from_digest(digest=hash_digest, bit_length=bit_length, endian=self.default_endian, signed=False)
            has_prev_item = False
            for prev_length in prev_lengths:
                #prev_item = data_item[0:prev_length]
                prev_item    = (prev_length, last_int_bits_from_digest(digest=hash_digest, bit_length=prev_length))
                #prev_item_id = (prev_length, prev_item)
                if (self.has_data_item_code_id(data_item_code_id=prev_item)):
                    has_prev_item = True
                    break
                if (self.has_scanned_data_item_code(data_item=prev_item, data_item_length=prev_length)):
                    has_prev_item = True
                    break
            if (has_prev_item is False):
                break
            offset += 1
        
        # init hash_item data
        is_target_item    = False
        data_item_code    = None
        data_item_code_id = (bit_length, last_int_bits_from_digest(digest=hash_digest, bit_length=bit_length))
        if (target_item_id is not None):
            if (target_item_id == data_item_code_id):
                is_target_item = True
                data_item_code = last_fba_bits_from_digest(digest=hash_digest, bit_length=bit_length, endian=self.default_endian, signed=False)
        
        return ScannedHashItem(
            hash_id            = hash_id,
            is_target_item     = is_target_item,
            data_item_code     = data_item_code,
            data_item_code_id  = data_item_code_id,
            offset             = offset,
            hash_digest        = hash_digest,
            target_item_length = bit_length,
        )
    
    def update_data_item_code_ids_by_length(self):
        self.data_item_code_ids_by_length.clear()
        for code_length, codes in self.data_item_codes_by_length.items():
            for code in codes:
                self.data_item_code_ids_by_length[code_length].add(ba2tuple(data=code))
    
    def find_data_item(self, target_data_item: frozenbitarray, clear_codes: bool=True) -> ScannedHashItem:
        if (clear_codes):
            self.scanned_item_codes_by_length.clear()
        self.update_data_item_code_ids_by_length()
        scan_ranges       = self.collect_scan_ranges()
        data_item_lengths = SortedSet(scan_ranges.keys())
        seeds_tree        = self.hash_ids_tree.seeds_tree
        prev_lengths      = SortedSet()

        for data_item_length in data_item_lengths.copy():
            length_ranges = scan_ranges[data_item_length]
            target_length = data_item_length
            if (target_length > self.max_target_item_length):
                target_length = self.max_target_item_length
            target_item = target_data_item[0:target_length]
            prev_lengths.clear()
            for prev_length in data_item_lengths.copy():
                prev_lengths.add(prev_length)
                if (prev_length >= target_length):
                    break
            #print(f"target_item={target_item}, dl={data_item_length}, tl={target_length}, ranges: {len(length_ranges)}")
            for _range_id, length_range in length_ranges.items():
                for seed_id in length_range.seed_ids:
                    seed         = length_range.get_seed(seeds_tree=seeds_tree, seed_id=seed_id)
                    nounces_tree = None
                    if (length_range.seed_id_type) == SeedIdType.CONSUMED:
                        nounces_tree = self.hash_ids_tree.get_seed_nounces_tree(seed=seed)
                    for nounce_id in length_range.nounce_ids:
                        if (length_range.seed_id_type == SeedIdType.REMAINING):
                            #nounce = length_range.get_nounce(nounce_id=nounce_id)
                            nounce = nounce_id
                        elif (length_range.seed_id_type == SeedIdType.CONSUMED):
                            nounce = length_range.get_nounce(nounce_id=nounce_id, nounces_tree=nounces_tree)
                        else:
                            raise Exception(f"Incorrect seed_id type: {length_range.seed_id_type.seed_id_type}")
                        # scan one hash_id
                        hash_id      = HashId(seed=seed, nounce=nounce)
                        scanned_item = self.scan_hash_item(hash_id=hash_id, bit_length=target_length, prev_lengths=prev_lengths, target_item_id=ba2tuple(data=target_item))
                        #scanned_item = self.scan_hash_item(hash_id=hash_id, bit_length=data_item_length, prev_lengths=prev_lengths)
                        # check result: is returned data item match target
                        #assert(len(target_item) == target_length)
                        if (scanned_item.is_target_item):
                            target_code = last_fba_bits_from_digest(digest=scanned_item.hash_digest, bit_length=target_length, endian=self.default_endian, signed=False)
                            if (target_code != target_item):
                                raise Exception(f"target_code={target_code} (e={target_code.endian()}) should be equal to target_item={target_item} (e={target_item.endian()}), target_item={target_item}, scanned_item={scanned_item}")
                            scanned_item.set_data_item_code(data_item_code=target_code)
                            #self.scanned_item_code_lengths = SortedSet(self.scanned_item_codes_by_length.keys())
                            return scanned_item
                        else:
                            #self.scanned_item_codes_by_length[target_length].add(scanned_item.data_item_code)
                            #self.scanned_item_codes_by_length[target_length].add(ba2tuple(scanned_item.data_item_code))
                            #self.scanned_item_codes_by_length[target_length].add(tuple2ba(scanned_item.data_item_code_id, endian=self.default_endian, signed=False))
                            self.scanned_item_codes_by_length[target_length].add(scanned_item.data_item_code_id)
        # search failed
        raise Exception(f"target_data_item={target_data_item} not found: maybe search area too small - try to use more bits for self.seed_prefix_length and self.nounce_prefix_length")
    
    def restore_scanned_item(self, target_hash_id: HashId, clear_codes: bool=True) -> ScannedHashItem:
        if (clear_codes):
            self.scanned_item_codes_by_length.clear()
        self.update_data_item_code_ids_by_length()
        scan_ranges          = self.collect_scan_ranges()
        data_item_lengths    = SortedSet(list(scan_ranges.keys()))
        seeds_tree           = self.hash_ids_tree.seeds_tree
        prev_lengths         = SortedSet()

        for data_item_length in data_item_lengths.copy():
            length_ranges = scan_ranges[data_item_length]
            target_length = data_item_length
            if (target_length > self.max_target_item_length):
                target_length = self.max_target_item_length
            prev_lengths.clear()
            for prev_length in data_item_lengths.copy():
                prev_lengths.add(prev_length)
                if (prev_length >= target_length):
                    break
            #print(f"target_hash_id={target_hash_id}, dl={data_item_length}, tl={target_length}, ranges: {len(length_ranges)}")
            for _range_id, length_range in length_ranges.items():
                for seed_id in length_range.seed_ids:
                    seed         = length_range.get_seed(seeds_tree=seeds_tree, seed_id=seed_id)
                    nounces_tree = None
                    if (length_range.seed_id_type) == SeedIdType.CONSUMED:
                        nounces_tree = self.hash_ids_tree.get_seed_nounces_tree(seed=seed)
                    for nounce_id in length_range.nounce_ids:
                        if (length_range.seed_id_type == SeedIdType.REMAINING):
                            #nounce = length_range.get_nounce(nounce_id=nounce_id)
                            nounce = nounce_id
                        elif (length_range.seed_id_type == SeedIdType.CONSUMED):
                            nounce = length_range.get_nounce(nounce_id=nounce_id, nounces_tree=nounces_tree)
                        else:
                            raise Exception(f"Incorrect seed_id type: {length_range.seed_id_type.seed_id_type}")
                        hash_id      = HashId(seed=seed, nounce=nounce)
                        scanned_item = self.scan_hash_item(hash_id=hash_id, bit_length=target_length, prev_lengths=prev_lengths)
                        #scanned_item = self.scan_hash_item(hash_id=hash_id, bit_length=data_item_length, prev_lengths=prev_lengths)
                        # check result: current hash_id must be equal to hash_id when data encoder stopped
                        #assert(len(scanned_item.data_item_code) == target_length)
                        if (hash_id == target_hash_id):
                            target_code = last_fba_bits_from_digest(digest=scanned_item.hash_digest, bit_length=target_length, endian=self.default_endian, signed=False)
                            scanned_item.set_data_item_code(data_item_code=target_code)
                            #self.scanned_item_code_lengths = SortedSet(self.scanned_item_codes_by_length.keys())
                            return scanned_item
                        else:
                            #self.scanned_item_codes_by_length[target_length].add(scanned_item.data_item_code_id)
                            #self.scanned_item_codes_by_length[target_length].add(tuple2ba(scanned_item.data_item_code_id, endian=self.default_endian, signed=False))
                            #self.scanned_item_codes_by_length[target_length].add(scanned_item.data_item_code)
                            #self.scanned_item_codes_by_length[scanned_item.data_item_length].add(scanned_item.data_item_code)
                            self.scanned_item_codes_by_length[target_length].add(scanned_item.data_item_code_id)
        # search failed
        raise Exception(f"Unable to restore target_hash_id={target_hash_id}")

    def update_hash_ids(self, saved_hash_id: SavedHashId, encoded_data_item: EncodedDataItem):
        """
        Must be called after self.save_hash_id()
        """
        for hash_id in self.sorted_hash_ids:
            self.update_saved_hash_item_code(hash_id=hash_id)
        #tree_updates = self.hash_ids_tree.hash_id_updates
        ##print(f"Tree updates:")
        ##pprint(dict(tree_updates))
        #for hash_id, update in tree_updates.items():
        #    assert (hash_id == update.hash_id)
        #    for hash_id_change in update.changes:
        #        if (hash_id_change.change_type == HashIdChangeType.HASH_ID_CODE):
        #            self.update_saved_hash_item_code(hash_id=hash_id)
        #            #self.set_hash_id_data_code(hash_id=hash_id, data_code=hash_id_change.new_value)
    
    def has_saved_hash_item(self, hash_id: HashId) -> bool:
        return (hash_id in self.saved_hash_items)
    
    def get_saved_hash_item(self, hash_id: HashId) -> SavedHashItem:
        if (self.has_saved_hash_item(hash_id=hash_id) is False):
            raise Exception(f"No saved hash item for hash_id={hash_id}")
        return self.saved_hash_items[hash_id]

    def has_init_item(self, hash_id: HashId) -> bool:
        return (hash_id in self.hash_id_init_items)

    def init_data_item(self, encoded_item: EncodedDataItem):
        if (self.has_init_item(encoded_item.hash_id)):
            raise Exception(f"Init item already created for hash_id={encoded_item.hash_id}: {self.hash_id_init_items[encoded_item.hash_id]}")
        # create initial element (immutable)
        init_item = InitHashItem(
            hash_id        = encoded_item.hash_id,
            hash_id_code   = encoded_item.hash_item_code,
            data_item_code = encoded_item.data_item_code,
            hash_digest    = encoded_item.hash_digest,
        )
        self.hash_id_init_items[encoded_item.hash_id] = init_item
        self.set_hash_id_digest(hash_id=encoded_item.hash_id, digest=encoded_item.hash_digest)
        self.encoded_bitarray += encoded_item.hash_item_code
    
    def update_saved_hash_item(self, saved_hash_id: SavedHashId):
        if (self.has_saved_hash_item(saved_hash_id.id) is False):
            # init dynamically updated hash_item
            data_item_code_length = len(saved_hash_id.encoded_hash_id) + self.default_score
            data_item_code        = self.get_bits_from_hash_id_digest(hash_id=saved_hash_id.id, bit_length=data_item_code_length)
            init_score            = self.hash_id_init_items[saved_hash_id.id]
            saved_item = SavedHashItem(
                hash_id        = saved_hash_id.id,
                hash_id_code   = saved_hash_id.encoded_hash_id,
                data_item_code = data_item_code,
                hash_digest    = self.get_hash_id_digest(hash_id=saved_hash_id.id),
                total_score    = init_score,
            )
            self.saved_hash_items[saved_hash_id.id] = saved_item
        # update hash item code if necessary
        self.saved_hash_items[saved_hash_id.id].update_hash_id_code(hash_id_code=saved_hash_id.encoded_hash_id)
    
    def save_hash_id(self, hash_id: HashId) -> SavedHashId:
        saved_hash_id = self.hash_ids_tree.save_hash_id(seed=hash_id.seed, nounce=hash_id.nounce)
        self.update_saved_hash_item(saved_hash_id=saved_hash_id)
        return saved_hash_id
    
    def save_encoded_data_item(self, encoded_data_item: EncodedDataItem):
        """
        Must be called before self.save_hash_id()
        """
        # init new item if this hash_id is never used before
        if (encoded_data_item.hash_id_type == HashIdType.NEW):
            self.init_data_item(encoded_item=encoded_data_item)
        # save new encoded item
        self.saved_data_items.append(deepcopy(encoded_data_item))
        
        # save hash id to tree and rebuild id trees and data trees
        saved_hash_id = self.save_hash_id(hash_id=encoded_data_item.hash_id)
        self.sorted_hash_ids.add(saved_hash_id.id)
        if (self.has_saved_hash_item(hash_id=saved_hash_id.id) is False):
            self.update_saved_hash_item(saved_hash_id=saved_hash_id)
        # update hash_id codes
        self.update_saved_hash_item_code(hash_id=encoded_data_item.hash_id)
        
        # catch updates for all changed hash_ids, rebuilding tree
        self.update_hash_ids(saved_hash_id=saved_hash_id, encoded_data_item=encoded_data_item) #, )
        
        # clear scanned item codes
        self.scanned_item_codes_by_length.clear()
        return encoded_data_item
    
    def save_data_item(self, data_item: bitarray) -> EncodedDataItem:
        encoded_data_item = self.encode_data_item(data_item=data_item)
        self.sorted_hash_ids.add(encoded_data_item.hash_id)
        self.save_encoded_data_item(encoded_data_item=encoded_data_item)
        return encoded_data_item
    
    def get_saved_data_item_code(self, data_item: bitarray) -> Union[EncodedDataItem, None]:
        data_item_code_lengths = SortedSet(self.data_item_codes_by_length.keys())
        for data_item_length in data_item_code_lengths:
            data_code = frozenbitarray(data_item[0:data_item_length])
            if (data_code in self.data_item_codes_by_length[data_item_length]):
                hash_id         = self.data_code_hash_ids[data_code]
                saved_hash_item = self.get_saved_hash_item(hash_id=hash_id)
                return EncodedDataItem(
                    hash_id        = hash_id,
                    hash_id_type   = HashIdType.OLD,
                    hash_item_code = saved_hash_item.hash_id_code.copy(),
                    data_item_code = saved_hash_item.data_item_code.copy(),
                    hash_digest    = self.get_hash_id_digest(hash_id=hash_id),
                )
                #return saved_hash_item
        return None
    
    #def get_saved_hash_item_code(self, hash_item: bitarray) -> Union[EncodedDataItem, None]:
    #    hash_item_code_lengths = self.hash_ids_tree.hash_id_lengths.copy()
    #    for hash_item_length in hash_item_code_lengths:
    #        hash_id_code = frozenbitarray(hash_item[0:hash_item_length])
    #        self.hash_ids_tree.decode_hash_id()
    #        if (hash_id_code in self.data_item_codes_by_length[hash_item_length]):
    #            hash_id         = self.data_code_hash_ids[hash_id_code]
    #            saved_hash_item = self.get_saved_hash_item(hash_id=hash_id)
    #            return EncodedDataItem(
    #                hash_id        = hash_id,
    #                hash_id_type   = HashIdType.OLD,
    #                hash_item_code = saved_hash_item.hash_id_code.copy(),
    #                data_item_code = saved_hash_item.data_item_code.copy(),
    #                hash_digest    = self.get_hash_id_digest(hash_id=hash_id),
    #            )
    #            #return saved_hash_item
    #    return None

    def encode_data_item(self, data_item: bitarray) -> EncodedDataItem:
        if (data_item.endian() != self.default_endian):
            raise Exception(f"Incorrect input data endian. Use '{self.default_endian}' ('{data_item.endian()}' given)")
        encoded_data_item = self.get_saved_data_item_code(data_item=data_item)
        if (encoded_data_item is not None):
            return encoded_data_item
        # creating new code if there is no match in existing codes
        new_hash_item = self.find_data_item(target_data_item=data_item)
        encoded_data_item  = EncodedDataItem(
            hash_id        = new_hash_item.hash_id,
            hash_id_type   = HashIdType.NEW,
            hash_item_code = self.hash_ids_tree.encode_hash_id(seed=new_hash_item.hash_id.seed, nounce=new_hash_item.hash_id.nounce),
            data_item_code = new_hash_item.data_item_code,
            hash_digest    = new_hash_item.hash_digest,
        )
        #if (self.debug is True):
        #    decoded_data_item = self.restore_scanned_item(target_hash_id=encoded_data_item.hash_id, clear_codes=False)
        #    assert(decoded_data_item.hash_id == encoded_data_item.hash_id)
        #    if (encoded_data_item.data_item_code != decoded_data_item.data_item_code):
        #        raise Exception(f"encoded_data_item={encoded_data_item.data_item_code} should be equal to decoded_data_item={decoded_data_item.data_item_code}, hash_id={encoded_data_item.hash_id}")
        return encoded_data_item
    
    def decode_data_item(self, data_item: bitarray) -> EncodedDataItem:
        if (data_item.endian() != self.default_endian):
            raise Exception(f"Incorrect input data endian. Use '{self.default_endian}' ('{data_item.endian()}' given)")
        #encoded_data_item = self.get_saved_data_item_code(data_item=data_item)
        #if (encoded_data_item is not None):
        #    return encoded_data_item
        hash_id = self.hash_ids_tree.decode_hash_id(encoded_hash_id=data_item)
        # trying to use existing codes first
        #if (self.has_saved_hash_id(hash_id=hash_id)):
        #    saved_hash_item = self.get_saved_hash_item(hash_id=hash_id)
        #    return EncodedDataItem(
        #        hash_id        = hash_id,
        #        hash_id_type   = HashIdType.OLD,
        #        hash_item_code = saved_hash_item.hash_id_code.copy(),
        #        data_item_code = saved_hash_item.data_item_code.copy(),
        #        hash_digest    = self.get_hash_id_digest(hash_id=hash_id),
        #    )
        if (hash_id in self.sorted_hash_ids) or (self.has_saved_hash_item(hash_id=hash_id)) or (self.has_saved_hash_id(hash_id=hash_id)):
            saved_hash_item = self.get_saved_hash_item(hash_id=hash_id)
            return EncodedDataItem(
                hash_id        = hash_id,
                hash_id_type   = HashIdType.OLD,
                hash_item_code = saved_hash_item.hash_id_code.copy(),
                data_item_code = saved_hash_item.data_item_code.copy(),
                hash_digest    = self.get_hash_id_digest(hash_id=hash_id),
            )
        if (self.has_hash_id_data_code(hash_id=hash_id)):
            return EncodedDataItem(
                hash_id        = hash_id,
                hash_id_type   = HashIdType.OLD,
                hash_item_code = self.get_saved_hash_id_code(hash_id=hash_id).copy(),
                data_item_code = self.get_hash_id_data_code(hash_id=hash_id).copy(),
                hash_digest    = self.get_hash_id_digest(hash_id=hash_id),
            )
        # creating new code if there is no match in existing codes: we restore data value from hash_id
        # by exactly repeating all search operations from self.find_data_item() stopping at the same hash_id
        # this is works like a blockchain history sync: we repeat all actions at same order and will have exactly same result
        restored_data_item = self.restore_scanned_item(target_hash_id=hash_id)
        if (restored_data_item.hash_id != hash_id):
            raise Exception(f"data_item={data_item}, restored hash_id={restored_data_item.hash_id} not equal to decoded hash_id={hash_id}")
        return EncodedDataItem(
            hash_id        = restored_data_item.hash_id,
            hash_id_type   = HashIdType.NEW,
            hash_item_code = self.hash_ids_tree.encode_hash_id(seed=restored_data_item.hash_id.seed, nounce=restored_data_item.hash_id.nounce),
            data_item_code = restored_data_item.data_item_code,
            hash_digest    = restored_data_item.hash_digest,
        )
    
    def encode_to_bitarray(self, data: bitarray) -> bitarray:
        item_id        = 0
        remaining_data = data
        if (data.endian() != self.default_endian):
            raise Exception(f"Incorrect input data endian. Use '{self.default_endian}' ('{data.endian()}' given)")
        while True:
            data_item         = remaining_data[0:self.max_data_item_length]
            encoded_data_item = self.save_data_item(data_item=data_item)
            #encoded_data_item = self.save_data_item(data_item=data_item)
            #saved_data_item = self.save_encoded_data_item(encoded_data_item=encoded_data_item)
            #saved_data_item = self.save_encoded_data_item(encoded_data_item=encoded_data_item)
            #saved_data_item = self.save_data_item(data_item=data_item)
            if (self.debug is True):
                print(f"{item_id}: encoded_item={encoded_data_item}")
                pprint(dict(SortedDict(self.data_item_code_ids_by_length)), max_length=26)
                #pprint(dict(SortedDict(self.data_code_hash_ids)), max_length=10)
            data_start     = encoded_data_item.data_item_length
            remaining_data = remaining_data[data_start:]
            
            if (len(remaining_data) < self.max_target_item_length):
            #if (len(remaining_data) == 0):
                self.data_tail        = remaining_data.copy()
                self.data_tail_length = len(self.data_tail)
                break
            item_id += 1
        return self.encoded_bitarray

    def decode_from_bitarray(self, data: bitarray) -> List[EncodedDataItem]:
        remaining_data = data
        item_id        = 0
        if (data.endian() != self.default_endian):
            raise Exception(f"Incorrect input data endian. Use '{self.default_endian}' ('{data.endian()}' given)")
        while True:
            hash_item = remaining_data[0:self.max_data_item_length]
            #hash_id = self.hash_ids_tree.decode_hash_id(encoded_hash_id=hash_item)
            #self.save_hash_id(hash_id=hash_id)
            decoded_data_item = self.decode_data_item(data_item=hash_item)
            #encoded_data_item = self.encode_data_item(data_item=decoded_data_item.data_item_code)
            #encoded_data_item = self.save_data_item(data_item=data_item)
            #saved_data_item   = self.save_encoded_data_item(encoded_data_item=decoded_data_item)
            if (self.debug is True):
                print(f"{item_id}: decoded_item={decoded_data_item}")
                pprint(dict(SortedDict(self.data_item_code_ids_by_length)), max_length=26)
            #self.sorted_hash_ids.add(encoded_data_item.hash_id)
            saved_data_item = self.save_encoded_data_item(encoded_data_item=decoded_data_item)
            #saved_data_item = self.save_data_item(data_item=data_item)
            
            data_start      = decoded_data_item.hash_id_length
            remaining_data  = remaining_data[data_start:]
            if (len(remaining_data) == 0):
                break
            item_id += 1
        return self.saved_data_items
    
    ### DATA ITEMS ###

    def has_saved_hash_id(self, hash_id: HashId) -> bool:
        return self.hash_ids_tree.has_saved_hash_id(hash_id=hash_id)
    
    def get_saved_hash_id(self, hash_id: HashId) -> SavedHashId:
        if (self.has_saved_hash_id(hash_id=hash_id) is False):
            raise Exception(f"Saved hash_id not found: {hash_id}")
        return self.hash_ids_tree.saved_hash_ids[hash_id]
    
    def get_saved_hash_id_code(self, hash_id: HashId) -> frozenbitarray:
        saved_hash_id = self.get_saved_hash_id(hash_id=hash_id)
        return frozenbitarray(saved_hash_id.encoded_hash_id.copy(), endian=self.default_endian)
    
    def get_saved_hash_id_code_length(self, hash_id: HashId) -> int:
        saved_hash_id = self.get_saved_hash_id(hash_id=hash_id)
        return saved_hash_id.encoded_length

    def has_hash_id_data_code(self, hash_id: HashId) -> bool:
        return (hash_id in self.hash_id_data_codes)
    
    def get_hash_id_data_code(self, hash_id: HashId) -> frozenbitarray:
        if (self.has_hash_id_data_code(hash_id=hash_id) is False):
            raise Exception(f"No data code for hash_id={hash_id}")
        return frozenbitarray(self.hash_id_data_codes[hash_id])
    
    def update_saved_hash_item_code(self, hash_id: HashId):
        if (self.has_saved_hash_item(hash_id=hash_id) is False):
            raise Exception(f"No saved hash item for hash_id={hash_id}")
        
        old_hash_item    = deepcopy(self.saved_hash_items[hash_id])
        new_hash_id_code = self.get_saved_hash_id_code(hash_id=hash_id).copy()
        self.saved_hash_items[hash_id].update_hash_id_code(hash_id_code=new_hash_id_code)
        
        #if (len(self.saved_hash_items[hash_id].data_item_code) > self.max_data_item_length):
        if (len(new_hash_id_code) > self.max_data_item_length):
            raise Exception(f"Too long code: hash_id={hash_id}, data_item_code={new_hash_id_code}, l={len(new_hash_id_code)}")
        
        if (old_hash_item.data_item_code != new_hash_id_code):
            if (old_hash_item.data_item_code in self.data_item_codes_by_length[old_hash_item.data_item_length]):
                self.data_item_codes_by_length[old_hash_item.data_item_length].discard(old_hash_item.data_item_code)
            if (old_hash_item.data_item_code in self.data_code_hash_ids):
                del self.data_code_hash_ids[old_hash_item.data_item_code]
            self.data_item_codes_by_length[len(new_hash_id_code)].add(new_hash_id_code)
            self.hash_id_data_codes[hash_id]          = new_hash_id_code
            self.data_code_hash_ids[new_hash_id_code] = hash_id
        
        if (new_hash_id_code not in self.data_item_codes_by_length[len(new_hash_id_code)]):
            self.data_item_codes_by_length[len(new_hash_id_code)].add(new_hash_id_code)
        if (hash_id not in self.hash_id_data_codes):
            self.hash_id_data_codes[hash_id] = new_hash_id_code
        if (self.saved_hash_items[hash_id].data_item_code not in self.data_code_hash_ids):
            self.data_code_hash_ids[new_hash_id_code] = hash_id

    def expand_hash_id_data_code(self, hash_id: HashId, new_data_code_length: int):
        if (self.has_hash_id_data_code(hash_id=hash_id) is False):
            raise Exception(f"No data code for hash_id={hash_id}")
        #new_data_item_code = self.get_bits_from_hash_id_digest(hash_id=hash_id, bit_length=new_data_code_length)
        #self.set_hash_id_data_code(hash_id=hash_id, data_code=new_data_item_code)
        self.update_saved_hash_item_code(hash_id=hash_id)
    
    def get_bits_from_hash_id_digest(self, hash_id: HashId, bit_length: int) -> frozenbitarray:
        digest = self.get_hash_id_digest(hash_id=hash_id)
        return last_fba_bits_from_digest(digest=digest, bit_length=bit_length, endian=self.default_endian, signed=False).copy()
    
    def has_hash_id_digest(self, hash_id: HashId) -> bool:
        return (hash_id in self.hash_id_digests)

    def get_hash_id_digest(self, hash_id: HashId) -> int:
        if (self.has_hash_id_digest(hash_id=hash_id) is False):
            raise Exception(f"No digest for hash_id={hash_id}")
        return self.hash_id_digests[hash_id]

    def set_hash_id_digest(self, hash_id: HashId, digest: int):
        if (self.has_hash_id_digest(hash_id=hash_id)):
            current_digest = self.get_hash_id_digest(hash_id=hash_id)
            if (current_digest != digest):
                raise Exception(f"Trying to set new digest for hash_id={hash_id} (already existing digest={current_digest})")
            return
        self.hash_id_digests[hash_id] = digest