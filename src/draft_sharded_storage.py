#!/usr/bin/python

# https://github.com/Textualize/rich
from fileinput import filename
from rich import print
# https://rich.readthedocs.io/en/latest/pretty.html
from rich.pretty import pprint
import rich.repr
# https://rich.readthedocs.io/en/latest/appendix/box.html
from rich import box
from rich.align import Align
# https://rich.readthedocs.io/en/latest/console.html
from rich.console import Console, Group
from rich.layout import Layout
from rich.panel import Panel
from rich.progress import Progress, SpinnerColumn, BarColumn, TextColumn
from rich.syntax import Syntax
from rich.table import Table as Table
from rich.text import Text as Text
# https://rich.readthedocs.io/en/latest/prompt.html
from rich.prompt import Prompt, IntPrompt
from rich.live import Live

# https://docs.python.org/3/library/multiprocessing.html#multiprocessing.JoinableQueue
import multiprocessing as mp

# https://realpython.com/python-data-classes/#advanced-default-values
from dataclasses import dataclass

from typing import List, Dict, Set, Tuple, Union, Optional

from custom_counter import CustomCounter as Counter

# https://bitstring.readthedocs.io/en/latest/index.html
from bitstring import Bits, BitArray, BitStream, ConstBitStream

import os, sys, io, leb128, json

if not 'workbookDir' in globals():
    workbookDir = os.getcwd()
os.chdir(workbookDir)

from datetime import datetime
from time import sleep

from hash_space_utils import HashItemAddress, \
    get_min_bit_length, get_aligned_bit_length

class Header:
    """Database generation process"""

    def __rich__(self) -> Panel:
        grid = Table.grid(expand=True)
        grid.add_column(justify="center", ratio=1)
        grid.add_column(justify="right")
        grid.add_row(
            "[b]Rich[/b] Current time",
            datetime.now().ctime().replace(":", "[blink]:[/]"),
        )
        return Panel(grid, style="white on blue")

class Footer:
    """Console output"""
    messages : List[Text] = list()

    def __rich__(self) -> Panel:
        console = Console()
        # https://rich.readthedocs.io/en/latest/console.html#paging
        with console.pager():
            grid = Table.grid(expand=True)
            grid.add_column(justify="left")
            for message in self.messages:
                grid.add_row(message)
        return Panel(grid, box=box.SIMPLE_HEAD)
    
    def print(self, message: str):
        console = Console()
        # print message with console and capture output
        with console.capture() as capture:
            console.print(f"> {message}")
        output = capture.get()
        # apply output console styles to message
        text   = Text.styled(output, justify="left")
        self.messages.insert(0, text)
    
    def pprint(self, value, indent_guides: bool=True, 
            max_length: Optional[int]=None, max_string: Optional[int]=50, max_depth: Optional[int]=None, expand_all: bool=False):
        console = Console()
        # print message with console and capture output
        with console.capture() as capture:
            pprint(
                value, 
                console       = console, 
                indent_guides = indent_guides, 
                max_length    = max_length, 
                max_string    = max_string, 
                max_depth     = max_depth,
                expand_all    = expand_all
            )
        output = capture.get()
        # apply output console styles to message
        text   = Text.styled(output, justify="left")
        # self.messages.insert(0, text)
        self.messages.insert(0, text)

def make_layout() -> Layout:
    """Define the layout."""
    layout = Layout(name="root")

    layout.split(
        Layout(name="header", size=3),
        Layout(name="main", ratio=1),
        Layout(name="footer", ratio=1),
    )
    layout["main"].split_row(
        Layout(name="side"),
        Layout(name="body"),
    )
    layout["side"].split(Layout(name="tree"), Layout(name="steps"))

    return layout

def main(q: mp.Queue):
    # minimum value length, required for any value instances to be saved in database
    min_value_length     = 2
    # number of items in first value tier
    min_value_options    = (256 ** min_value_length)
    # address must be shorter then value
    min_address_length   = (min_value_length - 1)
    default_active_cores = 4
    # number of job queue, processed by workers (in parallel)
    jobs_per_tier        = 256

    # init Rich console
    console = Console()

    # options input
    active_core_options = [f"{cv}" for cv in list(range(1, mp.cpu_count() + 1))]
    max_active_cores    = IntPrompt.ask(
        f"System has {mp.cpu_count()} cores available. Hom many cores should be used?", 
        choices = active_core_options, 
        default = default_active_cores,
        console = console
    )
    console.print(f"Process will use {max_active_cores} core(s)")

    # init layout parts
    header = Header()
    footer = Footer()

    # build layout
    layout = make_layout()
    layout["header"].update(header)
    layout["tree"].update(Panel(layout.tree, border_style="red"))
    layout["footer"].update(footer)

    # start displaing UI
    with Live(layout, refresh_per_second=0.5, screen=True) as live: #, console=console):
        #console.set_alt_screen(True)
        # log start event
        footer.print(f"Started main")
        live.refresh()
        sleep(2)
        
        

        # log finish event
        footer.print('Finished main')
        live.refresh()
        sleep(2)
    
    # finish processing
    pass    

if __name__ == '__main__':
    mp.set_start_method('spawn')
    q = mp.Queue()

    main(q)