# https://stackoverflow.com/questions/38340808/recursive-typing-in-python-3-5
from __future__ import annotations
from rich import print
from rich.pretty import pprint
from rich.text import Text
from rich.progress import TaskID, Progress, SpinnerColumn, BarColumn, TextColumn, MofNCompleteColumn,\
    TimeElapsedColumn, TimeRemainingColumn, track
from tqdm import tqdm
from custom_counter import CustomCounter as Counter
from custom_rich import CustomTaskProgressColumn as TaskProgressColumn
from bitarray.util import ba2int, int2ba, huffman_code, canonical_huffman, canonical_decode
from bitarray import bitarray, frozenbitarray
from collections import defaultdict, namedtuple
from dataclasses import dataclass, field
from copy import deepcopy, copy
from typing import List, Set, Dict, Tuple, Optional, Union, Iterable, Sequence, Generator
import random
import itertools

# https://ru.wikipedia.org/wiki/%D0%9B%D0%B8%D0%BD%D0%B5%D0%B9%D0%BD%D1%8B%D0%B9_%D0%BA%D0%BE%D0%BD%D0%B3%D1%80%D1%83%D1%8D%D0%BD%D1%82%D0%BD%D1%8B%D0%B9_%D0%BC%D0%B5%D1%82%D0%BE%D0%B4#%D0%A7%D0%B0%D1%81%D1%82%D0%BE_%D0%B8%D1%81%D0%BF%D0%BE%D0%BB%D1%8C%D0%B7%D1%83%D0%B5%D0%BC%D1%8B%D0%B5_%D0%BF%D0%B0%D1%80%D0%B0%D0%BC%D0%B5%D1%82%D1%80%D1%8B
INIT_PRESETS = [
    (20, 2 ** 20, 	106,    1283,    6075),
    (21, 2 ** 21, 	211,    1663,    7875),
    (22, 2 ** 22, 	421,    1663,    7875),
    (23, 2 ** 23, 	430,    2531,    11979),
    (23, 2 ** 23, 	936,    1399,    6655),
    (23, 2 ** 23, 	1366,   1283,    6075),
    (24, 2 ** 24, 	171,    11213,   53125),
    (24, 2 ** 24, 	859,    2531,    11979),
    (24, 2 ** 24, 	419,    6173,    29282),
    (24, 2 ** 24, 	967,    3041,    14406),
    (25, 2 ** 25, 	141,    28411,   134456),
    (25, 2 ** 25, 	625,    6571,    31104),
    (25, 2 ** 25, 	1541,   2957,    14000),
    (25, 2 ** 25, 	1741,   2731,    12960),
    (25, 2 ** 25, 	1291,   4621,    21870),
    (25, 2 ** 25, 	205,    29573,   139968),
    (26, 2 ** 26, 	421,    17117,   81000),
    (26, 2 ** 26, 	1255,   6173,    29282),
    (26, 2 ** 26, 	281,    28411,   134456),
    (27, 2 ** 27, 	1093,   18257,   86436),
    (27, 2 ** 27, 	421,    54773,   259200),
    (27, 2 ** 27, 	1021,   24631,   116640),
    (28, 2 ** 28, 	1277,   24749,   117128),
    (28, 2 ** 28, 	2041,   25673,   121500),
    (29, 2 ** 29, 	2311,   25367,   120050),
    (29, 2 ** 29, 	1597,   51749,   244944),
    (29, 2 ** 29, 	2661,   36979,   175000),
    (29, 2 ** 29, 	4081,   25673,   121500),
    (29, 2 ** 29, 	3661,   30809,   145800),
    (30, 2 ** 30, 	3877,   29573,   139968),
    (30, 2 ** 30, 	3613,   45289,   214326),
    (30, 2 ** 30, 	1366,   150889,  714025),
    (31, 2 ** 31, 	8121,   28411,   134456),
    (31, 2 ** 31, 	4561,   51349,   243000),
    (31, 2 ** 31, 	7141,   54773,   259200),
    (32, 2 ** 32, 	9301,   49297,   233280),
    (32, 2 ** 32, 	4096,   150889,  714025),
    (33, 2 ** 33, 	2416,   374441,  1771875),
    (34, 2 ** 34, 	17221,  107839,  510300),
    (34, 2 ** 34, 	36261,  66037,   312500),
    (35, 2 ** 35, 	84589,  45989,   217728),
]
# X_n = (a * (X_n-1) + b) mod m

@dataclass
class LinearGenerator:
    gen_fn     : Generator
    bit_length : int
    period     : int

    def __init__(self, a: int, b: int, m: int, bit_length: int=None):
        self.gen_fn     = linear_generator(a=a, b=b, m=m)
        self.bit_length = bit_length
        self.period     = m

def create_generator(preset_id: int) -> Generator:
    bit_length, max_bits, a, b, m = INIT_PRESETS[preset_id]
    #return LinearGenerator(a=a, b=b, m=m, bit_length=bit_length, period=period)
    return linear_generator(a=a, b=b, m=m)

# , bit_length: int=None
def linear_generator(a: int, b: int, m: int) -> Generator[int, None, None]:
    x_prev     = 0
    #bit_length = bit_length
    period     = m
    while True:
        x_n = (a * x_prev + b) % period
        yield x_n
        x_prev = x_n
        

class CMWC(random.Random):
    'Long period random number generator: Complementary Multiply with Carry'
    # http://en.wikipedia.org/wiki/Multiply-with-carry

    a = 3636507990
    logb = 32
    b = 2 ** logb
    r = 1359

    def _gen_word(self):
        i = self.i
        self.c, xc = divmod(self.a * self.Q[i] + self.c, self.b)
        x = self.Q[i] = (self.b - 1 - xc)
        self.i = (0 if i + 1 == self.r else i + 1)
        return x

    def getrandbits(self, k):
        while self.bits < k:
            self.f = (self.f << self.logb) | self._gen_word()
            self.bits += self.logb
        x = self.f & ((1 << k) - 1)
        self.f >>= k;  self.bits -= k
        return x

    def random(self, RECIP_BPF=random.RECIP_BPF, BPF=random.BPF):
        return self.getrandbits(BPF) * RECIP_BPF

    def seed(self, seed=None):
        seeder = random.Random(seed)
        Q = [seeder.randrange(0x100000000) for i in range(self.r)]
        c = seeder.randrange(0x100000000)
        self.setstate(0, 0, 0, c, Q)

    def getstate(self):
        return self.f, self.bits, self.i, self.c, tuple(self.Q)

    def setstate(self, f, bits, i, c, Q):
        self.f, self.bits, self.i, self.c, self.Q = f, bits, i, c, list(Q)

# https://code.activestate.com/recipes/576707/
def cmwc_random(seed=None, a=3636507990, b=2**32, logb=32, r=1359) -> Generator[int, None, None]:
    seeder = random.Random(seed)
    Q = [seeder.randrange(b) for i in range(r)]
    c = seeder.randrange(b)
    f = bits = 0
    for i in itertools.cycle(list(range(r))):
        t = a * Q[i] + c
        c = b - 1 - (t >> logb)
        x = Q[i] = t & (b - 1)
        f = (f << logb) | x;  bits += logb
        if bits >= 53:            
            #yield seeder.getstate()[1]
            yield (f & (2 ** 53 - 1)) * (2 ** -53)
            f >>= 53;  bits -= 53
