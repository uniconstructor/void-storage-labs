# https://stackoverflow.com/questions/38340808/recursive-typing-in-python-3-5
from __future__ import annotations
from rich import print
from rich.pretty import pprint
from tqdm import tqdm
from custom_counter import CustomCounter as Counter
from bitarray.util import ba2int, int2ba
from bitarray import bitarray, frozenbitarray
from collections import defaultdict, namedtuple
from dataclasses import dataclass, field
from copy import deepcopy, copy
from typing import List, Set, Dict, Tuple, Optional, Union, Iterable
from sortedcontainers import SortedSet, SortedDict, SortedList
from functools import lru_cache
#import math

from canonical_huffman_encoder import create_canonical_codes, CanonicalHuffmanDict

DEFAULT_ENDIAN         = 'big'
# total bits reserved for storing bit value length
DEFAULT_PREFIX_LENGTH  = 4
# minimal number of bits for bit value length
DEFAULT_START_LENGTH   = 0
# default options for new varint-related objects: must be None by default (but can be overridden)
DEFAULT_VARINT_OPTIONS = None

LAYER_ID_PREFIX_LENGTH    = 7
TOTAL_ITEMS_PREFIX_LENGTH = 14
TAIL_LENGTH_PREFIX_LENGTH = 6

def create_value_ranges(max_ranges: int,  min_index_bits: int):
    value_ranges   = list()
    max_index_bits = min_index_bits + max_ranges
    offset         = 0
    for value_length in range(min_index_bits, max_index_bits):
        range_id    = len(value_ranges)
        start_value = offset
        end_value   = offset + 2**value_length
        value_range = range(start_value, end_value)
        value_ranges.append(value_range)
        offset     += len(value_range)
        #print(f"range_id={range_id}: ({value_range.start}, {value_range.stop})")
    return value_ranges

def create_varint_item_id_range_limits(options: VarintOptions) -> Dict[int, int]:
    range_limits = dict()
    offset       = 0
    for item_id_length in range(options.get_min_item_id_length(), options.get_max_item_id_length()+1):
        range_id     = len(range_limits)
        value_length = options.get_value_length_by_item_id_length(id_length=item_id_length)
        start_value  = offset
        end_value    = offset + 2**item_id_length
        max_items    = len(range(start_value, end_value))
        range_limits[value_length] = max_items
        offset     += max_items
        #print(f"range_id={range_id}: ({value_range.start}, {value_range.stop})")
    return range_limits

def create_varint_item_id_max_values(options: VarintOptions) -> Dict[int, int]:
    range_limits = dict()
    offset       = 0
    for value_length in range(options.get_min_value_length(), options.get_max_value_length()+1):
        range_id       = len(range_limits)
        item_id_length = options.get_item_id_length_by_value_length(value_length=value_length)
        start_value    = offset
        end_value      = offset + 2**value_length
        max_values     = len(range(start_value, end_value))
        range_limits[item_id_length] = max_values
        offset        += max_values
        #print(f"range_id={range_id}: ({value_range.start}, {value_range.stop})")
    return range_limits

@dataclass()
class VarintDataLayer:
    layer_id        : int                   = field()
    total_items     : int                   = field()
    tail_length     : int                   = field()
    item_id_lengths : List[int]             = field()
    item_ids        : List[int]             = field()
    tail_bits       : frozenbitarray        = field()
    #item_id_pairs   : List[Tuple[int, int]] = field(default_factory=list)

@dataclass()
class VarintDictItem:
    value_bits         : bitarray = field()
    item_id            : int      = field()
    item_id_length     : int      = field()
    item_prefix_length : int      = field(init=False, repr=False)
    min_item_id_length : int      = field(init=False, repr=False)
    item_prefix_id     : int      = field(init=False, default=None, repr=False)
    item_prefix_bits   : int      = field(init=False, default=None)
    item_id_bits       : int      = field(init=False, default=None)
    item_bits          : bitarray = field(init=False, default=None, repr=False)
    value_length       : int      = field(init=False, default=None)
    item_length        : int      = field(init=False, default=None)

    def __init__(self, data_bits: bitarray, item_id_length: int, item_id: int|None=None, 
                 varint_options: VarintOptions=DEFAULT_VARINT_OPTIONS):
        self.item_prefix_length = varint_options.prefix_length 
        self.min_item_id_length = varint_options.start_length
        self.value_bits         = data_bits.copy()
        self.item_id_length     = item_id_length
        self.item_id            = item_id
        self.value_length       = len(self.value_bits)
        self.item_length        = self.item_prefix_length + self.item_id_length
        self.item_prefix_id     = self.item_id_length - self.min_item_id_length
        self.item_prefix_bits   = frozenbitarray(int2ba(self.item_prefix_id, length=self.item_prefix_length, endian='big', signed=False))
        self.item_id_bits       = None
        self.item_bits          = None
       
        if (self.item_id is not None):
            self.item_id_bits = frozenbitarray(int2ba(self.item_id, length=self.item_id_length, endian='big', signed=False))
            self.item_bits    = frozenbitarray(self.item_prefix_bits + self.item_id_bits)
            if (self.item_length != len(self.item_bits)):
                raise Exception(f"self.item_length={self.item_length}, but len(self.item_bits): {len(self.item_bits)}")

@dataclass()
class VarintSplitItem:
    position_id        : int      = field()
    data_bits          : bitarray = field()
    item_id_length     : int      = field()
    item_id            : int      = field(default=None)
    item_prefix_length : int      = field(default=DEFAULT_PREFIX_LENGTH)
    min_item_id_length : int      = field(default=DEFAULT_START_LENGTH, repr=False)
    item_prefix_id     : int      = field(init=False, default=None)
    item_prefix_bits   : int      = field(init=False, default=None)
    item_id_bits       : int      = field(init=False, default=None)
    item_bits          : bitarray = field(init=False, default=None)
    data_length        : int      = field(init=False, default=None)
    item_length        : int      = field(init=False, default=None)

    def __init__(self, position_id: int, data_bits: bitarray, item_id_length: int, item_id: int|None=None, 
                 prefix_length: int=DEFAULT_PREFIX_LENGTH, start_length: int=DEFAULT_START_LENGTH):
        self.position_id      = position_id 
        self.prefix_length    = prefix_length 
        self.start_length     = start_length
        self.data_bits        = data_bits.copy()
        self.item_id_length   = item_id_length
        self.item_id          = item_id
        self.data_length      = len(self.data_bits)
        self.item_length      = self.prefix_length + self.item_id_length
        self.item_prefix_id   = self.item_id_length - self.min_item_id_length
        self.item_prefix_bits = frozenbitarray(int2ba(self.item_prefix_id, length=self.item_prefix_length, endian='big', signed=False))
        self.item_id_bits     = None
        self.item_bits        = None
       
        if (self.item_id is not None):
            self.item_id_bits = frozenbitarray(int2ba(self.item_id, length=self.item_id_length, endian='big', signed=False))
            self.item_bits    = frozenbitarray(self.item_prefix_bits + self.item_id_bits)
            if (self.item_length != len(self.item_bits)):
                raise Exception(f"self.item_length={self.item_length}, but len(self.item_bits): {len(self.item_bits)}")
            
@dataclass()
class DictSplitItem:
    is_new      : bool           = field()
    position_id : int            = field()
    data_bits   : bitarray       = field()
    dict_value  : VarintDictItem = field(default=None)

@dataclass()
class VarintDataDict:
    options            : VarintOptions                        = field(repr=False)
    items              : Dict[frozenbitarray, VarintDictItem] = field(default=None, init=False)
    value_counts       : Dict[int, Counter]                   = field(default=None, init=False, repr=False)
    length_counts      : Counter                              = field(default=None, init=False)
    value_limits       : Dict[int, int]                       = field(default=False, init=False)
    open_value_lengths : SortedSet[int]|Set[int]              = field(default=False, init=False)
    active_length      : int                                  = field(default=False, init=False)
    capacity_overflow  : bool                                 = field(default=False, init=False)
    value_lengths      : SortedSet[int]|Set[int]              = field(default=False, init=False)
    item_lengths       : SortedSet[int]|Set[int]              = field(default=False, init=False)
    values_by_length   : Dict[int, Set[frozenbitarray]]       = field(default=False, init=False)
    values_by_id       : Dict[int, Dict[int, frozenbitarray]] = field(default=False, init=False)
    ids_by_value       : Dict[int, Dict[frozenbitarray, int]] = field(default=False, init=False)
    #value_history      : Dict[int, Dict[frozenbitarray, int]] = field(default=False, init=False, repr=False)

    def __init__(self, options: VarintOptions=DEFAULT_VARINT_OPTIONS):
        self.options              = options
        self.items                = dict()
        self.value_counts          = defaultdict(Counter)
        self.length_counts        = Counter()
        self.value_limits         = create_varint_item_id_range_limits(options=self.options)
        self.capacity_overflow    = False
        self.value_lengths        = SortedSet(self.options.get_all_value_lengths())
        self.item_lengths         = SortedSet(self.options.get_all_item_id_lengths())
        self.values_by_length     = defaultdict(SortedSet)
        self.values_by_id         = defaultdict(dict)
        self.ids_by_value         = defaultdict(dict)
        #self.value_history        = defaultdict(dict)

        self.open_value_lengths   = self.value_lengths.copy()
        self.active_length        = min(self.open_value_lengths)
        
        for value_length in self.value_lengths:
            self.length_counts[value_length] = 0
    @property
    def active_value_lengths(self) -> List[int]:
        active_lengths = list()
        for value_length in self.value_lengths:
            if (value_length not in self.open_value_lengths):
                active_lengths.append(value_length)
                continue
            if (value_length == self.active_length):
                active_lengths.append(value_length)
                continue
            break
        return active_lengths

    def get_item_id_length_by_data_item_length(self, data_item_length: int) -> int:
        return self.options.get_item_id_length_by_value_length(value_length=data_item_length)
    
    def has_open_slots(self, value_length: int) -> bool:
        if (value_length not in self.open_value_lengths):
            return False
        if (self.length_counts[value_length] == self.value_limits[value_length]):
            return False
        if (self.capacity_overflow is True):
            return False
        return True

    def can_add_data_item(self, data_item: bitarray) -> bool:
        if (self.has_open_slots(value_length=len(data_item)) is False):
            return False
        return True
    
    def get_new_item_id(self, item_value: frozenbitarray) -> int:
        value_length = len(item_value)
        return len(self.values_by_length[value_length])
    
    def predict_new_item_id(self, value_length: int, new_length_counts: Counter) -> int:
        new_length_counts = self.length_counts.copy() + new_length_counts
        return new_length_counts[value_length]
    
    def add_data_item(self, data_item: frozenbitarray) -> VarintDictItem:
        if isinstance(data_item, frozenbitarray) is False:
            raise Exception(f"Cannot add new data_item: frozenbitarray expected")
        if self.has_value(value_bits=data_item):
            raise Exception(f"Cannot add new data_item={data_item.to01()}: item already in dict")
        
        value_length   = len(data_item)
        item_value     = frozenbitarray(data_item)
        item_id_length = self.get_item_id_length_by_data_item_length(data_item_length=value_length)
        item_id        = self.get_new_item_id(item_value=item_value) #self.length_counts[data_item_length]
        
        if (self.can_add_data_item(data_item=data_item) is False):
            raise Exception(f"Cannot add new item_value={item_value.to01()} (l={value_length}): limit reached", self.length_counts)
        # add new value to dict
        self.values_by_length[value_length].add(item_value)
        self.values_by_id[value_length][item_id]    = item_value
        self.ids_by_value[value_length][item_value] = item_id
        # need update stats to track dict capacity
        self.length_counts.update({ value_length : 1 })
        #self.value_counts[value_length].update({ item_value : 1 })

        new_item = VarintDictItem(
            data_bits      = frozenbitarray(data_item),
            item_id_length = item_id_length,
            item_id        = item_id,
            varint_options = self.options,
        )
        self.items[item_value] = new_item

        if (self.length_counts[value_length] >= self.value_limits[value_length]):
            self.open_value_lengths.remove(value_length)
            if (len(self.open_value_lengths) > 0):
                self.active_length = min(self.open_value_lengths)
            else:
                self.active_length = None
            #print(f"value={data_item.to01()} (length={value_length}): last tier item added, new active_length={self.active_length}")
            if (self.active_length is None):
                print(f"CAPACITY REACHED")
        return new_item
    
    def increment_item_count(self, data_item: frozenbitarray) -> VarintDictItem:
        if (self.has_value(value_bits=data_item) is False):
            raise Exception(f"increment_item_count(): no data_item={data_item.to01()} ({len(data_item)}) in dict")
        value_length = len(data_item)
        self.value_counts[value_length].update({ data_item : 1 })
        value_id = self.get_value_id(value_bits=data_item)
        return self.get_value(value_length=value_length, value_id=value_id)
    
    def import_data_items(self, data_items: List[bitarray]):
        for data_item in data_items:
            self.add_data_item(data_item=data_item)

    def has_value(self, value_bits: frozenbitarray) -> bool:
        value_length = len(value_bits)
        return (value_bits in self.values_by_length[value_length])
    
    def get_value_id(self, value_bits: frozenbitarray) -> int:
        value_length = len(value_bits)
        if (self.has_value(value_bits=value_bits) is False):
            raise Exception(f"No value_id for bits={value_bits.to01()} in length={len(value_bits)}")
        return self.ids_by_value[value_length][value_bits]
    
    def get_value_item(self, value_length: int, value_id: int) -> VarintDictItem:
        if (value_id not in self.values_by_id[value_length]):
            raise Exception(f"No value with id={value_id} in length={value_length}")
        item_value = self.values_by_id[value_length][value_id]
        return self.items[item_value]

    #def get_value(self, value_bits: frozenbitarray) -> VarintDictItem:
    #def get_value(self, value_length: int, value_id: int) -> VarintDictItem:
    def get_value(self, value_length: int, value_id: int) -> frozenbitarray:
        if (value_id not in self.values_by_id[value_length]):
            raise Exception(f"No value with id={value_id} in length={value_length}")
        item_value = self.values_by_id[value_length][value_id]
        return item_value #self.items[item_value]
    
    def get_value_id_length(self, value_bits: frozenbitarray) -> int:
        value_length    = len(value_bits)
        value_id_length = value_length - self.options.prefix_length - 1
        return value_id_length
    
    #def get_item_value_id(self, value_bits: frozenbitarray) -> int:
    #    value_length    = len(value_bits)
    #    prefix_length   = self.options.prefix_length
    #    value_id_length = value_length - prefix_length - 1
    #    return value_id_length
        
    
    def preview_split_result(self, data: frozenbitarray, layer_id: int=0, max_new_items: int=None) -> SplitResultPreview:
        # делаем копию текущего словаря чтобы не менять этот
        preview_dict         = deepcopy(self)
        data_length          = len(data)
        input_split_bits     = bitarray(endian=DEFAULT_ENDIAN)
        output_length_bits   = bitarray(endian=DEFAULT_ENDIAN)
        output_item_id_bits  = bitarray(endian=DEFAULT_ENDIAN)
        # значения, полученные в результате разбиения
        split_items          = list()
        # количество использований значений словаря
        value_counts         = defaultdict(Counter)
        # количество новых значений для словаря (по длине)
        new_value_counts     = Counter()
        start_bit            = 0
        end_bit              = 0
        position_id          = 0
        tail_length          = 0
        tail_bits            = frozenbitarray()
        end_of_data          = False

        while (end_of_data is False):
            scan_item        = None
            active_item      = None
            new_item         = None
            split_item       = None
            dict_value       = None
            # сначала проверяем существующие значения
            for value_length in preview_dict.active_value_lengths:
                item_start_bit = start_bit
                item_end_bit   = item_start_bit + value_length
                scan_item      = data[item_start_bit:item_end_bit]
                if (item_end_bit > data_length):
                    end_of_data = True
                    end_bit     = start_bit
                    break
                if (preview_dict.has_value(value_bits=scan_item)):
                    active_item = scan_item.copy()
                    break
            # если нашли существующее значение - новое не ищем
            if (active_item is not None) and (end_of_data is False):
                active_value_id = preview_dict.get_value_id(value_bits=active_item)
                dict_value      = preview_dict.get_value_item(value_length=len(active_item), value_id=active_value_id)
                split_item = DictSplitItem(
                    is_new      = (self.has_value(value_bits=active_item) is False),
                    position_id = position_id,
                    data_bits   = active_item.copy(),
                    dict_value  = dict_value,
                )
                split_items.append(split_item)
                value_counts[value_length].update({ active_item : 1 })
                preview_dict.increment_item_count(data_item=active_item)
                input_split_bits    += active_item
                #output_length_bits  += dict_value.item_prefix_bits
                #output_item_id_bits += dict_value.item_id_bits
                position_id         += 1
                end_bit              = item_end_bit
                start_bit            = end_bit
                continue
            # перед добавлением нового значения проверяем что данные не закончились
            if (start_bit + preview_dict.active_length) > data_length:
                end_of_data = True
            # прекращаем процесс если в процессе поиска значения закончились данные
            if (end_of_data is True):
                break
            # когда существующее значение не нашли - добавляем новое
            value_length   = preview_dict.active_length
            item_start_bit = start_bit
            item_end_bit   = item_start_bit + value_length
            new_item       = data[item_start_bit:item_end_bit]
            # останавливаемся когда словарь закончился
            if (preview_dict.can_add_data_item(data_item=new_item) is False):
                raise Exception(f"Cannot add new_item={new_item.to01()}")
                #print(f"Cannot add new_item={new_item.to01()}")
                #break
            # имитируем будущее значение словаря для нового элемента
            dict_value = preview_dict.add_data_item(data_item=new_item)
            split_item = DictSplitItem(
                is_new      = True,
                position_id = position_id,
                data_bits   = new_item.copy(),
                dict_value  = dict_value,
            )
            split_items.append(split_item)
            new_value_counts.update({ value_length : 1 })
            value_counts[value_length].update({ new_item : 1 })
            preview_dict.increment_item_count(data_item=new_item)

            input_split_bits    += new_item
            #output_length_bits  += dict_value.item_prefix_bits
            #output_item_id_bits += dict_value.item_id_bits
            position_id         += 1
            end_bit              = item_end_bit
            start_bit            = end_bit
            # завершаем процесс если это был последний элемент
            if (end_bit == data_length):
                break
            if (preview_dict.capacity_overflow is True) or (preview_dict.active_length is None):
                raise Exception(f"Capacity_overflow")
                #break
        # проверяем не осталось ли в конце необработанных данных
        tail_length    = data_length - end_bit
        tail_start_bit = data_length - tail_length
        tail_end_bit   = data_length
        if (tail_length > 0):
            tail_bits = data[tail_start_bit:tail_end_bit]
        
        # init data layer
        next_layer_id         = layer_id + 1
        layer_item_id_lengths = list()
        layer_item_ids        = list()
        #layer_item_pairs      = list()
        split_item_position = 0
        for split_item in split_items:
            #split_item_value_length    = len(split_item.data_bits)
            #split_item_value_id_length = split_item_value_length - self.options.prefix_length - 1
            split_item_value_id_length = preview_dict.get_value_id_length(value_bits=split_item.data_bits)
            split_item_value_id        = preview_dict.get_value_id(value_bits=split_item.data_bits)
            if (split_item_value_id_length == 0):
                print(f"split_item_position={split_item_position}: l={split_item_value_id_length}, id={split_item_value_id}")
            #split_item_value_pair      = (split_item_value_id_length, split_item_value_id)
            #if (split_item_value_id_length > 0):
            layer_item_id_lengths.append(split_item_value_id_length)
            layer_item_ids.append(split_item_value_id)
            split_item_position += 1
            #layer_item_pairs.append(split_item_value_pair)
        data_layer = VarintDataLayer(
            layer_id        = next_layer_id,
            total_items     = len(split_items),
            item_id_lengths = layer_item_id_lengths,
            item_ids        = layer_item_ids,
            tail_length     = tail_length,
            tail_bits       = tail_bits,
            #item_id_pairs   = layer_item_pairs,
        )
        
        return SplitResultPreview(
            new_value_counts    = new_value_counts,
            value_counts        = value_counts,
            split_items         = split_items,
            capacity_overflow   = preview_dict.capacity_overflow,
            end_bit             = end_bit,
            data_length         = data_length,
            data_dict           = preview_dict,
            data_bits           = data.copy(),
            input_split_bits    = input_split_bits,
            output_length_bits  = output_length_bits,
            output_item_id_bits = output_item_id_bits,
            tail_length         = tail_length,
            tail_bits           = tail_bits,
            data_layer          = data_layer,
        )
    
    def apply_split_result(self, split_result: SplitResultPreview):
        for new_value in split_result.new_values:
            self.add_data_item(data_item=new_value)
        for value_length, value_counts in split_result.value_counts_diff.items():
            self.value_counts[value_length].update(value_counts)
        
        assert(self.active_length == split_result.data_dict.active_length)
        #self.length_counts.update(split_result.new_value_counts)
        #self.active_length = split_result.data_dict.active_length
        for value_length, total_items in self.length_counts.items():
            assert(total_items == split_result.data_dict.length_counts[value_length])

        new_open_value_lengths = list(split_result.data_dict.open_value_lengths)
        old_open_value_lengths = list(self.open_value_lengths)
        for _id in range(0, len(new_open_value_lengths)):
            assert(old_open_value_lengths[_id] == new_open_value_lengths[_id])
        

@dataclass()
class SplitResultPreview:
    new_value_counts    : Counter
    value_counts        : Dict[int, Counter]
    split_items         : List[DictSplitItem]
    capacity_overflow   : bool
    end_bit             : int
    data_length         : int
    data_dict           : VarintDataDict
    data_bits           : bitarray
    input_split_bits    : bitarray
    output_length_bits  : bitarray
    output_item_id_bits : bitarray
    tail_length         : int
    tail_bits           : bitarray|None
    data_layer          : VarintDataLayer

    @property
    def new_values(self) -> List[frozenbitarray]:
        new_values = list()
        for item in self.split_items:
            if (item.data_bits in new_values):
                continue
            if (item.is_new):
                new_values.append(item.data_bits)
        return new_values
    
    @property
    def value_counts_diff(self) -> Dict[int, Counter]:
        counts_diff = defaultdict(Counter)
        for item in self.split_items:
            value_length = len(item.data_bits)
            counts_diff[value_length].update({ item.data_bits : 1 })
        return counts_diff
    
    @property
    def input_split_length(self) -> List[int]:
        return len(self.input_split_bits)
    
    @property
    def output_split_length(self) -> List[int]:
        return len(self.output_length_bits) + len(self.output_item_id_bits)
    
    @property
    def item_id_lengths(self) -> List[int]:
        item_id_lengths = list()
        for split_item in self.split_items:
            item_id_lengths.append(split_item.dict_value.item_id_length)
        return item_id_lengths
    
    @property
    def item_ids(self) -> List[int]:
        item_ids = list()
        for split_item in self.split_items:
            item_ids.append(split_item.dict_value.item_id)
        return item_ids


@dataclass()
class VarintOptions:
    prefix_length  : int                  = field(default=DEFAULT_PREFIX_LENGTH)
    start_length   : int                  = field(default=DEFAULT_START_LENGTH)
    end_length     : int                  = field(default=None)
    length_codes   : CanonicalHuffmanDict = field(init=False, repr=False)
    value_ranges   : List[range]          = field(init=False, default=None)
    item_id_ranges : List[range]          = field(init=False, default=None)

    def __init__(self, prefix_length: int=DEFAULT_PREFIX_LENGTH, start_length: int=DEFAULT_START_LENGTH, end_length: int|None=None):
        self.value_ranges   = []
        self.item_id_ranges = []
        self.prefix_length  = prefix_length 
        self.start_length   = start_length
        if (end_length is None):
            self.end_length = self.start_length + 2 ** self.prefix_length
        else:
            raise Exception(f"Not implemented")
        # init codes for different id lengths
        self.length_codes = create_canonical_codes(values=list(range(self.start_length, self.end_length)))
        # init available number and item_id ranges
        self.init_value_ranges()
        self.init_item_id_ranges()
    
    def get_range_id_by_value_length(self, value_length: int) -> int:
        return self.get_length_id_by_value_length(value=value_length)
    
    def get_value_length_by_range_id(self, range_id: int) -> int:
        return self.length_codes.values[range_id]
    
    def get_item_id_length_by_range_id(self, range_id: int) -> int:
        value_length = self.get_value_length_by_range_id(range_id=range_id)
        return self.get_item_id_length_by_value_length(value_length=value_length)
    
    def get_range_id_by_item_id_length(self, id_length: int) -> int:
        value_length = self.get_value_length_by_item_id_length(id_length=id_length)
        return self.get_range_id_by_value_length(value=value_length)
    
    def get_all_item_id_lengths(self) -> List[int]:
        return self.length_codes.values
    
    def get_all_value_lengths(self) -> List[int]:
        value_lengths = list()
        id_lengths    = self.get_all_item_id_lengths()
        for id_length in id_lengths:
            value_length = self.get_value_length_by_item_id_length(id_length=id_length)
            value_lengths.append(value_length)
        return value_lengths
    
    def get_value_length_by_item_id_length(self, id_length: int) -> int:
        return (self.prefix_length + id_length) + 1
    
    def get_item_id_length_by_value_length(self, value_length: int) -> int:
        return (value_length - self.prefix_length) - 1
    
    def init_item_id_ranges(self) -> List[Tuple[int, int]]:
        max_value  = 0
        for item_id_length in self.get_all_item_id_lengths():
            min_value = max_value
            max_value = min_value + (2 ** item_id_length)
            self.item_id_ranges.append(range(min_value, max_value))
    
    def init_value_ranges(self) -> List[Tuple[int, int]]:
        max_value  = 0 #2 ** self.get_min_value_length() #self.start_length
        for value_length in self.get_all_value_lengths(): #range(self.start_length, self.end_length):
            min_value = max_value
            max_value = min_value + (2 ** value_length)
            self.value_ranges.append(range(min_value, max_value))
    
    def get_length_id_by_value_length(self, value_length: int) -> int:
        return self.length_codes.get_value_id(value=value_length)
    
    def get_prefix_code_by_value_length(self, value_length: int) -> bitarray:
        return self.length_codes.get_value_code(value=value_length)
    
    def get_value_length_by_prefix_code(self, prefix_code: bitarray) -> int:
        return self.length_codes.get_code_value(code=prefix_code)
    
    def get_min_value_length(self) -> int:
        return min(self.get_all_value_lengths())
    
    def get_max_value_length(self) -> int:
        return max(self.get_all_value_lengths())
    
    def get_min_item_id_length(self) -> int:
        return min(self.get_all_item_id_lengths())
    
    def get_max_item_id_length(self) -> int:
        return max(self.get_all_item_id_lengths())
    
    def get_full_encoded_length(self, value_length: int) -> int:
        return self.prefix_length + value_length
    
    def find_length_id_for_value(self, value: int) -> int:
        # value range ids are same as value length ids
        for value_range_id in range (0, len(self.value_ranges)):
            if (value in self.value_ranges[value_range_id]):
                return value_range_id
        raise Exception(f"value={value} not found in ranges: {self.value_ranges}")
    
    def get_value_length_for_value(self, value: int) -> int:
        length_id = self.find_length_id_for_value(value=value)
        return self.length_codes.values[length_id]
    
    def get_prefix_code_for_value(self, value: int) -> int:
        value_length = self.get_value_length_for_value(value=value)
        return self.get_prefix_code_by_value_length(value_length=value_length)

@dataclass()
class VarintDataLayerEncoder:
    data_layer : VarintDataLayer
    options    : VarintOptions
    
    @property
    def item_length_sequence_bits(self) -> frozenbitarray:
        sequence_bits = bitarray(endian=DEFAULT_ENDIAN)
        #for item_id_length in self.data_layer.item_id_lengths:
        for position in range(0, self.data_layer.total_items):
            #item_length      = self.data_layer.item_id_pairs[position][0]
            #item_length      = self.data_layer.item_id_lengths[position] + self.options.start_length
            #normalized_length = item_id_length + self.options.start_length
            #print(normalized_length)
            item_length       = self.data_layer.item_id_lengths[position]
            encoded_length    = int2ba(item_length, length=self.options.prefix_length, endian=DEFAULT_ENDIAN, signed=False)
            sequence_bits    += encoded_length
        return sequence_bits
    
    @property
    def item_id_sequence_bits(self) -> frozenbitarray:
        sequence_bits = bitarray(endian=DEFAULT_ENDIAN)
        for position in range(0, self.data_layer.total_items):
            #item_length    = self.data_layer.item_id_pairs[position][0]
            #item_id        = self.data_layer.item_id_pairs[position][1]
            item_length    = self.data_layer.item_id_lengths[position]
            item_id        = self.data_layer.item_ids[position]
            encoded_id     = int2ba(item_id, length=item_length, endian=DEFAULT_ENDIAN)
            sequence_bits += encoded_id.copy()
        return sequence_bits
    
    @property
    def metadata_bits(self) -> frozenbitarray:
        layer_id_bits    = int2ba(self.data_layer.layer_id, length=LAYER_ID_PREFIX_LENGTH, endian=DEFAULT_ENDIAN)
        total_items_bits = int2ba(self.data_layer.total_items, length=TOTAL_ITEMS_PREFIX_LENGTH, endian=DEFAULT_ENDIAN)
        tail_length_bits = int2ba(self.data_layer.tail_length, length=TAIL_LENGTH_PREFIX_LENGTH, endian=DEFAULT_ENDIAN)
        
        metadata_bits  = bitarray(endian=DEFAULT_ENDIAN)
        metadata_bits += layer_id_bits
        metadata_bits += total_items_bits
        metadata_bits += tail_length_bits
        
        return frozenbitarray(metadata_bits)

    @property
    def data_bits(self) -> frozenbitarray:
        data_bits = bitarray(endian=DEFAULT_ENDIAN)
        data_bits += self.metadata_bits
        data_bits += self.item_length_sequence_bits
        data_bits += self.item_id_sequence_bits
        if (self.data_layer.tail_length > 0):
            data_bits += self.data_layer.tail_bits
        return frozenbitarray(data_bits)

    @property
    def data_length(self) -> int:
        return len(self.data_bits)
    
    def encode(self) -> frozenbitarray:
        return self.data_bits

@dataclass()
class VarintDataLayerDecoder:
    data_bits : frozenbitarray
    options   : VarintOptions

    @property
    def data_length(self) -> int:
        return len(self.data_bits)
    
    @property
    def header_length(self) -> int:
        header_length  = 0
        header_length += LAYER_ID_PREFIX_LENGTH
        header_length += TOTAL_ITEMS_PREFIX_LENGTH
        header_length += TAIL_LENGTH_PREFIX_LENGTH
        return header_length
    
    @property
    def metadata_bits(self) -> frozenbitarray:
        start = 0
        end   = start + self.header_length
        return self.data_bits[start:end]
    
    @property
    def layer_id(self) -> int:
        start = 0
        end   = start + LAYER_ID_PREFIX_LENGTH
        return ba2int(self.metadata_bits[start:end])
    
    @property
    def total_items(self) -> int:
        start = LAYER_ID_PREFIX_LENGTH
        end   = start + TOTAL_ITEMS_PREFIX_LENGTH
        return ba2int(self.metadata_bits[start:end])
    
    @property
    def tail_length(self) -> int:
        start = LAYER_ID_PREFIX_LENGTH + TOTAL_ITEMS_PREFIX_LENGTH
        end   = start + TAIL_LENGTH_PREFIX_LENGTH
        return ba2int(self.metadata_bits[start:end])
    
    @property
    def item_length_sequence_bits(self) -> frozenbitarray:
        start = self.header_length
        end   = start + (self.total_items * self.options.prefix_length)
        return self.data_bits[start:end]
    
    @property
    def item_id_lengths(self) -> List[int]:
        item_id_lengths = list()
        sequence_bits   = self.item_length_sequence_bits
        for item_id in range(0, self.total_items):
            start          = item_id * self.options.prefix_length
            end            = start + self.options.prefix_length
            length_bits    = sequence_bits[start:end]
            item_id_length = ba2int(length_bits)
            item_id_lengths.append(item_id_length)
        #print(f"{item_id_lengths} ({(len(item_id_lengths))})")
        return item_id_lengths
    
    @property
    def item_id_sequence_bits(self) -> frozenbitarray:
        item_id_lengths        = self.item_id_lengths
        item_id_section_length = sum(item_id_lengths)
        start = self.header_length + (self.total_items * self.options.prefix_length)
        end   = start + item_id_section_length #self.data_length - self.tail_length
        return self.data_bits[start:end]
    
    @property
    def item_ids(self) -> List[int]:
        item_ids        = list()
        item_id_lengths = self.item_id_lengths
        sequence_bits   = self.item_id_sequence_bits
        item_start      = 0
        #prev_item_end   = item_start
        #print(sum(item_id_lengths))
        #print(f"{item_id_lengths}")
        for item_id_length in item_id_lengths:
            item_end     = item_start + item_id_length
            item_id_bits = sequence_bits[item_start:item_end]
            #print(item_id_length, item_start, item_end, len(sequence_bits))
            if (item_id_length == 0):
                print(f"item_start={item_start}, item_end={item_end}, item_id_bits={item_id_bits.to01()} ({len(item_id_bits)})")
                item_ids.append(None)
                continue
            else:
                item_id = ba2int(item_id_bits)
                item_ids.append(item_id)
            item_start = item_end #item_start + item_id_length
        return item_ids

    @property
    def tail_bits(self) -> frozenbitarray:
        if (self.tail_length == 0):
            return frozenbitarray(endian=DEFAULT_ENDIAN)
        start = self.data_length - self.tail_length
        end   = self.data_length
        return self.data_bits[start:end]
    
    def decode(self) -> VarintDataLayer:
        return VarintDataLayer(
            layer_id        = self.layer_id,
            total_items     = self.total_items,
            tail_length     = self.tail_length,
            item_id_lengths = self.item_id_lengths,
            item_ids        = self.item_ids,
            tail_bits       = self.tail_bits,
        )

def compress_layer(layer_id: int, layer_bits: frozenbitarray, data_dict: VarintDataDict) -> frozenbitarray:
    split_result  = data_dict.preview_split_result(data=layer_bits, layer_id=layer_id)
    #next_layer_id = layer_id + 1
    data_layer    = split_result.data_layer #data_layer_from_split_result(layer_id=next_layer_id, split_result=split_result)
    encoder       = VarintDataLayerEncoder(data_layer=data_layer, options=data_dict.options)
    data_dict.apply_split_result(split_result=split_result)
    #print(f"Encoded layer:")
    #pprint(data_layer, max_length=12)
    
    return encoder.encode()

def restore_layer(layer_bits: frozenbitarray, data_dict: VarintDataDict) -> frozenbitarray:
    decoder    = VarintDataLayerDecoder(data_bits=layer_bits, options=data_dict.options)
    data_layer = decoder.decode()
    layer_bits = bitarray(endian=DEFAULT_ENDIAN)
    #print(f"Decoded layer:")
    #pprint(data_layer, max_length=12)

    for i in range(0, data_layer.total_items):
        value_item_length = data_layer.item_id_lengths[i]
        value_item_id     = data_layer.item_ids[i]
        value_length      = value_item_length + data_dict.options.prefix_length + data_dict.options.start_length
        value_bits        = data_dict.get_value(value_length=value_length, value_id=value_item_id) #value_item.item_bits
        layer_bits       += value_bits
    if (data_layer.tail_length > 0):
        layer_bits += data_layer.tail_bits
    
    return frozenbitarray(layer_bits)  
    
@dataclass()
class VarintNumber:
    options        : VarintOptions = field(repr=False)
    value          : int           = field()
    value_bits     : bitarray      = field()
    value_length   : int           = field()
    position       : int           = field() # index inside value range (last bits when encoded)

    def __init__(self, init_value: int|bitarray, options: VarintOptions):
        self.options = options
        if isinstance(init_value, int):
            self.encode_value(input_number=init_value)
        elif isinstance(init_value, bitarray):
            self.decode_value(input_bits=init_value)
        else:
            raise Exception(f"value={init_value}: incorrect type")

    def get_prefix_bits(self) -> bitarray:
        return self.value_bits[0:self.options.prefix_length]
    
    def get_prefix_value(self) -> bitarray:
        prefix_code = self.get_prefix_bits()
        return self.options.get_value_length_by_prefix_code(prefix_code=prefix_code)
    
    def get_position_bits(self) -> bitarray:
        return self.value_bits[self.get_prefix_value():len(self.value_bits)]
    
    def get_position_value(self) -> int:
        item_id_bits = self.get_position_bits()
        return ba2int(item_id_bits, signed=False)

    def encode_value(self, input_number: int) -> bitarray:
        pass

    def decode_value(self, input_bits: int) -> bitarray:
        pass

def get_number_offset(number: int, bit_length: int|None=None) -> int:
    if (bit_length is None):
        number_length = number.bit_length()
    else:
        number_length = bit_length
    if (number_length == 0):
        return 1
    offset = 0
    for i in range(0, number_length):
        offset += 2**i
    return offset

@lru_cache()
def create_number_ranges(start_number_length: int, end_number_length: int) -> List[Tuple[int, int]]:
    ranges    = []
    max_value = 2 ** start_number_length
    for number_length in range(start_number_length, end_number_length):
        min_value = max_value
        max_value = min_value + 2**number_length
        ranges.append((min_value, max_value))
    return ranges

NUMBER_LENGTHS = [nl for nl in range(0, 2**DEFAULT_PREFIX_LENGTH)]
NUMBER_RANGES  = create_number_ranges(start_number_length=0, end_number_length=2**DEFAULT_PREFIX_LENGTH)

def get_number_range(number: int, ranges: List[Tuple[int, int]]=NUMBER_RANGES) -> Tuple[int, Tuple[int, int]]:
    #ranges          = create_number_ranges(max_length=max_length)
    range_id        = 0
    target_range_id = None
    target_start    = None
    target_end      = None
    
    for start, end in ranges:
        if (number >= start) and (number < end):
            target_range_id = range_id
            target_start    = start
            target_end      = end
            break
        else:
            range_id += 1
    
    if (target_range_id is None):
        raise Exception(f"number={number} range not found (start={start}, end={end})")
    
    return (target_range_id, (target_start, target_end))

def to_relative_number(number: int) -> int:
    number_range = get_number_range(number=number)
    offset       = number_range[1][0]
    return number - offset

def to_absolute_number(number: int, bit_length: int, ranges: List[Tuple[int, int]]=NUMBER_RANGES) -> int:
    number_range = ranges[bit_length]
    offset       = number_range[0]
    return offset + number

def get_bit_length_from_range(number: int, ranges: List[Tuple[int, int]]=NUMBER_RANGES) -> int:
    number_range = get_number_range(number=number, ranges=ranges)
    bit_length   = number_range[0]
    return bit_length

def encode_prefix(value: int, prefix_length: int, endian: str, signed: bool=False) -> bitarray:
    return int2ba(value, length=prefix_length, endian=endian, signed=signed)

def decode_prefix(encoded_prefix: bitarray, prefix_length: int, signed: bool=False) -> int:
    if (len(encoded_prefix) < prefix_length):
        raise Exception(f"encoded_prefix={encoded_prefix} (l={len(encoded_prefix)}) is too short (target prefix_length={prefix_length})")
    return ba2int(encoded_prefix[0:prefix_length], signed=signed)

def encode_relative_number(number: int, bit_length: int, endian: str, signed: bool=False) -> bitarray:
    if (bit_length == 0):
        return bitarray('', endian=endian)
    return int2ba(number, length=bit_length, endian=endian, signed=signed)

def decode_relative_number(encoded_number: bitarray, bit_length: int, signed: bool=False) -> int:
    if (bit_length == 0):
        return 0
    if (len(encoded_number) < bit_length):
        raise Exception(f"encoded_number={encoded_number} (l={len(encoded_number)}) is too short (target bit_length={bit_length})")
    return ba2int(encoded_number[0:bit_length], signed=signed)

def encode_number(number: int, prefix_length: int, endian: str) -> bitarray:
    number_range    = get_number_range(number=number, ranges=NUMBER_RANGES)
    bit_length      = number_range[0]
    relative_number = to_relative_number(number=number)
    encoded_prefix  = encode_prefix(value=bit_length, prefix_length=prefix_length, endian=endian)
    encoded_number  = encode_relative_number(number=relative_number, bit_length=bit_length, endian=endian)

    return encoded_prefix + encoded_number

def decode_number(encoded_number: bitarray, prefix_length: int) -> int:
    prefix_bits     = encoded_number[0:prefix_length]
    number_length   = decode_prefix(encoded_prefix=prefix_bits, prefix_length=prefix_length)
    number_bits     = encoded_number[prefix_length:prefix_length+number_length]
    relative_number = decode_relative_number(encoded_number=number_bits, bit_length=number_length)
    decoded_number  = to_absolute_number(number=relative_number, bit_length=number_length)

    return decoded_number

def encode_varint_number(number: int, prefix_length: int, endian: str) -> bitarray:
    return encode_number(number=number, prefix_length=prefix_length, endian=endian)

def decode_varint_number(encoded_number: bitarray, prefix_length: int) -> int:
    return decode_number(encoded_number=encoded_number, prefix_length=prefix_length)

