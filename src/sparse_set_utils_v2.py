# https://stackoverflow.com/questions/38340808/recursive-typing-in-python-3-5
from __future__ import annotations
from rich import print
from rich.pretty import pprint
#from rich.live import Live
#from rich.panel import Panel
#from rich.table import Table, Column
#from rich.progress import track, TaskID,\
#    BarColumn, Progress, Task, TaskID, TextColumn, TimeElapsedColumn, TimeRemainingColumn, TaskProgressColumn,\
#    MofNCompleteColumn, RenderableColumn, SpinnerColumn, TransferSpeedColumn, FileSizeColumn, ProgressColumn
#from rich.layout import Layout
#from rich.columns import Columns
#from rich.text import Text
from custom_counter import CustomCounter as Counter
#from tqdm import tqdm
from bitarray.util import ba2int, int2ba, canonical_huffman, huffman_code
from bitarray import bitarray, frozenbitarray
from collections import defaultdict, namedtuple
from dataclasses import dataclass, field
from copy import deepcopy, copy
from typing import List, Set, Dict, Tuple, Optional, Union, Iterable, Generator, Generic, Optional, Protocol,\
    TypeVar, FrozenSet, Hashable, Sequence
from functools import lru_cache
import xxhash
#import math
from bitarrayset.binaryarrayset import BinaryArraySet
from enum import IntEnum
from sortedcontainers import SortedSet, SortedDict, SortedList
# pip install git+https://github.com/djbpitt/pyskiplist.git@dumpNodes 
from pyskiplist import SkipList

from fib_encoder import from_c1_bits, to_c1_bits, get_encoded_c1_length, get_encoded_c1_bits

INPUT_NOUNCE_BYTES = 4

class ItemValueType(IntEnum):
    SEED     = 0
    NOUNCE   = 1
    # common dict of all previously used new_value_id values (int)
    # new value ids generated using class InvertedValueRange
    VALUE_ID = 2
    
class ItemIdType(IntEnum):
    NO_ID_MARKER  = 0
    OLD_ID        = 1
    NEW_ID_MARKER = 2
    NEW_ID        = 3

# several different ways to add new items to data dictionaries - this approach allows minimize overhead 
# of variable-length encoding and maximize value re-use
# every new value id created for seed list or seed nounce list will be registered in NewValueIdEncoder
# and can be used again to extend for any other id list
VALUE_ID_FORMATS = [
    # not creating new values forNewValueIdEncoder - just get existing id and continue to next seed or nounce
    (ItemIdType.OLD_ID),
    # single value id used to create new value id: for example we extend our seed collection by adding new seed,
    # and we will use OLD_VALUE_ID code from global dictionary (NewValueId) and apply this id to seed's collection
    # (InvertedValueRange) as InvertedValueRange.value_range[new_seed_id]
    (ItemIdType.NO_ID_MARKER, ItemIdType.OLD_ID),
    # absolutely new value - this option is used when required id cannot be represented with previously described formats
    # new id encoded as C1 fibonacci code, big endian
    (ItemIdType.NO_ID_MARKER, ItemIdType.NEW_ID_MARKER, ItemIdType.NEW_ID),
]

NEW_VALUE_TYPES = {
    ItemValueType.SEED : (
        'old_seed_id',
        'new_seed_id_from_old_value_id',
        'new_seed_id_from_new_value_id',
    ),
    ItemValueType.NOUNCE : (
        'old_nounce_id',
        'new_nounce_id_from_old_value_id',
        'new_nounce_id_from_new_value_id',
        # always equals 1: all nounce_id lists, created for new seed will have this single default value in dictionary
        # this will happen automatically, and those default items/codes will not be encoded and saved to output stream
        # (this is a standard behavior)
        'new_nounce_id_from_new_seed',
    ),
    ItemValueType.VALUE_ID : (
        'old_value_id',
        'new_value_id_from_old_value_id_pair',
        'new_value_id_from_old_value_id',
        'new_value_id_from_old_value_id_and_encoded_number',
        'new_value_id_from_encoded_number',
    ),
}

class SkipDict(SkipList):

    def has_value(self, value) -> bool:
        return (value in set(self.values()))
    
    def has_key(self, key) -> bool:
        return (key in set(self.keys()))
    
    def has_index(self, index) -> bool:
        return (index < self.__len__())

    def insert(self, key, value):
        """
        Insert a key-value pair in the list.
        Restrict insertion of existing key/value pairs
        """
        if (self.__contains__(key=key) is True):
            current_value = self.search(key=key)
            if (current_value == value):
                # item already in set
                return
            if (current_value is not None):
                raise Exception(f"Cannot add new item: key={key}, value={value}: this key already contains value={current_value}")
        #if (self.has_value(value=value) is True):
        #    raise Exception(f"Cannot add new item: key={key}, value={value}: value already exists inside set")
        super().insert(key=key, value=value)

@dataclass
class InvertedValueRange:
    skipped_values : SkipDict      = field(default_factory=SkipDict)
    min_new_values : int           = field(default=16)
    start_range    : int           = field(default=0)
    end_range      : int           = field(default=None)
    value_range    : Sequence[int] = field(default=None, init=False)

    def __post_init__(self):
        if (self.end_range is None):
            self.end_range = max(self.skipped_values.values()) + self.min_new_values + 1
        self.value_range = range(self.start_range, self.end_range)

    def get_remaining_values(self) -> Dict[int, int]:
        new_values    = dict()
        # 0 is a required system element: "no_id" marker
        new_values[0] = 0
        new_value_id  = 1
        for value_index, value in enumerate(self.value_range):
            if (value in set(self.skipped_values.values())):
                continue
            new_values[new_value_id] = value
            new_value_id += 1
        return new_values

@dataclass
class NewValueIdEncoder:
    id_counts             : Counter                     = field(default_factory=Counter, init=False)
    registered_ids        : Set[int]                    = field(default_factory=SortedSet, init=False)
    id_codes              : Dict[int, bitarray]         = field(default_factory=SortedDict, init=False)
    id_code_lengths       : Dict[int, int]              = field(default_factory=SortedDict, init=False)
    code_lengths          : Set[int]                    = field(default_factory=SortedSet, init=False)
    zero_element          : Union[int, Tuple[int, int]] = field(default=0, init=False)
    zero_element_id       : Union[int, Tuple[int, int]] = field(default=0, init=False)
    zero_element_code     : Union[int, bitarray]        = field(default=None, init=False)
    zero_element_length   : int                         = field(default=1, init=False)
    code_length_counts    : Counter                     = field(default_factory=Counter, init=False)
    version               : int                         = field(default=0, init=False)
    default_endian        : str                         = field(default='big', init=False, repr=False)

    def get_next_value_id(self) -> int:
        return len(self.id_counts)
    
    def has_value_id(self, value_id: int) -> bool:
        return (value_id in self.registered_ids)
    
    def update_version(self):
        self.version += 1
    
    def update_registered_ids(self):
        self.registered_ids.clear()
        self.registered_ids = SortedSet(self.id_counts.keys())
    
    def update_code_lengths(self):
        self.id_code_lengths.clear()
        self.code_lengths.clear()
        self.code_length_counts.clear()
        for value_id, value_code in self.id_codes.items():
            code_length                    = len(value_code)
            self.id_code_lengths[value_id] = code_length
            self.code_lengths.add(code_length)
            self.code_length_counts.update({ code_length : 1 })
        self.zero_element_length = len(self.id_codes[self.zero_element_id])
    
    def update_id_codes(self):
        self.id_codes.clear()
        self.id_codes = SortedDict(huffman_code(self.id_counts, endian=self.default_endian).items())
        self.zero_element_code = self.id_codes[self.zero_element_id]
        self.update_code_lengths()
        self.update_registered_ids()
        self.update_version()
    
    def get_next_code_length(self, value_id: int=None) -> int:
        if (value_id is None):
            value_id = self.get_next_value_id()
        next_id_counts = self.id_counts.copy()
        next_id_counts.update({ self.zero_element_id : 1 })
        next_id_counts.update({ value_id : 1 })
        next_codes = SortedDict(huffman_code(next_id_counts, endian=self.default_endian).items())
        return len(next_codes[value_id])
    
    def get_value_id_code(self, value_id: int) -> int:
        if (value_id in self.registered_ids):
            value_code = self.id_codes[value_id]
        else:
            raise Exception(f"No code for value_id={value_id}")
        return value_code
    
    def get_code_length(self, value_id: int) -> int:
        if (value_id in self.registered_ids):
            value_length = self.id_code_lengths[value_id]
        else:
            value_length = self.get_next_code_length()
        return value_length
    
    def get_min_code_length(self) -> int:
        return min(self.sorted_code_lengths)
    
    def get_max_code_length(self) -> int:
        return max(self.sorted_code_lengths)
    
    def __post_init__(self):
        self.id_counts.update({ self.zero_element_id : 1 })
        self.update_id_codes()
    
    def register_new_value_id(self, value_id: int):
        if (value_id in self.registered_ids):
            raise Exception(f"value_id={value_id} already registered")
        self.id_counts.update({ self.zero_element_id : 1 })
        self.update_id_codes()
        self.id_counts.update({ value_id : 1 })
        self.update_id_codes()
    
    def update_existing_value_id(self, value_id: int):
        if (value_id not in self.registered_ids):
            raise Exception(f"value_id={value_id} not registered")
        self.id_counts.update({ value_id : 1 })
        self.update_id_codes()

