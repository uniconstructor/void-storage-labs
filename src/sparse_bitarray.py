# https://github.com/ilanschnell/bitarray/tree/master/examples/sparse
import bz2
import gzip
import sys
from time import perf_counter
from collections import Counter
from itertools import islice, pairwise
from random import random, randrange
from bisect import bisect, bisect_left
from bitarray import bitarray
from bitarray.util import (
    zeros, urandom, intervals,
    serialize, deserialize,
    sc_encode, sc_decode,
    vl_encode, vl_decode,
)

class Common:

    def __repr__(self):
        return "SparseBitarray('%s')" % (''.join(str(v) for v in self))

    def pop(self, i = -1):
        if i < 0:
            i += len(self)
        res = self[i]
        del self[i]
        return res

    def remove(self, value):
        i = self.find(value)
        if i < 0:
            raise ValueError
        del self[i]

    def sort(self, reverse=False):
        if reverse:
            c1 = self.count(1)
            self[:c1:] = 1
            self[c1::] = 0
        else:
            c0 = self.count(0)
            self[:c0:] = 0
            self[c0::] = 1

    def _get_start_stop(self, key):
        if key.step not in (1, None):
            raise ValueError("only step = 1 allowed, got %r" % key)
        start = key.start
        if start is None:
            start = 0
        stop = key.stop
        if stop is None:
            stop = len(self)
        return start, stop

    def _adjust_index(self, i):
        n = len(self)
        if i < 0:
            i += n
            if i < 0:
                i = 0
        elif i > n:
            i = n
        return i


"""
Implementation of a sparse bitarray

Internally we store a list of positions at which a bit changes from
1 to 0 or vice versa.  Moreover, we start with bit 0, meaning that if the
first bit in the bitarray is 1 our list starts with posistion 0.
For example:

   bitarray('110011111000')

is represented as:

   flips:   [0, 2, 4, 9, 12]

The last element in the list is always the length of the bitarray, such that
an empty bitarray is represented as [0].
"""
class SparseBitarrayFlips(Common):

    def __init__(self, x = 0):
        if isinstance(x, int):
            self.flips = [x]  # bitarray with x zeros
        else:
            self.flips = [0]
            for v in x:
                self.append(int(v))

    def __len__(self):
        return self.flips[-1]

    def __getitem__(self, key):
        if isinstance(key, slice):
            start, stop = self._get_start_stop(key)
            if stop <= start:
                return SparseBitarrayFlips()

            i = bisect(self.flips, start)
            j = bisect_left(self.flips, stop)

            res = SparseBitarrayFlips()
            res.flips = [0] if i % 2 else []
            for k in range(i, j):
                res.flips.append(self.flips[k] - start)
            res.flips.append(stop - start)
            return res

        elif isinstance(key, int):
            if not 0 <= key < len(self):
                raise IndexError
            return bisect(self.flips, key) % 2

        else:
            raise TypeError

    def __setitem__(self, key, value):
        if isinstance(key, slice):
            start, stop = self._get_start_stop(key)
            if stop <= start:
                return

            i = bisect(self.flips, start)
            j = bisect_left(self.flips, stop)

            self.flips[i:j] = (
                ([] if i % 2 == value else [start]) +
                ([] if j % 2 == value else [stop])
            )

        elif isinstance(key, int):
            if not 0 <= key < len(self):
                raise IndexError
            p = bisect(self.flips, key)
            if p % 2 == value:
                return
            self.flips[p:p] = [key, key + 1]

        else:
            raise TypeError

        self._reduce()

    def __delitem__(self, key):
        if isinstance(key, slice):
            start, stop = self._get_start_stop(key)
            if stop <= start:
                return

            i = bisect(self.flips, start)
            j = bisect_left(self.flips, stop)

            size = stop - start
            for k in range(j, len(self.flips)):
                self.flips[k] -= size
            self.flips[i:j] = [start] if (j - i) % 2 else []

        elif isinstance(key, int):
            if not 0 <= key < len(self):
                raise IndexError
            p = bisect(self.flips, key)
            for j in range(p, len(self.flips)):
                self.flips[j] -= 1

        else:
            raise TypeError

        self._reduce()

    def _reduce(self):
        n = self.flips[-1]      # length of bitarray
        lst = []                # new representation list
        i = 0
        while True:
            c = self.flips[i]   # current element (at index i)
            if c == n:          # element with bitarray length reached
                break
            j = i + 1           # find next value (at index j)
            while self.flips[j] == c:
                j += 1
            if (j - i) % 2:     # only append index if repeated odd times
                lst.append(c)
            i = j
        lst.append(n)
        self.flips = lst

    def _intervals(self):
        v = 0
        start = 0
        for stop in self.flips:
            yield v, start, stop
            v = 1 - v
            start = stop

    def append(self, value):
        if value == len(self.flips) % 2:  # opposite value as last element
            self.flips.append(len(self) + 1)
        else:                             # same value as last element
            self.flips[-1] += 1

    def extend(self, other):
        n = len(self)
        m = len(other.flips)
        if len(self.flips) % 2:
            self.flips.append(n)
        for i in range(m):
            self.flips.append(other.flips[i] + n)
        self._reduce()

    def find(self, value):
        if len(self) == 0:
            return -1
        flips = self.flips
        if value:
            return -1 if len(flips) == 1 else flips[0]
        else:
            if flips[0] > 0:
                return 0
            return -1 if len(flips) == 2 else flips[1]

    def to_bitarray(self):
        a = bitarray(len(self))
        for v, start, stop in self._intervals():
            a[start:stop] = v
        return a

    def invert(self):
        self.flips.insert(0, 0)
        self._reduce()

    def insert(self, i, value):
        i = self._adjust_index(i)
        p = bisect_left(self.flips, i)
        for j in range(p, len(self.flips)):
            self.flips[j] += 1
        self[i] = value

    def count(self, value=1):
        cnt = 0
        for v, start, stop in self._intervals():
            if v == value:
                cnt += stop - start
        return cnt

    def reverse(self):
        n = len(self)
        lst = [0] if len(self.flips) % 2 else []
        lst.extend(n - p for p in reversed(self.flips))
        lst.append(n)
        self.flips = lst
        self._reduce()

"""
Implementation of a sparse bitarray

For example:

   bitarray('110011111000')

is represented as:

   length:  11
   ones:    [0, 1, 4, 5, 6, 7, 8]
"""
class SparseBitarray(Common):

    def __init__(self, x = 0):
        if isinstance(x, int):
            self.n = x
            self.ones = []
        else:
            self.n = 0
            self.ones = []
            for v in x:
                self.append(int(v))

    def __len__(self):
        return self.n

    def __getitem__(self, key):
        if isinstance(key, slice):
            start, stop = self._get_start_stop(key)
            if stop <= start:
                return SparseBitarray()

            i = bisect_left(self.ones, start)
            j = bisect_left(self.ones, stop)
            res = SparseBitarray(stop - start)
            for k in range(i, j):
                res.ones.append(self.ones[k] - start)
            return res

        elif isinstance(key, int):
            if not 0 <= key < self.n:
                raise IndexError
            i = bisect_left(self.ones, key)
            return int(i != len(self.ones) and self.ones[i] == key)

        else:
            raise TypeError

    def __setitem__(self, key, value):
        if isinstance(key, slice):
            start, stop = self._get_start_stop(key)
            if stop <= start:
                return

            i = bisect_left(self.ones, start)
            j = bisect_left(self.ones, stop)
            del self.ones[i:j]
            if value == 0:
                return
            self.ones.extend((range(start, stop)))
            self.ones.sort()

        elif isinstance(key, int):
            if not 0 <= key < self.n:
                raise IndexError
            i = bisect_left(self.ones, key)
            if i != len(self.ones) and self.ones[i] == key:  # key present
                if value == 0:
                    del self.ones[i]
            else:  # key not present
                if value == 1:
                    self.ones.insert(i, key)

        else:
            raise TypeError

    def __delitem__(self, key):
        if isinstance(key, slice):
            start, stop = self._get_start_stop(key)
            if stop <= start:
                return

            i = bisect_left(self.ones, start)
            j = bisect_left(self.ones, stop)
            del self.ones[i:j]
            size = stop - start
            for k in range(i, len(self.ones)):
                self.ones[k] -= size
            self.n -= size

        elif isinstance(key, int):
            if not 0 <= key < len(self):
                raise IndexError
            i = bisect_left(self.ones, key)
            if i != len(self.ones) and self.ones[i] == key:
                del self.ones[i]
            for k in range(i, len(self.ones)):
                self.ones[k] -= 1
            self.n -= 1

        else:
            raise TypeError

    def append(self, value):
        if value:
            self.ones.append(self.n)
        self.n += 1

    def find(self, value):
        ones = self.ones
        if value:
            return ones[0] if ones else -1
        else:
            m = len(ones)
            if m == self.n:
                return -1
            for i in range(m):
                if ones[i] != i:
                    return i
            return m

    def extend(self, other):
        self.ones.extend(other.ones[i] + self.n for i in
                         range(len(other.ones)))
        self.n += other.n

    def to_bitarray(self):
        a = bitarray(self.n)
        a.setall(0)
        a[self.ones] = 1
        return a

    def insert(self, k, value):
        k = self._adjust_index(k)
        i = bisect_left(self.ones, k)
        for j in range(i, len(self.ones)):
            self.ones[j] += 1
        self.n += 1
        self[k] = value

    def invert(self):
        self.ones = sorted(set(range(self.n)) - set(self.ones))

    def count(self, value=1):
        if value:
            return len(self.ones)
        else:
            return self.n - len(self.ones)

    def reverse(self):
        lst = [self.n - i - 1 for i in self.ones]
        lst.reverse()
        self.ones = lst

# Useful functions related to sparse bitarray compression.
# In particular the function sc_stats() which returns the
# frequency of each block type.



def read_n(n, stream):
    i = 0
    for j in range(n):
        i |= next(stream) << 8 * j
    if i < 0:
        raise ValueError("read %d bytes got negative value: %d" % (n, i))
    return i

def sc_decode_header(stream):
    head = next(stream)
    if head & 0xe0:
        raise ValueError("invalid header: 0x%02x" % head)
    endian = 'big' if head & 0x10 else 'little'
    length = head & 0x0f
    nbits = read_n(length, stream)
    return endian, nbits

def sc_decode_block(stream, stats):
    head = next(stream)
    if head == 0:  # stop byte
        return False

    if head < 0xa0:
        n = 0
        k = head if head <= 32 else 32 * (head - 31)
    elif head < 0xc0:
        n = 1
        k = head - 0xa0
    elif 0xc2 <= head <= 0xc4:
        n = head - 0xc0
        k = next(stream)
    else:
        raise ValueError("Invalid block head: 0x%02x" % head)

    stats['blocks'][n] += 1

    # consume block data
    size = max(1, n) * k        # size of block data
    next(islice(stream, size, size), None)

    return True

def sc_stats(stream):
    """sc_stats(stream) -> dict

Decode a compressed byte stream (generated by `sc_encode()` and return
useful statistics.  In particular, the frequency of each block type.
"""
    stream = iter(stream)
    endian, nbits = sc_decode_header(stream)

    stats = {
        'endian': endian,
        'nbits': nbits,
        'blocks': Counter()
    }

    while sc_decode_block(stream, stats):
        pass

    stop = False
    try:
        next(stream)
    except StopIteration:
        stop = True
    assert stop

    return stats

def test_sc_stat():
    a = bitarray(1<<33, 'little')
    a.setall(0)
    a[:1<<16] = 1
    a[:1<<18:1<<4] = 1
    a[:1<<22:1<<12] = 1
    a[:1<<30:1<<20] = 1
    assert a.count() == 79804
    b = sc_encode(a)
    stat = sc_stats(b)
    assert stat['endian'] == 'little'
    assert stat['nbits'] == 1 << 33
    blocks = stat['blocks']
    for i, n in enumerate([2, 754, 46, 48, 1]):
        print("         block type %d  %8d" % (i, blocks[i]))
        assert blocks[i] == n
    if sys.version_info[:2] >= (3, 10):
        print("total number of blocks %8d" % blocks.total())
    assert a == sc_decode(b)

def random_array(n, p=0.5):
    """random_array(n, p=0.5) -> bitarray

Generate random bitarray of length n.
Each bit has a probability p of being 1.
"""
    if p < 0.05:
        # XXX what happens for small N?  N=0 crashes right now.
        # when the probability p is small, it is faster to randomly
        # set p * n elements
        a = zeros(n)
        for _ in range(int(p * n)):
            a[randrange(n)] = 1
        return a

    return bitarray((random() < p for _ in range(n)))

def test_random_array():
    n = 10_000_000
    p = 1e-6
    while p < 1.0:
        a = random_array(n, p)
        cnt = a.count()
        print("%10.7f  %10.7f  %10.7f" % (p, cnt / n, abs(p - cnt / n)))
        p *= 1.4

def p_range():
    n = 1 << 28
    p = 1e-8
    print("        p          ratio         raw"
          "    type 1    type 2    type 3    type 4")
    print("   " + 73 *'-')
    while p < 1.0:
        a = random_array(n, p)
        b = sc_encode(a)
        blocks = sc_stats(b)['blocks']
        print('  %11.8f  %11.8f  %8d  %8d  %8d  %8d  %8d' % (
            p, len(b) / (n / 8),
            blocks[0], blocks[1], blocks[2], blocks[3], blocks[4]))
        assert a == sc_decode(b)
        p *= 1.8

def compare():
    n = 1 << 26
    # create random bitarray with p = 1 / 2^9 = 1 / 512 = 0.195 %
    a = bitarray(n)
    a.setall(1)
    for i in range(10):
        a &= urandom(n)

    raw = a.tobytes()
    print(20 * ' ' +  "compress (ms)   decompress (ms)             ratio")
    print(70 * '-')
    for name, f_e, f_d in [
            ('serialize', serialize, deserialize),
            ('vl', vl_encode, vl_decode),
            ('sc' , sc_encode, sc_decode),
            ('gzip', gzip.compress, gzip.decompress),
            ('bz2', bz2.compress, bz2.decompress)]:
        x = a if name in ('serialize', 'vl', 'sc') else raw
        t0 = perf_counter()
        b = f_e(x)  # compression
        t1 = perf_counter()
        c = f_d(b)  # decompression
        t2 = perf_counter()
        print("    %-11s  %16.3f  %16.3f  %16.4f" %
              (name, 1000 * (t1 - t0), 1000 * (t2 - t1), len(b) / len(raw)))
        assert c == x

if __name__ == '__main__':
    test_sc_stat()
    #test_random_array()
    compare()
    p_range()