from rich.text import Text
from rich.progress import TaskProgressColumn
from typing import List, Set, Dict, Tuple, Optional, Union, Iterable, Sequence

class CustomTaskProgressColumn(TaskProgressColumn):

    @classmethod
    def render_speed(cls, speed: Optional[float]) -> Text:
        """Render the speed in iterations per second.

        Args:
            task (Task): A Task object.

        Returns:
            Text: Text object containing the task speed.
        """
        if speed is None:
            return Text("", style="progress.percentage")
        return Text(f"{speed:#n} it/s", style="progress.percentage")