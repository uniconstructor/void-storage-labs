# https://stackoverflow.com/questions/38340808/recursive-typing-in-python-3-5
from __future__ import annotations
from rich import print, filesize
from rich.pretty import pprint
from rich.live import Live
from rich.panel import Panel
from rich.table import Table, Column
from rich.progress import track, TaskID,\
    BarColumn, Progress, Task, TaskID, TextColumn, TimeElapsedColumn, TimeRemainingColumn, TaskProgressColumn,\
    MofNCompleteColumn, RenderableColumn, SpinnerColumn, TransferSpeedColumn, FileSizeColumn, ProgressColumn
from rich.layout import Layout
from rich.columns import Columns
from rich.text import Text
from custom_counter import CustomCounter as Counter
from tqdm import tqdm
from bitarray.util import ba2int, int2ba, huffman_code
from bitarray import bitarray, frozenbitarray
from collections import defaultdict, namedtuple
from dataclasses import dataclass, field
#from copy import deepcopy, copy
from typing import List, Set, Dict, Tuple, Optional, Union, Iterable
#from functools import lru_cache
import xxhash
from math import ceil
from bitarrayset.binaryarrayset import BinaryArraySet
#from mongoengine import register_connection
#register_connection('default', db='256_byte_segments', host='127.0.0.1', port=27017, maxPoolSize=300)
from mongoengine import Document, QuerySet, LongField, IntField, \
    BinaryField, StringField, ListField, SortedListField, BooleanField, queryset_manager

from fib_encoder import fib_encode_position, get_fib_lengths, get_fib_value_range, get_fib_position_ranges

class CompressSpeedColumn(ProgressColumn):
    """Renders human readable compression speed in bits."""

    def render(self, task: "Task") -> Text:
        """Show data transfer speed."""
        speed = task.finished_speed or task.speed
        if speed is None:
            return Text("?", style="progress.data.speed")
        data_speed = speed
        return Text(f"{data_speed:4.2f} bit/s", style="progress.data.speed")

### BASE CLASSES ###

class BaseEncodedDataItem(Document):
    id             = IntField(primary_key=True)
    seed_layer_id  = IntField(null=True, required=False)
    seed_length    = IntField(null=False, required=True)
    nounce         = IntField(null=True, required=False)
    value          = IntField(null=False, required=True)
    value_length   = IntField(null=False, required=True)
    data_position  = IntField(null=False, required=True)
    item_score     = IntField(null=False, required=True)
    init_suffix    = IntField(null=True, required=False)
    #file_name      = StringField(null=False, required=True)
    # collection metadata
    meta = {
        'abstract': True,
        'indexes': [
            'seed_layer_id',
            'seed_length',
            'nounce',
            'value',
            'value_length',
            'data_position',
            'item_score',
            #'init_suffix',
            #'file_name',
        ],
    }

### CACHE ###

CACHED_CLASSES     = dict()
CACHED_CLASS_NAMES = set()

def has_cached_class(class_name: str) -> bool:
    return (class_name in CACHED_CLASS_NAMES)

def get_cached_class(class_name: str) -> Document:
    return CACHED_CLASSES.get(class_name)

### COLLECTION FACTORIES ###

def get_encoded_data_item_class_name(encoder_type: str, start_max_value_length: int, expand_distance: int) -> str:
    return f"EncodedDataItem{encoder_type}SMVL{start_max_value_length:02}ED{expand_distance:02}"

def get_encoded_data_item_collection_name(encoder_type: str, start_max_value_length: int, expand_distance: int) -> str:
    return f"encoded_data_item_{encoder_type.lower()}_smvl{start_max_value_length:02}_ed{expand_distance:02}"

def make_encoded_data_item_value_class(encoder_type: str, start_max_value_length: int, expand_distance: int) -> BaseEncodedDataItem:
    return type(get_encoded_data_item_class_name(
            encoder_type           = encoder_type,
            start_max_value_length = start_max_value_length,
            expand_distance        = expand_distance
        ), (BaseEncodedDataItem, ), {
        "meta" : {
            'collection': get_encoded_data_item_collection_name(
                encoder_type           = encoder_type,
                start_max_value_length = start_max_value_length,
                expand_distance        = expand_distance
            ),
        }
    })

def get_encoded_data_item_value_class(encoder_type: str, start_max_value_length: int, expand_distance: int) -> Union[BaseEncodedDataItem, Document]:
    class_name = get_encoded_data_item_class_name(
        encoder_type           = encoder_type,
        start_max_value_length = start_max_value_length,
        expand_distance        = expand_distance
    )
    if (has_cached_class(class_name) is False):
        CACHED_CLASSES[class_name] = make_encoded_data_item_value_class(
                encoder_type           = encoder_type,
                start_max_value_length = start_max_value_length,
                expand_distance        = expand_distance
            )
        CACHED_CLASS_NAMES.add(class_name)
    return get_cached_class(class_name)

### END OF METACLASSES ###

# result generated by create_contant_based_split()
ContentBasedSplit = namedtuple('ContentBasedSplit', [
    # split result (list of items of different length)
    'split_items',
    # unique elements only (data dictionary)
    'data_values',
    'value_ids',
    'unique_values',
    # this flag is True when split process was not finished because all tier dictionaries are full
    'capacity_overflow',
    # total number of number of unique values, grouped by length
    'length_counts',
    # number of usages of each dict value
    'value_counts',
    # popularity of values for each value length
    'length_usage_counts',
    # length of the result list
    'encoded_items_count',
    # number of unique values in dict
    'unique_values_count',
    'seed_items_count',
    # (required to continue split process with several iterations, used when capacity_overflow=True)
    'position',
    'total_score',
    'bits_per_seed',
])

def create_length_limits(nounce_length_bits: int, default_score: int, shift_offset: Dict[int, int]=dict()) -> Dict[int, int]:
    limits            = dict()
    min_value_length  = nounce_length_bits + default_score
    max_length_ranges = 2**nounce_length_bits
    
    if (len(shift_offset) > 0):
        for offset, offset_items in shift_offset.items():
            if (offset < 0) and (offset_items > 0):
                limits[min_value_length + offset] = offset_items
            max_length_ranges -= offset_items
    
    for nl in range(0, max_length_ranges):
        nl_value_length = nounce_length_bits + nl + default_score
        limits[nl_value_length] = 2**nl
    
    if (len(shift_offset) > 0):
        for offset, offset_items in shift_offset.items():
            if (offset >= 0) and (offset_items > 0):
                limits[min_value_length + offset] += offset_items
    #print(f"max_items_by_length: {len(limits)}")
    #pprint(limits)
    return limits

def get_value_length_offset(nounce_length_bits: int, default_score: int, value_length: int, shift_offset: Dict[int, int]=None) -> int:
    limits = create_length_limits(
        nounce_length_bits=nounce_length_bits,
        default_score=default_score,
        shift_offset=shift_offset,
    )
    value_lengths = sorted(list(limits.keys()))
    offset        = 0
    if (value_length > max(value_lengths)) or (value_length < min(value_lengths)):
        raise Exception(f"Incorrect value_length={value_length}: default_score={default_score}, nounce_length_bits={nounce_length_bits}")
    for vl, length_items in limits.items():
        if (vl >= value_length):
            break
        offset += length_items
    return offset

def get_value_length_range(nounce_length_bits: int, default_score: int, value_length: int, shift_offset: Dict[int, int]=None) -> Tuple[int, int]:
    limits = create_length_limits(
        nounce_length_bits=nounce_length_bits,
        default_score=default_score,
        shift_offset=shift_offset,
    )
    min_value = get_value_length_offset(
        nounce_length_bits=nounce_length_bits,
        default_score=default_score,
        value_length=value_length,
        shift_offset=shift_offset,
    )
    max_value = min_value + limits[value_length]
    return (min_value, max_value)

def create_content_based_split(data: frozenbitarray, nounce_length_bits: int, default_score: int=1, shift_offset: Dict[int, int]=None) -> ContentBasedSplit:
    """
    Split data to short items (several byte each), each item value should be unique.
    Process goes from short to values. Number of items of each length is defined separatley (based on leb_128 encoding). 
    """
    # sets to True when all dictionaries are full, but there is still more unique data items to process
    capacity_overflow   = False
    # unique fragments of data
    data_values         = set()
    unique_values       = defaultdict(list)
    value_ids           = list()
    # oridinal data, splitted into items according our dictionary
    split_items         = list()
    # number of usages for each byte value
    value_counts        = Counter()
    length_usage_counts = Counter()
    # combined usage of dictionary values, grouped by item length
    length_counts       = Counter()
    position            = 0
    # set up maximum values of each length
    #fib_lengths         = get_fib_lengths(encoder_type='C2')
    max_items_by_length = create_length_limits(
        nounce_length_bits=nounce_length_bits,
        default_score=default_score,
        shift_offset=shift_offset,
    )
    value_lengths       = sorted(list(max_items_by_length.keys()))
    max_value_length    = max(value_lengths)
    seed_items_count    = 0

    # read data using variable length items
    while (True):
        item_value = None
        # length of next value depends on current dictionary state: short values collected first,
        # new values will be added only if we don't meet them previously
        for value_length in value_lengths: #range(min_value_length, 32 + 1):
            # try to read and use short values first, use long values only if we dont have short ones
            v_start    = position
            v_end      = position + value_length
            scan_value = data[v_start:v_end]
            if (scan_value not in data_values):
                # new (unique) value recieved
                if length_counts[value_length] >= max_items_by_length[value_length]:
                    # reached maximum capacity for values with given length
                    if (value_length >= max_value_length):
                        # maximum unique values recieved - dictionary overflow
                        capacity_overflow = True
                        break
                    else:
                        # use capacity from next length tier
                        continue
                else:
                    # add new value to dictionary, updating item counter (and decreace tier capacity by 1 value)
                    length_counts.update({value_length : 1})
                    length_usage_counts.update({ value_length: 1})
                    item_value = scan_value
                    position   += value_length
                    # first values will be set as bitmap and will have no seeds
                    if (value_length > (nounce_length_bits + default_score)):
                        seed_items_count += 1
                    break
            else:
                # existing (not unique) value recieved - do not modify our dictionary, but save item value to split result
                length_usage_counts.update({ value_length: 1})
                item_value = scan_value
                position  += value_length
                break
        if capacity_overflow == True:
            # input max_value_length is not enough to create a dictionary
            # raise Exception(f"Length capacity reached: {length_counts} (items_processed={len(split_items)}, max_value_length={max_value_length})")
            break
        # update usage counter of the dictionary value
        item_value_int = ba2int(item_value, signed=False)
        value_counts.update({(value_length, item_value_int) : 1})
        # append item value to final result
        split_items.append(item_value)
        # add item value to data dictionary
        data_values.add(item_value)
        # group int values by length
        if (item_value_int not in unique_values[value_length]):
            unique_values[value_length].append(item_value_int)
            value_ids.append((len(value_ids), value_length, item_value_int))
        if ((len(data) - position) <= max_value_length):
            # all data processed:
            # last item will be saved and returned in 'remaining_data' field inside result dict (if any)
            break
    #for v_item, v_count in value_counts.items():
    #    v_length = v_item[0]
    #    length_usage_counts.update({ v_length: 1})
    encoded_items_count = len(split_items)
    unique_values_count = len(data_values)
    total_score         = 0 #encoded_items_count * default_score
    #if (shift_offset < 0):
    for vl, vl_items in length_usage_counts.items():
        if (vl == nounce_length_bits):
            continue
        elif (vl > nounce_length_bits) and (vl < (nounce_length_bits + default_score)):
            total_score += vl_items * (vl - (nounce_length_bits + default_score))
        elif (vl >= (nounce_length_bits + default_score)):
            total_score += vl_items * default_score
        elif (vl < nounce_length_bits):
            raise Exception(f"nounce_length_bits{nounce_length_bits}, vl={vl}")
        #for vl in value_lengths:
        #    if (vl >= (nounce_length_bits + default_score)):
        #        break
        #    penalty      = default_score + shift_offset
        #    total_score += penalty * length_usage_counts[vl]
    bits_per_seed = total_score / seed_items_count
    # final result
    return ContentBasedSplit(
        split_items         = tuple(split_items),
        data_values         = data_values,
        value_ids           = value_ids,
        unique_values       = unique_values,
        capacity_overflow   = capacity_overflow,
        length_counts       = length_counts,
        value_counts        = value_counts,
        length_usage_counts = length_usage_counts,
        encoded_items_count = encoded_items_count,
        unique_values_count = unique_values_count,
        seed_items_count    = seed_items_count,
        position            = position,
        total_score         = total_score,
        bits_per_seed       = bits_per_seed,
    )

@dataclass
class SeedPrefixTree:
    init_max_value_length : int
    overall_progress      : Progress                       = field(init=False)
    overall_task          : TaskID                         = field(init=False)
    item_progress         : Progress                       = field(init=False)
    item_task             : TaskID                         = field(init=False)
    nounce_progress       : Progress                       = field(init=False)
    nounce_tasks          : Dict[int, TaskID]              = field(init=False, default_factory=dict)
    value_progress        : Progress                       = field(init=False)
    value_tasks           : Dict[int, TaskID]              = field(init=False, default_factory=dict)
    data                  : frozenbitarray                 = field(repr=False)
    data_info             : str                            = field(init=False)
    nounce_length_bits    : int                            = field(default=0)
    nounce_shift_size     : int                            = field(default=0)
    nounce_shift_offset   : Dict[int, int]                 = field(default_factory=dict)
    enable_expansion      : bool                           = field(default=False)
    expand_distance       : int                            = field(default=4)
    nounce_encoder        : str                            = field(default='C1')
    seed_encoder          : str                            = field(default='C2')
    max_value_length      : int                            = field(init=False)
    next_frozen_length    : int                            = field(init=False)
    default_score         : int                            = field(default=1)
    expansion_threshold   : int                            = field(default=1, init=False)
    data_position         : int                            = field(default=0, init=False)
    data_item_id          : int                            = field(default=1, init=False)
    total_score           : int                            = field(default=0, init=False)
    total_unique          : int                            = field(default=0, init=False)
    bits_per_item         : int                            = field(default=1, init=False)
    bits_per_seed         : int                            = field(default=1, init=False)
    max_seed              : int                            = field(default=0, init=False)
    max_seed_length       : int                            = field(default=0, init=False)
    seed_lengths          : Dict[int, int]                 = field(default_factory=dict, init=False)
    value_lengths         : List[int]                      = field(default_factory=list, init=False)
    sorted_value_lengths  : List[int]                      = field(default_factory=list, init=False)
    active_lengths        : List[int]                      = field(default_factory=list, init=False)
    sorted_active_lengths : List[int]                      = field(default_factory=list, init=False)
    frozen_lengths        : List[int]                      = field(default_factory=list, init=False)
    init_lengths          : List[int]                      = field(default_factory=list, init=False)
    max_items_by_length   : Dict[int, int]                 = field(default_factory=dict, init=False)
    value_limits          : Dict[int, int]                 = field(default_factory=dict, init=False)
    encoded_values        : Dict[int, Set[int]]            = field(default_factory=lambda : defaultdict(set), init=False)
    encoded_value_nounces : Dict[int, Dict[int, int]]      = field(default_factory=lambda : defaultdict(dict), init=False)
    encoded_nounce_seeds  : Dict[int, int]                 = field(default_factory=dict, init=False)
    target_nounces        : Dict[int, List[int]]           = field(default_factory=lambda : defaultdict(list), init=False)
    used_nounces          : Dict[int, Set[int]]            = field(default_factory=lambda : defaultdict(set), init=False)
    free_nounce_counts    : Counter                        = field(default_factory=Counter, init=False)
    used_nounce_counts    : Counter                        = field(default_factory=Counter, init=False)
    value_counts          : Counter                        = field(default_factory=Counter, init=False)
    seed_counts           : Counter                        = field(default_factory=Counter, init=False)
    seed_length_counts    : Counter                        = field(default_factory=Counter, init=False)
    nounce_length_counts  : Counter                        = field(default_factory=Counter, init=False)
    length_score_counts   : Counter                        = field(default_factory=Counter, init=False)
    prev_removed_counts   : Counter                        = field(default_factory=Counter, init=False)
    active_removed_counts : Counter                        = field(default_factory=Counter, init=False)
    seed_values           : Dict[int, Dict[int, Set[int]]] = field(default_factory=lambda : defaultdict(lambda : defaultdict(set)), init=False) # seed->value_length->value->nounce
    prev_seed_values      : Dict[int, Set[int]]            = field(default_factory=lambda : defaultdict(set), init=False)
    prev_seed_prefixes    : Dict[int, Dict[int, Set[int]]] = field(default_factory=lambda : defaultdict(lambda : defaultdict(set)), init=False)
    used_init_values      : Dict[int, Set[int]]            = field(default_factory=lambda : defaultdict(set), init=False)
    used_init_value_count : int                            = field(default=0, init=False)

    def init_nounce_progress(self):
        self.nounce_progress = Progress(
            "{task.description}",
            TextColumn("[dim white]{task.fields[layer_steps]}"),
            TextColumn("[cyan]{task.fields[layer_free]:9.6f}%"),
            BarColumn(),
            TaskProgressColumn(),
            MofNCompleteColumn(),
            TextColumn("[yellow]{task.fields[layer_score]}"),
            TextColumn("[dim yellow]{task.fields[layer_avg]:>2.3f}"),
            TextColumn("[green]{task.fields[layer_items]}"),
            TextColumn("[dim red]{task.fields[layer_removed]}"),
            TextColumn("[dim green]{task.fields[layer_avg_i]:>2.3f}"),
            TextColumn("[purple]{task.fields[layer_new]:9.5f}%"),
            TextColumn("[red]{task.fields[layer_used]:9.6f}%"),
            TextColumn("[dim white]{task.fields[layer_total]:9.5f}%"),
        )
        layer_total = 0
        layer_new   = 0
        for value_length, max_items in self.value_limits.items():
            if (value_length > self.init_max_value_length):
                break
            layer_steps  = 0
            layer_avg    = 0
            layer_score  = self.length_score_counts[value_length]
            layer_items  = self.nounce_length_counts[value_length]
            layer_avg_i  = 0
            if (self.used_nounce_counts[value_length] > 0):
                layer_avg   = (layer_score / self.used_nounce_counts[value_length])
                layer_avg_i = self.nounce_length_counts[value_length] / self.used_nounce_counts[value_length]
            layer_free   = ((self.free_nounce_counts[value_length] * (2**(32-value_length))) / 2**32) * 100
            layer_used   = ((self.used_nounce_counts[value_length] * (2**(32-value_length))) / 2**32) * 100
            layer_total += layer_used
            if (value_length not in self.init_lengths):
                layer_new += layer_free
                if (layer_new > 0):
                    layer_steps = int(100 / layer_new)
            self.nounce_tasks[value_length] = self.nounce_progress.add_task(
                f"{value_length}:",
                total         = max_items,
                layer_free    = layer_free,
                layer_used    = layer_used,
                layer_total   = layer_total,
                layer_new     = layer_new,
                layer_steps   = layer_steps,
                layer_score   = layer_score,
                layer_avg     = layer_avg,
                layer_items   = layer_items,
                layer_avg_i   = layer_avg_i,
                layer_removed = self.active_removed_counts[value_length],
            )
    
    def init_value_progress(self):
        self.value_progress = Progress(
            "{task.description}",
            TextColumn("{task.fields[unique_count]:7}"),
            BarColumn(),
            TextColumn("[green]{task.fields[layer_total]:6.4f}%"),
            TextColumn("[dim green]+{task.fields[layer_unique]:7.5f}%"),
            TextColumn("({task.fields[min_seed]}-{task.fields[max_seed]}) {task.fields[avg_seed]:2.2f}"),
            #TextColumn("[green]'{task.fields[target_value]}'"),
        )
        for value_length, max_items in self.value_limits.items():
            if (value_length > self.init_max_value_length):
                break
            self.value_tasks[value_length] = self.value_progress.add_task(
                f"{value_length}:",
                total        = 2**32,
                unique_count = 0,
                layer_total  = 0,
                min_seed     = 0,
                max_seed     = 0,
                avg_seed     = 0,
                target_value = '',
                layer_unique = 0,
            )

    def __post_init__(self):
        for offset, offset_items in self.nounce_shift_offset.items():
            self.nounce_shift_size += offset_items
        DataItemClass = get_encoded_data_item_value_class(
            encoder_type           = self.nounce_encoder, 
            start_max_value_length = self.init_max_value_length,
            expand_distance        = self.expand_distance
        )
        DataItemClass.drop_collection()
        self.max_value_length = self.init_max_value_length
        #self.value_limits     = get_fib_lengths(encoder_type=self.nounce_encoder)
        self.value_limits = create_length_limits(
            nounce_length_bits = self.nounce_length_bits,
            default_score      = self.default_score,
            shift_offset       = self.nounce_shift_offset,
        )
        
        for value_length, max_items in self.value_limits.items():
            self.value_lengths.append(value_length)
            self.active_lengths.append(value_length)
            if (value_length < (self.nounce_length_bits + self.default_score)):
                self.init_lengths.append(value_length)
            self.max_items_by_length[value_length] = max_items
            #min_nounce, max_nounce            = get_fib_value_range(value_length=(value_length - 1), encoder_type=self.nounce_encoder)
            min_nounce, max_nounce = get_value_length_range(
                nounce_length_bits = self.nounce_length_bits,
                default_score      = self.default_score,
                value_length       = value_length,
                shift_offset       = self.nounce_shift_offset,
            )
            nounce_range = range(min_nounce, max_nounce)
            self.target_nounces[value_length] = [n for n in nounce_range]
            self.free_nounce_counts.update({ value_length: len(self.target_nounces[value_length]) })
            
            if (value_length >= self.init_max_value_length):
                break
        self.sorted_value_lengths  = sorted(self.value_lengths)
        self.sorted_active_lengths = sorted(self.active_lengths)
        self.next_frozen_length    = min(self.active_lengths)

        if (len(self.value_lengths) > self.expand_distance):
            threshold_value_length   = self.max_value_length - self.expand_distance
            self.expansion_threshold = self.max_items_by_length[threshold_value_length]
        
        if (len(self.init_lengths) > 0):
            self.total_score -= self.nounce_length_bits #(2**self.nounce_length_bits - self.max_items_by_length[self.nounce_length_bits]) * self.nounce_length_bits + self.nounce_length_bits
            for init_length in self.init_lengths:
                self.value_lengths.remove(init_length)
                self.value_lengths.append(init_length)
                self.active_lengths.remove(init_length)
                self.active_lengths.append(init_length)

        if (self.data is None):
            raise Exception(f"No data provided")
        self.data_info = {
            'size_bits'  : len(self.data),
            'size_bytes' : (len(self.data) // 8),
        }

        self.overall_progress = Progress(
            TextColumn("{task.description}"),
            BarColumn(),
            TaskProgressColumn(text_format="[progress.percentage]{task.percentage:>7.4f}%", show_speed=True),
            CompressSpeedColumn(),
            TimeRemainingColumn(),
            TimeElapsedColumn(),
            MofNCompleteColumn(),
            TextColumn("sc: {task.fields[total_score]}"),
            TextColumn("it: {task.fields[total_items]}/{task.fields[total_unique]}"),
            TextColumn("et: {task.fields[expansion_items]}/{task.fields[expansion_threshold]}"),
            #TextColumn("bpi: {task.fields[bits_per_item]:2.4f}"),
            TextColumn("bps: {task.fields[bits_per_seed]:2.4f}"),
            TextColumn("ms: {task.fields[max_seed]} ({task.fields[max_seed_length]})"),
            #TextColumn("[green]{task.speed} bit/s"),
        )
        self.overall_task = self.overall_progress.add_task(
            f"mvl={self.max_value_length}", 
            total               = int(self.data_info['size_bits']),
            total_score         = self.total_score,
            total_items         = self.data_item_id,
            total_unique        = self.total_unique,
            expansion_items     = 0,
            expansion_threshold = self.expansion_threshold,
            bits_per_item       = self.bits_per_item,
            bits_per_seed       = self.bits_per_seed,
            max_seed            = self.max_seed,
            max_seed_length     = self.max_seed_length,
        )
        self.item_progress = Progress(
            #SpinnerColumn(),
            TextColumn("id: {task.completed:3} ({task.fields[seed_length]:2})"),
            TextColumn("vl: {task.fields[value_length]:2}"),
            TextColumn("sc: {task.fields[seed_counts]}"),
            #"{task.description}",
            TextColumn("[green]{task.fields[target_item]:30}"),
            TextColumn("pc: {task.fields[prev_counts]}"),
            #BarColumn(),
            transient   = True,
        )
        self.item_task = self.item_progress.add_task(
            "", 
            total        = None,
            seed_length  = 0,
            value_length = min(self.active_lengths),
            prev_counts  = self.prev_removed_counts.first_items(6),
            seed_counts  = self.seed_counts.most_common(4),
            target_item  = '',
        )
        self.init_nounce_progress()
        self.init_value_progress()

        # log init params
        self.overall_progress.console.log(f"shift_offset={self.nounce_shift_offset}")
        self.overall_progress.console.log(f"shift_size={self.nounce_shift_size}")
        self.overall_progress.console.log(f"value_limits={self.value_limits}")
        self.overall_progress.console.log(f"max_items_by_length={self.max_items_by_length}")
        self.overall_progress.console.log(f"value_lengths={self.value_lengths}")
        self.overall_progress.console.log(f"init_lengths={self.init_lengths}")
        self.overall_progress.console.log(f"active_lengths={self.active_lengths}")
    
    def has_encoded_data_item(self, target_value: int, value_length: int) -> bool:
        if (target_value in self.encoded_values[value_length]):
            return True
        return False

    def has_encoded_data_prefix(self, target_value: int, value_length: int) -> bool:
        for prev_vl in self.sorted_value_lengths:
            if (prev_vl > value_length):
                break
            target_prefix = target_value % (2**prev_vl)
            if (target_prefix in self.encoded_values[prev_vl]):
                return True
        return False
    
    def get_encoded_data_item(self, target_value: int, value_length: int) -> Union[BaseEncodedDataItem, Document]:
        DataItemClass = get_encoded_data_item_value_class(
            encoder_type           = self.nounce_encoder, 
            start_max_value_length = self.init_max_value_length,
            expand_distance        = self.expand_distance
        )
        target_nounce = self.encoded_value_nounces[value_length][target_value]
        seed_layer_id = self.encoded_nounce_seeds[target_nounce]
        if (value_length == self.nounce_length_bits):
            item_score = 0
        elif (value_length < (self.nounce_length_bits + self.default_score)):
            item_score = (self.nounce_length_bits + self.default_score) - value_length
        else:
            item_score = self.default_score
        data_item = DataItemClass(
            id             = self.data_item_id,
            seed_layer_id  = seed_layer_id,
            seed_length    = 0,
            nounce         = target_nounce,
            value          = target_value,
            value_length   = value_length,
            data_position  = self.data_position,
            item_score     = item_score,
            init_suffix    = None,
        )
        return data_item
    
    def get_seed_data_item(self, target_value: int, value_length: int, target_nounce: int, seed_layer_id: int) -> Union[BaseEncodedDataItem, Document]:
        DataItemClass = get_encoded_data_item_value_class(
            encoder_type           = self.nounce_encoder, 
            start_max_value_length = self.init_max_value_length,
            expand_distance        = self.expand_distance,
        )
        self.update_seed_lengths(seed_layer_id=seed_layer_id)

        if (value_length == self.nounce_length_bits):
            seed_length = self.seed_lengths[seed_layer_id]
            item_score  = 0
        elif (value_length in self.init_lengths):
            seed_length = self.seed_lengths[seed_layer_id] #max(self.max_seed_length, seed_layer_id.bit_length()) #len(fib_encode_position(position=seed_layer_id, encoder_type=self.seed_encoder)) #seed_layer_id.bit_length() + 1 #
            item_score  = (self.nounce_length_bits + self.default_score) - value_length - seed_length
        else:
            seed_length = self.seed_lengths[seed_layer_id] #len(fib_encode_position(position=seed_layer_id, encoder_type=self.seed_encoder))
            item_score  = self.default_score - seed_length
        data_item = DataItemClass(
            id            = self.data_item_id,
            seed_layer_id = seed_layer_id,
            seed_length   = seed_length,
            nounce        = target_nounce,
            value         = target_value,
            value_length  = value_length,
            data_position = self.data_position,
            item_score    = item_score,
            init_suffix   = None,
        )
        return data_item
    
    def get_init_data_item(self, target_value: int, value_length: int) -> Union[BaseEncodedDataItem, Document]:
        DataItemClass = get_encoded_data_item_value_class(
            encoder_type           = self.nounce_encoder, 
            start_max_value_length = self.init_max_value_length,
            expand_distance        = self.expand_distance
        )
        init_suffix = target_value // (2**(self.nounce_length_bits))
        data_item   = DataItemClass(
            id             = self.data_item_id,
            seed_layer_id  = None,
            seed_length    = 1,
            nounce         = init_suffix,
            value          = target_value,
            value_length   = value_length,
            data_position  = self.data_position,
            item_score     = 0,
            init_suffix    = init_suffix
        )
        return data_item
    
    def update_frozen_lengths(self):
        for value_length in self.active_lengths:
            remaining_target_nounces = len(self.target_nounces[value_length])
            if (remaining_target_nounces > 0):
                continue
            self.active_lengths.remove(value_length)
            self.sorted_active_lengths.remove(value_length)
            self.frozen_lengths.append(value_length)
            self.overall_progress.console.log(f"FREEZING VALUE LENGTH: {value_length} ({len(self.target_nounces[value_length])}/{self.value_limits[value_length]}), next_frozen_length={min(self.active_lengths)}")
            self.next_frozen_length = min(self.active_lengths)
            self.overall_progress.console.log(f"new_frozen={self.frozen_lengths}, new_active={self.active_lengths}")
            self.overall_progress.console.log(f"free_nounces={self.free_nounce_counts.with_count_above(1).first_items()}")
            self.overall_progress.console.log(f"used_nounces={self.used_nounce_counts.with_count_above(1).first_items()}")

    def update_prefix_tree(self, value_length: int, new_value: int):
        target_divider = 2**value_length
        for enc_vl in self.sorted_value_lengths:
            if (enc_vl <= value_length):
                continue
            for tier_value in self.encoded_values[enc_vl].copy():
                tier_value_prefix = tier_value % target_divider
                if (tier_value_prefix == new_value):
                    value_nounce = self.encoded_value_nounces[enc_vl][tier_value]
                    enc_value    = int2ba(tier_value, length=enc_vl, endian='little', signed=False)
                    enc_prefix   = int2ba(new_value, length=value_length, endian='little', signed=False)
                    self.encoded_values[enc_vl].remove(tier_value)
                    self.target_nounces[enc_vl].append(value_nounce)
                    self.target_nounces[enc_vl] = sorted(self.target_nounces[enc_vl])
                    del self.encoded_value_nounces[enc_vl][tier_value]
                    del self.encoded_nounce_seeds[value_nounce]
                    self.free_nounce_counts.update({ enc_vl : 1 })
                    self.used_nounce_counts.update({ enc_vl : -1 })
                    self.active_removed_counts.update({ enc_vl : 1 })
                    self.overall_progress.console.log(f">prefix='{enc_prefix.to01()}' ({value_length})")
                    self.overall_progress.console.log(f"> value='{enc_value.to01()}' ({enc_vl})")
                    self.overall_progress.console.log(f">nounce={value_nounce}, usage=({self.used_nounce_counts[enc_vl]}/{self.max_items_by_length[enc_vl]})")
                    
    
    def update_prev_seed_prefix_trees(self, new_value: int, value_length: int):
        #target_divider = 2**value_length
        for enc_vl in self.sorted_value_lengths:
            #if (enc_vl <= value_length):
            #    continue
            if (enc_vl < value_length):
                prefix_divider = 2**enc_vl
                prefix_value   = new_value % prefix_divider
                self.prev_seed_prefixes[prefix_value][value_length].add(new_value)
                continue
            if (enc_vl == value_length):
                for pr_vl in self.sorted_value_lengths:
                    if (pr_vl <= value_length):
                        continue
                    suffix_values = self.prev_seed_prefixes[new_value][pr_vl]
                    for suffix_value in suffix_values:
                        self.prev_seed_values[pr_vl].discard(suffix_value)
                        self.prev_removed_counts.update({ pr_vl: 1 })
                        for sfx_vl in self.sorted_value_lengths:
                            if (sfx_vl > pr_vl):
                                break
                            if (sfx_vl >= value_length):
                                break
                            suffix_divider = 2**sfx_vl
                            subprefix      = suffix_value % suffix_divider
                            if (subprefix == new_value):
                                continue
                            self.prev_seed_prefixes[subprefix][sfx_vl].discard(suffix_value)
                    self.prev_seed_prefixes[new_value][pr_vl].clear()
                #del self.prev_seed_prefixes[new_value]    
                #for pr_vl, suffix_values in self.prev_seed_prefixes[new_value].copy().items():
                    #self.prev_seed_prefixes[new_value][pr_vl].clear()
                    #self.prev_seed_prefixes[suffix_value].clear()
                    #self.prev_seed_prefixes[new_value][pr_vl].clear()
                #del self.prev_seed_prefixes[new_value]
                continue
            if (enc_vl > value_length):
                break
            #for tier_value in self.prev_seed_values[enc_vl].copy():
            #    tier_value_prefix = tier_value % target_divider
            #    if (tier_value_prefix == new_value):
            #        #enc_value    = int2ba(tier_value, length=enc_vl, endian='little', signed=False)
            #        #enc_target   = int2ba(new_value, length=enc_vl, endian='little', signed=False)
            #        self.prev_seed_values[enc_vl].remove(tier_value)
            #        self.prev_removed_counts.update({ enc_vl: 1 })
        #if (len(removed_counts) > 0):
        #    self.overall_progress.console.log(f">>>prev='{enc_target}' ({value_length}): removed_counts={removed_counts.first_items()}")
    
    def update_nounce_ranges(self):
        if (self.enable_expansion is False):
            return
        # expand search space during the scan
        if (self.used_nounce_counts[self.max_value_length] >= self.expansion_threshold):
            next_max_value_length    = self.max_value_length + 1
            next_total_items         = self.value_limits[next_max_value_length]
            next_expansion_threshold = self.value_limits[next_max_value_length-self.expand_distance]

            self.max_items_by_length[next_max_value_length] = next_total_items
            self.value_lengths.append(next_max_value_length)
            self.sorted_value_lengths  = sorted(self.value_lengths)
            self.active_lengths.append(next_max_value_length)
            self.sorted_active_lengths = sorted(self.active_lengths)

            for init_length in self.init_lengths:
                if (init_length in self.value_lengths):
                    self.value_lengths.remove(init_length)
                    self.value_lengths.append(init_length)
                if (init_length in self.active_lengths):
                    self.active_lengths.remove(init_length)
                    self.active_lengths.append(init_length)
            
            #next_nounce_range = get_fib_value_range(value_length=(next_max_value_length - 1), encoder_type=self.nounce_encoder)
            next_nounce_range = get_value_length_range(
                nounce_length_bits = self.nounce_length_bits,
                default_score      = self.default_score,
                value_length       = next_max_value_length,
                shift_offset       = self.nounce_shift_offset,
            )
            next_nounce_range      = range(next_nounce_range[0], next_nounce_range[1])
            
            self.target_nounces[next_max_value_length] = [n for n in next_nounce_range]
            self.free_nounce_counts.update({ next_max_value_length: len(self.target_nounces[next_max_value_length]) })
            self.used_nounce_counts.update({ next_max_value_length: 0 })
            
            self.overall_progress.update(self.overall_task, description=f"mvl={max(self.active_lengths)}")
            self.nounce_tasks[next_max_value_length] = self.nounce_progress.add_task(
                f"l={next_max_value_length}", 
                total=len(self.target_nounces[next_max_value_length])
            )
            self.value_tasks[next_max_value_length] = self.value_progress.add_task(
                f"l={next_max_value_length}",
                total        = 2**32,
                unique_count = 0,
                layer_total  = 0,
                min_seed     = 0,
                max_seed     = 0,
                avg_seed     = 0,
                target_value = '',
                layer_unique = 0,
            )
            self.update_nounce_progress()
            
            self.overall_progress.console.log(f"SEARCH SPACE EXPANDED: new_length={next_max_value_length}, nounces: {min(next_nounce_range)}-{max(next_nounce_range)} ({next_total_items} items), next_expansion_threshold={next_expansion_threshold}")
            self.overall_progress.console.log(f"value_lengths={self.value_lengths}")
            self.overall_progress.console.log(f"active_lengths={self.active_lengths}")
            self.overall_progress.console.log(f"max_items_by_length={self.max_items_by_length}")
            self.overall_progress.console.log(f"free_nounce_counts={self.free_nounce_counts.with_count_above(1).first_items()}")
            self.overall_progress.console.log(f"used_nounce_counts={self.used_nounce_counts.first_items()}")
            # update item limits
            self.max_value_length    = next_max_value_length
            self.expansion_threshold = next_expansion_threshold
    
    def update_max_seed(self, seed: int):
        if (seed <= self.max_seed):
            return
        self.max_seed = seed
        #self.overall_progress.console.log(f"NEW MAX SEED: max_seed={self.max_seed} ({self.max_seed_length})")
        
        seed_length = seed.bit_length()
        if (seed_length <= self.max_seed_length):
            return
        self.max_seed_length = seed_length
        #self.overall_progress.console.log(f"NEW SEED LENGTH: max_seed_length={self.max_seed_length}")
    
    def update_nounce_progress(self):
        layer_total = 0
        layer_new   = 0
        for value_length in self.sorted_value_lengths:
            layer_steps  = 0
            layer_avg    = 0
            layer_score  = self.length_score_counts[value_length]
            layer_items  = self.nounce_length_counts[value_length]
            layer_avg_i  = 0
            if (self.used_nounce_counts[value_length] > 0):
                layer_avg   = (layer_score / self.used_nounce_counts[value_length])
                layer_avg_i = self.nounce_length_counts[value_length] / self.used_nounce_counts[value_length]
            layer_free   = (self.free_nounce_counts[value_length] * 2**(32-value_length) / 2**32) * 100
            layer_used   = (self.used_nounce_counts[value_length] * 2**(32-value_length) / 2**32 )* 100
            layer_total += layer_used
            if (value_length not in self.init_lengths) and (value_length not in self.frozen_lengths):
                layer_new += layer_free
                if (layer_new > 0):
                    layer_steps = ceil((100-layer_total) / layer_new)
            if (value_length in self.frozen_lengths):
                layer_free  = 0
                layer_steps = 0
                layer_new   = 0
            self.nounce_progress.update(
                self.nounce_tasks[value_length], 
                completed     = self.used_nounce_counts[value_length],
                layer_free    = layer_free,
                layer_used    = layer_used,
                layer_total   = layer_total,
                layer_new     = layer_new,
                layer_steps   = layer_steps,
                layer_score   = layer_score,
                layer_avg     = layer_avg,
                layer_items   = layer_items,
                layer_avg_i   = layer_avg_i,
                layer_removed = self.active_removed_counts[value_length] * (-1),
            )

    def update_used_init_values(self, target_value: int, value_length: int):
        if (value_length not in self.init_lengths):
            raise Exception("Incorrect init_length")
        init_value = target_value % (2**(self.nounce_length_bits))
        self.used_init_values[value_length].add(init_value)
        self.used_init_value_count += 1
    
    def has_free_init_prefix(self, target_value: int, value_length: int) -> bool:
        #if (value_length not in self.init_lengths):
        #    raise Exception("Incorrect init_length")
        if (self.used_init_value_count == self.nounce_shift_size):
            return False
        init_value = target_value % (2**(self.nounce_length_bits))
        return (init_value not in self.used_init_values[value_length])

    def has_prefix_in_prev_seed_layers(self, target_value: int, value_length: int, seed_layer_id: int) -> bool:
        # print(f"prev_value_lengths={prev_value_lengths}")
        #prev_seed_layer_ids = [sl_id for sl_id in range(0, seed_layer_id+1)]
        for pv_length in self.sorted_value_lengths:
            if (pv_length > value_length):
                break
            #if (value_length in self.init_lengths):
            #    continue
            target_prefix = target_value % (2**pv_length) #target_prefixes[pv_length]
            if (target_prefix in self.prev_seed_values[pv_length]):
                return True
            #if (target_value in self.prev_seed_prefixes):
            #    return True
        #for prev_seed_layer_id in range(0, seed_layer_id+1):
        #    for pv_length in self.sorted_active_lengths:
        #        if (pv_length > value_length):
        #            break
        #        target_prefix = target_value % (2**pv_length) #target_prefixes[pv_length]
        #        if (target_prefix in self.seed_values[prev_seed_layer_id][pv_length]):
        #            return True
        return False

    def has_next_data_item(self) -> bool:
        remaining_bits = self.data_info['size_bits'] - self.data_position
        if (remaining_bits > self.max_value_length):
            return True
        return False
    
    def update_seed_lengths(self, seed_layer_id: int):
        self.seed_counts.update({ seed_layer_id: 1 })
        hcode = huffman_code(self.seed_counts)
        for id, code in hcode.items():
            self.seed_lengths[id] = len(code)
        if (self.seed_counts[seed_layer_id] == 1):
            self.overall_progress.console.log(f"NEW SEED: {seed_layer_id}, ")
            self.overall_progress.console.log(f"l={self.seed_lengths}")
            self.overall_progress.console.log(f"c={self.seed_counts}")
    
    def update_encoded_items(self, data_item: BaseEncodedDataItem) -> BaseEncodedDataItem:
        self.encoded_values[data_item.value_length].add(data_item.value)
        self.encoded_value_nounces[data_item.value_length][data_item.value] = data_item.nounce
        self.encoded_nounce_seeds[data_item.nounce]                         = data_item.seed_layer_id
        self.data_position += data_item.value_length
        self.data_item_id  += 1
        self.total_score   += data_item.item_score
        #if (data_item.nounce is not None):
        self.target_nounces[data_item.value_length].remove(data_item.nounce)
        self.used_nounces[data_item.value_length].add(data_item.nounce)
        self.free_nounce_counts.update({ data_item.value_length : -1 })
        self.used_nounce_counts.update({ data_item.value_length : 1 })
        self.value_counts.update({ data_item.value: 1 })
        self.seed_length_counts.update({ data_item.seed_length: 1 })
        self.nounce_length_counts.update({ data_item.value_length: 1 })
        self.length_score_counts.update({ data_item.value_length: data_item.item_score })
        self.nounce_progress.advance(self.nounce_tasks[data_item.value_length], 1)
        self.update_prefix_tree(value_length=data_item.value_length, new_value=data_item.value)
        self.update_frozen_lengths()
        self.update_nounce_ranges()
        self.update_nounce_progress()
        if (data_item.seed_layer_id is not None):
            self.update_max_seed(seed=data_item.seed_layer_id)
        return data_item
    
    def get_next_data_item(self) -> Union[BaseEncodedDataItem, Document]:
        self.item_progress.reset(self.item_task)
        target_values = dict()
        target_items  = dict()
        for tv_length in self.length_v:
            item_start               = self.data_position
            item_end                 = item_start + tv_length
            target_data_item         = self.data[item_start:item_end]
            target_values[tv_length] = ba2int(target_data_item, signed=False)
            target_items[tv_length]  = target_data_item
        
        # сначала проверяем накопленный словарь
        has_encoded_item = False
        data_item        = None
        for value_length, target_value in target_values.items():
            if (self.has_encoded_data_item(target_value=target_value, value_length=value_length) is True):
                # создаём и сохраняем новый фрагмент значения
                data_item = self.get_encoded_data_item(target_value=target_value, value_length=value_length)
                data_item.save()
                # обновляем счетчики и передвигаем указатели
                self.data_position += data_item.value_length
                self.data_item_id  += 1
                self.total_score   += data_item.item_score
                self.nounce_length_counts.update({ data_item.value_length: 1 })
                self.length_score_counts.update({ data_item.value_length: data_item.item_score })
                self.update_nounce_progress()
                has_encoded_item   = True
                break
        
        # если найдено словарное значение - используем его, не начинаем поиск новых
        if (has_encoded_item is True):
            self.value_counts.update({ data_item.value: 1 })
            self.overall_progress.console.log(f" vl={data_item.value_length:2} ({([' ', '+'][data_item.item_score > 0] + str(data_item.item_score))}), id={data_item.id} ({self.used_nounce_counts[value_length]}/{self.value_limits[value_length]}), s=None, o=(None), tv={target_values[value_length]} ('{target_items[value_length].to01()}': {self.value_counts[data_item.value]}), p={data_item.data_position}, sc={self.total_score}")
            return data_item
        
        # пробуем найти новое значение в хеш-пространстве
        target_values = dict()
        for tv_length in self.active_lengths:
            item_start               = self.data_position
            item_end                 = item_start + tv_length
            target_data_item         = self.data[item_start:item_end]
            target_values[tv_length] = ba2int(target_data_item, signed=False)
        
        seed_layer_id    = 0
        seed_item_found  = False
        data_item        = None

        unique_item_counts = Counter()
        base_32_items      = 0
        vl_32_items        = Counter()
        layer_32_items     = Counter()

        for enc_vl in self.sorted_value_lengths:
            enc_vl_items        = self.used_nounce_counts[enc_vl] * (2**(32-enc_vl))
            vl_32_items[enc_vl] = base_32_items
            vl_32_items.update({ enc_vl: enc_vl_items })
            unique_item_counts.update({ enc_vl: self.used_nounce_counts[enc_vl] })
            if (self.used_nounce_counts[enc_vl] < self.max_items_by_length[enc_vl]):
                self.value_progress.reset(self.value_tasks[enc_vl])
                self.value_progress.update(
                    self.value_tasks[enc_vl], 
                    layer_total  = (vl_32_items[enc_vl] / 2**32) * 100,
                    unique_count = self.used_nounce_counts[enc_vl],
                    completed    = 0, #vl_32_items[enc_vl],
                    min_seed     = 0,
                    max_seed     = 0,
                    avg_seed     = 0,
                    target_value = target_items[enc_vl].to01(),
                    layer_unique = 0,
                )
                #base_32_items += enc_vl_items
            else:
                self.value_progress.update(
                    self.value_tasks[enc_vl], 
                    layer_total  = (vl_32_items[enc_vl] / 2**32) * 100,
                    unique_count = self.max_items_by_length[enc_vl],
                    completed    = 2**32,
                    layer_unique = 0,
                )
            base_32_items += enc_vl_items
        layer_total = 0

        while (True):
            seed_offset_sums = Counter()
            if (seed_layer_id in self.seed_lengths):
                seed_length = self.seed_lengths[seed_layer_id]
            else:
                seed_length = '?' #len(fib_encode_position(position=seed_layer_id, encoder_type=self.seed_encoder))
            for value_length in self.active_lengths: #target_values.items():
                target_value                   = target_values[value_length]
                seed_offset_sums[value_length] = 0
                length_item_count              = 0
                max_nounce_offset              = 0
                min_nounce_offset              = 2**16
                avg_seed_offset                = 0
                layer_advance                  = 2**(32-value_length)
                # пробуем найти свободные префиксы, чтобы использовать биты длины nounce-значения как дополнительное пространство
                # для кодирования элементов
                #if (value_length == (self.nounce_length_bits + 1)):
                #    if (self.has_free_init_prefix(target_value=target_value, value_length=value_length) is True):
                #        data_item = self.get_init_data_item(target_value=target_value, value_length=value_length)
                #        data_item.save()
                #        self.update_used_init_values(target_value=data_item.value, value_length=data_item.value_length)
                #        # обновляем счетчики и передвигаем указатели
                #        self.update_encoded_items(data_item=data_item)
                #        seed_item_found = True
                #if (len(self.target_nounces[value_length]) == 0):
                #    continue
                #if (seed_item_found is False):
                for nounce in self.target_nounces[value_length]:
                    nounce_seed_offset = 0
                    while (True):
                        seed             = seed_layer_id + nounce_seed_offset
                        hash_input       = nounce.to_bytes(length=4, byteorder='little', signed=False)
                        digest           = xxhash.xxh32_intdigest(input=hash_input, seed=seed)
                        nounce_value     = digest % (2**value_length)
                        has_encoded_prefix = self.has_encoded_data_prefix(target_value=nounce_value, value_length=value_length)
                        if (has_encoded_prefix is True):
                            nounce_seed_offset += 1
                            continue
                        has_value_prefix = self.has_prefix_in_prev_seed_layers(target_value=nounce_value, value_length=value_length, seed_layer_id=seed_layer_id)
                        if (has_value_prefix is True):
                            nounce_seed_offset += 1
                            continue
                        seed_offset_sums.update({ value_length: nounce_seed_offset })
                        if (nounce_seed_offset > max_nounce_offset):
                            max_nounce_offset = nounce_seed_offset
                        if (nounce_seed_offset < min_nounce_offset):
                            min_nounce_offset = nounce_seed_offset
                        break
                    length_item_count += 1
                    #if (value_length == self.max_value_length) and (length_item_count % 500 == 0):
                    #    layer_total     = (vl_32_items[value_length] / 2**32) * 100
                    #    avg_seed_offset = (seed_offset_sums[value_length] / length_item_count)
                    #    self.item_progress.update(self.item_task, description=f"{target_items[value_length][0:8].to01()}, l={value_length} ({self.used_nounce_counts[value_length]}/{self.value_limits[value_length]}), u: {unique_item_counts[value_length]} ({layer_total:2.6f}%), avg={avg_seed_offset:2.2f}, max={max_nounce_offset}, min={min_nounce_offset}")
                    if (nounce_value == target_value):
                        data_item = self.get_seed_data_item(
                            target_value  = target_value,
                            value_length  = value_length,
                            target_nounce = nounce,
                            seed_layer_id = seed_layer_id,
                        )
                        data_item.save()
                        self.update_encoded_items(data_item=data_item)
                        seed_item_found = True
                        break
                    else:
                        #self.seed_values[seed_layer_id][value_length].add(nounce_value)
                        if (value_length not in self.init_lengths):
                            self.update_prev_seed_prefix_trees(new_value=nounce_value, value_length=value_length)
                            self.prev_seed_values[value_length].add(nounce_value)
                        #unique_item_counts.update({ value_length: 1 })
                        #layer_32_items.update({ value_length: layer_advance })
                        #vl_32_items[value_length] += layer_advance
                if (seed_item_found is True):
                    break
                total_layer_advance              = layer_advance * len(self.prev_seed_values[value_length])
                vl_32_items[value_length]        = total_layer_advance
                unique_item_counts[value_length] = len(self.prev_seed_values[value_length]) + self.used_nounce_counts[value_length]
                layer_32_items[value_length]     = layer_advance * (unique_item_counts[value_length] - self.used_nounce_counts[value_length])
                #if (value_length not in self.init_lengths):
                #    unique_item_counts.update({ value_length: len(self.prev_seed_values[value_length]) })
                layer_completed                  = unique_item_counts[value_length] * layer_advance
                layer_unique                     = (layer_32_items[value_length] / 2**32) * 100
                #layer_total                      = unique_item_counts[value_length] * layer_advance

                for c_vl in self.sorted_value_lengths:
                    if (c_vl >= value_length):
                        break
                    #if (c_vl in self.init_lengths):
                    #    continue
                    c_vl_advance     = 2**(32-c_vl)
                    layer_completed += len(self.prev_seed_values[c_vl]) * c_vl_advance + self.used_nounce_counts[c_vl] * c_vl_advance
                    #layer_total     += self.used_nounce_counts[c_vl] * c_vl_advance + len(self.prev_seed_values[c_vl]) * c_vl_advance
                layer_total = (layer_completed / 2**32) * 100

                if (length_item_count > 0):
                    avg_seed_offset = (seed_offset_sums[value_length] / length_item_count)
                if (value_length not in self.init_lengths):
                    self.value_progress.update(
                        self.value_tasks[value_length], 
                        layer_total  = layer_total,
                        unique_count = unique_item_counts[value_length],
                        completed    = layer_completed, #vl_32_items[value_length],
                        min_seed     = min_nounce_offset,
                        max_seed     = max_nounce_offset,
                        avg_seed     = avg_seed_offset,
                        layer_unique = layer_unique,
                    )
                self.overall_progress.update(self.overall_task, description=f"mvl={self.max_value_length}") # vl={value_length:2}, id={seed_layer_id} ({seed_length})
                self.item_progress.update(
                    self.item_task,
                    value_length = value_length,
                    seed_length  = seed_length,
                    seed_counts  = self.seed_counts.most_common(4),
                    prev_counts  = self.prev_removed_counts.first_items(6),
                    target_item  = target_items[value_length].to01(),                      
                )
            if (seed_item_found is True):
                self.overall_progress.console.log(f"+vl={data_item.value_length:2} ({([' ', '+'][data_item.item_score > 0] + str(data_item.item_score))}), id={data_item.id} ({self.used_nounce_counts[value_length]}/{self.value_limits[value_length]}), s={seed_layer_id} ({data_item.seed_length}), o=({seed_layer_id}+{nounce_seed_offset}={seed}), tv={target_values[value_length]} ('{target_items[value_length].to01()}': {self.value_counts[data_item.value]}), p={data_item.data_position}, sc={self.total_score}") # , ii={init_item}, is={data_item.init_suffix}
                self.total_unique += 1
                break
            # continue to scan new layers
            seed_layer_id += 1
            self.item_progress.advance(self.item_task, 1)
        #self.seed_values.clear()
        self.prev_seed_values.clear()
        self.prev_seed_prefixes.clear()
        self.prev_removed_counts.clear()
        return data_item

    def encode_data(self):
        progress_table = Table.grid()
        progress_table.add_row(
            Panel.fit(self.value_progress, title="Values by length", border_style="cyan"),
            Panel.fit(self.nounce_progress, title="Items by length", border_style="yellow"),
        )
        progress_columns = Columns(column_first=True)
        progress_columns.add_renderable(Panel(self.overall_progress, title="Overall Progress", border_style="green"))
        progress_columns.add_renderable(Panel(self.item_progress, title="Current Item", border_style="red"))
        progress_columns.add_renderable(progress_table)

        with Live(progress_columns, refresh_per_second=4, vertical_overflow="ellipsis"):
            while (True):
                data_item          = self.get_next_data_item()
                self.bits_per_item = ((self.total_unique * self.default_score) + ((data_item.id - self.total_unique) * self.default_score)) / self.total_unique
                self.bits_per_seed = sum([sl * sc for sl, sc in self.seed_length_counts.items()]) / self.total_unique
                self.overall_progress.advance(self.overall_task, data_item.value_length)
                self.overall_progress.update(
                    self.overall_task,
                    total_score         = self.total_score,
                    total_items         = self.data_item_id,
                    total_unique        = self.total_unique,
                    expansion_items     = self.used_nounce_counts[self.max_value_length],
                    expansion_threshold = self.expansion_threshold,
                    bits_per_item       = self.bits_per_item,
                    bits_per_seed       = self.bits_per_seed,
                    max_seed            = self.max_seed,
                    max_seed_length     = self.max_seed_length,
                )
                # checking end of data
                if (self.has_next_data_item() is False):
                    self.overall_progress.advance(self.overall_task, self.data_info['size_bits']-self.data_position)
                    self.overall_progress.update(self.overall_task, completed=True, refresh=True)
                    self.overall_progress.console.log(f"position={self.data_position}/{self.data_info['size_bits']}")
                    self.overall_progress.console.log(f"END OF DATA\n")
                    break