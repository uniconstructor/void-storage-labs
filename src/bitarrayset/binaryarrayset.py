# 
# Binary array set (Python)
# 
# Copyright (c) 2021 Project Nayuki. (MIT License)
# https://www.nayuki.io/page/binary-array-set
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy of
# this software and associated documentation files (the "Software"), to deal in
# the Software without restriction, including without limitation the rights to
# use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
# the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
# - The above copyright notice and this permission notice shall be included in
#   all copies or substantial portions of the Software.
# - The Software is provided "as is", without warranty of any kind, express or
#   implied, including but not limited to the warranties of merchantability,
#   fitness for a particular purpose and noninfringement. In no event shall the
#   authors or copyright holders be liable for any claim, damages or other
#   liability, whether in an action of contract, tort or otherwise, arising from,
#   out of or in connection with the Software or the use or other dealings in the
#   Software.
# 

import abc
from typing import Generator, Generic, Iterable, List, Optional, Protocol, TypeVar


E = TypeVar("E", bound="_Comparable")

class _Comparable(Protocol):
	def __lt__(self: E, other: E) -> bool: ...
	def __le__(self: E, other: E) -> bool: ...
	def __gt__(self: E, other: E) -> bool: ...
	def __ge__(self: E, other: E) -> bool: ...


class BinaryArraySet(Generic[E]):
	
	values: List[Optional[List[E]]]
	length: int
	
	
	# Runs in O(n * (log n)^2) time
	def __init__(self, coll: Optional[Iterable[E]] = None):
		self.clear()
		if coll is not None:
			for val in coll:
				self.add(val)
	
	
	# Runs in O(1) time
	def __len__(self) -> int:
		return self.length
	
	
	# Runs in O(1) time
	def clear(self) -> None:
		# For each i, self.values[i] is either None or an ascending list of length 2^i
		self.values = []
		self.length = 0
	
	
	# Note: Not fail-fast on concurrent modification
	def __iter__(self) -> Generator[E,None,None]:
		for vals in self.values:
			if vals is not None:
				yield from vals
	
	
	# Runs in O((log n)^2) time
	def __contains__(self, val: E) -> bool:
		for vals in self.values:
			if vals is not None:
				# Binary search
				start: int = 0
				end: int = len(vals)
				while start < end:
					mid: int = (start + end) // 2
					midval: E = vals[mid]
					if val < midval:
						end = mid
					elif val > midval:
						start = mid + 1
					elif val == midval:
						return True
					else:
						raise AssertionError()
		return False
	
	
	# Runs in average-case O((log n)^2) time, worst-case O(n) time
	def add(self, val: E) -> None:
		# Checking for duplicates is expensive
		if val not in self:
			self.add_unique(val)
	
	
	# Runs in amortized O(1) time, worst-case O(n) time
	def add_unique(self, val: E) -> None:
		toput: Optional[List[E]] = [val]
		for (i, vals) in enumerate(self.values):
			assert (toput is not None) and (len(toput) == 1 << i)
			if vals is None:
				self.values[i] = toput
				toput = None
				break
			else:
				# Merge two sorted arrays
				assert len(vals) == 1 << i
				next: List[E] = []
				j: int = 0
				k: int = 0
				while j < len(vals) and k < len(toput):
					if vals[j] < toput[k]:
						next.append(vals[j])
						j += 1
					else:
						next.append(toput[k])
						k += 1
				next.extend(vals [j : ])
				next.extend(toput[k : ])
				assert len(next) == 2 << i
				toput = next
				self.values[i] = None
		if toput is not None:
			self.values.append(toput)
		self.length += 1
	
	
	# For unit tests
	def check_structure(self) -> None:
		if self.length < 0:
			raise AssertionError()
		
		sum: int = 0
		for (i, vals) in enumerate(self.values):
			if vals is not None:
				if len(vals) != 1 << i:
					raise AssertionError()
				sum += len(vals)
				for j in range(1, len(vals)):
					if vals[j - 1] >= vals[j]:
						raise AssertionError()
		if sum != self.length:
			raise AssertionError()

class Common:

    def __repr__(self):
        return "SparseBitarray('%s')" % (''.join(str(v) for v in self))

    def pop(self, i = -1):
        if i < 0:
            i += len(self)
        res = self[i]
        del self[i]
        return res

    def remove(self, value):
        i = self.find(value)
        if i < 0:
            raise ValueError
        del self[i]

    def sort(self, reverse=False):
        if reverse:
            c1 = self.count(1)
            self[:c1:] = 1
            self[c1::] = 0
        else:
            c0 = self.count(0)
            self[:c0:] = 0
            self[c0::] = 1

    def _get_start_stop(self, key):
        if key.step not in (1, None):
            raise ValueError("only step = 1 allowed, got %r" % key)
        start = key.start
        if start is None:
            start = 0
        stop = key.stop
        if stop is None:
            stop = len(self)
        return start, stop

    def _adjust_index(self, i):
        n = len(self)
        if i < 0:
            i += n
            if i < 0:
                i = 0
        elif i > n:
            i = n
        return i

"""
Implementation of a sparse bitarray

Internally we store a list of positions at which a bit changes from
1 to 0 or vice versa.  Moreover, we start with bit 0, meaning that if the
first bit in the bitarray is 1 our list starts with posistion 0.
For example:

   bitarray('110011111000')

is represented as:

   flips:   [0, 2, 4, 9, 12]

The last element in the list is always the length of the bitarray, such that
an empty bitarray is represented as [0].
"""
from bisect import bisect, bisect_left

from bitarray import bitarray


class SparseBitarray(Common):

    def __init__(self, x = 0):
        if isinstance(x, int):
            self.flips = [x]  # bitarray with x zeros
        else:
            self.flips = [0]
            for v in x:
                self.append(int(v))

    def __len__(self):
        return self.flips[-1]

    def __getitem__(self, key):
        if isinstance(key, slice):
            start, stop = self._get_start_stop(key)
            if stop <= start:
                return SparseBitarray()

            i = bisect(self.flips, start)
            j = bisect_left(self.flips, stop)

            res = SparseBitarray()
            res.flips = [0] if i % 2 else []
            for k in range(i, j):
                res.flips.append(self.flips[k] - start)
            res.flips.append(stop - start)
            return res

        elif isinstance(key, int):
            if not 0 <= key < len(self):
                raise IndexError
            return bisect(self.flips, key) % 2

        else:
            raise TypeError

    def __setitem__(self, key, value):
        if isinstance(key, slice):
            start, stop = self._get_start_stop(key)
            if stop <= start:
                return

            i = bisect(self.flips, start)
            j = bisect_left(self.flips, stop)

            self.flips[i:j] = (
                ([] if i % 2 == value else [start]) +
                ([] if j % 2 == value else [stop])
            )

        elif isinstance(key, int):
            if not 0 <= key < len(self):
                raise IndexError
            p = bisect(self.flips, key)
            if p % 2 == value:
                return
            self.flips[p:p] = [key, key + 1]

        else:
            raise TypeError

        self._reduce()

    def __delitem__(self, key):
        if isinstance(key, slice):
            start, stop = self._get_start_stop(key)
            if stop <= start:
                return

            i = bisect(self.flips, start)
            j = bisect_left(self.flips, stop)

            size = stop - start
            for k in range(j, len(self.flips)):
                self.flips[k] -= size
            self.flips[i:j] = [start] if (j - i) % 2 else []

        elif isinstance(key, int):
            if not 0 <= key < len(self):
                raise IndexError
            p = bisect(self.flips, key)
            for j in range(p, len(self.flips)):
                self.flips[j] -= 1

        else:
            raise TypeError

        self._reduce()

    def _reduce(self):
        n = self.flips[-1]      # length of bitarray
        lst = []                # new representation list
        i = 0
        while True:
            c = self.flips[i]   # current element (at index i)
            if c == n:          # element with bitarray length reached
                break
            j = i + 1           # find next value (at index j)
            while self.flips[j] == c:
                j += 1
            if (j - i) % 2:     # only append index if repeated odd times
                lst.append(c)
            i = j
        lst.append(n)
        self.flips = lst

    def _intervals(self):
        v = 0
        start = 0
        for stop in self.flips:
            yield v, start, stop
            v = 1 - v
            start = stop

    def append(self, value):
        if value == len(self.flips) % 2:  # opposite value as last element
            self.flips.append(len(self) + 1)
        else:                             # same value as last element
            self.flips[-1] += 1

    def extend(self, other):
        n = len(self)
        m = len(other.flips)
        if len(self.flips) % 2:
            self.flips.append(n)
        for i in range(m):
            self.flips.append(other.flips[i] + n)
        self._reduce()

    def find(self, value):
        if len(self) == 0:
            return -1
        flips = self.flips
        if value:
            return -1 if len(flips) == 1 else flips[0]
        else:
            if flips[0] > 0:
                return 0
            return -1 if len(flips) == 2 else flips[1]

    def to_bitarray(self):
        a = bitarray(len(self))
        for v, start, stop in self._intervals():
            a[start:stop] = v
        return a

    def invert(self):
        self.flips.insert(0, 0)
        self._reduce()

    def insert(self, i, value):
        i = self._adjust_index(i)
        p = bisect_left(self.flips, i)
        for j in range(p, len(self.flips)):
            self.flips[j] += 1
        self[i] = value

    def count(self, value=1):
        cnt = 0
        for v, start, stop in self._intervals():
            if v == value:
                cnt += stop - start
        return cnt

    def reverse(self):
        n = len(self)
        lst = [0] if len(self.flips) % 2 else []
        lst.extend(n - p for p in reversed(self.flips))
        lst.append(n)
        self.flips = lst
        self._reduce()