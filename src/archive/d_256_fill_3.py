from rich import print
from rich.pretty import pprint
from b_256_utils import discover_shard_values
from mongoengine import register_connection
register_connection('default', db=f'256_byte_segments', host='127.0.0.1', port=27017, maxPoolSize=200)

target_shard = input("target_shard:")
extra_shards = input("extra_shards:")

if (target_shard == ''):
  target_shard = None
else:
  target_shard = int(target_shard)

if (extra_shards == ''):
  extra_shards = 1024
else:
  extra_shards = int(extra_shards)

print(f"Scanning target shard {target_shard} (+{extra_shards} extra shards):")
discover_shard_values(shard_id=target_shard, include_partial=True)

for i in range(0, extra_shards):
  print(f"\nScanning extra shard {i+1}/{extra_shards}:")
  discover_shard_values()