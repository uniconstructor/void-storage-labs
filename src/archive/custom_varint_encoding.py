# https://github.com/Textualize/rich
from rich import print, inspect
# https://rich.readthedocs.io/en/latest/pretty.html
from rich.pretty import pprint
# https://rich.readthedocs.io/en/latest/traceback.html
from rich.traceback import install
install(show_locals=True)
# https://rich.readthedocs.io/en/latest/logging.html
import logging
from rich.logging import RichHandler
logging.basicConfig(
    level="INFO",
    format="%(message)s",
    datefmt="[%X]",
    handlers=[RichHandler(rich_tracebacks=True, locals_max_length=16)]
)
log = logging.getLogger("rich")
# https://github.com/tqdm/tqdm
import tqdm
# https://realpython.com/python-data-classes/
# https://realpython.com/python-data-classes/#advanced-default-values
from dataclasses import dataclass, make_dataclass, field
# https://github.com/keon/algorithms
# https://github.com/keon/algorithms/blob/master/algorithms/compression/rle_compression.py
from algorithms.compression.rle_compression import encode_rle, decode_rle
# https://bitstring.readthedocs.io/en/latest/index.output_item_idhtml
from bitstring import Bits, BitArray, BitStream, ConstBitStream
# https://pythongeeks.org/ordereddict-in-python/
# https://www.geeksforgeeks.org/counters-in-python-set-1/
# https://docs.python.org/3/library/collections.html#counter-objects
# https://realpython.com/python-namedtuple/
# https://realpython.com/linked-lists-python/
from collections import OrderedDict, Counter, defaultdict, namedtuple, deque
# https://github.com/multiformats/unsigned-varint
# https://github.com/fmoo/python-varint
#import varint
# https://docs.python.org/3/library/typing.html
# https://realpython.com/python-data-classes/#more-flexible-data-classes
from typing import List, Set, Dict, Tuple, Optional, Union, Deque, Literal
# https://www.geeksforgeeks.org/floor-ceil-function-python/?ref=lbp
import math
# https://docs.python.org/3/library/operator.html#module-operator
import operator
from os.path import exists
# https://stackoverflow.com/questions/55212014/python-count-copies-in-dictionary
# https://docs.python.org/3/library/copyreg.html#module-copyreg
# https://docs.python.org/3/library/shelve.html#module-shelve
# https://docs.python.org/3/library/pickle.html#persistence-of-external-objects
import copyreg, copy, pickle, json
# https://habr.com/ru/company/timeweb/blog/564826/
# https://docs.python.org/3/library/enum.html
import enum
from enum import Enum, IntEnum
import os, sys
if not 'workbookDir' in globals():
    workbookDir = os.getcwd()
os.chdir(workbookDir)

# функции для работы с хеш-пространством
from hash_space_utils import HashItemAddress, HashItemPosition, HashSegmentAddress, PositionSeed, \
    DEFAULT_VALUE_STEP, DEFAULT_POSITION_STEP, \
    get_min_bit_length, \
    get_aligned_bit_length, \
    read_hash_item, \
    value_at_position, \
    find_value_in_segment
# параметры для кодирования одного элемента в зависимости от его длины
ItemLengthOptions = namedtuple('ItemLengthOptions', [
    'position_length', 
    'value_length', 
    'position_step', 
    'value_step', 
    'prefix_length',
    'space_length',
    'encoded_prefix',
    'start_search_position',
    'end_search_position',
    'option_number',
    'prev_value_length',
    'max_items',
    'option_index',
    'prev_option_index',
    'open_positions',
])

class NumberType(bytes, Enum):
    """
    >>> NumberType(0)
    <NumberType.POSITION: 0>
    >>> NumberType(1)
    <NumberType.VALUE: 1>
    >>> NumberType(0).unit_field
    'position_step'
    >>> NumberType(0).length_field
    'position_length'
    >>> NumberType(1).unit_field
    'value_step'
    >>> NumberType(1).length_field
    'value_length'
    """
    def __new__(cls, id, unit_field, length_field):
        obj = bytes.__new__(cls, [id])
        obj._value_      = id
        obj.unit_field   = unit_field
        obj.length_field = length_field
        return obj
    POSITION = (0, 'position_step', 'position_length')
    VALUE    = (1, 'value_step', 'value_length')

def get_prefix_length_for_options(length_options: Union[List, Dict]) -> int:
    options_count = len(length_options)
    if (options_count == 0):
        raise Exception(f"Incorrect number of length options (0): length_options={length_options}")
    if (options_count == 1):
        length_prefix_bits = 0
    else:
        length_prefix_bits  = get_min_bit_length(options_count - 1)
    return length_prefix_bits

def get_mapping_from_value_length_to_prefix(length_options: Dict[str, ItemLengthOptions]) -> Dict[int, str]:
    result = dict()
    for option in length_options.values():
        result[option.value_length] = option.encoded_prefix.bin
    return result

def create_open_positions_range(start_search_position: int, end_search_position: int, position_step: int) -> range:
    start_range = start_search_position
    end_range   = end_search_position
    #print(f"{start_range}, {end_range}, {position_step}")
    return range(start_range, end_range, position_step)

def create_value_length_options(min_length: int, max_length: int, free_space_bits: int, \
        position_step: int, value_step: int, key_by:str='option_index') -> Dict[str, ItemLengthOptions]:
    """
    >>> free_space_bits = 1
    >>> min_length      = 8
    >>> max_length      = 32
    >>> position_step   = 8
    >>> value_step      = 8
    >>> options         = create_value_length_options(min_length, max_length, free_space_bits, position_step, value_step)
    >>> len(options)
    4
    >>> #options['00']
    >>> options['00'].option_index
    '00'
    >>> options['00'].prev_option_index == None
    True
    >>> options['00'].prev_value_length == None
    True
    >>> options['00'].option_number
    0
    >>> options['00'].space_length
    1
    >>> options['00'].prefix_length
    2
    >>> options['00'].position_length
    5
    >>> options['00'].value_length
    8
    >>> options['00'].encoded_prefix
    BitArray('0b00')
    >>> options['00'].position_length == options['00'].value_length - options['00'].prefix_length - options['00'].space_length
    True
    >>> options['00'].position_step == position_step
    True
    >>> options['00'].value_step == value_step
    True
    >>> options['00'].space_length == free_space_bits
    True
    >>> options['00'].start_search_position
    0
    >>> options['00'].end_search_position == (2 ** options['00'].position_length) * value_step
    True
    >>> options['00'].max_items == (2 ** options['00'].position_length)
    True
    >>> len(options['00'].open_positions)
    32
    >>> len(options['00'].open_positions) == options['00'].max_items
    True
    >>> list(options['00'].open_positions)
    [0, 8, 16, 24, 32, 40, 48, 56, 64, 72, 80, 88, 96, 104, 112, 120, 128, 136, 144, 152, 160, 168, 176, 184, 192, 200, 208, 216, 224, 232, 240, 248]
    >>> #options['01']
    >>> options['01'].option_index
    '01'
    >>> options['01'].prev_option_index == options['00'].option_index
    True
    >>> options['01'].prev_value_length
    8
    >>> options['01'].prev_value_length == options['00'].value_length
    True
    >>> options['01'].option_number
    1
    >>> options['01'].space_length
    1
    >>> options['01'].prefix_length
    2
    >>> options['01'].position_length
    13
    >>> options['01'].value_length
    16
    >>> options['01'].encoded_prefix
    BitArray('0b01')
    >>> options['01'].position_length == (options['01'].value_length - options['01'].prefix_length) - options['01'].space_length
    True
    >>> options['01'].position_step == position_step
    True
    >>> options['01'].value_step == value_step
    True
    >>> options['01'].space_length == free_space_bits
    True
    >>> options['01'].start_search_position == options['00'].end_search_position
    True
    >>> options['01'].end_search_position == options['01'].start_search_position + (2 ** options['01'].position_length) * value_step
    True
    >>> options['01'].max_items == (2 ** options['01'].position_length)
    True
    >>> len(options['01'].open_positions) == options['01'].max_items
    True
    >>> list(options['01'].open_positions[0:8])
    [256, 264, 272, 280, 288, 296, 304, 312]
    >>> first_item_position = list(options['01'].open_positions)[0]
    >>> prev_position_index = (len(options['00'].open_positions) - 1)
    >>> last_prev_position  = list(options['00'].open_positions)[prev_position_index]
    >>> first_item_position == last_prev_position + options['00'].position_step
    True
    >>> #options['10']
    >>> options['10'].option_index
    '10'
    >>> options['10'].prev_option_index == options['01'].option_index
    True
    >>> options['10'].prev_value_length
    16
    >>> options['10'].prev_value_length == options['01'].value_length
    True
    >>> options['10'].option_number
    2
    >>> options['10'].space_length
    1
    >>> options['10'].prefix_length
    2
    >>> options['10'].position_length
    21
    >>> options['10'].value_length
    24
    >>> options['10'].encoded_prefix
    BitArray('0b10')
    >>> options['10'].position_length == (options['10'].value_length - options['10'].prefix_length) - options['10'].space_length
    True
    >>> options['10'].position_step == position_step
    True
    >>> options['10'].value_step == value_step
    True
    >>> options['10'].space_length == free_space_bits
    True
    >>> options['10'].start_search_position == options['01'].end_search_position
    True
    >>> options['10'].end_search_position == options['10'].start_search_position + (2 ** options['10'].position_length) * value_step
    True
    >>> options['10'].max_items == (2 ** options['10'].position_length)
    True
    >>> len(options['10'].open_positions) == options['10'].max_items
    True
    >>> list(options['10'].open_positions[0:8])
    [65792, 65800, 65808, 65816, 65824, 65832, 65840, 65848]
    >>> #options['11']
    >>> options['11'].option_index
    '11'
    >>> options['11'].prev_option_index == options['10'].option_index
    True
    >>> options['11'].prev_value_length
    24
    >>> options['11'].prev_value_length == options['10'].value_length
    True
    >>> options['11'].option_number
    3
    >>> options['11'].space_length
    1
    >>> options['11'].prefix_length
    2
    >>> options['11'].position_length
    29
    >>> options['11'].value_length
    32
    >>> options['11'].encoded_prefix
    BitArray('0b11')
    >>> options['11'].position_length == (options['11'].value_length - options['11'].prefix_length) - options['11'].space_length
    True
    >>> options['11'].position_step == position_step
    True
    >>> options['11'].value_step == value_step
    True
    >>> options['11'].space_length == free_space_bits
    True
    >>> options['11'].start_search_position == options['10'].end_search_position
    True
    >>> options['11'].end_search_position == options['11'].start_search_position + (2 ** options['11'].position_length) * value_step
    True
    >>> options['11'].max_items == (2 ** options['11'].position_length)
    True
    >>> len(options['11'].open_positions) == options['11'].max_items
    True
    """
    options             = dict()
    value_lengths       = list(range(min_length, (max_length + position_step), position_step))
    # define minimum bits needed to enumerate different options
    length_prefix_bits  = get_prefix_length_for_options(value_lengths)
    value_length_number = 0
    prev_option         = None
    for value_length in value_lengths:
        # define the maximum length of the position in the encoded item
        position_length       = get_position_length_for_value_length(value_length, length_prefix_bits, free_space_bits)
        max_items             = (2 ** position_length)
        # allocate bits for the length prefix if (and only if) we have more then 1 different length options
        if length_prefix_bits > 0:
            encoded_length_prefix = BitArray(uint=value_length_number, length=length_prefix_bits)
            option_index          = encoded_length_prefix.bin
        else:
            encoded_length_prefix = BitArray()
            option_index          = '0'
        # define start bit position for search
        if (prev_option is None):
            start_search_position = 0
            prev_value_length     = None
            prev_option_index     = None
        else:
            start_search_position = prev_option.end_search_position
            prev_value_length     = prev_option.value_length
            prev_option_index     = prev_option.option_index
        # define end bit position for search
        end_search_position = start_search_position + (max_items * position_step)
        # prepare positions range
        open_positions = create_open_positions_range(start_search_position, end_search_position, position_step)
        # TODO: skip creation of item options for some length when free_space_bits is too large for them
        item_options = ItemLengthOptions(
            position_length, 
            value_length, 
            position_step,
            value_step,
            length_prefix_bits, 
            free_space_bits,
            encoded_length_prefix,
            start_search_position,
            end_search_position,
            value_length_number,
            prev_value_length,
            max_items,
            option_index,
            prev_option_index,
            open_positions,
        )
        # save prev item for linking
        options[option_index] = item_options
        value_length_number  += 1
        prev_option           = item_options
    return options

def get_position_length_for_value_length(value_length: int, length_prefix_bits: int, free_space_bits: int) -> int:
    """
    >>> length_prefix_bits = 2
    >>> free_space_bits    = 1
    >>> value_length       = 8
    >>> get_position_length_for_value_length(value_length, length_prefix_bits, free_space_bits)
    5
    >>> free_space_bits    = 2
    >>> get_position_length_for_value_length(value_length, length_prefix_bits, free_space_bits)
    4
    >>> length_prefix_bits = 3
    >>> get_position_length_for_value_length(value_length, length_prefix_bits, free_space_bits)
    3
    """
    return (value_length - length_prefix_bits) - free_space_bits

def get_item_based_position_length(position: int, position_step: int=DEFAULT_POSITION_STEP) -> int:
    """
    >>> position_step = 8
    >>> position      = 3 * position_step
    >>> get_item_based_position_length(position, position_step)
    2
    >>> position      = 4 * position_step
    >>> get_item_based_position_length(position, position_step)
    3
    >>> position = 255 * position_step
    >>> get_item_based_position_length(position, position_step)
    8
    >>> position = 256 * position_step
    >>> get_item_based_position_length(position, position_step)
    9
    >>> position = (2**16-1) * position_step
    >>> get_item_based_position_length(position, position_step)
    16
    >>> position = (2**16) * position_step
    >>> get_item_based_position_length(position, position_step)
    17
    >>> position_step = 16
    >>> position      = 3 * position_step
    >>> get_item_based_position_length(position, position_step)
    2
    >>> position      = 4 * position_step
    >>> get_item_based_position_length(position, position_step)
    3
    >>> position = 255 * position_step
    >>> get_item_based_position_length(position, position_step)
    8
    >>> position = (2**16-1) * position_step
    >>> get_item_based_position_length(position, position_step)
    16
    >>> position = (2**16) * position_step
    >>> get_item_based_position_length(position, position_step)
    17
    """
    if (position > position_step) and (position % position_step != 0):
        raise Exception(f"Incorrect position step: {position_length}, position={position}, position_step={position_step}")
    item_position   = (position // position_step)
    position_length = get_min_bit_length(item_position)
    return position_length

def get_item_based_value_length(value: int, value_step: int=DEFAULT_VALUE_STEP) -> int:
    """
    >>> value_step = 8
    >>> value = 3
    >>> get_item_based_value_length(value, value_step)
    8
    >>> value = 255
    >>> get_item_based_value_length(value, value_step)
    8
    >>> value = 256
    >>> get_item_based_value_length(value, value_step)
    16
    >>> value = (2**16-1)
    >>> get_item_based_value_length(value, value_step)
    16
    >>> value = (2**16)
    >>> get_item_based_value_length(value, value_step)
    24
    >>> value_step = 16
    >>> value = 3
    >>> get_item_based_value_length(value, value_step)
    16
    >>> value = 255
    >>> get_item_based_value_length(value, value_step)
    16
    >>> value = (2**16-1)
    >>> get_item_based_value_length(value, value_step)
    16
    >>> value = (2**16)
    >>> get_item_based_value_length(value, value_step)
    32
    """
    value_length = get_min_bit_length(value)
    for i in range(1, 9):
        item_based_length = (i * value_step)
        if (value_length <= item_based_length):
            return item_based_length
    raise Exception(f"Incorrect value_length={value_length}, value={value}, value_step={value_step}")

# encoding/decoding api:

def encode_number(number: int, item_options: ItemLengthOptions, number_type: NumberType) -> BitArray:
    """
    >>> free_space_bits = 1
    >>> min_length      = 8
    >>> max_length      = 32
    >>> position_step   = 8
    >>> value_step      = 8
    >>> length_options  = create_value_length_options(min_length, max_length, free_space_bits, position_step, value_step)
    >>> item_options    = length_options['00']
    >>> encoded_number  = encode_number(5, item_options, NumberType.POSITION)
    >>> encoded_number
    BitArray('0b0000101')
    >>> item_options    = length_options['01']
    >>> encoded_number  = encode_number(40, item_options, NumberType.POSITION)
    >>> encoded_number
    BitArray('0b010000000101000')
    >>> item_options    = length_options['00']
    >>> encoded_number  = encode_number(31, item_options, NumberType.POSITION)
    >>> encoded_number
    BitArray('0b0011111')
    """
    item_length  = item_options.__getattribute__(number_type.length_field)
    encoded_item = BitArray(uint=number, length=item_length)
    encoded_item.prepend(item_options.encoded_prefix)
    return encoded_item

def decode_number(item: Union[Bits, BitArray, BitStream, ConstBitStream], item_options: ItemLengthOptions) -> int:
    """
    >>> free_space_bits = 1
    >>> min_length      = 8
    >>> max_length      = 32
    >>> position_step   = 8
    >>> value_step      = 8
    >>> length_options  = create_value_length_options(min_length, max_length, free_space_bits, position_step, value_step)
    >>> item_options    = length_options['00']
    >>> encoded_number  = encode_number(5, item_options, NumberType.POSITION)
    >>> encoded_number
    BitArray('0b0000101')
    >>> decoded_number = decode_number(encoded_number, item_options)
    >>> decoded_number
    5
    """
    # locate bits storing item element value in data
    value_start = item_options.prefix_length
    # read bits from item and convert them to number (item value)
    item_value  = item[value_start:].uint
    return item_value

def encode_hash_item_address(item_address: HashItemAddress, length_options: Dict[int, ItemLengthOptions]) -> BitArray:
    """
    >>> free_space_bits = 1
    >>> min_length      = 8
    >>> max_length      = 32
    >>> position_step   = 8
    >>> value_step      = 8
    >>> length_options  = create_value_length_options(min_length, max_length, free_space_bits, position_step, value_step)
    >>> item_value      = Bits('0x00')
    >>> item_address    = HashItemAddress(bit_position=40, bit_length=8, seed=0)
    >>> encoded_item    = encode_hash_item_address(item_address, length_options)
    >>> len(encoded_item)
    7
    >>> len(encoded_item) == (len(item_value) - free_space_bits)
    True
    >>> encoded_item
    BitArray('0b0000101')
    """
    value_length_mapping = get_mapping_from_value_length_to_prefix(length_options)
    options_key          = value_length_mapping[item_address.bit_length]
    item_options         = length_options[options_key]
    item_position        = (item_address.bit_position // item_options.position_step)
    encoded_position     = encode_number(item_position, item_options, NumberType.POSITION) # BitArray(uint=item_position, length=item_options.position_length)
    return encoded_position

def decode_hash_item_address(item_value: BitStream, length_options: Dict[str, ItemLengthOptions], seed: int) -> HashItemAddress:
    """
    >>> free_space_bits = 1
    >>> min_length      = 8
    >>> max_length      = 32
    >>> position_step   = DEFAULT_POSITION_STEP
    >>> value_step      = DEFAULT_VALUE_STEP
    >>> length_options  = create_value_length_options(min_length, max_length, free_space_bits, position_step, value_step)
    >>> item_value      = Bits('0x00')
    >>> item_address    = HashItemAddress(bit_position=40, bit_length=8, seed=0)
    >>> seed            = 0
    >>> encoded_item    = encode_hash_item_address(item_address, length_options)
    >>> encoded_item
    BitArray('0b0000101')
    >>> decoded_item    = decode_hash_item_address(encoded_item, length_options, seed)
    >>> decoded_item
    HashItemAddress(bit_position=40, bit_length=8, seed=0)
    >>> decoded_item == item_address
    True
    >>> decoded_value = read_hash_item(item_address)
    >>> decoded_value
    Bits('0x00')
    >>> decoded_value == item_value
    True
    >>> item_value      = Bits('0x9200')
    >>> seed            = 0
    >>> item_address    = find_value_in_segment(item_value, HashSegmentAddress(0, 2**16, 0))
    >>> item_address
    HashItemAddress(bit_position=32, bit_length=16, seed=0)
    >>> encoded_item    = encode_hash_item_address(item_address, length_options)
    >>> encoded_item
    BitArray('0b010000000000100')
    >>> decoded_item    = decode_hash_item_address(encoded_item, length_options, seed)
    >>> decoded_item
    HashItemAddress(bit_position=32, bit_length=16, seed=0)
    >>> decoded_item == item_address
    True
    >>> decoded_value = read_hash_item(item_address)
    >>> decoded_value
    Bits('0x9200')
    >>> decoded_value == item_value
    True
    """
    prefix_length  = get_prefix_length_for_options(length_options)
    item_prefix    = item_value[0:prefix_length]
    if (item_prefix.bin not in length_options):
        raise Exception(f"Incorrect item_prefix={item_prefix.bin}, item_value={item_value}, length_options: {length_options}")
    item_options   = length_options[item_prefix.bin]
    decoded_number = decode_number(item_value, item_options)
    # convert item value to bit value
    bit_position   = (decoded_number * item_options.position_step)
    # length of the decoded value (located at the address)
    value_length   = item_options.value_length
    item_address   = HashItemAddress(bit_position, value_length, seed)
    return item_address

def encode_custom_varint_list(numbers: List[int], length_options: Dict[str, ItemLengthOptions]) -> List[int]:
    prefix_length = get_prefix_length_for_options(length_options)
    pass

def decode_custom_varint_list(data: Union[Bits, BitArray, BitStream, ConstBitStream], \
        length_options: Dict[str, ItemLengthOptions]) -> List[int]:
    """
    >>> free_space_bits = 1
    >>> min_length      = 8
    >>> max_length      = 32
    >>> position_step   = DEFAULT_POSITION_STEP
    >>> value_step      = DEFAULT_VALUE_STEP
    >>> length_options  = create_value_length_options(min_length, max_length, free_space_bits, position_step, value_step)
    >>> #numbers = BitArray('')
    >>> #decode_custom_varint_list(numbers, length_options)
    """
    prefix_length = get_prefix_length_for_options(length_options)
    numbers       = list()
    while len(data) > 0:
        item_prefix  = data[0:prefix_length]
        item_options = length_options[item_prefix.bin]
        item_length  = prefix_length + item_options.position_length
        item_bits    = data[0:item_length]
        number       = decode_number(item_bits, item_options, NumberType.POSITION)
        numbers.append(number)
        # prepare to read next varint value (clear decoded bytes)
        start = item_length
        end   = len(data)
        data  = data[start:end]
    return numbers