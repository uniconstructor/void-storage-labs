# поддержка импорта по относительному пути
import os, sys
if not 'workbookDir' in globals():
    workbookDir = os.getcwd()
os.chdir(workbookDir)

# https://airflow.apache.org/docs/apache-airflow/stable/tutorial.html
# https://airflow.apache.org/docs/apache-airflow/stable/start/local.html
# https://airflow.apache.org/docs/apache-airflow/stable/concepts/dags.html#task-dependencies
# https://airflow.apache.org/docs/apache-airflow/stable/concepts/taskflow.html
# https://github.com/apache/airflow/tree/main/airflow/example_dags
# https://airflow.apache.org/docs/apache-airflow/stable/concepts/operators.html
# https://airflow.apache.org/docs/apache-airflow/stable/tutorial_taskflow_api.html
# https://github.com/chriscardillo/gusty
#from airflow import DAG
#from airflow.decorators import task
# https://github.com/chriscardillo/gusty#dag-and-taskgroup-control
# from gusty import create_dag

# https://github.com/Textualize/rich
from rich import print, inspect
# https://rich.readthedocs.io/en/latest/pretty.html
from rich.pretty import pprint
# https://rich.readthedocs.io/en/latest/traceback.html
from rich.traceback import install
install(show_locals=True)
# https://rich.readthedocs.io/en/latest/logging.html
import logging
from rich.logging import RichHandler
logging.basicConfig(
    level="INFO",
    format="%(message)s",
    datefmt="[%X]",
    handlers=[RichHandler(rich_tracebacks=True, locals_max_length=16)]
)
log = logging.getLogger("rich")
# https://github.com/tqdm/tqdm
import tqdm

# https://realpython.com/python-data-classes/
# https://realpython.com/python-data-classes/#advanced-default-values
from dataclasses import dataclass, make_dataclass, field
# https://github.com/keon/algorithms
# https://github.com/keon/algorithms/blob/master/algorithms/compression/rle_compression.py
from algorithms.compression.rle_compression import encode_rle, decode_rle

# https://bitstring.readthedocs.io/en/latest/index.output_item_idhtml
from bitstring import Bits, BitArray, BitStream, ConstBitStream
# https://pythongeeks.org/ordereddict-in-python/
# https://www.geeksforgeeks.org/counters-in-python-set-1/
# https://docs.python.org/3/library/collections.html#counter-objects
# https://realpython.com/python-namedtuple/
from collections import OrderedDict, Counter, defaultdict, namedtuple
# https://realpython.com/linked-lists-python/
from collections import deque
# https://github.com/multiformats/unsigned-varint
# https://github.com/fmoo/python-varint
import varint
# https://docs.python.org/3/library/typing.html
# https://realpython.com/python-data-classes/#more-flexible-data-classes
from typing import List, Set, Dict, Tuple, Optional, Union, Deque
# https://www.geeksforgeeks.org/floor-ceil-function-python/?ref=lbp
import math
# https://docs.python.org/3/library/operator.html#module-operator
import operator
# https://stackoverflow.com/questions/55212014/python-count-copies-in-dictionary
import copy

# TODO: ChainMap: https://docs.python.org/3/library/collections.html#chainmap-examples-and-recipes
# TODO: вычислять последовательность как топологический порядок графа связей: https://docs.python.org/3/library/graphlib.html

from hash_space_utils import HashItemAddress, \
    value_at_position, \
    read_hash_item, \
    split_data, \
    count_split_values, \
    collect_split_positions, \
    create_data_bitmap, \
    collect_bitmap_keys, \
    get_item_based_data_length_from_number, \
    get_item_based_data_length, \
    get_data_item_position_from_data_length, \
    get_data_item_position_from_number

# default item processing options - used for reading seed/item value from hash space
DEFAULT_SEED_ITEM_LENGTH  : int = 24
DEFAULT_VALUE_ITEM_LENGTH : int = 8
SEED_VALUE_START_POSITION : int = 0

# DATA ITEM MAPPING + item-to-next-item transformations

def create_bitmap_data_mapping(bitmap: Bits, format:str, item_length: int, target_bitmap_value: bool = True):
    """
    Создает отображение всех возможных значений данных на подмножество использованных
    Позволяет независимо от прочитанного из хешей значения всегда получать из него используемое значение данных
    Этот метод никогда не вернет те байты, которые никогда не встречаются в исходных данных

    >>> data    = ConstBitStream(value_at_position(0, 64, 999999))
    >>> bitmap  = create_data_bitmap(data, 8)
    >>> mapping = create_bitmap_data_mapping(bitmap, format='uint', item_length=8)
    >>> len(mapping)
    8
    >>> bitmap.count(1) == len(mapping)
    True
    >>> mapping
    [8, 11, 105, 116, 124, 143, 223, 250]
    >>> create_bitmap_data_mapping(bitmap, format='Bits', item_length=8)
    [Bits('0x08'), Bits('0x0b'), Bits('0x69'), Bits('0x74'), Bits('0x7c'), Bits('0x8f'), Bits('0xdf'), Bits('0xfa')]
    """
    mapping       = list()
    bitmap_length = len(bitmap)
    for i in range(0, bitmap_length):
        if (bitmap[i] == target_bitmap_value):
            if (format == 'uint'):
                value = i
            elif (format == 'Bits'):
                value = Bits(uint=i, length=item_length)
            elif (format == 'bin'):
                value = Bits(uint=i, length=item_length).bin
            elif (format == 'hex'):
                value = Bits(uint=i, length=item_length).hex
            else:
                raise Exception(f"Incorrect format: {format}")
            mapping.append(value)
    return mapping

def get_bitmap_value_from_number(bitmap: Bits, number: int, item_length: int = DEFAULT_VALUE_ITEM_LENGTH) -> Bits:
    """
    Получить значение элемента данных (обычно 1 байта) в исходных данных из любого числа
    """
    bitmap_length = len(bitmap)
    data_length   = bitmap_length * item_length
    if (item_length is None):
        item_length = get_item_based_data_length_from_number(data_length, item_length)
    mapping     = create_bitmap_data_mapping(bitmap, format='uint', item_length=item_length)
    # количество использованных значений из общего количества возможных
    value_count = bitmap.count(1)
    # значения которые нужно оставить без изменений
    skip_items  = collect_bitmap_keys(bitmap)
    # не преобразуем значения которые есть в словаре
    if (number in skip_items):
        return number
    # если в пространстве хешей содержится значение которое вообще не упоминается в данных - возвращаем вместо него
    # значение которое есть в данных, 
    if (number >= value_count):
        mapping_position = number % value_count
    else:
        mapping_position = number
    return Bits(uint=mapping[mapping_position], length=item_length)

def get_mapped_hash_item_value(bitmap: Bits, mapping: list, item_value: Bits) -> Bits:
    """
    Получить значение элемента из пространства хешей, отображенное через словарь возможных значений данных
    Позволяет получить гарантированно валидное значение 
    """
    # значения которые нужно оставить без изменений
    skip_items        = collect_bitmap_keys(bitmap)
    max_item_number   = bitmap.count(1)
    item_value_number = item_value.uint
    # не преобразуем элементы которые и так лежат в области допустимых значений
    if (item_value.uint in skip_items):
        return item_value
    # определяем позицию элемента данных в созданном отображении
    if (item_value_number >= max_item_number):
        position = item_value_number % max_item_number
    else:
        position = item_value_number
    return mapping[position]

def get_mapped_hash_item_position(data_bitmap: Bits, data_mapping: list, position_bitmap: Bits, position_mapping: list, seed: int) -> int:
    """
    TODO: по аналогии с get_mapped_hash_item_value возвращать только те позиции которые содержат только валидные значения данных
    Можно использовать матрицу: для каждого значения в bitmap создать еще один bitmap, который отображает 
    совпадение value из пространства хешей и из данных в разных позициях разных seed-значений
    """
    pass

# Функции сканирования пространства по слоям
# Главная идея такого подхода состоит в том чтобы выделить в начале пространства хешей столько же места сколько
# занимают исходные данные и затем при помощи перебора разных seed-значений сравнивать каждый байт хеш-пространства
# с байтом исходных данных в той же позиции
# результатом будет являться битовая карта для каждого seed-значения, которая определяет какие позиции исходных данных
# можно сохранить используя этот seed
# одновременно с битовой картой каждого seed для каждой позиции создается список seed-значений которые для нее подходят
# (что-то вроде обратного индекса)
# Перебор seed-значений происходит не последовательно а по ссылкам: после сканирования всех позиций одного seed 
# мы берем все совпадающие позиции и читаем новый seed из каждой из них (читая с того же места уже не 1 байт 
# как мы делали для данных а 3 байта и интерпретируя значение уже как новое seed-значение)
# при таком подходе мы работаем только с теми seed-значениями которые точно ссылаются друг на друга
# а значит составить связанный список из них будет гораздо проще чем из значений полученных перебором
# при получении байта данных данных из позиции хеша мы читаем значение не напрямую а используем
# битовую карту используемых значений, составленную по исходным данным
# Карта нужна чтобы сократить количество неподходящих элементов (в исходных данных какие-то значения байт могут просто
# ни разу не встречаться), а также (в будущем) она должна чаще возвращать те значения байт которые чаще встречаются в исходных
# данных и реже - те которые встречаются реже (но при этом сохраняя детерменизм в генерации результатов)
# Детерменизм можно сохранить сохранив вместе с картой количество вхождений каждого значения и при распаковке составить
# ее таким образом чтобы каждое значение встречалось там столько раз сколько и в исходных данных

def create_data_mapping_for_seed(data: ConstBitStream, seed: int, item_length: int = None, data_mapping: list = None, data_bitmap: Bits = None):
    """
    Создать битовую карту позиций в которых значение хеша и значение данных совпадают

    >>> item_length = 8
    >>> seed        = 0
    >>> data        = ConstBitStream(value_at_position(position=0*item_length, length=8*item_length, seed=seed))
    >>> seed_bitmap = create_data_mapping_for_seed(data, seed=seed, item_length=item_length)
    >>> data
    ConstBitStream('0xa6cd5e9392000f6a')
    >>> (len(seed_bitmap) == (len(data) // item_length))
    True
    >>> seed_bitmap.bin
    '11111111'
    >>> create_data_mapping_for_seed(ConstBitStream('0x00cd5e9992000fff'), seed=0, item_length=8).bin
    '01101110'
    >>> data        = ConstBitStream(value_at_position(position=0*item_length, length=64*item_length, seed=0))
    >>> seed_bitmap = create_data_mapping_for_seed(data, seed=3, item_length=item_length)
    >>> seed_bitmap.bin
    '0000000000000000000000000000000000000000000000001000001000000000'
    >>> (value_at_position(54*item_length, length=item_length, seed=0) == value_at_position(54*item_length, length=item_length, seed=3))
    True
    """
    seed_bitmap_length = get_item_based_data_length_from_number(len(data), item_length)
    if (data_bitmap is None):
        data_bitmap  = create_data_bitmap(data, item_length)
    if (data_mapping is None):
        data_mapping = create_bitmap_data_mapping(data_bitmap, format='Bits', item_length=item_length)
    seed_bitmap        = BitArray('0b0' * seed_bitmap_length)
    for position in range(0, seed_bitmap_length):
        # определяем координаты для чтения данных
        item_start       = position * item_length
        item_end         = item_start + item_length
        # получаем фрагмент данных нужной длины
        data_item        = data[item_start:item_end]
        # получаем данные из пространства хешей с той же позиции
        hash_item        = value_at_position(item_start, item_length, seed)
        # нормализуем полученное значение так чтобы оно гарантированно присутствовало в наших данных
        mapped_hash_item = get_mapped_hash_item_value(data_bitmap, data_mapping, hash_item)
        # проверяем совпадают ли значения элемента полученного из пространства хешей и элемента полученного из данных
        if (data_item.bin == mapped_hash_item.bin):
            # сохраняем результат совпадения в битовую карту
            seed_bitmap[position] = True
    return seed_bitmap

# базовые структуры данных

# id элемента в пространстве хешей
HashItemId     = namedtuple("HashItemId", ["seed", "position"])
# id списка из элементов данных
HashItemListId = namedtuple("HashItemListId", ["seed", "position"])

# классы данных, которые используют всю логику выше
# TODO: использовать слоты для уменьшения памяти: https://realpython.com/python-data-classes/#optimizing-data-classes

@dataclass
class SeedMapping:
    """
    Отображение массива данных на одно seed-пространство хешей
    TODO: накладывать данные на пространству хешей несколько раз в цикле 
          (как рулон обоев на стену)
          Размер пространства которое заполняется копиями файла определяется длиной seed-значения
          и размером элемента при разбиении (для 24 бит seed-значения и 8 бит на элемент это 2**24 позиций по 8 бит)
          Также имеет смысл протестировать диапазон размером (2 ** item_length) * (data_length // 8)
          То есть каждый элемент должен повториться там столько раз чтобы гарантированно встретиться 
          в нужной позиции хотя бы однажды
          В этом случае: 
          data_item_position == hash_item_position == input_seed, 
          output_seed == output_item_position == output_data_item_position
    """

    __slots__ = [
        'id',
        'seed', 
        'bitmap', 
        'item_positions', 
        'linked_lists', 
        'input_seeds', 
        'output_seeds', 
        'item_length', 
        'seed_length',
        'valid_positions_count',
        'data_positions_count',
        'data_bitmap',
        'data_length',
    ]

    # id отображения - совпадает с seed-значением
    id                    : int
    # число для определения пространства хешей
    seed                  : int
    # карта доступных позиций для этого seed-пространства
    bitmap                : Bits
    # индекс: все доступные позиции элементов, перечисленные как set
    # TODO: rename to item_positions
    item_positions        : Set[int]
    # все списки созданные в этом seed-пространстве
    linked_lists          : dict
    # все seed-пространства из которых доступен переход сюда
    input_seeds           : Set[int]
    # все seed-пространства в которые доступен переход отсюда
    output_seeds          : Set[int]
    # длина одного элемента в битах
    item_length           : int
    # длина одного seed-значения в битах
    seed_length           : int
    # общее количество доступных позиций в пространстве этого seed-значения
    valid_positions_count : int
    # количество элементов на которое разбиты все данные
    data_positions_count  : int
    # битовая карта используемых значений даных
    data_bitmap           : Bits
    # длина сиходных данных в битах
    data_length           : int

    def __init__(self, seed: int, data: ConstBitStream, data_mapping: list, data_bitmap: Bits, \
            item_length: int = DEFAULT_VALUE_ITEM_LENGTH, seed_length: int = DEFAULT_SEED_ITEM_LENGTH):
        self.id           = seed
        self.seed         = seed
        self.data_length  = len(data)
        self.data_bitmap  = data_bitmap
        self.item_length  = item_length
        self.seed_length  = seed_length
        # вычисляем битовую карту совпадающих участков пространства хешей и данных
        self.bitmap                = create_data_mapping_for_seed(data, seed, item_length, data_mapping, data_bitmap)
        # считаем количество совпадающих элементов среди всех позиций
        self.valid_positions_count = self.count_valid_positions()
        # считаем количество элементов в отображении
        self.data_positions_count  = self.count_data_positions()
        # по данным битовой карты:
        # TODO: заполнить позиции
        # TODO: заполнить input и output seeds
        # TODO: заполнить linked_lists
        self.item_positions = set()
        self.linked_lists   = dict()
        self.input_seeds    = set()
        self.output_seeds   = set()
    
    def count_valid_positions(self) -> int:
        if (self.bitmap is None) or (len(self.bitmap) == 0):
            raise Exception(f"self.bitmap is not set: {self.bitmap}")
        return self.bitmap.count(1)
    
    def count_data_positions(self) -> int:
        if (self.bitmap is None):
            raise Exception(f"self.bitmap is not set: {self.bitmap}")
        bitmap_length = len(self.bitmap)
        if (bitmap_length == 0):
            raise Exception(f"len(self.bitmap) is 0: {self.bitmap}")
        return bitmap_length
    
    def has_valid_positions(self) -> bool:
        if (self.valid_positions_count is None):
            raise Exception(f"self.valid_positions_count is not set: {self.valid_positions_count}")
        return (self.valid_positions_count > 0)
    

@dataclass(frozen=False)
class HashItem:
    """
    Элемент пространства хешей внутри указанного seed-пространства на выбранной позиции
    Может быть целиком создан из одного seed-числа

    >>> item_length  = 8
    >>> seed_length  = 24
    >>> seed         = 0
    >>> data         = ConstBitStream(value_at_position(position=0*item_length, length=8*item_length, seed=seed))
    >>> data
    ConstBitStream('0xa6cd5e9392000f6a')
    >>> data_bitmap  = create_data_bitmap(data, item_length=item_length)
    >>> data_mapping = create_bitmap_data_mapping(data_bitmap, format='Bits', item_length=item_length)
    >>> seed_bitmap  = SeedMapping(seed=seed, data=data, item_length=item_length, data_mapping=data_mapping, data_bitmap=data_bitmap)
    >>> item         = HashItem(seed, data, data_bitmap, seed_bitmap, data_mapping, item_length=item_length, seed_length=seed_length)
    >>> item.id
    HashItemId(seed=0, position=0)
    >>> item.seed
    0
    >>> item.item_position
    0
    >>> item.value
    Bits('0xa6')
    >>> item.data_item_value
    Bits('0xa6')
    >>> item.value == item.data_item_value
    True
    >>> item.is_valid_value
    True
    >>> item.output_seed
    10931550
    >>> item.output_item_position
    6
    >>> item.item_length == item_length
    True
    >>> item.seed_length == seed_length
    True
    >>> (value_at_position(item.item_position, item.item_length, item.seed) == item.value) == item.data_item_value
    True
    >>> (value_at_position(item.item_position, item.item_length, item.seed) == item.value) == item.is_valid_value
    True
    >>> next_seed_bitmap = SeedMapping(seed=item.output_seed, data=data, item_length=item_length, data_mapping=data_mapping, data_bitmap=data_bitmap)
    >>> item.is_next_seed(item.output_seed)
    True
    """

    __slots__ = [
        'id',
        'list_id',
        'seed', 
        'item_position', 
        'value', 
        'data_item_value',
        'output_seed', 
        'output_item_position',
        'output_item_id',
        'is_valid_value', 
        'item_length', 
        'seed_length',
        'data_length',
    ]

    # id элемента пространства хешей
    id                   : HashItemId
    # id списка в котором находится элемент
    list_id              : HashItemListId
    # число для определения пространства хешей
    seed                 : int
    # позиция элемента в пространстве хешей (должна совпадать с позицией элемента в исходных данных)
    item_position        : int
    # значение пространства хешей для этого seed в этой позиции
    value                : Bits
    # значение исходных данных в этой же позиции
    data_item_value      : Bits
    # указатель на следующий элемент (независимо от того существует ли он или пока еще не найден)
    output_seed          : int
    # позиция следующего элемента вычисляется из output_seed
    output_item_position : int
    # id следующего элемента, на который указывает этот
    output_item_id       : HashItemId
    # маркер того является ли значение (value) полученное из хешей корректным (то есть совпадающим с реальными данными)
    is_valid_value       : bool
    # длина значения
    item_length          : int
    # длина одного seed-значения в битах
    seed_length          : int
    # длина данных в битах
    data_length          : int

    def __init__(self, seed: int, data: ConstBitStream, data_bitmap: Bits, seed_bitmap: SeedMapping, data_mapping: list, \
            item_length: int = DEFAULT_VALUE_ITEM_LENGTH, seed_length: int = DEFAULT_SEED_ITEM_LENGTH):
        # seed-значение для xxhash: номер пространства хешей в котором находится элемент
        self.seed        = seed
        self.item_length = item_length
        self.seed_length = seed_length
        self.data_length = len(data)
        # параметры, вычисляемые от базовых
        self.item_position   = self.create_item_position(self.seed)
        self.value           = self.create_value(data_bitmap, data_mapping)
        # ссылка на следующий элемент
        self.output_seed          = self.create_output_seed()
        self.output_item_position = self.create_output_item_position()
        self.output_item_id       = HashItemId(self.output_seed, self.output_item_position)
        # проверка соответствия значения хеша значению данных в этой позиции
        self.data_item_value = self.create_data_value(data)
        self.is_valid_value  = self.get_is_valid_value()
        # уникальный id элемента
        self.id              = HashItemId(self.seed, self.item_position)
        self.list_id         = None

    def create_item_position(self, seed: int) -> int:
        """
        Получить из seed-значения номер позиции в пространстве хешей: в этой позиции биты пространства хешей 
        совпадают с битами исходных данных, длина совпадабщего отрезка определяется в self.item_length (в битах)
        """
        if (self.data_length is None):
            raise Exception(f"self.data_length is not set: {self.data_length}")
        if (self.item_length is None):
            raise Exception(f"self.item_length is not set: {self.item_length}")
        return get_data_item_position_from_data_length(self.data_length, seed, self.item_length)

    def create_output_seed(self) -> int:
        """
        Получить seed-число для следующего элемента цепочки хешей
        """
        if (self.seed_length is None):
            raise Exception(f"self.seed_length is not set: {self.seed_length}")
        if (self.seed is None):
            raise Exception(f"self.seed is not set: {self.seed}")
        return value_at_position(SEED_VALUE_START_POSITION, self.seed_length, self.seed).uint
    
    def create_output_item_position(self) -> int:
        """
        Alias для self.create_item_position(seed)
        """
        if (self.output_seed is None):
            raise Exception(f"self.output_seed is not set: {self.output_seed}")
        return self.create_item_position(self.output_seed)
    
    def create_value(self, data_bitmap: Bits, data_mapping: Bits) -> Bits:
        """
        Получить значение из пространства хешей в позиции, вычисленной из seed-значения
        """
        if (self.item_position is None):
            raise Exception(f"self.item_position is not set: {self.item_position}")
        if (self.seed is None):
            raise Exception(f"self.seed is not set: {self.seed}")
        if (self.item_length is None):
            raise Exception(f"self.item_length is not set: {self.item_length}")
        # получаем данные из пространства хешей с той же позиции
        item_bit_position = self.item_position * self.item_length
        hash_item_value   = value_at_position(item_bit_position, self.item_length, self.seed)
        # нормализуем полученное значение так чтобы оно гарантированно присутствовало в наших данных
        return Bits(get_mapped_hash_item_value(data_bitmap, data_mapping, hash_item_value))
    
    def create_data_value(self, data: ConstBitStream) -> Bits:
        """
        Получить значение элемента исходных данных из той же позиции на которой находится этот элемент пространства хешей
        """
        if (self.item_position is None):
            raise Exception(f"self.item_position is not set: {self.item_position}")
        if (self.item_length is None):
            raise Exception(f"self.item_length is not set: {self.item_length}")
        start = self.item_position * self.item_length
        end   = start + self.item_length
        return Bits(data[start:end])
    
    def get_is_valid_value(self) -> bool:
        """
        Соответствует ли значение хеша значению данных в этой же позиции 
        """
        if (self.value is None) or (len(self.value) == 0):
            raise Exception(f"self.value is not set: {self.value}")
        # проверяем соответствует ли значение пространства хешей значению данных в той же позиции
        return (self.value.bin == self.data_item_value.bin)

    def is_prev_seed(self, prev_output_seed: int) -> bool:
        """
        Является ли переданнное seed-значение предыдущим звеном цепочки хешей для текущего элемента
        """
        # проверяем указывает ли output_seed из предыдущего элемента на пространство в котором находится этот
        if (prev_output_seed != self.seed):
            return False
        # проверяем указывает ли output_position предыдущего элемента на позицию этого элемента
        prev_output_position = self.create_item_position(prev_output_seed)
        if (prev_output_position != self.item_position):
            return False
        return True
    
    def is_prev_position(self, prev_output_position: int) -> bool:
        """
        Указывает ли переданная следующая позиция на текущую стартовую позицию
        """
        return (prev_output_position == self.item_position)

    def is_next_seed(self, next_input_seed: int) -> bool:
        """
        Является ли переданнное seed-значение следующим звеном цепочки хешей для текущего элемента
        """
        # TODO: для некорректных значений отключена возможность прикреплять элементы спереди
        if (self.is_valid_value == False):
            return False
        # проверяем указывает ли этот элемент на нужное пространство хешей
        if (next_input_seed != self.output_seed):
            return False
        # проверяем указывает ли этот элемент на нужную позицию
        next_output_position = self.create_item_position(next_input_seed)
        if (next_output_position != self.output_item_position):
            return False
        return True
    
    def is_next_position(self, next_item_position: int) -> bool:
        return (self.output_item_position == next_item_position)


@dataclass
class HashItemList:
    """
    >>> item_length  = 8
    >>> seed_length  = 24
    >>> seed         = 0
    >>> data         = ConstBitStream(value_at_position(position=0*item_length, length=8*item_length, seed=seed))
    >>> data
    ConstBitStream('0xa6cd5e9392000f6a')
    >>> data_bitmap  = create_data_bitmap(data, item_length=item_length)
    >>> data_mapping = create_bitmap_data_mapping(data_bitmap, format='Bits', item_length=item_length)
    >>> seed_bitmap  = SeedMapping(data=data, seed=seed, item_length=item_length, data_mapping=data_mapping, data_bitmap=data_bitmap)
    >>> item         = HashItem(seed, data, data_bitmap, seed_bitmap, data_mapping, item_length=item_length, seed_length=seed_length)
    >>> item
    HashItem(id=HashItemId(seed=0, position=0), list_id=None, seed=0, item_position=0, value=Bits('0xa6'), data_item_value=Bits('0xa6'), output_seed=10931550, output_item_position=6, output_item_id=HashItemId(seed=10931550, position=6), is_valid_value=True, item_length=8, seed_length=24, data_length=64)
    >>> item.list_id == None
    True
    >>> hash_list = HashItemList(item)
    >>> hash_list.id
    HashItemListId(seed=0, position=0)
    >>> hash_list.head.id
    HashItemId(seed=0, position=0)
    >>> hash_list.head.list_id == hash_list.id
    True
    >>> hash_list.tail is None
    True
    >>> hash_list.head.value
    Bits('0xa6')
    >>> hash_list.head.value == hash_list.head.data_item_value
    True
    >>> hash_list.head.is_valid_value
    True
    >>> hash_list.status
    'active'
    >>> hash_list.total_steps
    1
    >>> hash_list.visited_seeds
    {0}
    >>> hash_list.visited_item_positions
    {0}
    >>> hash_list.visited_values
    {Bits('0xa6')}
    >>> hash_list.visited_item_ids
    deque([HashItemId(seed=0, position=0)])
    >>> hash_list.item_position_counts
    Counter({0: 1})
    >>> hash_list.value_counts
    Counter({Bits('0xa6'): 1})
    >>> seed_bitmap  = SeedMapping(data=data, seed=item.output_seed, item_length=item_length, data_mapping=data_mapping, data_bitmap=data_bitmap)
    >>> new_head     = HashItem(item.output_seed, data, data_bitmap, seed_bitmap, data_mapping, item_length=item_length, seed_length=seed_length)
    >>> new_head.id
    HashItemId(seed=10931550, position=6)
    >>> updated_head = hash_list.update_head(new_head)
    >>> updated_head.id
    HashItemId(seed=10931550, position=6)
    >>> hash_list.head.id == updated_head.id == new_head.id
    True
    >>> hash_list.head.list_id == updated_head.list_id == new_head.list_id == hash_list.id
    True
    >>> hash_list.tail.id
    HashItemId(seed=0, position=0)
    >>> hash_list.tail.list_id == hash_list.id
    True
    >>> hash_list.total_steps
    2
    >>> hash_list.visited_seeds
    {0, 10931550}
    >>> hash_list.visited_item_positions
    {0, 6}
    >>> hash_list.visited_values
    {Bits('0xa6'), Bits('0x0f')}
    >>> hash_list.visited_item_ids
    deque([HashItemId(seed=0, position=0), HashItemId(seed=10931550, position=6)])
    >>> hash_list.item_position_counts
    Counter({0: 1, 6: 1})
    >>> hash_list.value_counts
    Counter({Bits('0xa6'): 1, Bits('0x0f'): 1})
    >>> hash_list.tail.output_seed == hash_list.head.seed
    True
    >>> hash_list.tail.output_item_position == hash_list.head.item_position
    True
    >>> hash_list.tail.output_item_id == hash_list.head.id
    True
    """

    __slots__ = [
        'id',
        'head', 
        'tail', 
        'init_item_position',
        'init_seed', 
        'visited_seeds', 
        'visited_values',
        'visited_item_positions',
        'visited_item_ids',
        'item_position_counts', 
        'value_counts', 
        'status', 
        'merged_children',
        'merge_target_id',
        'total_steps',
    ]

    # уникальный id списка: сочетание стартового seed и position: (seed, position)
    id                     : HashItemListId
    # первый элемент списка (всегда указывает на один элемент впереди)
    head                   : HashItem
    # последний элемент списка (к нему добавляются новые значения, на него могут указывать несколько элементов)
    tail                   : HashItem
    # позиция в которой список был создан: не может меняться в отличии от head и tail
    init_item_position     : int
    # слой seed-значений в котором список был изначально создан: не может меняться
    init_seed              : int
    # список всех seed-значений через которые проходит список
    visited_seeds          : Set[int]
    # все посещенные этим списком уникальные значения
    visited_values         : Set[str]
    # все посещенные этим списком уникальные позиции
    visited_item_positions : Set[int]
    # все элементы, посещенные списком (seedId, positionId)
    visited_item_ids       : Deque[HashItemId]
    # TODO: все посещенные этим списком позиции, распределенные по seed-пространствам
    # visited_seed_positions : dict
    # количество посещений каждой позиции
    item_position_counts   : Counter
    # количество посещений каждого значения
    value_counts           : Counter
    # статус списка: 
    # active   - растет: в head-элементе доступен не исследованный переход по next_seed 
    # pending  - ждет: head-элемент может продолжить рост когда появится новый элемент, подходящий под условия
    # finished - завершен: следующий элемент указывает на seed с невалидным значением 
    #            (полученное после преобразования значение не совпадает со значением данных в этой позиции)
    #            к таким спискам можно добавить значение только в начало, но не в конец
    # merged   - включен в состав другого списка: повторяет все его дальнейшие переходы, не может быть дополнен
    #            с конца, но могут быть добавлены элементы в начало
    status            : str
    # TODO: все списки которые поглощаются этим списком - то есть указывают на какой-то из его элементов
    merged_children   : set
    # TODO: id списка, с которым произошло слияние (None если слияние не произошло)
    merge_target_id   : HashItemListId
    # длина списка как количество переходов между элементами
    total_steps       : int

    def __init__(self, item: HashItem):
        self.id                 = HashItemListId(item.seed, item.item_position)
        self.head               = None
        self.tail               = None
        self.status             = 'active'
        self.merge_target_id    = None
        self.total_steps        = 0
        self.init_seed          = item.seed
        self.init_item_position = item.item_position

        self.item_position_counts   = Counter()
        self.value_counts           = Counter()
        self.visited_values         = set()
        self.visited_item_positions = set()
        self.visited_item_ids       = deque()
        self.visited_seeds          = set()
        self.merged_children        = set()

        self.update_head(item)
    
    def update_stats(self, item: HashItem):
        """
        Обновить счетчики посещенных списком элементов и списки использованных значений
        """
        # обновляем счетчики
        self.total_steps += 1
        self.item_position_counts.update({item.item_position: 1})
        count_key = item.value
        self.value_counts.update({count_key: 1})
        # сохраняем список посещенных элементов
        self.visited_values.add(item.value)
        self.visited_seeds.add(item.seed)
        self.visited_item_positions.add(item.item_position)
        # сохраняем порядок добавления элементов в список
        if (item.id == self.head.id):
            self.visited_item_ids.append(item.id)
        elif (item.id == self.tail.id):
            self.visited_item_ids.appendLeft(item.id)
        # если получен невалидный первый элесент - останавливаем рост списка
        if (self.head.is_valid_value is False):
            self.status = 'finished'
    
    def update_head(self, item: HashItem) -> HashItem:
        """
        Добавить элемент в начало списка
        """
        if (not self.is_new_head(item)):
            raise Exception(f"Incorrect item ({item}) for new head: {self.head}")
        if (self.head is not None):
            if (self.tail is not None):
                raise Exception(f"Non empty tail for new head: {item}")
            old_head  = self.head
            self.tail = old_head
        item.list_id = self.id
        self.head    = item
        self.update_stats(item)
        return self.head
    
    def update_tail(self, item: HashItem) -> HashItem:
        """
        Добавить элемент в конец списка
        """
        if (not self.is_new_tail(item)):
            raise Exception(f"Incorrect item ({item}) for new tail: {self.tail}")
        item.list_id = self.id
        self.tail    = item
        self.update_stats(item)
        return self.tail

    def is_new_head(self, item: HashItem) -> bool:
        if (self.head is None):
            return True
        if (item.seed != self.head.output_seed):
            return False
        if (item.item_position != self.head.output_item_position):
            return False
        if (item.item_length != self.head.item_length):
            return False
        if (item.seed_length != self.head.seed_length):
            return False
        # TODO: проверка value + is_valid
        return True

    def is_new_tail(self, item: HashItem) -> bool:
        if (self.tail is None):
            if (self.head is None):
                raise Exception(f"Head must not be empty when updating tail")
            seed          = self.head.seed
            item_position = self.head.item_position
            item_length   = self.head.item_length
            seed_length   = self.head.seed_length
        else:
            seed          = self.tail.seed
            item_position = self.tail.item_position
            item_length   = self.tail.item_length
            seed_length   = self.tail.seed_length
        if (item.output_seed != seed):
            return False
        if (item.output_item_position != item_position):
            return False
        if (item.item_length != item_length):
            return False
        if (item.seed_length != seed_length):
            return False
        # TODO: проверка value + is_valid
        return True
    
    def collect_items_ids(self, start_item_id: HashItemId = None) -> List[HashItemId]:
        """
        Получить все элементы списка начиная с указанного (включительно)
        """
        item_ids = []
        if (self.tail is None):
            if (start_item_id is not None) and (start_item_id != self.head.id):
                raise Exception(f"Incorrect start_item_id: {start_item_id}")
            item_ids.append(self.head.id)
            return item_ids
        if (start_item_id == None):
            start_item_id = self.tail.id
        # ищем и собираем все элементы списка
        start_item_detected = False
        for item_id in self.visited_item_ids:
            if start_item_detected or (item_id == start_item_id):
                item_ids.append(item_id)
                start_item_detected = True
        if (start_item_detected == False):
            raise Exception(f"Incorect start_item_id={start_item_id} for list (id={self.id}) containing items {self.visited_item_ids}")
        return 
    
    def merge_to_list_item(self, target_list_item: HashItem):
        """
        Выполнить слияние текущего списка с новым
        """
        # TODO: проходим все элементы списка по ссылкам
        merge_item_ids = self.collect_items_ids(start_item_id=target_list_item.id)
        #       TODO: получаем список элементов по id (TODO: возможность вычислять элементы без data)
        #             (TODO: функции получения списка элементов, элемента хеша, элемента данных по их id)
        # TODO: обновляем счетчики каждым элементом нового списка
        # TODO: указываем ссылку на список
        if (self.merge_target_id is not None):
            raise Exception(f"Merge list (id={self.id}) error: new target list_id={target_list_item.list_id} try to overwrite existing self.merge_target_id={self.merge_target_id}")
        self.merge_target_id = target_list_item.list_id
        # TODO: обновляем статус
        self.status = 'merged'
        

@dataclass
class DataMapping:
    """
    Отображение массива данных на все доступные пространства хешей
    Используется в процессе сжатия, накапливает все возможные связи элементов между собой в виде связанных списков
    постоянно дополняется новыми значениями
    Ранее созданные списки следят за добавлением новых элементов и дополняют себя слева или справа если видят подходящее значение
    Для всех новых элементов возможны только 3 сценария:
    - нет списков, которые могли бы быть дополнены этим элементом слева или справа: создается новый список из 1 элемента
    - список может быть дополнен справа: заменяется head, обновляются данные списка
    - список может быть дополнен слева: заменяется tail, обновляются данные списка
    - один список может быть дополнен справа, а второй - слева: произвести слияние двух списков
    При слиянии список у которого заменили head переходит в статус merged и больше не проверяется при появлении новых значений

    >>> item_length  = 8
    >>> seed_length  = 24
    >>> seed         = 0
    >>> data         = ConstBitStream(value_at_position(position=0*item_length, length=8*item_length, seed=seed))
    >>> data
    ConstBitStream('0xa6cd5e9392000f6a')
    >>> mapping = DataMapping(data)
    >>> mapping.data == data
    True
    >>> mapping.item_length == item_length
    True
    >>> mapping.seed_length == seed_length
    True
    >>> mapping.seeds
    {0}
    >>> mapping.item_positions
    {0, 1, 2, 3, 4, 5, 6, 7}
    >>> mapping.seed_mappings[0].bitmap
    BitArray('0xff')
    >>> mapping.bitmap
    BitArray('0x8001000000000000000000020020000000003000020000000004000000000000')
    >>> mapping.new_seeds
    {9671168, 1010372, 3946, 6997067, 9568271, 6198162, 13459091, 10931550}
    >>> mapping.process_new_seeds()
    {9671168, 1010372, 3946, 6997067, 9568271, 6198162, 13459091, 10931550}
    >>> mapping.seeds
    {0, 9671168, 1010372, 3946, 6997067, 9568271, 6198162, 13459091, 10931550}
    >>> mapping.new_seeds
    {3250944, 5184963, 6310437, 11309573, 8611275, 4895052, 15855641, 16591317, 9778007, 16320441, 4995647}
    >>> data    = ConstBitStream(value_at_position(position=0*item_length, length=512*item_length, seed=1))
    >>> mapping = DataMapping(data)
    >>> mapping.new_seeds
    {11960649, 10787146, 7352741}
    >>> mapping.seeds
    {0}
    >>> len(mapping.new_items[0]) == 1
    True
    >>> mapping.new_items[0][0] == (0, 0)
    True
    >>> mapping.process_new_seeds()
    {11960649, 10787146, 7352741}
    >>> mapping.new_items[0].keys()
    dict_keys([0])
    >>> mapping.new_items[11960649].keys()
    dict_keys([329])
    >>> mapping.new_items[7352741].keys()
    dict_keys([421])
    >>> mapping.new_seeds
    {5398848, 11385890, 12153513, 1758030, 2529748, 2159832, 1993241, 1592284, 13104510}
    >>> mapping.process_new_seeds()
    {5398848, 11385890, 12153513, 1758030, 2529748, 2159832, 1993241, 1592284, 13104510}
    >>> mapping.new_seeds
    {263432, 5593229, 1762464, 7909029, 5221797, 11121449, 8218794, 11783998, 15469944, 1271629, 15363791, 14078816, 3969893, 4615657, 8050801, 2145009, 8392702, 1742840, 7445754, 2025851}
    >>> len(mapping.new_seeds)
    20
    >>> mapping.process_new_seeds(limit=2)
    {263432, 5593229}
    >>> len(mapping.new_seeds)
    21
    >>> mapping.process_new_seeds(limit=2, priority_seeds={3969893, 8050801})
    {8050801, 11160451, 3969893, 6422918}
    >>> 3969893 not in mapping.new_seeds
    True
    >>> 8050801 not in mapping.new_seeds
    True
    """

    # исходные данных без изменений
    data                  : ConstBitStream
    # кеш отображения словаря исходных данных для пространства хешей
    data_mapping          : list
    # все просканированные на данный момент seed-значения (пространства)
    seeds                 : Set[int]
    # все позиции, посещенные хотя бы одним списком в любом seed-пространстве
    item_positions        : Set[int]
    # карта заполненных (содержащихся хотя бы в одном списке) позиций
    # (то же что и список positions, но в виде битовой карты)
    positions_bitmap      : Bits
    # количество заполненных позиций элементами данных из разных списков (len для positions)
    item_positions_count  : int
    # общее количество элементов которое нужно найти и заполнить через списки
    max_item_positions    : int
    # отображения данных на различные пространства хеш-значений (ключи словаря - seed-значения)
    # TODO: переименовать в seed_mapping
    seed_mappings         : Dict[int, SeedMapping]
    # все связанные списки которые были созданы на данный момент (ключи словаря - seed-значения)
    linked_lists          : Dict[int, Dict[int, HashItemList]]
    # новые seed-значения для которых еще не создано отображение
    new_seeds             : Set[int]
    # новые элементы, которые ждут добавления в список
    new_items             : Dict[int, Dict[int, HashItemId]]
    # общее количество добавленных уникальных пар (seed, position)
    new_items_count       : Dict[int, Dict[HashItemId, int]]
    # длина одного элемента в битах
    item_length           : int = DEFAULT_VALUE_ITEM_LENGTH
    # длина одного seed-значения в битах
    seed_length           : int = DEFAULT_SEED_ITEM_LENGTH
    # длина данных в битах
    data_length           : int = 0
    # самое большое количество заполненных позиций одним списком
    max_list_positions    : int = 0
    # длина самого большого связанного списка
    max_list_length       : int = 0
    # карта используемых вариантов значений элемента
    bitmap                : Bits = None

    def __init__(self, data: ConstBitStream, item_length: int = DEFAULT_VALUE_ITEM_LENGTH, seed_length: int = DEFAULT_SEED_ITEM_LENGTH):
        self.data           = data
        self.seeds          = set()
        self.item_positions = set()
        self.new_seeds      = set()
        self.new_items      = dict()
        self.seed_mappings  = dict()
        self.linked_lists   = dict()
        # вычисляем стартовые параметры для исходных данных
        self.data_length          = len(data)
        self.item_length          = item_length
        self.seed_length          = seed_length
        self.item_positions_count = len(self.item_positions)
        self.new_items_count      = 0
        self.max_item_positions   = get_item_based_data_length_from_number(self.data_length, self.item_length)
        self.bitmap               = create_data_bitmap(data, self.item_length)
        self.data_mapping         = create_bitmap_data_mapping(self.bitmap, format='Bits', item_length=item_length)
        
        # инициируем стартовый список seed-значений
        self.process_new_seed(0)

    def process_new_seed(self, seed: int):
        """
        Создать отображение данных на новое пространство: 
        - создает отображение каждого элемента данных в хеш-пространство
        - определяет какие из элементов совпадают
        - для всех совпадающих элементов вычисляет output_seed
        - добавляет все output_seed в очередь на сканирование
        Таким образом мы каждый раз выбираем новые seed-значенмя только из тех, которые связаны между собой
        """
        # проверяем, сканировали ли мы уже это пространство
        if (seed in self.seeds):
            self.new_seeds.discard(seed)
            return
        # создать отображение данных для этого пространства
        seed_mapping = self.create_seed_mapping(seed)
        # отображение не содержит ни одного совпадающего элемента данных - не обрабатываем этот seed
        if (seed_mapping.has_valid_positions() is False):
            # регистрируем элемент в списке обработанных
            self.seeds.add(seed)
            # убираем обработанный элемент из списка новых
            self.new_seeds.discard(seed)
            # убираем отображение на это пространство из кеша
            self.seed_mappings[seed] = None
            return
        # обрабатываем все ссылки из этоtupleго seed на следующие
        self.collect_new_seeds_from_mapping(seed)
        # регистрируем элемент в списке обработанных
        self.seeds.add(seed)
        # убираем обработанный элемент из списка новых
        self.new_seeds.discard(seed)
        # создаем элемент списка из нового seed
        new_item = HashItem(seed=seed, data=self.data, data_bitmap=self.bitmap, seed_bitmap=seed_mapping, data_mapping=self.data_mapping,\
            item_length=self.item_length, seed_length=self.seed_length)
        # добавляем новый элемент к связанным спискам
        self.process_new_item(new_item)
    
    def process_new_seeds(self, limit: int = 1000, priority_seeds: set=set()) -> set:
        """
        Обработать список новых seed-значений, извлекая из каждого из них новое связанное seed-значение
        """
        seeds = list()
        # добавляем новые seed-значения если автоматически генерируемые закончились 
        if (len(self.new_seeds) == 0):
            extra_new_seed = max(self.seeds) + 1
            priority_seeds.add(extra_new_seed)
        # добавляем в начало списка те элементы которые нужно обработать первыми
        if (len(priority_seeds) > 0):
            priority_list = list(priority_seeds.intersection(self.new_seeds))
            if (limit is not None) and (limit > 0):
                if (limit < len(priority_seeds)):
                    seeds.extend(priority_list[0:limit])
                else:
                    seeds.extend(priority_list)
        # добавляем остальные элементы
        if (limit is not None) and (limit > 0):
            if (limit < len(self.new_seeds)):
                seeds.extend(list(self.new_seeds)[0:limit])
            else:
                seeds.extend(list(self.new_seeds))
        else:
            seeds.extend(list(self.new_seeds))
        # обрабатываем seed-значения с учетом приоритета и ограничений
        seed_count = 0
        for seed in seeds:
            seed_count = seed_count + 1
            if (seed in self.seeds):
                self.new_seeds.discard(seed)
                continue
            self.process_new_seed(seed)
            if (limit is not None) and (limit > 0) and (seed_count >= limit):
                msg = f"process_new_seeds(): limit reached at seed={seed} (step={seed_count})"
                log.debug(msg)
                break
        return set(seeds)
    
    def process_new_item(self, item: HashItem):
        # увеличиваем счетчик обработанных пар (seed, position)
        self.new_items_count += 1
        # for linked_list in seed_lists:
        # проверяем все текущие списки и ищем в них доступные элементы для стыковки
        if (item.seed not in self.new_items):
            self.new_items[item.seed] = dict()
        
        # проверяем наличие словаря сприсков позиции пространства
        seed_item_positions = self.new_items[item.seed]
        if (item.item_position not in seed_item_positions):
            self.new_items[item.seed][item.item_position] = item.id
        # добавляем в список элементов ожидающих в этой позиции id нового элемента
        #self.new_items[item.seed][item.item_position].add(item.id)
        # TODO: добавить в элемент id списка
        # TODO: при добавлении нового элемента проверять все доступные 
        #       для дополнения списки в позиции (seed, position) и дополнять каждый из них, который можно дополнить
        #       заменить этим механизмом вложенный перебор seed + seed_positions
        
        # проверяем наличие словаря списков пространства
        if (item.seed not in self.linked_lists):
            self.linked_lists[item.seed] = dict()
        
        # проверяем наличие словаря списков позиции пространства
        seed_item_positions = self.linked_lists[item.seed]
        if (item.item_position not in seed_item_positions):
            item_list = HashItemList(item)
            self.linked_lists[item.seed][item.item_position] = item_list
        else:
            # список на этой позиции уже существует
            existing_seed_position_list = self.linked_lists[item.seed][item.item_position]
            msg = f"Linked list already created: id={existing_seed_position_list.id}"
            log.info(msg)
        # TODO: обработка слияния списков
        # ищем можно ли продолжить любой из ранее созданных списков
        active_seeds = list(self.linked_lists.keys())
        for active_seed in active_seeds:
            seed_positions = list(self.linked_lists[active_seed].keys())
            for active_position in seed_positions:
                seed_position_list = self.linked_lists[active_seed][active_position]
                # первый элемент можно добавлять только новым спискам или спискам которые имеют валидный head-элемент
                if (seed_position_list.status in ['pending', 'active']):
                    # список не поглощен другим списком, не содержит кольцевых ссылок и не упирается
                    # в несовместимое значение - значит его возможно дополнить спереди
                    if (seed_position_list.is_new_head(item)):
                        seed_position_list.update_head(item)
                        msg = f"NEW HEAD: seed={active_seed} [new_items_count={self.new_items_count}], position={active_position}: list_length={seed_position_list.total_steps}, (list.id={seed_position_list.id}), max={self.max_list_length}"
                        log.info(msg)
                        # убираем элемент из списка ожидания
                        if (self.max_list_length < seed_position_list.total_steps):
                            self.max_list_length = seed_position_list.total_steps
                            msg = f"NEW MAX LENGTH: {self.max_list_length}, (list.id={seed_position_list.id})"
                            log.info(msg)
                    # если первый элемент списка содержит корректное значение - то пробуем на следующем шаге дополнить
                    # его спереди еще раз, продолжая до тех пор пока первый элемент не наткнется на некорректное значение 
                    # или на себя самого
                    if (seed_position_list.head.is_valid_value):
                        # для этого получаем из первого элемента списка id пространства хешей на которое он указывает
                        # и добавляем его в очередь на обработку
                        # при следующем вызове 
                        self.new_seeds.add(seed_position_list.head.output_seed)
                    # TODO: удалять id списка из очереди списков доступных для дополнения спереди если новый head не валиден
                # добавлять элементы в конец можно любым спискам
                if (seed_position_list.status in ['active', 'pending', 'finished', 'merged']):
                    if (seed_position_list.is_new_tail(item)):
                        seed_position_list.update_tail(item)
                        msg = f"NEW TAIL: seed={active_seed} [new_items_count={self.new_items_count}], item_position={active_position}: list_length={seed_position_list.total_steps}, (list.id={seed_position_list.id}), max={self.max_list_length}"
                        log.info(msg)
                        if (self.max_list_length < seed_position_list.total_steps):
                            self.max_list_length = seed_position_list.total_steps
                            msg = f"NEW MAX LENGTH: {self.max_list_length}, (list.id={seed_position_list.id})"
                            log.info(msg)
        
        #return self.new_items[item.seed][item.item_position]
    
    def check_item_position(self, item: HashItem):
        """
        Проверить позицию элемента на наличие ссылок, получить списки, которые можно дополнить этим элементом
        """
        pass
    
    def create_seed_mapping(self, seed: int) -> SeedMapping:
        """
        Создать битовую карту отображения данных на пространство хешей, на которой отмечены совпадающие позиции
        """
        if (seed not in self.seed_mappings):
            self.seed_mappings[seed] = SeedMapping(seed=seed, data=self.data, data_mapping=self.data_mapping, data_bitmap=self.bitmap,\
                item_length=self.item_length, seed_length=self.seed_length)
        return self.seed_mappings[seed]
    
    def collect_new_seeds_from_mapping(self, seed: int) -> set:
        """
        Получить возможные переходы в другие seed-пространства из указанного seed-пространства 
        доступные для текущего разбиения исходных данных
        """
        # список seed-значений которые собраны за этот проход
        collected_seeds     = set()
        # получаем список доступных переходов
        seed_mapping        = self.seed_mappings[seed]
        # нет ни одного доступного перехода из указанного seed-пространства - прерываем поиск
        if (seed_mapping is None) and (seed in self.seeds):
            return collected_seeds
        
        # проверяяем отображение данных на пространство хешей в поисках ссылок на другие seed-пространства
        for item_position in range(0, len(seed_mapping.bitmap)):
            # проверяем все элементы, ищем те в которых значение данных и значение пространства хешей совпадают
            if (seed_mapping.bitmap[item_position] == True):
                # позиция в хеш-пространстве - в битах, поэтому чтобы ее получить нужно умножить позицию элемента на его длину
                hash_position = item_position * self.item_length
                # получаем номер seed-пространства в которое возможен переход из этой позиции 
                # изнутри текущего отображения данных на переданное пространство хешей (переданный seed)
                hash_seed     = value_at_position(hash_position, self.seed_length, seed).uint
                # заполняем посещенные позиции
                self.item_positions.add(item_position)
                if (hash_seed not in self.seeds) and (hash_seed not in self.new_seeds):
                    # добавляем новое seed-пространство в список на сканирование
                    self.new_seeds.add(hash_seed)
                # добавляем собранные из этого пространства значения в отдельный список
                collected_seeds.add(hash_seed)
        # обновляем счетчик посещенных позиций
        self.item_positions_count = len(self.item_positions)
        # возвращаем только те seed-значения которые были собраны этот проход, без фильтрации
        return collected_seeds

@dataclass
class DataItemsCollection:
    """
    Коллекция всех элементов данных:
    - при сжатии: список оставшихся элементов - дополняется по 1 элементу
    - при распаковке: список найденных элементов - уменьшается по 1 элементу за шаг
    Всегда выдает непосещенный элемент в ответ на позицию
    Содержит список DataItem
    """

@dataclass
class DataItem:
    """
    Элемент данных без привязки к хеш-пространству
    """

    __slots__ = [
        'id',
        'item_position',
        'seeds', 
        'value',
        'item_length',
        'seed_length',
    ]

    # id элемента данных - совпадает с порядковым номером (позицией) элемента
    id                 : int
    # номер элемента в исходных данных
    item_position      : int
    # все seed-значения для которых значение пространства хешей в этой позиции совпадает со значением даных
    seeds              : set
    # значение элемента данных в этой позиции
    value              : Bits
    # длина одного элемента в битах
    item_length        : int
    # длина одного seed-значения в битах
    seed_length        : int

    def __init__(self, value: Bits, item_position: int, item_length: int = DEFAULT_VALUE_ITEM_LENGTH, seed_length: int = DEFAULT_SEED_ITEM_LENGTH):
        self.id            = item_position
        self.value         = value
        self.item_position = item_position
        self.item_length   = item_length
        self.seed_length   = seed_length
        self.seeds         = set()
    
    def add_seed(self, seed: int):
        self.seeds.add(seed)

@dataclass
class DataItemPosition:
    """
    Позиция в пространстве хешей с привязкой к данным
    Используется при построении цепочек хешей, служит контейнером для группировки всех входящих и исходящих ссылок
    для одной позиции элемента данных
    Содержит только валидные элементы - то есть те для которых значение элемента в хеш-пространстве и данных совпадает
    """
    # позиция элемента в пространстве хешей (должна совпадать с позицией элемента в исходных данных)
    position         : int
    # словарь-индекс: ключами являются все использованные seed-числа в этой позиции, значениями - объекты HashItem
    items            : dict
    # все seed-значения которые указывают сюда (на эту позицию)
    input_seeds      : set
    # все seed-значения, доступные из этой позиции (во всех позициях)
    output_seeds     : set
    # все позиции, доступные из этой позиции (во всех seed-пространствах)
    output_positions : set
    # значение хеша в этой позиции: может быть только одно-единственное и должно совпадать со значением данных в такой же позиции
    item_value       : set
    # длина значения
    item_length      : int

@dataclass
class HashItemPosition:
    """
    Позиция элемента в пространстве хешей (без привязки к данным)
    Сохраняется в базу и используется при повторных поисках
    """
    
    # id элемента данных - совпадает с порядковым номером (позицией) элемента
    id                 : int
    # номер элемента в исходных данных
    item_position      : int
    # seed-значение при котором в этой позиции содержится указанное значение (value)
    seed               : int
    # значение элемента данных в этой позиции
    value              : Bits
    # длина элемента в битах
    item_length        : int
    # длина seed-значения в битах
    seed_length        : int

    def __init__(self, seed: Bits, item_position: int, item_length: int = DEFAULT_VALUE_ITEM_LENGTH, seed_length: int = DEFAULT_SEED_ITEM_LENGTH):
        self.item_position = item_position
        self.item_length   = item_length
        self.seed_length   = seed_length
        self.seed          = seed
        self.id            = self.item_position
        self.value         = self.get_value()
    
    def get_value(self):
        if (self.item_position is None):
            raise Exception(f"self.item_position is not set: {self.item_position}")
        if (self.item_length is None) or (len(self.item_length) == 0):
            raise Exception(f"self.item_length is not set: {self.item_length}")
        # позиция в хеш-пространстве - в битах, поэтому чтобы ее получить нужно умножить позицию элемента на его длину
        value_bit_position = self.item_position * self.item_length
        # получаем номер seed-пространства в которое возможен переход из этой позиции 
        # изнутри текущего отображения данных на переданное пространство хешей (переданный seed)
        return value_at_position(value_bit_position, self.item_length, self.seed)

# TODO: класс Seed, и класс hash_space_header()
# TODO: определить классы Seed, HashRange, HashPosition, HashValue, DataPosition, DataValue, LinkedHashList
# TODO: в классе Seed задать правила для входных данных (параметры необходимые для создания нового элемента списка):
#       - input seed
#       - output seed (next seed)
#       - input/output data item position
#       - input/output hash item position
#       - input/output data value (1 byte)
#       - input/output data item length
#       - input seed length
#       - output seed length
# TODO: элементы-списки (одиночные) добавляются при каждом проходе по всей длине содержимого 
#       при обнаружении позиций в которых значение data и hash_item_data совпадают.
#       Каждый список делает 1 шаг вперед (если возможен переход дальше), после чего
#       или продолжает движение в следующий ход, или переходит в режим ожидания (ждет новых значений)
#       или окончательно останавливается если next_seed указывает на позицию с некорректным значением
#       либо если указатель создаст кольцевую ссылку на себя в этой точке
# TODO: основные задачи: 
#       - просканировать все байты файла, вычислить позиции в которых файл совпадает с текущим seed
#       - добавить новый список-observer
#       - зарегистрировать список в указанной позиции (seed + hash_position + data)
#       - дополнить элемент списка
#       - обновить статистику списка после добавления нового значения
#       - сменить статус (waiting/finished/active)
#       - произвести слияние списка (список, который пересек элементы другого списка не исчезает, 
#       - но начинает повторять все команды, т. к. последовательность детерминирована)

@dataclass
class SeedNode:
    # hash space positions, used to create values for next list item (next seed and current data value)
    SEED_VALUE_START_POSITION : int = 0
    ITEM_VALUE_START_POSITION : int = 0
    
    # default linked list node props
    next  = None
    prev  = None
    value = None

    input_seed  : int = None
    output_seed : int = None

    input_hash_position    : int  = None
    output_hash_position   : int  = None
    input_hash_item_value  : Bits = None
    output_hash_item_value : Bits = None
    
    input_data_position    : int  = None
    output_data_position   : int  = None
    input_data_item_value  : Bits = None
    output_data_item_value : Bits = None

    input_seed_length        : int = DEFAULT_SEED_ITEM_LENGTH
    output_seed_length       : int = DEFAULT_SEED_ITEM_LENGTH
    input_value_item_length  : int = DEFAULT_VALUE_ITEM_LENGTH
    output_value_item_length : int = DEFAULT_VALUE_ITEM_LENGTH
    
    # output only: seed valid for current data, hash value extracted from position contains correct value
    is_valid = None

    def __init__(self, seed: int, next_node = None, prev_node = None, data_length: int = None, data_bitmap: Bits = None):
        self.value      = seed
        self.next       = next_node
        self.prev       = prev_node
        # saved file metadata
        self.data_length = data_length
        self.data_bitmap = data_bitmap
        # item-specific setup
        self.input_seed  = self.value
        self.output_seed = self.create_output_seed()
        # positions
        self.input_hash_position  = self.get_input_hash_position()
        self.output_hash_position = self.create_output_hash_position()
        self.input_data_position  = self.get_input_data_position()
        self.output_data_position = self.create_output_data_position()
        # data item value
        self.input_hash_item_value  = self.get_input_hash_item_value()
        self.output_hash_item_value = self.create_output_hash_item_value()
        self.input_data_item_value  = self.get_input_data_item_value()
        self.output_data_item_value = self.create_output_data_item_value()

    def init_data_options(self, data_length: int = None, data_bitmap: Bits = None):
        self.data_length = data_length
        self.data_bitmap = data_bitmap
    
    def get_input_seed(self) -> int:
        return self.input_seed

    def create_output_seed(self) -> int:
        return value_at_position(self.SEED_VALUE_START_POSITION, self.input_seed_length, self.input_seed).uint
    
    def get_input_hash_position(self) -> int:
        return get_data_item_position_from_data_length(self.data_length, self.input_seed, self.input_value_item_length)
    
    def create_output_hash_position(self) -> int:
        return get_data_item_position_from_data_length(self.data_length, self.output_seed, self.output_value_item_length)
    
    def get_input_data_position(self) -> int:
        # позиция данных всегда совпадает с позицией в пространстве хешей 
        return self.get_input_hash_position()

    def create_output_data_position(self) -> int:
        # позиция данных всегда совпадает с позицией в пространстве хешей 
        return self.create_output_hash_position()
    
    # значение байта исходных данных в указанной позиции (совпадает со значением байта пространства хешей в той же позиции)
    def get_input_hash_item_value(self) -> Bits:
        return value_at_position(self.input_hash_position, self.input_value_item_length, self.input_seed)
    
    def create_output_hash_item_value(self) -> Bits:
        return self.get_input_hash_item_value()
    
    def get_input_data_item_value(self) -> Bits:
        # return value_at_position(self.input_data_position, self.input_value_item_length, self.input_seed)
        return self.get_input_hash_item_value()
    
    def create_output_data_item_value(self) -> Bits:
        # не используем возможность создавать различиные значения для input и output data value
        # байт данных из указанной позиции используем только 1 раз по прямому назначению: при восстановлении данных
        return self.get_input_data_item_value()
    
    def __repr__(self):
        return str(self.value)

# https://realpython.com/linked-lists-python/#how-to-create-a-linked-list
class LinkedSeedList:
    # default item processing options - used for reading seed/item value from hash space
    SEED_ITEM_LENGTH  : int = 24
    VALUE_ITEM_LENGTH : int = 8
    # default linked list props
    head = None
    tail = None

    def __init__(self, values=None):
        self.head = None
        self.tail = None
        if values is not None:
            self.add_multiple_nodes(values)
            
    def __str__(self):
        return ' -> '.join([str(node) for node in self])
    
    def __len__(self):
        count = 0
        node = self.head
        while node:
            count += 1
            node = node.next
        return count
    
    def __iter__(self):
        current = self.head
        while current:
            yield current
            current = current.next
    
    # https://www.programiz.com/python-programming/property  
    @property
    def values(self):
        return [node.value for node in self]
    
    def add_node(self, value):
        if self.head is None:
            self.tail = self.head = SeedNode(value)
        else:
            self.tail.next = SeedNode(value)
            self.tail = self.tail.next
        return self.tail
    
    def add_multiple_nodes(self, values):
        for value in values:
            self.add_node(value)
            
    def add_node_as_head(self, value):
        if self.head is None:
            self.tail = self.head = SeedNode(value)
        else:
            self.head = SeedNode(value, self.head)
        return self.head

    def __repr__(self):
        node  = self.head
        nodes = []
        while node is not None:
            nodes.append(node.value)
            node = node.next
        nodes.append("None")
        return " -> ".join(nodes)


# TESTING
# https://github.com/astropy/pytest-doctestplus#skipping-tests
__doctest_skip__ = [
    'SeedNode', 
    'LinkedSeedList', 
    'LinkedSeedList.*',
    'HashItemPosition',
    'ItemPosition',
    'DataItem',
]
# https://github.com/astropy/pytest-doctestplus
# https://docs.pytest.org/en/latest/how-to/doctest.html
#if __name__ == "__main__":
    #import doctest
    #doctest.testmod()