# https://github.com/Textualize/rich
from rich import print, inspect
# https://rich.readthedocs.io/en/latest/pretty.html
from rich.pretty import pprint
# https://rich.readthedocs.io/en/latest/traceback.html
from rich.traceback import install
install(show_locals=True)
# https://rich.readthedocs.io/en/latest/logging.html
import logging
from rich.logging import RichHandler
logging.basicConfig(
    level="INFO",
    format="%(message)s",
    datefmt="[%X]",
    handlers=[RichHandler(rich_tracebacks=True, locals_max_length=16)]
)
log = logging.getLogger("rich")
# https://github.com/tqdm/tqdm
import tqdm

# https://realpython.com/python-data-classes/
# https://realpython.com/python-data-classes/#advanced-default-values
from dataclasses import dataclass, make_dataclass, field
# https://github.com/keon/algorithms
# https://github.com/keon/algorithms/blob/master/algorithms/compression/rle_compression.py
from algorithms.compression.rle_compression import encode_rle, decode_rle
# https://bitstring.readthedocs.io/en/latest/index.output_item_idhtml
from bitstring import Bits, BitArray, BitStream, ConstBitStream
# https://pythongeeks.org/ordereddict-in-python/
# https://www.geeksforgeeks.org/counters-in-python-set-1/
# https://docs.python.org/3/library/collections.html#counter-objects
# https://realpython.com/python-namedtuple/
# https://realpython.com/linked-lists-python/
from collections import OrderedDict, Counter, defaultdict, namedtuple, deque
# https://github.com/multiformats/unsigned-varint
# https://github.com/fmoo/python-varint
import varint
# https://docs.python.org/3/library/typing.html
# https://realpython.com/python-data-classes/#more-flexible-data-classes
from typing import List, Set, Dict, Tuple, Optional, Union, Deque, Literal
# https://www.geeksforgeeks.org/floor-ceil-function-python/?ref=lbp
import math
# https://docs.python.org/3/library/operator.html#module-operator
import operator
# https://stackoverflow.com/questions/55212014/python-count-copies-in-dictionary
import copy
# https://www.attrs.org/en/stable/
# https://www.attrs.org/en/latest/api.html#attr.ib
import attr
from attr import field, fields, define, make_class

# https://docs.sqlalchemy.org/en/14/core/engines.html#sqlite
from sqlalchemy import create_engine, orm, Table, Column, Integer as IntegerColumn, SmallInteger
# https://docs.sqlalchemy.org/en/14/dialects/sqlite.html
# https://docs.sqlalchemy.org/en/14/dialects/sqlite.html#sqlite-data-types
from sqlalchemy.dialects.sqlite import BLOB, BOOLEAN, INTEGER, NUMERIC, JSON, SMALLINT, VARCHAR
# https://docs.sqlalchemy.org/en/14/orm/tutorial.html#declare-a-mapping
from sqlalchemy.orm import sessionmaker, query
from sqlalchemy.ext.declarative import declarative_base
# https://docs.sqlalchemy.org/en/14/orm/tutorial.html#using-exists
from sqlalchemy.sql import exists, func, asc, desc
from sqlalchemy.orm import registry

# https://docs.sqlalchemy.org/en/14/orm/declarative_styles.html#example-three-attrs-with-imperative-table
mapper_registry = registry()
# engine = create_engine('sqlite:///data/values-v2.local.db', echo=True)
#engine = create_engine('sqlite:///data/values-v3.local.db', echo=False)
engine = create_engine('sqlite:///data/values-v4.local.db', echo=False)
# engine = create_engine('sqlite:///:memory:', echo=True)
# https://docs.sqlalchemy.org/en/14/orm/tutorial.html#creating-a-session
Session = sessionmaker(bind=engine)
session = Session()
#Base    = mapper_registry.generate_base()

if __name__ == "__main__":
    # поддержка импорта по относительному пути
    import os, sys
    if not 'workbookDir' in globals():
        workbookDir = os.getcwd()
    os.chdir(workbookDir)

# функции для работы с хеш-пространством
from hash_space_utils import HashItemAddress, \
    get_min_bit_length, \
    get_aligned_bit_length, \
    get_varint_bit_length, \
    value_at_position, \
    read_hash_item, \
    split_data, \
    count_split_values, \
    collect_split_positions

# конструкторы автоматически вычисляемых значений

def create_bytes_value(value: int) -> bytes:
    byte_length = (get_aligned_bit_length(value) // 8)
    return value.to_bytes(byte_length, byteorder='big')

def create_varint_value(value: int) -> bytes:
    return varint.encode(value)

def create_length_value(value: int) -> int:
    """
    Получить минимальную длину значения в битах
    """
    return get_min_bit_length(value)

def create_bytes_length_value(value: int) -> int:
    """
    Получить длину байтового значения в битах
    """
    return get_aligned_bit_length(value)

def create_item_bytes_position_value(bit_position: int, value: int) -> int:
    """
    Получить позицию элемента в разбиении с учетом его длины 
    (предполагаем что все элементы одинаковые и разбиение начато с позиции 0)
    """
    item_bit_length = create_bytes_length_value(value)
    if (bit_position % item_bit_length != 0):
        msg = f"incorrect value length ({item_bit_length}) for item position {bit_position} for value {value}"
        log.info(msg)
        # TODO: raise Exception(msg)
        return None
    return (bit_position // item_bit_length)

def create_varint_length_value(value: int) -> int:
    """
    Получить длину varint-значения в битах
    """
    return get_varint_bit_length(value)

def get_value_using_getter(raw_value, attribute: str) -> int:
    field_getter = operator.attrgetter(attribute)
    return field_getter(raw_value)

# функции для создания лямбда-выражений (генераторов значений по умолчанию)

def create_bytes_value_from_getter(raw_value, attribute: str) -> bytes:
    field_value = get_value_using_getter(raw_value, attribute)
    return create_bytes_value(field_value)

def create_varint_value_from_getter(raw_value, attribute: str) -> bytes:
    field_value = get_value_using_getter(raw_value, attribute)
    return create_varint_value(field_value)

def create_length_value_from_getter(raw_value, attribute: str) -> int:
    field_value = get_value_using_getter(raw_value, attribute)
    return create_length_value(field_value)

def create_bytes_length_value_from_getter(raw_value, attribute: str) -> int:
    field_value = get_value_using_getter(raw_value, attribute)
    return create_bytes_length_value(field_value)

def create_varint_length_value_from_getter(raw_value, attribute: str) -> int:
    field_value = get_value_using_getter(raw_value, attribute)
    return create_varint_length_value(field_value)

def create_item_position_value(raw_value) -> int:
    return create_item_bytes_position_value(raw_value.position, raw_value.value)

# https://docs.sqlalchemy.org/en/14/orm/declarative_styles.html#example-three-attrs-with-imperative-table
@mapper_registry.mapped
@attr.s
class RawValue:
    """
    Сохраненное, ранее найденное значение элемента данных
    - длина всегда только в битах
    - изначально задается только value, seed и position - остальные поля заполняются автоматически

    >>> value    = 26
    >>> position = 42
    >>> seed     = 1
    >>> raw_value = RawValue(value, position, seed)
    >>> raw_value.value == value
    True
    >>> raw_value.position == position
    True
    >>> raw_value.seed == seed
    True
    >>> # байты из значений
    >>> raw_value.value_bytes.hex()
    '1a'
    >>> raw_value.value_bytes == create_bytes_value(raw_value.value)
    True
    >>> raw_value.position_bytes.hex()
    '2a'
    >>> raw_value.position_bytes == create_bytes_value(raw_value.position)
    True
    >>> raw_value.seed_bytes.hex()
    '01'
    >>> raw_value.seed_bytes == create_bytes_value(raw_value.seed)
    True
    >>> # varint-представление
    >>> raw_value.value_varint.hex()
    '1a'
    >>> varint.decode_bytes(raw_value.value_varint) == raw_value.value
    True
    >>> raw_value.position_varint.hex()
    '2a'
    >>> varint.decode_bytes(raw_value.position_varint) == raw_value.position
    True
    >>> raw_value.seed_varint.hex()
    '01'
    >>> varint.decode_bytes(raw_value.seed_varint) == raw_value.seed
    True
    >>> # длина значений
    >>> raw_value.value_length
    5
    >>> raw_value.position_length
    6
    >>> raw_value.seed_length
    1
    >>> raw_value.value_bytes_length
    8
    >>> raw_value.position_bytes_length
    8
    >>> raw_value.seed_bytes_length
    8
    >>> raw_value.value_varint_length
    8
    >>> raw_value.position_varint_length
    8
    >>> raw_value.seed_varint_length
    8
    >>> raw_value.item_position is None
    True
    """

    # таблица в базе
    __table__           = Table(
        'raw_values',
        mapper_registry.metadata,
        # id записи (сквозная нумерация)
        Column("id", IntegerColumn, primary_key=True),
        # значения в виде положительных целых чисел
        Column("value", IntegerColumn, nullable=False, index=True),
        # позиция (номер бита) в виде целого числа
        Column("position", IntegerColumn, nullable=False, index=True),
        # seed-значение (инициализация хеш-функции) в виде целого числа (0 -> (2**64)-1)
        Column("seed", IntegerColumn, nullable=False, index=True),
        
        # Автоматически заполняемые значения:
        
        # представление в виде байтов:
        # данные элемента в виде байтов
        Column("value_bytes", VARCHAR, index=True),
        # позиция элемента (номер бита) в виде байтов
        Column("position_bytes", VARCHAR, index=True),
        # стартовое seed-значение для хеш-функции в виде байтов
        Column("seed_bytes", VARCHAR, index=True),
        
        # varint-представление:
        # данные элемента в виде байтов varint
        Column("value_varint", VARCHAR, index=True),
        # позиция элемента (номер бита) в виде байтов varint
        Column("position_varint", VARCHAR, index=True),
        # стартовое seed-значение для хеш-функции в виде байтов varint
        Column("seed_varint", VARCHAR, index=True),
        
        # кешированные значения длины
        
        # длина значения в битах
        Column("value_length", SMALLINT, index=True),
        # длина varint-значения в битах
        Column("value_varint_length", SMALLINT, index=True),
        # длина байтов значения в битах
        Column("value_bytes_length", SMALLINT, index=True),
        
        # длина позиции в битах
        Column("position_length", SMALLINT, index=True),
        # длина varint-позиции в битах
        Column("position_varint_length", SMALLINT, index=True),
        # длина байтов позиции в битах
        Column("position_bytes_length", SMALLINT, index=True),
        
        # длина seed-значения в битах
        Column("seed_length", SMALLINT, index=True),
        # длина seed-значения в битах (после преобразования в varint)
        Column("seed_varint_length", SMALLINT, index=True),
        # длина байтов seed-значения в битах
        Column("seed_bytes_length", SMALLINT, index=True),

        # позиция элемента с учетом разбиения
        Column("item_position", IntegerColumn, index=True),
        # TODO: позиция элемента с учетом разбиения, с округлением / без округления в сторону целого байта
        # TODO: item_position_offset минимальная позиция для первого элемента с учетом разбиения
    )
    id       = attr.ib(init=False)
    value    = attr.ib()
    position = attr.ib()
    seed     = attr.ib()
    
    value_bytes     = attr.ib(
        default=attr.Factory(lambda self: create_bytes_value_from_getter(self, 'value'), takes_self=True),
    )
    position_bytes  = attr.ib(
        default=attr.Factory(lambda self: create_bytes_value_from_getter(self, 'position'), takes_self=True),
    )
    seed_bytes      = attr.ib(
        default=attr.Factory(lambda self: create_bytes_value_from_getter(self, 'seed'), takes_self=True),
    )

    value_varint    = attr.ib(
        default=attr.Factory(lambda self: create_varint_value_from_getter(self, 'value'), takes_self=True),
    )
    position_varint = attr.ib(
        default=attr.Factory(lambda self: create_varint_value_from_getter(self, 'position'), takes_self=True),
    )
    seed_varint     = attr.ib(
        default=attr.Factory(lambda self: create_varint_value_from_getter(self, 'seed'), takes_self=True),
    )

    value_length           = attr.ib(
        default=attr.Factory(lambda self: create_length_value_from_getter(self, 'value'), takes_self=True),
    )
    position_length        = attr.ib(
        default=attr.Factory(lambda self: create_length_value_from_getter(self, 'position'), takes_self=True),
    )
    seed_length            = attr.ib(
        default=attr.Factory(lambda self: create_length_value_from_getter(self, 'seed'), takes_self=True),
    )

    value_bytes_length     = attr.ib(
        default=attr.Factory(lambda self: create_bytes_length_value_from_getter(self, 'value'), takes_self=True),
    )
    position_bytes_length  = attr.ib(
        default=attr.Factory(lambda self: create_bytes_length_value_from_getter(self, 'position'), takes_self=True),
    )
    seed_bytes_length      = attr.ib(
        default=attr.Factory(lambda self: create_bytes_length_value_from_getter(self, 'seed'), takes_self=True),
    )

    value_varint_length    = attr.ib(
        default=attr.Factory(lambda self: create_varint_length_value_from_getter(self, 'value'), takes_self=True),
    )
    position_varint_length = attr.ib(
        default=attr.Factory(lambda self: create_varint_length_value_from_getter(self, 'position'), takes_self=True),
    )
    seed_varint_length     = attr.ib(
        default=attr.Factory(lambda self: create_varint_length_value_from_getter(self, 'seed'), takes_self=True),
    )
    
    item_position = attr.ib(
        default=attr.Factory(lambda self: create_item_position_value(self), takes_self=True),
    )

    # https://docs.sqlalchemy.org/en/14/orm/constructors.html
    #@orm.reconstructor
    #def init_on_load(self):
    #    pass


def create_values_table():
    """
    Создать таблицу для хранения значений: вызывается 1 раз в начале работы
    """
    mapper_registry.metadata.create_all(engine)
    inspect(RawValue.__table__)

def create_conditions_from_raw_value(value: RawValue) -> dict:
    """
    Создать объект с условиями из объекта значения
    """
    conditions = defaultdict()
    fields     = [
        'value', 'position', 'seed', 
        'value_bytes_length', 'position_bytes_length', 'seed_bytes_length'
    ]
    for field_name in fields:
        field_getter = operator.attrgetter(field_name)
        field_value  = field_getter(value)
        if (field_value is not None):
            conditions[field_name] = field_value
    return conditions

def has_saved_value(raw_value: RawValue) -> bool:
    """
    Определить, есть ли уже в базе сохраненное значение
    """
    exists_stmt      = exists()
    fields     = [
        'value', 'position', 'seed', 
        'value_bytes_length', 'position_bytes_length', 'seed_bytes_length'
    ]
    conditions       = create_conditions_from_raw_value(raw_value)
    condition_fields = list(conditions.keys())
    for field_name in fields:
        if (field_name in condition_fields) and (conditions[field_name] is not None):
            field_getter = operator.attrgetter(field_name)
            model_field = field_getter(RawValue)
            exists_stmt = exists_stmt.where(operator.eq(model_field, conditions[field_name]))
    exists_result = session.query(RawValue).filter(exists_stmt).first()
    if (exists_result is None):
        return False
    return True

def get_all_saved_values(raw_value: RawValue) -> bool:
    #query            = session.query(RawValue)
    fields     = [
        'value', 'position', 'seed', 
        'value_bytes_length', 'position_bytes_length', 'seed_bytes_length'
    ]
    conditions       = create_conditions_from_raw_value(raw_value)
    condition_fields = list(conditions.keys())
    for field_name in fields:
        if (field_name in condition_fields) and (conditions[field_name] is not None):
            field_getter = operator.attrgetter(field_name)
            model_field = field_getter(RawValue)
            session.query(RawValue).filter(operator.eq(model_field, conditions[field_name]))
    saved_values = session.query(RawValue).all()
    return saved_values

def get_first_saved_value(raw_value: RawValue) -> bool:
    #query            = session.query(RawValue)
    fields     = [
        'value', 'position', 'seed', 
        'value_bytes_length', 'position_bytes_length', 'seed_bytes_length'
    ]
    conditions       = create_conditions_from_raw_value(raw_value)
    condition_fields = list(conditions.keys())
    for field_name in fields:
        if (field_name in condition_fields) and (conditions[field_name] is not None):
            field_getter = operator.attrgetter(field_name)
            model_field = field_getter(RawValue)
            session.query(RawValue).filter(operator.eq(model_field, conditions[field_name]))
    saved_value = session.query(RawValue).first()
    return saved_value

def save_raw_value(raw_value: RawValue) -> RawValue:
    """
    Сохранить значение в базу
    """
    # проверяем существует ли уже такое значение
    value_exists = has_saved_value(raw_value)
    # value_exists = get_first_saved_value(new_value)
    if (value_exists):
        return get_first_saved_value(raw_value)
    # сохраняем только после того как всё проставлено
    result = session.add(raw_value)
    session.commit()
    return result

def create_raw_value_from_hash_item(address: HashItemAddress) -> RawValue:
    """
    Создать объект взаимодействующий с базой из хеш-значения, расположенного по указанному адресу
    """
    value       = read_hash_item(address)
    db_value    = value.uint
    db_position = address.bit_position
    db_seed     = address.seed
    return RawValue(db_value, db_position, db_seed)

def save_hash_item(address: HashItemAddress) -> RawValue:
    """
    Сохранить в базу экземпляр HashItem который находится по указанному адресу
    """
    # создаем объект для вставки в базу
    raw_value = create_raw_value_from_hash_item(address)
    return save_raw_value(raw_value)

def has_hash_item(address: HashItemAddress) -> bool:
    raw_value = create_raw_value_from_hash_item(address)
    return has_saved_value(raw_value)

def find_one_hash_item(value: int=None, position: int=None, seed: int=None, value_length: int=None, position_length: int=None, seed_length: int=None) -> RawValue:
    pass

def find_hash_item_by_id(id: int) -> RawValue:
    pass

def find_all_hash_items(value: int=None, position: int=None, seed: int=None, value_length: int=None, position_length: int=None, seed_length: int=None) -> RawValue:
    pass

def delete_one_hash_item(address: HashItemAddress) -> RawValue:
    pass

def delete_one_hash_item_by_id(id: int) -> RawValue:
    pass