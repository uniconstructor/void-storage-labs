# https://stackoverflow.com/questions/38340808/recursive-typing-in-python-3-5
from __future__ import annotations
import os
from rich import print
from rich.pretty import pprint
from custom_counter import CustomCounter as Counter
from tqdm import tqdm
from bitarray.util import ba2int, int2ba
from bitarray import bitarray, frozenbitarray
from collatz_utils import VALUE_SHARD_SIZE, discover_hailstone_numbers

from mongoengine import register_connection
register_connection('default', db=f'collatz_db', host='127.0.0.1', port=27017, maxPoolSize=200)

target_shard = input("target_shard:")
extra_shards = input("extra_shards:")

if (target_shard == ''):
  target_shard = None
else:
  target_shard = int(target_shard)

if (extra_shards == ''):
  extra_shards = 8
else:
  extra_shards = int(extra_shards)

min_id = target_shard * VALUE_SHARD_SIZE
max_id = min_id + VALUE_SHARD_SIZE
print(f"Scanning target shard {target_shard} (+{extra_shards} extra shards): min_id={min_id}, max_id={max_id}")
discover_hailstone_numbers(min_id=min_id, max_id=max_id)

for i in range(0, extra_shards):
  min_id += VALUE_SHARD_SIZE
  max_id += VALUE_SHARD_SIZE
  print(f"\nScanning extra shard {i+1}/{extra_shards}: min_id={min_id}, max_id={max_id}")
  discover_hailstone_numbers(min_id=min_id, max_id=max_id)