import os
from rich import print
from rich.pretty import pprint
#from tqdm import tqdm
from bitarray import bitarray, frozenbitarray
from multi_seed_prefix_utils import SeedPrefixTree, create_content_based_split

from mongoengine import register_connection
register_connection('default', db=f'multi_seed_trees', host='127.0.0.1', port=27017, maxPoolSize=300)

file_dir  = os.path.dirname(os.path.realpath('__file__'))
#file_name = os.path.join(file_dir, './data/AMillionRandomDigits.bin')
file_name = os.path.join(file_dir, './data/image-144kb.jpg')
data      = bitarray(endian='little')
data_file = open(file=file_name, mode='rb')
data.fromfile(data_file)
data        = frozenbitarray(data) # [0:2**14] # first n bits - to make things faster
data_length = len(data)
print(f"file_name: {file_name}, size: {len(data)} bits, ({len(data) // 8} bytes), ({len(data) // 16} items)")

nounce_length_bits    = input("nounce_length_bits (11):")
default_score         = input("default_score (2):")
nounce_shift_size     = input("nounce_shift_size (2033):")
init_max_value_length = input("init_max_value_length (21):")

if (nounce_length_bits == ''):
  nounce_length_bits = 11
else:
  nounce_length_bits = int(nounce_length_bits)

if (default_score == ''):
  default_score = 2
else:
  default_score = int(default_score)

if (nounce_shift_size == ''):
  nounce_shift_size = 2033
else:
  nounce_shift_size = int(nounce_shift_size)

if (init_max_value_length == ''):
  init_max_value_length = 21
else:
  init_max_value_length = int(init_max_value_length)

nounce_shift_offset = { -1: nounce_shift_size, 0: 0 }
cbs = create_content_based_split(
    data=data, 
    nounce_length_bits=nounce_length_bits, 
    default_score=default_score, 
    shift_offset=nounce_shift_offset,
)
print(f"total_score={cbs.total_score}, position={cbs.position} capacity_overflow={cbs.capacity_overflow}, lc={len(cbs.length_counts)}/{2**nounce_length_bits} ({2**nounce_length_bits-len(cbs.length_counts)}), bits_per_seed={cbs.bits_per_seed}")
print(f"encoded_items_count={cbs.encoded_items_count}, unique_values_count={cbs.unique_values_count}, seed_items_count={cbs.seed_items_count}")
pprint(cbs.length_counts)

ptree = SeedPrefixTree(data=data, 
    nounce_length_bits=nounce_length_bits, 
    default_score=default_score,
    nounce_shift_offset={-1: nounce_shift_size, 0: 0}, 
    init_max_value_length=init_max_value_length,
    enable_expansion=False,
    expand_distance=4, 
    nounce_encoder=f"PPP{nounce_length_bits}", 
    seed_encoder='C1'
)
ptree.encode_data()