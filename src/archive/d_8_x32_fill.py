
import os
from rich import print
from rich.pretty import pprint
from b_8_x32_utils import HASH_SHARD_SIZE, discover_hash_segments, get_data_segment_bytes, get_last_hash_segment_id
from bitarray.util import ba2int, int2ba
from bitarray import bitarray, frozenbitarray
from typing import List, Set, Dict, Tuple, Optional, Union, Iterable
from mongoengine import register_connection
register_connection('default', db=f'8_byte_segments', host='127.0.0.1', port=27017, maxPoolSize=200)

file_dir  = os.path.dirname(os.path.realpath('__file__'))
file_name = os.path.join(file_dir, './data/AMillionRandomDigits.bin')
data      = bitarray(endian='big')
data_file = open(file=file_name, mode='rb')
data.fromfile(data_file)
data        = frozenbitarray(data) # [0:2**14] # first n bits - to make things faster
data_length = len(data)
print(f"file_name: {file_name}, size: {len(data)} bits, ({len(data) // 8} bytes), ({len(data) // 16} items)")

data_segment_id = input("data_segment_id:")
data_segment_id = int(data_segment_id)
last_segment_id = get_last_hash_segment_id(data_segment_id=data_segment_id)
print(f"data_segment_id={data_segment_id}, last_segment_id={last_segment_id}, last_shard_id={last_segment_id // HASH_SHARD_SIZE}")

start_shart_id  = input("start_shart_id:")
max_shards      = input("max_shards (default 16):")
data_bytes      = get_data_segment_bytes(data_segment_id=data_segment_id, data=data)
print(f"data_bytes={data_bytes[0:16]} (0-16)")

if (start_shart_id == ''):
  start_shart_id = 0
else:
  start_shart_id = int(start_shart_id)

if (max_shards == ''):
  max_shards = None
else:
  max_shards = int(max_shards)

print(f"Scanning data_segment_id={data_segment_id} target shard {start_shart_id} (+{max_shards}):")

discover_hash_segments(data_segment_id=data_segment_id, data_bytes=data_bytes, start_shard_id=start_shart_id, max_shards=max_shards)