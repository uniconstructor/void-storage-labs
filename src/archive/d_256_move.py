# https://github.com/Textualize/rich
from rich import print
# https://rich.readthedocs.io/en/latest/pretty.html
from rich.pretty import pprint
from custom_counter import CustomCounter as Counter
from tqdm import tqdm

from hash_range_iterator import bits_at_position
from hash_space_utils import get_min_bit_length
from typing import List, Set, Dict, Tuple, Optional, Union, Iterable
# https://realpython.com/lru-cache-python/
from functools import lru_cache

from b_256_utils import ExpansionSeedV2, \
  load_seed_items, get_row_value_counts, get_value_class, seed_to_part_id
from mongoengine import register_connection, QuerySet
register_connection('default', db=f'256_byte_segments', host='127.0.0.1', port=27017, maxPoolSize=200)

def move_extension_values(min_part_id: int, max_part_id: int):
  total_parts = max_part_id - min_part_id
  progress    = tqdm(total=2**16*total_parts, miniters=256, smoothing=0)

  for part_id in range(min_part_id, max_part_id):
    start_seed = 65536 * part_id
    end_seed   = start_seed + 65536
    
    for min_seed in range(start_seed, end_seed, 256):
      max_seed      = min_seed + 256
      OldValueClass = ExpansionSeedV2
      NewValueClass = get_value_class(seed=min_seed)
      old_values    = OldValueClass.objects(seed__gte=min_seed, seed__lt=max_seed).order_by('id').limit(65536)
      new_values    = list()
      progress.set_description_str(f"collection={NewValueClass._get_collection_name()}")
      
      for old_value in old_values:
        part_id      = seed_to_part_id(old_value.seed)
        new_value_id = old_value.id - (part_id * 65536 * 256)
        new_value  = NewValueClass(
          id       = new_value_id,
          seed     = old_value.seed,
          position = old_value.position,
          level    = old_value.level,
          value    = old_value.value,
        )
        new_values.append(new_value)
      
      if (len(new_values) > 0):
        QuerySet(NewValueClass, collection=NewValueClass._get_collection()).insert(new_values, load_bulk=False)
        new_values.clear()
        progress.update(256)

min_part_id = int(input("min_part_id (start):"))
max_part_id = min_part_id + 8

print(f"min_part_id={min_part_id}, max_part_id={max_part_id}")
move_extension_values(min_part_id=min_part_id, max_part_id=max_part_id)