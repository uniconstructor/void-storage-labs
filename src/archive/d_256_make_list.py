# https://github.com/Textualize/rich
from rich import print
# https://rich.readthedocs.io/en/latest/pretty.html
from rich.pretty import pprint
from custom_counter import CustomCounter as Counter
from tqdm import tqdm

from hash_range_iterator import bits_at_position
from hash_space_utils import get_min_bit_length
from typing import List, Set, Dict, Tuple, Optional, Union, Iterable
# https://realpython.com/lru-cache-python/
from functools import lru_cache

from b_256_utils import get_value_class, get_value_list_class, seed_to_part_id, seed_to_shard_id, get_next_undiscovered_shard_seed,\
  is_started_shard, is_completed_shard
from mongoengine import register_connection, QuerySet
register_connection('default', db=f'256_byte_segments', host='127.0.0.1', port=27017, maxPoolSize=200)

def create_value_lists(min_part_id: int, max_part_id: int):
  total_parts = max_part_id - min_part_id
  progress    = tqdm(total=2**16*total_parts, mininterval=0.5, smoothing=0)

  for shard_id in range(min_part_id, max_part_id):
    start_seed  = 65536 * shard_id
    if is_completed_shard(shard_id=shard_id):
      progress.update(65536)
      continue
    if is_started_shard(shard_id=shard_id):
      start_seed = get_next_undiscovered_shard_seed(shard_id=shard_id)
      previous_progress = start_seed - (65536 * shard_id)
      progress.update(previous_progress)
    end_seed    = (65536 * shard_id) + 65536
    new_lists   = list()
    buffer_size = 0
    
    for seed in range(start_seed, end_seed):
      values         = list()
      ValueClass     = get_value_class(seed=seed)
      ValueListClass = get_value_list_class(seed=seed)
      seed_items     = ValueClass.objects(seed=seed).order_by('id').only('value').limit(256)
      if (len(seed_items) < 256):
        progress.update(1)
        continue
      progress.set_description_str(f"shard_id={shard_id}, seed={seed}, {ValueListClass._get_collection_name()}, buffer: {buffer_size:4}", refresh=False)
      # collect raw values from seed items
      for list_item in seed_items:
        values.append(list_item.value)
      # create list
      new_list = ValueListClass(
        id     = seed,
        values = values.copy(),
      )
      new_lists.append(new_list)
      values.clear()
      buffer_size += 1
      progress.update(1)
      # using batch save
      if (buffer_size >= 1024):
        progress.set_description_str(f"shard_id={shard_id}, seed={seed}, {ValueListClass._get_collection_name()}, buffer: {buffer_size:4} (saving...)", refresh=True)
        QuerySet(ValueListClass, collection=ValueListClass._get_collection()).insert(new_lists, load_bulk=False)
        new_lists.clear()
        buffer_size = 0
        progress.set_description_str(f"shard_id={shard_id}, seed={seed}, {ValueListClass._get_collection_name()}, buffer: {buffer_size:4}", refresh=True)
    # save remaining data
    if (len(new_lists) > 0):
      for new_list in new_lists:
        new_list.save()
      #QuerySet(ValueListClass, collection=ValueListClass._get_collection()).insert(new_lists, load_bulk=False)
      #new_lists.clear()
      progress.update(buffer_size)
      buffer_size = 0
      progress.set_description_str(f"shard_id={shard_id}, seed={seed}, {ValueListClass._get_collection_name()}, buffer: {buffer_size:4}", refresh=True)
    # drop old collection
    ValueClass.drop_collection()
    print(f"Data collected. Collection '{ValueClass._get_collection_name()}' has beed dropped.")

print(f"Create value lists:")
min_part_id = int(input("min_part_id:"))
total_parts = int(input("total_parts:"))
max_part_id = min_part_id + total_parts

print(f"min_part_id={min_part_id}, max_part_id={max_part_id-1}, total_parts={total_parts}")
create_value_lists(min_part_id=min_part_id, max_part_id=max_part_id)