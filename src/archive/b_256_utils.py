# https://stackoverflow.com/questions/38340808/recursive-typing-in-python-3-5
from __future__ import annotations
# https://github.com/Textualize/rich
from rich import print
# https://rich.readthedocs.io/en/latest/pretty.html
from rich.pretty import pprint
from custom_counter import CustomCounter as Counter
from tqdm import tqdm
from bitarray.util import ba2int, int2ba, ba2hex, hex2ba, make_endian, vl_encode, vl_decode, huffman_code
from bitarray import bitarray, frozenbitarray
from collections import defaultdict, namedtuple
import math

from hash_range_iterator import bits_at_position, int_bytes_from_digest, int_bytes_from_nounce, bytes_from_nounce, int_from_nounce,\
  HASH_DIGEST_BITS, HASH_DIGEST_BYTES
from hash_space_utils import get_min_bit_length
from typing import List, Set, Dict, Tuple, Optional, Union, Iterable
# https://realpython.com/lru-cache-python/
from functools import lru_cache
from dataclasses import dataclass, field
# https://docs.python.org/3/library/enum.html
from enum import Enum, IntEnum
from copy import copy, deepcopy

#from mongoengine import register_connection
#sregister_connection('default', db=f'256_byte_segments', host='127.0.0.1', port=27017, maxPoolSize=300)
from mongoengine import Document, QuerySet, LongField, IntField, \
    BinaryField, StringField, DictField, ListField, SortedListField, BooleanField, queryset_manager

# длина одного элемента в сегменте хеш-пространства
HASH_ITEM_BITS      = 16
HASH_ITEM_BYTES     = HASH_ITEM_BITS // 8
# шаг чтения элементов хеш-пространства (для чтения с наложением или без наложения соседних значений друг на друга)
HASH_ITEM_STEP_BITS = 16
# длина сегменте хеш-пространства
HASH_SEGMENT_BITS   = 256 * HASH_ITEM_STEP_BITS
HASH_SEGMENT_BYTES  = HASH_SEGMENT_BITS // 8
# количество хешей, необходимое для получения одного сегмента
HASH_SEGMENT_CHUNKS = HASH_SEGMENT_BITS // HASH_DIGEST_BITS
# количество элементов в одном фрагменте хеша
HASH_CHUNK_ITEMS    = HASH_DIGEST_BITS // HASH_ITEM_BITS
# total number seeds inside one collection
MAX_SEEDS_PER_COLLECTION = 65536
MAX_ITEMS_PER_COLLECTION = MAX_SEEDS_PER_COLLECTION * 256
# последний id записи в полностью заполненной коллекции
LAST_VALUE_COLLECTION_ID = 16777215
# Один part_id это одна коллекция в базе, которая содержит 65536 (2**16) сидов в каждом из которых 256 записей (по одному значению на позицию)
# в сумме это дает ровно 16777216 (2**24) записей в каждой коллекции
# Полный размер одной коллекции (все данные + все индексы) составляет примерно 1 гигабайт
# 
# Максимальное количество коллекций сейчас ограничено 256 элементами по двум причинам:
# - это почти все доступное пространство на моём SSD
# - 256 коллекций по 65536 сидов в сумме дают диаразон в 16777216 что соответствует 2**24 или трехбайтовому int-значению
# 
# Количество коллекций может быть и больше, размер и время поиска при увеличении количества коллекций растут линейно
# Заполнение одной коллекции (2**16 сидов и 2**24 записей) после всех оптимизаций занимает примерно 4 часа
# на Intel Core i5 3.2 GHz в один поток, коллекции можно (и нужно) заполнять параллельными процессами,
# для 12 ядер оптимальное количество потоков - 10
# Поэтому сгенерировать 256 коллекций еще реально (около 4,5 суток для 10 потоков) а вот 65536 уже нецелесообразно:
# данные не поместятся на диск, а время генерации, даже при использовании 10 параллельных потоков - уже примерно 3 года
MAX_PART_ID              = 256
MAX_SHARD_ID             = 65536

HistoryItem = namedtuple('HistoryItem', [
    'step',
    'data_position',
    'hash_position',
    'position_byte',
    'data_byte',
    'data_item',
    'data_item_id',
    'prev_data_byte',
    'prev_item',
    'stack_size',
])

@dataclass(init=True)
class ExpansionMatrix:
    history                : List[HistoryItem]
    hash_positions         : Dict[int, List[frozenbitarray]]
    position_update_counts : Counter
    byte_counts            : Counter
    item_counts            : Counter
    last_byte              : Union[frozenbitarray, None]

def create_expansion_matrix(data: frozenbitarray, endian: str='big') -> ExpansionMatrix:
    last_byte      = None
    hash_positions = dict()
    #byte_ids       = dict()
    history        = list()
    if (len(data) % 16 != 0):
        last_byte = data[len(data)-8:len(data)]
        data      = data[0:len(data)]
    
    position_update_counts = Counter()
    for i in range(0, 256):
        hash_positions[i] = list()
        hash_positions[i].append(None)
        position_update_counts.update({ i: 0 })
        #byte_ids[frozenbitarray(int2ba(i, length=8, endian=endian, signed=False))] = i

    byte_counts = Counter()
    item_counts = Counter()
    #item_bytes  = defaultdict(set)
    #byte_items  = defaultdict(set)

    step = 0
    for byte_id in range(0, (len(data) // 16)):
        item_start      = byte_id * 16
        item_end        = item_start + 16
        item            = data[item_start:item_end]
        position_byte   = item[0:8]
        data_byte       = item[8:16]
        item_id         = ba2int(item, signed=False)
        hash_position   = item_id // 256
        prev_item_level = len(hash_positions[hash_position]) - 1
        prev_item       = hash_positions[hash_position][prev_item_level]
        if (prev_item is None):
            prev_data_byte = None
        else:
            prev_data_byte = prev_item[8:16]
        
        history_item = HistoryItem(
            step           = step,
            data_position  = byte_id,
            hash_position  = hash_position,
            position_byte  = position_byte,
            data_byte      = data_byte,
            data_item      = item,
            data_item_id   = item_id,
            prev_data_byte = prev_data_byte,
            prev_item      = prev_item,
            stack_size     = len(hash_positions[hash_position]),
        )
        history.append(history_item)
        hash_positions[hash_position].append(item)
        position_update_counts.update({ hash_position: 1 })
        step += 1

        byte_counts.update({ position_byte: 1 })
        byte_counts.update({ data_byte: 1 })
        item_counts.update({ item: 1 })

    return ExpansionMatrix(
        history                = history,
        hash_positions         = hash_positions,
        position_update_counts = position_update_counts,
        byte_counts            = byte_counts,
        item_counts            = item_counts,
        last_byte              = last_byte,
    )

def get_matrix_row(matrix: ExpansionMatrix, row_id: int) -> List[int]:
    hash_row = []
    for i in range(0, 256):
        hash_row.append(matrix.hash_positions[i][row_id])
    row_values = [ba2int(row_item, signed=False) for row_item in hash_row]
    return row_values

# 2**3=8 позиций максимум
BITMAP_TOTAL_POSITION_BITS = 3
# максимальное расстояние между элементами: 2**(2**3)=256 позиций
BITMAP_ITEM_LENGTH_BITS    = 3
BITMAP_MIN_DISTANCE_BITS   = 3

@dataclass(init=True)
class HashPositionBitmap:
    # полная длина в битах
    bit_length           : int
    byte_length          : int
    # список переданных позиций
    positions            : Set[int]
    # список расстояний между позициями
    distances            : List[int]
    adjusted_distances   : List[int]
    item_bit_length      : int
    total_items          : int
    max_items            : int
    # минимальное расстояние между соседними позициями
    min_distance         : int
    max_distance         : int
    distance_diff        : int
    #total_positions_bits : int = BITMAP_TOTAL_POSITION_BITS
    #item_length_bits     : int = BITMAP_ITEM_LENGTH_BITS

def get_target_position_bitmap(positions: Set[int], length_offset: int, sort_positions: bool=True) -> HashPositionBitmap:
    #total_positions_bits = BITMAP_TOTAL_POSITION_BITS
    #item_length_bits     = BITMAP_ITEM_LENGTH_BITS
    if (len(positions) == 0):
        raise Exception(f"No positions given: positions={positions} ({len(positions)})")
    if (len(positions) < length_offset):
        raise Exception(f"Number of positions is too low: {positions} ({len(positions)}), min={length_offset}")
    
    if (sort_positions):
      bitmap_positions = positions.copy()
      bitmap_positions = sorted(bitmap_positions)
    else:
      bitmap_positions = positions.copy()
    
    distances          = list()
    adjusted_distances = list()
    bit_lengths        = set()
    bit_length_counts  = Counter()
    item_bit_length    = 0
    total_items        = 0
    bitmap_bit_length  = 0
    # считаем позиции от -1 чтобы минимальное расстояние между ними всегда равнялось 1 даже если задействована нулевая позиция
    prev_position      = -1
    
    # вычисляем расстояние между каждой парой позиций
    for position_id in range(0, len(bitmap_positions)):
        position      = bitmap_positions[position_id]
        distance      = position - prev_position
        distances.append(distance)
        prev_position = position
    
    # вычисляем минимальное расстояние между позициями и вычитаем его из всех значений чтобы уменьшить длину каждого значения
    min_distance  = min(distances)
    max_distance  = max(distances)
    distance_diff = max_distance - min_distance
    for full_distance in distances:
        adjusted_distance = full_distance - min_distance
        bit_length        = get_min_bit_length(adjusted_distance)
        adjusted_distances.append(adjusted_distance)
        bit_lengths.add(bit_length)
        bit_length_counts.update({ bit_length: 1 })
    
    item_bit_length    = max(bit_lengths)
    bitmap_bit_length  = (item_bit_length * len(distances)) + BITMAP_MIN_DISTANCE_BITS # total_positions_bits + item_length_bits + 
    bitmap_byte_length = math.ceil(bitmap_bit_length / 8)
    total_items        = len(distances) - length_offset
    
    return HashPositionBitmap(
        bit_length         = bitmap_bit_length,
        byte_length        = bitmap_byte_length,
        positions          = positions,
        distances          = distances,
        adjusted_distances = adjusted_distances,
        item_bit_length    = item_bit_length,
        total_items        = total_items,
        max_items          = len(distances),
        min_distance       = min_distance,
        max_distance       = max_distance,
        distance_diff      = distance_diff,
    )

def encode_position_bitmap(bitmap: HashPositionBitmap, endian: str='big') -> bitarray:
    encoded_bitmap  = bitarray(endian=endian)
    encoded_bitmap += int2ba(bitmap.total_items, length=bitmap.total_positions_bits, endian=endian, signed=False)
    encoded_bitmap += int2ba(bitmap.item_bit_length, length=bitmap.item_length_bits, endian=endian, signed=False)
    for distance in bitmap.distances:
        encoded_bitmap += int2ba(distance, length=bitmap.item_bit_length, endian=endian, signed=False)
    return encoded_bitmap

def decode_position_bitmap(data: bitarray, start_byte_id: int, seed_length: int) -> HashPositionBitmap:
    segment_bits     = data[start_byte_id*8:start_byte_id*8 + 8*64]
    total_items      = ba2int(segment_bits[0:BITMAP_TOTAL_POSITION_BITS], signed=False)
    item_bit_length  = ba2int(segment_bits[BITMAP_TOTAL_POSITION_BITS:BITMAP_TOTAL_POSITION_BITS + BITMAP_ITEM_LENGTH_BITS], signed=False)
    distances        = list()
    start_bit        = BITMAP_TOTAL_POSITION_BITS + BITMAP_ITEM_LENGTH_BITS
    positions        = set()
    position         = 0
    bit_length       = start_bit
    item_list_length = total_items + seed_length + 2
  
    for i in range(0, item_list_length):
        item_start  = start_bit  + i * item_bit_length
        item_end    = item_start + item_bit_length
        distance    = ba2int(segment_bits[item_start:item_end], signed=False)
        position   += distance
        bit_length += item_bit_length
        distances.append(distance)
        positions.add(position)

    return HashPositionBitmap(
        bit_length      = bit_length,
        byte_length     = math.ceil(bit_length / 8),
        positions       = positions,
        distances       = distances,
        item_bit_length = item_bit_length,
        total_items     = total_items,
        max_items       = len(distances),
    )

# параметры для получения одного элемента данных из значения хеша на любом уровне
ItemExtractionParams = namedtuple('ItemExtractionParams', [
    # порядковый номер элемента в сегменте
    'item_id',
    'item_start_byte',
])

@dataclass(init=True)
class HashSegmentChunk:
    # порядковый номер номер части сегмента, начиная с 0
    id               : int
    # соотношение позиции значения и параметров для получения первого байта из хеша
    position_mapping : Dict[int, ItemExtractionParams]

@lru_cache
def prepare_hash_chunks() -> Dict[int, HashSegmentChunk]:
    """
    Подготовить список вспомогательных объектов для оптимизированного сканирования уровней хеш-пространства:
    каждый объект отвечает за один фрагмент пространства хешей и содержит предустановленные значения для
    быстрой проверки хеша при поиске нужных значений в заданных позициях путем перебора уровней высоты сегмента в пространстве
    """
    chunks = dict()
    for chunk_id in range(0, HASH_SEGMENT_CHUNKS):
        chunk_mapping = dict()
        # генерируем параметры для фильтрации значений одного хеша
        for chunk_position_id in range(0, HASH_CHUNK_ITEMS):
            item_id = chunk_id * HASH_CHUNK_ITEMS + chunk_position_id
            chunk_mapping[item_id] = ItemExtractionParams(
                item_id         = item_id,
                item_start_byte = chunk_position_id * HASH_ITEM_BYTES,
            )
        # добавляем параметры каждого фрагмента как отдельный объект
        # объекты будут удаляться из словаря по мере того как значения в них будут найдены
        chunks[chunk_id] = HashSegmentChunk(
            id               = chunk_id,
            position_mapping = chunk_mapping,
        )
    return chunks

def fast_load_seed_values(seed: int) -> List[int]:
    """
    Оптимизированная версия load_seed_values()
    """
    seed_values    = list()
    hash_chunks    = deepcopy(prepare_hash_chunks())
    seed_level     = 0
    seed_loaded    = False
    # перебираем уровни (кратные длине сегмента участки позиций) пока не найдем все 256 значений
    while (seed_loaded is False):
        chunk_ids = list(hash_chunks.keys())
        for chunk_id in chunk_ids:
            # вычисляем часть сегмента как целое число
            chunk_nounce   = (seed_level * HASH_SEGMENT_CHUNKS) + hash_chunks[chunk_id].id
            #chunk_digest   = int_from_nounce(nounce=chunk_nounce, seed=seed)
            chunk_bytes    = bytes_from_nounce(nounce=chunk_nounce, seed=seed)
            chunk_item_ids = list(hash_chunks[chunk_id].position_mapping.keys())
            # разбираем число на байты и ищем совпадение
            for item_id in chunk_item_ids:
                item_params      = hash_chunks[chunk_id].position_mapping[item_id]
                #first_byte_value = int_bytes_from_digest(digest=chunk_digest, start_byte=item_params.item_start_byte, byte_length=1)
                first_byte_value = chunk_bytes[item_params.item_start_byte]
                # проверяем совпадает ли значение первого байта элемента с его порядеовым номером в сегменте
                if (first_byte_value == item_id):
                    #item_value = int_bytes_from_digest(digest=chunk_digest, start_byte=item_params.item_start_byte, byte_length=2)
                    #item_value = int_bytes_from_nounce(nounce=chunk_nounce, seed=seed, start_byte=item_params.item_start_byte, byte_length=2)
                    item_value = (first_byte_value * 256) + chunk_bytes[item_params.item_start_byte + 1]
                    # если совпадает - сохраняем значение элемента в итоговый список с результатами 
                    seed_values.append(item_value)
                    # саму позицию исключаем из дальнейшего поиска
                    del hash_chunks[chunk_id].position_mapping[item_id]
                    # проверяем остались ли еще позиции внутри сегмента
                    if (len(hash_chunks[chunk_id].position_mapping) == 0):
                        # если внутри фрагмента больше не осталось позиций - удаляем его из дальнейшего поиска
                        del hash_chunks[chunk_id]
                        # если фрагментов для сканирования больше не осталось - то поиск завершен
                        if (len(hash_chunks) == 0):
                            seed_loaded = True
        # end of search
        if seed_loaded:
            break
        # поднимаемся на 1 уровень если не нашли все значения на текущем
        seed_level += 1
    return seed_values

def get_next_undiscovered_shard_id(include_partial: bool=False) -> int:
    next_shard_id = 0
    for shard_id in range(0, MAX_SHARD_ID):
        if (is_completed_shard(shard_id=shard_id) is False):
            # определяем, подходят ли нам частично заполненные блоки или нужны только новые, в которых еще нет значений
            if (include_partial is True) and (is_started_shard(shard_id=shard_id) is True):
                next_shard_id = shard_id
                break
            if (include_partial is False) and (is_started_shard(shard_id=shard_id) is False):
                next_shard_id = shard_id
                break
    return next_shard_id

def get_next_undiscovered_shard_seed(shard_id: int) -> int:
    min_seed       = shard_id * MAX_SEEDS_PER_COLLECTION
    max_seed       = min_seed + MAX_SEEDS_PER_COLLECTION - 1
    ValueListClass = get_value_list_class(seed=min_seed)
    last_item      = ValueListClass.objects.order_by('-id').limit(1).first()
    if (last_item is not None):
        if (last_item.id == max_seed):
            return None
        else:
            return last_item.id + 1
    return min_seed

def is_completed_shard(shard_id: int) -> bool:
    """
    Определить закончен ли поиск для указанного блока seed-значений
    """
    next_seed = get_next_undiscovered_shard_seed(shard_id=shard_id)
    if (next_seed is None):
        return True
    return False

def is_started_shard(shard_id: int) -> bool:
    """
    Определить был ли начат поиск для указанного блока seed-значений 
    (поиск считается начатым если в коллекцию сохранено ли хотя бы 1 значение)
    Используется чтобы предотвращать пересечания процессов при параллельном сканировании
    """
    min_seed       = shard_id * MAX_SEEDS_PER_COLLECTION
    ValueListClass = get_value_list_class(seed=min_seed)
    first_item     = ValueListClass.objects(id=min_seed).limit(1).first()
    if (first_item is not None):
        return True
    return False

def discover_shard_values(shard_id: int=None, include_partial: bool=False):
    """
    Просканировать один блок сидов (65536 значений), вычислить и сохранить первые 256 значений каждого хеш-пространства
    """
    pending_items = list()
    # если номер блока сидов не передан - получаем первый не начатый 
    # (частично заполненные пропускаем - они могут использоваться другими процессами)
    if (shard_id is None):
        shard_id = get_next_undiscovered_shard_id(include_partial=include_partial)
        print(f"Auto detected next shard_id={shard_id}")
    min_seed = get_next_undiscovered_shard_seed(shard_id=shard_id)
    if (min_seed is None):
        print(f"shard_id={shard_id} already scanned - no action needed")
        return
    max_seed       = shard_id * MAX_SEEDS_PER_COLLECTION + MAX_SEEDS_PER_COLLECTION
    progress       = tqdm(range(min_seed, max_seed), miniters=4, smoothing=0)
    buffer_size    = 0
    ValueListClass = get_value_list_class(seed=min_seed)
    print(f"target_shard_id={shard_id}, min_seed={min_seed}, max_seed={max_seed}, total_seeds={max_seed-min_seed}, collection='{ValueListClass._get_collection_name()}'")

    for seed in progress:
        # получаем все значения 256 позиций для одного хеш-пространства
        #seed_values = load_seed_values(seed=seed)
        document = ValueListClass(
            id     = seed,
            values = fast_load_seed_values(seed=seed), #seed_values.copy(),
        )
        pending_items.append(document)
        # end of seed
        buffer_size += 1
        progress.set_description(f"shard_id={shard_id}, seed={seed}/{max_seed}, buffer_size: {buffer_size:4}", refresh=False)
        if ((seed % 2048) == 0):
            progress.set_description(f"shard_id={shard_id}, seed={seed}/{max_seed}, (saving...)", refresh=True)
            QuerySet(ValueListClass, collection=ValueListClass._get_collection()).insert(pending_items, load_bulk=False)
            pending_items.clear()
            buffer_size = 0
            progress.set_description(f"shard_id={shard_id}, seed={seed}/{max_seed}, buffer_size: {buffer_size:4}", refresh=True)
    # сохраняем последнюю часть созданных значений если их осталось меньше чем сохраняемый блок
    if (len(pending_items) > 0):
        progress.set_description(f"shard_id={shard_id}, seed={seed}/{max_seed}, (saving...)", refresh=True)
        QuerySet(ValueListClass, collection=ValueListClass._get_collection()).insert(pending_items, load_bulk=False)
        pending_items.clear()
        buffer_size = 0
        progress.set_description(f"shard_id={shard_id}, seed={seed}/{max_seed}, buffer_size: {buffer_size:4}", refresh=True)

def get_row_value_list_counts(row_values: List[int], min_values_per_seed: int=2, target_shards: List[int]=None) -> Counter:
    """
    Подсчитать количество совпадений для переданного ряда матрицы подстановок
    """
    # TODO: отдельно считать количество упоминаний значений
    value_counts = Counter()
    if (target_shards is None):
        max_shard_id  = get_next_undiscovered_shard_id(include_partial=False)
        target_shards = range(0, max_shard_id)
    # запрашиваем и считаем количество значений равными частями (1 коллекция - 65536 сидов, каждый сид содержит 256 значений)
    progress = tqdm(target_shards, miniters=1)
    for shard_id in progress:
        min_seed       = shard_id * MAX_SEEDS_PER_COLLECTION
        ValueListClass = get_value_list_class(seed=min_seed)
        progress.set_description_str(f"shard_id={shard_id+1}/{max_shard_id}, collection: {ValueListClass._get_collection_name()}")
        # получаем количество совпадений для каждого сида в виде счетчика чтобы было удобнее фильтровать значения
        shard_seeds = Counter()
        for row_value in row_values:
            # считаем количество совпадений для каждого сида  отдельными запросами к базе 
            # (потому что синтаксис запроса не позволяет подсчитать сколько элементов из переданного списка
            # содержатся в списке значений поля внутри записи)
            seeds = ValueListClass.objects(
                values__in = [row_value],
            ).only('id')
            for seed in seeds:
                shard_seeds.update({ seed.id: 1 })
        # запоминаем сколько было значений до фильтраций
        total_shard_seeds = len(shard_seeds)
        # отбрасываем сиды с недостаточным количеством совпадений
        shard_seeds = Counter(shard_seeds).most_common_above(min_values_per_seed)
        # обновляем статистику по накопленным значениям
        value_counts.update(shard_seeds)
        progress.set_postfix({
            "shard_seeds"   : f"{len(shard_seeds.copy())} (from {total_shard_seeds} items)",
            "ss_agregated" : f"{shard_seeds.copy().aggregated_counts().first_items()}",
            "value_counts" : f"({len(value_counts)}) {value_counts.most_common(8)}",
            "vc_agregated" : f"{value_counts.aggregated_counts().first_items()}",
        })
    return value_counts

def get_row_seeds(row_values: List[int]) -> Counter:
    row_length = len(row_values)
    min_values = math.ceil(row_length / 32)
    return get_row_value_list_counts(row_values=row_values, min_values_per_seed=min_values)

BitmapValuePosition = namedtuple('BitmapValuePosition', [
    'value',
    'absolute_position',
    'relative_position',
])

def get_row_value_positions(seed: int, row_values: List[int], chain_mapping: Dict[int, BitmapValuePosition]) -> Dict[int, BitmapValuePosition]:
    ValueListClass    = get_value_list_class(seed=seed)
    value_list        = ValueListClass.objects(id=seed).get()
    positions         = dict()
    relative_position = 0
    mapped_values     = list(chain_mapping.keys())
    mapped_positions  = [mapped_value.absolute_position for mapped_value in chain_mapping.values()]
    
    for position in range(0, 256):
        seed_value = value_list.values[position]
        if (seed_value in mapped_values):
            # в относительную нумерацию не входят значения других сидов
            continue
        if (position in mapped_positions):
            # в относительную нумерацию не входят позиции занятые другими сидами
            continue
        if (seed_value in row_values):
            positions[seed_value] = BitmapValuePosition(
                value             = seed_value,
                absolute_position = position,
                relative_position = relative_position,
            )
        relative_position += 1
    return positions

def update_seed_counts(seed_counts: Counter, min_count: int, row_values: List[int], chain_mapping: List[int]) -> Counter:
    new_counts = seed_counts.copy()
    for seed, _ in seed_counts.most_common_above(min_count).items():
        new_value_count  = len(get_row_value_positions(seed=seed, row_values=row_values, chain_mapping=chain_mapping))
        new_counts[seed] = new_value_count
        if (new_value_count == 0):
            del new_counts[seed]
    return new_counts

### BASE CLASSES ###

class BaseSeedValueList(Document):
    """
    Сжатая версия списка из 256 значений в хеш-пространстве, первый байт которого совпадает с его собственной позицией (0-255)
    Шаг позиции - 16 бит
    Хранит все значения внутри одной записи списком, вместо выделения одной записи на каждое значение, не хранит level и position
    (их всегда можно вычислить зная seed и они никогда не меняются)
    """
    # номер хеш-пространства, стартовое значение параметра seed для инициализации функции xxhash
    id     = IntField(primary_key=True)
    # отсортированный по возрастанию список из 256 значений, номер значения в списке соответствует его позиции
    # все значения в списке уникальны
    values = SortedListField(IntField(), null=False, required=True)
    # collection metadata
    meta = {
        'abstract': True,
        'indexes': [
          'values',
        ],
    }

### CACHE ###

CACHED_CLASSES     = dict()
CACHED_CLASS_NAMES = set()

def has_cached_class(class_name: str) -> bool:
    return (class_name in CACHED_CLASS_NAMES)

def get_cached_class(class_name: str) -> Union[BaseSeedValueList, Document]: # BaseExpansionValue
    return CACHED_CLASSES.get(class_name)

### COLLECTION FACTORIES ###

def seed_to_shard_id(seed: int) -> int:
    return seed // 65536

def get_value_list_class_name(shard_id: int) -> str:
    return f"SeedValueListS{shard_id:05}"
    
def get_value_list_collection_name(shard_id: int) -> str:
    return f"seed_value_list_s{shard_id:05}"

def make_value_list_class(seed: int) -> BaseSeedValueList:
    shard_id = seed_to_shard_id(seed=seed)
    return type(get_value_list_class_name(shard_id=shard_id), (BaseSeedValueList, ), {
        "meta" : {
            'collection': get_value_list_collection_name(shard_id=shard_id),
        }
    })

def get_value_list_class(seed: int) -> Union[BaseSeedValueList, Document]:
    shard_id   = seed_to_shard_id(seed=seed)
    class_name = get_value_list_class_name(shard_id=shard_id)
    if (has_cached_class(class_name) is False):
        value_list_class = make_value_list_class(seed=seed)
        CACHED_CLASSES[class_name] = value_list_class
        CACHED_CLASS_NAMES.add(class_name)
    return get_cached_class(class_name)


### DEPRECATED ###

class BaseExpansionValue(Document):
    """
    Значение в хеш-пространстве, первый байт которого совпадает с его собственной позицией (0-255)
    Шаг позиции - 16 бит
    """
    id         = IntField(primary_key=True)
    seed       = IntField(null=False, required=True)
    position   = IntField(null=False, required=True)
    level      = IntField(null=False, required=True)
    value      = IntField(null=False, required=True)
    # collection metadata
    meta = {
        'abstract': True,
        'indexes': [
          'seed',
          'value',
        ],
    }

def seed_to_part_id(seed: int) -> int:
    return seed // 65536

def get_value_class_name(part_id: int) -> str:
    return f"ExtensionValueP{part_id}"
    
def get_value_collection_name(part_id: int) -> str:
    return f"extension_value_p{part_id}"

def make_value_class(seed: int) -> BaseExpansionValue:
    part_id = seed_to_part_id(seed=seed)
    # creating class dynamically (OMG, Python, you are AWESOME!)
    # https://www.geeksforgeeks.org/create-classes-dynamically-in-python/
    return type(get_value_class_name(part_id=part_id), (BaseExpansionValue, ), {
        "meta" : {
            'collection': get_value_collection_name(part_id=part_id),
        }
    })

def get_value_class(seed: int) -> Union[BaseExpansionValue, Document]:
    shard_id   = seed_to_part_id(seed=seed)
    class_name = get_value_class_name(part_id=shard_id)
    if (has_cached_class(class_name) is False):
        value_class = make_value_class(seed=seed)
        CACHED_CLASSES[class_name] = value_class
        CACHED_CLASS_NAMES.add(class_name)
    return get_cached_class(class_name)


def get_row_value_counts(row_values: List[int], min_values_per_seed: int=2) -> Counter:
    """
    Подсчитать количество совпадений для переданного ряда матрицы подстановок
    """
    value_counts = Counter()
    max_part_id  = get_next_undiscovered_part_id(include_partial=False)
    progress     = tqdm(range(0, max_part_id), miniters=1, smoothing=0)
    # запрашиваем и считаем количество значений равными частями (1 коллекция - 65536 сидов, каждый сид содержит 256 значений)
    for part_id in progress:
        min_seed   = part_id * MAX_SEEDS_PER_COLLECTION
        ValueClass = get_value_class(seed=min_seed)
        min_value  = min(row_values)
        max_value  = max(row_values)
        progress.set_description_str(f"part_id={part_id+1}/{max_part_id}, collection: {ValueClass._get_collection_name()}")

        # считаем количество совпадений для каждого сида запросами к базе (так в несколько раз быстрее)
        part_items = ValueClass.objects(
            value__gte = min_value,
            value__lte = max_value,
            value__in  = row_values,
        ).only('seed').item_frequencies('seed') # .hint('seed_1_value_1')
        # получаем количество совпадений для каждого сида в виде счетчика чтобы было удобнее фильтровать значения
        part_seeds = Counter(part_items).most_common_above(min_values_per_seed)
        # отбрасываем сиды с недостаточным количеством совпадений
        value_counts.update(part_seeds)
        
        # обновляем статистику по накопленным значениям
        progress.set_postfix({
            "part_seeds"   : f"{len(part_seeds)} (from {len(part_items)} items)",
            "ps_agregated" : f"{part_seeds.aggregated_counts().first_items()}",
            "value_counts" : f"({len(value_counts)}) {value_counts.most_common(8)}",
            "vc_agregated" : f"{value_counts.aggregated_counts().first_items()}",
        })
    return value_counts

@lru_cache(maxsize=65536)
def get_hash_item_id(item: frozenbitarray) -> int:
    return ba2int(item, signed=False)

def discover_seed_values(part_id: int=None):
    """
    Просканировать один блок сидов (65536 значений), вычислить и сохранить первые 256 значений каждого хеш-пространства
    """
    last_saved_seed = None
    pending_items   = list()
    seed_items      = list()
    # если номер блока сидов не передан - получаем первый не начатый 
    # (частично заполненные пропускаем - они могут использоваться другими процессами)
    if (part_id is None):
        part_id = get_next_undiscovered_part_id(include_partial=False)
        print(f"Auto detected next part_id={part_id}")
    min_seed = get_next_undiscovered_seed(part_id=part_id)
    if (min_seed is None):
        print(f"part_id={part_id} already scanned - no action needed")
        return
    max_seed    = part_id * MAX_SEEDS_PER_COLLECTION + MAX_SEEDS_PER_COLLECTION
    progress    = tqdm(range(min_seed, max_seed), miniters=1, smoothing=0)
    buffer_size = 0
    ValueClass  = get_value_class(seed=min_seed)
    print(f"target_part_id={part_id}, min_seed={min_seed}, max_seed={max_seed}, total_seeds={max_seed-min_seed}, collection='{ValueClass._get_collection_name()}'")

    for seed in progress:
        # получаем все значения 256 позиций для одного хеш-пространства
        seed_items = load_seed_items(seed)
        for seed_item in seed_items:
            document = ValueClass(
                id       = seed_item.id,
                seed     = seed_item.seed,
                position = seed_item.position,
                level    = seed_item.level,
                value    = seed_item.value,
            )
            pending_items.append(document)
        # end of seed
        buffer_size += 1
        progress.set_description(f"seed={seed}/{max_seed}, last_saved_at={last_saved_seed}, buffer_size={buffer_size}", refresh=False)
        if ((seed % 256) == 0):
            progress.set_description(f"seed={seed}/{max_seed}, last_saved_at={last_saved_seed}, (saving...)", refresh=True)
            QuerySet(ValueClass, collection=ValueClass._get_collection()).insert(pending_items, load_bulk=False)
            pending_items.clear()
            buffer_size     = 0
            last_saved_seed = seed
            progress.set_description(f"seed={seed}/{max_seed}, last_saved_at={last_saved_seed}, buffer_size={buffer_size}", refresh=True)
    # сохраняем последнюю часть созданных значений если их осталось меньше чем сохраняемый блок
    if (len(pending_items) > 0):
        progress.set_description(f"seed={seed}/{max_seed}, last_saved_at={last_saved_seed}, (saving...)", refresh=True)
        QuerySet(ValueClass, collection=ValueClass._get_collection()).insert(pending_items, load_bulk=False)
        pending_items.clear()
        buffer_size     = 0
        last_saved_seed = seed
        progress.set_description(f"seed={seed}/{max_seed}, last_saved_at={last_saved_seed}, buffer_size={buffer_size}", refresh=True)

def is_started_part(part_id: int) -> bool:
    """
    Определить был ли начат поиск для указанного блока seed-значений 
    (поиск считается начатым если в коллекцию сохранено ли хотя бы 1 значение)
    Используется чтобы предотвращать пересечания процессов при параллельном сканировании
    """
    min_seed   = part_id * MAX_SEEDS_PER_COLLECTION
    ValueClass = get_value_class(seed=min_seed)
    first_item = ValueClass.objects(id=0).limit(1).first()
    if (first_item is not None):
        return True
    return False

def is_completed_part(part_id: int) -> bool:
    """
    Определить закончен ли поиск для указанного блока seed-значений
    """
    next_seed = get_next_undiscovered_seed(part_id=part_id)
    if (next_seed is None):
        return True
    return False

def get_next_undiscovered_seed(part_id: int) -> int:
    min_seed   = part_id * MAX_SEEDS_PER_COLLECTION
    ValueClass = get_value_class(seed=min_seed)
    last_item  = ValueClass.objects.order_by('-id').limit(1).first()
    if (last_item is not None):
        if (last_item.id == LAST_VALUE_COLLECTION_ID):
            return None
        else:
            return last_item.seed + 1
    return min_seed

def get_next_undiscovered_part_id(include_partial: bool=False) -> int:
    next_part_id = 0
    for part_id in range(0, MAX_PART_ID):
        if (is_completed_part(part_id=part_id) is False):
            # определяем, подходят ли нам частично заполненные блоки или нужны только новые, в которых еще нет значений
            if (include_partial is True) and (is_started_part(part_id=part_id) is True):
                next_part_id = part_id
                break
            if (include_partial is False) and (is_started_part(part_id=part_id) is False):
                next_part_id = part_id
                break
    return next_part_id

# value object for seed item value
SeedItem = namedtuple('SeedItem', [
    # уникальный id элемента в контексте одной коллекции (2**24 элементов)
    'id',
    'seed',
    'position',
    'level',
    # big endian
    'value',
    # уникальный id элемента среди всех значений во всех коллекциях (не сохраняется в базу)
    'absolute_id',
])
# list of numbers 0-255 (just for copying)
TARGET_POSITIONS = [p for p in range(0, 256)]

def load_seed_items(seed: int) -> List[SeedItem]:
    seed_items     = list()
    seed_positions = TARGET_POSITIONS.copy()
    seed_level     = 0
    seed_loaded    = False
    min_position   = 0
    max_position   = 255
    segment_length = 16 * (max_position - min_position) + 16
    # перебираем уровни (кратные участки позиций) пока не найдем все значения
    while (seed_loaded is False):
        segment_start  = (seed_level * 16 * 256)
        segment_start += (min_position * 16)
        segment_bits   = bits_at_position(bit_position=segment_start, bit_length=segment_length, seed=seed, frozen=False, endian='big')
        #print(f"min_position={min_position}, max_position={max_position}, segment_start={segment_start}, segment_length={segment_length}")
        for item_position in seed_positions:
            item_start   = (item_position * 16)
            # перемещаем начало чтения назад на столько же насколько был укорочен сегмент хеш-пространства чтобы всё совпало
            item_start  -= (min_position * 16)
            item_end     = item_start + 16
            item_bits    = segment_bits[item_start:item_end]
            item_value   = ba2int(item_bits, signed=False) #get_hash_item_id(item_bits)
            item_byte_id = (item_value // 256)
            # первый байт значения должен совпадать с его относительной позицией
            if (item_position == item_byte_id): # and (item_id not in seed_items): #ba2int(byte_0, signed=False)):
                seed_positions.remove(item_position)
                # вычисляем абсолютный и относительный id элемента
                part_id     = seed_to_part_id(seed=seed)
                absolute_id = (seed * 256) + item_position
                item_id     = absolute_id - (part_id * 65536 * 256)
                seed_item = SeedItem(
                    id          = item_id,
                    seed        = seed,
                    position    = item_position,
                    level       = seed_level,
                    value       = item_value,
                    absolute_id = absolute_id,
                )
                seed_items.append(seed_item)
                # прекращаем поиск если значения для всех позиций найдено
                if (len(seed_positions) == 0):
                    seed_loaded = True
                    break
                # вычисляем минимально необходимую длину загружаемого хеш-сегмента
                min_position   = min(seed_positions)
                max_position   = max(seed_positions)
                segment_length = 16 * (max_position - min_position) + 16
        # end of positions
        if seed_loaded:
            break
        # поднимаемся на 1 уровень если не нашли все значения на текущем
        seed_level += 1
    return seed_items

def load_seed_values(seed: int) -> List[int]:
    """
    Функция аналогична load_seed_items(), но более быстрая, возвращает только значения, без позиций и уровней
    """
    seed_values    = list()
    seed_positions = TARGET_POSITIONS.copy()
    seed_level     = 0
    seed_loaded    = False
    min_position   = 0
    max_position   = 255
    segment_length = 16 * (max_position - min_position) + 16
    # перебираем уровни (кратные участки позиций) пока не найдем все значения
    while (seed_loaded is False):
        segment_start  = (seed_level * 16 * 256)
        segment_start += (min_position * 16)
        segment_bits   = bits_at_position(bit_position=segment_start, bit_length=segment_length, seed=seed, frozen=False, endian='big')
        #print(f"min_position={min_position}, max_position={max_position}, segment_start={segment_start}, segment_length={segment_length}")
        for item_position in seed_positions:
            item_start   = (item_position * 16)
            # перемещаем начало чтения назад на столько же насколько был укорочен сегмент хеш-пространства чтобы всё совпало
            item_start  -= (min_position * 16)
            item_end     = item_start + 16
            item_bits    = segment_bits[item_start:item_end]
            item_value   = ba2int(item_bits, signed=False)
            item_byte_id = (item_value // 256)
            # первый байт значения должен совпадать с его относительной позицией
            if (item_position == item_byte_id):
                seed_positions.remove(item_position)
                seed_values.append(item_value)
                # прекращаем поиск если значения для всех позиций найдено
                if (len(seed_positions) == 0):
                    seed_loaded = True
                    break
                # вычисляем минимально необходимую длину загружаемого хеш-сегмента
                min_position   = min(seed_positions)
                max_position   = max(seed_positions)
                segment_length = 16 * (max_position - min_position) + 16
        # end of positions
        if seed_loaded:
            break
        # поднимаемся на 1 уровень если не нашли все значения на текущем
        seed_level += 1
    return seed_values