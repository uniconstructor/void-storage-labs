from datetime import datetime

# https://github.com/Textualize/rich
from rich import print
# https://rich.readthedocs.io/en/latest/pretty.html
from rich.pretty import pprint
import rich.repr

from rich import box
from rich.align import Align
from rich.console import Console, Group
from rich.layout import Layout
from rich.panel import Panel
from rich.progress import Progress, SpinnerColumn, BarColumn, TextColumn
from rich.syntax import Syntax
from rich.table import Table as RichTable
from rich.text import Text as RichText

console = Console()

# https://realpython.com/python-data-classes/#advanced-default-values
from dataclasses import dataclass

# https://more-itertools.readthedocs.io/en/stable/api.html#more_itertools.windowed
from more_itertools import adjacent, map_reduce, mark_ends, pairwise, windowed, seekable, peekable, spy, \
    bucket, split_when, split_before, split_at, \
    consecutive_groups, run_length, first_true, islice_extended,\
    unique_everseen, locate, replace, time_limited
# https://bitstring.readthedocs.io/en/latest/index.html
from bitstring import Bits, BitArray, BitStream, ConstBitStream
from collections import OrderedDict, defaultdict, namedtuple, deque
from typing import List, Dict, Set, Tuple, Optional, Union

import os, sys, io, leb128, json

if not 'workbookDir' in globals():
    workbookDir = os.getcwd()
os.chdir(workbookDir)

from hash_space_utils import count_split_values, collect_split_values, \
    get_min_bit_length, get_aligned_bit_length, bytes_at_position, value_at_position

from partition_utils import SeedData, ScoreLayerMapping, PartitionItem, DataPartitions, \
  define_min_partitions

from custom_counter import CustomCounter as Counter

from a1_v2_item_values import count_seed_score_for_data, \
  count_seed_range_score_for_data, init_seed_scores, map_data_to_seed_range

