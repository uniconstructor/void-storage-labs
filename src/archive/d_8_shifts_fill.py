# https://stackoverflow.com/questions/38340808/recursive-typing-in-python-3-5
from __future__ import annotations
import os
from rich import print
from rich.pretty import pprint
from custom_counter import CustomCounter as Counter
from tqdm import tqdm
from bitarray.util import ba2int, int2ba
from bitarray import bitarray, frozenbitarray
from b_8_shift_utils import NounceRange, get_nounce_ranges, find_target_nounces

file_dir  = os.path.dirname(os.path.realpath('__file__'))
file_name = os.path.join(file_dir, './data/AMillionRandomDigits.bin')
data      = bitarray(endian='little')
data_file = open(file=file_name, mode='rb')
data.fromfile(data_file)
data        = frozenbitarray(data) # [0:2**14] # first n bits - to make things faster
data_length = len(data)
print(f"file_name: {file_name}, size: {len(data)} bits, ({len(data) // 8} bytes), ({len(data) // 16} items)")

nounce_ranges = get_nounce_ranges()
range_id      = 1
for nounce_range in nounce_ranges:
    target_nounces = find_target_nounces(data=data, start_position=0, nounce_range=nounce_range)
    print(f"\n({range_id}/{len(nounce_ranges)}) target_nounces: {len(target_nounces)}")
    if (len(target_nounces) > 0):
        pprint(target_nounces)
        break
    range_id += 1