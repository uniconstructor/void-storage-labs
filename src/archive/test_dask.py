# https://stackoverflow.com/questions/38340808/recursive-typing-in-python-3-5
from __future__ import annotations
from email.policy import default
from sys import prefix
from typing import List, Dict, Set, Tuple, Optional, Union, Iterable
# https://github.com/Textualize/rich
from rich import print
# https://rich.readthedocs.io/en/latest/pretty.html
from rich.pretty import pprint

from bitarray.util import ba2int, int2ba
from bitarray import bitarray
from custom_counter import CustomCounter as Counter

import dask
#dask.config.set(scheduler='threads')
from dask.distributed import Client, as_completed
from dask.diagnostics import ProgressBar
import time, os

if not 'workbookDir' in globals():
    workbookDir = os.getcwd()
os.chdir(workbookDir)

from collections import defaultdict, namedtuple

from hash_range_iterator import HashIterator

ScanResult = namedtuple('ScanResult', [
    'prefixes',
    'prefix_positions',
    'start_position',
    'stop_position',
    'seed',
])

### task processing
# TODO: https://realpython.com/python-mmap/#sharing-data-between-processes-with-pythons-mmap
# TODO: https://github.com/ilanschnell/bitarray/blob/master/examples/mmapped-file.py
#       https://github.com/ilanschnell/bitarray/blob/master/doc/buffer.rst

def find_prefix(prefix_ids: Set[int], prefix_length: int, start_position: int, stop_position: int, seed: int) -> ScanResult:
    print(f"starting find_prefix(): {len(prefix_ids)} prefix_ids, pos: {start_position}-{stop_position}")
    prefix_positions = defaultdict(set)
    prefixes         = set()
    seed_iterator    = HashIterator(item_bit_length=prefix_length, start_position=start_position, stop_position=stop_position, seed=seed)
    
    #progress      = tqdm.tqdm(total=len(seed_iterator), mininterval=1)
    position = start_position
    for value in seed_iterator:
        value_id = ba2int(value, signed=False)
        if (value_id in prefix_ids):
            #print(f"located prefix: id={value_id}, p={position}, v={value} ({len(value)}), prl={prefix_length}")
            prefixes.add(value_id)
            prefix_positions[value_id].add(position)
        else:
            position += 1
    # результат возвращается только после завершения сканирования всего пространства
    return ScanResult(
        prefixes=prefixes,
        prefix_positions=prefix_positions,
        start_position=start_position,
        stop_position=stop_position,
        seed=seed,
    )

if __name__ == '__main__':
    file_name = os.path.join(os.getcwd(), "src/data/image-36kb.jpg")
    print(file_name)
    test_data = bitarray(endian='little')
    test_file = open(file=file_name, mode='rb')
    test_data.fromfile(test_file)

    prefix_ids    = set()
    prefix_length = 24
    start_prefix  = 0
    max_data_items = (len(test_data) // prefix_length)
    data_items          = list()
    data_item_positions = defaultdict(set)
    for i in range(0, max_data_items):
        end_prefix = start_prefix + prefix_length
        prefix_id  = ba2int(test_data[start_prefix:end_prefix], signed=False)
        prefix_ids.add(prefix_id)
        # сохраняем соотношение номер элемента / значение после разбиения
        data_items.append(prefix_id)
        data_item_positions[prefix_id].add(i)
        start_prefix = end_prefix

    pprint(prefix_ids, max_length=16)

    # create teaks
    tasks              = []
    positions_per_task = 2**24
    start_position     = 0
    item_length        = prefix_length
    for i in range(0, 10):
        stop_position = start_position + positions_per_task
        task          = (prefix_ids, item_length, start_position, stop_position, 0)
        tasks.append(task)
        start_position = stop_position

    pprint(tasks, max_length=10)    
    #pbar = ProgressBar()                
    #pbar.register() 

    #client  = Client(processes=False)
    client  = Client()
    futures = []
    for task in tasks:
        future = client.submit(find_prefix, *task)
        futures.append(future)
    
    # собираем итоговый результат
    remaining_prefix_ids = prefix_ids.copy()
    prefix_positions     = defaultdict(set)
    raw_prefix_positions = dict()
    all_prefix_positions = set()
    # как data_items только уже с заменой: ключ это номер элемента в данных при разбиении, 
    # значение - позиция начала элемента в пространстве хешей (то есть с кактх позиций нужно считать данные)
    # такими же частями как и в файле чтобы получилось одно и то же
    data_item_prefix_positions = dict()
    
    # получаем результаты сканирования по мере готовности
    seq = as_completed(futures)
    for future in seq:
        scan_result = future.result()
        if len(scan_result.prefixes) > 0:
            print(f"Result: {len(scan_result.prefixes)}")
            for prefix_id in scan_result.prefixes:
                prefix_id = int(prefix_id)
                remaining_prefix_ids.discard(prefix_id)
                prefix_ids.discard(prefix_id)
                # сохраняем все позиции каждого префикса]
                for prefix_position in scan_result.prefix_positions[prefix_id]:
                    prefix_positions[prefix_id].add(prefix_position)
                # дадаем новый мэппинг - уже для хеш-пространства
                for prefix_item_position in data_item_positions[prefix_id]:
                    data_item_prefix_positions[prefix_item_position] = min(prefix_positions[prefix_id])
                all_prefix_positions.add(min(prefix_positions[prefix_id]))
            
            print(f"+{len(scan_result.prefixes)} prefixes, ({len(remaining_prefix_ids)} remaining)")
            # добавляем задачи пока не будут найдены все значения
            if (len(prefix_ids) > 0):
                start_position = scan_result.stop_position
                stop_position  = start_position + positions_per_task
                task = (prefix_ids, item_length, start_position, stop_position, scan_result.seed)
                # добавляем задачу в очкредь
                new_future = client.submit(find_prefix, *task)
                seq.add(new_future)    
        else:
            print(f"No values located")
            # добавляем задачи пока не будут найдены все значения
            if (len(prefix_ids) > 0):
                start_position = scan_result.stop_position
                stop_position  = start_position + positions_per_task
                task = (prefix_ids, item_length, start_position, stop_position, scan_result.seed)
                # добавляем задачу в очкредь
                new_future = client.submit(find_prefix, *task)
                seq.add(new_future)
        
    pprint(sorted(all_prefix_positions), max_length=128)
    print(f"all_prefix_positions: {len(all_prefix_positions)}")
    