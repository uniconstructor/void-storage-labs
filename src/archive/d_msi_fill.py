from rich import print
from rich.pretty import pprint
from tqdm import tqdm

from multi_seed_utils import MIN_ITEM_LENGTH, discover_seed_item_values

from mongoengine import register_connection
register_connection('default', db=f'multi_seed_values', host='127.0.0.1', port=27017, maxPoolSize=300)

seed_bits       = int(input("seed_bits (3-5):"))
nounce_bits     = int(input("nounce_bits (3-5):"))
min_item_length = input("min_item_length:")

if (min_item_length == ''):
  min_item_length = 8
else:
  min_item_length = int(min_item_length)

print(f"Scanning seed_bits={seed_bits}, nounce_bits={nounce_bits}, min_item_length={min_item_length}:")

discover_seed_item_values(seed_bits=seed_bits, nounce_bits=nounce_bits, min_item_length=min_item_length)