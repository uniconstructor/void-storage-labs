from rich import print
from rich.pretty import pprint
# https://pyoxidizer.readthedocs.io/en/stable/pyoxidizer_packaging_static_linking.html
from tqdm import tqdm as tqdm_notebook
# https://bitstring.readthedocs.io/en/latest/index.html
from bitstring import Bits, BitArray, BitStream, ConstBitStream
from collections import Counter, defaultdict, namedtuple
# https://docs.python.org/3/library/typing.html
from typing import List, Dict, Tuple, Optional, Union, Set, Iterable
import leb128
import os
if not 'workbookDir' in globals():
    workbookDir = os.getcwd()
os.chdir(workbookDir)

from hash_space_utils import get_min_bit_length, get_aligned_bit_length, bytes_at_position
from rle_utils import create_rle_spans, encode_rle_spans, allocate_content_based_split
# https://more-itertools.readthedocs.io/en/stable/api.html#more_itertools.distinct_permutations
from more_itertools import distinct_permutations, distinct_combinations, split_into

# https://docs.mongoengine.org/
from mongoengine import *

connect(db='blib', host='127.0.0.1', port=27017, maxPoolSize=300)
# https://docs.mongoengine.org/guide/querying.html#advanced-queries
from mongoengine.queryset.visitor import Q
# https://docs.mongoengine.org/guide/connecting.html#switch-collection
from mongoengine.context_managers import switch_collection

class SpecialSeedValueQuerySet(QuerySet):
    def get_last_position(self, seed, value_length) -> int:
        last_item = self.filter(seed=seed, value_length=value_length).order_by('-position').first()
        if (last_item is None):
            return 0
        return last_item.position

class SpecialSeedValue(Document):
    seed                = IntField(min_value=0, max_value=2**(8*4))
    position            = IntField(min_value=0, max_value=2**(8*3))
    value               = BinaryField(max_bytes=4)
    value_length        = IntField(min_value=2, max_value=4)
    meta = {
        'queryset_class'    : SpecialSeedValueQuerySet,
        #'index_background'  : True,
        'auto_create_index' : True,
        'indexes' : [
            #'seed',
            #'position',
            #'-position',
            #'value',
            #'value_length',
            #('seed', 'value_length', 'position'),
            ('seed', 'value_length', 'value'),
            ('seed', 'value_length', '-position'),
        ]
    }

def encode_group_prefix(split_sizes: Iterable[int]) -> BitArray:
  if (len(split_sizes) % 4) != 0:
    raise Exception(f"{split_sizes}, l={len(split_sizes)}: incorrect length (must be grouped by 4 elements)")
  # assemble prefix bits into bytes
  prefix_bits = BitArray()
  for split_size in split_sizes:
    split_size_bits = BitArray(uint=split_size, length=2)
    prefix_bits.append(split_size_bits)
  # return result as bits
  return prefix_bits

def encode_group_prefix_to_varint(prefix_bits: BitArray) -> BitArray:
  # encode result to varint
  encoded_bytes = leb128.u.encode(prefix_bits.uintbe)
  # return result as bits
  return BitArray(bytes=encoded_bytes)

def decode_group_prefix_from_varint(varint_bits: Union[ConstBitStream, BitArray, Bits]) -> BitArray:
  # decode prefix from varint
  varint_bytes   = varint_bits.tobytes()
  decoded_int    = leb128.u.decode(varint_bytes)
  decoded_length = get_aligned_bit_length(decoded_int)
  decoded_bits   = BitArray(uint=decoded_int, length=decoded_length)
  # return result as bits
  return decoded_bits

def decode_group_prefix(encoded_prefix: BitArray) -> Iterable[int]:
  if (len(encoded_prefix) % 8) != 0:
    raise Exception(f"{encoded_prefix}, l={len(encoded_prefix)}: incorrect length (must be grouped by 8 bits)")
  # split bits into pairs
  split_sizes = list()
  for split_size in encoded_prefix.cut(2):
    split_sizes.append(split_size.uint)
  # return result as list of item sizes
  return split_sizes

def value_sizes_to_address_sizes(value_sizes: Iterable[int]) -> Iterable[int]:
  address_sizes = list()
  for value_size in value_sizes:
    address_sizes.append(value_size - 1)
  return address_sizes

def address_sizes_to_value_sizes(address_sizes: Iterable[int]) -> Iterable[int]:
  value_sizes = list()
  for address_size in address_sizes:
    value_sizes.append(address_size + 1)
  return value_sizes

def seed_to_split(seed: int) -> List[int]:
  seed_bits = Bits(uint=seed, length=8)
  split = [bit_pair.uint for bit_pair in seed_bits.cut(2)]
  return split

def split_to_seed(split: List[int]) -> int:
  prefix = encode_group_prefix(split)
  return prefix.uint

def value_length_to_position_length(value_length: int) -> int:
  if (value_length == 0):
    raise Exception(f"Incorrect value_length={value_length}")
  if (value_length > 4):
    raise Exception(f"Incorrect value_length={value_length}")
  if (value_length == 1):
    return 1
  return (value_length - 1)

def position_length_to_value_length(position_length: int) -> int:
  if (position_length == 0):
    return 1
  if (position_length > 3):
    raise Exception(f"Incorrect position_length={position_length}")
  return (position_length + 1)

def seed_to_item_value_length(seed: int) -> int:
  seed_split   = seed_to_split(seed)
  value_sizes  = address_sizes_to_value_sizes(seed_split)
  value_length = sum(value_sizes)
  return value_length

def seed_to_item_address_length(seed: int) -> int:
  seed_split     = seed_to_split(seed)
  address_length = 0
  for address_bytes in seed_split:
    if address_bytes == 0:
      address_length += 1
    else:
      address_length += address_bytes
  return address_length

def apply_split_to_data(data: ConstBitStream, split: List[int]) -> List[bytes]:
  value_split = address_sizes_to_value_sizes(split)
  split_bytes = list(split_into(data.tobytes(), value_split))
  return [bytes(sb_list) for sb_list in split_bytes]

def apply_seed_to_data(data: ConstBitStream, seed: int) -> List[bytes]:
  seed_split = seed_to_split(seed)
  return apply_split_to_data(data, seed_split)

def get_seed_items_from_data(data: ConstBitStream, seed: int) -> List[Tuple[int, bytes]]:
  data_split = apply_seed_to_data(data, seed)
  return [(len(item_bytes), item_bytes) for item_bytes in data_split].copy()

def find_seed_value(value_length: int, value: bytes, seed: int, excluded_positions: set=set()) -> Union[SpecialSeedValue, None]:
  if (len(excluded_positions) > 0):
    return SpecialSeedValue.objects(seed=seed, value_length=value_length, value=value, position__nin=excluded_positions)\
      .only('seed', 'position', 'value', 'value_length').limit(1).first()
  return SpecialSeedValue.objects(seed=seed, value_length=value_length, value=value)\
    .only('seed', 'position', 'value', 'value_length').limit(1).first()

def find_missing_value(value_length: int, value: bytes, excluded_positions: set=set()) -> Union[SpecialSeedValue, None]:
  all_seeds = list(range(0, 256))
  if (len(excluded_positions) > 0):
    return SpecialSeedValue.objects(seed__in=all_seeds, value_length=value_length, value=value, position__nin=excluded_positions)\
      .only('seed', 'position', 'value', 'value_length').limit(1).first()
  else:
    return SpecialSeedValue.objects(seed__in=all_seeds, value_length=value_length, value=value)\
      .only('seed', 'position', 'value', 'value_length').limit(1).first()

### DATABASE FILLING ####

def fill_special_seed(seed_splits: List[int], min_score: int=0, value_lengths: set=None):
  encoded_prefix    = encode_group_prefix(seed_splits)
  seed              = encoded_prefix.uint
  skipped_positions = defaultdict(list)
  total_saves_count = 0
  skip_item_count   = Counter()
  save_item_count   = Counter()
  
  split_item_count = 0
  split_length     = len(seed_splits) // 4
  split_score      = 0
  # pick only length used by value split by default
  if (value_lengths is None):
    value_lengths = set(address_sizes_to_value_sizes(seed_splits))
  
  # calculate default seed score
  for split_item in seed_splits:
    if (split_item > 0):
      split_item_count += 1
  split_score = split_item_count - split_length
  
  # filter seeds with lowest potential score
  if (split_score < min_score):
    print(f"{seed_splits}: score={split_score}, seed={seed} [SKIPPED]")
    return None
  else:
    print(f"{seed_splits}: score={split_score}, encoded={encoded_prefix.bin} seed={seed}, lengths={value_lengths} (max={max(value_lengths)})")
  
  seed_values         = list()
  pending_value_count = 0
  # save position values for each length
  for value_length in sorted(value_lengths):
    min_position = SpecialSeedValue.objects.get_last_position(seed=seed, value_length=value_length)
    max_position = 2 ** (8 * (value_length - 1))
    
    # skip already scanned seeds
    last_positions = [max_position, (max_position - 1), (max_position - 2), (max_position - 3), (max_position - 4)]
    if min_position in last_positions:
      print(f"vl={value_length}, min_position={min_position} (skipping)")
      continue
    
    # indicate presence of previously saved values
    if min_position > 0:
      print(f"vl={value_length}, min_position={min_position}")
    # find duplicate value positions
    prev_values             = set()
    duplicate_positions     = set()
    next_duplicate_position = None
    for prev_position in tqdm_notebook(range(0, max_position), mininterval=1):
      prev_value_bytes = bytes_at_position(prev_position * 8, value_length * 8, seed, use_bytearray=False)
      if prev_value_bytes in prev_values:
        duplicate_positions.add(prev_position)
        if prev_position <= min_position:
          skip_item_count.update({ value_length: 1 })
          duplicate_positions.discard(prev_position)
        continue
      prev_values.add(prev_value_bytes)
    # free memory asap
    prev_values.clear()
    
    # define first position to skip
    if len(duplicate_positions) > 0:
      next_duplicate_position = min(duplicate_positions)
    # find and save new values
    position_range = tqdm_notebook(range(min_position, max_position), mininterval=1, smoothing=0)
    for position in position_range:
      # skip duplicate positions
      #if position in duplicate_positions:
      if position == next_duplicate_position:
        duplicate_positions.discard(position)
        skip_item_count.update({ value_length: 1 })
        if len(duplicate_positions) > 0:
          next_duplicate_position = min(duplicate_positions)
        continue
      value_bytes = bytes_at_position(position * 8, value_length * 8, seed, use_bytearray=False)
      # init value
      seed_value = SpecialSeedValue(
        seed=seed,
        position=position,
        value=value_bytes,
        value_length=value_length
      )
      seed_values.append(seed_value)
      pending_value_count += 1
      save_item_count.update({ value_length: 1 })
      # flush values to db
      if (pending_value_count >= 8000):
        saved_values = QuerySet(SpecialSeedValue, collection=SpecialSeedValue._get_collection()).insert(seed_values, load_bulk=False)
        seed_values.clear()
        total_saves_count += pending_value_count
        pending_value_count = 0
        position_range.set_description_str(f"position={position}")
        position_range.set_postfix({
          "skips"      : f"{skip_item_count} ({sum(skip_item_count.values())})",
          "saved"      : f"{save_item_count} ({sum(save_item_count.values())})",
          "duplicates" : f"({len(duplicate_positions)})",
        })
    # save last batch of the values
    if (len(seed_values) > 0):
      saved_values = QuerySet(SpecialSeedValue, collection=SpecialSeedValue._get_collection()).insert(seed_values, load_bulk=False)
    seed_values.clear()
    total_saves_count += pending_value_count
    # update stats
    position_range.set_postfix({
      "skips" : f"{skip_item_count} ({sum(skip_item_count.values())})",
      "saved" : f"{save_item_count} ({sum(save_item_count.values())})",
      "duplicates" : f"({len(duplicate_positions)})",
    })
    pending_value_count = 0
    duplicate_positions.clear()
  
  return seed #skipped_positions

def create_byte_splits(prefix: List[int]=[]) -> List[List[int]]:
  byte_splits = list()
  for i in range(0, 256):
    v = 255 - i
    v0 = (v // 64) % 4
    v1 = (v // 16) % 4
    v2 = (v // 4) % 4
    v3 = (v // 1) % 4
    byte_split = prefix.copy() + [v0, v1, v2, v3]
    byte_splits.append(byte_split)
  return byte_splits

def scan_byte_splits(splits: List[List[int]]):
  splits = create_byte_splits()
  for split in splits:
    fill_special_seed(split)