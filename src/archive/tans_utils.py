# https://github.com/GarethCa/Py-tANS
# nextState = (currentState + (5/8) range + 3) % range
# https://fastcompression.blogspot.com/2014/02/fse-distributing-symbol-values.html

from rich import print
from rich.pretty import pprint

# https://pyoxidizer.readthedocs.io/en/stable/pyoxidizer_packaging_static_linking.html
from tqdm import tqdm as tqdm_notebook

# https://bitstring.readthedocs.io/en/latest/index.html
from bitstring import Bits, BitArray, BitStream, ConstBitStream

# https://docs.python.org/3/library/typing.html
from typing import List, Dict, Tuple, Optional, Union, Set, Iterable

from custom_counter import CustomCounter as Counter
from math import floor, ceil
import random
import numpy as np
import matplotlib.pyplot as plt

# define table
tableLog = 5
tableSize = 1 << tableLog

# Return the Index of the First Non-Zero Bit.
def first1Index(val):
    counter = 0
    while val > 1:
        counter += 1
        val = val >> 1
    return counter


# Define how often a symbol is seen, total should equal the table size
# symbol_occurrences = {"0":10,"1":10, "2":12}
symbol_occurrences = {}
print(symbol_occurrences, sum(symbol_occurrences.values()))

####
# Define the Initial Positions of States in StateList.
####
symbol_list = [symbol for symbol, occcurences in symbol_occurrences.items()]
cumulative = [0 for _ in range(len(symbol_list) + 2)]
for u in range(1, len(symbol_occurrences.items()) + 1):
    cumulative[u] = cumulative[u - 1] + list(symbol_occurrences.items())[u - 1][1]
cumulative[-1] = tableSize + 1

#####
# Spread Symbols to Create the States Table
#####
highThresh = tableSize - 1
stateTable = [0 for _ in range(tableSize)]
tableMask = tableSize - 1
step = (tableSize >> 1) + (tableSize >> 3) + 3
pos = 0
for symbol, occurrences in symbol_occurrences.items():
    for i in range(occurrences):
        stateTable[pos] = symbol
        pos = (pos + step) & tableMask
        while pos > highThresh:
            position = (pos + step) & tableMask
assert pos == 0
print(stateTable)

#####
# Build Coding Table from State Table
#####
outputBits = [0 for _ in range(tableSize)]
codingTable = [0 for _ in range(tableSize)]
cumulative_cp = cumulative.copy()
for i in range(tableSize):
    s = stateTable[i]
    index = symbol_list.index(s)
    codingTable[cumulative_cp[index]] = tableSize + i
    cumulative_cp[index] += 1
    outputBits[i] = tableLog - first1Index(tableSize + i)

#####
# Create the Symbol Transformation Table
#####
total = 0
symbolTT = {}
for symbol, occurrences in symbol_occurrences.items():
    symbolTT[symbol] = {}
    if occurrences == 1:
        symbolTT[symbol]["deltaNbBits"] = (tableLog << 16) - (1 << tableLog)
        symbolTT[symbol]["deltaFindState"] = total - 1
    elif occurrences > 0:
        maxBitsOut = tableLog - first1Index(occurrences - 1)
        minStatePlus = occurrences << maxBitsOut
        symbolTT[symbol]["deltaNbBits"] = (maxBitsOut << 16) - minStatePlus
        symbolTT[symbol]["deltaFindState"] = total - occurrences
        total += occurrences
print(symbolTT)

# Output NbBits to a BitStream
def outputNbBits(state, nbBits):
    mask = (1 << nbBits) - 1
    little = state & mask
    if nbBits > 0:
        string = "{:b}".format(little)
    else:
        return ""
    while len(string) < nbBits:
        string = "0" + string
    return string


# Encode a Symbol Using tANS, giving the current state, the symbol, and the bitstream and STT
def encodeSymbol(symbol, state, bitStream, symbolTT):
    # print(symbolTT, symbol)
    # symbolTT = symbolTT[symbol]
    symbolTT = symbolTT[bytes([symbol])]
    nbBitsOut = (state + symbolTT["deltaNbBits"]) >> 16
    bitStream += outputNbBits(state, nbBitsOut)
    state = codingTable[(state >> nbBitsOut) + symbolTT["deltaFindState"]]
    return state, bitStream


#####
# Generate a Decoding Table
#####
decodeTable = [{} for _ in range(tableSize)]
nextt = list(symbol_occurrences.items())
for i in range(tableSize):
    t = {}
    t["symbol"] = stateTable[i]
    index = symbol_list.index(t["symbol"])
    x = nextt[index][1]
    nextt[index] = (nextt[index][0], nextt[index][1] + 1)
    t["nbBits"] = tableLog - first1Index(x)
    t["newX"] = (x << t["nbBits"]) - tableSize
    decodeTable[i] = t

# Convert Bits from Bitstream to the new State.
def bitsToState(bitStream, nbBits):
    bits = bitStream[-nbBits:]
    rest = int(bits, 2)
    if nbBits == len(bitStream):
        remaining = ""
        return rest, remaining
    remaining = bitStream[:-nbBits]
    return rest, remaining


# Return a Symbol + New State + Bitstream from the bitStream and State.
def decodeSymbol(state, bitStream, stateT):
    symbol = stateT[state]["symbol"]
    nbBits = stateT[state]["nbBits"]
    rest, bitStream = bitsToState(bitStream, nbBits)
    state = stateT[state]["newX"] + rest
    return symbol, state, bitStream


# Split an Input String into a list of Symbols
def split(string):
    return [char for char in string]


#####
# Functions to Encode and Decode Streams of Data.
#####
def encodeData(inpu):
    bitStream = ""
    state, bitStream = encodeSymbol(inpu[0], 0, "", symbolTT)
    # state, bitStream = encodeSymbol(inpu[0], 0, bytearray(), symbolTT)
    bitStream = ""
    for char in inpu:
        state, bitStream = encodeSymbol(char, state, bitStream, symbolTT)
    bitStream += outputNbBits(state - tableSize, tableLog)  # Includes Current Bit
    return bitStream


def decodeData(bitStream):
    output = []
    state, bitStream = bitsToState(bitStream, tableLog)
    while len(bitStream) > 0:
        symbol, state, bitStream = decodeSymbol(state, bitStream, decodeTable)
        output = [symbol] + output
    return output


# Test Encoding
# input = "1102010120"
input = bytes.fromhex("deadbeefdebebebe")
bitStream = encodeData(input)

# Test Decoding
output = decodeData(bitStream)

# Assert that input and Output are the same
print([bytes([b]) for b in split(input)], " = input")
print(bitStream, " = bitStream, int =", int(f"0b{bitStream}", base=0))
print(output, " = output")
# assert(split(inpu) == output)
assert [bytes([b]) for b in split(input)] == output
