# https://stackoverflow.com/questions/38340808/recursive-typing-in-python-3-5
from __future__ import annotations
from rich import print
from rich.pretty import pprint
from custom_counter import CustomCounter as Counter
from tqdm import tqdm
from bitarray.util import ba2int, int2ba
from bitarray import bitarray, frozenbitarray
from collections import defaultdict, namedtuple
import math

from hash_space_utils import get_min_bit_length
from typing import List, Set, Dict, Tuple, Optional, Union, Iterable
# https://realpython.com/lru-cache-python/
from functools import lru_cache
from dataclasses import dataclass, field
# https://docs.python.org/3/library/enum.html
from enum import Enum, IntEnum
from copy import copy, deepcopy
import json

from mongoengine import Document, QuerySet, LongField, IntField, \
    BinaryField, StringField, DictField, ListField, SortedListField, BooleanField

# максимальное число id в запросе фильтрации
MAX_NUMBERS_IN_REQUEST    : int = 16384
# максимальное кольчество операций вставки в 1 запросе
MAX_NUMBERS_IN_SAVE_BATCH : int = 16384
# размер одного шарда (сканируемого непрерывного блока) хеш-пространства: указывается как количество сегментов
VALUE_SHARD_SIZE          : int = 2**24

class HailStoneNumber(Document):
    """
    Числа последовательности, получаемой формулой для гипотезы Коллатца называют числами-градинами
    потому что они поднимаются и опускаются вниз как частицы льда в облаке при формировании града
    """
    # значение числа как 64-битное целое - одновременно используется как id записи
    id      = LongField(primary_key=True)
    next_id = LongField(null=False, required=True)
    # collection metadata
    meta = {
        'indexes': [
            'next_id',
        ],
    }

def get_next_hailstone_number(number: int) -> int:
    """
    Получить следующий элемент последовательности для гипотезы Коллатца
    """
    if (number % 2) == 1:
        return (number * 3) + 1
    else:
        return (number // 2)

def get_prev_hailstone_numbers(number: int) -> Set[int]:
    """
    Получить возможные варианты для предыдущего элемента последовательности для гипотезы Коллатца
    """
    prev_numbers = set()
    # всегда существует как минимум 1 число из которого может быть получено это
    prev_numbers.add(number * 2)
    if ((number - 1) % 3) == 0:
        prev_number = (number - 1) // 3
        if prev_number > 0:
            prev_numbers.add(prev_number)
    return prev_numbers

def create_hailstone_number(number: int) -> HailStoneNumber:
    next_id = get_next_hailstone_number(number=number)
    return HailStoneNumber(
        id      = number,
        next_id = next_id,
    )

def has_hailstone_number(number: int) -> bool:
    existing_number = HailStoneNumber.objects(id=number).limit(1).first()
    if (existing_number is None):
        return False
    return True

CollatzSequence = namedtuple('CollatzSequence', [
    # число с которого начинается последовательность
    'first_id',
    # полный список чисел-градин: значений которые проходит функция Коллатца перед тем как войти в основной цикл 4->2->1->4
    'numbers',
    # последнее значение последовательности
    'last_id',
    # минимальное количество бит для числа в цепочке
    'min_bits',
    # номер последнего шага последовательности (длина найденной цепочки
    'step',
    # значение через которое должна пройти последовательность (если нужна не вся последовательность а только часть)
    # содержит None если необходимо получить все числа последовательности, включая основной цикл 4->2->1
    'target',
])

#@lru_cache(maxsize=2**16)
def get_collatz_sequence(number: int, skip_prev_numbers: bool=False, show_progress: bool=False, target_number: int=None) -> CollatzSequence:
    """
    Получить последовательность Коллатца начиная с указанного числа

    Параметры
    ----------
    number : int
        Первое число последовательности
    skip_prev_numbers : bool, optional
        Исключить ранее найденные числа: остановиться и вернуть последовательность как только 
        будет найдено первое значение меньше чем number [default: False]
    show_progress : bool, optional
        Показывать индикацию процесса (для ОЧЕНЬ больших чисел вроде 2**1024 и выше реально необходимо) [default: False]
    target_number : int, optional
        Число через которое должна пройти последовательность (если ищем цепочку чисел в одной последовательности).
        Если target_number указано - поиск будет остановлен дойдя до этого числа (но оно может и не встретиться - тогда
        будет возвращена вся последовательнось целиком).
        Если target_number не передано (или передано None) - то функция вернет всю последовательность [default: None]
    
    Returns
    -------
    out : CollatzSequence
        Объект со всеми числами и метаданными последовательности
    """
    first_id            = number
    last_id             = None
    step                = 0
    next_number         = number
    collatz_sequence    = list()
    length_counts       = Counter()
    min_bits            = number.bit_length()
    main_cycle_numbers  = set([4, 2, 1])
    
    if (show_progress):
        progress = tqdm(mininterval=0.5, smoothing=0, total=float("inf"))
    while True:
        collatz_sequence.append(next_number)
        next_number        = get_next_hailstone_number(number=next_number)
        next_number_length = next_number.bit_length()
        if (min_bits > next_number_length):
            min_bits = next_number_length
        if show_progress:
            progress.update(1)
            length_counts.update({ next_number_length: 1 })
            progress.set_description_str(f"step {step}, ({next_number_length} bits)", refresh=False)
            progress.set_postfix({
                "max_counts": f"{length_counts.first_items(8)}",
                "min_counts": f"{length_counts.last_items(8)}",
                "agregated" : f"{length_counts.aggregated_counts()}",
                "min_bits"  : f"{min_bits}",
            }, refresh=False)
        # считаем расстояние между начальным и конечным числом как количество шагов алоритма ("2/x" или "3*x+1")
        # которые нужно рекурсиво повторить чтобы получить из первого числа второе
        step += 1
        # если ищем последовательность проходящую через указанное число - то останавливаемся на нём если нашли
        if (target_number is not None) and (next_number == target_number):
            # в процессе конденсации (спуска вниз до основного цикла) мы наткнулись на нужное число - значит они части одной цепочки
            last_id = next_number
            collatz_sequence.append(last_id)
            break
        # проверяем дошли ли мы до чисел основного цикла
        if next_number in main_cycle_numbers:
            if next_number == 1:
                last_id = 1
                collatz_sequence.append(1)
                break
            # если дошли - то проходим цикл только 1 раз, для этого удаляем из цикла каждое число которое добавили в результат
            main_cycle_numbers.remove(next_number)
            #done_cycle_numbers.add(next_number)
            if len(main_cycle_numbers) == 0:
                last_id = next_number
                break
        if (skip_prev_numbers is True) and (next_number < number): # and has_hailstone_number(number=next_number):
            last_id = next_number
            break
    
    return CollatzSequence(
        first_id = first_id,
        numbers  = collatz_sequence,
        last_id  = last_id,
        min_bits = min_bits,
        step     = step,
        target   = target_number,
    )

@lru_cache(maxsize=2**16)
def has_number_in_sequence(start_sequence_number: int, target_number: int) -> bool:
    """
    Определить, встречается ли указанное число в последовательности, начинающейся с другого числа
    """
    if (start_sequence_number == target_number):
        return True
    sequence = get_collatz_sequence(number=start_sequence_number, skip_prev_numbers=False, show_progress=False, target_number=target_number)
    if (sequence.last_id == target_number):
        return True
    return False

def discard_exsisting_numbers(numbers: List[int]) -> List[int]:
    filtered_numbers = []
    total_batches    = math.ceil(len(numbers) / MAX_NUMBERS_IN_REQUEST)
    for batch_id in range(0, total_batches):
        start_id      = batch_id * MAX_NUMBERS_IN_REQUEST
        end_id        = batch_id + MAX_NUMBERS_IN_REQUEST
        batch_numbers = list()
        batch_numbers = numbers[start_id:end_id]
        existing_ids  = set(HailStoneNumber.objects(id__in=batch_numbers).scalar('id'))
        #pprint(existing_ids, max_length=5)
        for batch_number in batch_numbers:
            if (batch_number in existing_ids):
                continue
            filtered_numbers.append(batch_number)
    return filtered_numbers

def save_hailstone_numbers(numbers: List[int]):
    filtered_numbers = discard_exsisting_numbers(numbers=numbers)
    total_batches    = math.ceil(len(filtered_numbers) / MAX_NUMBERS_IN_SAVE_BATCH)
    batch_objects    = list()
    pending_numbers  = set()
    for batch_id in range(0, total_batches):
        start_id      = batch_id * MAX_NUMBERS_IN_SAVE_BATCH
        end_id        = start_id + MAX_NUMBERS_IN_SAVE_BATCH
        batch_numbers = list()
        batch_objects = list()
        batch_numbers = filtered_numbers[start_id:end_id]
        for new_number in batch_numbers:
            if new_number in pending_numbers:
                continue
            number_object = create_hailstone_number(number=new_number)
            batch_objects.append(number_object)
            pending_numbers.add(new_number)
        if (len(batch_objects) > 0):
            QuerySet(HailStoneNumber, collection=HailStoneNumber._get_collection()).insert(batch_objects, load_bulk=False)
            batch_objects = list()
    if (len(batch_objects) > 0):
        QuerySet(HailStoneNumber, collection=HailStoneNumber._get_collection()).insert(batch_objects, load_bulk=False)
        #batch_objects.clear()
    return None

def discover_hailstone_numbers(min_id: int, max_id: int):
    progress        = tqdm(range(min_id, max_id), mininterval=0.5, smoothing=0)
    pending_numbers = list()
    buffer_size     = 0
    # основной цикл сканирования и поиска
    for next_id in progress:
        sequence = get_collatz_sequence(number=next_id, skip_prev_numbers=True)
        for number in sequence.numbers:
            #if (number in pending_numbers):
            #    continue
            pending_numbers.append(number)
            buffer_size += 1
        progress.set_description_str(f"next_id={next_id}, buffered: {buffer_size}", refresh=False)
        # exclude already created values from the queue periodically, to make less write operations
        #if (buffer_size >= MAX_NUMBERS_IN_SAVE_BATCH):
        #    progress.set_description_str(f"next_id={next_id}, (filtering...)", refresh=True)
        #    pending_numbers = discard_exsisting_numbers(numbers=pending_numbers)
        #    buffer_size     = len(pending_numbers)
        # flush save buffer when we ready
        if (buffer_size >= MAX_NUMBERS_IN_SAVE_BATCH):
            progress.set_description_str(f"next_id={next_id}, (saving...)", refresh=False)
            save_hailstone_numbers(numbers=pending_numbers.copy())
            pending_numbers = list()
            buffer_size     = 0
            progress.set_description_str(f"next_id={next_id}, buffered: {buffer_size}", refresh=False)
    # flush buffer again in the end
    if (len(pending_numbers) > 0):
        progress.set_description_str(f"next_id={next_id}, (saving...)")
        save_hailstone_numbers(numbers=pending_numbers.copy())
        #pending_numbers.clear()
        pending_numbers = list()
        buffer_size     = 0
        progress.set_description_str(f"next_id={next_id}, buffered: {buffer_size}", refresh=True)

def get_last_bits_with_offset(data: frozenbitarray, value_length: int, offset: int) -> frozenbitarray:
    """
    Получить указанное количество бит данных с конца битового массива, с заданным смещением которое
    также задается с конца. Функция используется чтобы последовательно получать биты из файла 
    в обратном порядке и составлять из них последовательность Коллатца.
    Сначала ищутся отрезки файла начиная с конца, начиная с самых коротких.
    При распаковке же - наоборот, просто берется первое число (самое большое) и список её элементов,
    по которому можно восстановить весь файл до конца.
    Любая последовательность должна рано или поздно придти к циклу 4->2->1 (ну, вроде как должна - пока не доказано)
    а это значит что вычислив эту последовательность мы получим весь файл (ну, вроде как - пока не проверено)
    """
    if (data.endian() != 'big'):
        raise Exception(f"Only big-endian data allowed ({data.endian()} given)")
    data_length = len(data)
    item_end    = data_length - offset
    item_start  = item_end - value_length
    if (item_start < 0):
        raise Exception(f"Incorrect value_length={value_length} or offset={offset} for data_length={data_length} (item_start={item_start})")
    return data[item_start:item_end]

def get_data_item(data: frozenbitarray, min_value_length: int, offset: int) -> frozenbitarray:
    """
    Получить следующий фрагмент данных, длина которого в битах всегда совпадает с его целочисленным представлением
    (то есть без ведущих нулей - иначе мы их потом обратно не восстановим)
    """
    # максимальная длина одного элемента - все оставшиеся до отступа биты
    max_value_length = (len(data) - offset)
    if (min_value_length < 1):
        raise Exception(f"Incorrect min_value_length={min_value_length} (must be at least 1)")
    for value_length in range(min_value_length, max_value_length):
        item_value_bits   = get_last_bits_with_offset(data=data, value_length=value_length, offset=offset)
        item_value_number = ba2int(item_value_bits, signed=False)
        #print(f"{value_length}: item_value_bits={item_value_bits} ({item_value_number})")
        if (item_value_number == 0):
            # нужна хотя бы одна единица в числе
            continue
        if (item_value_number.bit_length() != len(item_value_bits)):
            # в числе есть ведущий ноль - продолжаем пытаться пока не получим
            continue
        #if (has_number_in_sequence(start_sequence_number=start_sequence_number, target_number=item_value_number) is True):
        #    # число уже встречалось в предыдущих элементах - оно не подходит, нам нужны числа по порядку
        #    continue
        return item_value_bits
    raise Exception(f"End of data")