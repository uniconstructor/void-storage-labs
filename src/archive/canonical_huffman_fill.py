import os
from rich import print
from rich.pretty import pprint
#from tqdm import tqdm
from bitarray import bitarray, frozenbitarray
from canonical_huffman_utils_v2 import HuffmanPrefixTree

from mongoengine import register_connection
register_connection('default', db=f'multi_seed_trees', host='127.0.0.1', port=27017, maxPoolSize=300)

file_dir  = os.path.dirname(os.path.realpath('__file__'))
file_name = os.path.join(file_dir, './data/AMillionRandomDigits.bin')
#file_name = os.path.join(file_dir, './data/image-144kb.jpg')
data      = bitarray(endian='little')
data_file = open(file=file_name, mode='rb')
data.fromfile(data_file)
data        = frozenbitarray(data) # [0:2**14] # first n bits - to make things faster
data_length = len(data)
print(f"file_name: {file_name}, size: {len(data)} bits, ({len(data) // 8} bytes), ({len(data) // 16} items)")

tree = HuffmanPrefixTree(data=data, default_score=1)
#tree.register_nounce(seed=1, nounce=1)
#pprint(tree, max_length=16)
tree.encode_data()
pprint(dict(tree.nounce_codes), max_length=16)