# https://github.com/Textualize/rich
from rich import inspect
from rich import print as rprint
# https://rich.readthedocs.io/en/latest/pretty.html
from rich.pretty import pprint
# https://rich.readthedocs.io/en/latest/protocol.html
from rich.console import Console, OverflowMethod
console = Console()
# https://rich.readthedocs.io/en/latest/traceback.html
from rich.traceback import install
install(show_locals=True)
# https://rich.readthedocs.io/en/latest/logging.html
import logging
from rich.logging import RichHandler
logging.basicConfig(
    level="INFO",
    format="%(message)s",
    datefmt="[%X]",
    handlers=[RichHandler(rich_tracebacks=True, locals_max_length=16)]
)
log = logging.getLogger("rich")
# https://pyoxidizer.readthedocs.io/en/stable/pyoxidizer_packaging_static_linking.html
import tqdm

# https://docs.python.org/3/library/itertools.html
from itertools import count, accumulate, zip_longest
# https://more-itertools.readthedocs.io/en/stable/api.html
from more_itertools import windowed, sliced
# https://bitstring.readthedocs.io/en/latest/index.html
from bitstring import Bits, BitArray, BitStream, ConstBitStream
# https://pythongeeks.org/ordereddict-in-python/
# https://www.geeksforgeeks.org/counters-in-python-set-1/
# https://docs.python.org/3/library/collections.html#counter-objects
from collections import OrderedDict, Counter, defaultdict, namedtuple, deque
# https://github.com/multiformats/unsigned-varint
# https://github.com/fmoo/python-varint
import varint
# https://docs.python.org/3/library/typing.html
from typing import List, Dict, Set, Tuple, Optional, Union
# https://docs.python.org/3.6/library/random.html#module-random
import random
# https://www.geeksforgeeks.org/floor-ceil-function-python/?ref=lbp
import math
import operator
from statistics import mean
# https://stackoverflow.com/questions/55212014/python-count-copies-in-dictionary
import copy
# https://github.com/eerimoq/detools
# https://pypi.org/project/delta-of-delta/
# https://github.com/blester125/delta-of-delta
# https://github.com/blester125/delta-of-delta/blob/master/tests/test_encoders.py#L155
from delta_of_delta import delta_encode, delta_of_delta_encode, delta_of_delta_decode, delta_decode
# https://smarx.com/posts/2020/09/dynamic-programming-is-easy/
# https://smarx.com/posts/2020/12/memoization-in-one-line-of-python/
from functools import lru_cache
# https://docs.python.org/3.8/library/difflib.html#difflib.SequenceMatcher.get_opcodes
from difflib import SequenceMatcher

import os, sys
if not 'workbookDir' in globals():
    workbookDir = os.getcwd()
os.chdir(workbookDir)

# функции для работы с хеш-пространством
from hash_space_utils import HashItemAddress, \
    value_at_position, \
    read_hash_item, \
    split_data, \
    count_split_values, \
    collect_split_positions \
    

from hash_item_tiers import Tier, \
    init_search_tiers, \
    get_next_available_tier, \
    create_raw_value_from_hash, \
    has_tiers_value, \
    set_tier_value, \
    get_new_item_tier, \
    peek_new_value_from_data, \
    read_new_value_from_data, \
    get_min_varint_value, \
    get_max_varint_value, \
    get_max_varint_items, \
    get_varint_length, \
    get_varint_position_tier, \
    get_tier_position_bit_length, \
    get_position_tier, \
    get_max_tier_position, \
    get_min_tier_position, \
    get_tier_size, \
    get_max_seed_value, \
    get_max_position_tier, \
    get_min_tier_count, \
    get_max_tier_count, \
    get_tier_counts


from hash_item_db import RawValue, \
    save_raw_value, \
    has_saved_value, \
    get_first_saved_value

# seed-значение закрепленное за фрагментом данных: связывает varint-позицию и seed-значение уникальным образом
# позволяет задать seed-значение один раз в словаре, а в данных использовать только varint-значения
PositionSeed  = namedtuple('PositionSeed', ['position', 'seed'])
# значение элемента в указанной позиции
PositionItem = namedtuple('PositionItem', ['position', 'value']) # , 'position_length', 'value_length'
# список из 256 байт которые указывают на те seed-значения которые должны быть использованы чтобы произвести
# замену двух 8-битных элементов одним 16-битным в своей позиции
# каждый элемент из списка содержит 1 байт и заменяет собой 2
TransormationItems = namedtuple('TransormationItems', ['position_seeds'])
# параметры одного шага трансформации (замены 256 двухбайтовых пар столько же однобайтовых указателей на seed-значения)
# не содержит заменяемые байты, но может хранить список дополнительных seed-значений которые заменяют ранее используемые seed
# поскольку разных seed-значений может быть одновременно только 256 - новые seed должны содержать информацию о трех вещах:
# полное seed-значение, и позиция на которую оно встает в seed-словаре
# байты данных которые на этом шаге будут ссылаться на эту позицию будут использовать новое значение
# значение остается в указанной позиции на последующих шагах
TransformationOptions = namedtuple('TransformationOptions', ['new_seeds', 'finished_positions'])

DEFAULT_ITEM_FORMAT            = 'hex'
DEFAULT_ITEM_BIT_DEPTH         = 8
DEFAULT_POSITION_BIT_DEPTH     = 8
DEFAULT_SEED_BIT_DEPTH         = 8
DEFAULT_LIST_LENGTH            = 2
DEFAULT_INPUT_BIT_DEPTH        = DEFAULT_LIST_LENGTH * DEFAULT_ITEM_BIT_DEPTH
DEFAULT_SEED_OPTIONS_BIT_DEPTH = DEFAULT_SEED_BIT_DEPTH * 2

class DataTransformer:
    """
    Класс для чтения и упаковки данных в хеш-значения

    >>> data = ConstBitStream(filename='./data/image.jpg')[0:256*8]
    >>> dt   = DataTransformer(data)
    >>> len(dt.data) == len(data)
    True
    """

    # исходные данные (неизменяемый объект)
    data                     : ConstBitStream
    # итоговый результат: байты в varint-кодировке, которые хранят последовательность позиций которые нужно прочитать из хеш-поля
    # seed-значения хранятся отдельно, прикрепляются к итоговому результату как словарь
    result                   : BitArray
    # список преобразований, элементов данных по порядку - содержит только списки одинаковой длины (256 байт)
    # которых каждый байт указывает на один из 256 выбранных seed и стоит в той позиции откуда будет считываться
    # 16-битное значение при распаковке этого байта
    item_transformations     : Dict[int, TransormationItems]
    # параметры для всех шагов преобразования в одном слое
    transformation_options   : Dict[int, TransformationOptions]
    # значения-остатки: не участвуют в заменах, имеются в конце тех списков позиций в которых количество значений
    # не кратно количеству элементов в замене
    suffix_items             : Dict[int, str]
    # все seed-значения используемые для замены групп на одиночные значения
    seeds                    : Set[int]
    # seed-значения, используемые на этом шаге при преобразовании данных: каждый seed указывает на пространство
    # в котором первые 256 16-битных значений содержат как можно больше совпадений с парами из position_items
    # ключ - позиция (число), значение - seed (число), который подставляется при замене чтобы загрузить более 
    # длинное значение из хешей взамен более короткого на ту же позицию
    current_seeds            : Dict[int, int]
    # списки seed-значений, накопленные каждой позицией при поиске замен для групп
    # ключ - позиция (число), значение - список seed-значений использованных в этой позиции
    # элементы перечисляются в том же порядке в котором идут элементы группы
    # список содержит столько же элементов сколько и список групп в этой же позиции
    position_seeds           : Dict[int, List[int]]
    # списки элементов данных, накопленные каждой позицией, за вычетом suffix_items 
    # (всегда делятся на группы нужной длины для выполнения замен)
    position_items           : Dict[int, List[str]]
    # группы значений для замены созданные из списков значений путем объединения соседних элементов
    position_groups          : Dict[int, List[List[str]]]
    # id групп, распределенные по позициям для каждой из которых нужно найти seed который позволит из этой позиции
    # прочитать все элементы списка полностью, одним фрагментом
    # содержит только уникальные id списков в каждой позиции, id удаляются из списка позиции по мере нахождения
    required_position_groups : Dict[int, Set[str]]
    # количество упоминаний каждого значения
    item_counts              : Counter
    # количество упоминаний для каждой группы значений
    group_counts             : Counter
    # количество упоминаний каждого seed-значения
    seed_counts              : Counter
    # количество упоминаний для каждой группы значений разбитое по позициям
    position_group_counts    : Dict[int, Counter]
    # seed-значения, распределенные по позициям ключи - id групп
    position_group_seeds     : Dict[int, Dict[str, Counter]]
    # количество упоминаний каждого seed-значения, разбитое по позициям
    position_seed_counts     : Dict[int, Counter]
    # id групп распределенные по позициям, ключи - seed-значения
    position_seed_groups     : Dict[int, Dict[int, Counter]]
    # полное количество элементов во всем потоке данных после разбиения
    data_item_count          : int
    # максимальное количество контейнеров-позиций по которым распределяются позиции
    max_position_bucket      : int
    # максимальный id для seed-значений, используемых для этого потока данных
    max_seed_bucket          : int
    # максимальное количество всех уникальных seed-значений используемых на всех шагах сжатия 
    max_seed_option_bucket   : int
    # максимальное int-значение одного элемента, используемое для этого потока данных
    max_item_value           : int
    # формат в котором извлекаются и сравниваются элементы
    data_item_format         : str = DEFAULT_ITEM_FORMAT
    # количество позиций для замены - совпадает с количеством контейнеров для значений
    position_bit_depth       : int = DEFAULT_POSITION_BIT_DEPTH
    # количество уникальных значений
    item_bit_depth           : int = DEFAULT_ITEM_BIT_DEPTH
    # количество активных вариантов seed-значений, используемых при замене длинных значений короткими
    # состав элементов может меняться на каждом шаге, список элементов составляется из seed_options
    seed_bit_depth           : int = DEFAULT_SEED_BIT_DEPTH
    # количество всех уникальных seed-значений используемых на всех шагах сжатия 
    # (общее хранилище из которого составляется список активных seed-значений для каждого шага)
    seed_options_bit_depth   : int = DEFAULT_SEED_OPTIONS_BIT_DEPTH
    # минимальное количество элементов, необходимое для выполнения операции замены двух элементов одним
    items_per_transformation : int = DEFAULT_LIST_LENGTH
    # длина заменяемого элемента
    list_bit_depth           : int = DEFAULT_INPUT_BIT_DEPTH

    def __init__(self, data: ConstBitStream):
        self.data                     = data
        self.result                   = BitArray()

        self.max_position_bucket      = 2 ** self.position_bit_depth
        self.max_seed_bucket          = 2 ** self.seed_bit_depth
        self.max_item_value           = 2 ** self.item_bit_depth
        self.max_seed_option_bucket   = 2 ** self.seed_options_bit_depth

        self.suffix_items             = defaultdict(str)
        self.current_seeds            = defaultdict(int)

        self.position_seeds           = defaultdict(list)
        self.position_items           = defaultdict(list)
        self.position_groups          = defaultdict(list)

        self.item_counts              = Counter()
        self.group_counts             = Counter()
        self.seed_counts              = Counter()

        self.position_group_counts    = defaultdict(lambda: defaultdict(Counter))
        self.position_seed_counts     = defaultdict(lambda: defaultdict(Counter))
        self.position_group_seeds     = defaultdict(lambda: defaultdict(lambda: defaultdict(Counter)))
        self.position_seed_groups     = defaultdict(lambda: defaultdict(lambda: defaultdict(Counter)))

        self.required_position_groups = defaultdict(set)
        self.data_item_count          = 0
        
        # предварительная подготовка значений
        self.init_item_counts()
        self.init_position_seeds()
        self.collect_data_items()
        self.init_suffix_items()
        self.init_position_groups()
        self.collect_position_seeds()
    
    def init_item_counts(self) -> Counter:
        """
        >>> data = ConstBitStream(filename='./data/image.jpg')[0:256*8]
        >>> dt   = DataTransformer(data)
        >>> len(dt.item_counts)
        256
        """
        for item_value in range(0, self.max_item_value):
            format_getter = operator.attrgetter(self.data_item_format)
            item_key      = format_getter(Bits(uint=item_value, length=self.item_bit_depth))
            self.item_counts.update({item_key: 0})
        return self.item_counts
    
    def init_position_seeds(self) -> dict:
        """
        >>> data = ConstBitStream(filename='./data/image.jpg')[0:256*8]
        >>> dt   = DataTransformer(data)
        >>> len(dt.position_seeds) == dt.max_seed_bucket
        True
        >>> len(dt.position_seeds) == 2 ** dt.position_bit_depth
        True
        >>> dt.position_seeds[0]
        """
        for seed_bucket_id in range(0, self.max_seed_bucket):
            self.position_seeds[seed_bucket_id] = None
        return self.position_seeds

    def collect_data_items(self):
        """
        Разделить исходные данные на элементы одинаковой длины (self.item_bit_depth) и распределить их
        по заданному в настройках количеству списков (256), каждый список содержит только значения 
        в позициях кратных своему номеру, в том же порядке в котором они добавлялись в него, 
        без преобразований и со всеми повторами
        Получившийся набор списков позволяет без потерь собрать исходные данные обратно из этих же элементов
        если совершить весь процесс чтения списка еще раз: начать с элемента в позиции 0 на каждом новом шаге 
        преобразовывать номер шага как остаток от деления на 256 и забирая один элемент из списка с этим номером

        >>> data = ConstBitStream(filename='./data/image.jpg')
        >>> dt   = DataTransformer(data)
        >>> len(dt.data) // dt.item_bit_depth
        172603
        >>> dt.data_item_count == (len(dt.data) // dt.item_bit_depth)
        True
        >>> dt.item_counts.most_common(3)
        [('00', 1587), ('8c', 1120), ('1c', 1110)]
        >>> dt.position_items[0][0:3]
        ['ff', '42', '4a']
        >>> len(dt.position_items[0])
        674
        >>> dt.suffix_items[0]
        '45'
        """
        self.data.bitpos = 0
        start = 0
        end   = len(self.data) // self.item_bit_depth
        for item_position in range(start, end):
            # определяем в какой контейнер направить значение
            position_bucket = (item_position % self.max_position_bucket)
            # читаем 1 элемент из потока исходных данных
            data_value      = self.data.read(f"{self.data_item_format}:{self.item_bit_depth}")
            # обновляем счетчик упоминаний
            self.item_counts.update({data_value: 1})
            # распределяем значение в контейнер в зависимости от позиции
            self.position_items[position_bucket].append(data_value)
        # вычисляем общее количество элеменов данных
        self.data_item_count = sum(self.item_counts.values())
        # перемещаем указатель обратно на 0
        self.data.bitpos = 0
    
    def init_suffix_items(self):
        """
        Разбить исходные данные на элементы равной длины, распределить элементы по контейнерам в зависимости от их позиции

        >>> data = ConstBitStream(filename='./data/image.jpg')[0:516*8]
        >>> dt   = DataTransformer(data)
        >>> list(dt.suffix_items.values())[0:10]
        ['4a', '53', '54', '55', None, None, None, None, None, None]
        >>> len(dt.suffix_items) == dt.max_position_bucket
        True
        """
        for position_bucket in range(0, self.max_position_bucket):
            items_count = len(self.position_items[position_bucket])
            if ((items_count % 2) == 0):
                self.suffix_items[position_bucket] = None
                continue
            last_item = self.position_items[position_bucket].pop()
            self.suffix_items[position_bucket] = last_item
        return self.suffix_items

    def init_position_groups(self):
        """
        Разбить список элементов в каждой позиции на группы равной длины

        >>> data = ConstBitStream(filename='./data/image.jpg')[0:1030*8]
        >>> dt   = DataTransformer(data)
        >>> dt.position_items[0]
        ['ff', '42', '4a', 'd2']
        >>> dt.suffix_items[0]
        '79'
        >>> list(dt.position_groups[0])
        [('ff', '42'), ('4a', 'd2')]
        >>> dt.group_counts['ff42']
        1
        >>> dt.group_counts['4ad2']
        1
        >>> dt.position_group_counts[0]['ff42']
        1
        >>> dt.position_group_counts[0]['4ad2']
        1
        >>> dt.required_position_groups[0]
        {'4ad2', 'ff42'}
        """
        for position_bucket in range(0, self.max_position_bucket):
            position_items  = self.position_items[position_bucket]
            # соединяем элементы в группы (число элементов в группе задается в self.items_per_transformation)
            position_groups = zip_longest(*[iter(position_items)] * self.items_per_transformation, fillvalue=None)
            # сохраняем все группы в контейнер позиции
            self.position_groups[position_bucket] = list(position_groups).copy()
            for position_group in self.position_groups[position_bucket]:
                group_id = "".join(position_group)
                # обновляем счетчики упоминаний
                self.group_counts.update({group_id: 1})
                self.position_group_counts[position_bucket].update({group_id: 1})
                # собираем уникальные id групп в контейнер позиции чтобы на следующем этапе
                # отслеживать найденные группы и удалять их из этого списка 
                self.required_position_groups[position_bucket].add(group_id)
        return self.position_groups
    
    def collect_position_seeds(self):
        """
        Собрать все seed-значения которые содержат непрерывные фрагменты данных (items) в нужных позициях
        Метод запускается после объединения значений позиций в группы и ищет все возможные seed-значения 
        для списка групп в каждой позиции

        >>> data = ConstBitStream(filename='./data/image.jpg')[0:1030*8]
        >>> dt   = DataTransformer(data)
        >>> dt.position_seeds
        """
        # ищем списки seed-значений по позициям, чтобы было равномернее 
        # (каждый контейнер позиции содержит примерно равное количество элементов)
        for position_bucket in range(0, self.max_position_bucket):
            # position_seeds = self.get_seeds_for_position_groups(position_bucket)
            # unique_seeds   = set(position_seeds)
            self.get_seeds_for_position_groups(position_bucket)
        return self.position_seeds

    def get_seeds_for_position_groups(self, position_bucket: int) -> Set[int]:
        """
        Найти или получить из кеша все seed-значения для всех групп в указанной позиции
        """
        # количество seed-значений в позиции должно совпадать с количеством групп
        group_ids = self.required_position_groups[position_bucket]
        # ищем для этой позиции seed который можно использовать для создания одной группы из списка
        for group_id in group_ids:
            # получаем существующее значение либо находим новое (функция всегда должна возвращать валидный seed)
            seed = self.get_seed_for_position_group(position_bucket, group_id)
            # регистрируем найденый seed в общем списке
            self.seeds.add(seed)
            # добавляем seed-значение в список этой позиции
            self.position_seeds[position_bucket].append(seed)
            # обновляем общий счетчик использований
            self.seed_counts.update({seed: 1})
            # обновляем счетчик использований в конкретной позиции
            self.position_seed_counts[position_bucket].update({seed: 1})
            # запоминаем распределение по группам: 
            # списки групп, распределеные по seed-значениям 
            # (ключ - seed, значение - список групп, которые можно создать этим значением в этой позиции)
            self.position_seed_groups[position_bucket][seed].update({group_id: 1})
            # списки seed-значений, распределенные по группам
            # (ключ - группа, значение - список seed-значений, из которых можно создать эту группу в этой позиции)
            self.position_group_seeds[position_bucket][group_id].update({seed: 1})
        return self.position_seeds[position_bucket]

    def get_seed_for_position_group(self, position_bucket: int, group_id: str) -> int:
        """
        Получить значение из внутреннего кеша внутри класса: проверяется первым
        """
        return None

    def has_seed_for_position_group(self, position_bucket: int, group_id: str) -> bool:
        """
        Проверить есть ли seed-значение для указанной группы и переданной позиции
        Проверяет кеш класса и наличие записи в базе
        Возвращает False если значение ни в кеше ни в базе не найдено и его нужно искать перебором в пространстве хешей
        """
        position_group_seeds = set(self.position_group_seeds[position_bucket][group_id].values())
        if (len(position_group_seeds) > 0):
            return True
        if (self.has_saved_seed_for_position_group(position_bucket, group_id)):
            return True
        return False
    
    def has_saved_seed_for_position_group(self, position_bucket: int, group_id: str) -> bool:
        """
        Проверить есть ли для списка в этой позиции нужное seed-значение в базе
        """
        return False

    def get_saved_seed_for_position_group(self, position_bucket: int, group_id: str) -> int:
        """
        Поиск значения в базе: проверяется только если значение не найдено в кеше
        """
        pass

    def discover_seed_for_position_group(self, position_bucket: int, group_id: str) -> int:
        """
        Поиск значения в пространстве хешей: проверяется только если значения нет ни в кеше ни в базе
        Сохраняет найденное значение в базу чтобы в следующий раз не искать его заново
        """
        pass
