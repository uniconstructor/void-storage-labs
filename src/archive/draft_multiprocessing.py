#!/usr/bin/python

from concurrent.futures import process
import multiprocessing as mp
# from multiprocessing import Process, cpu_count, active_children, current_process, get_context
import time
from collections import OrderedDict, defaultdict, namedtuple, deque
# https://pyoxidizer.readthedocs.io/en/stable/pyoxidizer_packaging_static_linking.html
from tqdm import tqdm as tqdm_notebook
from typing import List, Dict, Set, Tuple, Optional, Union

from hash_space_utils import bytes_at_position
from custom_counter import CustomCounter as Counter

import os, sys, io, leb128, json

if not 'workbookDir' in globals():
    workbookDir = os.getcwd()
os.chdir(workbookDir)

# https://docs.mongoengine.org/
from mongoengine import *

# https://docs.mongoengine.org/guide/querying.html#advanced-queries
from mongoengine.queryset.visitor import Q

from a1_v2_item_values import create_value_id, ItemValueA1V2, \
    scan_seed_range, find_common_seeds, count_seed_range_score_for_data

JobData = namedtuple('JobData', [
    'job_id',
    'process_id',
    'start_seed',
    'end_seed',
    'target_positions',
    'target_values',
    'job_size',
    'total_jobs',
])

register_connection('default', db='blib', host='127.0.0.1', port=27017, maxPoolSize=300)

def run_job(q: mp.Queue, job_id: int, job_data: JobData):
    # each job uses it's own connection
    connection_name = f"connection_{job_id}"
    connect(db='blib', alias=connection_name, host='127.0.0.1', port=27017)
    
    # starting db filling
    total_progress = (job_id / job_data.total_jobs) * 100
    total_progress = round(total_progress, 4)
    print(f"[Start] job id={job_id}/{job_data.total_jobs} [{total_progress}%], process_id={job_data.process_id}, values: {job_data.target_values}\n")
    scan_result = scan_seed_range(
        job_data.start_seed, 
        job_data.end_seed, 
        job_data.target_positions, 
        job_data.target_values,
        job_data.process_id
    )
    print(f"[Finish] job id={job_id}/{job_data.total_jobs} [{total_progress}%], process_id={job_data.process_id}, values: {job_data.target_values}\n")
    print(f"[Finish] top_score: {scan_result.top_score}, top_seeds: {scan_result.seed_counts.most_common(8)}")
    
    # disconnecting after job is finished
    disconnect(alias=connection_name)
    # send result of the last scan
    return scan_result

def main(q: mp.Queue):
    print('Started main')
    max_active_cores = 12
    print(f"System has {mp.cpu_count()} cores available, process will use {max_active_cores} cores\n")

    #value_length = 2
    last_seed        = ItemValueA1V2.objects.get_last_seed()
    job_size         = 2**21
    total_jobs       = (2**32) // job_size
    target_positions = set(list(range(0, 256)))
    target_values    = [b'\x7fG', b'\x85\xfb', b'\x91\xf0', b'\xad\x14', b'\x01/']
    print(f"Last_seed: {last_seed}, job_size: {job_size}, total_jobs: {total_jobs}")

    # scedule jobs
    jobs     = list()
    end_seed = last_seed
    for job_id in range(0, 2): #2**11):
        start_seed = end_seed + 1
        end_seed   = start_seed + job_size
        process_id = job_id % max_active_cores
        job_data = JobData(
            job_id,
            process_id,
            start_seed,
            end_seed,
            target_positions,
            target_values,
            job_size,
            total_jobs,
        )
        jobs.append((job_id, job_data))
    
    processes    = dict()
    started_jobs = set()
    for job_id, job_data in jobs:
        job_process = mp.Process(target=run_job, args=(q, job_id, job_data))
        job_process.start()
        # register each started job
        processes[job_id] = job_process
        started_jobs.add(job_id)
        # limit parallel core usage
        if (len(started_jobs) >= max_active_cores):
            join_id = started_jobs.pop()
            print(f"[Work] waiting for job_id={join_id} to finish...")
            processes[join_id].join()

    while (len(started_jobs) > 0):
        join_id = started_jobs.pop()
        print(f"[Shutdown] joining job_id={join_id} to finish...")
        processes[join_id].join()
    
    print('Finished main')

if __name__ == '__main__':
    mp.set_start_method('spawn')
    q = mp.Queue()
    main(q)