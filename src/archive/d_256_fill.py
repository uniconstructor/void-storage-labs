
from rich import print
from rich.pretty import pprint
from b_256_utils import discover_seed_values
from mongoengine import register_connection
register_connection('default', db=f'256_byte_segments', host='127.0.0.1', port=27017, maxPoolSize=200)

max_parts = int(input("max_parts:"))
for i in range(0, max_parts):
  print(f"Starting task {i+1}/{max_parts}:")
  discover_seed_values()