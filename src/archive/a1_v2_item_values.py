# https://github.com/Textualize/rich
from rich import print, inspect
# https://rich.readthedocs.io/en/latest/pretty.html
from rich.pretty import pprint
import rich.repr
# https://github.com/tqdm/tqdm#nested-progress-bars
# https://pyoxidizer.readthedocs.io/en/stable/pyoxidizer_packaging_static_linking.html
from tqdm import tqdm as tqdm_notebook

# https://more-itertools.readthedocs.io/en/stable/api.html#more_itertools.windowed
from more_itertools import adjacent, map_reduce, mark_ends, pairwise, windowed, seekable, peekable, spy, \
    bucket, split_when, split_before, split_at, roundrobin, \
    consecutive_groups, run_length, first_true, islice_extended, first, last, rstrip, tail, \
    unique_everseen, locate, replace, time_limited
# https://bitstring.readthedocs.io/en/latest/index.html
from bitstring import Bits, BitArray, BitStream, ConstBitStream
from collections import OrderedDict, defaultdict, namedtuple, deque
from typing import List, Dict, Set, Tuple, Optional, Union, Iterable

import json
import os, sys, io, leb128

if not 'workbookDir' in globals():
    workbookDir = os.getcwd()
os.chdir(workbookDir)

# функции для работы с хеш-пространством
from hash_space_utils import HashItemAddress, HashItemPosition, \
    DEFAULT_VALUE_STEP, DEFAULT_POSITION_STEP, HASH_DIGEST_BITS, \
    split_data, count_segment_items, count_split_values, collect_split_positions, \
    get_min_bit_length, get_aligned_bit_length, get_aligned_byte_length, \
    bytes_at_position

from custom_counter import CustomCounter as Counter

# https://realpython.com/python-data-classes/#advanced-default-values
from dataclasses import dataclass
# https://docs.mongoengine.org/
from mongoengine import *

#connect(db='blib', host='127.0.0.1', port=27017, maxPoolSize=300)
# https://docs.mongoengine.org/guide/querying.html#advanced-queries
from mongoengine.queryset.visitor import Q

def create_value_id(seed: int, position: int) -> int:
    return ((seed * 256) + position)

def seed_from_id(id: int) -> int:
    return (id // 256)

def position_from_id(id: int) -> int:
    return (id % 256)

class ItemValueA1V2QuerySet(QuerySet):
    def get_last_seed(self) -> int:
        last_item = self.filter().order_by('-seed').first()
        if (last_item is None):
            return 0
        return last_item.seed

class ItemValueA1V2(Document):
    id       = LongField(min_value=0, max_value=((2 ** 40) - 1), primary_key=True)
    seed     = IntField(min_value=0, max_value=((2 ** 32) - 1))
    position = IntField(min_value=0, max_value=255)#, choices=set(range(0, 256)))
    value    = BinaryField(max_bytes=2)
    meta = {
        'queryset_class'    : ItemValueA1V2QuerySet,
        'auto_create_index' : True,
        'indexes' : [
            ('value', 'position', 'seed'),
            '-seed',
        ]
    }

def find_common_seeds(next_values: List[bytes], positions: Tuple[int]=(0, 255), seeds: Tuple[int]=(0, (2**32)-1)) -> Counter:
    seed_counts   = Counter()
    target_values = list(set(next_values.copy()))
    #value_count   = len(next_values)
    #unique_count  = len(target_values)
    min_position  = min(positions)
    max_position  = max(positions)
    min_seed      = min(seeds)
    max_seed      = max(seeds)
    
    # TODO: remove min seed and min position
    # check discovered seed range
    last_seed = ItemValueA1V2.objects.get_last_seed()
    if (last_seed < min_seed):
        return Counter()
    if (last_seed < max_seed):
        max_seed = last_seed

    # create conditions
    value_condition = None
    for target_value in target_values:
        target_condition = Q(
            value__in = target_values,
            position__lte = max_position,
        )
        if (value_condition is None):
            value_condition = target_condition
        else:
            value_condition = value_condition & target_condition

    #position_condition = Q(
    #    position__gte = min_position,
    #    position__lte = max_position,
    #)
    #seed_condition = Q(
    #    seed__gte = min_seed,
    #    seed__lte = max_seed,
    #)
    condition = value_condition #& position_condition #& seed_condition
    
    # load all related items from database
    db_items = ItemValueA1V2.objects(condition).order_by('seed').only('seed').item_frequencies('seed')
    
    # update seed counter
    if (len(db_items) > 0):
        seed_counts.update(db_items)
    
    # result - collected seeds
    return seed_counts

RangeScanResult = namedtuple('RangeScanResult', [
    'top_score',
    'start_seed',
    'end_seed',
    'values',
    'seed_counts',
    'located_counts',
])

def scan_seed_range(start_seed: int, end_seed: int, target_positions: Set[int], target_values: Set[bytes], 
        process_id: int, max_seed_score: int=None, value_length: int=2):
    # init progress indication
    seed_range    = range(start_seed, end_seed)
    seed_progress = tqdm_notebook(seed_range, mininterval=1, smoothing=0, position=process_id, dynamic_ncols=True) #), nrows=40
    # init counters
    total_located = 0
    total_pending = 0
    # number of occurances for each value across all seeds
    located_counts = Counter()
    # number of target values inside each seed
    seed_counts    = Counter()
    # update progress every 1%
    progress_update_fraction = ((end_seed - start_seed) // 100)
    if (end_seed - start_seed) <= 100:
        progress_update_fraction = 5
    if (max_seed_score is None):
        max_seed_score = len(target_positions)

    # starting seed scan
    pending_values      = list()
    item_collection     = ItemValueA1V2._get_collection()
    current_seed_values = set()
    current_seed_score  = 0
    top_seed_score      = 0
    for seed in seed_progress:
        # refresh current seed data
        current_seed_values.clear()
        current_seed_score = 0
        # scan target seed positions, read 2-byte value from each
        for position in target_positions:
            # get position value
            value = bytes_at_position(position=position*value_length*8, length=value_length*8, seed=seed, use_bytearray=False)
            # skip non-target values
            if (value in target_values):
                # init new database item
                new_item = ItemValueA1V2(
                    id       = create_value_id(seed, position),
                    seed     = seed,
                    position = position,
                    value    = value,
                )
                pending_values.append(new_item)
                total_pending += 1
                seed_counts.update({ seed: 1 })
                # not update counters again if this value occurs twise in this seed
                if (value in current_seed_values):
                    continue
                current_seed_values.add(value)
                # update counters
                total_located += 1
                located_counts.update({ value.hex(): 1 })
                current_seed_score += 1
        # update progress
        if ((seed % progress_update_fraction) == 0):
            total_progress = round(((seed / 2**32) * 100), 4)
            #seed_progress.set_description_str(f"seed: {seed}/{2**32} [{total_progress}%]")
            seed_progress.set_postfix({
                'q'  : f"{total_pending}",
                's'  : f"{seed_counts.most_common(1)} ({len(seed_counts)})",
                'v'  : f"{located_counts.most_common(1)}",
                'g'  : f"{seed}/{2**32} [{total_progress}%]",
            })
        # save new values (in batch)
        if (total_pending >= 256): #(seed % 20) == 0:
            # TODO: check save result
            QuerySet(ItemValueA1V2, collection=item_collection).insert(pending_values, load_bulk=False)
            # clear pending value queue
            pending_values.clear()
            # reset pending value counter
            total_pending = 0
        # update top score
        top_seed_score = max(current_seed_score, top_seed_score)
        # stop search if max_seed_score is set
        if (top_seed_score >= max_seed_score):
            break
        
    # save remaining values if any
    if (len(pending_values) > 0):
        # TODO: check save result
        QuerySet(ItemValueA1V2, collection=item_collection).insert(pending_values, load_bulk=False)

    # return progress info
    return RangeScanResult(
        top_seed_score,
        start_seed,
        end_seed,
        target_values,
        seed_counts,
        located_counts,
    )

@rich.repr.auto
@dataclass()
class DataSeed:
    # номер пространства хешей
    seed            : int
    # баллы полученные сид-пространством за наличие в нем нужных значений
    score           : int
    # значения данных, найденные в этом хеш-пространстве
    values          : Set[str]
    # позиции значений данных
    positions       : Set[int]
    # количество найденных значений
    values_count    : int
    # количество позиций (совпадает с количеством значений)
    positions_count : int
    # мэппинг (значение->позиция)
    value_positions : Dict[str, int]
    # обратный мэппинг (позиция->значение)
    position_values : Dict[int, str]
    min_values      : int
    # True если сканирование было прервано до полного подсчета всех значений (для ускорения перебора)
    break_triggered : bool

    def __init__(self, seed: int, score: int, values: Set[str], positions : Set[int],
            value_positions : Dict[str, int], position_values : Dict[int, str], 
            min_values: int, break_triggered: bool):
        self.seed            = seed          
        self.score           = score         
        self.values          = values
        self.positions       = positions
        self.values_count    = len(self.values)
        self.positions_count = len(self.positions)
        self.value_positions = value_positions
        self.position_values = position_values
        self.min_values      = min_values
        self.break_triggered = break_triggered

def count_seed_score_for_data(seed: int, target_values: Set[str], value_scores: Counter=None, 
        min_score: int=0, min_values: int=0,
        value_length: int=2, break_early: bool=True) -> DataSeed:
    """
    Count score of a given seed for a given set of target values at available positions
    """
    score            = 0
    max_positions    = (256 ** (value_length - 1))
    target_positions = range(0, max_positions)
    values           = set()
    positions        = set()
    value_positions  = dict()
    position_values  = dict()
    # counters for current scan status
    scanned_count    = 0
    remaining_count  = 0
    values_count     = 0
    break_triggered  = False
    
    if (min_score > 0) and (min_values == 0):
        min_values = 1
    if (min_values == 0):
        break_early = False
    
    # scan all seed positions
    for position in target_positions:
        value = bytes_at_position(position=position*value_length*8, length=value_length*8, seed=seed, use_bytearray=False).hex()
        if break_early:
            # early break: stop scan remaining positions if minimum values cannot be reached
            scanned_count   += 1
            remaining_count -= 1
            if ((min_values - values_count) > remaining_count):
                break_triggered = True
                break
        if ((value in target_values) is False):
            # seed value is not from the target list 
            continue
        if value in values:
            # this value is not unique (already persists on a previous position)
            continue
        # update counters
        score        = score + value_scores[value]
        values_count = values_count + 1
        # update collected values/positions
        values.add(value)
        positions.add(value)
        # update mapping
        value_positions[value] = position
        position_values[position] = value
    
    return DataSeed(
        seed,
        score,
        values,
        positions,
        value_positions,
        position_values,
        min_values,
        break_triggered
    )

def init_seed_scores(seeds: range, init_score: int=0) -> Counter:
    seed_scores = Counter()
    progress    = tqdm_notebook(seeds, smoothing=0)
    for seed in progress:
        seed_scores.update({ seed: init_score })
    progress.close()
    return seed_scores

@dataclass()
class DataSeedRange:
    seed_scores  : Counter
    value_scores : Counter
    best_seed    : DataSeed

    def __init__(self, seed_scores: Counter, value_scores: Counter, best_seed: DataSeed):
        self.seed_scores  = seed_scores
        self.value_scores = value_scores
        self.best_seed    = best_seed

def count_seed_range_score_for_data(seed_scores: Counter, value_scores: Counter, process_id: int=0, value_length: int=2) -> DataSeedRange:
    score_counts    = Counter()
    target_values   = set(value_scores.keys())
    total_values    = len(target_values)
    total_seeds     = len(seed_scores)
    # best score for current values
    best_score      = 0
    start_score     = start_score
    # best score for previous value set
    # top_score       = seed_scores.most_common(1)[0][1]
    # create score dict containing "1" for every value (to measure number of values without their frequency)
    default_scores  = dict.fromkeys(target_values, 1)
    zero_seeds      = set()
    # choosing best seed to start
    for seed_id in range(0, total_seeds):
        start_seed  = seed_scores.most_common(seed_id+1)[seed_id][0]
        best_seed   = count_seed_score_for_data(
            seed=start_seed, 
            target_values=target_values, 
            value_scores=default_scores, 
            value_length=value_length
        ) #  value_scores=value_scores,
        best_score  = best_seed.score
        start_score = best_score
        if (best_score > 0):
            break
        else:
            zero_seeds.add(start_seed)
    # remove located empty seeds if any
    if (len(zero_seeds) > 0):
        for zero_seed in zero_seeds:
            seed_scores[zero_seed] = 0
            seed_scores.update({ zero_seed: 0 })
            score_counts.update({ 0: 1 })
    # iterate only best possible scores or over fresh generated seeds (with score=0)
    best_seeds = list(seed_scores.most_common_above(best_score).keys())
    if (len(best_seeds) == 0):
        best_seeds = list(seed_scores.top_items().keys())
    # adjist update frequency
    update_fraction = (len(best_seeds) // 20000) + 1
    if (update_fraction <= 20001):
        update_fraction = 20001
    
    # init progress display
    seed_progress = tqdm_notebook(best_seeds, mininterval=1, smoothing=0, position=process_id, dynamic_ncols=True)

    seed_number = 0
    for seed in seed_progress:
        data_seed  = count_seed_score_for_data(
            seed=seed, 
            target_values=target_values, 
            value_scores=default_scores, 
            value_length=value_length
        ) #  value_scores=value_scores,
        seed_score = data_seed.score
        # update counts
        seed_scores[seed] = 0
        seed_scores.update({ seed: seed_score })
        score_counts.update({ seed_score: 1 })
        # update best score
        if (seed_score > best_score):
            best_score = seed_score
            best_seed  = data_seed
        # update progress
        if ((seed_number % update_fraction) == 0):
            #top_scores    = score_counts.max_item_counts(4) # seed_scores.agregated_counts().most_common(4) # all_scores[-4:]
            #bottom_scores = score_counts.min_item_counts(4) # seed_scores.agregated_counts().least_common(4) # all_scores[:4]
            #top_score     = seed_scores.most_common(1)[0][1]
            seed_progress.set_postfix({
                'z'    : f"{score_counts[0]}",
                #'t/b'  : f"{top_scores}/{bottom_scores}, {top_score}/{best_score}",
                's'    : f"{seed_scores.most_common(4)}", #[{len(seed_scores)}] 
                'tv/ts': f"{total_values}/{total_seeds}",
                'st'   : f"{start_score}",
                'mu'   : f"{score_counts.max_counts(16)}", 
                'lu'   : f"{score_counts.min_counts(4)}",
            })
        # update scanning seed number
        seed_number += 1
   
    # finish progress display
    #top_scores    = score_counts.max_item_counts(4)
    #bottom_scores = score_counts.min_item_counts(4)
    #top_score     = seed_scores.most_common(1)[0][1]
    seed_progress.set_postfix({
        'z'    : f"{score_counts[0]}",
        #'t/b'  : f"{top_scores}/{bottom_scores}, {top_score}/{best_score}",
        's'    : f"{seed_scores.most_common(5)}", #[{len(seed_scores)}] 
        'tv/ts': f"{total_values}/{total_seeds}",
        'st'   : f"{start_score}",
        'mu'   : f"{score_counts.max_counts(16)}", 
        'lu'   : f"{score_counts.min_counts(4)}",
    })
    seed_progress.close()

    return DataSeedRange(
        seed_scores,
        value_scores,
        best_seed,
    )

SeedRangeDataMapping = namedtuple('SeedRangeDataMapping', [
    'best_seeds',
    'seed_scores',
    'value_scores',
    'range_scores',
    'best_seed_scores',
    'seed_value_counts',
    'seed_positions',
    'position_seeds',
    'total_items',
    'value_length',
])

def map_data_to_seed_range(seed_scores: Counter, value_scores: Counter, data_values: Dict[int, str], value_length: int=2) -> SeedRangeDataMapping:
    best_seeds        = list()
    best_seed_scores  = Counter()
    seed_value_counts = Counter()
    inital_values     = len(value_scores)
    inital_seeds      = len(seed_scores)
    total_items       = len(data_values)
    # value mapping
    value_positions      = defaultdict(list)
    seed_value_positions = defaultdict(dict)
    
    # collect positions used by each value
    for item_position, item_value in data_values.items():
        value_positions[item_value].append(item_position)
    # positions used by each seed
    seed_positions    = defaultdict(list)
    position_seeds    = dict()
    
    # allocate values to seeds
    while (len(value_scores) > 0):
        if (len(seed_scores) == 0):
            raise Exception(f"No seeds left")
        
        # find best seeds
        range_scores = count_seed_range_score_for_data(seed_scores, value_scores, process_id=0, value_length=value_length)
        seed_scores  = range_scores.seed_scores
        value_scores = range_scores.value_scores
        best_seed    = range_scores.best_seed
        
        # precess seed values
        best_seed_score = 0
        for value in best_seed.values:
            # get value score (based on frequency)
            value_score     = value_scores[value]
            # update total seed score
            best_seed_score = best_seed_score + value_score
            # update total number of values used by seed
            seed_value_counts.update({ best_seed.seed: 1 })
            # update seed value mapping
            if len(value_positions[value]) == 0:
                raise Exception(f"No positions for value: {value}")
            for value_position in value_positions[value]:
                # save seed mapping
                seed_positions[best_seed.seed].append(value_position)
                seed_value_positions[best_seed.seed][value_position] = value
                # set global positions of current seed
                position_seeds[value_position] = best_seed.seed
            # remove allocated values
            del value_scores[value]
        
        # remove seeds that doesn't contain values 
        zero_seeds = seed_scores.least_common_below(0)
        for seed, seed_score in zero_seeds.items():
            if (seed_score == 0):
                del seed_scores[seed]
        
        # save allocation result
        best_seeds.append(best_seed)
        best_seed_scores.update({ best_seed.seed: best_seed_score })
        print(f"[{len(best_seeds)}] best_seed: {best_seed.seed} ({best_seed_score}/{seed_value_counts[best_seed.seed]}), values: {len(value_scores)}/{inital_values}, seeds: {len(seed_scores)}/{inital_seeds}")
    
    return SeedRangeDataMapping(
        best_seeds        = best_seeds,
        seed_scores       = seed_scores,
        value_scores      = value_scores,
        range_scores      = range_scores,
        best_seed_scores  = best_seed_scores,
        seed_value_counts = seed_value_counts,
        seed_positions    = seed_positions,
        position_seeds    = position_seeds,
        total_items       = total_items,
        value_length      = value_length,
    )

@dataclass()
class TargetTier:
    min_height  : int
    max_height  : int
    items       : List[int]
    item_counts : Counter
    total_items : int
    total_sum   : int

    def __init__(self, min_height: int, max_height: int, items: List[int], item_counts: Counter, total_items: int, total_sum: int):
        self.min_height  = min_height 
        self.max_height  = max_height
        self.items       = items
        self.item_counts = item_counts
        self.total_items = total_items
        self.total_sum   = total_sum

    def __rich_repr__(self) -> rich.repr.Result:
        yield "min_height", self.min_height
        yield "max_height", self.max_height
        yield "items[:8] ", self.items[:8], []
        yield "items[-8:]", self.items[-8:], []
        yield "item_counts[:8] ", self.item_counts.max_counts(8), Counter()
        yield "item_counts[-8:]", self.item_counts.min_counts(8), Counter()
        yield "total_items", self.total_items, 0
        yield "total_sum", self.total_sum, 0

def create_target_tier(min_height: int=1, max_height: int=256) -> TargetTier:
    items       = list()
    total_sum   = 0
    total_items = 0
    row         = list()
    item_counts = Counter()
    
    max_height  = min_height + max_height
    if max_height > 257:
        max_height = 257
  
    for height in range(min_height, max_height):
        row_sum = 0
        row.clear()
        min_item = min_height
        max_item = min_height + height
        if (max_item > 257):
            max_item = 257
        for row_item in range(min_item, max_item):
            # do not add items shorter than minimum
            if (row_item < min_item):
              continue
            row.append(row_item)
            item_counts.update({ row_item: 1 })
            items.append(row_item)
            total_items += 1
        row_sum   = sum(row)
        total_sum = total_sum + row_sum
    
    return TargetTier(
        min(items),
        max(items),
        items,
        item_counts,
        total_items,
        total_sum
    )

def get_minimal_tier(total_values: int, min_height: int=1):
    tier = None
    for height in range(min_height, 257):
        tier = create_target_tier(min_height, max_height=height)
        if (tier.total_sum >= total_values):
            return tier
    return tier

def fill_target_tier(total_values: int, min_height=1) -> TargetTier:
    input_tier       = get_minimal_tier(total_values=total_values, min_height=min_height)
    remaining_values = total_values
    items            = list()
    item_counts      = Counter()
    
    for item in input_tier.items:
        current_item = 0
        for i in range(0, item):
            remaining_values = remaining_values - 1
            current_item     = current_item + 1
            if (remaining_values == 0):
                break
        if (current_item > 0):
            items.append(current_item)
            item_counts.update({ current_item: 1 })
        if (remaining_values == 0):
            break
    
    return TargetTier(
        min(items),
        max(items),
        items,
        item_counts,
        len(items),
        total_values
    )
        
def map_data_to_seed_ranges(range_size: int, value_scores: Counter, data_values: Dict[int, str], value_length: int=2):
    seed_scores = Counter()
    start_seed  = 0
    while (len(value_scores) > 0):
        end_seed         = start_seed + range_size
        seed_range       = range(start_seed, end_seed)
        new_seed_scores  = init_seed_scores(seed_range)
        new_seed_mapping = map_data_to_seed_range(
            seed_scores=new_seed_scores, 
            value_scores=value_scores, 
            data_values=data_values, 
            value_length=value_length
        )
    pass

