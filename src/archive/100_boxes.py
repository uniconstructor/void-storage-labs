from __future__ import annotations
from rich import print
from rich.pretty import pprint
from rich.text import Text
from rich.progress import track,\
    BarColumn, Progress, Task, TaskID, TextColumn, TimeElapsedColumn, TimeRemainingColumn,\
    MofNCompleteColumn, RenderableColumn, SpinnerColumn, TransferSpeedColumn, FileSizeColumn, ProgressColumn
from rich.layout import Layout
from rich.columns import Columns
from rich.text import Text
from custom_rich import CustomTaskProgressColumn as TaskProgressColumn
from custom_counter import CustomCounter as Counter
from collections import defaultdict, namedtuple
from bitarray import bitarray, frozenbitarray
from bitarray.util import ba2int, int2ba, canonical_huffman, huffman_code,\
    vl_encode, vl_decode, sc_encode, sc_decode, serialize
from sortedcontainers import SortedSet, SortedDict, SortedList, SortedKeyList, SortedListWithKey,\
    SortedKeysView, SortedValuesView, SortedItemsView
from typing import List, Set, Dict, Tuple, Optional, Union, Iterable
from dataclasses import dataclass, field
from enum import Enum, IntEnum
from copy import deepcopy, copy
import operator
import graphviz
from lolviz import *
from delta_of_delta import delta_encode, delta_decode
from itertools import accumulate
from more_itertools import nth, countable

from hash_range_iterator import DEFAULT_ENDIAN, HashPositionBitmap, \
    int_from_nounce, int_bits_from_nounce, last_int_bits_from_nounce,\
    last_ba_bits_from_nounce, last_ba_bits_from_digest, last_fba_bits_from_digest, last_int_bits_from_digest,\
    split_data, count_data_items, create_value_bitmap, collect_missing_positions, delta_to_list, \
    get_target_position_bitmap, encode_position_bitmap, decode_position_bitmap
from cycle_gen import CMWC
from _256_byte_prototype import ValuePath, DataBlock, PermutationBlock, EncodedItemSequence, EncodedOrderedSet,\
    HashPositionBitmap, IsolatedCycle, SequenceItem, encode_item_order, find_block_position, collect_best_seed_block_cycles,\
    collect_extra_block_seeds,\
    BlockBytes, DistanceMapping, EncodedNumberCounter, CycleType, SeedTargetType

file_name = './data/AMillionRandomDigits.bin'
#file_name = f"./data/image-144kb.jpg"
data = bitarray(endian=DEFAULT_ENDIAN)
file = open(file=file_name, mode='rb')
data.fromfile(file)

data        = frozenbitarray(data) # first n bits
data_length = len(data)

print(f"file: {file_name},\nsize: {len(data):,} bits,\n      {(len(data) // 8):,} bytes,\n      {len(data) // 8 // 1024} Kb")

file_bytes   = []
value_counts = Counter()
for i in range(256*1, 256*2):
    file_byte = ba2int(data[i*8:i*8+8], signed=False)
    file_bytes.append(file_byte)
    value_counts.update({ file_byte: 1 })

print(f"vc={value_counts.first_items(32)}, l={len(value_counts)}")
pprint(value_counts.aggregated_counts().first_items())

block_bytes = BlockBytes(items=file_bytes)

#block_cycles = collect_best_seed_block_cycles(block_bytes=block_bytes, target_type=SeedTargetType.MAX_NEW_ITEMS, min_seed=1, max_seed=2**16)
block_cycles = collect_extra_block_seeds(block_bytes=block_bytes, max_items=128, min_seed=1, max_seed=2**10)
pprint(block_cycles)