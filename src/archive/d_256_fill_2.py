
from rich import print
from rich.pretty import pprint
from b_256_utils import discover_seed_values
from mongoengine import register_connection
register_connection('default', db=f'256_byte_segments', host='127.0.0.1', port=27017, maxPoolSize=200)

target_part = int(input("target_part:"))
extra_parts = int(input("extra_parts:"))

print(f"Scanning target part {target_part}:")
discover_seed_values(part_id=target_part)

for i in range(0, extra_parts):
  print(f"Scanning extra part {i+1}/{extra_parts}:")
  discover_seed_values()