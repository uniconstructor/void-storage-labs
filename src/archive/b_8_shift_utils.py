# https://stackoverflow.com/questions/38340808/recursive-typing-in-python-3-5
from __future__ import annotations
from rich import print
from rich.pretty import pprint
from custom_counter import CustomCounter as Counter
from tqdm import tqdm
from bitarray.util import ba2int, int2ba
from bitarray import bitarray, frozenbitarray
from collections import defaultdict, namedtuple
import os, math

#if not 'workbookDir' in globals():
#    workbookDir = os.getcwd()
#os.chdir(workbookDir)

from hash_range_iterator import HASH_DIGEST_BITS, bits_at_position, int_bytes_from_digest, int_from_nounce, bytes_from_nounce
#from hash_space_utils import get_min_bit_length
from typing import List, Set, Dict, Tuple, Optional, Union, Iterable
# https://realpython.com/lru-cache-python/
#from functools import lru_cache
#from dataclasses import dataclass, field
# https://docs.python.org/3/library/enum.html
#from enum import Enum, IntEnum
#from copy import copy, deepcopy
#import json

#from mongoengine import register_connection
#register_connection('default', db=f'8_byte_segments', host='127.0.0.1', port=27017, maxPoolSize=200)

PREFIX_SIGN_BITS   : int = 0
VALUE_TYPE_BITS    : int = 0
NOUNCE_LENGTH_BITS : int = 6

MIN_NOUNCE_BITS    : int = 0 #8
MAX_NOUNCE_BITS    : int = MIN_NOUNCE_BITS + (2 ** NOUNCE_LENGTH_BITS) - 1

MIN_PREFIX_BITS    : int = PREFIX_SIGN_BITS + VALUE_TYPE_BITS + NOUNCE_LENGTH_BITS

NounceRange = namedtuple('NounceRange', [
    'prefix_bits',
    'nounce_bits',
    'value_bits',
    'shift_bits',
    'start',
    'end',
    'total',
])

def get_nounce_ranges() -> List[NounceRange]:
    ranges       = []
    start_nounce = 0
    for nounce_bits in range(MIN_NOUNCE_BITS, MAX_NOUNCE_BITS):
        end_nounce    = start_nounce + (2 ** nounce_bits)
        prefix_bits   = MIN_PREFIX_BITS + nounce_bits
        value_bits    = prefix_bits
        shift_bits    = HASH_DIGEST_BITS - value_bits
        total_nounces = end_nounce - start_nounce
        ranges.append(NounceRange(
            prefix_bits = prefix_bits,
            nounce_bits = nounce_bits,
            value_bits  = value_bits,
            shift_bits  = shift_bits,
            start       = start_nounce,
            end         = end_nounce,
            total       = total_nounces,
        ))
        start_nounce = end_nounce
    return ranges

TargetNounce = namedtuple('TargetNounce', [
    'value',
    'prefix_bits',
    'nounce_bits',
    'value_bits',
    'shift_bits',
    'extended',
])

def find_target_nounces(data: frozenbitarray, start_position: int, nounce_range: NounceRange) -> List[TargetNounce]:
    target_nounces   = []
    end_position     = start_position + 64
    
    target_value     = data[start_position:end_position]
    target_divider   = 2**nounce_range.value_bits
    target_number    = ba2int(target_value, signed=False) % target_divider

    extended_value   = data[start_position:end_position]
    extended_divider = 2**(nounce_range.value_bits+1)
    extended_target  = ba2int(extended_value, signed=False) % extended_divider
    
    #pprint(nounce_range) 
    #print(f"{nounce_range.start}-{nounce_range.end}: {nounce_range.total} total, nounce_bits={nounce_range.nounce_bits}, value_bits={nounce_range.value_bits}")
    #print(f"target_value: {target_value[0:nounce_range.value_bits]}, extended_value: {extended_value[0:nounce_range.value_bits+1]}, target_number={target_number}, extended_target={extended_target}")
    
    progress = tqdm(range(nounce_range.start, nounce_range.end), mininterval=0.5, smoothing=0)
    progress.set_description_str(f"({nounce_range.nounce_bits} + {MIN_PREFIX_BITS} -> {nounce_range.value_bits} bits): target_number={target_number}")
    progress.set_postfix_str(f"{nounce_range.start} - {nounce_range.start}")
    
    for nounce in progress:
        digest      = int_from_nounce(nounce=nounce)
        hash_number = digest % target_divider #digest >> nounce_range.shift_bits
        if (hash_number == target_number):
            extended_hash_number = digest % extended_divider #digest >> (nounce_range.shift_bits - 1)
            if (extended_target == extended_hash_number):
                print(f"  extended_hash={extended_hash_number}")
                print(f"extended_number={extended_target}")
                return [TargetNounce(
                    value       = extended_hash_number,
                    prefix_bits = nounce_range.prefix_bits,
                    nounce_bits = nounce_range.nounce_bits,
                    value_bits  = (nounce_range.value_bits + 1),
                    shift_bits  = (nounce_range.shift_bits - 1),
                    extended    = True,
                )]
            print(f"  hash_number={hash_number}")
            print(f"target_number={target_number}")
            target_nounces.append(TargetNounce(
                value       = extended_hash_number,
                prefix_bits = nounce_range.prefix_bits,
                nounce_bits = nounce_range.nounce_bits,
                value_bits  = nounce_range.value_bits,
                shift_bits  = nounce_range.shift_bits,
                extended    = False,
            ))
    return target_nounces

