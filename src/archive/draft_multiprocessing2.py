# https://github.com/Textualize/rich
from rich import print, inspect
# https://rich.readthedocs.io/en/latest/pretty.html
from rich.pretty import pprint
import rich.repr
# https://rich.readthedocs.io/en/latest/appendix/box.html
from rich import box
from rich.align import Align
# https://rich.readthedocs.io/en/latest/console.html
from rich.console import Console, Group
from rich.layout import Layout
from rich.panel import Panel
from rich.progress import Progress, SpinnerColumn, BarColumn, TextColumn, track
from rich.syntax import Syntax
from rich.table import Table as Table
from rich.text import Text as Text
# https://rich.readthedocs.io/en/latest/prompt.html
from rich.prompt import Prompt, IntPrompt
from rich.live import Live

# https://pyoxidizer.readthedocs.io/en/stable/pyoxidizer_packaging_static_linking.html
#from tqdm import tqdm as tqdm_notebook

# https://realpython.com/python-data-classes/#advanced-default-values
from dataclasses import dataclass, field

# https://more-itertools.readthedocs.io/en/stable/api.html#more_itertools.windowed
from more_itertools import adjacent, map_reduce, mark_ends, pairwise, windowed, seekable, peekable, spy, \
    bucket, split_when, split_before, split_at, \
    consecutive_groups, run_length, first_true, islice_extended,\
    unique_everseen, locate, replace, time_limited
# https://bitstring.readthedocs.io/en/latest/index.html
from bitstring import Bits, BitArray, BitStream, ConstBitStream
from collections import OrderedDict, defaultdict, namedtuple, deque
from typing import List, Dict, Set, Tuple, Optional, Union

import os, sys, io, leb128, json

if not 'workbookDir' in globals():
    workbookDir = os.getcwd()
os.chdir(workbookDir)

from hash_space_utils import HashItemAddress, HashSegmentAddress, DataUnit, \
    count_split_values, collect_split_values, \
    get_min_bit_length, get_aligned_bit_length, bytes_at_position, hex_at_position, normalize_length_value

from partition_utils import SeedData, ScoreLayerMapping, PartitionItem, DataPartitions, \
  define_min_partitions

from custom_counter import CustomCounter as Counter

from a1_v2_item_values import ItemValueA1V2, \
    find_common_seeds, seed_from_id, position_from_id, create_value_id, count_seed_score_for_data, \
    count_seed_range_score_for_data, init_seed_scores, map_data_to_seed_range

from sharded_storage_utils import get_seed_value_class, get_value_instance_class

from mongoengine import register_connection, connect
#connect(db='blib', host='127.0.0.1', port=27017, maxPoolSize=300)
register_connection('default', db='blib', host='127.0.0.1', port=27017, maxPoolSize=300)

# https://docs.python.org/dev/library/concurrent.futures.html
import concurrent.futures
# https://docs.python.org/dev/library/multiprocessing.html#module-multiprocessing
# https://docs.python.org/dev/library/queue.html#
import multiprocessing as mp

@dataclass()
class ScanTaskResult:
    task_id          : int
    seed             : int
    target_positions : range
    target_values    : Set[str]
    positions        : List[int]
    values           : Set[str]
    value_counts     : Counter
    value_positions  : Dict[str, Set[int]]
    position_values  : Dict[int, str] = field(init=dict)

def scan_value_positions_in_seed(task_id: int, seed: int, min_position: int, max_position: int, target_values: Set[str], value_length: int, 
        position_step: int=1, process_id: int=0) -> ScanTaskResult:
    value_count      = 0
    position_count   = 0
    value_counts     = Counter()
    target_positions = range(min_position, max_position, position_step)
    values           = set()
    positions        = set()
    value_positions  = defaultdict(set)
    position_values  = dict()
    
    # perform scan
    for position in target_positions:
        value          = hex_at_position(position=position*8, length=value_length*8, seed=seed)
        position_count = position_count + 1
        if (value in target_values):
            # value/position items
            values.add(value)
            positions.add(position)
            # update mapping
            value_positions[value].add(position)
            position_values[position] = value
            # update counters
            value_counts.update({ value: 1 })
            value_count = value_count + 1
    
    final_frequency = ((len(target_positions) * len(target_values)) // (value_count + 1))
    print(f"task_id={task_id}, total: {value_count} ({final_frequency} positions per value), {len(target_positions)} values scanned")
    
    return ScanTaskResult(
        task_id          = task_id,
        seed             = seed,
        target_positions = target_positions,
        target_values    = target_values,
        positions        = positions,
        values           = values,
        value_counts     = value_counts,
        value_positions  = value_positions,
        position_values  = position_values,
    )

@dataclass()
class ScanTask:
    """
    Parameters for scanning part of the hash space
    """
    task_id         : int
    seed            : int
    min_position    : int
    max_position    : int
    value_length    : int
    target_values   : Set[str] = field(init=set)
    position_step   : int = field(default=1)
    process_id      : int = field(default=0)

    def as_tuple(self) -> tuple:
        return (
            self.task_id, 
            self.seed,
            self.min_position, 
            self.max_position, 
            self.target_values, 
            self.value_length, 
            self.position_step, 
            self.process_id,
        )

def create_scan_task(task_number: int, seed: int, start_position: int, positions_per_task: int, target_values: Set[str], value_length: int, 
        max_workers: int, position_step: int=1) -> ScanTask:
    
    min_position = start_position + task_number * positions_per_task 
    max_position = min_position + positions_per_task 
    process_id   = task_number % max_workers
    
    return ScanTask(
        task_id         = task_number,
        seed            = seed,
        min_position    = min_position,
        max_position    = max_position,
        target_values   = target_values,
        value_length    = value_length,
        position_step   = position_step,
        process_id      = process_id,
    )

def init_scan_tasks(seed: int, start_position: int, total_tasks: int, positions_per_task: int, target_values: Set['str'],
        value_length: int, max_workers: int, position_step: int) -> mp.Queue:
    tasks = mp.Queue()
    for task_number in range(0, total_tasks):
        task = create_scan_task(
            task_number        = task_number,
            seed               = seed,
            start_position     = start_position,
            positions_per_task = positions_per_task,
            target_values      = target_values,
            value_length       = value_length,
            max_workers        = max_workers,
            position_step      = position_step,
        )
        tasks.put(task)
    return tasks

#def submit_scan_task(tasks: mp.Queue, executor: concurrent.futures.Executor, total_tasks: int, task_pipe: mp.Pipe) -> concurrent.futures.Future:
def submit_scan_task(tasks: mp.Queue, executor: concurrent.futures.Executor, total_tasks: int) -> concurrent.futures.Future:
    task          = tasks.get()
    task_args     = task.as_tuple()
    future_result = executor.submit(scan_value_positions_in_seed, *task_args)
    print(f"task_id={task_args[0]}({task_args[0]+1}/{total_tasks}) - submitted with task_args: {task_args}")
    return future_result

def main():
    value_length       = 2 
    seed               = 0
    target_values      = set(['0000'])
    #total_positions    = (2**20) * 16 # 2**32
    total_tasks        = 2**8
    start_position     = 0
    position_step      = 1 
    positions_per_task = 2**24 #total_positions // total_tasks
    max_workers        = mp.cpu_count()
    #event_queue        = mp.Queue()
    
    results  = list()
    #ctx = mp.get_context()
    progress = Progress(refresh_per_second=1)
    total_task_id = progress.add_task(f"Total:", total=total_tasks)
    
    with Live(progress, refresh_per_second=1) as live:
        with concurrent.futures.ProcessPoolExecutor(max_workers=max_workers) as executor:
            # submit inital tasks
            for task_number in range(0, total_tasks):
                #process_id = progress.add_task(f"Task #{task_number}...", total=positions_per_task)
                #out_conn, in_conn = mp.Pipe()
                task = create_scan_task(
                    task_number        = task_number,
                    seed               = seed,
                    start_position     = start_position,
                    positions_per_task = positions_per_task,
                    target_values      = target_values,
                    value_length       = value_length,
                    max_workers        = max_workers,
                    position_step      = position_step,
                )
                task_args     = task.as_tuple()
                future_result = executor.submit(scan_value_positions_in_seed, *task_args)
                progress.console.print(f"task_id={task_number}({task_number+1}/{total_tasks}): submitted with task_args: {task_args}")
                results.append(future_result)
            # process completed tasks
            for future in concurrent.futures.as_completed(results):
                task_result = future.result()
                progress.advance(total_task_id)
                progress.console.print(f"task_id={task_result.task_id}({task_result.task_id+1}/{total_tasks}): done with task_result: {task_result.value_counts}")
                #pprint(task_result, max_length=10)

if __name__ == '__main__':
    main()