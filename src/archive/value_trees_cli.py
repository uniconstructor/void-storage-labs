from __future__ import annotations
from rich import print
from rich.pretty import pprint
from custom_counter import CustomCounter as Counter
from collections import defaultdict, namedtuple
from bitarray import bitarray, frozenbitarray
from bitarray.util import ba2int, int2ba, canonical_huffman, huffman_code
#from sortedcontainers import SortedSet, SortedDict, SortedList, SortedKeyList, SortedListWithKey,\
#    SortedKeysView, SortedValuesView, SortedItemsView
#from typing import List, Set, Dict, Tuple, Optional, Union, Iterable
#from dataclasses import dataclass, field
#from enum import Enum, IntEnum
#from copy import deepcopy, copy
#
#from hash_range_iterator import int_from_nounce, int_bits_from_nounce, last_int_bits_from_nounce,\
#    last_ba_bits_from_nounce, last_ba_bits_from_digest, last_fba_bits_from_digest, last_int_bits_from_digest
from varint_tree_encoder import DEFAULT_ITEM_SCORE, HashId, HashIdType, InitHashItem, SavedHashItem, \
    ConsumableVarintSet, VarintNumberEncoder, HashItemTree

file_name = './data/AMillionRandomDigits.bin'
#file_name = f"./data/image-144kb.jpg"
test_data = bitarray(endian='little')
test_file = open(file=file_name, mode='rb')
test_data.fromfile(test_file)

test_data   = frozenbitarray(test_data) # first n bit14s
data_length = len(test_data)

print(f"file_name: {file_name}, size: {len(test_data)} bits, ({len(test_data) // 8} bytes)")

encoder_input = test_data.copy()
print(f"input:   {encoder_input[0:256]} ({len(encoder_input)})")

encoder_tree  = HashItemTree(nounce_id_prefix_length=4, seed_id_prefix_length=0)
encoded_data  = encoder_tree.encode_data(data=encoder_input)

print(f"input:   {encoder_input[0:256]} ({len(encoder_input)})")
print(f"encoded: {encoded_data[0:256]} ({len(encoded_data)})")  