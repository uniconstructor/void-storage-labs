# https://stackoverflow.com/questions/38340808/recursive-typing-in-python-3-5
from __future__ import annotations
# https://github.com/Textualize/rich
from rich import print
# https://rich.readthedocs.io/en/latest/pretty.html
from rich.pretty import pprint
from custom_counter import CustomCounter as Counter
from tqdm import tqdm
from bitarray.util import ba2int, int2ba, huffman_code
from bitarray import bitarray, frozenbitarray
from collections import defaultdict, namedtuple
import math

from hash_range_iterator import bits_at_position, int_bytes_from_digest, int_bytes_from_nounce, bytes_from_nounce, int_from_nounce,\
  HASH_DIGEST_BITS, HASH_DIGEST_BYTES
from hash_space_utils import get_min_bit_length
from typing import List, Set, Dict, Tuple, Optional, Union, Iterable
# https://realpython.com/lru-cache-python/
from functools import lru_cache
from dataclasses import dataclass, field
# https://docs.python.org/3/library/enum.html
from enum import Enum, IntEnum
from copy import copy, deepcopy

#from mongoengine import register_connection
#sregister_connection('default', db=f'256_byte_segments', host='127.0.0.1', port=27017, maxPoolSize=300)
from mongoengine import Document, QuerySet, LongField, IntField, \
    BinaryField, StringField, ListField, SortedListField, BooleanField, queryset_manager

# размер стандартного сегмента данных в байтах (всегда общий как для сегмента данных так и для сегмента хеш-пространства)
HASH_SEGMENT_SIZE_BYTES : int = 256
# размер стандартного сегмента данных в байтах
HASH_SEGMENT_SIZE_BITS  : int = HASH_SEGMENT_SIZE_BYTES * 8

# размер хеш-блока сегмента в байтах
HASH_CHUNK_SIZE_BYTES   : int = HASH_DIGEST_BYTES
# размер хеш-блока сегмента в битах
HASH_CHUNK_SIZE_BITS    : int = HASH_CHUNK_SIZE_BYTES * 8

# количество хеш-блоков в сегменте (32 для 8-байтного/64-битного хеша и 256-байтного/2048-битного сегмента)
MAX_CHUNKS_IN_SEGMENT   : int = HASH_SEGMENT_SIZE_BITS // HASH_CHUNK_SIZE_BITS

# максимально доступный для сканирования сегмент хеш-пространства 
# (вообще может быть бесконечно большое число, но мы пока возьмем 56 бит а там посмотрим)
MAX_HASH_SEGMENT_ID     : int = 2**56
# размер одного шарда (сканируемого непрерывного блока) хеш-пространства: указывается как количество сегментов
HASH_SHARD_SIZE         : int = 2**24
# максимальное количество шардов, сканируемых в 1 поток (64 by default)
MAX_SEGMENT_SHARDS      : int = 64 #MAX_HASH_SEGMENT_ID // HASH_SHARD_SIZE
# максимальное количество записей сохраяемых в базу одним запросом
SEGMENT_SAVE_BATCH_SIZE : int = 1024
# максимальное количество шардов для 1 процесса по умолчанию (из расчета 30 минут на 1 шард в однопоточном режиме)
DEFAULT_MAX_SHARDS      : int = 16

# TODO: deprecated, remove
class SegmentMatches(Document):
    # hash segment id
    id              = LongField(primary_key=True)
    data_segment_id = IntField(null=False, required=True)
    item_count      = IntField(null=False, required=True)
    positions       = SortedListField(IntField(), null=False, required=True)
    values          = ListField(IntField(), null=False, required=True)
    # collection metadata
    meta = {
        'indexes': [
          'data_segment_id',
          'item_count',
          'positions',
          'values',
        ],
    }

### BASE CLASSES ###

class BaseSegmentMatches(Document):
    """
    Список совпадений для 256 позиций
    """
    # hash segment id
    id              = LongField(primary_key=True)
    #data_segment_id = IntField(null=False, required=True)
    item_count      = IntField(null=False, required=True)
    positions       = SortedListField(IntField(), null=False, required=True)
    # collection metadata
    meta = {
        'abstract': True,
        'indexes': [
          'item_count',
          'positions',
        ],
    }

### CACHE ###

CACHED_CLASSES     = dict()
CACHED_CLASS_NAMES = set()

def has_cached_class(class_name: str) -> bool:
    return (class_name in CACHED_CLASS_NAMES)

def get_cached_class(class_name: str) -> Union[BaseSegmentMatches, Document]: # BaseExpansionValue
    return CACHED_CLASSES.get(class_name)

### COLLECTION FACTORIES ###


def get_class_name(data_segment_id: int) -> str:
    return f"SegmentMatches{data_segment_id:05}"
    
def get_collection_name(data_segment_id: int) -> str:
    return f"segment_matches_{data_segment_id:05}"

def make_value_list_class(data_segment_id: int) -> BaseSegmentMatches:
    return type(get_class_name(data_segment_id=data_segment_id), (BaseSegmentMatches, ), {
        "meta" : {
            'collection': get_collection_name(data_segment_id=data_segment_id),
        }
    })

def get_segment_class(data_segment_id: int) -> Union[BaseSegmentMatches, Document]:
    class_name = get_class_name(data_segment_id=data_segment_id)
    if (has_cached_class(class_name) is False):
        value_list_class = make_value_list_class(data_segment_id=data_segment_id)
        CACHED_CLASSES[class_name] = value_list_class
        CACHED_CLASS_NAMES.add(class_name)
    return get_cached_class(class_name)


### END OF METACLASSES ###


def get_matched_byte_ids(segment_id: int, data_bytes: List[int], byte_ids: List[int]=range(0, HASH_SEGMENT_SIZE_BYTES), seed: int=0) -> List[int]:
    """
    Получить номера байт совпадающих в хеш-пространстве и в данных
    """
    matched_byte_ids = []
    prev_chunk_id = None
    chunk_bytes   = None
    for byte_id in byte_ids:
        chunk_id      = (segment_id * MAX_CHUNKS_IN_SEGMENT) + (byte_id // HASH_CHUNK_SIZE_BYTES)
        chunk_byte_id = byte_id % HASH_CHUNK_SIZE_BYTES
        if (prev_chunk_id != chunk_id):
            chunk_bytes   = bytes_from_nounce(nounce=chunk_id, seed=seed)
            prev_chunk_id = chunk_id
        hash_byte = chunk_bytes[chunk_byte_id]
        data_byte = data_bytes[byte_id]
        if (hash_byte == data_byte):
            matched_byte_ids.append(byte_id)
        #print(f"{byte_id}: hash_byte={hash_byte}, data_byte={data_byte} ({hash_byte == data_byte})")
    return matched_byte_ids

def get_matched_byte_count(segment_id: int, data_bytes: List[int], byte_ids: List[int]=range(0, HASH_SEGMENT_SIZE_BYTES), seed: int=0) -> int:
    """
    Подсчитать количество совпадений в сегменте из 256 байт
    """
    matches_count = 0
    prev_chunk_id = None
    chunk_bytes   = None
    for byte_id in byte_ids:
        chunk_id      = (segment_id * MAX_CHUNKS_IN_SEGMENT) + (byte_id // HASH_CHUNK_SIZE_BYTES)
        chunk_byte_id = byte_id % HASH_CHUNK_SIZE_BYTES
        if (prev_chunk_id != chunk_id):
            chunk_bytes   = bytes_from_nounce(nounce=chunk_id, seed=seed)
            prev_chunk_id = chunk_id
        hash_byte = chunk_bytes[chunk_byte_id] #get_hash_byte_by_id(segment_id=segment_id, byte_id=byte_id, seed=seed)
        data_byte = data_bytes[byte_id]
        if (hash_byte == data_byte):
            matches_count += 1
        #print(f"{byte_id}: hash_byte={hash_byte}, data_byte={data_byte} ({hash_byte == data_byte})")
    return matches_count

def get_hash_segment_bytes(hash_segment_id: int, seed: int=0) -> List[int]:
    segment_bytes = []
    for chunk_id in range(0, MAX_CHUNKS_IN_SEGMENT):
        nounce      = (hash_segment_id * MAX_CHUNKS_IN_SEGMENT) + chunk_id
        chunk_bytes = bytes_from_nounce(nounce=nounce, seed=seed)
        for byte_id in range(0, HASH_CHUNK_SIZE_BYTES):
            segment_bytes.append(chunk_bytes[byte_id])
    return segment_bytes

def get_data_segment_bytes(data_segment_id: int, data: bitarray) -> List[int]:
    data_bytes     = []
    start_position = data_segment_id * HASH_SEGMENT_SIZE_BITS
    for byte_id in range(0, HASH_SEGMENT_SIZE_BYTES):
        start     = start_position + byte_id * 8
        end       = start + 8
        data_byte = data[start:end]
        data_bytes.append(ba2int(data_byte, signed=False))
    return data_bytes

def create_hash_segment_object(hash_segment_id: int, data_segment_id: int,
        data_bytes: List[int], seed: int=0) -> Union[BaseSegmentMatches, Document, QuerySet]:
    SegmentClass = get_segment_class(data_segment_id=data_segment_id)
    positions    = get_matched_byte_ids(segment_id=hash_segment_id, data_bytes=data_bytes, seed=seed)
    return SegmentClass(
        id          = hash_segment_id,
        item_count  = len(positions),
        positions   = positions,
    )

def save_data_segment_items(best_segment_ids: Counter, data_segment_id: int, data_bytes: List[int], seed: int=0):
    SegmentClass       = get_segment_class(data_segment_id=data_segment_id)
    hash_segment_items = best_segment_ids.most_common()
    total_batches      = math.ceil(len(hash_segment_items) / SEGMENT_SAVE_BATCH_SIZE)
    for batch_id in range(0, total_batches):
        batch_items   = hash_segment_items[batch_id*SEGMENT_SAVE_BATCH_SIZE:batch_id*SEGMENT_SAVE_BATCH_SIZE+SEGMENT_SAVE_BATCH_SIZE]
        batch_objects = list()
        for hash_segment_id, match_count in batch_items:
            batch_object = create_hash_segment_object(
                hash_segment_id = hash_segment_id, 
                data_segment_id = data_segment_id, 
                data_bytes      = data_bytes, 
                seed            = seed,
            )
            batch_objects.append(batch_object)
        if (len(batch_objects) > 0):
            QuerySet(SegmentClass, collection=SegmentClass._get_collection()).insert(batch_objects, load_bulk=False)
        else:
            return

def get_last_hash_segment_id(data_segment_id: int, min_id: int=None, max_id: int=None) -> int:
    SegmentClass = get_segment_class(data_segment_id=data_segment_id)
    if (min_id is None):
        min_id = 0
    if (max_id is None):
        max_id = MAX_HASH_SEGMENT_ID
    last_segment = SegmentClass.objects(id__gte=min_id, id__lt=max_id).order_by('-id').limit(1).only('id').first()
    if (last_segment is None):
        return 0
    return last_segment.id

def discover_hash_segments(data_segment_id: int, data_bytes: List[int], start_shard_id: int=0, max_shards: int=None, 
      min_count: int=6, byte_ids: List[int]=range(0, HASH_SEGMENT_SIZE_BYTES), seed: int=0):
    if (max_shards is None):
        max_shards = DEFAULT_MAX_SHARDS #MAX_SEGMENT_SHARDS - start_shard_id
    start_segment_id = HASH_SHARD_SIZE * start_shard_id
    end_segment_id   = HASH_SHARD_SIZE * (start_shard_id + max_shards)
    end_shard_id     = end_segment_id // HASH_SHARD_SIZE
    last_segment_id  = get_last_hash_segment_id(data_segment_id=data_segment_id, min_id=start_segment_id, max_id=end_segment_id)
    last_shard_id    = last_segment_id // HASH_SHARD_SIZE
    # берем последний сохраненный id сегмента если продолжается прерванное сканирование
    if (last_segment_id > start_segment_id):
        start_segment_id = last_segment_id + 1
        start_shard_id   = start_segment_id // HASH_SHARD_SIZE
    # прогресс измеряется в количестве 256-байтных сегментов
    progress         = tqdm(range(start_segment_id, end_segment_id), mininterval=0.5, smoothing=0, unit=' seg')
    buffer_size      = 0
    max_buffer_size  = SEGMENT_SAVE_BATCH_SIZE
    pending_records  = Counter()
    segment_counts   = Counter()
    print(f"last_segment_id={last_segment_id}, last_shard_id={last_shard_id}, start_segment_id={start_segment_id}, end_segment_id={end_segment_id}, start_shard_id={start_shard_id}, end_shard_id={end_shard_id}")
    
    # основной цикл сканирования и поиска
    for segment_id in progress:
        segment_count = get_matched_byte_count(segment_id=segment_id, data_bytes=data_bytes, byte_ids=byte_ids, seed=seed)
        if (segment_count < min_count):
            continue
        if (segment_count == min_count):
            if ((segment_counts[segment_count] % 64) == 0):
                progress.set_postfix({
                    "counts" : f"{segment_counts.most_common(8)}",
                }, refresh=False)
        segment_counts.update({ segment_count: 1 })
        # fill save buffer
        pending_records.update({ segment_id: segment_count })
        buffer_size += 1
        progress.set_description_str(f"buffered: {buffer_size}", refresh=False)
        # flush save buffer
        if (buffer_size >= max_buffer_size):
            progress.set_description_str(f"(saving...)")
            save_data_segment_items(best_segment_ids=pending_records, data_segment_id=data_segment_id, data_bytes=data_bytes, seed=seed)
            pending_records.clear()
            buffer_size = 0
            progress.set_description_str(f"buffered: {buffer_size}", refresh=False)
    # flush buffer again in the end
    if (len(pending_records) > 0):
        progress.set_description_str(f"(saving...)")
        save_data_segment_items(best_segment_ids=pending_records, data_segment_id=data_segment_id, data_bytes=data_bytes, seed=seed)
        pending_records.clear()
        buffer_size = 0
        progress.set_description_str(f"buffered: {buffer_size}", refresh=True)

def update_segment_counts(segment_counts: Counter, data_bytes: List[int], 
        byte_ids: List[int]=range(0, HASH_SEGMENT_SIZE_BYTES), min_updated_count: int=0, min_segment_id: int=None, required_byte_id: int=None, seed: int=0) -> Counter:
    new_counts = segment_counts.copy()
    if (min_updated_count < 0):
        min_updated_count = 0
    for segment_id, _ in tqdm(segment_counts.most_common_above(min_updated_count).items(), mininterval=0.5, smoothing=0):
        if (min_segment_id is not None) and (segment_id < min_segment_id):
            del new_counts[segment_id]
            continue
        if (required_byte_id is None):
           new_value_count = get_matched_byte_count(segment_id=segment_id, data_bytes=data_bytes, byte_ids=byte_ids, seed=seed)
        else:
            segment_value_ids = get_matched_byte_ids(segment_id=segment_id, data_bytes=data_bytes, byte_ids=byte_ids, seed=seed)
            # если указана обязательная стартовая позиция, то исключаем те сегменты в которых её нет
            if (required_byte_id not in segment_value_ids):
                del new_counts[segment_id]
                continue
            new_value_count = len(segment_value_ids)
        new_counts[segment_id] = new_value_count
        if (new_value_count == 0):
            del new_counts[segment_id]
    return new_counts

def load_segment_counts(data_segment_id: int) -> Counter:
    SegmentClass   = get_segment_class(data_segment_id=data_segment_id)
    segment_counts = Counter()
    item_counts    = Counter()
    total_items    = SegmentClass.objects.count()
    progress       = tqdm(total=total_items, mininterval=0.5, smoothing=0)
    batch_size     = 2**16
    total_batches  = math.ceil(total_items / batch_size)

    for batch_id in range(0, total_batches + 1):
        skip        = batch_id * batch_size
        limit       = batch_size
        batch_items = SegmentClass.objects().only('id', 'item_count').order_by('id').skip(skip).limit(limit)
        progress.set_description_str(f"batch_id={batch_id}/{total_batches}")
        for segment_item in batch_items:
            segment_counts.update({ segment_item.id: segment_item.item_count })
            item_counts.update({ segment_item.item_count: 1 })
            progress.update(1)
        progress.set_postfix_str(f"{item_counts.most_common(8)}")
    return segment_counts

def get_segment_density(byte_ids: List[int]) -> float:
    """
    Определить плотность распределения совпадений в сегменте
    """
    total_bytes  = len(byte_ids)
    min_byte_id  = min(byte_ids)
    max_byte_id  = max(byte_ids)
    max_distance = max_byte_id - min_byte_id + 1
    # чем больше значений и чем меньше отрезок на котором они расположены - тем выше плотность
    return (total_bytes / max_distance)

def get_avg_value_distance(byte_ids: List[int]) -> float:
    """
    Определить среднюю дистанцию между 2 позициями при покрытии сегмента
    """
    total_bytes  = len(byte_ids)
    min_byte_id  = min(byte_ids)
    max_byte_id  = max(byte_ids)
    if (min_byte_id == max_byte_id):
        return 0
    max_distance = max_byte_id - min_byte_id + 1
    return (max_distance / total_bytes)