from __future__ import annotations
from rich import print
from rich.pretty import pprint
from custom_counter import CustomCounter as Counter
from dataclasses import dataclass, field
from bitarray import bitarray, frozenbitarray
from bitarray.util import ba2int, int2ba, huffman_code, ones, zeros, intervals
from typing import List, Set, Dict, Tuple, Optional, Union, Iterable, Sequence, Generator, FrozenSet
from levenshtein_coding import levenshtein_coding, decode_levenshtein_coding
from more_itertools import SequenceView, spy, seekable, peekable
from functools import lru_cache

DEFAULT_ENDIAN = 'big'

MAX_LEVENSHTEIN_VALUES = Counter({
    1 : 1, 
    2 : 1, 
    4 : 2, 
    7 : 4, 
    8 : 8, 
    12: 16, 
    13: 32, 
    14: 64,
    15: 128,
    17: 256,
    18: 512,
    19: 1024,
    20: 2048,
    21: 4096,
    22: 8192,
    23: 16384,
    24: 32768,
    29: 65536,
    30: 131072,
    31: 262144,
    32: 524288,
    33: 1048576,
    34: 2097152, 
    35: 4194304,
    36: 8388608,
})

@dataclass()
class DecodedNumber:
    value  : int
    length : int

def get_relative_value_id(value: int) -> int:
    ranges = get_value_ranges()
    for value_length, value_range in get_value_ranges().items():
        if (value in value_range):
            return value - value_range.start
    raise Exception(f"Not implemented")

def get_absolute_value(encoded_value_length: int, relative_value_id: int) -> int:
    if (has_levenshtein_length(value_length=encoded_value_length) is False):
        raise Exception(f"No id for value_length={encoded_value_length}")
    ranges = get_value_ranges()
    return ranges[encoded_value_length].start + relative_value_id

@lru_cache()
def get_value_ranges() -> Dict[int, range]:
    ranges = dict()
    start  = 0
    end    = 0
    for value_length, max_items in MAX_LEVENSHTEIN_VALUES.items():
        end  = start + max_items
        ranges[value_length] = range(start, end)
        start = end
    return ranges

def get_encoded_value_length(value: int) -> int:
    for value_length, value_range in get_value_ranges().items():
        if (value in value_range):
            return value_length
    return len(encode_number(number=value))

#@lru_cache()
def has_levenshtein_length(value_length: int) -> bool:
    precomputed_value_lengths = sorted(list(MAX_LEVENSHTEIN_VALUES.keys()))
    if (value_length < min(precomputed_value_lengths)):
        return False
    if (value_length > max(precomputed_value_lengths)):
        raise Exception(f"Not implemented")
    return (value_length in MAX_LEVENSHTEIN_VALUES)

#@lru_cache()
def get_levenshtein_length_id(value_length: int) -> int:
    if (has_levenshtein_length(value_length=value_length) is False):
        raise Exception(f"No id for value_length={value_length}")
    precomputed_value_lengths = sorted(list(MAX_LEVENSHTEIN_VALUES.keys()))
    return precomputed_value_lengths.index(value_length)

#@lru_cache()
def get_levenshtein_length_by_id(value_length_id: int) -> int:
    precomputed_value_lengths = sorted(list(MAX_LEVENSHTEIN_VALUES.keys()))
    return precomputed_value_lengths[value_length_id]

def get_total_intervals(first_interval_type: int, total: int) -> Tuple[int, int]:
    total_0 = total // 2
    total_1 = total_0
    if (total % 2) == 1:
        if (first_interval_type == 0):
            total_0 += 1
        else:
            total_1 += 1
    return (total_0, total_1)

@lru_cache(maxsize=2**17)
def encode_number(number: List[int]) -> bitarray:
    return bitarray(levenshtein_coding(n=number), endian=DEFAULT_ENDIAN)

def decode_number(bits: bitarray) -> DecodedNumber:
    decoded_number = decode_levenshtein_coding(code=bits.to01())
    decoded_length = get_encoded_value_length(value=decode_number()) #len(levenshtein_coding(n=decoded_number))
    return DecodedNumber(
        value  = decoded_number,
        length = decoded_length,
    )

def encode_number_sequence(numbers: List[int]) -> bitarray:
    sequence = bitarray()
    for n in numbers:
        sequence += bitarray(levenshtein_coding(n=n), endian=DEFAULT_ENDIAN)
    return sequence

def decode_number_sequence(bits: bitarray) -> List[int]:
    decoded_numbers = list()
    remaining_bits  = bits.to01()
    while (len(remaining_bits) > 0):
        decoded_number = decode_levenshtein_coding(code=remaining_bits)
        decoded_length = len(levenshtein_coding(n=decoded_number))
        remaining_bits = remaining_bits[decoded_length:len(remaining_bits)]
        decoded_numbers.append(decoded_number)
    return decoded_numbers

def data_bits_to_lv_bits(data: bitarray, min_distance: int=1) -> bitarray:
    if (len(data) == 0):
        return data
    encoded_data   = bitarray(endian=DEFAULT_ENDIAN) 
    flips          = list(intervals(data))
    distances      = list()

    if (flips[0][0] == 0):
        first_item_type = bitarray('0', endian=DEFAULT_ENDIAN)
    else:
        first_item_type = bitarray('1', endian=DEFAULT_ENDIAN)
    
    for value_type, start, end in flips:
        distance = end - start - min_distance
        distances.append(distance)
    encoded_distances = encode_number_sequence(numbers=distances)
    #print(f"encoded_distances={distances} ({len(distances)})")
    
    encoded_data += first_item_type
    encoded_data += encoded_distances

    return encoded_data

def lv_bits_to_data_bits(data: bitarray, min_distance: int=1) -> bitarray:
    if (len(data) == 0):
        return data
    interval_type       = data[0]
    sequence            = decode_number_sequence(bits=data[1:len(data)])
    decoded_sequence    = bitarray(endian=DEFAULT_ENDIAN)
    #distances           = list()

    for interval_length in sequence:
        if (interval_type == 1):
            decoded_length   = interval_length + min_distance
            decoded_interval = ones(decoded_length, endian=DEFAULT_ENDIAN)
            interval_type    = 0
        else:
            decoded_length   = interval_length + min_distance
            decoded_interval = zeros(decoded_length, endian=DEFAULT_ENDIAN)
            interval_type    = 1
        decoded_sequence += decoded_interval
    #print(f"decoded_distances={sequence} ({len(distances)})")
    
    return decoded_sequence


def encode_sparse_bitarray(data: bitarray, min_distance: int=1) -> bitarray:
    if (len(data) == 0):
        return data
    encoded_data   = bitarray() 
    flips          = list(intervals(data))
    distances      = list()
    distances_0    = list()
    distances_1    = list()
    min_distance_0 = 0
    min_distance_1 = 0
    #max_distance_1 = 0

    if (flips[0][0] == 0):
        first_item_type = bitarray(levenshtein_coding(0))
    else:
        first_item_type = bitarray(levenshtein_coding(1))
    
    for value_type, start, end in flips:
        distance = end - start - min_distance
        if (value_type == 0):
            distances_0.append(distance)
        else:
            distances_1.append(distance)
        distances.append(distance)
    
    encoded_distances_0 = bitarray(endian=DEFAULT_ENDIAN)
    if (len(distances_0) > 0):
        min_distance_0       = min(distances_0)
        adjusted_distances_0 = [d_0-min_distance_0 for d_0 in distances_0]
        encoded_distances_0  = encode_number_sequence(numbers=adjusted_distances_0)
        #pprint(distances_0, max_length=12)
        #pprint(adjusted_distances_0, max_length=12)
    
    encoded_distances_1 = bitarray()
    if (len(distances_1) > 0):
        min_distance_1       = min(distances_1)
        #max_distance_1       = max(distances_1)
        #if (min_distance_1 != max_distance_1):
        adjusted_distances_1 = [d_1-min_distance_1 for d_1 in distances_1]
        encoded_distances_1  = encode_number_sequence(numbers=adjusted_distances_1)
        #pprint(distances_1, max_length=12)
        #pprint(adjusted_distances_1, max_length=12)
    
    encoded_data += first_item_type
    encoded_data += bitarray(levenshtein_coding(min_distance_0), endian=DEFAULT_ENDIAN)
    encoded_data += bitarray(levenshtein_coding(min_distance_1), endian=DEFAULT_ENDIAN)
    #encoded_data += bitarray(levenshtein_coding(max_distance_1))
    encoded_data += encoded_distances_1
    encoded_data += encoded_distances_0

    #print(min_distance_0, min_distance_1, max_distance_1)
    #pprint(flips, max_length=12)
    #print(f"first: {flips[0][0]}, distances: {len(distances)}, remainder: {len(distances) % 2}, half: {len(distances) // 2},  d_0: {len(distances_0)}, d_1: {len(distances_1)}")
    
    return encoded_data

def decode_sparse_bitarray(data: bitarray, min_distance: int=1) -> bitarray:
    sequence            = decode_number_sequence(bits=data)
    decoded_sequence    = bitarray(endian=DEFAULT_ENDIAN)
    first_interval_type = sequence[0]
    min_distance_0      = sequence[1]
    min_distance_1      = sequence[2]
    #max_distance_1      = sequence[3]
    intervals           = sequence[3:len(sequence)]
    total_intervals     = get_total_intervals(first_interval_type=first_interval_type, total=len(intervals))
    intervals_0         = list()
    intervals_1         = list()
    
    if (total_intervals[0] > 0):
        intervals_0 = intervals[total_intervals[1]:len(intervals)]
    if (total_intervals[1] > 0):
        intervals_1 = intervals[0:total_intervals[1]]
    
    #print(sequence)
    #print(intervals)
    #print(intervals_0)
    #print(intervals_1)
    
    interval_type = first_interval_type
    for interval_position in range(0, len(intervals)):
        interval_id = interval_position // 2
        if (interval_type == 0):
            interval_length  = intervals_0[interval_id] + min_distance_0 + min_distance
            decoded_interval = zeros(interval_length, endian=DEFAULT_ENDIAN)
            interval_type    = 1
        else:
            interval_length  = intervals_1[interval_id] + min_distance_1 + min_distance
            decoded_interval = ones(interval_length, endian=DEFAULT_ENDIAN)
            interval_type    = 0
        decoded_sequence += decoded_interval

    return decoded_sequence