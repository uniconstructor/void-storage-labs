# https://stackoverflow.com/questions/38340808/recursive-typing-in-python-3-5
from __future__ import annotations
from rich import print
from rich.pretty import pprint
from rich.live import Live
from rich.panel import Panel
from rich.table import Table, Column
from rich.progress import track, TaskID,\
    BarColumn, Progress, Task, TaskID, TextColumn, TimeElapsedColumn, TimeRemainingColumn, TaskProgressColumn,\
    MofNCompleteColumn, RenderableColumn, SpinnerColumn, TransferSpeedColumn, FileSizeColumn, ProgressColumn
from rich.layout import Layout
from rich.columns import Columns
from rich.text import Text
from custom_counter import CustomCounter as Counter
#from tqdm import tqdm
from bitarray.util import ba2int, int2ba, canonical_huffman, huffman_code
from bitarray import bitarray, frozenbitarray
from collections import defaultdict, namedtuple
from dataclasses import dataclass, field
from copy import deepcopy, copy
from typing import List, Set, Dict, Tuple, Optional, Union, Iterable, Generator, Generic, Optional, Protocol, TypeVar, FrozenSet
from functools import lru_cache
import xxhash
#import math
from bitarrayset.binaryarrayset import BinaryArraySet
from enum import Enum
from sortedcontainers import SortedSet, SortedDict, SortedList

from fib_encoder import from_c1_bits, to_c1_bits, get_encoded_c1_length, get_encoded_c1_bits

INPUT_NOUNCE_BYTES = 4

@dataclass
class InvertedSortedSet:
    min_new_values      : int                               = field(default=32)
    old_value_ids       : Union[SortedDict, Dict[int, int]] = field(default=None)
    id_old_values       : Union[SortedDict, Dict[int, int]] = field(default=None, init=False)
    old_values          : Union[SortedSet, Set[int]]        = field(default=None, init=False)
    new_values          : Union[SortedSet, Set[int]]        = field(default=None, init=False)
    new_value_ids       : Union[SortedDict, Dict[int, int]] = field(default=None, init=False)
    value_range         : range[int]                        = field(default=None, init=False)
    min_old_value       : int                               = field(default=None, init=False)
    max_old_value       : int                               = field(default=None, init=False)
    min_new_value       : int                               = field(default=None, init=False)
    max_new_value       : int                               = field(default=None, init=False)

    def __post_init__(self):
        self.old_values      = SortedSet(self.old_value_ids.keys())
        self.id_old_values   = SortedDict([(ov_id, ov_val) for ov_val, ov_id in self.old_value_ids.items()])
        self.min_old_value   = min(self.old_values)
        self.max_old_value   = max(self.old_values)
        self.value_range     = range(0, (self.min_new_values + len(self.old_values)))
        self.new_values      = SortedSet([0]).update(SortedSet(self.value_range).difference(self.old_values))
        self.new_value_ids   = SortedDict([(0, 0)] + [(self.new_values[nv_id], nv_id) for nv_id in range(1, len(self.new_values))])
        self.min_new_value   = min(self.new_values)
        self.max_new_value   = max(self.new_values)

    def has_old_value(self, value: int) -> bool:
        return (value in self.old_values)
    
    def has_new_value(self, value: int) -> bool:
        return (not self.has_old_value(value=value))
    
    def has_old_value_id(self, old_value_id: int) -> bool:
        return (old_value_id in self.id_old_values.keys())
    
    def has_new_value_id(self, new_value_id: int) -> bool:
        return (new_value_id < len(self.new_values))
    
    def get_old_value_index(self, value: int) -> int:
        if (self.has_old_value(value=value) is False):
            return None
        return self.old_values.index(value=value)
    
    def get_old_value_id(self, value: int) -> int:
        if (self.has_old_value(value=value) is False):
            return None
        return self.old_values.index(value=value)
    
    def get_new_value_index(self, value: int) -> int:
        if (self.has_new_value(value=value) is False):
            return None
        return self.new_values.index(value=value)
    
    def get_old_value_by_id(self, value_id: int) -> int:
        if (value_id not in self.old_value_ids.values()):
            return None
        return self.id_old_values[value_id]
    
    def get_new_value_by_id(self, value_id: int) -> int:
        if (value_id > len(self.new_values)):
            return None
        return self.new_values[value_id]
    

@dataclass
class NewValueIdTree:
    value_ids             : Dict[int, int]              = field(default_factory=SortedDict)
    id_counts             : Counter                     = field(default_factory=Counter)
    min_new_values        : int                         = field(default=32)
    values                : Set[int]                    = field(default_factory=SortedSet, init=False)
    inverted_values       : InvertedSortedSet           = field(default=None, init=False)
    id_codes              : Dict[int, bitarray]         = field(default_factory=SortedDict, init=False)
    id_code_lengths       : Dict[int, int]              = field(default_factory=SortedDict, init=False)
    zero_element          : Union[int, Tuple[int, int]] = field(default=0, init=False)
    zero_element_id       : Union[int, Tuple[int, int]] = field(default=0, init=False)
    zero_element_code     : Union[int, bitarray]        = field(default=None, init=False)
    zero_element_length   : Union[int, int]             = field(default=1, init=False)

    def __post_init__(self):
        self.inverted_values     = InvertedSortedSet(old_value_ids=self.value_ids, min_new_values=self.min_new_values)
        self.values              = self.inverted_values.old_values
        self.has_old_value       = self.inverted_values.has_old_value
        self.has_new_value       = self.inverted_values.has_new_value
        self.has_old_value_id    = self.inverted_values.has_old_value_id
        self.has_new_value_id    = self.inverted_values.has_new_value_id
        self.get_old_value_index = self.inverted_values.get_old_value_index
        self.get_new_value_index = self.inverted_values.get_new_value_index
        self.get_old_value_by_id = self.inverted_values.get_old_value_by_id
        self.get_new_value_by_id = self.inverted_values.get_new_value_by_id
    
    def get_next_old_value_id(self) -> int:
        return len(self.value_ids)

    def get_next_old_value_id_code_length(self, next_old_value_id: int) -> int:
        if (next_old_value_id is None):
            next_old_value_id = self.get_next_old_value_id()
        next_id_counts = self.id_counts.copy()
        next_id_counts.update({ self.zero_element_id : 1 })
        next_id_counts.update({ next_old_value_id : 1 })
        next_codes = SortedDict(huffman_code(next_id_counts, endian=self.default_endian).items())
        return len(next_codes[next_old_value_id])

    def update_code_lengths(self):
        self.id_code_lengths.clear()
        self.sorted_code_lengths.clear()
        for value_id, value_code in self.id_codes.items():
            code_length                    = len(value_code)
            self.id_code_lengths[value_id] = code_length
            self.sorted_code_lengths.add(code_length)
            self.code_length_counts.update({ code_length : 1 })
    
    def update_id_codes(self):
        self.id_codes.clear()
        self.id_codes = SortedDict(huffman_code(self.id_counts, endian=self.default_endian).items())
        self.update_code_lengths()

    def register_new_value(self, new_value_id: int, value_id: int):
        if (self.has_new_value_id(new_value_id=new_value_id) is False):
            raise Exception(f"new_value_id={new_value_id} not found")
        new_value = self.get_new_value_by_id(value_id=new_value_id)
        if (self.has_new_value(value=new_value)):
            raise Exception(f"new_value={new_value} already registered")
        next_value_id                 = self.get_next_old_value_id()
        self.value_ids[new_value]     = next_value_id
        self.id_values[next_value_id] = new_value
        self.values.add(new_value)
    
    def add_new_value_id(self, new_value_id: int):
        if (self.has_new_value_id(new_value_id=new_value_id) is False):
            raise Exception(f"Cannot add new new_value_id={new_value_id} current next_value_id={self.get_next_value_id()}")
        #self.add_encoded_value_id(value_id=self.zero_element_id)
        self.id_counts.update({ self.zero_element_id : 1 })
        self.update_id_codes()
        next_value_id = self.get_next_old_value_id()
        self.id_counts.update({ next_value_id : 1 })
        self.update_id_codes()
        #self.add_encoded_value_id(value_id=new_value_id)
#
    #def add_value(self, value: Union[int, Tuple[int, int]]):
    #    if (self.has_value(value=value) is True):
    #        value_id = self.get_value_id(value=value)
    #        new_id   = False
    #    else:
    #        value_id = self.get_next_value_id()
    #        new_id   = True
    #    self.add_value_id(value_id=value_id)
    #    
    #    if (new_id is True):
    #        self.register_new_value(value=value, value_id=value_id)
    #    self.update_version()

@dataclass
class ItemCodesTree:
    steps_tree   : HuffmanTree            = field(default=None, init=False)
    seeds_tree   : HuffmanTree            = field(default=None, init=False)
    nounce_trees : Dict[int, HuffmanTree] = field(default_factory=SortedDict, init=False)
    seed_scores  : Dict[int, int]         = field(default_factory=SortedDict, init=False)
    version      : int                    = field(default=0, init=False)

    def __post_init__(self):
        self.steps_tree = HuffmanTree(zero_element=0)
        self.seeds_tree = HuffmanTree(zero_element=(0, 0), parent_tree=self.steps_tree)
    
    def register_step(self, step: int):
        pass

    def register_seed(self, seed: int):
        pass

    def register_nounce(self, seed: int, nounce: int):
        pass





@dataclass
class SparseRangeItem:
    id     : int
    step   : int
    offset : int
    value  : int

E = TypeVar("E", bound="_Comparable")

class _Comparable(Protocol):
    def __lt__(self: E, other: E) -> bool: ...
    def __le__(self: E, other: E) -> bool: ...
    def __gt__(self: E, other: E) -> bool: ...
    def __ge__(self: E, other: E) -> bool: ...


def to_value_id(length: int, value: int) -> Tuple(int, int):
    return (length, value)

def to_nounce_bytes(nounce: int) -> bytes:
    return nounce.to_bytes(length=INPUT_NOUNCE_BYTES, byteorder='little', signed=False)

@lru_cache(maxsize=2**16)
def value_offset_to_extend_unique_set(value: int, numbers: Union[Set[int], SortedSet[int], FrozenSet[int]]) -> int:
    if (value not in numbers):
        return 0
    offset = 1
    while (True):
        new_value = value + offset
        if (new_value not in numbers):
            return offset
        offset += 1

@lru_cache(maxsize=2**16)
def id_offset_to_extend_unique_set(value: int, numbers: Union[Set[int], SortedSet[int], FrozenSet[int]]) -> int:
    value_offset = value_offset_to_extend_unique_set(value=value, numbers=numbers)
    new_value    = value + value_offset
    if (new_value > max(numbers)):
        return value - len(numbers) - 1
    if (new_value < min(numbers)):
        return 1
    id_offset = 1
    for number in sorted(numbers):
        if (new_value < number):
            break
        id_offset += 1
    return id_offset

@lru_cache(maxsize=2**16)
def value_id_to_extend_unique_set(value: int, numbers: Union[Set[int], SortedSet[int], FrozenSet[int]]) -> int:
    new_values     = SortedSet()
    number         = 0
    while (True):
        if (value < number):
            break
        value_offset = value_offset_to_extend_unique_set(value=number, numbers=numbers)
        new_value    = number + value_offset
        new_values.add(new_value)
        number += 1
    return len(new_values) - 1

@dataclass
class SparseRange(Generic[E]):
    items : BinaryArraySet = field(default_factory=BinaryArraySet, init=False)
    
    # Runs in O(1) time
    def __len__(self) -> int:
        return self.items.length
    
    # Runs in O(1) time
    def clear(self) -> None:
        self.items.clear()
    
    # Note: Not fail-fast on concurrent modification
    def __iter__(self) -> Generator[E, None, None]:
        for vals in self.items.values:
            if vals is not None:
                yield from vals
        #return self.items.__iter__()
    
    # Runs in O((log n)^2) time
    def __contains__(self, val: E) -> bool:
        return (val in self.items)

    def get_next_id(self):
        return self.__len__()
    
    def get_next_item(self, step: int) -> SparseRangeItem:
        offset  = 0
        next_id = self.get_next_id()
        while (True):
            value = step + offset
            if (value not in self.items):
                break
            offset += 1
        return SparseRangeItem(
            id     = next_id,
            step   = step,
            offset = offset,
            value  = value, 
        )
    
    def apply_step(self, step: int) -> SparseRangeItem:
        """
        Добавить новый элемент в массив, логика расширения: диапазон
        number добавляется в items если он еще не присутствует в множестве
        Если number уже есть в items то к нему прибавляется 1 до тех пор пока не будет получено 
        уникальное (не пристуствующее в items) число. Это уникальное число в итоге и добавляется в items
        """
        next_item = self.get_next_item(step=step)
        self.items.add_unique(next_item.value)
        return next_item

    def apply_steps(self, steps: Union[Set[int], SortedSet[int]]) -> List[SparseRangeItem]:
        new_items = list()
        for step in steps:
            new_item = self.apply_step(step=step)
            new_items.append(new_item)
        return new_items
    
    def add_item(self, item: SparseRangeItem):
        self.items.add_unique(item.value)
    
    def add_value(self, value: int):
        self.items.add_unique(value)

@dataclass
class HuffmanTree:
    parent_tree           : HuffmanTree                 = field(default=None, repr=False)
    zero_element          : Union[int, Tuple[int, int]] = field(default=0)
    zero_element_id       : Union[int, Tuple[int, int]] = field(default=0, init=False)
    zero_element_type     : str                         = field(default='number', init=False)
    version               : int                         = field(default=0, init=False)
    values                : Set[int]                    = field(default_factory=SortedSet, init=False)
    value_ids             : Dict[int, int]              = field(default_factory=SortedDict, init=False)
    id_values             : Dict[int, int]              = field(default_factory=SortedDict, init=False)
    id_counts             : Counter                     = field(default_factory=Counter, init=False)
    id_codes              : Dict[int, bitarray]         = field(default_factory=SortedDict, init=False)
    id_code_lengths       : Dict[int, int]              = field(default_factory=SortedDict, init=False)
    sorted_code_lengths   : Set[int]                    = field(default_factory=SortedSet, init=False)
    code_length_counts    : Counter                     = field(default_factory=Counter, init=False)
    next_code_length      : int                         = field(default=None, init=False)
    encoded_ids           : List[bitarray]              = field(default_factory=list, init=False)
    encoded_ids_data      : bitarray                    = field(default=None, init=False)
    encoded_values        : List[bitarray]              = field(default_factory=list, init=False)
    encoded_values_data   : bitarray                    = field(default=None, init=False)
    default_endian        : str                         = field(default='big', init=False, repr=False)

    def get_next_value_id(self) -> int:
        return len(self.id_counts)
    
    def has_value(self, value: Union[int, Tuple[int, int]]) -> bool:
        return (value in self.values)
    
    def convert_to_parent_format(self, value: Union[int, Tuple[int, int]]) -> Union[int, Tuple[int, int]]:
        if (self.parent_tree is None):
            return value
        if (self.zero_element_type != self.parent_tree.zero_element_type):
            if (self.zero_element_type == 'tuple'):
                parent_value = value[1]
            else:
                parent_value = (value.bit_length(), value)
        else:
            parent_value = value
        return parent_value

    def has_parent_value(self, value: Union[int, Tuple[int, int]]) -> bool:
        if (self.parent_tree is None):
            return False
        parent_value = self.convert_to_parent_format(value=value)
        return self.parent_tree.has_value(value=parent_value)
    
    def get_parent_value_code(self, value: Union[int, Tuple[int, int]]) -> bitarray:
        if (self.has_parent_value(value=value) is False):
            raise Exception(f"code for value value={value} not found in parent tree (or no parent tree set)")
        parent_value    = self.convert_to_parent_format(value=value)
        parent_value_id = self.parent_tree.value_ids[parent_value]
        return self.parent_tree.id_codes[parent_value_id]

    def get_next_code_length(self, value_id: int=None) -> int:
        if (value_id is None):
            value_id = self.get_next_value_id()
        next_id_counts = self.id_counts.copy()
        next_id_counts.update({ self.zero_element_id : 1 })
        next_id_counts.update({ value_id : 1 })
        next_codes = SortedDict(huffman_code(next_id_counts, endian=self.default_endian).items())
        return len(next_codes[value_id])
    
    def get_value_id(self, value: Union[int, Tuple[int, int]]) -> int:
        if (value in self.values):
            value_id = self.value_ids[value]
        else:
            value_id = self.get_next_value_id()
        return value_id

    def get_code_length(self, value: Union[int, Tuple[int, int]]) -> int:
        if (value in self.values):
            value_id     = self.value_ids[value]
            value_length = self.id_code_lengths[value_id]
        else:
            value_length = self.next_value_length
        return value_length
    
    def get_min_code_length(self) -> int:
        return min(self.sorted_code_lengths)
    
    def get_max_code_length(self) -> int:
        return max(self.sorted_code_lengths)
    
    def update_version(self) -> int:
        self.version += 1
        return self.version
    
    def update_code_lengths(self):
        self.id_code_lengths.clear()
        self.sorted_code_lengths.clear()
        for value_id, value_code in self.id_codes.items():
            code_length                    = len(value_code)
            self.id_code_lengths[value_id] = code_length
            self.sorted_code_lengths.add(code_length)
            self.code_length_counts.update({ code_length : 1 })
    
    def update_next_code_length(self):
        self.next_code_length = self.get_next_code_length()
    
    def update_id_codes(self):
        self.id_codes.clear()
        self.id_codes = SortedDict(huffman_code(self.id_counts, endian=self.default_endian).items())
        self.update_code_lengths()
        self.update_next_code_length()
    
    def __post_init__(self):
        if (type(self.zero_element) is tuple):
            self.zero_element_type = 'tuple'
        else:
            self.zero_element_type = 'number'
        self.value_ids[self.zero_element] = self.get_next_value_id()
        self.zero_element_id              = self.value_ids[self.zero_element]
        self.values.add(self.zero_element)
        self.id_values[self.zero_element_id] = self.zero_element
        self.id_counts.update({ self.zero_element_id : 1 })
        self.update_id_codes()
        self.encoded_ids_data    = bitarray(endian=self.default_endian)
        self.encoded_values_data = bitarray(endian=self.default_endian)
    
    def add_encoded_value(self, value: Union[int, Tuple[int, int]]):
        if (type(value) is tuple):
            value_code = to_c1_bits(value[1])
        else:
            value_code = to_c1_bits(value)
        self.encoded_values.append(value_code)
        self.encoded_values_data += value_code
    
    def add_encoded_value_id(self, value_id: int):
        value_id_code = self.id_codes[value_id]
        self.encoded_ids.append(value_id_code)
        self.encoded_ids_data += value_id_code

    def register_new_value(self, value: Union[int, Tuple[int, int]], value_id: int):
        if (value in self.values):
            raise Exception(f"value={value} already registered")
        if (type(value) is tuple):
            if (value[0] < value[1].bit_length()):
                raise Exception(f"Incorrect bit_length={value[0]}, for value={value} (minimum is {value[1].bit_length()})")
        self.value_ids[value]    = value_id
        self.id_values[value_id] = value
        self.values.add(value)
        self.add_encoded_value(value=value)
    
    def add_value_id(self, value_id: int):
        if (value_id > self.get_next_value_id()):
            raise Exception(f"Cannot add new value_id={value_id} current next_value_id={self.get_next_value_id()}")
        if (value_id == self.get_next_value_id()):
            # for every new value_id we add a marker (zero-element code)
            self.add_encoded_value_id(value_id=self.zero_element_id)
            self.id_counts.update({ self.zero_element_id : 1 })
            self.update_id_codes()
            self.id_counts.update({ value_id : 1 })
            self.update_id_codes()
            self.add_encoded_value_id(value_id=value_id)
        else:
            self.add_encoded_value_id(value_id=value_id)
            self.id_counts.update({ value_id : 1 })
            self.update_id_codes()

    def add_value(self, value: Union[int, Tuple[int, int]]):
        if (self.has_value(value=value) is True):
            value_id = self.get_value_id(value=value)
            new_id   = False
        else:
            value_id = self.get_next_value_id()
            new_id   = True
        self.add_value_id(value_id=value_id)
        
        if (new_id is True):
            self.register_new_value(value=value, value_id=value_id)
        self.update_version()
    
    def encode(self) -> bitarray:
        result = bitarray(endian=self.default_endian)
        return result
    
    def decode(self, data: Union[bitarray, frozenbitarray]):
        pass

@dataclass
class StepRange:
    prefix_length          : int                 = field(default=5)
    numbers                : Set[int]            = field(default_factory=SortedSet, init=False)
    _steps                 : SparseRange         = field(default_factory=SparseRange, init=False, repr=False)
    steps                  : Set[int]            = field(default_factory=SortedSet, init=False)
    number_ids             : Dict[int, int]      = field(default_factory=SortedDict, init=False)
    id_numbers             : Dict[int, int]      = field(default_factory=SortedDict, init=False)
    step_ids               : Dict[int, int]      = field(default_factory=SortedDict, init=False)
    id_steps               : Dict[int, int]      = field(default_factory=SortedDict, init=False)
    id_lengths             : Dict[int, int]      = field(default_factory=SortedDict, init=False)
    number_id_counts       : Counter             = field(default_factory=Counter, init=False)
    step_id_counts         : Counter             = field(default_factory=Counter, init=False)
    step_id_codes          : Dict[int, bitarray] = field(default_factory=SortedDict, init=False)
    step_lengths           : Set[int]            = field(default_factory=SortedSet, init=False)
    next_step_length       : int                 = field(default=None, init=False)
    number_collection_size : int                 = field(default=None, init=False)

    def get_next_number_id(self) -> int:
        return len(self.number_id_counts)
    
    def get_next_step_id(self) -> int:
        return self._steps.get_next_id()
    
    def update_step_lengths(self):
        self.step_lengths.clear()
        for step_id, step_code in self.step_id_codes.items():
            step_length              = len(step_code)
            self.id_lengths[step_id] = step_length
            self.step_lengths.add(step_length)
    
    def update_step_codes(self):
        self.step_id_codes.clear()
        self.step_id_codes = SortedDict(huffman_code(self.step_id_counts, endian='little').items())
        self.update_step_lengths()
        self.update_next_step_length()
    
    def update_next_step_length(self):
        self.next_step_length = self.get_next_step_length()
    
    def update_sorted_steps(self):
        self.steps = SortedSet(self._steps)
    
    def get_next_step_length(self) -> int:
        next_id_counts = self.step_id_counts.copy()
        next_step_id   = self.get_next_step_id()
        next_id_counts.update({ self.step_ids[0] : 1 })
        next_id_counts.update({ next_step_id : 1 })
        next_codes     = SortedDict(huffman_code(next_id_counts, endian='little').items())
        return len(next_codes[next_step_id])
    
    def get_step_length(self, step: int) -> int:
        if (step in self.steps):
            step_id     = self.step_ids[step]
            step_length = self.id_lengths[step_id]
        else:
            step_length = self.next_step_length #self.get_next_step_length()
        return step_length
    
    def get_number_collection_size(self) -> int:
        size = 0
        for number in self.numbers:
            size += self.prefix_length
            size += number.bit_length()
        return size
    
    def update_number_collection_size(self) -> int:
        self.number_collection_size = self.get_number_collection_size()

    def __post_init__(self):
        self.number_ids[0] = self.get_next_number_id()
        self.numbers.add(0)
        self.id_numbers[self.number_ids[0]] = 0
        self.number_id_counts.update({ self.number_ids[0] : 1 })
        
        self.new_number(1)

        self.step_ids[0] = self.get_next_step_id()
        self._steps.add_value(value=0)
        self.id_steps[self.step_ids[0]] = 0
        self.step_id_counts.update({ self.step_ids[0]: 1 })

        self.new_step_from_number_id(self.number_ids[1])
        
        self.update_next_step_length()
        self.update_number_collection_size()
        self.update_sorted_steps()

    def new_step_from_step_id(self, step_id: int) -> SparseRangeItem:
        if (step_id not in set(self.id_steps.keys())):
            raise Exception(f"step_id={step_id} not registered")
        step      = self.id_steps[step_id]
        next_step = self._steps.get_next_item(step=step)
        if (next_step.value in self._steps):
            raise Exception(f"step={next_step} already registered")
        self._steps.add_item(item=next_step)
        self.step_ids[next_step.value] = next_step.id
        self.id_steps[next_step.id]    = next_step.value
        self.step_id_counts.update({ self.step_ids[0]: 1 })
        self.step_id_counts.update({ next_step.id: 1 })
        self.update_sorted_steps()
        self.update_step_codes()
        return next_step

    def new_step_from_number_id(self, number_id: int) -> SparseRangeItem:
        if (number_id not in set(self.id_numbers.keys())):
            raise Exception(f"number_id={number_id} not registered")
        number = self.id_numbers[number_id]
        if (number not in self.numbers):
            raise Exception(f"number={number} not registered")
        next_step = self._steps.get_next_item(step=number)
        if (next_step.value in self._steps):
            raise Exception(f"step={next_step} already registered")
        self._steps.add_item(item=next_step)
        self.step_ids[next_step.value] = next_step.id
        self.id_steps[next_step.id]    = next_step.value
        self.step_id_counts.update({ self.step_ids[0]: 1 })
        self.step_id_counts.update({ next_step.id: 1 })
        self.update_sorted_steps()
        self.update_step_codes()
        return next_step
    
    def new_number(self, number: int):
        if (number in self.numbers):
            raise Exception(f"number={number} already registered")
        number_id = self.get_next_number_id()
        self.numbers.add(number)
        self.number_ids[number]    = number_id
        self.id_numbers[number_id] = number
        self.number_id_counts.update({ self.number_ids[0] : 1 })
        self.number_id_counts.update({ number_id : 1 })
        self.update_number_collection_size()
    
    def new_step_from_new_number(self, number: int) -> SparseRangeItem:
        self.new_number(number=number)
        return self.new_step_from_number_id(number_id=self.number_ids[number])
    
@dataclass
class SeedRange:
    _seeds           : SparseRange         = field(default_factory=SparseRange, init=False, repr=False)
    seeds            : Set[int]            = field(default_factory=SortedSet, init=False)
    seed_ids         : Dict[int, int]      = field(default_factory=SortedDict, init=False)
    id_seeds         : Dict[int, int]      = field(default_factory=SortedDict, init=False)
    id_lengths       : Dict[int, int]      = field(default_factory=SortedDict, init=False)
    seed_id_counts   : Counter             = field(default_factory=Counter, init=False)
    seed_id_codes    : Dict[int, bitarray] = field(default_factory=SortedDict, init=False)
    seed_lengths     : Set[int]            = field(default_factory=SortedSet, init=False)
    next_seed_length : int                 = field(default=None, init=False)

    def get_next_seed_id(self) -> int:
        return self._seeds.get_next_id()
    
    def update_seed_lengths(self):
        self.seed_lengths.clear()
        for seed_id, seed_code in self.seed_id_codes.items():
            seed_length              = len(seed_code)
            self.id_lengths[seed_id] = seed_length
            self.seed_lengths.add(seed_length)
    
    def update_seed_codes(self):
        self.seed_id_codes.clear()
        self.seed_id_codes = SortedDict(huffman_code(self.seed_id_counts, endian='little').items())
        self.update_seed_lengths()
        self.update_next_seed_length()
    
    def update_next_seed_length(self):
        self.next_seed_length = self.get_next_seed_length()
    
    def update_sorted_seeds(self):
        self.seeds = SortedSet(self._seeds)
    
    def get_next_seed_length(self) -> int:
        next_id_counts = self.seed_id_counts.copy()
        next_seed_id   = self.get_next_seed_id()
        next_id_counts.update({ self.seed_ids[0] : 1 })
        next_id_counts.update({ next_seed_id : 1 })
        next_codes     = SortedDict(huffman_code(next_id_counts, endian='little').items())
        return len(next_codes[next_seed_id])
    
    def get_seed_length(self, seed: int) -> int:
        if (seed in self._seeds):
            seed_id     = self.seed_ids[seed]
            seed_length = self.id_lengths[seed_id]
        else:
            seed_length = self.next_seed_length #self.get_next_seed_length()
        return seed_length
    
    def __post_init__(self):
        self.seed_ids[0] = self.get_next_seed_id()
        self._seeds.add_value(value=0)
        self.id_seeds[self.seed_ids[0]] = 0
        self.seed_id_counts.update({ self.seed_ids[0]: 1 })

        self.update_next_seed_length()
    
    def get_seed_from_step(self, step: int) -> SparseRangeItem:
        return self._seeds.get_next_item(step=step)
    
    def new_seed_from_step(self, step: int) -> SparseRangeItem:
        next_seed = self.get_seed_from_step(step=step)
        if (next_seed.value in self._seeds):
            raise Exception(f"seed={next_seed} already registered")
        self._seeds.add_item(item=next_seed)
        self.seed_ids[next_seed.value] = next_seed.id
        self.id_seeds[next_seed.id]    = next_seed.value
        self.seed_id_counts.update({ self.seed_ids[0]: 1 })
        self.seed_id_counts.update({ next_seed.id: 1 })
        self.update_sorted_seeds()
        self.update_seed_codes()
        return next_seed

@dataclass
class SeedNounceRange:
    seed               : int
    seed_id            : int
    seed_length        : int
    _nounces           : SparseRange         = field(default_factory=SparseRange, init=False, repr=False)
    nounces            : Set[int]            = field(default_factory=SortedSet, init=False)
    nounce_ids         : Dict[int, int]      = field(default_factory=SortedDict, init=False)
    id_nounces         : Dict[int, int]      = field(default_factory=SortedDict, init=False)
    id_lengths         : Dict[int, int]      = field(default_factory=SortedDict, init=False)
    nounce_id_counts   : Counter             = field(default_factory=Counter, init=False)
    nounce_id_codes    : Dict[int, bitarray] = field(default_factory=SortedDict, init=False)
    nounce_lengths     : Set[int]            = field(default_factory=SortedSet, init=False)
    next_nounce_length : int                 = field(default=None, init=False)

    def get_next_nounce_id(self) -> int:
        return self._nounces.get_next_id()
    
    def update_nounce_lengths(self):
        self.nounce_lengths.clear()
        for nounce_id, nounce_code in self.nounce_id_codes.items():
            nounce_length              = len(nounce_code)
            self.id_lengths[nounce_id] = nounce_length
            self.nounce_lengths.add(nounce_length)
    
    def update_nounce_codes(self):
        self.nounce_id_codes.clear()
        self.nounce_id_codes = SortedDict(huffman_code(self.nounce_id_counts, endian='little').items())
        self.update_next_nounce_length()
    
    def update_next_nounce_length(self):
        self.next_nounce_length = self.get_next_nounce_length()
    
    def update_sorted_nounces(self):
        self.nounces = SortedSet(self._nounces)
    
    def get_next_nounce_length(self) -> int:
        next_id_counts = self.nounce_id_counts.copy()
        next_nounce_id = self.get_next_nounce_id()
        next_id_counts.update({ self.nounce_ids[0] : 1 })
        next_id_counts.update({ next_nounce_id : 1 })
        next_codes     = SortedDict(huffman_code(next_id_counts, endian='little').items())
        return len(next_codes[next_nounce_id])
    
    def get_nounce_length(self, nounce: int) -> int:
        if (nounce in self._nounces):
            nounce_id     = self.nounce_ids[nounce]
            nounce_length = self.id_lengths[nounce_id]
        else:
            nounce_length = self.next_nounce_length #self.get_next_nounce_length()
        return nounce_length
    
    def __post_init__(self):
        self.nounce_ids[0] = self.get_next_nounce_id()
        self._nounces.add_value(value=0)
        self.id_nounces[self.nounce_ids[0]] = 0
        self.nounce_id_counts.update({ self.nounce_ids[0]: 1 })

        self.new_nounce_from_step(step=1)

        self.update_next_nounce_length()
    
    def get_nounce_from_step(self, step: int) -> SparseRangeItem:
        return self._nounces.get_next_item(step=step)

    def new_nounce_from_step(self, step: int) -> SparseRangeItem:
        next_nounce = self.get_next_nounce_from_step(step=step)
        if (next_nounce.value in self._nounces):
            raise Exception(f"nounce={next_nounce} already registered")
        self._nounces.add_item(item=next_nounce)
        self.nounce_ids[next_nounce.value] = next_nounce.id
        self.id_nounces[next_nounce.id]    = next_nounce.value
        self.nounce_id_counts.update({ self.nounce_ids[0]: 1 })
        self.nounce_id_counts.update({ next_nounce.id: 1 })
        self.update_sorted_nounces()
        self.update_nounce_codes()
        return next_nounce

@dataclass
class DataValues:
    default_score    : int                        = field(default=2)
    values           : Set[Tuple[int, int]]       = field(default_factory=SortedSet, init=False)
    value_ids        : Dict[Tuple[int, int], int] = field(default_factory=SortedDict, init=False)
    id_values        : Dict[int, Tuple[int, int]] = field(default_factory=SortedDict, init=False)
    id_nounces       : Dict[int, int]             = field(default_factory=SortedDict, init=False)
    id_lengths       : Dict[int, int]             = field(default_factory=SortedDict, init=False)
    # all prefixes of the each value
    id_prefix_ids    : Dict[int, Set[int]]        = field(default_factory=SortedDict, init=False)
    nounce_value_ids : Dict[int, Tuple[int, int]] = field(default_factory=SortedDict, init=False)
    value_id_counts  : Counter                    = field(default_factory=Counter, init=False)
    value_id_codes   : Dict[int, bitarray]        = field(default_factory=SortedDict, init=False)
    value_lengths    : Set[int]                   = field(default_factory=SortedSet, init=False)

@dataclass
class SeedNounceValueItem:
    id              : int
    seed_step       : int
    seed_offset     : int
    seed_id         : int
    seed            : int
    nounce_step     : int
    nounce_offset   : int
    nounce_id       : int
    nounce          : int
    value_id        : int
    value           : int
    seed_length     : int
    nounce_length   : int
    value_length    : int
    is_target_value : bool

@dataclass
class SeedNounceValues:
    seed_id            : int
    seed               : int
    seed_length        : int
    seed_id_count      : int
    default_score      : int
    target_value_ids   : SortedSet[Tuple[int, int]] = field(default_factory=SortedSet)
    value_ids          : SortedSet[Tuple[int, int]] = field(default_factory=SortedSet, init=False)
    new_value_ids      : SortedSet[Tuple[int, int]] = field(default_factory=SortedSet, init=False)
    nounces            : SparseRange                = field(default_factory=SparseRange, init=False)
    nounce_id_counts   : Counter                    = field(default_factory=Counter, init=False)
    nounce_codes       : SortedDict                 = field(default_factory=SortedDict, init=False)
    nounce_ids         : SortedDict                 = field(default_factory=SortedDict, init=False)
    init_nounce        : int                        = field(default=1, init=False)
    next_nounce_length : int                        = field(default=None, init=False)

    def __post_init__(self):
        self.nounces.add_value(0)
        self.nounce_id_counts.update({ 0 : 1 })
        self.nounce_ids[0] = 0
        
        self.nounces.add_value(self.init_nounce)
        self.nounce_ids[self.init_nounce] = 1
        self.nounce_id_counts.update({ self.nounce_ids[self.init_nounce] : 1 })
        self.update_nounce_codes()

        value_length = self.get_value_length(nounce=self.init_nounce)
        value        = self.get_seed_value(seed=self.seed, nounce_bytes=to_nounce_bytes(self.init_nounce), value_length=value_length)
        self.value_ids.add(to_value_id(length=value_length, value=value))

        self.next_nounce_length = self.get_next_nounce_length()
    
    def get_next_nounce_id(self) -> int:
        return len(self.nounce_id_counts)
    
    def update_nounce_codes(self):
        self.nounce_codes.clear()
        self.nounce_codes = SortedDict(huffman_code(self.nounce_id_counts, endian='little').items())
    
    def get_nounce_length(self, nounce: int) -> int:
        nounce_id = self.nounce_ids[nounce]
        return len(self.nounce_codes[nounce_id])
    
    def get_next_nounce_length(self) -> int:
        next_id_counts = self.nounce_id_counts.copy()
        next_nounce_id = self.get_next_nounce_id()
        next_id_counts.update({ self.nounce_ids[0] : 1 })
        next_id_counts.update({ next_nounce_id : 1 })
        next_codes     = SortedDict(huffman_code(next_id_counts, endian='little').items())
        return len(next_codes[next_nounce_id])
    
    def get_value_length(self, nounce: int) -> int:
        if (nounce in self.nounces):
            nounce_length = self.get_nounce_length(nounce=nounce)
        else:
            nounce_length = self.get_next_nounce_length()
        return self.seed_length + nounce_length + self.default_score

    def get_seed_value(self, seed: int, nounce_bytes: bytes, value_length: int) -> int:
        digest = xxhash.xxh32_intdigest(input=nounce_bytes, seed=seed)
        return digest % (2**value_length)
    
    def new_value_from_nounce_step(self, nounce_step: int, target_value: int) -> SeedNounceValueItem:
        new_nounce    = self.nounces.get_next_item(step=nounce_step)
        seed_offset   = 0
        input_nounce  = to_nounce_bytes(nounce=new_nounce.value)
        nounce_length = self.get_next_nounce_length()
        value_length  = self.get_value_length(nounce=new_nounce.value)
        target_found  = False
        
        while (True):
            input_seed   = self.seed + seed_offset
            new_value    = self.get_seed_value(seed=input_seed, nounce_bytes=input_nounce, value_length=value_length)
            new_value_id = to_value_id(length=value_length, value=new_value)
            has_prefix   = False
            for prev_length in self.value_lengths:
                if (prev_length > value_length):
                    break
                new_value_prefix = new_value % (2**prev_length)
                if (new_value_prefix in self.values[prev_length]):
                    has_prefix = True
                    break
                if (prev_length in self.new_value_lengths) and (new_value_prefix in self.new_values[prev_length]):
                    has_prefix = True
                    break
            if (has_prefix is True):
                seed_offset += 1
                continue
            break
        
        if (target_value == new_value):
            target_found = True
        
        return SeedNounceValueItem(
            id              = new_value_id,
            seed_step       = self.seed,
            seed_offset     = seed_offset,
            seed_id         = self.seed_id,
            seed            = input_seed,
            nounce_step     = new_nounce.step,
            nounce_offset   = new_nounce.offset,
            nounce_id       = new_nounce.id,
            nounce          = new_nounce.value,
            value_id        = new_value_id,
            value           = new_value,
            seed_length     = self.seed_length,
            nounce_length   = nounce_length,
            value_length    = self.get_value_length(nounce=new_nounce.value),
            is_target_value = target_found,
        )
    
    def find_target_values_for_nounce_steps(self, nounce_steps: SortedSet[int], new_value_ids: SortedSet[Tuple[int, int]]) -> Dict[int, SeedNounceValueItem]:
        nounce_items = dict()
        self.new_value_ids.clear()
        self.new_value_ids = new_value_ids
        
        for nounce_step in nounce_steps:
            for target_length in self.target_lengths:
                target_value = self.target_values[target_length]
                new_item     = self.new_value_from_nounce_step(nounce_step=nounce_step, value_length=target_length, target_value=target_value)
                if (new_item.is_target_value is True):
                    return { nounce_step : new_item }
                else:
                    nounce_items[nounce_step] = new_item
        
        return nounce_items
    
    def add_new_item(self, item: SeedNounceValueItem):
        if (item.is_target_value is not True):
            raise Exception(f"Incorrect new seed item={item}")
        self.value_ids.add(item.value_id)
        self.nounces.add_value(item.nounce)
        self.nounce_ids[item.nounce_id] = item.nounce
        self.nounce_id_counts.update({ self.nounce_ids[0] : 1 })
        self.nounce_id_counts.update({ item.nounce_id : 1 })
        self.update_nounce_codes()
        self.seed_id_count += 1


#def load(self, data: bitarray, id_values: Dict[int, int]):
    #    position            = 0
    #    item_id             = 0
    #    remaining_bits      = len(data)
    #    zero_element_code   = self.id_codes[self.zero_element_id]
    #    zero_element_length = len(zero_element_code)
    #    next_item_lengths   = SortedSet([zero_element_length])
    #    value_id_counts     = Counter()
    #    
    #    while (True):
    #        if (remaining_bits <= 0):
    #            if (remaining_bits < 0):
    #                raise Exception(f"Error loading tree: last element length was incorrect")
    #            break
    #        next_item_length = zero_element_length
    #        next_item_start  = position
    #        next_item_end    = next_item_start + next_item_length
    #        next_item        = data[next_item_start:next_item_end]
    #        if (next_item == zero_element_code):
    #            value_id_counts.update({ self.zero_element_id : 1 })
    #            position += zero_element_length
    #            item_id  += 1