# https://github.com/Textualize/rich
from rich import print
# https://rich.readthedocs.io/en/latest/pretty.html
from rich.pretty import pprint
# https://pyoxidizer.readthedocs.io/en/stable/pyoxidizer_packaging_static_linking.html
from tqdm import tqdm as tqdm_notebook
# https://bitstring.readthedocs.io/en/latest/index.html
from bitstring import Bits, BitArray, BitStream, ConstBitStream
# https://pythongeeks.org/ordereddict-in-python/
# https://www.geeksforgeeks.org/counters-in-python-set-1/
# https://docs.python.org/3/library/collections.html#counter-objects
from collections import OrderedDict, Counter, defaultdict, namedtuple, deque
# https://docs.python.org/3/library/typing.html
from typing import List, Dict, Tuple, Optional, Union, Set
# https://habr.com/ru/company/timeweb/blog/564826/
# https://docs.python.org/3/library/enum.html
from enum import Enum
# https://realpython.com/python-data-classes/
# https://realpython.com/python-data-classes/#advanced-default-values
from dataclasses import dataclass, make_dataclass, field
import json
# https://github.com/mohanson/leb128
# https://en.wikipedia.org/wiki/LEB128
import leb128
import vlq
import os
if not 'workbookDir' in globals():
    workbookDir = os.getcwd()
os.chdir(workbookDir)

from leb_128_mongo import DEFAULT_BYTE_ORDER, HashItemValue, ContentBasedSplit, \
    create_content_based_split, \
    get_last_seed, find_best_seeds_for_tier, allocate_tier_values, \
    scan_new_seeds_for_tier, collect_tier_values
  
from hash_item_tiers import get_max_position_tier

### Chunked RLE encoding (inspired by dat.protocol v1)

class RleSpanType(int, Enum):
  """Тип rle-фрагмента"""
  # диапазон повторяющихся элементов
  RANGE    : int = 1
  # битовое поле
  BITFIELD : int = 0

class RleSpanValue(bytes, Enum):
  """Тип диапазона элементов"""
  # все элементы присутствуют
  ALL_PRESENT : bytes = bytes.fromhex('FF')
  # все элементы отсутствуют
  ALL_ABSENT  : bytes = bytes.fromhex('00')

@dataclass
class RleSpan:
  """Фрагмент rle-значения"""
  __slots__ = [
    'length',
    'type',
    'value',
  ]
  # number of chunks
  length : int
  # type of the span
  type   : RleSpanType
  # span value for continuation or bitfield value
  value  : Union[RleSpanValue, bytes]

def create_rle_spans(data: Union[ConstBitStream, Bits, BitArray]) -> List[RleSpan]:
  """
  Split binary data into list of RLE spans
  """
  if (len(data) % 8) != 0:
    raise Exception(f"Only byte-aligned value can be encoded")
  spans            = list()
  current_span     = None
  prev_span        = None
  # split data into chinks
  for position in range(0, (len(data) // 8)):
    chunk_start = position * 8
    chunk_end   = chunk_start + 8
    chunk_value = data[chunk_start:chunk_end]
    # define chunk type and value
    if (chunk_value.hex == '00'):
      span_type  = RleSpanType.RANGE
      span_value = RleSpanValue.ALL_ABSENT
      #print(f"{position}: RANGE, ALL_ABSENT", chunk_value.hex, span_value.hex())
    elif (chunk_value.hex == 'ff'):
      span_type  = RleSpanType.RANGE
      span_value = RleSpanValue.ALL_PRESENT
      #print(f"{position}: RANGE, ALL_PRESENT", chunk_value.hex, span_value.hex())
    else:
      span_type  = RleSpanType.BITFIELD
      span_value = chunk_value.tobytes()
      #print(f"{position}: BITFIELD", chunk_value.hex, span_value.hex())
    # create new current span
    current_span = RleSpan(
      length = 1,
      type   = span_type,
      value  = span_value,
    )
    # required for last span in the list
    last_span_saved = False
    # combine current and previous span (if they are parts of the same range/bitfield)
    if (prev_span is not None):
      if (current_span.type == RleSpanType.BITFIELD):
        # join bitfields
        if (prev_span.type == current_span.type):
          current_span.length = prev_span.length + current_span.length
          current_span.value  = bytes().join([prev_span.value, current_span.value])
        else:
          last_span_saved = True
          spans.append(prev_span)
      elif (current_span.type == RleSpanType.RANGE):
        # join sequence ranges
        if (prev_span.type == current_span.type) and (prev_span.value == current_span.value):
          current_span.length = prev_span.length + current_span.length
        else:
          last_span_saved = True
          spans.append(prev_span)
    # remember previous span value
    prev_span = current_span
    # pprint(current_span)
  # save last span manually if we need to do so
  if (last_span_saved is False):
    spans.append(current_span)
  return spans

def encode_rle_spans(spans: List[RleSpan]) -> bytes:
  """
  Encode list of RLE spans into bytes
  """
  encoded_bytes_list = list()
  for span in spans:
    if (span.type == RleSpanType.BITFIELD):
      # add 0b0 as last bit and shift length value to 1
      encoded_span_length = span.length * 2
    elif (span.type == RleSpanType.RANGE):
      if (span.value == RleSpanValue.ALL_PRESENT):
        # add 0b11 as last bits and shift length value to 2
        encoded_span_length = (span.length * 4) + 3
      elif (span.value == RleSpanValue.ALL_ABSENT):
        # add 0b01 as last bits and shift length value to 2
        encoded_span_length = (span.length * 4) + 1
      else:
        raise Exception(f"incorrect span.value={span.value}")
    else:
      raise Exception(f"incorrect span.type={span.type}")
    # print(bin(encoded_span_length))
    # encode each span as custom varint
    encoded_span_bytes = leb128.u.encode(encoded_span_length)
    #encoded_span_bytes = vlq.encode([encoded_span_length])
    # add bitfield bytes after each bitfield span varint
    if (span.type == RleSpanType.BITFIELD):
      encoded_span_bytes = bytearray().join([encoded_span_bytes, span.value])
    # add each encoded span to the final result
    encoded_bytes_list.append(encoded_span_bytes)
  # pprint([item.hex() for item in encoded_bytes_list])
  return bytearray().join(encoded_bytes_list)

def encode_rle_data(data: Union[ConstBitStream, Bits, BitArray]) -> bytearray:
  spans = create_rle_spans(data)
  return encode_rle_spans(spans)

def decode_rle_spans(spans: List[RleSpan]) -> BitArray:
  """
  Expand list of RLE spans into original bitfield
  """
  pass

def restore_rle_spans(encoded_spans: BitArray) -> List[RleSpan]:
  """
  Expand list of RLE spans into original bitfield
  """
  pass

def decode_rle_data(data: Union[ConstBitStream, Bits, BitArray]) -> bytearray:
  pass


def allocate_content_based_split(split_data: ContentBasedSplit, priority_seeds: Set[int]=None):
  # final seed dictiorary: all seeds, used across all allocations of each tier (unique values only)
  # this set will be used in final result to encode/decode data values
  allocation_seeds = set()
  # define maximum available tier
  max_tier = get_max_position_tier(total_data_items=split_data.unique_values_count)
  min_tier = 1
  # allocate values of each tier in 3 stages:
  # - allocate_tier_values(): database only, priority_seeds only
  # - scan_new_seeds_for_tier(): hash-space only, priority_seds only
  # - scan_new_seeds_for_tier(): hash-space only, excluding priority_seeds, start from seed 0
  tiers = sorted([tier for tier in range(min_tier, (max_tier + 1))], reverse=True)
  # start allocation from largest tier in order to maximize priority seeds usage 
  # (because largest tier have most options for value allocation)
  for tier in tiers:
    tier_address_length = tier
    tier_value_length   = tier_address_length + 1
    # collect values for this tier
    tier_values = collect_tier_values(target_value_length=tier_value_length, data_values=split_data.data_values)
    print(f"[T{tier}]: l={len(tier_values)} ->", f"(0-15): {sorted(list(tier_values))[0:16]}...")
    # starting first allocation, using priority-seeds only
    # if database doesn't have enough values to make complete allocation
    # search for new values is automatically started, adding new values in db
    tier_allocation = allocate_tier_values(address_length=tier_address_length, values=tier_values.copy())
    #pprint(tier_allocation, max_length=16)
    pprint(dict(tier_allocation.position_seeds), max_length=16)

# https://docs.python.org/3/library/enum.html#omitting-values
class NoValue(Enum):
    def __repr__(self):
        return '<%s.%s>' % (self.__class__.__name__, self.name)

# https://bitstring.readthedocs.io/en/latest/constbitstream.html#bitstring.ConstBitStream.readlist
# https://bitstring.readthedocs.io/en/latest/constbitstream.html#bitstring.ConstBitStream.read
# https://docs.python.org/3/library/enum.html#when-to-use-new-vs-init
class EncodedLayerOptionsFormat(bytes, Enum):
    """
    Top-level encoded layer data

    >>> len(EncodedLengthOptionsFormat)
    6
    >>> [value_field.label for value_field in EncodedLengthOptionsFormat]
    ['length_item_unit', 'min_length', 'max_length', 'free_space_bits', 'position_step', 'value_step']
    >>> [value_field.format for value_field in EncodedLengthOptionsFormat]
    ['uint:4', 'uint:2', 'uint:3', 'uint:3', 'uint:2', 'uint:2']
    >>> length_options = EncodedLengthOptions(8, 1, 4, 2, 1, 1)
    >>> length_options
    EncodedLengthOptions(length_item_unit=8, min_length=1, max_length=4, free_space_bits=2, position_step=1, value_step=1)
    >>> [f"{EncodedLengthOptionsFormat(value_number).format}={length_options[value_number]}" for value_number in range(0, len(length_options))]
    ['uint:4=8', 'uint:2=1', 'uint:3=4', 'uint:3=2', 'uint:2=1', 'uint:2=1']
    """
    def __new__(cls, position, label, format):
        obj = bytes.__new__(cls, [position])
        obj._value_ = position
        obj.label   = label
        obj.format  = format
        return obj
    layer_id           = (0, 'layer_id', 'uint:16')
    seed_bitmap_length = (1, 'seed_bitmap_length', 'uint:32')
    data_items         = (2, 'data_items', 'uint:32')
    header_length      = (3, 'header_length', 'uint:24')

# named tuple for storing decoded options
EncodedLayerOptions = namedtuple('EncodedLayerOptions', [value_field.label for value_field in EncodedLayerOptionsFormat])