# https://github.com/Textualize/rich
from rich import print
# https://rich.readthedocs.io/en/latest/pretty.html
from rich.pretty import pprint
import rich.repr

# https://bitstring.readthedocs.io/en/latest/index.html
from bitstring import Bits, BitArray, BitStream, ConstBitStream
import collections
from collections import OrderedDict, defaultdict, namedtuple, deque
from typing import List, Dict, Sequence, Set, Tuple, Optional, Union, Iterable
# https://docs.python.org/3/library/dataclasses.html
from dataclasses import dataclass, make_dataclass, field, asdict
from custom_counter import CustomCounter as Counter

# https://docs.mongoengine.org/
from mongoengine import Document, QuerySet, LongField, IntField, BinaryField, StringField

#connect(db='blib', host='127.0.0.1', port=27017, maxPoolSize=300)
# https://docs.mongoengine.org/guide/querying.html#advanced-queries
from mongoengine.queryset.visitor import Q

import os

if not 'workbookDir' in globals():
    workbookDir = os.getcwd()
os.chdir(workbookDir)

# функции для работы с хеш-пространством
from hash_space_utils import HASH_DIGEST_BITS, DataUnit, bytes_at_position, value_at_position

# db collection classes
from sharded_seed_classes import *

DEFAULT_SEED = 0

# размер сегмента хеш-пространства: количество позиций которое сканируется при поиске нового элемента данных
DEFAULT_SEGMENT_SIZE = 2**32

# кеш для запросов проверки инициализированных значений
BASIS_CACHE = defaultdict(lambda: None)

### STATS/METADATA ###

class SeedValueStatsQuerySet(QuerySet):
    """
    Дополнительные функции для подсчета статистики по значениям
    """
    
    def update_last_position(self, value: bytes):
        """
        Обновить позицию последнего упоминания значения
        """
        value_id = bytes_to_value_id(value=value)
        if (has_seed_value(value=value) is False):
            return
        ValueCollection = get_value_collection(value=value)
        last_position   = ValueCollection.objects.get_last_position(value_id=value_id)
        SeedValueStats.objects(id=value_id).update_one(
            set__last_position = last_position,
        )
    
    def update_instances_count(self, value: bytes):
        """
        Обновить счетчик количества упоминаний значения: упоминанием считается любое более длинное значение 
        начало которого совпадает с переданным
        """
        value_id = bytes_to_value_id(value=value)
        if (has_seed_value(value=value) is False):
            return
        ValueCollection = get_value_collection(value=value)
        instances_count = ValueCollection.objects.value_instances(value_id=value_id).count()
        SeedValueStats.objects(id=value_id).update_one(
            set__instances_count = instances_count,
        )
    
    def update_children_count(self, value: bytes):
        """
        Обновить счетчик количества значений-потомков
        """
        value_id = bytes_to_value_id(value=value)
        if (has_seed_value(value=value) is False):
            return
        current_stats   = SeedValueStats.objects(id=value_id).get()
        ValueCollection = get_value_collection(value=value)
        children_count  = ValueCollection.objects.children_values(value_id=value_id).count()
        is_completed    = current_stats.is_completed
        if (children_count == 256):
            is_completed = True
            if (current_stats.is_completed is False):
                print(f"value_id=0x{value_id} l={current_stats.value_length}: is_completed=True (children_count={children_count})")
        SeedValueStats.objects(id=value_id).update_one(
            set__children_count = children_count,
            set__is_completed   = is_completed,
        )

class SeedValueStats(Document):
    """
    Статистика и метаданные значения в хеш-пространстве: в отличие от BaseSeedValue данные этой коллекции
    не разделены 256 частей и хранятся совместно
    """
    # value_id - само значение в качестве уникального id
    id                       = StringField(max_length=16, primary_key=True)
    #id                       = BinaryField(max_bytes=8, primary_key=True)
    # id пространства хешей
    seed_id                  = IntField(min_value=0)
    # id более короткого значения
    #parent_value_id          = BinaryField(max_bytes=7)
    parent_value_id          = StringField(max_length=16)
    # длина значения (в байтах)
    value_length             = IntField(min_value=1, max_value=16)
    # первое упоминание значения в пространстве хешей
    first_position           = LongField(min_value=0)
    # последнее известное упоминание значения в пространстве хешей
    last_position            = LongField(min_value=0)
    # текущее количество упоминаний этого значения найденных во всех просканированных сегментах
    instances_count          = IntField(min_value=0)
    # текущее количество найденных потомков  этого значения
    children_count           = IntField(min_value=0, max_value=256)
    # следующий сегмент хеш-пространства для поиска позиций этого значения
    next_segment             = IntField(min_value=0, default=0)
    # является ли поиск потомков этого значения завершенным (по умолчанию False)
    # True устанавливается после того как будут найдены позиции всех 256 возможных потомков этого значения 
    is_completed             = BooleanField(default=False)
    meta = {
        'queryset_class': SeedValueStatsQuerySet,
        'indexes': [
            #'seed_id',
            'parent_value_id',
            'value_length',
            'first_position',
            'last_position',
            'is_completed',
        ],
    }

    def update_stats(self):
        """
        Обновить статистику по этому значению
        """
        value = value_id_to_bytes(self.id)
        self._qs.update_last_position(value=value)
        self._qs.update_instances_count(value=value)
        self._qs.update_children_count(value=value)
        self.reload()
        return self

### POSITION SCANNING ###

def bytes_to_value_id(value: bytes) -> str:
    return value.hex()

def value_id_to_bytes(value_id: str) -> bytes:
    return bytes.fromhex(value_id)

def get_prefix(value: bytes) -> bytes:
    if len(value) == 0:
        raise Exception(f"No prefix for value={value} (length=0)")
    return value[0:1]

def get_prefix_id(value: bytes) -> str:
    if (len(value) == 0):
        return ''
    prefix = get_prefix(value=value)
    return prefix.hex()

def get_value_collection(value: bytes, seed: int=DEFAULT_SEED) -> Union[BaseSeedValue, Document]:
    prefix_id = get_prefix_id(value=value)
    return get_seed_value_class(prefix_id=prefix_id, seed=seed)

def get_position_collection(value: bytes, seed: int=DEFAULT_SEED) -> Union[BaseSeedValuePosition, Document]:
    prefix_id = get_prefix_id(value=value)
    return get_seed_value_position_class(prefix_id=prefix_id, seed=seed)

def drop_value_collection(value: bytes):
    ValueCollection = get_value_collection(value=value)
    ValueCollection.drop_collection()

def drop_position_collection(value: bytes):
    PositionCollection = get_position_collection(value=value)
    PositionCollection.drop_collection()

def get_max_prefix_value(value: bytes, min_length: int=2) -> Union[BaseSeedValue, Document]:
    ValueCollection = get_value_collection(value=value)
    value_id        = bytes_to_value_id(value)
    return ValueCollection.objects.get_longest_parent(value_id=value_id, min_length=min_length)

def get_min_prefix_value(value: bytes, min_length: int=2) -> Union[BaseSeedValue, Document]:
    ValueCollection = get_value_collection(value=value)
    value_id        = bytes_to_value_id(value)
    return ValueCollection.objects.get_shortest_parent(value_id=value_id, min_length=min_length)

def has_basis(value: bytes) -> bool:
    prefix_id = get_prefix_id(value=value)
    if (BASIS_CACHE[prefix_id] is not None):
        return BASIS_CACHE[prefix_id]
    ValueCollection = get_value_collection(value=value)
    BASIS_CACHE[prefix_id] = ValueCollection.objects.is_completed(value_id=prefix_id)
    return BASIS_CACHE[prefix_id]

def has_seed_value(value: bytes) -> bool:
    ValueCollection = get_value_collection(value=value)
    value_id        = bytes_to_value_id(value)
    value_count     = ValueCollection.objects(id=value_id).limit(1).count(with_limit_and_skip=True)
    return (value_count > 0)

def get_seed_value(value: bytes) -> Union[BaseSeedValue, Document]:
    ValueCollection = get_value_collection(value=value)
    value_id        = bytes_to_value_id(value)
    return ValueCollection.objects(id=value_id).get()

def has_parent_value(value: bytes, parent_length: int=None) -> bool:
    ValueCollection = get_value_collection(value=value)
    value_id        = bytes_to_value_id(value)
    value_count     = ValueCollection.objects.parent_value(value_id=value_id, parent_length=parent_length).limit(1).count(with_limit_and_skip=True)
    return (value_count > 0)

def get_parent_value(value: bytes, parent_length: int=None) -> Union[BaseSeedValue, Document]:
    ValueCollection = get_value_collection(value=value)
    value_id        = bytes_to_value_id(value)
    return ValueCollection.objects.parent_value(value_id=value_id, parent_length=parent_length).get()

def has_value_position(value: bytes, position: int) -> bool:
    PositionCollection = get_position_collection(value=value)
    position_count     = PositionCollection.objects(id=position).limit(1).count(with_limit_and_skip=True)
    return (position_count > 0)

def get_value_position(value: bytes, position: int) -> Union[BaseSeedValuePosition, Document]:
    PositionCollection = get_position_collection(value=value)
    return PositionCollection.objects(id=position).get()

def has_value_stats(value: bytes) -> bool:
    value_id    = bytes_to_value_id(value)
    value_count = SeedValueStats.objects(id=value_id).limit(1).count(with_limit_and_skip=True)
    return (value_count > 0)

def get_value_stats(value: bytes) -> Union['SeedValueStats', Document]:
    value_id = bytes_to_value_id(value)
    return SeedValueStats.objects(id=value_id).get()

def update_value_stats(value: bytes) -> Union['SeedValueStats', Document]:
    """
    Обновить статистику для указанного значения
    """
    value_stats = None
    if (has_value_stats(value=value)):
        value_stats = get_value_stats(value=value)
    else:
        value_stats = init_value_stats(value=value)
    value_stats.update_stats()
    return value_stats

def find_prefix_root_position(value: bytes) -> int:
    """
    Найти первое вхождение первого байта значения
    """
    positions        = range(0, 2**32)
    value_prefix     = get_prefix(value=value)
    prefix_length    = len(value_prefix)
    for position in positions:
        value = bytes_at_position(position=position, length=prefix_length, seed=DEFAULT_SEED, use_bytearray=False, unit=DataUnit.BYTES)
        if (value == value_prefix):
            break
    return position

def find_root_basis(value: bytes) -> Dict[str, int]:
    """
    Найти позицию первого вхождения первого байта значения (например '00') и позиции всех его 256 потомков (от '0000' до '00ff')
    """
    positions        = range(0, 2**32)
    ValueCollection  = get_value_collection(value=value)
    prefix           = get_prefix(value=value)
    prefix_id        = get_prefix_id(value=value)
    prefix_length    = len(prefix)
    value_length     = prefix_length + 1
    target_value_ids = ValueCollection.objects.get_children_ids(value_id=prefix_id)
    result           = dict()
    # set root item
    root_position     = find_prefix_root_position(value=value)
    result[prefix_id] = root_position
    
    # find position for all prefix children
    for position in positions:
        value    = bytes_at_position(position=position, length=value_length, seed=DEFAULT_SEED, use_bytearray=False, unit=DataUnit.BYTES)
        value_id = bytes_to_value_id(value)
        if (value_id in target_value_ids):
            result[value_id] = position
            target_value_ids.remove(value_id)
            if (len(target_value_ids) == 0):
                break
    # result will contain first occurances of all 2-byte values starting with prefix
    return result

def find_seed_values(values: Set[bytes], positions: Iterable[int], min_length: int=2, seed: int=DEFAULT_SEED) -> Dict[str, List[int]]:
    """
    Find all occurences of the value within a given position range
    """
    prefixes         = set()
    parent_ids       = defaultdict(set)
    min_value_length = None
    max_value_length = None
    target_value_ids = set()
    result           = defaultdict(list)
    # prepare values for scan
    for value in values:
        ValueCollection = get_value_collection(value=value)
        value_length    = len(value)
        value_id        = bytes_to_value_id(value=value)
        if (min_value_length is None) or (value_length < min_value_length):
            min_value_length = value_length
        if (max_value_length is None) or (value_length > max_value_length):
            max_value_length = value_length
        # collect original values
        target_value_ids.add(value_id)
        # collect prefixes
        prefix_id = get_prefix_id(value=value)
        prefixes.add(prefix_id)
        # collect common parent values
        value_parent_ids = ValueCollection.objects.get_parent_ids(value_id=value_id, min_length=min_length)
        for parent_id in value_parent_ids:
            parent_ids[prefix_id].add(parent_id)
            target_value_ids.add(parent_id)
    # scan seed items
    min_target_length = min_length
    max_target_length = max_value_length
    target_lengths    = list(range(min_target_length, max_target_length + 1))
    for position in positions:
        max_position_value = bytes_at_position(position=position, length=max_target_length, seed=seed, use_bytearray=False, unit=DataUnit.BYTES)
        for target_length in target_lengths:
            position_value    = max_position_value[0:target_length]
            position_value_id = bytes_to_value_id(value=position_value)
            if (position_value_id in target_value_ids):
                result[position_value_id].append(position)
    # result will contain all positions for all values
    return result
        
def extend_value_position(value: bytes, position: int, seed: int=DEFAULT_SEED) -> Union[BaseSeedValuePosition, Document]:
    if (has_basis(value=value) is False):
        init_root_basis(value=value)
    return save_seed_value(value=value, position=position, seed=seed)

def init_root_basis(value: bytes) -> List[Union[BaseSeedValue, Document]]:
    """
    Найти и сохранить стартовый набор значений начинающихся с первого байта значения
    """
    ValueCollection = get_value_collection(value=value)
    prefix_id       = get_prefix_id(value=value)
    if (has_basis(value=value) is True):
        return ValueCollection.objects.basis_values(value_id=prefix_id).all()
    basis_items     = find_root_basis(value=value)
    new_items       = list()
    new_positions   = list()
    new_value_stats = list()
    # save each item in basis
    for value_id, position in basis_items.items():
        value        = value_id_to_bytes(value_id=value_id)
        value_length = len(value)
        new_item     = ValueCollection(
            id           = value_id,
            position     = position,
            value_length = value_length,
        )
        new_item.save()
        new_position   = save_seed_value_position(new_item)
        new_item_stats = save_seed_value_stats(new_item)
        # update item/position list
        new_items.append(new_item)
        new_positions.append(new_position)
        new_value_stats.append(new_item_stats)
        # update parent value stats
        if (has_parent_value(value=value)):
            new_item_parent = get_parent_value(value=value)
            update_value_stats(value=value_id_to_bytes(new_item_parent.id))
    # update cache
    BASIS_CACHE[prefix_id] = True
    return new_items

def init_value_stats(value: bytes) -> SeedValueStats:
    """
    Вычислить и сохранить в базу статистику по указанному значению
    """
    value_id = bytes_to_value_id(value=value)
    if (has_value_stats(value=value)):
        return get_value_stats(value=value)
    ValueCollection = get_value_collection(value=value)
    value_item      = get_seed_value(value=value)
    parent_value_id = ValueCollection.objects.get_parent_id(value_id=value_id)
    last_position   = ValueCollection.objects.get_last_position(value_id=value_id)
    instances_count = ValueCollection.objects.value_instances(value_id=value_id).count()
    children_count  = ValueCollection.objects.children_values(value_id=value_id).count()

    value_stats = SeedValueStats(
        id              = value_item.id,
        seed_id         = DEFAULT_SEED,
        first_position  = value_item.position,
        last_position   = last_position,
        parent_value_id = parent_value_id,
        value_length    = value_item.value_length,
        instances_count = instances_count,
        children_count  = children_count,
    )
    value_stats.save()
    return value_stats

def get_unique_saving_value(value: bytes, position: int, seed: int=DEFAULT_SEED) -> bytes:
    """
    Получить значение для сохранения в базу из переданного значения
    Функция возвращает самый короткий не использованный ранее вариант значения 
    """
    value_id = bytes_to_value_id(value=value)
    if (has_basis(value=value) is False):
        init_root_basis(value=value)
    ValueCollection       = get_value_collection(value=value, seed=seed)
    value_length          = len(value)
    target_parent_length  = value_length - 1
    longest_parent        = ValueCollection.objects.get_longest_parent(value_id=value_id, min_length=1)
    longest_parent_length = (len(longest_parent.id) // 2)
    
    if (longest_parent_length < target_parent_length):
        # truncate for too long values
        target_value_length = longest_parent_length + 1
        return value[0:target_value_length]
    if (has_seed_value(value=value) is False):
        # no modifications if value length is ok
        return value
    for new_length in range(value_length, 16):
        # extend too short values - load more data from hash space
        new_value = bytes_at_position(position=position, length=new_length, seed=seed, use_bytearray=False, unit=DataUnit.BYTES)
        if (has_seed_value(value=new_value) is False):
            return new_value
    raise Exception(f"Error: get_unique_saving_value() very long value (v={new_length}, l={new_length}) still exists in db")

def save_seed_value(value: bytes, position: int, seed: int=DEFAULT_SEED) -> Union[BaseSeedValue, Document]:
    if (has_basis(value=value) is False):
        init_root_basis(value=value)
    value_length = len(value)
    if (value_length < 2):
        raise Exception(f"Value length must be >= 2 (value={value}, l={len(value)})")
    ValueCollection = get_value_collection(value=value, seed=seed)
    new_value       = get_unique_saving_value(value=value, position=position, seed=seed)
    # create and save new document
    value_length = len(new_value)
    new_item     = ValueCollection(
        id           = bytes_to_value_id(new_value),
        position     = position,
        value_length = value_length
    )
    new_item.save()
    # поскольку в базу всегда сохраняется новое уникальное значение - то нужно обновлять счетчик дочерних элементов
    if (has_parent_value(value=new_value)):
        new_value_parent = get_parent_value(value=new_value)
        update_value_stats(value=value_id_to_bytes(new_value_parent.id))
    # update value position
    position_item = save_seed_value_position(new_item, seed=seed)
    value_stats   = save_seed_value_stats(new_item, seed=seed)
    return new_item

def save_seed_value_position(seed_value: BaseSeedValue, seed: int=DEFAULT_SEED) -> Union[BaseSeedValuePosition, Document]:
    value          = value_id_to_bytes(seed_value.id)
    value_tier     = seed_value.value_length
    value_position = seed_value.position
    if (has_value_position(value=value, position=value_position) is True):
        # update value position tier
        position_item = get_value_position(value=value, position=value_position)
        if (position_item.tier < value_tier):
            position_item.tier = value_tier
            position_item.save()
    else:
        # create new value position
        PositionCollection = get_position_collection(value=value, seed=seed)
        position_item = PositionCollection(
            id   = value_position,
            tier = value_tier,
        )
        position_item.save()
    return position_item

def save_seed_value_stats(seed_value: BaseSeedValue, seed: int=DEFAULT_SEED) -> Union[BaseSeedValuePosition, Document]:
    value = value_id_to_bytes(seed_value.id)
    return update_value_stats(value=value)

def init_seed(seed: int):
    # 1) create tasks:
    # - find and save all root values (min_tier 1, address_length: 1, value_length: 1, value list (256 items): 0x00..0xff)
    # - init each root value, creating new tasks: 
    #   - ProvideRootValueScore(score=1), 
    #   - CollectValueInstances(value_id='0x00', max_tier=2)
    # - set up position segment size
    # - set up segment scan tasks (each segment will contain all values)
    # - set up collection and processing of every scan result
    # - set up final stats calculation for each value
    # - set up final stats calculation for tier
    # 2) run all created tasks:
    # - update db after each task
    # - display final init result
    pass

@dataclass()
class ValueTier:
    """
    Custom set of seed values, defined by position range slice.
    Each tier contains fixed number of values, all values inside single tier have same length and score. 
    Compression process based on search of best tier configuration (each tier must contain maximum number of data values)
    Decompression process uses tier configuration as position/value mapping for data 
    """
    min_position: int
    max_position: int
    position_step: int
    position_range: range
    # first item in tier, numeration based on "position_step" parameter 
    start_item: int
    value_length: int
    target_values: set
    target_values_count: int
    # all values inside given tier
    tier_values: set
    tier_values_count: int


def find_start_tier_location(target_values: Set[str], value_length: int) -> ValueTier:
    pass