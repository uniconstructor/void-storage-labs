# https://github.com/Textualize/rich
from rich import print, inspect
# https://rich.readthedocs.io/en/latest/pretty.html
from rich.pretty import pprint
# https://rich.readthedocs.io/en/latest/traceback.html
from rich.traceback import install
install(show_locals=True)
# https://rich.readthedocs.io/en/latest/logging.html
import logging
from rich.logging import RichHandler
logging.basicConfig(
    level="INFO",
    format="%(message)s",
    datefmt="[%X]",
    handlers=[RichHandler(rich_tracebacks=True, locals_max_length=16)]
)
log = logging.getLogger("rich")
# https://github.com/tqdm/tqdm
import tqdm
# https://realpython.com/python-data-classes/
# https://realpython.com/python-data-classes/#advanced-default-values
from dataclasses import dataclass, make_dataclass, field
# https://bitstring.readthedocs.io/en/latest/index.output_item_idhtml
import bitstring
from bitstring import Bits, BitArray, BitStream, ConstBitStream
# https://pythongeeks.org/ordereddict-in-python/
# https://www.geeksforgeeks.org/counters-in-python-set-1/
# https://docs.python.org/3/library/collections.html#counter-objects
# https://realpython.com/python-namedtuple/
# https://realpython.com/linked-lists-python/
from collections import OrderedDict, Counter, defaultdict, namedtuple, deque
# https://docs.python.org/3/library/typing.html
# https://realpython.com/python-data-classes/#more-flexible-data-classes
from typing import List, Set, Dict, Tuple, Optional, Union, Deque, Literal, Type
# https://www.geeksforgeeks.org/floor-ceil-function-python/?ref=lbp
import math
# https://docs.python.org/3/library/operator.html#module-operator
import operator
# https://habr.com/ru/company/timeweb/blog/564826/
# https://docs.python.org/3/library/enum.html
import enum
from enum import Enum
# https://stackoverflow.com/questions/55212014/python-count-copies-in-dictionary
# https://docs.python.org/3/library/copyreg.html#module-copyreg
# https://docs.python.org/3/library/shelve.html#module-shelve
# https://docs.python.org/3/library/pickle.html#persistence-of-external-objects
import copy, pickle, json
# https://github.com/multiformats/unsigned-varint
# https://github.com/fmoo/python-varint
import varint
# https://more-itertools.readthedocs.io/en/stable/api.html#more_itertools.bucket
from more_itertools import bucket
import os, sys
if not 'workbookDir' in globals():
    workbookDir = os.getcwd()
os.chdir(workbookDir)

from archive.custom_varint_encoding import ItemLengthOptions, \
    encode_hash_item_address, decode_hash_item_address, create_value_length_options
from data_split_utils import DataSplit, SplitType, create_data_split, get_item_length_options
from hash_space_utils import HashItemAddress, HashItemPosition, HashSegmentAddress, PositionSeed, \
    DEFAULT_VALUE_STEP, DEFAULT_POSITION_STEP, HASH_DIGEST_BITS, \
    get_min_bit_length, get_aligned_bit_length, get_varint_bit_length, \
    read_hash_item, read_hash_segment, decode_varint_bytes, \
    find_value_in_segment, split_data

# pointer to value inside encoded bytes
DataPointer = namedtuple('DataPointer', ['start_bit', 'bit_length'])

# multiplier for encoded length options: min_length, max_length, position_step, value_step
DEFAULT_LENGTH_ITEM_UNIT    : int = 8
# length options size in header, after encoding
ENCODED_LENGTH_OPTIONS_SIZE : int = 16

# https://docs.python.org/3/library/enum.html#omitting-values
class NoValue(Enum):
    def __repr__(self):
        return '<%s.%s>' % (self.__class__.__name__, self.name)

# https://bitstring.readthedocs.io/en/latest/constbitstream.html#bitstring.ConstBitStream.readlist
# https://bitstring.readthedocs.io/en/latest/constbitstream.html#bitstring.ConstBitStream.read
# https://docs.python.org/3/library/enum.html#when-to-use-new-vs-init
class EncodedLengthOptionsFormat(bytes, Enum):
    """
    Inital param values, that used in create_value_length_options() to create length_options dict
    Can be indexed by position and by value name:

    >>> EncodedLengthOptionsFormat(3)
    <EncodedLengthOptionsFormat.free_space_bits: 3>
    >>> EncodedLengthOptionsFormat(3).label
    'free_space_bits'
    >>> EncodedLengthOptionsFormat['free_space_bits']
    <EncodedLengthOptionsFormat.free_space_bits: 3>
    >>> EncodedLengthOptionsFormat['free_space_bits'].format
    'uint:3'
    >>> len(EncodedLengthOptionsFormat)
    6
    >>> [value_field.label for value_field in EncodedLengthOptionsFormat]
    ['length_item_unit', 'min_length', 'max_length', 'free_space_bits', 'position_step', 'value_step']
    >>> [value_field.format for value_field in EncodedLengthOptionsFormat]
    ['uint:4', 'uint:2', 'uint:3', 'uint:3', 'uint:2', 'uint:2']
    >>> length_options = EncodedLengthOptions(8, 1, 4, 2, 1, 1)
    >>> length_options
    EncodedLengthOptions(length_item_unit=8, min_length=1, max_length=4, free_space_bits=2, position_step=1, value_step=1)
    >>> [f"{EncodedLengthOptionsFormat(value_number).format}={length_options[value_number]}" for value_number in range(0, len(length_options))]
    ['uint:4=8', 'uint:2=1', 'uint:3=4', 'uint:3=2', 'uint:2=1', 'uint:2=1']
    """
    def __new__(cls, position, label, format):
        obj = bytes.__new__(cls, [position])
        obj._value_ = position
        obj.label   = label
        obj.format  = format
        return obj
    # common divider for:
    # - min_length, 
    # - max_length, 
    # - position_step, 
    # - value_step 
    # (use length_item_unit=8 to keep all values byte-aligned)
    length_item_unit = (0, 'length_item_unit', 'uint:4')
    min_length       = (1, 'min_length', 'uint:2')
    max_length       = (2, 'max_length', 'uint:3')
    free_space_bits  = (3, 'free_space_bits', 'uint:3')
    position_step    = (4, 'position_step', 'uint:2')
    value_step       = (5, 'value_step', 'uint:2')
# named tuple for storing decoded options
EncodedLengthOptions = namedtuple('EncodedLengthOptions', [value_field.label for value_field in EncodedLengthOptionsFormat])

# this Enum describes order of bits inside encoded value
class EncodedDataHeaderFormat(bytes, Enum):
    def __new__(cls, position, label, format):
        obj = bytes.__new__(cls, [position])
        obj._value_ = position
        obj.label   = label
        obj.format  = format
        return obj
    # common length of the prefix for all encoded data items (in bits)
    prefix_length       = (0, 'prefix_length', f"uint:4")
    # length of the last data item that was not encoded and saves "as is" 
    # (used when last data item is too short to be encoded)
    suffix_item_length  = (1, 'items_count', f"uint:4")
    # original data length (in bits)
    data_length         = (2, 'data_length', f"uint:32")
    # total number of encoded items
    items_count         = (3, 'items_count', f"uint:32")
    # length options have a fixed bit size, their structure described above
    length_options      = (4, 'length_options', f"bits:{ENCODED_LENGTH_OPTIONS_SIZE}")
    # seed mapping (item_position->address_seed) saved as list of varints - so we will read and decode them as a stream
    seed_mapping_length = (5, 'seed_mapping_length', f"uint:32")
    # last item in header - value length can be omitted: 
    # https://bitstring.readthedocs.io/en/latest/constbitarray.html#bitstring.Bits.unpack
    seed_mapping        = (6, 'seed_mapping', 'bits')


@dataclass
class DataHeader:
    """Заголовок архива с данными - содержит указатели на диапазоны байт со словарями и сжатыми элементами"""
    
    __slots__ = [
        # указатели на позиции данных
        #'prefix_length_pointer',
        #'length_options_pointer',
        #'item_mapping_pointer',
        #'seed_mapping_pointer',
        #'data_length_pointer',
        #'suffix_item_pointer',
        #'items_count_pointer',

        'prefix_length',
        'length_options',
        #'item_mapping',
        'seed_mapping',
        'data_length',
        'suffix_item',
        'items_count',

        #'encoded_prefix_length',
        #'encoded_length_options',
        #'encoded_item_mapping',
        #'encoded_seed_mapping',
        #'encoded_data_length',
        #'encoded_suffix_item',
        #'encoded_items_count',
    ]

    #prefix_length_pointer  : DataPointer
    #length_options_pointer : DataPointer
    #item_mapping_pointer   : DataPointer
    #seed_mapping_pointer   : DataPointer
    #data_length_pointer    : DataPointer
    #suffix_item_pointer    : DataPointer
    #items_count_pointer    : DataPointer

    prefix_length  : int
    length_options : Dict[str, ItemLengthOptions]
    #item_mapping   : Dict[Bits, HashItemAddress]
    seed_mapping   : Dict[int, int]
    data_length    : int
    suffix_item    : Bits
    items_count    : int
    
    #encoded_prefix_length  : BitArray
    #encoded_length_options : BitArray
    #encoded_item_mapping   : BitArray
    #encoded_seed_mapping   : BitArray
    #encoded_data_length    : BitArray
    #encoded_suffix_item    : BitArray
    #encoded_items_count    : BitArray
    
    def __init__(self, data_split: DataSplit):
        self.prefix_length  = data_split.prefix_length
        self.length_options = data_split.length_options
        #self.item_mapping   = data_split.item_mapping
        self.seed_mapping   = data_split.seed_mapping
        self.data_length    = data_split.data_length
        self.suffix_item    = data_split.suffix_item
        self.items_count    = len(data_split.item_addresses)

        #self.encoded_prefix_length  = None # BitArray(uint=self.prefix_length, length=int(self.prefix_length).bit_length())
        #self.encoded_length_options = None # encode_length_options(self.length_options)
        #self.encoded_item_mapping   = None # encode_item_mapping(self.item_mapping)
        #self.encoded_seed_mapping   = None # encode_seed_mapping(self.seed_mapping)
        #self.encoded_data_length    = None # BitArray(uint=self.data_length, length=int(self.data_length).bit_length())
        #self.encoded_suffix_item    = None # self.suffix_item
        #self.encoded_items_count    = None # BitArray(uint=self.items_count, length=int(self.items_count).bit_length())
        #
        #self.prefix_length_pointer  = None # DataPointer(0, len(self.encoded_prefix_length))
        #self.length_options_pointer = None # DataPointer(get_pointer_end_position(self.prefix_length_pointer), len(self.encoded_length_options))
        #self.item_mapping_pointer   = None # DataPointer(get_pointer_end_position(self.length_options_pointer), len(self.encoded_item_mapping))
        #self.seed_mapping_pointer   = None # DataPointer(get_pointer_end_position(self.item_mapping_pointer), len(self.encoded_seed_mapping))
        #self.data_length_pointer    = None # DataPointer(get_pointer_end_position(self.seed_mapping_pointer), len(self.encoded_data_length))
        #self.suffix_item_pointer    = None # DataPointer(get_pointer_end_position(self.data_length_pointer), len(self.encoded_suffix_item))
        #self.items_count_pointer    = None # DataPointer(get_pointer_end_position(self.suffix_item_pointer), len(self.encoded_items_count))

def get_pointer_end_position(data_pointer: DataPointer) -> int:
    return (data_pointer.start_bit + data_pointer.bit_length)

def pack_enum_data(enum_format: Enum, data_values: tuple):
    """
    >>> length_options = EncodedLengthOptions(8, 1, 4, 2, 1, 1)
    >>> length_options
    EncodedLengthOptions(length_item_unit=8, min_length=1, max_length=4, free_space_bits=2, position_step=1, value_step=1)
    >>> packed_data = pack_enum_data(EncodedLengthOptionsFormat, length_options)
    >>> packed_data
    BitStream('0x8625')
    >>> unpacked_options = unpack_enum_data(EncodedLengthOptionsFormat, EncodedLengthOptions, packed_data)
    >>> unpacked_options
    EncodedLengthOptions(length_item_unit=8, min_length=1, max_length=4, free_space_bits=2, position_step=1, value_step=1)
    >>> length_options == unpacked_options
    True
    """
    # https://bitstring.readthedocs.io/en/latest/functions.html#bitstring.pack
    format = ','.join([f"{enum_format(value_number).format}={data_values[value_number]}" for value_number in range(0, len(enum_format))])
    return bitstring.pack(format)

def unpack_enum_data(enum_format: Type[Enum], values_type: Type[tuple], data_values: Bits) -> tuple:
    """
    >>> length_options = EncodedLengthOptions(8, 1, 4, 2, 1, 1)
    >>> length_options
    EncodedLengthOptions(length_item_unit=8, min_length=1, max_length=4, free_space_bits=2, position_step=1, value_step=1)
    >>> packed_data = pack_enum_data(EncodedLengthOptionsFormat, length_options)
    >>> packed_data
    BitStream('0x8625')
    >>> unpacked_options = unpack_enum_data(EncodedLengthOptionsFormat, EncodedLengthOptions, packed_data)
    >>> unpacked_options
    EncodedLengthOptions(length_item_unit=8, min_length=1, max_length=4, free_space_bits=2, position_step=1, value_step=1)
    >>> length_options == unpacked_options
    True
    """
    # https://bitstring.readthedocs.io/en/latest/constbitarray.html#bitstring.Bits.unpack
    format          = [value_field.format for value_field in enum_format]
    unpacked_values = data_values.unpack(format)
    return values_type(*unpacked_values)

# main encoding functions

def encode_length_options(length_options: Dict[str, ItemLengthOptions]) -> BitArray:
    """
    >>> free_space_bits    = 2
    >>> min_length         = 8
    >>> max_length         = 32
    >>> position_step      = 8
    >>> value_step         = 8
    >>> length_options     = create_value_length_options(min_length, max_length, free_space_bits, position_step, value_step)
    >>> encoded_options    = encode_length_options(length_options)
    >>> encoded_options
    BitStream('0x8625')
    >>> decoded_options    = decode_length_options(encoded_options)
    >>> decoded_options == length_options
    True
    """
    # we don't have to store all length options data - it's enouth to save all params 
    # for create_value_length_options() function and then create all options again with them
    lengths         = list()
    # collect common options
    for item_options in list(length_options.values()):
        lengths.append(item_options.value_length)
        free_space_bits = item_options.space_length
        position_step   = item_options.position_step
        value_step      = item_options.value_step
    min_length    = min(lengths) // DEFAULT_LENGTH_ITEM_UNIT
    max_length    = max(lengths) // DEFAULT_LENGTH_ITEM_UNIT
    position_step = position_step // DEFAULT_LENGTH_ITEM_UNIT
    value_step    = value_step // DEFAULT_LENGTH_ITEM_UNIT
    options_data  = EncodedLengthOptions(
        DEFAULT_LENGTH_ITEM_UNIT,
        min_length,
        max_length,
        free_space_bits,
        position_step,
        value_step
    )
    return pack_enum_data(EncodedLengthOptionsFormat, options_data)

def encode_seeds_dict(data_split: DataSplit) -> List[Bits]:
    """
    >>> free_space_bits    = 2
    >>> min_length         = 8
    >>> max_length         = 32
    >>> position_step      = 8
    >>> value_step         = 8
    >>> length_options     = create_value_length_options(min_length, max_length, free_space_bits, position_step, value_step)
    >>> data               = BitStream(hex='0xdeadbeef0123456789abcdefdeadbeef00112233445566778899aabbccddeeff')
    >>> data_split         = create_data_split(data, length_options)
    >>> data_split.seed_mapping
    {0: 29, 1: 34, 2: 117, 3: 5, 4: 13, 5: 39, 6: 7, 7: 3, 8: 13, 9: 12, 10: 0, 11: 5, 12: 29, 13: 34, 14: 117, 15: 5, 16: 0, 17: 27, 18: 26, 19: 24, 20: 3, 21: 33, 22: 8, 23: 14, 24: 0, 25: 32}
    >>> encoded_seed_dict  = encode_seeds_dict(data_split)
    >>> len(encoded_seed_dict)
    17
    >>> [item.hex for item in encoded_seed_dict.values()]
    ['05', '00', '1d', '22', '75', '0d', '03', '27', '07', '0c', '1b', '1a', '18', '21', '08', '0e', '20']
    >>> [item.uint for item in encoded_seed_dict.values()]
    [5, 0, 29, 34, 117, 13, 3, 39, 7, 12, 27, 26, 24, 33, 8, 14, 32]
    """
    encoded_seeds      = dict()
    seed_counts        = data_split.seed_counts.most_common()
    # TODO: try RLE encoding
    for seed_data in seed_counts:
        # most common seed values will use less space because of variable encoding
        encoded_seeds[seed_data[0]] = Bits(bytes=varint.encode(seed_data[0]))
        #encoded_seeds[seed_data[0]] = Bits(uint=seed_data[0], length=get_min_bit_length(seed_data[0]))
    return encoded_seeds

def encode_seed_mapping(data_split: DataSplit) -> List[Bits]:
    """
    >>> free_space_bits    = 2
    >>> min_length         = 8
    >>> max_length         = 32
    >>> position_step      = 8
    >>> value_step         = 8
    >>> length_options     = create_value_length_options(min_length, max_length, free_space_bits, position_step, value_step)
    >>> data               = BitStream(hex='0xdeadbeef0123456789abcdefdeadbeef00112233445566778899aabbccddeeff')
    >>> data_split         = create_data_split(data, length_options)
    >>> encoded_seed_mapping = encode_seed_mapping(data_split)
    >>> [item.hex for item in encoded_seed_mapping]
    ['1d', '22', '75', '05', '0d', '27', '07', '03', '0d', '0c', '00', '05', '1d', '22', '75', '05', '00', '1b', '1a', '18', '03', '21', '08', '0e', '00', '20']
    >>> [item for item in data_split.seed_mapping.values()]
    [29, 34, 117, 5, 13, 39, 7, 3, 13, 12, 0, 5, 29, 34, 117, 5, 0, 27, 26, 24, 3, 33, 8, 14, 0, 32]
    >>> [item.uint for item in encoded_seed_mapping]
    [29, 34, 117, 5, 13, 39, 7, 3, 13, 12, 0, 5, 29, 34, 117, 5, 0, 27, 26, 24, 3, 33, 8, 14, 0, 32]
    """
    encoded_seed_mapping = BitArray()
    encoded_seeds        = encode_seeds_dict(data_split)
    total_items          = len(data_split.data_items)
    seed_id_length       = get_min_bit_length(total_items)
    #encoded_seed_mapping.join(encoded_seeds)
    
    encoded_positions = list()
    for seed_position in range(0, total_items):
        item_seed  = data_split.seed_mapping[seed_position]
        #seed_id    = Bits(uint=seed_position, length=seed_id_length)
        seed_value = encoded_seeds[item_seed]
        encoded_positions.append(seed_value)
        #encoded_seed_mapping.append(seed_id)
    #seed_mapping_length = len(encoded_seed_mapping)
    #encoded_seed_mapping.append(encoded_seed_mapping)
    # TODO: Enum + namedtuple
    #return encoded_seed_mapping
    return encoded_positions

def create_data_header(data_split: DataSplit) -> DataHeader:
    """
    >>> free_space_bits    = 2
    >>> min_length         = 8
    >>> max_length         = 32
    >>> position_step      = 8
    >>> value_step         = 8
    >>> length_options     = create_value_length_options(min_length, max_length, free_space_bits, position_step, value_step)
    >>> data               = BitStream(hex='0xdeadbeef0123456789abcdefdeadbeef00112233445566778899aabbccddeeff')
    >>> data_split         = create_data_split(data, length_options)
    >>> data_split.prefix_length
    2
    >>> data_header        = create_data_header(data_split)
    >>> data_header.prefix_length == data_split.prefix_length
    True
    >>> data_header.length_options == data_split.length_options
    True
    >>> data_header.seed_mapping == data_split.seed_mapping
    True
    >>> data_header.data_length == data_split.data_length
    True
    >>> data_header.suffix_item == data_split.suffix_item
    True
    >>> data_header.items_count == len(data_split.item_addresses)
    True
    """
    header = DataHeader(data_split)
    # TODO: после сжатия оставить только seed-значения в заголовке 
    #      (вычислять seed из позиции элемента, позиция в seed всегда совпадает с позицией данных)
    # TODO: использовать переменную длину для позиции, длина вычисляется как количество бит позиции и не хранится в адресе
    # TODO: использовать один общий словарь seed-значений (первые элементы используются чаще всего)
    # TODO: обратимое дерево Меркла: создавать 1 новый элемент как замену 2 соседних элементов одним
    # TODO: ссылки на значения: последний бит определяет тип ссылки (1: конечная (на данные), 0: промежуточная (на другую ссылку))
    # TODO: перенести методы для работы с кодированием переменной длины: 
    #       https://gitlab.com/uniconstructor/hash-store-spec/-/blob/main/hashstorage/test-formats.py
    return header

def encode_data_header(data_header: DataSplit) -> BitArray:
    return BitArray(bytes=pickle.dumps(data_header, protocol=pickle.HIGHEST_PROTOCOL))

def encode_data_addresses(data_split: DataSplit) -> List[BitArray]:
    """
    >>> free_space_bits   = 2
    >>> min_length        = 8
    >>> max_length        = 32
    >>> position_step     = 8
    >>> value_step        = 8
    >>> length_options    = create_value_length_options(min_length, max_length, free_space_bits, position_step, value_step)
    >>> data              = BitStream(hex='0xdeadbeef0123456789abcdefdeadbeef00112233445566778899aabbccddeeff')
    >>> data_split        = create_data_split(data, length_options)
    >>> encoded_addresses = encode_data_addresses(data_split)
    >>> len(encoded_addresses)
    26
    >>> len(encoded_addresses) == len(data_split.data_items)
    True
    >>> len(encoded_addresses[0])
    6
    >>> encoded_addresses[0:4]
    [BitArray('0b001000'), BitArray('0b001010'), BitArray('0b000000'), BitArray('0b001110')]
    >>> item_seed = data_split.seed_mapping[0]
    >>> item_seed
    29
    >>> decoded_address = decode_hash_item_address(encoded_addresses[0], length_options, item_seed)
    >>> decoded_address
    HashItemAddress(bit_position=64, bit_length=8, seed=29)
    >>> read_hash_item(decoded_address)
    Bits('0xde')
    >>> read_hash_item(decoded_address) == data_split.data_items[0]
    True
    """
    encoded_items = list()
    for data_item in data_split.data_items:
        item_address = data_split.item_mapping[data_item]
        encoded_item = encode_hash_item_address(item_address, data_split.length_options)
        encoded_items.append(encoded_item)
    return encoded_items

def encode_data(data: ConstBitStream, length_options: Dict[int, ItemLengthOptions]) -> BitStream:
    """
    >>> free_space_bits   = 2
    >>> min_length        = 8
    >>> max_length        = 32
    >>> position_step     = 8
    >>> value_step        = 8
    >>> length_options    = create_value_length_options(min_length, max_length, free_space_bits, position_step, value_step)
    >>> data              = BitStream(hex='0xdeadbeef0123456789abcdefdeadbeef00112233445566778899aabbccddeeff')
    >>> encoded_data      = encode_data(data, length_options)
    >>> len(data)
    256
    >>> #encoded_data[0:64]
    >>> #BitStream('0xb88b01800595ac08')
    >>> encoded_header_length = encoded_data[0:40].tobytes()
    >>> header_length         = varint.decode_bytes(encoded_header_length)
    >>> #header_length
    >>> #17848
    >>> header_offset = get_varint_bit_length(header_length)
    >>> start_header = header_offset
    >>> end_header   = header_offset+header_length
    >>> encoded_header = encoded_data[start_header:end_header]
    >>> decoded_header = decode_data_header(encoded_header)
    >>> # TODO: decoded_header 
    >>> decoded_header.length_options == length_options
    True
    >>> encoded_items = encoded_data[end_header:]
    >>> len(encoded_items)
    196
    >>> encoded_items
    BitStream('0x20a00e3032493cd04e20a00e14e2c41d827651567374a9f42')
    """
    encoded_data  = BitStream()
    data_split    = create_data_split(data, length_options)
    # encoding header
    data_header           = create_data_header(data_split)
    encoded_header        = encode_data_header(data_header)
    header_length         = len(encoded_header)
    encoded_header_length = varint.encode(header_length)
    encoded_header.prepend(encoded_header_length)
    encoded_data.append(encoded_header)
    # encoding data items
    encoded_items = BitArray().join(encode_data_addresses(data_split))
    encoded_data.append(encoded_items)

    return encoded_data

def encode_file(file_name: str, length_options: Dict[int, ItemLengthOptions]) -> BitStream:
    data = ConstBitStream(filename=file_name)
    return encode_data(data, length_options)

###############################
### main decoding functions ###
###############################

def decode_length_options(length_options: BitStream) -> BitArray:
    """
    >>> free_space_bits    = 2
    >>> min_length         = 8
    >>> max_length         = 32
    >>> position_step      = 8
    >>> value_step         = 8
    >>> length_options     = create_value_length_options(min_length, max_length, free_space_bits, position_step, value_step)
    >>> encoded_options    = encode_length_options(length_options)
    >>> encoded_options
    BitStream('0x8625')
    >>> decoded_options    = decode_length_options(encoded_options)
    >>> decoded_options == length_options
    True
    """
    unpacked_options = unpack_enum_data(EncodedLengthOptionsFormat, EncodedLengthOptions, length_options)
    length_item_unit = unpacked_options.length_item_unit
    min_length       = unpacked_options.min_length * length_item_unit
    max_length       = unpacked_options.max_length *length_item_unit     
    free_space_bits  = unpacked_options.free_space_bits 
    position_step    = unpacked_options.position_step * length_item_unit
    value_step       = unpacked_options.value_step * length_item_unit  
    
    return create_value_length_options(min_length, max_length, free_space_bits, position_step, value_step)

def decode_seeds_dict(seeds_dict: BitStream) -> Dict[int, int]:
    """
    >>> free_space_bits    = 2
    >>> min_length         = 8
    >>> max_length         = 32
    >>> position_step      = 8
    >>> value_step         = 8
    >>> length_options     = create_value_length_options(min_length, max_length, free_space_bits, position_step, value_step)
    >>> data               = BitStream(hex='0xdeadbeef0123456789abcdefdeadbeef00112233445566778899aabbccddeeff')
    >>> data_split         = create_data_split(data, length_options)
    >>> data_split.seed_mapping
    {0: 29, 1: 34, 2: 117, 3: 5, 4: 13, 5: 39, 6: 7, 7: 3, 8: 13, 9: 12, 10: 0, 11: 5, 12: 29, 13: 34, 14: 117, 15: 5, 16: 0, 17: 27, 18: 26, 19: 24, 20: 3, 21: 33, 22: 8, 23: 14, 24: 0, 25: 32}
    >>> encoded_seed_dict  = encode_seeds_dict(data_split)
    >>> encoded_dict = [item.hex for item in encoded_seed_dict.values()]
    >>> encoded_dict
    ['05', '00', '1d', '22', '75', '0d', '03', '27', '07', '0c', '1b', '1a', '18', '21', '08', '0e', '20']
    >>> original_dict = [item.uint for item in encoded_seed_dict.values()]
    >>> original_dict
    [5, 0, 29, 34, 117, 13, 3, 39, 7, 12, 27, 26, 24, 33, 8, 14, 32]
    >>> encoded_dict = BitStream(hex=''.join(encoded_dict))
    >>> encoded_dict
    BitStream('0x05001d22750d0327070c1b1a1821080e20')
    >>> decoded_dict = decode_seeds_dict(encoded_dict)
    >>> len(decoded_dict) == len(original_dict)
    True
    >>> [item for item in decoded_dict.values()] == original_dict
    True
    >>> [item for item in decoded_dict.values()]
    [5, 0, 29, 34, 117, 13, 3, 39, 7, 12, 27, 26, 24, 33, 8, 14, 32]
    >>> decoded_dict
    {0: 5, 1: 0, 2: 29, 3: 34, 4: 117, 5: 13, 6: 3, 7: 39, 8: 7, 9: 12, 10: 27, 11: 26, 12: 24, 13: 33, 14: 8, 15: 14, 16: 32}
    """
    decoded_dict    = dict()
    decoded_numbers = decode_varint_bytes(seeds_dict.tobytes())
    position_key    = 0
    for decoded_number in decoded_numbers:
        decoded_dict[position_key] = decoded_number
        position_key += 1
    return decoded_dict

def decode_seed_mapping(position_seeds: BitStream) -> Dict[int, int]:
    """
    >>> free_space_bits    = 2
    >>> min_length         = 8
    >>> max_length         = 32
    >>> position_step      = 8
    >>> value_step         = 8
    >>> length_options     = create_value_length_options(min_length, max_length, free_space_bits, position_step, value_step)
    >>> data               = BitStream(hex='0xdeadbeef0123456789abcdefdeadbeef00112233445566778899aabbccddeeff')
    >>> data_split         = create_data_split(data, length_options)
    >>> encoded_seed_mapping = encode_seed_mapping(data_split)
    >>> [item.hex for item in encoded_seed_mapping]
    ['1d', '22', '75', '05', '0d', '27', '07', '03', '0d', '0c', '00', '05', '1d', '22', '75', '05', '00', '1b', '1a', '18', '03', '21', '08', '0e', '00', '20']
    >>> [item.uint for item in encoded_seed_mapping]
    [29, 34, 117, 5, 13, 39, 7, 3, 13, 12, 0, 5, 29, 34, 117, 5, 0, 27, 26, 24, 3, 33, 8, 14, 0, 32]
    >>> encoded_mapping = BitStream().join(encoded_seed_mapping)
    >>> encoded_mapping
    BitStream('0x1d2275050d2707030d0c00051d227505001b1a180321080e0020')
    >>> decoded_seed_mapping = decode_seed_mapping(encoded_mapping)
    >>> [item for item in decoded_seed_mapping.values()]
    [29, 34, 117, 5, 13, 39, 7, 3, 13, 12, 0, 5, 29, 34, 117, 5, 0, 27, 26, 24, 3, 33, 8, 14, 0, 32]
    >>> [item.uint for item in encoded_seed_mapping] == [item for item in decoded_seed_mapping.values()]
    True
    >>> len(encoded_seed_mapping)
    26
    >>> len(encoded_seed_mapping) == len(decoded_seed_mapping)
    True
    """
    decoded_mapping = dict()
    decoded_numbers = decode_varint_bytes(position_seeds.tobytes())
    position_key    = 0
    for decoded_number in decoded_numbers:
        decoded_mapping[position_key] = decoded_number
        position_key += 1
    return decoded_mapping

def decode_data_header(header: BitStream) -> DataHeader:
    return pickle.loads(header.tobytes())

def get_encoded_address_options(address: Bits, data_split: DataSplit) -> ItemLengthOptions:
    prefix = address[0:data_split.prefix_length]
    return data_split.length_options[prefix.bin]

def split_encoded_addresses(addresses: BitStream, data_split: DataSplit) -> List[Bits]:
    """
    >>> free_space_bits    = 2
    >>> min_length         = 8
    >>> max_length         = 32
    >>> position_step      = 8
    >>> value_step         = 8
    >>> length_options     = create_value_length_options(min_length, max_length, free_space_bits, position_step, value_step)
    >>> data               = BitStream(hex='0xdeadbeef0123456789abcdefdeadbeef00112233445566778899aabbccddeeff')
    >>> data_split         = create_data_split(data, length_options)
    >>> encoded_addresses  = encode_data_addresses(data_split)
    >>> joined_addressed   = BitStream().join(encoded_addresses)
    >>> split_addresses    = split_encoded_addresses(joined_addressed, data_split)
    >>> len(data_split.data_items)
    26
    >>> len(encoded_addresses)
    26
    >>> len(split_addresses)
    26
    >>> len(split_addresses) == len(encoded_addresses) == len(data_split.data_items)
    True
    >>> split_addresses[0] == encoded_addresses[0]
    True
    >>> split_addresses[25] == encoded_addresses[25]
    True
    """
    encoded_addresses = list()
    prefix_length     = data_split.prefix_length
    old_bitpos        = addresses.bitpos
    while (addresses.bitpos < len(addresses)):
        prefix          = addresses.peek(f"bits:{prefix_length}")
        item_options    = data_split.length_options[prefix.bin]
        item_length     = item_options.position_length + prefix_length
        encoded_address = Bits(addresses.read(f"bits:{item_length}"))
        encoded_addresses.append(encoded_address)
    addresses.bitpos = old_bitpos
    return encoded_addresses

def decode_data_addresses(addresses: BitStream, data_split: DataSplit) -> List[HashItemAddress]:
    """
    >>> free_space_bits    = 2
    >>> min_length         = 8
    >>> max_length         = 32
    >>> position_step      = 8
    >>> value_step         = 8
    >>> length_options     = create_value_length_options(min_length, max_length, free_space_bits, position_step, value_step)
    >>> data               = BitStream(hex='0xdeadbeef0123456789abcdefdeadbeef00112233445566778899aabbccddeeff')
    >>> data_split         = create_data_split(data, length_options)
    >>> encoded_addresses  = encode_data_addresses(data_split)
    >>> joined_addresses   = BitStream().join(encoded_addresses)
    >>> len(encoded_addresses)
    26
    >>> len(joined_addresses)
    196
    >>> decoded_addressess = decode_data_addresses(joined_addresses, data_split)
    >>> len(decoded_addressess)
    26
    >>> len(encoded_addresses) == len(decoded_addressess)
    True
    >>> decoded_addressess[0]
    HashItemAddress(bit_position=64, bit_length=8, seed=29)
    >>> data_split.item_addresses[0] == decoded_addressess[0]
    True
    >>> data_split.item_addresses[25] == decoded_addressess[25]
    True
    >>> data_split.data_items[0] == read_hash_item(decoded_addressess[0])
    True
    >>> data_split.data_items[25] == read_hash_item(decoded_addressess[25])
    True
    """
    decoded_addresses = list()
    item_position     = 0
    encoded_addresses = split_encoded_addresses(addresses, data_split)
    for encoded_address in encoded_addresses:
        address_seed    = data_split.seed_mapping[item_position]
        decoded_address = decode_hash_item_address(encoded_address, data_split.length_options, address_seed)
        decoded_addresses.append(decoded_address)
        item_position  += 1
    return decoded_addresses

def decode_data_items(addresses: List[HashItemAddress]) -> List[Bits]:
    """
    >>> free_space_bits    = 2
    >>> min_length         = 8
    >>> max_length         = 32
    >>> position_step      = 8
    >>> value_step         = 8
    >>> length_options     = create_value_length_options(min_length, max_length, free_space_bits, position_step, value_step)
    >>> data               = BitStream(hex='0xdeadbeef0123456789abcdefdeadbeef00112233445566778899aabbccddeeff')
    >>> data_split         = create_data_split(data, length_options)
    >>> decoded_data_items = BitArray().join(decode_data_items(data_split.item_addresses))
    >>> decoded_data_items
    BitArray('0xdeadbeef0123456789abcdefdeadbeef00112233445566778899aabbccddee')
    """
    decoded_items = list()
    for item_address in addresses:
        decoded_items.append(read_hash_item(item_address))
    return decoded_items

def decode_data(data: ConstBitStream) -> BitStream:
    """
    >>> free_space_bits   = 2
    >>> min_length        = 8
    >>> max_length        = 32
    >>> position_step     = 8
    >>> value_step        = 8
    >>> length_options    = create_value_length_options(min_length, max_length, free_space_bits, position_step, value_step)
    >>> data              = BitStream(hex='0xdeadbeef0123456789abcdefdeadbeef00112233445566778899aabbccddeeff')
    >>> encoded_data      = encode_data(data, length_options)
    >>> encoded_header_length = encoded_data[0:40].tobytes()
    >>> header_length         = varint.decode_bytes(encoded_header_length)
    >>> #header_length
    >>> #17848
    >>> header_offset = get_varint_bit_length(header_length)
    >>> start_header = header_offset
    >>> end_header   = header_offset+header_length
    >>> encoded_header = encoded_data[start_header:end_header]
    >>> decoded_header = decode_data_header(encoded_header)
    >>> #decoded_header
    >>> decoded_header.length_options == length_options
    True
    >>> encoded_items = encoded_data[end_header:]
    >>> len(encoded_items)
    196
    >>> encoded_items
    BitStream('0x20a00e3032493cd04e20a00e14e2c41d827651567374a9f42')
    >>> decoded_addresses = decode_data_addresses(encoded_items, decoded_header)
    >>> decoded_data_items = BitArray().join(decode_data_items(decoded_addresses))
    >>> decoded_data_items
    BitArray('0xdeadbeef0123456789abcdefdeadbeef00112233445566778899aabbccddee')
    >>> decoded_data = decode_data(encoded_data)
    >>> decoded_data == data
    True
    """
    decoded_data          = BitStream()
    # decoding header
    encoded_header_length = data[0:40].tobytes()
    header_length         = varint.decode_bytes(encoded_header_length)
    header_offset         = get_varint_bit_length(header_length)
    start_header          = header_offset
    end_header            = header_offset+header_length
    encoded_header        = data[start_header:end_header]
    decoded_header        = decode_data_header(encoded_header)
    # decoding data item addresses
    addresses          = data[end_header:]
    decoded_addresses  = decode_data_addresses(addresses, decoded_header)
    decoded_data_items = BitArray().join(decode_data_items(decoded_addresses))
    if (len(decoded_header.suffix_item) > 0):
        decoded_data_items.append(decoded_header.suffix_item)
    decoded_data.append(decoded_data_items)
    
    return decoded_data

def decode_file(file_name: str) -> BitStream:
    data = ConstBitStream(filename=file_name)
    return decode_data(data)