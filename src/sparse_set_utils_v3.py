# https://stackoverflow.com/questions/38340808/recursive-typing-in-python-3-5
from __future__ import annotations
from rich import print
from rich.pretty import pprint
from custom_counter import CustomCounter as Counter
#from tqdm import tqdm
from bitarray.util import ba2int, int2ba, canonical_huffman, huffman_code
from bitarray import bitarray, frozenbitarray
from collections import defaultdict, namedtuple
from dataclasses import dataclass, field
from copy import deepcopy, copy
from typing import List, Set, Dict, Tuple, Optional, Union, Iterable, Generator, Generic, Optional, Protocol,\
    TypeVar, FrozenSet, Hashable, Sequence
#from functools import lru_cache
#import xxhash
#import math
#from bitarrayset.binaryarrayset import BinaryArraySet
from enum import Enum, IntEnum
from sortedcontainers import SortedSet, SortedDict, SortedList
# pip install git+https://github.com/djbpitt/pyskiplist.git@dumpNodes 
from pyskiplist import SkipList
from fib_encoder import from_c1_bits, to_c1_bits, get_encoded_c1_length, get_encoded_c1_bits
from custom_varint import DEFAULT_PREFIX_LENGTH, encode_varint_number, decode_varint_number

class SkipDict(SkipList):
    """
    Modification of original SkipList: https://github.com/djbpitt/pyskiplist.git
    Duplicate key-value pairs are not allowed
    """

    def has_value(self, value) -> bool:
        return (value in set(self.values()))
    
    def has_key(self, key) -> bool:
        return (key in set(self.keys()))
    
    def has_index(self, index) -> bool:
        return (index < self.__len__())

    def insert(self, key, value):
        """
        Insert a key-value pair in the list.
        Restrict insertion of existing key/value pairs
        """
        if (self.__contains__(key=key) is True):
            current_value = self.search(key=key)
            if (current_value == value):
                # item already in set
                return
            if (current_value is not None):
                raise Exception(f"Cannot add new item: key={key}, value={value}: this key already contains value={current_value}")
        super().insert(key=key, value=value)

@dataclass
class ConsumableRange:
    """
    Поглощаемый (или расходуемый) диапазон целых чисел: диапазон значений из которого каждое число можно использовать только 1 раз
    Поддерживает две отдельные нумерации для использованных чисел и для оставшихся: оба списка чисел нумеруются 
    от меньшего к большему
    
    Восстановление использованных чисел (то есть возврат их обратно в список "неиспользованных") не поддерживается

    Число с id=0 всегда присутствует в списке оставшихся значений (self.numbers.remaining_values) и всегда содержит 0
    
    TODO: попробовать BinaryArraySet для хранения использованных/оставшихся значений и посмотреть будет ли эффективнее
    """
    # список использованных значений: изначально пустой, если не задан
    # диапазон инициализируется этим словарём: значения сортируются по ключу, нумерация должна
    # быть непрерывной и начинаться с 0
    # В каждой паре значений ключ - это порядковый номер добавленного значения, поэтому использованные числа
    # сохраняют свои id при дополнении списка а оставшиеся, наоборот смещают нумерацию всех значений после 
    # удаленного элемента каждый раз когда из remaining_values забирается 1 число
    consumed_values      : SkipDict       = field(default_factory=SkipDict)
    # минимальное количество новых значений, которые находятся в списке remaining_values
    min_remaining_values : int            = field(default=16)
    start_range          : int            = field(default=0)
    end_range            : int            = field(default=None)
    # полный диапазон значений, включающий в себя как использованные так и неиспользованные числе
    value_range          : Sequence[int]  = field(default=None, init=False)
    # список оставшихся чисел (которые еще не были использованы): длина ограничена min_remaining_values 
    # поскольку оставшимися числами мы считаем все натуральные числа, не упомянутые в consumed_values
    remaining_values     : Dict[int, int] = field(default_factory=SortedDict, init=False)

    def __post_init__(self):
        if (self.consumed_values is None):
            self.consumed_values = SkipDict()
        if (self.end_range is None):
            initial_consumed_count  = 0
            if (len(list(self.consumed_values.values())) > 0):
                initial_consumed_values = self.consumed_values.values()
                initial_consumed_count = max(initial_consumed_values)
            self.end_range = initial_consumed_count + self.min_remaining_values + 1
        self.value_range = range(self.start_range, self.end_range)
        self.update_remaining_values()
    
    def get_next_consumed_value_id(self) -> int:
        """
        Получить номер для следующего использованного числа
        """
        return len(self.consumed_values)

    def has_consumed_value(self, value: int) -> bool:
        return self.consumed_values.has_value(value=value)
    
    def has_remaining_value(self, value: int) -> bool:
        return (not self.has_consumed_value(value=value))
    
    def has_consumed_value_id(self, value_id: int) -> bool:
        return self.consumed_values.has_key(key=value_id)
    
    def has_remaining_value_id(self, value_id: int) -> bool:
        return (not self.has_consumed_value_id(value_id=value_id))
    
    def get_consumed_values(self) -> Dict[int, int]:
        return SortedDict(self.consumed_values.items())
    
    def get_remaining_values(self) -> Dict[int, int]:
        remaining_values    = SortedDict()
        # 0 is a required system element: "no_id" marker
        remaining_values[0] = 0
        remaining_value_id  = 1
        consumed_values     = SortedSet(self.consumed_values.values())
        for _, value in enumerate(self.value_range):
            if (value in consumed_values):
                continue
            remaining_values[remaining_value_id] = value
            remaining_value_id += 1
        return remaining_values
    
    def update_remaining_values(self):
        self.remaining_values = self.get_remaining_values()
    
    def get_consumed_value_id(self, value: int) -> Union[int, None]:
        if (self.has_consumed_value(value=value) is False):
            return None
        for consumed_id, consumed_value in self.consumed_values.items(): #self.get_consumed_values().items():
            if (consumed_value == value):
                return consumed_id
        raise Exception(f"consumed value id not found for value={value}")
    
    def get_remaining_value_id(self, value: int) -> Union[int, None]:
        if (self.has_remaining_value(value=value) is False):
            return None
        for remaining_id, remaining_value in self.get_remaining_values().items():
            if (remaining_value == value):
                return remaining_id
        raise Exception(f"remaining value id not found for value={value}")
    
    def consume_value(self, value: int) -> int:
        value_id = self.get_next_consumed_value_id()
        self.consumed_values.insert(key=value_id, value=value)
        self.update_remaining_values()
        return value_id

@dataclass
class ConsumableSet:
    # список изначальных чисел без изменений
    remaining_dict       : SkipDict       = field(default_factory=SkipDict)
    # список использованных значений
    consumed_values      : Dict[int, int] = field(default_factory=SortedDict)
    # список оставшихся чисел (которые еще не были использованы)
    remaining_values     : Dict[int, int] = field(default_factory=SortedDict)
    # автоматически пополнять список оставшихся чисел при использовании нового числа
    # (необходимо чтобы список оставшихся чисел не заканчивался)
    refill_on_consume    : bool           = field(default=False)

    def __post_init__(self):
        self.remaining_dict.insert(key=0, value=0)
        self.consumed_values[0] = 0

    def get_next_consumed_value_id(self) -> int:
        return len(self.consumed_values)
    
    def get_next_remaining_value_id(self) -> int:
        return len(self.remaining_values)
    
    def get_next_remaining_dict_id(self) -> int:
        return len(self.remaining_dict)

    def has_consumed_value(self, value: int) -> bool:
        return (value in self.consumed_values.values())
    
    def has_consumed_value_id(self, value_id: int) -> bool:
        return (value_id <= len(self.consumed_values))
    
    def has_remaining_value(self, value: int) -> bool:
        return (value in self.remaining_values.values())
    
    def has_remaining_value_id(self, value_id: int) -> bool:
        return (value_id <= len(self.remaining_values))
    
    def has_remaining_dict_id(self, value_id: int) -> bool:
        return (value_id <= len(self.remaining_dict))
    
    def get_max_remaining_dict_id(self) -> int:
        next_dict_id    = self.get_next_remaining_dict_id()
        max_dict_id     = next_dict_id - 1
        return max_dict_id
    
    def get_max_remaining_value_id(self) -> int:
        next_value_id    = self.get_next_remaining_value_id()
        max_value_id     = next_value_id - 1
        return max_value_id
    
    def get_max_remaining_value(self) -> int:
        max_remaining_value_id = self.get_next_consumed_value_id()
        self.remaining_values[max_remaining_value_id]
    
    def get_remaining_values(self) -> Dict[int, int]:
        remaining_values    = SortedDict()
        # 0 is a required system element: "no_id" marker
        remaining_values[0] = 0
        remaining_value_id  = 1
        consumed_values     = SortedSet(self.consumed_values.values())
        for _, item in enumerate(self.remaining_dict.items()):
            value = item[1]
            if (value in consumed_values):
                continue
            # print(f"id={remaining_value_id}, value={value}")
            remaining_values[remaining_value_id] = value
            remaining_value_id += 1
        return remaining_values
    
    def update_remaining_values(self) -> Dict[int, int]:
        self.remaining_values = self.get_remaining_values()
        return self.remaining_values
    
    def get_consumed_value_id(self, value: int) -> int:
        if (self.has_consumed_value(value=value) is False):
            raise Exception(f"consumed value id not found for value={value}")
        for consumed_id, consumed_value in self.consumed_values.items():
            if (consumed_value == value):
                return consumed_id
        raise Exception(f"consumed value id not found for value={value}")
    
    def get_remaining_value_id(self, value: int) -> int:
        if (self.has_remaining_value(value=value) is False):
            raise Exception(f"remaining value id not found for value={value}")
        for remaining_id, remaining_value in self.remaining_values.items():
            if (remaining_value == value):
                return remaining_id
        raise Exception(f"remaining value id not found for value={value}")
    
    def add_consumed_value(self, value: int) -> int:
        if (self.has_consumed_value(value=value)):
            raise Exception(f"consumed value={value} already exists")
        value_id = self.get_next_consumed_value_id()
        self.consumed_values[value_id] = value
        return value_id
    
    def add_remaining_value(self, value: int) -> int:
        if (self.has_remaining_value(value=value)):
            raise Exception(f"remaining value={value} already exists")
        if (self.has_consumed_value(value=value)):
            raise Exception(f"remaining value={value} already exists, and has been consumed")
        dict_id  = self.get_next_remaining_dict_id()
        value_id = self.get_next_remaining_value_id()
        self.remaining_dict.insert(key=dict_id, value=value)
        self.update_remaining_values()
        return value_id
    
    def refill_remaining_dict(self, consumed_id: int, consumed_value: int) -> int:
        max_remaining_value = self.get_max_remaining_value()
        if (max_remaining_value == consumed_value):
            return consumed_id
        next_dict_id    = self.get_next_remaining_dict_id()
        max_dict_id     = next_dict_id - 1
        max_dict_value  = self.remaining_dict[max_dict_id]
        next_dict_value = max_dict_value[1] + 1
        self.remaining_dict.insert(key=next_dict_id, value=next_dict_value)
        return next_dict_id
    
    def consume_value(self, value: int) -> int:
        if (self.has_remaining_value(value=value) is False):
            raise Exception(f"remaining value={value} not exists")
        if (self.has_consumed_value(value=value)):
            raise Exception(f"consumed value={value} already exists")
        consumed_id = self.add_consumed_value(value=value)
        # auto-refill remaining value list
        if (self.refill_on_consume):
            self.refill_remaining_dict(consumed_id=consumed_id, consumed_value=value)
        # update remaining values with new refreshed dict
        self.update_remaining_values()
        return consumed_id
    
    def add_remaining_values(self, values: List[int]) -> None:
        unique_values = set(values)
        if (len(unique_values) != len(values)):
            raise Exception(f"provided values is not unique")
        # first loop only perform validation, without data change
        for value in values:
            if (self.has_remaining_value(value=value)):
                raise Exception(f"remaining value={value} already exists")
            if (self.has_consumed_value(value=value)):
                raise Exception(f"remaining value={value} already exists, and has been consumed")
        # second loop only performs insertions on validated data: no rollback needed for incorrect input
        for value in values:
            dict_id = self.get_next_remaining_dict_id()
            self.remaining_dict.insert(key=dict_id, value=value)
        self.update_remaining_values()

SEED_BATCH_SIZE = 32

class SeedType(int, Enum):
    CONSUMED  : int = 0
    REMAINING : int = 1

class SeedIdType(int, Enum):
    # consumed seed id encoded as consumed seed value
    CONSUMED      : int = 0
    # remaining seed id encoded as consumed seed value
    REMAINING     : int = 1
    # consumed seed id encoded as raw number
    CONSUMED_RAW  : int = 2
    # remaining seed id encoded as raw number
    REMAINING_RAW : int = 3

class SeedChangeType(str, Enum):
    SEED_ID      : int = 'id'
    #SEED_VALUE   : int = 1
    SEED_ID_CODE : int = 'code'
    CODE_LENGTH  : int = 'length'

@dataclass
class SeedChangeData:
    change_type : SeedChangeType                   = field()
    old_value   : Union[int, frozenbitarray, None] = field()
    new_value   : Union[int, frozenbitarray, None] = field()

@dataclass
class SeedChange:
    seed_id      : int                  = field()
    seed_id_code : frozenbitarray       = field()
    #seed_value   : int                  = field(default=None)
    changes      : List[SeedChangeData] = field(default_factory=list)

@dataclass
class SeedsTree:
    """
    Дерево Хаффмана со всеми используемыми значениями номеров хеш-пространств (seed)
    Это дерево запоминает и хранит все seed-значения используемые хотя бы 1 раз
    для указания значений всегда используются их id
    """
    seeds                 : ConsumableSet               = field(default_factory=ConsumableSet)
    sorted_seeds          : Set[int]                    = field(default_factory=SortedSet)
    seed_batch_size       : int                         = field(default=SEED_BATCH_SIZE)
    debug                 : bool                        = field(default=True)
    # количество использований id каждого seed-значения 
    # (это важно - мы считаем не количество использований самого значения а количество использований его id)
    id_counts             : Counter                     = field(default_factory=Counter, init=False)
    # коды Хаффмана для id чисел
    id_codes              : Dict[int, bitarray]         = field(default_factory=SortedDict, init=False)
    # reverse code/id mapping
    code_ids              : Dict[bitarray, int]         = field(default_factory=SortedDict, init=False)
    # mapping для определения длины кода для каждого id (ключ - id числа, значение - длина кода для этого id)
    id_code_lengths       : Dict[int, int]              = field(default_factory=SortedDict, init=False)
    # все используемые варианты длины кода id
    code_lengths          : Set[int]                    = field(default_factory=SortedSet, init=False)
    # все коды Хаффмана, сгруппированные по длине кода
    id_codes_by_length    : Dict[int, Set[bitarray]]    = field(default_factory=lambda: defaultdict(SortedSet), init=False)
    # все id значений, сгруппированные по длине кода
    ids_by_length         : Dict[int, Set[int]]         = field(default_factory=lambda: defaultdict(SortedSet), init=False)
    # значение нулевого элемента (специальный маркер всегда равен 0)
    zero_element          : int                         = field(default=0, init=False)
    # значение id нулевого элемента (специальный маркер,всегда равен 0)
    zero_element_id       : int                         = field(default=0, init=False)
    # код id нулевого элемента (может меняться по мере роста дерева и добавления новых чисел)
    zero_element_code     : bitarray                    = field(default=None, init=False)
    # длина кода id нулевого элемента (в битах)
    zero_element_length   : int                         = field(default=1, init=False)
    # счетчик количества кодов разной длины в дереве: обнуляется и пересчитывается при каждом перестроении дерева
    # поэтому считает количество элементов каждой длинны в текущем дереве а не общее количество использования 
    # всех элементов каждой длины
    code_length_counts    : Counter                     = field(default_factory=Counter, init=False)
    # флаг определяющий завершено ли перестроение дерева после добавления последнего элемента
    changes_committed     : bool                        = field(default=True, init=False)
    # порядок чтения/записи бит при генерации кода Хаффмана и кода Фибоначчи (для класса bitarray)
    default_endian        : str                         = field(default='big', init=False, repr=False)
    # количество префиксных бит при указании длины числа в varint-формате
    varint_prefix_length  : int                         = field(default=DEFAULT_PREFIX_LENGTH, init=False, repr=False)

    def __post_init__(self):
        # заполняем изначальный набор возможных значений
        self.seeds   = ConsumableSet(refill_on_consume=True)
        start_batch  = 1
        end_batch    = start_batch + self.seed_batch_size
        init_seeds   = list(range(start_batch, end_batch))
        self.seeds.add_remaining_values(values=init_seeds)
        # создаём стартовое дерево Хаффмана
        self.id_counts.update({ self.zero_element_id: 0 })
        self.update_id_codes()
    
    def has_consumed_seed(self, seed: int) -> bool:
        return self.seeds.has_consumed_value(value=seed)
    
    def has_consumed_seed_id(self, seed_id: int) -> bool:
        return self.seeds.has_consumed_value_id(value_id=seed_id)
    
    def get_consumed_seed(self, seed_id: int) -> int:
        return self.seeds.consumed_values[seed_id]

    def get_consumed_seed_id(self, seed: int) -> int:
        seed_id = self.seeds.get_consumed_value_id(value=seed)
        if (seed_id is None):
            raise Exception(f"seed={seed} is not registered (seed_id=None)")
        return seed_id
    
    def has_remaining_seed(self, seed: int) -> bool:
        return self.seeds.has_remaining_value(value=seed)
    
    def has_remaining_seed_id(self, seed_id: int) -> bool:
        return self.seeds.has_remaining_value_id(value_id=seed_id)
    
    def get_remaining_seed(self, seed_id: int) -> int:
        return self.seeds.remaining_values[seed_id]
    
    def get_remaining_seed_id(self, seed: int) -> int:
        return self.seeds.get_remaining_value_id(value=seed)
    
    def get_seed_id(self, seed: int) -> int:
        if (self.has_consumed_seed(seed=seed) is True):
            return self.get_consumed_seed_id(seed=seed)
        elif (self.has_remaining_seed(seed=seed) is True):
            return self.get_remaining_seed_id(seed=seed)
        else:
            raise Exception(f"cannot get seed_id: seed={seed} is not registered")
    
    def get_seed(self, seed_id: int, seed_type: SeedType) -> int:
        if (seed_type == SeedType.CONSUMED):
            if (self.has_consumed_seed_id(seed_id=seed_id) is False):
                raise Exception(f"No consumed seed_id={seed_id}")
            return self.get_consumed_seed(seed_id=seed_id)
        if (seed_type == SeedType.REMAINING):
            if (self.has_remaining_seed_id(seed_id=seed_id) is False):
                raise Exception(f"No remaining seed_id={seed_id}")
            return self.get_remaining_seed(seed_id=seed_id)
        else:
            raise Exception(f"Unknown seed_type={seed_type}")
    
    def get_seed_id_code(self, seed_id: int) -> frozenbitarray:
        if (self.has_consumed_seed_id(seed_id=seed_id) is False):
            raise Exception(f"No Huffman code for seed_id={seed_id}")
        seed_code = frozenbitarray(self.id_codes[seed_id])
        return seed_code.copy()
    
    def get_seed_code_id(self, seed_code: bitarray) -> int:
        seed_code = frozenbitarray(seed_code)
        if (seed_code not in self.code_ids):
            raise Exception(f"No seed_id Huffman code: seed_code={seed_code}")
        seed_id = self.code_ids[seed_code]
        if (self.has_consumed_seed_id(seed_id=seed_id) is False):
            raise Exception(f"No Huffman code for seed_id={seed_id}")
        return seed_id
    
    def get_zero_element_code(self) -> bitarray:
        return self.id_codes[self.zero_element_id]
    
    def update_id_codes(self) -> List[SeedChange]:
        """
        Rebuild Huffman tree and update seed_id codes
        """
        # saving old tree metadata to detect diffs
        old_id_codes        = self.id_codes.copy()
        old_id_code_lengths = self.id_code_lengths.copy()
        old_ids             = set(old_id_codes.keys())
        # clearing old tree metadata
        self.id_codes.clear()
        self.code_ids.clear()
        self.id_code_lengths.clear()
        self.code_lengths.clear()
        self.code_length_counts.clear()
        self.id_codes_by_length.clear()
        self.ids_by_length.clear()
        # main update: rebuild Huffman tree
        self.id_codes = SortedDict(huffman_code(self.id_counts, endian=self.default_endian).items())
        
        # preparing update list
        updates = list()
        changes = list()
        # all other updates derived from new Huffman tree
        for seed_id, seed_id_code in self.id_codes.items():
            seed_id_code                  = frozenbitarray(seed_id_code)
            code_length                   = len(seed_id_code)
            self.code_ids[seed_id_code]   = seed_id
            self.id_code_lengths[seed_id] = code_length
            self.code_lengths.add(code_length)
            self.code_length_counts.update({ code_length : 1 })
            self.id_codes_by_length[code_length].add(seed_id_code)
            self.ids_by_length[code_length].add(seed_id)
            # detect changes
            changes = list()
            if (seed_id not in old_ids):
                #print(f"seed_id={seed_id}, old_ids={old_ids}")
                # new seed_id added
                changes.append(SeedChangeData(
                    change_type = SeedChangeType.SEED_ID,
                    old_value   = None,
                    new_value   = seed_id,
                ))
                changes.append(SeedChangeData(
                    change_type = SeedChangeType.SEED_ID_CODE,
                    old_value   = None,
                    new_value   = seed_id_code,
                ))
                changes.append(SeedChangeData(
                    change_type = SeedChangeType.CODE_LENGTH,
                    old_value   = None,
                    new_value   = code_length,
                ))
            else:
                # existing seed_id updated
                old_seed_id_code = old_id_codes[seed_id]
                if (old_seed_id_code != seed_id_code):
                    changes.append(SeedChangeData(
                        change_type = SeedChangeType.SEED_ID_CODE,
                        old_value   = frozenbitarray(old_seed_id_code),
                        new_value   = seed_id_code,
                    ))
                old_code_length = old_id_code_lengths[seed_id]
                if (old_code_length != code_length):
                    changes.append(SeedChangeData(
                        change_type = SeedChangeType.CODE_LENGTH,
                        old_value   = old_code_length,
                        new_value   = code_length,
                    ))
            # update seed_id if we have at least one change
            if (len(changes) > 0):
                updates.append(SeedChange(
                    seed_id      = seed_id,
                    seed_id_code = seed_id_code,
                    changes      = changes.copy(),
                ))
        # update zero element 
        self.zero_element_code   = self.get_zero_element_code()
        self.zero_element_length = len(self.get_zero_element_code())
        
        # return all performed updates
        return updates
    
    def get_next_code_length(self, seed_id: int=None) -> int:
        if (seed_id is None):
            seed_id = self.seeds.get_next_consumed_value_id()
        next_id_counts = self.id_counts.copy()
        next_id_counts.update({ self.zero_element_id : 1 })
        next_id_counts.update({ seed_id : 1 })
        next_codes = SortedDict(huffman_code(next_id_counts, endian=self.default_endian).items())
        return len(next_codes[seed_id])
    
    def get_code_length(self, seed_id: int) -> int:
        if (seed_id in self.seeds.has_consumed_value_id(value_id=seed_id)):
            id_code_length = self.id_code_lengths[seed_id]
        else:
            id_code_length = self.get_next_code_length()
        return id_code_length
    
    def get_min_code_length(self) -> int:
        return min(self.code_lengths)
    
    def get_max_code_length(self) -> int:
        return max(self.code_lengths)
    
    def get_next_seed_id_value(self) -> int:
        return len(self.sorted_seeds) + 1
    
    def absorb_zero_element_id(self, times: int=1) -> None:
        self.id_counts.update({ self.zero_element_id : times })
    
    def absorb_seed_id(self, seed_id: int) -> None:
        if (self.has_consumed_seed(seed=seed_id) is False):
            raise Exception(f"No Huffman code for seed_id={seed_id}")
        self.id_counts.update({ seed_id : 1 })
    
    def absorb_raw_seed(self, seed: int) -> int:
        if (self.has_consumed_seed(seed=seed) is True):
            consumed_seed_id = self.get_consumed_seed_id(seed=seed)
            self.id_counts.update({ consumed_seed_id: 1 })
            return consumed_seed_id
        if (seed == self.zero_element):
            raise Exception(f"cannot register new seed={seed} equals to self.zero_element={seed}")
        consumed_seed_id = self.seeds.consume_value(value=seed)
        self.sorted_seeds.add(seed)
        self.id_counts.update({ consumed_seed_id: 1 })
        return consumed_seed_id
    
    def get_seed_type(self, seed: int) -> SeedType:
        if (self.has_consumed_seed(seed=seed) is True):
            return SeedType.CONSUMED
        if (self.has_remaining_seed(seed=seed) is True):
            return SeedType.REMAINING
        raise Exception(f"unknown type for seed={seed}")
    
    def get_seed_type_from_id_type(self, seed_id_type: SeedIdType) -> SeedType:
        if (seed_id_type == SeedIdType.CONSUMED) or (seed_id_type == SeedIdType.CONSUMED_RAW):
            return SeedType.CONSUMED
        if (seed_id_type == SeedIdType.REMAINING) or (seed_id_type == SeedIdType.REMAINING_RAW):
            return SeedType.REMAINING
        raise Exception(f"unknown seed_id_type={seed_id_type}")
    
    def get_seed_id_type(self, seed_type: SeedType, seed: int) -> SeedIdType:
        if (seed_type == SeedType.CONSUMED):
            seed_id = self.get_consumed_seed_id(seed=seed)
            #id_type = self.get_seed_type(seed=seed_id)
            if (self.has_consumed_seed(seed=seed_id)):
                return SeedIdType.CONSUMED
            if (self.has_remaining_seed(seed=seed_id)):
                return SeedIdType.CONSUMED_RAW
            else:
                raise Exception(f"unknown type for seed_id (seed={seed}, id={seed_id})")
        if (seed_type == SeedType.REMAINING):
            seed_id = self.get_remaining_seed_id(seed=seed)
            #id_type = self.get_seed_type(seed=seed_id)
            if (self.has_consumed_seed(seed=seed_id)):
                return SeedIdType.REMAINING
            if (self.has_remaining_seed(seed=seed_id)):
                return SeedIdType.REMAINING_RAW
            else:
                raise Exception(f"unknown type for seed_id (seed={seed}, id={seed_id})")
        else:
            raise Exception(f"unknown type for seed={seed}")
    
    def get_seed_prefix(self, seed_id_type: SeedIdType) -> bitarray:
        if (seed_id_type == SeedIdType.CONSUMED):
            return bitarray('', endian=self.default_endian).copy()
        elif (seed_id_type == SeedIdType.REMAINING):
            return self.get_zero_element_code().copy()
        elif (seed_id_type == SeedIdType.CONSUMED_RAW):
            return (self.get_zero_element_code().copy() * 2).copy()
        elif (seed_id_type == SeedIdType.REMAINING_RAW):
            return (self.get_zero_element_code().copy() * 3).copy()
        else:
            raise Exception(f"unknown seed_id_type={seed_id_type}")
    
    def get_prefix_length_by_seed_id_type(self, seed_id_type: SeedIdType) -> int:
        if (seed_id_type == SeedIdType.CONSUMED):
            return self.zero_element_length * seed_id_type
        elif (seed_id_type == SeedIdType.REMAINING):
            return self.zero_element_length * seed_id_type
        elif (seed_id_type == SeedIdType.CONSUMED_RAW):
            return self.zero_element_length * seed_id_type
        elif (seed_id_type == SeedIdType.REMAINING_RAW):
            return self.zero_element_length * seed_id_type
        else:
            raise Exception(f"unknown seed_id_type={seed_id_type}")
    
    def absorb_seed(self, seed: int) -> bitarray:
        if (self.changes_committed is False):
            raise Exception(f"Changes not committed to tree: cannot process new value. You need to call self.commit_changes() for that")
        # encode seed with current tree state
        encoded_seed = self.encode_seed(seed=seed)
        # define params to update tree
        seed_type    = self.get_seed_type(seed=seed)
        seed_id      = self.get_seed_id(seed=seed)
        seed_id_type = self.get_seed_id_type(seed_type=seed_type, seed=seed)
        
        # prepare tree updates
        if (seed_id_type == SeedIdType.CONSUMED):
            self.absorb_seed_id(seed_id=seed_id)
        elif (seed_id_type == SeedIdType.REMAINING):
            self.absorb_zero_element_id(times=1)
            new_seed_id = self.absorb_raw_seed(seed=seed)
            self.absorb_seed_id(seed_id=seed_id)
            if (self.has_consumed_seed(seed=new_seed_id) is False):
                self.absorb_raw_seed(seed=new_seed_id)
        elif (seed_id_type == SeedIdType.CONSUMED_RAW):
            self.absorb_zero_element_id(times=2)
            new_seed_id  = self.absorb_raw_seed(seed=seed)
            new_value_id = self.absorb_raw_seed(seed=seed_id)
            if (self.has_consumed_seed(seed=new_seed_id) is False):
                self.absorb_raw_seed(seed=new_seed_id)
            if (self.has_consumed_seed(seed=new_value_id) is False):
                self.absorb_raw_seed(seed=new_value_id)
        elif (seed_id_type == SeedIdType.REMAINING_RAW):
            self.absorb_zero_element_id(times=3)
            new_seed_id  = self.absorb_raw_seed(seed=seed)
            new_value_id = self.absorb_raw_seed(seed=seed_id)
            if (self.has_consumed_seed(seed=new_seed_id) is False):
                self.absorb_raw_seed(seed=new_seed_id)
            if (self.has_consumed_seed(seed=new_value_id) is False):
                self.absorb_raw_seed(seed=new_value_id)
        else:
            raise Exception(f"unknown seed_id_type={seed_id_type}")
        # mark changes uncommitted
        self.changes_committed = False

        return encoded_seed
    
    def commit_changes(self) -> List[SeedChange]:
        if (self.changes_committed is True):
            return []
        # update Huffman tree only when encoding completed: this is required to keep encoding and decoding
        # trees identical on each step
        updates                = self.update_id_codes()
        self.changes_committed = True
        # return all updates for the tree
        return updates
    
    def encode_raw_seed(self, seed: int) -> bitarray:
        return encode_varint_number(number=seed).copy()
    
    def encode_seed_prefix(self, seed_id_type: SeedIdType) -> bitarray:
        return self.get_seed_prefix(seed_id_type=seed_id_type).copy()

    def encode_seed_id(self, seed_id: int, seed_id_type: SeedIdType) -> bitarray:
        # encoding seed id value 
        if (seed_id_type == SeedIdType.CONSUMED):
            return self.get_seed_id_code(seed_id=seed_id).copy()
        elif (seed_id_type == SeedIdType.REMAINING):
            return self.get_seed_id_code(seed_id=seed_id).copy()
        elif (seed_id_type == SeedIdType.CONSUMED_RAW):
            return self.encode_raw_seed(seed=seed_id).copy()
        elif (seed_id_type == SeedIdType.REMAINING_RAW):
            return self.encode_raw_seed(seed=seed_id).copy()
        else:
            raise Exception(f"unknown seed_id_type={seed_id_type}")

    def encode_seed(self, seed: int) -> bitarray:
        seed_type    = self.get_seed_type(seed=seed)
        seed_id      = self.get_seed_id(seed=seed)
        seed_id_type = self.get_seed_id_type(seed_type=seed_type, seed=seed)
        # encoding seed id type inside prefix
        encoded_seed_prefix = self.encode_seed_prefix(seed_id_type=seed_id_type)
        # encoding seed id depending on it's type
        encoded_seed_id     = self.encode_seed_id(seed_id=seed_id, seed_id_type=seed_id_type)
        encoded_seed        = encoded_seed_prefix + encoded_seed_id
        # decode seed with same SeedTree state and check the result (for testing)
        if (self.debug is True):
            decoded_seed = self.decode_seed(encoded_seed=encoded_seed)
            assert(decoded_seed == seed)
        return encoded_seed
    
    def decode_seed(self, encoded_seed: bitarray) -> int:
        seed_id_type  = self.decode_seed_prefix(encoded_seed=encoded_seed)
        seed_type     = self.get_seed_type_from_id_type(seed_id_type=seed_id_type)
        seed_id       = self.decode_seed_id(encoded_seed=encoded_seed, seed_id_type=seed_id_type)
        seed          = self.get_seed(seed_id=seed_id, seed_type=seed_type)
        return seed

    def decode_seed_prefix(self, encoded_seed: bitarray) -> int:
        prefix_chunk_length = self.zero_element_length
        seed_id_type        = NounceIdType.CONSUMED
        prefix_chunks = {
            SeedIdType.REMAINING     : encoded_seed[0:prefix_chunk_length],
            SeedIdType.CONSUMED_RAW  : encoded_seed[prefix_chunk_length:prefix_chunk_length*2],
            SeedIdType.REMAINING_RAW : encoded_seed[prefix_chunk_length*2:prefix_chunk_length*3]
        }
        for new_seed_id_type, chunk_bits in prefix_chunks.items():
            if (chunk_bits != self.zero_element_code):
                return seed_id_type
            seed_id_type = new_seed_id_type
        return seed_id_type

    def decode_seed_id(self, encoded_seed: bitarray, seed_id_type: SeedIdType) -> int:
        prefix_length = self.get_prefix_length_by_seed_id_type(seed_id_type=seed_id_type)
        seed_id_bits  = encoded_seed[prefix_length:len(encoded_seed)]
        if (seed_id_type == SeedIdType.CONSUMED) or (seed_id_type == SeedIdType.REMAINING):
            return self.decode_huffman_seed_id(encoded_seed_id_bits=seed_id_bits)
        if (seed_id_type == SeedIdType.CONSUMED_RAW) or (seed_id_type == SeedIdType.REMAINING_RAW):
            return self.decode_raw_seed_id(raw_seed_id_bits=seed_id_bits)
        raise Exception(f"unknown seed_id_type={seed_id_type}")
    
    def decode_huffman_seed_id(self, encoded_seed_id_bits: bitarray) -> int:
        for code_length in self.code_lengths:
            encoded_bits = encoded_seed_id_bits[0:code_length]
            length_codes = self.id_codes_by_length[code_length]
            for seed_code in length_codes:
                #print(f"l={code_length}, seed_code={seed_code}, bits={encoded_bits}")
                if (seed_code == encoded_bits):
                    seed_id = self.code_ids[seed_code]
                    return seed_id
        raise Exception(f"No Huffman code for encoded_seed_id_bits={encoded_seed_id_bits}")

    def decode_raw_seed_id(self, raw_seed_id_bits: bitarray) -> int:
        return decode_varint_number(encoded_number=raw_seed_id_bits, prefix_length=self.varint_prefix_length)

class NounceType(int, Enum):
    CONSUMED  : int = 0
    REMAINING : int = 1

class NounceIdType(int, Enum):
    # consumed nounce id encoded as consumed nounce value
    CONSUMED           : int = 0
    # remaining nounce id encoded as consumed nounce value (remaining nounces are created from consumed seeds)
    REMAINING          : int = 1
    # consumed nounce id encoded as seed value (seed value encoded as seed id)
    CONSUMED_RAW      : int = 2
    # remaining nounce id encoded as seed value (seed value encoded as seed id)
    REMAINING_RAW     : int = 3

class NounceChangeType(str, Enum):
    NOUNCE_ID      : int = 'id'
    NOUNCE_ID_CODE : int = 'code'
    CODE_LENGTH    : int = 'length'

@dataclass
class NounceChangeData:
    change_type : NounceChangeType                 = field()
    old_value   : Union[int, frozenbitarray, None] = field()
    new_value   : Union[int, frozenbitarray, None] = field()

@dataclass
class NounceChange:
    nounce_id      : int                    = field()
    nounce_id_code : frozenbitarray         = field()
    changes        : List[NounceChangeData] = field(default_factory=list)

@dataclass
class SeedNouncesTree:
    """
    Дерево Хаффмана со всеми используемыми значениями хеша (nounce) одного хеш-пространств (seed)
    Это дерево запоминает и хранит все nounce-значения используемые хотя бы 1 раз внутри одного хеш-пространства
    Для указания nounce-значений всегда используются их id
    """
    # ссылка на глобальное дерево с id всех хеш-пространств
    seeds_tree            : SeedsTree                   = field(default=None, repr=False)
    # id номера хеш-пространства в котором находится это дерево
    seed_id               : int                         = field(default=None)
    # текущий актуальный код id хеш-пространства
    seed_id_code          : frozenbitarray              = field(default=None)
    # номер хеш-пространства в котором находится это дерево
    seed                  : int                         = field(default=None)
    debug                 : bool                        = field(default=True)
    
    # все nounce-значения используемые в этом хеш-пространстве
    nounces               : ConsumableSet               = field(default_factory=ConsumableSet)
    sorted_nounces        : Set[int]                    = field(default_factory=SortedSet)

    # количество использований id каждого nounce-значения 
    # (это важно - мы считаем не количество использований самого значения а количество использований его id)
    id_counts             : Counter                     = field(default_factory=Counter)
    # коды Хаффмана для id чисел
    id_codes              : Dict[int, bitarray]         = field(default_factory=SortedDict)
    # reverse code/id mapping
    code_ids              : Dict[bitarray, int]         = field(default_factory=SortedDict)
    # mapping для определения длины кода для каждого id (ключ - id числа, значение - длина кода для этого id)
    id_code_lengths       : Dict[int, int]              = field(default_factory=SortedDict)
    # все используемые варианты длины кода id
    code_lengths          : Set[int]                    = field(default_factory=SortedSet)
    # все коды Хаффмана, сгруппированные по длине кода
    id_codes_by_length    : Dict[int, Set[bitarray]]    = field(default_factory=lambda: defaultdict(SortedSet))
    # все id значений, сгруппированные по длине кода
    ids_by_length         : Dict[int, Set[int]]         = field(default_factory=lambda: defaultdict(SortedSet))
    # значение нулевого элемента (специальный маркер всегда равен 0)
    zero_element          : int                         = field(default=0)
    # значение id нулевого элемента (специальный маркер,всегда равен 0)
    zero_element_id       : int                         = field(default=0)
    # код id нулевого элемента (может меняться по мере роста дерева и добавления новых чисел)
    zero_element_code     : bitarray                    = field(default=None)
    # длина кода id нулевого элемента (в битах)
    zero_element_length   : int                         = field(default=1)
    # счетчик количества кодов разной длины в дереве: обнуляется и пересчитывается при каждом перестроении дерева
    # поэтому считает количество элементов каждой длинны в текущем дереве а не общее количество использования 
    # всех элементов каждой длины
    code_length_counts    : Counter                     = field(default_factory=Counter)
    # флаг определяющий завершено ли перестроение дерева после добавления последнего элемента
    changes_committed     : bool                        = field(default=True)
    # порядок чтения/записи бит при генерации кода Хаффмана и кода Фибоначчи (для класса bitarray)
    default_endian        : str                         = field(default='big', repr=False)
    # количество префиксных бит при указании длины числа в varint-формате
    varint_prefix_length  : int                         = field(default=DEFAULT_PREFIX_LENGTH, init=False, repr=False)

    #def __init__(self, seeds_tree: SeedsTree, seed_id: int):
    #    self.seeds_tree    = seeds_tree
    #    self.seed_id       = seed_id
    #    self.seed_value    = self.seeds_tree.get_consumed_seed(seed_id=seed_id)
    #    seed_id_code       = self.seeds_tree.get_seed_id_code(seed_id=seed_id)
    #    self.update_seed_id_code(seed_id_code=seed_id_code)
    #
    #    # заполняем изначальный набор возможных значений
    #    self.nounces        = ConsumableSet()
    #    self.sorted_nounces = SortedSet()
    #    # заполняем изначальный набор возможных значений
    #    self.sync_remaining_values(seeds_tree=self.seeds_tree)
    #    # создаём стартовое дерево Хаффмана
    #    #self.id_counts.update({ self.zero_element_id: 0 })
    #    #self.update_id_codes()

    def __post_init__(self):
        # заполняем изначальный набор возможных значений
        self.nounces       = ConsumableSet(refill_on_consume=False)
        # заполняем изначальный набор возможных значений
        #self.sync_remaining_values(seeds_tree=self.seeds_tree)
        # создаём стартовое дерево Хаффмана
        self.id_counts.update({ self.zero_element_id: 0 })
        self.update_id_codes()
    
    def sync_remaining_values(self, seeds_tree: SeedsTree=None) -> List[int]:
        if (seeds_tree is not None):
            self.seeds_tree = seeds_tree
        total_seeds   = len(self.seeds_tree.seeds.consumed_values)
        total_nounces = len(self.nounces.remaining_dict)
        new_seeds     = total_seeds - total_nounces
        if (new_seeds <= 0):
            return []
        # add new available nounces from seeds
        new_nounces = list()
        start_seed  = total_nounces
        end_seed    = total_seeds
        for seed_id in range(start_seed, end_seed):
            seed = self.seeds_tree.get_consumed_seed(seed_id=seed_id)
            new_nounces.append(seed)
        self.nounces.add_remaining_values(values=new_nounces)
        self.nounces.update_remaining_values()
        return new_nounces
    
    def set_seeds_tree(self, seeds_tree: SeedsTree):
        #if (self.seeds_tree is not None):
        #    raise Exception(f"seeds_tree already set")
        self.sync_remaining_values(seeds_tree=seeds_tree)
        self.update_id_codes()

    def set_seed(self, seed: int):
        if (self.seeds_tree.has_consumed_seed(seed=seed) is False):
            raise Exception(f"seed={seed} is not registered")
        seed_id = self.seeds_tree.get_consumed_seed_id(seed=seed)
        return self.set_seed_id(seed_id=seed_id)

    def set_seed_id(self, seed_id: int):
        if (self.seed_id is not None):
            raise Exception(f"seed_id already set (self.seed_id={self.seed_id})")
        if (seed_id == self.seeds_tree.zero_element):
            raise Exception(f"seed_id={seed_id} cannot be zero element")
        if (self.seeds_tree.has_consumed_seed_id(seed_id=seed_id) is False):
            raise Exception(f"seed_id={seed_id} is not registered")
        self.seed_id = seed_id
        self.seed    = self.seeds_tree.get_consumed_seed(seed_id=seed_id)
        seed_id_code = self.seeds_tree.get_seed_id_code(seed_id=seed_id)
        self.set_seed_id_code(seed_id_code=seed_id_code)
    
    def set_seed_id_code(self, seed_id_code: bitarray):
        self.seed_id_code = frozenbitarray(seed_id_code)
    
    ### nounce tree logic ###
    
    def has_consumed_nounce(self, nounce: int) -> bool:
        return self.nounces.has_consumed_value(value=nounce)
    
    def has_consumed_nounce_id(self, nounce_id: int) -> bool:
        return self.nounces.has_consumed_value_id(value_id=nounce_id)
    
    def get_consumed_nounce(self, nounce_id: int) -> int:
        return self.nounces.consumed_values[nounce_id]

    def get_consumed_nounce_id(self, nounce: int) -> int:
        nounce_id = self.nounces.get_consumed_value_id(value=nounce)
        if (nounce_id is None):
            raise Exception(f"nounce={nounce} is not registered (nounce_id=None)")
        return nounce_id
    
    def has_remaining_nounce(self, nounce: int) -> bool:
        return self.nounces.has_remaining_value(value=nounce)
    
    def has_remaining_nounce_id(self, nounce_id: int) -> bool:
        return self.nounces.has_remaining_value_id(value_id=nounce_id)
    
    def get_remaining_nounce(self, nounce_id: int) -> int:
        return self.nounces.remaining_values[nounce_id]
    
    def get_remaining_nounce_id(self, nounce: int) -> int:
        return self.nounces.get_remaining_value_id(value=nounce)
    
    def get_nounce_id(self, nounce: int) -> int:
        if (self.has_consumed_nounce(nounce=nounce) is True):
            return self.get_consumed_nounce_id(nounce=nounce)
        elif (self.has_remaining_nounce(nounce=nounce) is True):
            return self.get_remaining_nounce_id(nounce=nounce)
        else:
            raise Exception(f"cannot get nounce_id: nounce={nounce} is not registered")
    
    def get_nounce(self, nounce_id: int, nounce_type: NounceType) -> int:
        if (nounce_type == NounceType.CONSUMED):
            if (self.has_consumed_nounce_id(nounce_id=nounce_id) is False):
                raise Exception(f"No consumed nounce_id={nounce_id}")
            return self.get_consumed_nounce(nounce_id=nounce_id)
        if (nounce_type == NounceType.REMAINING):
            if (self.has_remaining_nounce_id(nounce_id=nounce_id) is False):
                raise Exception(f"No remaining nounce_id={nounce_id}")
            return self.get_remaining_nounce(nounce_id=nounce_id)
        else:
            raise Exception(f"Unknown nounce_type={nounce_type}")
    
    def get_nounce_id_code(self, nounce_id: int) -> frozenbitarray:
        if (self.has_consumed_nounce_id(nounce_id=nounce_id) is False):
            raise Exception(f"No Huffman code for nounce_id={nounce_id}")
        nounce_code = frozenbitarray(self.id_codes[nounce_id])
        return nounce_code
    
    def get_nounce_code_id(self, nounce_code: bitarray) -> int:
        nounce_code = frozenbitarray(nounce_code)
        if (nounce_code not in self.code_ids):
            raise Exception(f"No nounce_id Huffman code: seed_code={nounce_code}")
        nounce_id = self.code_ids[nounce_code]
        if (self.has_consumed_nounce_id(nounce_id=nounce_id) is False):
            raise Exception(f"No Huffman code for nounce_id={nounce_id}")
        return nounce_id
    
    def get_zero_element_code(self) -> bitarray:
        return self.id_codes[self.zero_element_id]
    
    def update_id_codes(self) -> List[NounceChange]:
        """
        Rebuild Huffman tree and update nounce_id codes
        """
        # saving old tree metadata to detect diffs
        old_id_codes        = self.id_codes.copy()
        old_id_code_lengths = self.id_code_lengths.copy()
        old_ids             = set(old_id_codes.keys())
        # clearing old tree metadata
        self.id_codes.clear()
        self.code_ids.clear()
        self.id_code_lengths.clear()
        self.code_lengths.clear()
        self.code_length_counts.clear()
        self.id_codes_by_length.clear()
        self.ids_by_length.clear()
        # main update: rebuild Huffman tree
        self.id_codes = SortedDict(huffman_code(self.id_counts, endian=self.default_endian).items())
        
        # preparing update list
        updates = list()
        changes = list()
        # all other updates derived from new Huffman tree
        for nounce_id, nounce_id_code in self.id_codes.items():
            nounce_id_code                  = frozenbitarray(nounce_id_code)
            code_length                     = len(nounce_id_code)
            self.code_ids[nounce_id_code]   = nounce_id
            self.id_code_lengths[nounce_id] = code_length
            self.code_lengths.add(code_length)
            self.code_length_counts.update({ code_length : 1 })
            self.id_codes_by_length[code_length].add(nounce_id_code)
            self.ids_by_length[code_length].add(nounce_id)
            # detect changes
            changes.clear()
            if (nounce_id not in old_ids):
                #print(f"nounce_id={nounce_id}, old_ids={old_ids}")
                # new seed_id added
                changes.append(NounceChangeData(
                    change_type = NounceChangeType.NOUNCE_ID,
                    old_value   = None,
                    new_value   = nounce_id,
                ))
                changes.append(NounceChangeData(
                    change_type = NounceChangeType.NOUNCE_ID_CODE,
                    old_value   = None,
                    new_value   = nounce_id_code,
                ))
                changes.append(NounceChangeData(
                    change_type = NounceChangeType.CODE_LENGTH,
                    old_value   = None,
                    new_value   = code_length,
                ))
            else:
                # existing seed_id updated
                old_nounce_id_code = old_id_codes[nounce_id]
                if (old_nounce_id_code != nounce_id_code):
                    changes.append(NounceChangeData(
                        change_type = NounceChangeType.NOUNCE_ID_CODE,
                        old_value   = frozenbitarray(old_nounce_id_code),
                        new_value   = nounce_id_code,
                    ))
                old_code_length = old_id_code_lengths[nounce_id]
                if (old_code_length != code_length):
                    changes.append(NounceChangeData(
                        change_type = NounceChangeType.CODE_LENGTH,
                        old_value   = old_code_length,
                        new_value   = code_length,
                    ))
            # update seed_id if we have at least one change
            if (len(changes) > 0):
                update = NounceChange(
                    nounce_id      = nounce_id,
                    nounce_id_code = nounce_id_code,
                    changes        = changes.copy(),
                )
                updates.append(update)
                changes.clear()
        # update zero element 
        self.zero_element_code   = self.get_zero_element_code()
        self.zero_element_length = len(self.get_zero_element_code())
        
        # return all performed updates
        return updates
    
    def get_next_code_length(self, nounce_id: int=None) -> int:
        if (nounce_id is None):
            nounce_id = self.nounces.get_next_consumed_value_id()
        next_id_counts = self.id_counts.copy()
        next_id_counts.update({ self.zero_element_id : 1 })
        next_id_counts.update({ nounce_id : 1 })
        next_codes = SortedDict(huffman_code(next_id_counts, endian=self.default_endian).items())
        return len(next_codes[nounce_id])
    
    def get_code_length(self, nounce_id: int) -> int:
        if (nounce_id in self.nounces.has_consumed_value_id(value_id=nounce_id)):
            id_code_length = self.id_code_lengths[nounce_id]
        else:
            id_code_length = self.get_next_code_length()
        return id_code_length
    
    def get_min_code_length(self) -> int:
        return min(self.code_lengths)
    
    def get_max_code_length(self) -> int:
        return max(self.code_lengths)
    
    def get_next_nounce_id_value(self) -> int:
        return len(self.sorted_nounces) + 1
    
    def absorb_zero_element_id(self, times: int=1) -> None:
        self.id_counts.update({ self.zero_element_id : times })
    
    def absorb_nounce_id(self, nounce_id: int) -> None:
        if (self.has_consumed_nounce(nounce=nounce_id) is False):
            raise Exception(f"No Huffman code for nounce_id={nounce_id}")
        self.id_counts.update({ nounce_id : 1 })
    
    def absorb_raw_nounce(self, nounce: int) -> int:
        if (self.has_consumed_nounce(nounce=nounce) is True):
            consumed_nounce_id = self.get_consumed_nounce_id(nounce=nounce)
            self.id_counts.update({ consumed_nounce_id: 1 })
            return consumed_nounce_id
        if (nounce == self.zero_element):
            raise Exception(f"cannot register new nounce={nounce} equals to self.zero_element={nounce}")
        consumed_nounce_id = self.nounces.consume_value(value=nounce)
        self.sorted_nounces.add(nounce)
        self.id_counts.update({ consumed_nounce_id: 1 })
        return consumed_nounce_id
    
    def get_nounce_type(self, nounce: int) -> NounceType:
        if (self.has_consumed_nounce(nounce=nounce) is True):
            return NounceType.CONSUMED
        if (self.has_remaining_nounce(nounce=nounce) is True):
            return NounceType.REMAINING
        else:
            self.nounces.add_remaining_value(value=nounce)
            return NounceType.REMAINING
        #raise Exception(f"unknown type for nounce={nounce}")
    
    def get_nounce_type_from_id_type(self, nounce_id_type: NounceIdType) -> NounceType:
        if (nounce_id_type == NounceIdType.CONSUMED) or (nounce_id_type == NounceIdType.CONSUMED_RAW):
            return NounceType.CONSUMED
        if (nounce_id_type == NounceIdType.REMAINING) or (nounce_id_type == NounceIdType.REMAINING_RAW):
            return NounceType.REMAINING
        raise Exception(f"unknown nounce_id_type={nounce_id_type}")
    
    def get_nounce_id_type(self, nounce_type: NounceType, nounce: int) -> NounceIdType:
        if (nounce_type == NounceType.CONSUMED):
            nounce_id = self.get_consumed_nounce_id(nounce=nounce)
            id_type   = self.get_nounce_type(nounce=nounce_id)
            if (id_type == NounceType.CONSUMED):
                return NounceIdType.CONSUMED
            if (id_type == NounceType.REMAINING):
                return NounceIdType.CONSUMED_RAW
            else:
                raise Exception(f"unknown type for nounce_id (nounce={nounce}, id={nounce_id})")
        if (nounce_type == NounceType.REMAINING):
            nounce_id = self.get_remaining_nounce_id(nounce=nounce)
            id_type   = self.get_nounce_type(nounce=nounce_id)
            if (id_type == NounceType.CONSUMED):
                return NounceIdType.REMAINING
            if (id_type == NounceType.REMAINING):
                return NounceIdType.REMAINING_RAW
            else:
                raise Exception(f"unknown type for nounce_id (nounce={nounce}, id={nounce_id})")
        else:
            raise Exception(f"unknown type for nounce={nounce}")
    
    def get_nounce_prefix(self, nounce_id_type: NounceIdType) -> bitarray:
        if (nounce_id_type == NounceIdType.CONSUMED):
            return bitarray('', endian=self.default_endian)
        elif (nounce_id_type == NounceIdType.REMAINING):
            return self.get_zero_element_code().copy()
        elif (nounce_id_type == NounceIdType.CONSUMED_RAW):
            return (self.get_zero_element_code().copy() * 2).copy()
        elif (nounce_id_type == NounceIdType.REMAINING_RAW):
            return (self.get_zero_element_code().copy() * 3).copy()
        else:
            raise Exception(f"unknown nounce_id_type={nounce_id_type}")
    
    def get_prefix_length_by_nounce_id_type(self, nounce_id_type: NounceIdType) -> int:
        if (nounce_id_type == NounceIdType.CONSUMED):
            return self.zero_element_length * nounce_id_type
        elif (nounce_id_type == NounceIdType.REMAINING):
            return self.zero_element_length * nounce_id_type
        elif (nounce_id_type == NounceIdType.CONSUMED_RAW):
            return self.zero_element_length * nounce_id_type
        elif (nounce_id_type == NounceIdType.REMAINING_RAW):
            return self.zero_element_length * nounce_id_type
        else:
            raise Exception(f"unknown nounce_id_type={nounce_id_type}")
    
    def absorb_nounce(self, nounce: int):
        if (self.changes_committed is False):
            raise Exception(f"Changes not committed to tree: cannot process new value. You need to call self.commit_changes() for that")
        if (self.seeds_tree.has_consumed_seed(nounce) is False):
            raise Exception(f"New nounce must be one of consumed seed values (nounce={nounce})")
        encoded_nounce = self.encode_nounce(nounce=nounce)
        # define params to update tree
        nounce_type    = self.get_nounce_type(nounce=nounce)
        nounce_id      = self.get_nounce_id(nounce=nounce)
        nounce_id_type = self.get_nounce_id_type(nounce_type=nounce_type, nounce=nounce)
        
        # prepare tree updates
        if (nounce_id_type == NounceIdType.CONSUMED):
            self.absorb_nounce_id(nounce_id=nounce_id)
        elif (nounce_id_type == NounceIdType.REMAINING):
            self.absorb_zero_element_id(times=1)
            self.absorb_raw_nounce(nounce=nounce)
            self.absorb_nounce_id(nounce_id=nounce_id)
        elif (nounce_id_type == NounceIdType.CONSUMED_RAW):
            self.absorb_zero_element_id(times=2)
            self.absorb_raw_nounce(nounce=nounce)
            self.absorb_raw_nounce(nounce=nounce_id)
        elif (nounce_id_type == NounceIdType.REMAINING_RAW):
            self.absorb_zero_element_id(times=3)
            self.absorb_raw_nounce(nounce=nounce)
            self.absorb_raw_nounce(nounce=nounce_id)
        else:
            raise Exception(f"unknown nounce_id_type={nounce_id_type}")
        # mark changes uncommitted
        self.changes_committed = False
        return encoded_nounce
    
    def commit_changes(self) -> List[NounceChange]:
        self.sync_remaining_values()
        if (self.changes_committed is True):
            return []
        # update Huffman tree only when encoding completed: this is required to keep encoding and decoding
        # trees identical on each step
        updates                = self.update_id_codes()
        self.changes_committed = True
        # return all updates for the tree
        return updates
    
    def encode_raw_nounce_id(self, nounce_id: int) -> bitarray:
        #nounce_id = self.get_nounce_id(nounce=nounce)
        return encode_varint_number(number=nounce_id).copy()
        #if (self.seeds_tree.has_consumed_seed(seed=nounce) is False):
        #    raise Exception(f"unable to encode nounce={nounce} using consumed seed id")
        #seed_id   = self.seeds_tree.get_consumed_seed_id(seed=nounce)
        #seed_code = self.seeds_tree.get_seed_id_code(seed_id=seed_id)
        #return seed_code.copy()
    
    def encode_nounce_prefix(self, nounce_id_type: NounceIdType) -> bitarray:
        return self.get_nounce_prefix(nounce_id_type=nounce_id_type).copy()

    def encode_nounce_id(self, nounce_id: int, nounce: int, nounce_id_type: NounceIdType) -> bitarray:
        # encoding seed id value 
        if (nounce_id_type == NounceIdType.CONSUMED):
            return self.get_nounce_id_code(seed_id=nounce_id).copy()
        elif (nounce_id_type == NounceIdType.REMAINING):
            return self.get_nounce_id_code(seed_id=nounce_id).copy()
        elif (nounce_id_type == NounceIdType.CONSUMED_RAW):
            return self.encode_raw_nounce_id(nounce_id=nounce_id).copy()
        elif (nounce_id_type == NounceIdType.REMAINING_RAW):
            return self.encode_raw_nounce_id(nounce_id=nounce_id).copy()
        else:
            raise Exception(f"unknown nounce_id_type={nounce_id_type}")

    def encode_nounce(self, nounce: int) -> bitarray:
        #if (self.has_consumed_nounce(nounce=nounce)):
        #    raise Exception(f"unable to encode nounce={nounce}: this value is already registered in current seed={self.seed_id}")
        nounce_type    = self.get_nounce_type(nounce=nounce)
        nounce_id      = self.get_nounce_id(nounce=nounce)
        nounce_id_type = self.get_nounce_id_type(nounce_type=nounce_type, nounce=nounce)
        # encoding seed id type inside prefix
        encoded_nounce_prefix = self.encode_nounce_prefix(nounce_id_type=nounce_id_type)
        # encoding seed id depending on it's type
        encoded_nounce_id     = self.encode_nounce_id(nounce_id=nounce_id, nounce=nounce, nounce_id_type=nounce_id_type)
        encoded_nounce        = encoded_nounce_prefix + encoded_nounce_id
        # decode seed with same SeedTree state and check the result (for testing)
        if (self.debug is True):
            decoded_nounce = self.decode_nounce(encoded_nounce=encoded_nounce)
            if (decoded_nounce != nounce):
                decoded_nounce_id_type  = self.decode_nounce_prefix(encoded_nounce=encoded_nounce)
                decoded_nounce_type     = self.get_nounce_type_from_id_type(nounce_id_type=decoded_nounce_id_type)
                decoded_nounce_id       = self.decode_nounce_id(encoded_nounce=encoded_nounce, nounce_id_type=decoded_nounce_id_type)
                decoded_prefix_bits     = self.decode_nounce_prefix_bits(encoded_nounce=encoded_nounce)
                decoded_nounce_id_bits  = self.decode_nounce_id_bits(encoded_nounce=encoded_nounce, nounce_id_type=decoded_nounce_id_type)
                msg  = "ENCODED:\n"
                msg += f"nounce_id={nounce_id}, nounce_type={nounce_type.name}, nounce_id_type={nounce_id_type.name}\n"
                msg += f"encoded_nounce_prefix={encoded_nounce_prefix}, encoded_nounce_id={encoded_nounce_id},\n"
                msg += f"nounce={nounce} ({encoded_nounce})\n"
                msg += "\nDECODED:\n"
                msg += f"decoded_nounce_prefix={decoded_prefix_bits}, decoded_nounce_id={decoded_nounce_id_bits},\n"
                msg += f"nounce_id={decoded_nounce_id}, nounce_type={decoded_nounce_type.name}, nounce_id_type={decoded_nounce_id_type.name}\n"
                msg += f"nounce={decoded_nounce}\n"
                raise Exception(msg)
            #assert(decoded_nounce == nounce)
        return encoded_nounce
    
    def decode_nounce(self, encoded_nounce: bitarray) -> int:
        nounce_id_type  = self.decode_nounce_prefix(encoded_nounce=encoded_nounce)
        nounce_type     = self.get_nounce_type_from_id_type(nounce_id_type=nounce_id_type)
        nounce_id       = self.decode_nounce_id(encoded_nounce=encoded_nounce, nounce_id_type=nounce_id_type)
        nounce          = self.get_nounce(nounce_id=nounce_id, nounce_type=nounce_type)
        return nounce

    def decode_nounce_prefix_bits(self, encoded_nounce: bitarray) -> bitarray:
        prefix_chunk_length = self.zero_element_length
        prefix_chunks = {
            NounceIdType.REMAINING     : encoded_nounce[0:prefix_chunk_length],
            NounceIdType.CONSUMED_RAW  : encoded_nounce[prefix_chunk_length:prefix_chunk_length*2],
            NounceIdType.REMAINING_RAW : encoded_nounce[prefix_chunk_length*2:prefix_chunk_length*3]
        }
        prefix_bits = bitarray('', endian=self.default_endian)
        for _, chunk_bits in prefix_chunks.items():
            if (chunk_bits != self.zero_element_code):
                return prefix_bits
            prefix_bits += chunk_bits
        return prefix_bits
    
    def decode_nounce_prefix(self, encoded_nounce: bitarray) -> int:
        prefix_chunk_length = self.zero_element_length
        nounce_id_type      = NounceIdType.CONSUMED
        prefix_chunks = {
            NounceIdType.REMAINING     : encoded_nounce[0:prefix_chunk_length],
            NounceIdType.CONSUMED_RAW  : encoded_nounce[prefix_chunk_length:prefix_chunk_length*2],
            NounceIdType.REMAINING_RAW : encoded_nounce[prefix_chunk_length*2:prefix_chunk_length*3]
        }
        for new_nounce_id_type, chunk_bits in prefix_chunks.items():
            if (chunk_bits != self.zero_element_code):
                return nounce_id_type
            nounce_id_type = new_nounce_id_type
        return nounce_id_type
    
    def decode_nounce_id_bits(self, encoded_nounce: bitarray, nounce_id_type: NounceIdType) -> bitarray:
        prefix_length  = self.get_prefix_length_by_nounce_id_type(nounce_id_type=nounce_id_type)
        nounce_id_bits = encoded_nounce[prefix_length:len(encoded_nounce)]
        return nounce_id_bits

    def decode_nounce_id(self, encoded_nounce: bitarray, nounce_id_type: NounceIdType) -> int:
        nounce_id_bits = self.decode_nounce_id_bits(encoded_nounce=encoded_nounce, nounce_id_type=nounce_id_type)
        if (nounce_id_type == NounceIdType.CONSUMED) or (nounce_id_type == NounceIdType.REMAINING):
            return self.decode_huffman_nounce_id(encoded_nounce_id_bits=nounce_id_bits)
        if (nounce_id_type == NounceIdType.CONSUMED_RAW) or (nounce_id_type == NounceIdType.REMAINING_RAW):
            return self.decode_raw_nounce_id(raw_nounce_id_bits=nounce_id_bits)
        raise Exception(f"unknown nounce_id_type={nounce_id_type}")
    
    def decode_huffman_nounce_id(self, encoded_nounce_id_bits: bitarray) -> int:
        for code_length in self.code_lengths:
            encoded_bits = encoded_nounce_id_bits[0:code_length]
            length_codes = self.id_codes_by_length[code_length]
            for nounce_code in length_codes:
                #print(f"l={code_length}, seed_code={seed_code}, bits={encoded_bits}")
                if (nounce_code == encoded_bits):
                    nounce_id = self.code_ids[nounce_code]
                    return nounce_id
        raise Exception(f"No Huffman code for encoded_nounce_id_bits={encoded_nounce_id_bits}")

    def decode_raw_nounce_id(self, raw_nounce_id_bits: bitarray) -> int:
        nounce_id = decode_varint_number(encoded_number=raw_nounce_id_bits, prefix_length=self.varint_prefix_length)
        return nounce_id
        #if (self.seeds_tree.has_consumed_seed(seed=nounce) is False):
        #    raise Exception(f"unable to encode nounce={nounce} as consumed seed id")
        #seed_id = self.seeds_tree.decode_huffman_seed_id(encoded_seed_id_bits=raw_nounce_seed_id_bits)
        #seed    = self.seeds_tree.get_seed(seed_id=seed_id, seed_type=SeedType.CONSUMED)
        #return seed
        #return seed_id


class NumberType(int, Enum):
    """
    Тип числа в дереве Хаффмана
    """
    # зарегистрированное ранее, имеющее код в дереве Хаффмана
    # (само число уже упоминалось хотя бы раз, содержится в дереве Хаффмана и имеет id)
    # (префикс отсутствует)
    # (id задаётся кодом Хаффмана в основном дереве чисел)
    CONSUMED : int = 0
    # новое число из списка remaining_values, но его id в этом списке принадлежит к типу NumberType::CONSUMED
    # (само число ни разу ни упоминалось, число-значение id упоминалось хотя бы раз)
    # (префикс: 1 нулевой элемент)
    # (id задаётся кодом Хаффмана в основном дереве чисел)
    REMAINING : int = 1

class NumberIdType(int, Enum):
    """
    Тип id для числа в дереве Хаффмана с уникальными числами
    От типа id зависит способ его кодирования
    """
    # id ранее использованного числа в списке использованных чисел
    # (NumbersTree.numbers.get_consumed_number_id(number))
    # (задаётся кодом Хаффмана для id из списка ранее упомянутых значений)
    # (префикс: отсутствует, новых чисел в дерево не добавляется)
    CONSUMED      : int = 0
    # id нового числа в списке оставшихся чисел 
    # (ConsumableRange.numbers.get_remaining_number_id(number))
    # (задаётся кодом Хаффмана для id из списка оставшихся неиспользованных чисел)
    # (префикс: 1 нулевой элемент, в дерево добавляется 1 новое значение: число с указанным remaining_id)
    REMAINING     : int = 1
    # id ранее использованного числа в списке использованных чисел значение которого отсутствует в дереве чисел
    # (NumbersTree.numbers.get_consumed_number_id(number))
    # (задаётся кодом Фибоначчи)
    # (префикс: 2 нулевых элемента, в дерево добавляется 1 новое значение: id числа)
    CONSUMED_RAW  : int = 2
    # id нового числа в списке оставшихся чисел 
    # (ConsumableRange.numbers.get_remaining_number_id(number))
    # (задаётся кодом Фибоначчи)
    # (префикс: 3 нулевых элемента, в дерево добавляется 2 значения: новое число и его remaining_id)
    REMAINING_RAW : int = 3

class NumberIdEncodingType(int, Enum):
    """
    Тип кодирования id числа
    """
    HUFFMAN   : int = 0
    VARINT    : int = 1
    FIBONACCI : int = 2

@dataclass
class AbsorbedNumber:
    """
    Результат добавления нового числа к дереву уникальных чисел (NumbersTree)
    """
    number_value        : int                             = field(default=None)
    number_id_value     : int                             = field(default=None)
    number_type         : NumberType                 = field(default_factory=NumberType)
    number_id_type      : NumberIdType                    = field(default_factory=NumberIdType)
    id_encoding_type    : NumberIdEncodingType            = field(default_factory=NumberIdEncodingType)
    zero_element_code   : bitarray                        = field(default_factory=bitarray)
    #prefix_size         : int                             = field(default=0)
    encoded_prefix      : bitarray                        = field(default_factory=bitarray)
    encoded_id          : bitarray                        = field(default_factory=bitarray)
    encoded_number      : bitarray                        = field(default_factory=bitarray)
    updated_id_counts   : Counter                         = field(default_factory=Counter)
    consumed_numbers    : Set[int]                        = field(default_factory=SortedSet)
    #op_code             : bitarray                        = field(default_factory=bitarray)
    # {id : (value, code)}
    #new_numbers         : Dict[int, Tuple(int, bitarray)] = field(default_factory=SortedDict)
    # [(value, code)]
    #sorted_numbers      : List[Tuple[int, bitarray]]      = field(default_factory=list)
    
@dataclass
class NumbersTree:
    """
    Расширяемое дерево Хаффмана которое хранит уникальные целые числа
    Код каждого числа соответствует частоте использования (чем чаще встречается число - тем короче код)
    Структура данных представляет собой нечто среднее между префиксным деревом и функцией-губкой
    
    Это дерево служит источником значений для:
    - дерева используемых хеш-пространств (SeedTree) 
    - дерева используемых значений внутри хеш-пространства (NounceTree)
    
    То есть мы разделяем все значения на группы:
    - какие у нас вообще используются уникальные числа (и как часто)
    - какие из них используются как номера хеш-пространств (и как часто)
    - какие из них используются как номера значений внутри каждого хеш-пространства (и как часто именно внутри него)
    
    Новое число добавляется по следующим правилам:
    1) Если число уже было использовано (содержится в self.numbers) то используется его код Хаффмана, (без префикса)
    2) Если число встречается впервые (отсутствует в self.numbers), но для его id в списке self.numbers.remaining_values 
       уже есть существующий код - то используется код этого id, перед которым один раз записывается код нулевого элемента (префикс)
    3) Если число встречается впервые (отсутствует в self.numbers), и для его id в списке self.numbers.remaining_values 
       также нет кода (id числа в списке self.numbers.remaining_values отсутствует в self.sorted_numbers) 
       то для его записи используется код Фибоначчи C1, перед которым два раза записывается код нулевого элемента (как префикс)
    
    После добавления числа обновляется счетчик количества упоминаний, при этом каждое упоминание нулевого элемента считается
    также как и обычное число
    После добавления каждого числа дерево Хаффмана полностью пересоздаётся, используя счетчик количества упоминаний чисел
    как таблицу частот
    Каждое следующее число может приводить к полному перестроению дерева и переназначению кодов, поэтому
    и при кодировании и при декодировании нужно всегда ссылаться на последнюю версию дерева Хаффмана

    Число с id=0 всегда присутствует в списке оставшихся значений (self.numbers.remaining_values) и всегда содержит 0
    """
    # диапазон всех используемых чисел
    numbers               : ConsumableRange             = field(default=None)
    # числа для которых есть varint-коды
    sorted_numbers        : Set[int]                    = field(default_factory=SortedSet, init=False)
    # количество использований id каждого числа 
    # (это важно - мы считаем не количество использований самого значения а количество использований его id)
    # чтобы получить id числа по его значению используются методы self.get_consumed_number_id() и self.get_new_number_id()
    id_counts             : Counter                     = field(default_factory=Counter, init=False)
    # коды Хаффмана для id чисел
    id_codes              : Dict[int, bitarray]         = field(default_factory=SortedDict, init=False)
    # mapping для определения длины кода для каждого id (ключ - id числа, значение - длина кода для этого id)
    id_code_lengths       : Dict[int, int]              = field(default_factory=SortedDict, init=False)
    # все используемые варианты длины кода id
    code_lengths          : Set[int]                    = field(default_factory=SortedSet, init=False)
    # значение нулевого элемента (специальный маркер всегда равен 0)
    zero_element          : int                         = field(default=0, init=False)
    # значение id нулевого элемента (специальный маркер,всегда равен 0)
    zero_element_id       : int                         = field(default=0, init=False)
    # код id нулевого элемента (может меняться по мере роста дерева и добавления новых чисел)
    zero_element_code     : bitarray                    = field(default=None, init=False)
    # длина кода id нулевого элемента (в битах)
    zero_element_length   : int                         = field(default=1, init=False)
    # счетчик количества кодов разной длины в дереве: обнуляется и пересчитывается при каждом перестроении дерева
    # поэтому считает количество элементов каждой длинны в текущем дереве а не общее количество использования 
    # всех элементов каждой длины
    code_length_counts    : Counter                     = field(default_factory=Counter, init=False)
    # порядок чтения/записи бит при генерации кода Хаффмана и кода Фибоначчи (для класса bitarray)
    default_endian        : str                         = field(default='big', init=False, repr=False)

    def __post_init__(self):
        if (self.numbers is None):
            consumed_values = SkipDict()
            consumed_values.insert(self.zero_element_id, self.zero_element)
            self.numbers = ConsumableRange(consumed_values=consumed_values)
        self.id_counts.update({ self.zero_element_id: 1 })
        self.update_id_codes()

    def has_consumed_number(self, number: int) -> bool:
        return self.numbers.has_consumed_value(value=number)
    
    def has_consumed_number_id(self, number_id: int) -> bool:
        return self.numbers.has_consumed_value_id(value_id=number_id)
    
    def get_consumed_number_id(self, number: int) -> int:
        number_id = self.numbers.get_consumed_value_id(value=number)
        if (number_id is None):
            raise Exception(f"number={number} is not registered (number_id=None)")
        return number_id
    
    def has_remaining_number(self, number: int) -> bool:
        return self.numbers.has_remaining_value(value=number)
    
    def get_remaining_number(self, number_id: int) -> int:
        return self.numbers.remaining_values[number_id]
    
    def get_remaining_number_id(self, number: int) -> int:
        return self.numbers.get_remaining_value_id(value=number)
    
    def get_number_id(self, number: int) -> int:
        if (self.has_consumed_number(number=number) is True):
            return self.get_consumed_number_id(number=number)
        elif (self.has_remaining_number(number=number) is True):
            return self.get_remaining_number_id(number=number)
        else:
            raise Exception(f"cannot get number_id: number={number} is not registered")
    
    def raw_encode_number(self, number: int) -> bitarray:
        return to_c1_bits(number, endian=self.default_endian)
    
    def get_number_id_code(self, number_id: int) -> bitarray:
        if (self.has_consumed_number_id(number_id=number_id) is False):
            raise Exception(f"No Huffman code for number_id={number_id}")
        return self.id_codes[number_id]
    
    def get_zero_element_code(self) -> bitarray:
        return self.id_codes[self.zero_element_id]
    
    def update_code_lengths(self):
        self.id_code_lengths.clear()
        self.code_lengths.clear()
        self.code_length_counts.clear()
        for number_id, number_code in self.id_codes.items():
            code_length                    = len(number_code)
            self.id_code_lengths[number_id] = code_length
            self.code_lengths.add(code_length)
            self.code_length_counts.update({ code_length : 1 })
        self.zero_element_length = len(self.get_zero_element_code())
    
    def update_id_codes(self):
        self.id_codes.clear()
        self.id_codes = SortedDict(huffman_code(self.id_counts, endian=self.default_endian).items())
        self.zero_element_code = self.get_zero_element_code() #self.id_codes[self.zero_element_id]
        self.update_code_lengths()
    
    def get_next_code_length(self, number_id: int=None) -> int:
        if (number_id is None):
            number_id = self.numbers.get_next_consumed_value_id()
        next_id_counts = self.id_counts.copy()
        next_id_counts.update({ self.zero_element_id : 1 })
        next_id_counts.update({ number_id : 1 })
        next_codes = SortedDict(huffman_code(next_id_counts, endian=self.default_endian).items())
        return len(next_codes[number_id])
    
    def get_code_length(self, number_id: int) -> int:
        if (number_id in self.numbers.has_consumed_value_id(value_id=number_id)):
            number_length = self.id_code_lengths[number_id]
        else:
            number_length = self.get_next_code_length()
        return number_length
    
    def get_min_code_length(self) -> int:
        return min(self.code_lengths)
    
    def get_max_code_length(self) -> int:
        return max(self.code_lengths)
    
    def get_next_number_id_value(self) -> int:
        return len(self.sorted_numbers) + 1
    
    def absorb_zero_element_id(self, times: int=1) -> None:
        self.id_counts.update({ self.zero_element_id : times })
    
    def absorb_number_id(self, number_id: int) -> None:
        if (self.has_consumed_number(number=number_id) is False):
            raise Exception(f"No Huffman code for number_id={number_id}")
        self.id_counts.update({ number_id : 1 })
    
    def absorb_raw_number(self, number: int) -> int:
        if (self.has_consumed_number(number=number) is True):
            #raise Exception(f"number={number} already registered")
            consumed_number_id = self.get_consumed_number_id(number=number)
            self.id_counts.update({ consumed_number_id: 1 })
            return consumed_number_id
        if (number == self.zero_element):
            raise Exception(f"cannot register new number={number} equals to self.zero_element={number}")
        consumed_number_id = self.numbers.consume_value(value=number)
        self.sorted_numbers.add(number)
        self.id_counts.update({ consumed_number_id: 1 })
        return consumed_number_id
    
    def get_number_type(self, number: int) -> NumberType:
        if (self.has_consumed_number(number=number) is True):
            return NumberType.CONSUMED
        if (self.has_remaining_number(number=number) is True):
            return NumberType.REMAINING
        raise Exception(f"unknown type for number={number}")
    
    def get_number_id_type(self, number_type: NumberType, number: int) -> NumberIdType:
        if (number_type == NumberType.CONSUMED):
            number_id = self.get_consumed_number_id(number=number)
            id_type   = self.get_number_type(number=number_id)
            if (id_type == NumberType.CONSUMED):
                return NumberIdType.CONSUMED
            if (id_type == NumberType.REMAINING):
                return NumberIdType.CONSUMED_RAW
            else:
                raise Exception(f"unknown type for number_id (number={number}, id={number_id})")
        if (number_type == NumberType.REMAINING):
            number_id = self.get_remaining_number_id(number=number)
            id_type   = self.get_number_type(number=number_id)
            if (id_type == NumberType.CONSUMED):
                return NumberIdType.REMAINING
            if (id_type == NumberType.REMAINING):
                return NumberIdType.REMAINING_RAW
            else:
                raise Exception(f"unknown type for number_id (number={number}, id={number_id})")
        else:
            raise Exception(f"unknown type for number={number}")
    
    def get_number_prefix(self, number_id_type: NumberIdType) -> bitarray:
        if (number_id_type == NumberIdType.CONSUMED):
            return bitarray('', endian=self.default_endian)
        if (number_id_type == NumberIdType.REMAINING):
            return self.get_zero_element_code().copy()
        if (number_id_type == NumberIdType.CONSUMED_RAW):
            return self.get_zero_element_code().copy() * 2
        if (number_id_type == NumberIdType.REMAINING_RAW):
            return self.get_zero_element_code().copy() * 3
        else:
            raise Exception(f"unknown number_id_type={number_id_type}")

    def absorb_number(self, number: int) -> bitarray: #AbsorbedNumber:
        """
        Добавляет число к дереву Хаффмана

        Результат добавления (или скорее "впитывания") числа может быть двух типов:
        - это новое число, ни оно само, ни его префикс ни разу не встречался в дереве чисел: в этом
          случае число добавляется к дереву Хаффмана, получает уникальный id (в порядке добавления),
          после чего коды Хаффмана для всех id чисел в дереве в дереве пересоздаются
          При этом id сохраняются за чисел навсегда а коды Хаффмана для этих id могут постоянно меняться
        - это число уже есть в дереве и за ним закреплен уникальный id: в этом случае новое число в дерево 
          не добавляется, но количество упоминаний для id числа увеличивается на 1, 
          после чего коды Хаффмана для всех id чисел в дереве в дереве пересоздаются
          Поскольку дерево Хаффмана строится с учётом счётчика частоты упоминания, то наиболее часто 
          используемые id получают самые короткие коды
          При этом id сохраняются за чисел навсегда а коды Хаффмана для этих id могут постоянно меняться
        """
        number_type    = self.get_number_type(number=number)
        number_id      = self.get_number_id(number=number)
        number_id_type = self.get_number_id_type(number_type=number_type, number=number)
        number_prefix  = self.get_number_prefix(number_id_type=number_id_type)
        encoded_number = number_prefix.copy()

        if (number_id_type == NumberIdType.CONSUMED):
            self.absorb_number_id(number_id=number_id)
            encoded_number += self.get_number_id_code(number_id=number_id).copy()
        elif (number_id_type == NumberIdType.REMAINING):
            self.absorb_zero_element_id(times=1)
            self.absorb_raw_number(number=number)
            self.absorb_number_id(number_id=number_id)
            encoded_number += self.get_number_id_code(number_id=number_id).copy()
        elif (number_id_type == NumberIdType.CONSUMED_RAW):
            self.absorb_zero_element_id(times=2)
            self.absorb_raw_number(number=number)
            self.absorb_raw_number(number=number_id)
            encoded_number += self.raw_encode_number(number=number_id).copy()
        elif (number_id_type == NumberIdType.REMAINING_RAW):
            self.absorb_zero_element_id(times=3)
            self.absorb_raw_number(number=number)
            self.absorb_raw_number(number=number_id)
            encoded_number += self.raw_encode_number(number=number_id).copy()
        else:
            raise Exception(f"unknown number_id_type={number_id_type}")
        
        return encoded_number
    
    def decode_number(self, encoded_number: bitarray) -> int:
        pass

    def encode_number(self, number: int) -> bitarray:
        encoded_number = self.absorb_number(number=number)
        # decode number with same NumberTree state (for testing)
        # decoded_number = self.decode_number(encoded_number=encoded_number)
        # assert(decoded_number == number)
        self.update_id_codes()
        return encoded_number

