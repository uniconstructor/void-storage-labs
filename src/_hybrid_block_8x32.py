from __future__ import annotations
from rich import print
from rich.pretty import pprint
from rich.text import Text
from rich.progress import track,\
    BarColumn, Progress, Task, TaskID, TextColumn, TimeElapsedColumn, TimeRemainingColumn,\
    MofNCompleteColumn, RenderableColumn, SpinnerColumn, TransferSpeedColumn, FileSizeColumn, ProgressColumn
from custom_rich import CustomTaskProgressColumn as TaskProgressColumn
from tqdm.notebook import tqdm
from custom_counter import CustomCounter as Counter, ConsumableCounter, init_byte_counter
from collections import defaultdict
from bitarray import bitarray, frozenbitarray
from bitarray.util import ba2int, int2ba, canonical_huffman, canonical_decode, huffman_code,\
    vl_encode, vl_decode, sc_encode, sc_decode, serialize, zeros, intervals
from sortedcontainers import SortedSet, SortedDict, SortedList, SortedKeyList, SortedListWithKey,\
    SortedKeysView, SortedValuesView, SortedItemsView
from typing import List, Set, Dict, Tuple, Optional, Union, Iterable, DefaultDict,\
    TypeAlias, TypeVar, NewType, Annotated, Match, ChainMap, ParamSpec
from dataclasses import dataclass, field
from enum import Enum, IntEnum
from copy import deepcopy, copy
from delta_of_delta import delta_encode, delta_decode

from hash_range_iterator import DEFAULT_ENDIAN
from cycle_gen import CMWC

# blocks per chunk
DEFAULT_CHUNK_SIZE   = 256
# bytes per block
DEFAULT_BLOCK_LENGTH = 256
# bytes per block section
BLOCK_SECTION_SIZE   = 8
# sections per block
MAX_BLOCK_SECTIONS   = DEFAULT_BLOCK_LENGTH // BLOCK_SECTION_SIZE
# default size for number of sections inside each section group
GROUP_SIZE_BITS = {
    8: 2, 
    7: 4, 
    6: 4, 
    5: 4, 
    4: 4, 
    3: 4, 
    2: 3, 
    1: 2, 
    0: 2
}

class MappingMode(int, Enum):
    # all positions except given
    EXCLUDE : int = 0
    # only given positions
    INCLUDE : int = 1

    def __str__(self):
        return f'{self.name}({self.value})'
    
    def __repr__(self):
        return f'{self.name}({self.value})'

class BlockValueType(int, Enum):
    # no occurrences
    EXCLUDED   : int = 0
    # exactly 1 occurrence
    UNIQUE     : int = 1
    # 2 or more occurrences
    DUPLICATED : int = 2

    def __str__(self):
        return f'{self.name}({self.value})'
    
    def __repr__(self):
        return f'{self.name}({self.value})'

def get_section_group_size_bits(group_id: int) -> int:
    # TODO: calculate dynamically
    return GROUP_SIZE_BITS[group_id]

def encode_section_group_size(group_id: int, size: int, endian: str=DEFAULT_ENDIAN) -> bitarray:
    # TODO: encode size as number >= 1
    bit_length = get_section_group_size_bits(group_id=group_id)
    return int2ba(size, length=bit_length, endian=endian, signed=False)

def decode_section_group_size(bits: bitarray, group_id: int) -> int:
    # TODO: decode size as number >= 1
    return ba2int(bits, signed=False)

def get_file_bytes_block_by_id(block_id: int, data: bitarray) -> List[int]:
    block_bytes = list()
    for i in range(DEFAULT_BLOCK_LENGTH*(block_id), DEFAULT_BLOCK_LENGTH*(block_id+1)):
        file_byte = ba2int(data[i*8:i*8+8], signed=False)
        block_bytes.append(file_byte)
    return block_bytes

def get_file_bytes_chunk_by_id(chunk_id: int, data: bitarray) -> List[int]:
    chunk_bytes    = list()
    start_block_id = DEFAULT_CHUNK_SIZE * chunk_id
    end_block_id   = start_block_id + DEFAULT_CHUNK_SIZE
    for block_id in range(start_block_id, end_block_id):
        block_bytes = get_file_bytes_block_by_id(block_id=block_id, data=data)
        chunk_bytes += block_bytes
    return chunk_bytes

def get_first_bit_position(bitmap: bitarray, target_bit: int) -> int:
    return bitmap.index(target_bit)

def unary_encode(n: int, min_value: int=1, last_bit: int=0, endian: str=DEFAULT_ENDIAN) -> bitarray:
    if (n < min_value):
        raise Exception(f"n={n} must be greater than min_value={min_value}")
    space_bit   = 1 - last_bit
    result      = bitarray('', endian=endian)
    for i in range(min_value, n):
        result.append(space_bit)
    result.append(last_bit)
    return result

def unary_decode(bits: bitarray, min_value: int=1, last_bit: int=0) -> int:
    #space_bit    = 1 - last_bit
    result       = min_value
    if (bits.count(last_bit) == 0):
        # all bits are spacers - last bit is omitted
        result += len(bits)
    else:
        # count all bits before end marker bit
        last_index = bits.index(last_bit)
        result    += last_index
    return result

def make_shifted_list(values: List[int], distance: int) -> List[int]:
    if (distance == 0):
        return values.copy()
    if (distance > 0):
        left_start  = len(values)-distance
        left_end    = len(values)
        right_start = 0
        right_end   = len(values)-distance
    else:
        left_start  = abs(distance)
        left_end    = len(values)
        right_start = 0
        right_end   = abs(distance)
    left_part  = values[left_start:left_end]
    right_part = values[right_start:right_end]
    result     = left_part + right_part
    return result

def encode_item_order(items: List[int], max_values: int=None) -> bitarray:
    item_count  = len(items)
    item_counts = Counter()
    for v in range(item_count):
        if (v not in items):
            raise Exception(f"Incorrect item order: v={v} is missing. Sorted items={items}")
        item_counts.update({ v: 1 })  
    item_bits = bitarray("", endian=DEFAULT_ENDIAN)
    for item in items:
        item_codes = huffman_code(item_counts, endian=DEFAULT_ENDIAN)
        item_code  = item_codes[item].copy()
        item_bits += item_code
        #print(f"item={item}: code={item_code}, item_bits={item_bits} ({len(item_bits)})")
        del item_counts[item]
    return item_bits

def decode_item_order(item_bits: bitarray, length: int, max_values: int=None) -> List[int]:
    remaining_counts = Counter([x for x in range(length)])
    values           = list()
    if (max_values is None):
        max_values = length
    
    for i in range(0, max_values):
        codes        = huffman_code(remaining_counts, endian=DEFAULT_ENDIAN)
        code_lengths = Counter()
        for v, vc in codes.items():
            code_lengths.update({ v: len(vc) })
        # start from shortest codes
        decoded_value = None
        for value, code_length in code_lengths.least_common():
            code         = codes[value]
            encoded_bits = item_bits[0:code_length]
            if (code == encoded_bits):
                decoded_value = value
                item_bits     = item_bits[code_length:len(item_bits)].copy()
                values.append(decoded_value)
                del remaining_counts[decoded_value]
                break
        if (decoded_value is None) and (code_length > 0):
            raise Exception(f"Step {i} ({max_values}): No value found for huffman code (encoded_bits={encoded_bits.to01()} ({len(encoded_bits)}), item_bits={item_bits.to01()} ({len(item_bits)})")
        #else:
        #    print(f"{i}: value={decoded_value}, code={code.to01()}, lvb={len(item_bits)}")
    return values

@dataclass()
class FileStats:
    bit_length         : int                = field()
    start_block_id     : int                = field()
    value_counts       : Dict[int, Counter] = field(default_factory=lambda: defaultdict(Counter), repr=False)
    block_value_counts : Counter            = field(default_factory=Counter, repr=False)
    aggregated_counts  : Dict[int, Counter] = field(default_factory=lambda: defaultdict(Counter))
    end_block_id       : int                = field(default=None, init=False)
    byte_length        : int                = field(default=None, init=False)
    total_blocks       : int                = field(default=None, init=False)
    total_chunks       : int                = field(default=None, init=False)
    block_tail_length  : int                = field(default=None, init=False)
    chunk_tail_length  : int                = field(default=None, init=False)

    def __post_init__(self):
        self.byte_length       = self.bit_length // 8
        self.total_blocks      = self.byte_length // DEFAULT_BLOCK_LENGTH
        self.total_chunks      = self.total_blocks // DEFAULT_CHUNK_SIZE
        self.block_tail_length = self.byte_length % DEFAULT_BLOCK_LENGTH
        self.chunk_tail_length = self.total_blocks % DEFAULT_CHUNK_SIZE

def get_file_stats(file_name: str, start_block_id=0, end_block_id=None, max_blocks_per_value=None, endian=DEFAULT_ENDIAN) -> FileStats:
    #if (max_blocks_per_value is None):
    #    max_blocks_per_value = DEFAULT_BLOCK_LENGTH - 1
    data         = bitarray(endian=endian)
    file         = open(file=file_name, mode='rb')
    data.fromfile(file)
    data         = frozenbitarray(data)
    stats        = FileStats(
        bit_length     = len(data), 
        start_block_id = start_block_id,
    )
    # set up block range (if any)
    if (end_block_id is None):
        end_block_id = stats.total_blocks
    if (end_block_id < start_block_id):
        raise Exception(f"Incorrect block range: end_block_id={end_block_id} < start_block_id={start_block_id}")
    
    max_count_reached = False
    for block_id in tqdm(range(start_block_id, end_block_id)):
        file_bytes  = get_file_bytes_block_by_id(block_id=block_id, data=data)
        block_bytes = BlockBytes(items=file_bytes)
        #stats.block_value_counts.update(block_bytes.value_counts.most_common_above(1).keys())
        included_values   = block_bytes.included_values
        
        # check maximum blocks per value limit
        if (max_blocks_per_value is not None):
            max_count_values  = stats.block_value_counts.with_count(max_blocks_per_value).keys()
            if (len(max_count_values) > 0):
                for mc_value in max_count_values:
                    if (mc_value in included_values):
                        max_count_reached = True
                        break 
                if (max_count_reached):
                    break
        # update usage counts
        stats.block_value_counts.update(block_bytes.value_counts.most_common_above(1).keys())
        # count number of blocks touched by each value for each usage count of this value
        for value_frequency, value_count in block_bytes.value_counts.aggregated_counts().first_items():
            stats.aggregated_counts[value_frequency].update({ value_count: 1 })
            current_freq_values = block_bytes.value_counts.with_count(value_frequency).keys()
            for freq_value in current_freq_values:
                stats.value_counts[value_frequency].update({ freq_value: 1 })
    # sorting aggregated counts
    for oc in stats.aggregated_counts.keys():
        stats.aggregated_counts[oc] = Counter(dict(sorted(stats.aggregated_counts[oc].items())))
        stats.value_counts[oc]      = Counter(dict(sorted(stats.value_counts[oc].items(), key=lambda x: x[1])))
    # sorting value counts 
    stats.block_value_counts = Counter(dict(sorted(stats.block_value_counts.items(), key=lambda x: x[1])))
    # saving latest processed block id
    stats.end_block_id = block_id
    
    return stats

@dataclass
class SpId:
    """Section Position Id"""
    section_id : int
    byte_id    : int

def block_position_to_section_id(block_position: int) -> int:
    return block_position // BLOCK_SECTION_SIZE

def block_position_to_section_byte_id(block_position: int) -> int:
    section_id = block_position_to_section_id(block_position=block_position)
    return block_position - section_id * BLOCK_SECTION_SIZE

def block_position_to_section_position_id(block_position: int) -> SpId:
    section_id = block_position_to_section_id(block_position=block_position)
    byte_id    = block_position_to_section_byte_id(block_position=block_position)
    return SpId(section_id=section_id, byte_id=byte_id)

### BLOCK SECTION ###

class SItemCounts:
    unique     : int
    duplicated : int

@dataclass
class SItemPositions:
    unique     : Set[int] = field(default_factory=SortedSet)
    duplicated : Set[int] = field(default_factory=SortedSet)

    def __init__(self, unique: List[int], duplicated: List[int]):
        self.unique     = SortedSet(unique.copy())
        self.duplicated = SortedSet(duplicated.copy())

@dataclass
class SItemValues:
    unique     : List[int] = field(default_factory=list)
    duplicated : List[int] = field(default_factory=list)

    def __init__(self, unique: List[int], duplicated: List[int]):
        self.unique     = unique.copy()
        self.duplicated = duplicated.copy()

@dataclass
class BlockSection:
    section_id        : int       = field(default=None)
    hidden_values     : List[int] = field(default_factory=list)
    opened_values     : List[int] = field(default_factory=list)
    excluded_values   : Set[int]  = field(default_factory=SortedSet, repr=False)
    unique_values     : Set[int]  = field(default_factory=SortedSet, repr=False)
    duplicated_values : Set[int]  = field(default_factory=SortedSet, repr=False)

@dataclass
class SectionBitmap:
    bits       : bitarray = field(default=None, init=False)
    section_id : int      = field(default=None)
    item_count : int      = field(default=0, init=False)

    def __init__(self, section_id: int):
        self.section_id = section_id
        self.bits       = zeros(BLOCK_SECTION_SIZE, endian=DEFAULT_ENDIAN)
    
    def set_bitmap_position(self, byte_id: int, value: int=1):
        if (byte_id < 0) or (byte_id > (BLOCK_SECTION_SIZE-1)):
            raise Exception(f"Incorrect byte_id={byte_id} in section_id={self.section_id}")
        self.bits[byte_id] = 1
        self.item_count    = self.bits.count(1)

@dataclass
class BlockBytes:
    items                  : Dict[int, int]                 = field(default_factory=dict, repr=False)
    sections               : List[BlockSection]             = field(default_factory=list, init=False)
    included_count         : int                            = field(default=0, init=False)
    excluded_count         : int                            = field(default=0, init=False)
    unique_count           : int                            = field(default=0, init=False)
    duplicated_count       : int                            = field(default=0, init=False)
    included_values        : Set[int]                       = field(default_factory=SortedSet, init=False)
    excluded_values        : Set[int]                       = field(default_factory=SortedSet, init=False)
    unique_values          : Set[int]                       = field(default_factory=SortedSet, init=False)
    duplicated_values      : Set[int]                       = field(default_factory=SortedSet, init=False)
    value_counts           : Counter                        = field(default_factory=Counter, init=False, repr=False)
    value_positions        : Dict[int, List[int]]           = field(default_factory=lambda: defaultdict(list), init=False, repr=False)
    duplicate_positions    : Dict[int, List[int]]           = field(default_factory=lambda: defaultdict(list), init=False)
    duplicate_sections     : Dict[int, List[SpId]]          = field(default_factory=lambda: defaultdict(list), init=False)
    section_bitmaps        : Dict[int, SectionBitmap]       = field(default_factory=dict, init=False)
    # grouped by number of duplicate values in the section
    sorted_section_bitmaps : Dict[int, List[SectionBitmap]] = field(default_factory=lambda: defaultdict(list), init=False)
    sorted_section_counts  : Counter                        = field(default_factory=Counter, init=False)
    sorted_section_ids     : List[int]                      = field(default_factory=list, init=False)

    def __init__(self, items: Dict[int, int] | List[int]):
        if (items is not None) and (len(items) != DEFAULT_BLOCK_LENGTH):
            raise Exception(f"Incorrect block length={len(items)}: items={items}")
        self.items                  = dict()
        self.sections               = list()
        self.value_counts           = init_byte_counter()
        self.value_positions        = defaultdict(list)
        self.duplicate_positions    = defaultdict(list)
        # all sections, with duplicate occurrences 
        self.duplicate_sections     = defaultdict(list)
        self.section_bitmaps        = dict()
        self.sorted_section_bitmaps = defaultdict(list)
        self.sorted_section_counts  = Counter()
        self.sorted_section_ids     = list()

        for position in range(DEFAULT_BLOCK_LENGTH):
            byte_value = items[position]
            if (byte_value < 0) or (byte_value > 255):
                raise Exception(f"Incorrect byte_value={byte_value}, items={items}")
            self.items[position] = byte_value
            self.value_counts.update({ byte_value: 1 })
            self.value_positions[byte_value].append(position)
        
        self.included_values   = SortedSet(self.value_counts.with_count_above(1).keys())
        self.excluded_values   = SortedSet(self.value_counts.with_count(0).keys())
        self.unique_values     = SortedSet(self.value_counts.with_count(1).keys())
        self.duplicated_values = SortedSet(self.value_counts.with_count_above(2).keys())
        self.included_count    = len(self.included_values)
        self.excluded_count    = len(self.excluded_values)
        self.unique_count      = len(self.unique_values)
        self.duplicated_count  = len(self.duplicated_values)

        block_values = list(self.items.values())
        for section_id in range(MAX_BLOCK_SECTIONS):
            start_section_idx   = BLOCK_SECTION_SIZE * section_id
            end_section_idx     = start_section_idx + BLOCK_SECTION_SIZE
            section_values      = block_values[start_section_idx:end_section_idx]
            s_excluded_values   = SortedSet()
            s_unique_values     = SortedSet()
            s_duplicated_values = SortedSet()
            for sv in section_values:
                if (sv in self.excluded_values):
                    s_excluded_values.add(sv)
                if (sv in self.unique_values):
                    s_unique_values.add(sv)
                if (sv in self.duplicated_values):
                    s_duplicated_values.add(sv)
            self.sections.append(BlockSection(
                section_id        = section_id,
                hidden_values     = section_values,
                excluded_values   = s_excluded_values,
                unique_values     = s_unique_values,
                duplicated_values = s_duplicated_values,
            ))
            self.section_bitmaps[section_id] = SectionBitmap(section_id=section_id)

        for d_value in self.duplicated_values:
            self.duplicate_positions[d_value] = self.value_positions[d_value].copy()
            for dv_position in self.duplicate_positions[d_value]:
                sp_id = block_position_to_section_position_id(dv_position)
                self.duplicate_sections[d_value].append(sp_id)
                self.section_bitmaps[sp_id.section_id].set_bitmap_position(byte_id=sp_id.byte_id)

        self.duplicates_distances = defaultdict(list)
        self.duplicates_bitmap    = bitarray(None, endian=DEFAULT_ENDIAN)
        # created sorted & grouped sections list
        self.sort_section_bitmaps()
    
    def get_value_type(self, value: int) -> BlockValueType:
        if (value in self.excluded_values):
            return BlockValueType.EXCLUDED
        if (value in self.unique_values):
            return BlockValueType.UNIQUE
        if (value in self.duplicated_values):
            return BlockValueType.DUPLICATED
        raise Exception(f"Unknown type for value={value}")
    
    def count_type_values(self, value_type: BlockValueType) -> int:
        if (value_type == BlockValueType.EXCLUDED):
            return self.excluded_count
        if (value_type == BlockValueType.UNIQUE):
            return self.unique_count
        if (value_type == BlockValueType.DUPLICATED):
            return self.duplicated_count
        raise Exception(f"Unknown value_type={value_type}")
    
    #def count_section_value_types(self, section: )
        
    def sort_section_bitmaps(self):
        for section_bitmap in self.section_bitmaps.values():
            #section_bitmap.bits = frozenbitarray(section_bitmap.bits)
            self.sorted_section_counts.update({ section_bitmap.item_count })
            self.sorted_section_bitmaps[section_bitmap.item_count].append(section_bitmap)
            self.sorted_section_bitmaps[section_bitmap.item_count] = sorted(
                self.sorted_section_bitmaps[section_bitmap.item_count], 
                key=lambda x: frozenbitarray(x.bits),
            )
        self.sorted_section_bitmaps = dict(reversed(SortedDict(self.sorted_section_bitmaps).items()))
        # collect section ids - we will encode them to restore original section order
        for group_id, group_sections in self.sorted_section_bitmaps.items():
            for group_section in group_sections:
                self.sorted_section_ids.append(group_section.section_id)
        return self.sorted_section_bitmaps
    
    def restore_group_prefix_bitmap(self, group_id: int, prefix: bitarray) -> bitarray:
        result = prefix.copy()
        
        if (len(prefix) == 8):
            return result
        
        if (group_id == 8):
            return bitarray('11111111', endian=DEFAULT_ENDIAN)
        
        if (group_id == 0):
            return bitarray('00000000', endian=DEFAULT_ENDIAN)

        if (group_id == 7):
            #target_bit = 1
            for bit_position in range(len(result), BLOCK_SECTION_SIZE):
                result.append(1)
            return result

    def encode_group_bitmaps(self, group_id: int) -> bitarray:
        result  = bitarray('', endian=DEFAULT_ENDIAN)
        bitmaps = self.sorted_section_bitmaps[group_id]
        
        if (len(bitmaps) == 0):
            # no bitmaps to encode
            return result
        if (group_id == 8) or (group_id == 0):
            # all bits are same - no need to encode anything
            return result
        
        # split bitmaps into first and all others
        first_bitmap = bitmaps[0]
        next_bitmaps = bitmaps[1:]
        group_size   = len(bitmaps)
        encoded_size = encode_section_group_size(group_id=group_id, size=group_size)
        result      += encoded_size
        print(f"group_id={group_id}: size={group_size}, encoded_size={encoded_size} ({len(encoded_size)})")

        # encoding format defined by group_id
        if (group_id == 7):
            # 1 unique + 7 duplicated
            print(f"Encoding: 1 unique + 7 duplicated ({len(bitmaps)} sections)")
            target_bit           = 0
            prev_target_position = get_first_bit_position(bitmap=first_bitmap.bits, target_bit=target_bit)
            encoded_prefix       = unary_encode(prev_target_position, min_value=0, last_bit=target_bit)
            result              += encoded_prefix
            print(f"prefix={encoded_prefix} ({len(encoded_prefix)}), bitmap={first_bitmap}")
            for next_bitmap in next_bitmaps:
                target_position = get_first_bit_position(bitmap=next_bitmap.bits, target_bit=target_bit)
                if (target_position == prev_target_position):
                    delta = 0
                else:
                    delta = target_position - prev_target_position
                encoded_delta        = unary_encode(delta, min_value=0, last_bit=0)
                result              += encoded_delta
                print(f"delta={encoded_delta} ({len(encoded_delta)}), bitmap={next_bitmap}")
                prev_target_position = target_position
            return result
        # TODO: groups 6-0

        raise Exception(f"Incorrect group_id={group_id}")

    def decode_group_bitmaps(self, bitmaps: bitarray, group_id: int) -> List[bitarray]:
        result            = list()
        group_size_length = get_section_group_size_bits(group_id=group_id)
        encoded_size      = bitmaps[0:group_size_length]
        group_size        = decode_section_group_size(bits=encoded_size, group_id=group_id)
        remaining_data    = bitmaps[group_size_length:]
        print(f"group_id={group_id}: size={group_size}, encoded_size={encoded_size} ({len(encoded_size)}), data={remaining_data}")
        
        if (group_id == 8):
            for i in range(group_size):
                bitmap = self.restore_group_prefix_bitmap(group_id=group_id, prefix=bitarray('', endian=DEFAULT_ENDIAN))
                result.append(bitmap)
            return result
        
        if (group_id == 0):
            for i in range(group_size):
                bitmap = self.restore_group_prefix_bitmap(group_id=group_id, prefix=bitarray('', endian=DEFAULT_ENDIAN))
                result.append(bitmap)
            return result
        
        if (group_id == 7):
            # 1 unique + 7 duplicated
            print(f"Decoding: 1 unique + 7 duplicated ({group_size} sections)")
            target_bit           = 0
            prefix_length        = get_first_bit_position(bitmap=remaining_data, target_bit=target_bit) + 1
            encoded_prefix       = remaining_data[0:prefix_length]
            first_bitmap         = self.restore_group_prefix_bitmap(group_id=group_id, prefix=encoded_prefix)
            remaining_data       = remaining_data[prefix_length:]
            result.append(first_bitmap)
            print(f"prefix={encoded_prefix} ({len(encoded_prefix)}), bitmap={first_bitmap}, data={remaining_data}")
            for bitmap_id in range(1, group_size):
                delta_length   = get_first_bit_position(bitmap=remaining_data, target_bit=target_bit) + 1
                encoded_delta  = remaining_data[0:delta_length]
                delta          = unary_decode(bits=encoded_delta, min_value=0, last_bit=0)
                if (delta > 0):
                    prefix_length += delta
                    encoded_prefix = unary_encode(prefix_length, min_value=1, last_bit=0)
                next_bitmap = self.restore_group_prefix_bitmap(group_id=group_id, prefix=encoded_prefix)
                result.append(next_bitmap)
                print(f"delta={encoded_delta} ({len(encoded_delta)}), bitmap={next_bitmap}, encoded_prefix={encoded_prefix}, data={remaining_data}")
                remaining_data = remaining_data[delta_length:]
            return result
        # TODO: groups 6-0

        raise Exception(f"Incorrect group_id={group_id}")


#def create_exclusion_bitmap(block_bytes: BlockBytes, seed: int) -> bitarray:
#    generator    = CMWC(x=seed)
#    values       = generator.sample(range(DEFAULT_BLOCK_LENGTH), DEFAULT_BLOCK_LENGTH)
#    mapping      = bitarray(endian=DEFAULT_ENDIAN)
#    mapped_count = 0
#    for position in range(0, len(values)):
#        mapping_bit    = zeros(1, endian=DEFAULT_ENDIAN)
#        position_value = values[position]
#        if (position_value in block_bytes.excluded_values):
#            mapping_bit[0] = 1
#            mapped_count  += 1
#        mapping += mapping_bit
#        if (mapped_count == block_bytes.excluded_count):
#            break
#    return mapping

def create_canonical_exclusion_bitmap(block_bytes: BlockBytes) -> bitarray:
    values       = [x for x in range(DEFAULT_BLOCK_LENGTH)]
    mapping      = bitarray(endian=DEFAULT_ENDIAN)
    mapped_count = 0
    for position in range(0, len(values)):
        mapping_bit    = zeros(1, endian=DEFAULT_ENDIAN)
        position_value = values[position]
        if (position_value in block_bytes.excluded_values):
            mapping_bit[0] = 1
            mapped_count  += 1
        mapping += mapping_bit
        if (mapped_count == block_bytes.excluded_count):
            break
    return mapping

@dataclass
class CounterSelection:
    values         : List[int]
    matches_count  : int
    match_map      : List[bool]
    mirror_mapping : bool

def select_bytes_by_counter(generator: CMWC, k: int, value_counts: Counter, target_values: SortedSet[int]) -> CounterSelection:
    values           = list()
    remaining_counts = value_counts.copy()
    matches_count    = 0
    match_map        = list()
    mirror_mapping   = True
    mirror_distance  = k // 2 
    if (k % 2) == 1:
        mirror_mapping = False
    
    for i in range(0, k):
        weights = [c for _v, c in remaining_counts.first_items()]
        value   = generator.choices(population=range(256), weights=weights, k=1)[0]
        values.append(value)
        # never emit value again if there is no occurrences at this interval
        if (value in target_values):
            remaining_counts.update({ value: -1 })
            matches_count += 1
            match_map.append(True)
        else:
            remaining_counts[value] = 0
            match_map.append(False)
        # sometimes we can use 8 bit to save 16-bit mapping (when first 8 bits are equal inverted last 8 bits)
        if (mirror_mapping is True) and (i >= mirror_distance):
            if (match_map[i] == match_map[i-mirror_distance]):
                mirror_mapping = False
    
    return CounterSelection(
        values         = values,
        matches_count  = matches_count,
        match_map      = match_map,
        mirror_mapping = mirror_mapping,
    )

def find_seed_with_first_values(block_bytes: BlockBytes, sample_size: int=8, min_seed: int=0, max_seed: int=2**16) -> int:
    generator       = CMWC(x=min_seed)
    block_values    = block_bytes.items.values() #block_bytes.included_values.copy()
    target_values   = list(block_bytes.items.values())[0:sample_size] #SortedSet(list(block_values.copy())[0:sample_size])

    print(f"block_values={block_values} ({len(block_values)}), target_values={target_values}")
    
    max_value_count     = 1
    max_sequence_length = 1
    max_count_seed      = min_seed
    max_sequence_seed   = min_seed
    value_counts        = Counter()
    sequence_counts     = Counter()
    value_bitmaps       = defaultdict(lambda: defaultdict(SortedSet))
    
    for seed in tqdm(range(min_seed, max_seed), mininterval=0.3):
        generator.seed(seed=seed)
        values = select_bytes_by_counter(
            generator=generator, k=sample_size, value_counts=block_bytes.value_counts, target_values=target_values
        )
        seed_value_count     = 0
        seed_sequence_length = 0
        max_seed_sequence    = None
        value_bitmap         = bitarray('', endian=DEFAULT_ENDIAN)
        for value in values:
            value_bit = zeros(1, endian=DEFAULT_ENDIAN)
            if (value in target_values):
                value_bit[0] = 1
                seed_value_count += 1
                if (max_seed_sequence is None):
                    seed_sequence_length += 1
            else:
                max_seed_sequence = seed_sequence_length
            value_bitmap += value_bit
        
        value_counts.update({ seed_value_count: 1 })
        sequence_counts.update({ max_seed_sequence: 1 })
        
        if (seed_value_count < 3):
            continue
        
        if (seed_value_count >= 3):
            value_bitmap = frozenbitarray(value_bitmap)
            if (value_bitmap not in value_bitmaps[seed_value_count]):
                value_bitmaps[seed_value_count][value_bitmap].add(seed)
        if (max_seed_sequence >= max_sequence_length):
            print(f"seed={seed}: max_sequence_length={max_seed_sequence} ({max_sequence_length} -> {max_seed_sequence})")
            print(f"value_counts={value_counts}, sequence_counts={sequence_counts}")
            max_sequence_seed = seed
            max_sequence_length = max_seed_sequence
            if (max_sequence_length >= sample_size):
                break
        if (seed_value_count >= max_value_count):
            print(f"seed={seed}: max_value_count={seed_value_count} ({max_value_count} -> {seed_value_count}), max_seed_sequence={max_seed_sequence}")
            print(f"value_counts={value_counts}, sequence_counts={sequence_counts}")
            max_count_seed = seed
            max_value_count = seed_value_count
            if (max_value_count >= sample_size):
                break
    print(f"value_counts: {value_counts.first_items()}")
    print(f"sequence_counts: {sequence_counts.first_items()}")
    pprint(value_bitmaps)