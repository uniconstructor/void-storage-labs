from __future__ import annotations
from rich import print
from rich.pretty import pprint
#from tqdm.notebook import tqdm
from custom_counter import CustomCounter as Counter, ConsumableCounter
from collections import defaultdict, ChainMap, deque
from collections.abc import Iterable, Callable, Hashable, Generator,\
    ItemsView, KeysView, ValuesView, MappingView,\
    Mapping, MutableMapping,\
    Sequence, MutableSequence
from bitarray import bitarray, frozenbitarray
from bitarray.util import ba2int, int2ba, huffman_code, zeros, intervals, canonical_huffman
from sortedcontainers import SortedSet #, SortedDict
from typing import List, Set, Dict, Tuple, Optional, Union
from dataclasses import dataclass, field
from operator import attrgetter
from enum import Enum, IntEnum
from copy import deepcopy, copy
import json
import math
import functools
from itertools import chain
from more_itertools import consecutive_groups, run_length

from cycle_gen import CMWC
from canonical_huffman_encoder import create_canonical_codes

DEFAULT_ENDIAN    = 'big'
DEFAULT_STEP_BITS = 8

def value_list_to_range_list(values: List[int]) -> List[range]:
    ranges           = list()
    new_value_groups = consecutive_groups(sorted(values))
    for group in new_value_groups:
        range_values = list(group)
        start        = min(range_values)
        end          = max(range_values) + 1
        group_range  = range(start, end)
        ranges.append(group_range)
    return ranges

@dataclass()
class ChainedRange(Mapping):
    value_length        : int         = field()
    value_ranges        : List[range] = field()
    index_ranges        : List[range] = field(init=False, default=None)
    total_length        : int         = field(init=False, default=0)
    total_ranges        : int         = field(init=False, default=0)
    first_range_id      : int         = field(init=False, default=0)
    last_range_id       : int         = field(init=False, default=0)

    def __init__(self, ranges: List[range], value_length: int):
        if (len(ranges) == 0):
            raise Exception(f"Need at least one range to init ChainedRange")
        sorted_ranges = sorted(ranges, key=attrgetter('start'))

        self.value_length        = value_length
        self.value_ranges        = list()
        self.index_ranges        = list()
        self.total_length        = 0
        self.total_ranges        = len(ranges)
        self.first_range_id      = 0
        self.last_range_id       = self.total_ranges - 1

        start_index = 0
        end_index   = 0
        for range_id in range(self.first_range_id, self.total_ranges):
            input_range  = sorted_ranges[range_id]
            range_length = len(input_range)
            start_index  = end_index
            end_index    = start_index + range_length
            self.value_ranges.append(input_range)
            self.index_ranges.append(range(start_index, end_index))
        
        self.update_total_ranges()
        self.update_total_length()
    
    def __len__(self):
        return self.total_length

    def __getitem__(self, key):
        offset = 0
        for range_id in range(self.first_range_id, self.total_ranges):
            if (key in self.index_ranges[range_id]):
                value_id = key - offset
                return self.value_ranges[range_id][value_id]
            offset += len(self.index_ranges[range_id])
        raise KeyError
    
    def __contains__(self, key) -> bool:
        offset = 0
        for range_id in range(self.first_range_id, self.total_ranges):
            if (key in self.index_ranges[range_id]):
                return True
            offset += len(self.index_ranges[range_id])
        return False
    
    def __iter__(self):
        yield from chain.from_iterable(self.value_ranges)
    
    def __reversed__(self):
        yield from chain.from_iterable([reversed(vr) for vr in reversed(self.value_ranges)])
    
    def has_value(self, value: int) -> bool:
        for range_id in range(self.first_range_id, self.total_ranges):
            value_range = self.value_ranges[range_id]
            if (value in value_range):
                return True
        return False
    
    def get_range_id(self, value: int) -> int:
        for range_id in range(self.first_range_id, self.total_ranges):
            value_range = self.value_ranges[range_id]
            if (value in value_range):
                return range_id
        raise ValueError
    
    def update_index_ranges(self):
        index_ranges = list()
        start_index  = 0
        end_index    = 0
        for range_id in range(0, len(self.value_ranges)):
            value_range  = self.value_ranges[range_id]
            range_length = len(value_range)
            start_index  = end_index
            end_index    = start_index + range_length
            index_ranges.append(range(start_index, end_index))
        self.index_ranges.clear()
        self.index_ranges = index_ranges
    
    def update_total_ranges(self) -> int:
        value_range_count = len(self.value_ranges)
        index_range_count = len(self.index_ranges)
        if (value_range_count != index_range_count):
            raise Exception(f"Incorrect total range count: value_range_count={value_range_count} not equal index_range_count={index_range_count}")
        self.total_ranges  = value_range_count
        self.last_range_id = self.total_ranges - 1
        return self.total_ranges
    
    def update_total_length(self) -> int:
        total_length = 0
        for range_id in range(self.first_range_id, self.total_ranges):
            value_range_length = len(self.value_ranges[range_id])
            index_range_length = len(self.index_ranges[range_id])
            if (value_range_length != index_range_length):
                raise Exception(f"Incorrect range lengths: range_id={range_id} value_range_length={value_range_length} not equal index_range_length={index_range_length}")
            total_length += value_range_length
        self.total_length = total_length
        return self.total_length
    
    def index(self, value: int) -> int:
        #offset = 0
        for range_id in range(self.first_range_id, self.total_ranges):
            value_range = self.value_ranges[range_id]
            index_range = self.index_ranges[range_id]
            if (value in value_range):
                #value_id = offset + value_range.index(value)
                value_index = value_range.index(value)
                return index_range[value_index]
            #offset += len(self.value_ranges[range_id])
        raise KeyError
    
    def remove_value(self, value: int):
        if (self.has_value(value=value) is False):
            raise ValueError(f"Cannot remove value={value}: value not found")
        value_id         = self.index(value=value)
        new_value_ranges = list()
        new_index_ranges = list()
        
        for range_id in range(self.first_range_id, self.total_ranges):
            value_range = self.value_ranges[range_id]
            index_range = self.index_ranges[range_id]
            if (value not in value_range):
                new_value_ranges.append(value_range)
                new_index_ranges.append(index_range)
                continue
            if (value_id not in index_range):
                pprint(value_range)
                pprint(index_range)
                raise Exception(f"value={value}, value_id={value_id}")
            # value inside this range: check range margins
            left_value_edge  = min(value_range)
            right_value_edge = max(value_range)
            left_index_edge  = min(index_range)
            right_index_edge = max(index_range)
            
            if (value == left_value_edge) and (value == right_value_edge):
                # single-item range: removing range completely
                if (value_id != left_index_edge) or (value_id != right_index_edge):
                    raise Exception(f"range_id={range_id}: value={value} and value_id={value_id} edges didn't match: lve={left_value_edge}, rve={right_value_edge}, lie={left_index_edge}, rie={right_index_edge}")
                continue
            elif (value == left_value_edge) and (value < right_value_edge):
                # left edge match: update range start
                if (value_id != left_index_edge):
                    raise Exception(f"range_id={range_id}: value={value} and value_id={value_id} edges didn't match: lve={left_value_edge}, rve={right_value_edge}, lie={left_index_edge}, rie={right_index_edge}")
                new_value_range = range(value_range.start+1, value_range.stop)
                new_index_range = range(index_range.start+1, index_range.stop)
                new_value_ranges.append(new_value_range)
                new_index_ranges.append(new_index_range)
                continue
            elif (value > left_value_edge) and (value == right_value_edge):
                 # right edge match: update range end
                if (value_id != right_index_edge):
                    raise Exception(f"range_id={range_id}: value={value} and value_id={value_id} edges didn't match: lve={left_value_edge}, rve={right_value_edge}, lie={left_index_edge}, rie={right_index_edge}")
                new_value_range = range(value_range.start, value_range.stop-1)
                new_index_range = range(index_range.start, index_range.stop-1)
                new_value_ranges.append(new_value_range)
                new_index_ranges.append(new_index_range)
                continue
            elif (value > left_value_edge) and (value < right_value_edge):
                # between edges: split range into 2 parts around removed value
                new_left_value_range  = range(value_range.start, value)
                new_right_value_range = range(value+1, value_range.stop)
                new_left_index_range  = range(index_range.start, value_id)
                new_right_index_range = range(value_id+1, index_range.stop)
                new_value_ranges.append(new_left_value_range)
                new_value_ranges.append(new_right_value_range)
                new_index_ranges.append(new_left_index_range)
                new_index_ranges.append(new_right_index_range)
                continue
            else:
                raise Exception(f"range_id={range_id}: value={value} and value_id={value_id} - incorrect condition flow. lve={left_value_edge}, rve={right_value_edge}, lie={left_index_edge}, rie={right_index_edge}")
        # replace old ranges with new
        self.value_ranges = new_value_ranges
        self.index_ranges = new_index_ranges
        # update counters
        self.update_total_ranges()
        self.update_total_length()

    def remove_value_id(self, value_id: int):
        if (value_id >= self.total_length):
            raise KeyError(f"ChainedRange not contains value_id={value_id} (total_length={self.total_length})")
        value = self.__getitem__(value_id)
        self.remove_value(value=value)
    
    def remove_value_range(self, external_range: range):
        left_external_edge  = min(external_range)
        right_external_edge = max(external_range)
        new_internal_ranges = list()
        for range_id in range(0, self.total_ranges):
            internal_range      = self.value_ranges[range_id]
            left_internal_edge  = min(internal_range)
            right_internal_edge = max(internal_range)
            # check intersection between internal and external ranges
            if (right_external_edge < left_internal_edge):
                # external range ends before internal range starts - no intersection
                new_internal_ranges.append(range(internal_range.start, internal_range.stop))
                continue
            if (left_external_edge > right_internal_edge):
                # external range starts after end of internal range - no intersection
                new_internal_ranges.append(range(internal_range.start, internal_range.stop))
                continue
            if (left_external_edge <= left_internal_edge) and (right_internal_edge <= right_external_edge):
                # external range completely contains internal: omit internal range
                continue
            if (left_internal_edge < left_external_edge) and (right_external_edge < right_internal_edge):
                # internal range completely contains external: split internal range into 2 parts - before and after external range
                left_internal_part  = range(internal_range.start, left_external_edge + 1)
                right_internal_part = range(right_external_edge, internal_range.stop)
                new_internal_ranges.append(left_internal_part)
                new_internal_ranges.append(right_internal_part)
                continue
            if (left_external_edge < left_internal_edge) and (right_internal_edge >= right_external_edge):
                # external range intersects with internal (left): left part of internal range is truncated
                new_internal_ranges.append(range(external_range.stop, internal_range.stop))
                continue
            if (left_internal_edge <= left_external_edge) and (right_internal_edge < right_external_edge):
                # external range intersects with internal (right): left part of internal range is truncated
                new_internal_ranges.append(range(internal_range.start, external_range.start + 1))
                continue
        # update value ranges
        self.value_ranges = new_internal_ranges
        # update ids and counts
        self.update_index_ranges()
        self.update_total_ranges()
        self.update_total_length()

@dataclass()
class NestedRange:
    max_value_slots     : Counter                   = field()
    open_slot_counts    : Counter                   = field(init=False, default=None)
    closed_slot_counts  : Counter                   = field(init=False, default=None)
    values_by_length    : Dict[int, Set[int]]       = field(init=False, default=None, repr=False)
    values_by_id        : Dict[int, Dict[int, int]] = field(init=False, default=None, repr=False)
    ids_by_value        : Dict[int, Dict[int, int]] = field(init=False, default=None, repr=False)
    value_id_counter    : Counter                   = field(init=False, default=None, repr=False)
    active_value_length : int                       = field(init=False, default=None, repr=False)

    def __init__(self, max_value_slots: Counter):
        self.max_value_slots     = Counter(dict(sorted(list(max_value_slots.items()))))
        self.open_slot_counts    = self.max_value_slots.copy()
        self.closed_slot_counts  = Counter()
        self.values_by_length    = dict()
        self.values_by_id        = defaultdict(dict)
        self.ids_by_value        = defaultdict(dict)
        self.value_id_counter    = Counter()
        self.active_value_length = self.get_active_value_length()

        for value_length in sorted(list(self.max_value_slots.keys())):
            self.closed_slot_counts[value_length] = 0
            self.value_id_counter[value_length]   = 0
            self.values_by_length[value_length]   = SortedSet()
    
    def add_value(self, value: int, value_length: int) -> int:
        if (value.bit_length() > value_length):
            raise Exception(f"Cannot add value={value}: given value_length={value_length} is incorrect")
        if (self.open_slot_counts[value_length] == 0):
            raise Exception(f"Cannot add value={value} (value_length={value_length}): no open slots for this length")
        if (value in self.values_by_length[value_length]):
            raise Exception(f"Cannot add value={value} (value_length={value_length}) already added")
        # add new value to collection
        self.values_by_length[value_length].add(value)
        # update counters
        self.open_slot_counts.update({ value_length: -1 })
        self.closed_slot_counts.update({ value_length: 1 })
        self.value_id_counter.update({ value_length: 1 })
        self.update_active_value_length()
        # assign and value id
        new_value_id = self.value_id_counter[value_length]
        self.values_by_id[value_length][new_value_id] = value
        self.ids_by_value[value_length][value]        = new_value_id
        # remove overlapping values if any
        for target_length in self.get_all_value_lengths():
            if (target_length <= value_length):
                continue
            overlapping_values = self.get_overlapping_values(value=value, value_length=value_length, target_length=target_length)
            if (len(overlapping_values) > 0):
                print(f"target_length={target_length}: removing {len(overlapping_values)} overlaps={overlapping_values}")
                for overlapping_value in overlapping_values:
                    #print(f"Removing overlapping_value={overlapping_value}")
                    self.remove_value(value=overlapping_value, value_length=target_length)
        # return new value id
        return new_value_id
    
    def add_values(self, values: List[int], value_length: int) -> List[int]:
        new_values = dict()
        for value in values:
            if (value in self.values_by_length[value_length]):
                # skip already added values
                continue
            new_value_id             = self.add_value(value=value, value_length=value_length)
            new_values[new_value_id] = value
        return new_values #self.values_by_length[value_length]
    
    def remove_value(self, value: int, value_length: int) -> int:
        if (value.bit_length() > value_length):
            raise Exception(f"Cannot remove value={value}: given value_length={value_length} is incorrect")
        if (value not in self.values_by_length[value_length]):
            raise Exception(f"Cannot remove value={value} (value_length={value_length}): value not found")
        # remove value from collection
        self.values_by_length[value_length].remove(value)
        # update counters
        self.open_slot_counts.update({ value_length: 1 })
        self.closed_slot_counts.update({ value_length: -1 })
        # return remaining closed slots for this length
        return self.closed_slot_counts[value_length]
    
    def get_covered_value_range(self, source_value: int, source_length: int, target_length: int) -> range:
        if (source_value.bit_length() > source_length):
            raise Exception(f"Error: for given source_value={source_value} source_length={source_length} is incorrect")
        length_diff = target_length - source_length
        if (length_diff < 0):
            raise Exception(f"Cannot get covered range (l={target_length}) for value={source_value} (l={source_length}): incorrect source or target length")
        # 0b...000-0b...111
        range_size  = 2**length_diff
        # use left binary shift
        start_value = source_value * 2**length_diff
        # range will always cover 2^n values: more diff means more numbers covered by 1 value
        end_value   = start_value + range_size
        return range(start_value, end_value)
    
    def collect_tier_coverage_ranges(self, source_length: int, target_length: int) -> Dict[int, range]:
        length_diff = target_length - source_length
        ranges      = dict()
        if (length_diff < 0):
            raise Exception(f"Cannot collect tier coverage - source_length={source_length}, target_length={target_length}: target length cannot be less that source")
        if (len(self.values_by_length[source_length]) == 0):
            return ranges
        for source_value in self.values_by_length[source_length]:
            ranges[source_value] = self.get_covered_value_range(source_value=source_value, source_length=source_length, target_length=target_length)
        return ranges
    
    def get_overlapping_values(self, value: int, value_length: int, target_length: int) -> List[int]:
        if (value_length >= target_length):
            raise Exception(f"target_length={target_length} must be greater that value_length={value_length}")
        overlapping_values = list()
        overlapping_range  = self.get_covered_value_range(source_value=value, source_length=value_length, target_length=target_length)
        for target_value in self.values_by_length[target_length]:
            if (target_value in overlapping_range):
                overlapping_values.append(target_value)
        return overlapping_values
    
    def get_all_value_lengths(self) -> List[int]:
        return sorted(list(self.max_value_slots.keys()))
    
    def get_active_value_length(self) -> int|None:
        for value_length in self.get_all_value_lengths():
            if (self.open_slot_counts[value_length] > 0):
                return value_length
        return None
    
    def update_active_value_length(self) -> int:
        self.active_value_length = self.get_active_value_length()
        return self.active_value_length

    def get_prev_value_lengths(self, target_length: int) -> List[int]:
        prev_lengths = list()
        for value_length in self.get_all_value_lengths():
            if (value_length < target_length):
                prev_lengths.append(value_length)
        return prev_lengths

    def get_next_value_lengths(self, target_length: int) -> List[int]:
        next_lengths = list()
        for value_length in self.get_all_value_lengths():
            if (value_length > target_length):
                next_lengths.append(value_length)
        return next_lengths
    
    def collect_included_ranges(self, target_length: int, exclude_target_length: bool=True) -> Sequence[range]:
        included_ranges = list()
        min_value       = 0
        max_value       = 2**target_length
        excluded_ranges = self.collect_excluded_ranges(target_length=target_length, exclude_target_length=exclude_target_length)
        if (len(excluded_ranges) == 0):
            included_ranges.append(range(min_value, max_value))
            return included_ranges
        if (excluded_ranges[0].start == min_value):
            prev_excluded_range_end = excluded_ranges[0].stop
            first_excluded_range_id = 1
        else:
            prev_excluded_range_end = min_value
            first_excluded_range_id = 0
        
        for excluded_range_id in range(first_excluded_range_id, len(excluded_ranges)):
            excluded_range = excluded_ranges[excluded_range_id]
            start_value    = prev_excluded_range_end
            end_value      = excluded_range.start
            included_range = range(start_value, end_value)
            included_ranges.append(included_range)
            prev_excluded_range_end = excluded_range.stop
        
        if (prev_excluded_range_end < max_value):
            included_range = range(prev_excluded_range_end, max_value)
            included_ranges.append(included_range)
        return included_ranges

    def collect_excluded_ranges(self, target_length: int, exclude_target_length: bool=True) -> Sequence[range]:
        excluded_ranges = list()
        source_lengths  = self.get_prev_value_lengths(target_length=target_length)
        if (exclude_target_length):
            # same level values can also provide excluded ranges: in this case each range contains 1 element (value itself)
            source_lengths.append(target_length)
        if (len(source_lengths) == 0):
            return excluded_ranges
        for source_length in source_lengths:
            tier_ranges = self.collect_tier_coverage_ranges(source_length=source_length, target_length=target_length)
            for tier_range in tier_ranges.values():
                excluded_ranges.append(tier_range)
        if (len(excluded_ranges) == 0):
            return excluded_ranges
        # all ranges must be non-overlapped, so we can sort them using start value
        return sorted(excluded_ranges, key=attrgetter('start'))
    
    def count_excluded_values(self, target_length: int, exclude_target_length: bool=True) -> int:
        excluded_ranges = self.collect_excluded_ranges(target_length=target_length, exclude_target_length=exclude_target_length)
        excluded_count  = 0
        for excluded_range in excluded_ranges:
            excluded_count += len(excluded_range)
        return excluded_count

    def count_included_values(self, target_length: int, exclude_target_length: bool=True) -> int:
        included_ranges = self.collect_included_ranges(target_length=target_length, exclude_target_length=exclude_target_length)
        included_count  = 0
        for included_range in included_ranges:
            included_count += len(included_range)
        return included_count
    
    def get_included_values(self, target_length: int, exclude_target_length: bool=True) -> ChainedRange:
        return ChainedRange(
            ranges       = self.collect_included_ranges(target_length=target_length, exclude_target_length=exclude_target_length),
            value_length = target_length,
        )
    
    def get_excluded_values(self, target_length: int, exclude_target_length: bool=True) -> ChainedRange:
        return ChainedRange(
            ranges       = self.collect_excluded_ranges(target_length=target_length, exclude_target_length=exclude_target_length),
            value_length = target_length,
        )
    
    def has_value(self, value_length: int, value: int) -> bool:
        #included_values = self.get_included_values(target_length=value_length, exclude_target_length=True)
        #return included_values.has_value(value=value)
        if (value_length not in self.values_by_length):
            return False
        return (value in self.values_by_length[value_length])
    
    def has_value_id(self, value_length: int, value_id: int) -> int:
        if (value_length not in self.values_by_id):
            return False
        return (value_id in self.values_by_id[value_length])

    def get_value_by_id(self, value_length: int, value_id: int) -> int:
        if (self.has_value_id(value_length=value_length, value_id=value_id) is False):
            return False
        return self.values_by_id[value_length][value_id]
    
    def get_id_by_value(self, value_length: int, value: int) -> int:
        if (value_length not in self.ids_by_value):
            return False
        if (value not in self.ids_by_value[value_length]):
            return False
        return self.ids_by_value[value_length][value]
    
@dataclass()
class NestedNumber:
    id     : Tuple[int, int] = field(init=False, repr=False)
    value  : int             = field()
    length : int             = field()
    step   : int             = field(default=DEFAULT_STEP_BITS)
    picked : bool            = field(default=False)
    bits   : bitarray        = field(init=False, default=None)

    def __init__(self, length: int, value: int, step: int=DEFAULT_STEP_BITS, picked: bool=False):
        if (value.bit_length() > length):
            raise Exception(f"Incorrect bit_length={length} for value={value}")
        if ((length % step) > 0):
            raise Exception(f"Incorrect bit_length={length} for step={step} (value={value})")
        self.id         = (length, value)
        self.value      = value
        self.length     = length
        self.step       = step
        self.picked     = picked
        self.bits       = self.to_bitarray()
    
    def get_parent_length(self) -> int | None:
        if (self.length == 0):
            return None
        if (self.length == self.step):
            return 0
        return self.length - self.step
    
    def get_child_length(self) -> int:
        return self.length + self.step
    
    def get_tier_multiplier(self) -> int:
        return 2 ** self.step
    
    def get_parent_value(self) -> int | None:
        if (self.length == 0):
            return None
        if (self.length == self.step):
            return 0
        return self.value // self.get_tier_multiplier()
    
    def get_child_values(self) -> List[int]:
        values     = list()
        base_value = self.value * self.get_tier_multiplier()
        for i in range(0, self.get_tier_multiplier()):
            value = base_value + i
            values.append(value)
        return values
    
    def get_parent(self) -> NestedNumber | None:
        if (self.length == 0):
            return None
        return NestedNumber(length=self.get_parent_length(), value=self.get_parent_value(), step=self.step)
    
    def get_children(self) -> List[NestedNumber]:
        children = []
        for i in range(0, self.get_tier_multiplier()):
            children.append(NestedNumber(
                length = self.get_child_length(),
                value  = (self.value * self.get_tier_multiplier()) + i),
                step   = self.step,
            )
        return children
    
    def get_siblings(self) -> List[NestedNumber]: #NestedNumber | None:
        siblings = []
        for i in range(0, self.get_tier_multiplier()):
            value = self.get_parent_value() + i
            if (value != self.value):
                siblings.append(NestedNumber(length=self.length, value=value, step=self.step))
        return siblings
    
    def to_bitarray(self, freeze: bool=False, endian: str=DEFAULT_ENDIAN) -> bitarray:
        if (self.length == 0):
            bits = bitarray('')
        else:
            bits = int2ba(self.value, length=self.length, endian=endian, signed=False)
        if (freeze):
            return frozenbitarray(bits)
        else:
            return bits
    
    def pick(self):
        self.picked = True
    
    def unpick(self):
        self.picked = False

@dataclass()
class NestedNumberNode:
    """
    Single node of the nested range tree. Represents one value with specific length and 
    can provide all values starting from it.
    Can be "picked" (used for encoding), "disabled" (removed from future search, cannot be picked),
    and "targeted" (marked for picking), "scored" (scored nodes contain number of children that are targeted).
    Tree nodes also responsible for collecting value ranges for picking values from it.
    Each node responsible for collecting available value ranges, excluding picked values,
    disabling ranges of values during search inside specific length
    """
    number : NestedNumber = field()

@dataclass()
class ChainedRangeTree:
    value_lengths          : List[int]            = field()
    new_values             : Dict[int, List[int]] = field()
    value_counts           : Dict[int, Counter]   = field()
    step                   : int                  = field(default=DEFAULT_STEP_BITS)
    value_length_counts    : Counter              = field(init=False, default=None)
    root                   : NestedNumber         = field(init=False, default=None)
    picked_values          : Dict[int, Set[int]]  = field(init=False, default=None)
    disabled_values        : Dict[int, Set[int]]  = field(init=False, default=None)
    #value_ranges           : ChainedRange         = field(init=False, default=None)
    
    def __init__(self, value_lengths: List[int], new_values: Dict[int, List[int]], value_counts: Dict[int, Counter], step: int=DEFAULT_STEP_BITS):
        """
        Tree must be initialized with result of ContentBasedSplit (value_lengths, new_values, value_counts)
        Encoding tasks sequence:
        1) create content-based split
        2) build and prepare nested number tree: set up list of all values that will be used in each value length
        3) calculate tree metadata: how many values should be extracted from each node (max 256), what values
           are picked on each level, how many times each new value will be repeated
        4) find and choose sequence of seeds, where each seed gives maximum number of new values 
           (picking them from nested number ranges randomly), decreasing search space after each picked number
           seed values are chosen from metadata and play 2 roles at once to reduce overhead from storing extra seed list
        """
        self.value_lengths       = list()
        self.new_values          = defaultdict(list)
        self.value_counts        = defaultdict(Counter)
        self.step                = step
        self.value_length_counts = Counter()
        self.root                = NestedNumber(length=0, value=None, step=self.step)
        self.picked_values       = defaultdict(SortedSet)
        self.disabled_values     = defaultdict(SortedSet)

        for value_length in sorted(value_lengths):
            if ((value_length % step) > 0):
                raise Exception(f"Incorrect value_length={value_length} for step={step}")
            if (len(new_values[value_lengths]) == 0) or (len(new_values[value_lengths]) != len(value_counts[value_length])):
                raise Exception(f"Incorrect value_length={value_length} for new_values (l={len(new_values[value_lengths])}) or value_counts (l={len(value_counts[value_length])})")
            self.value_lengths.append(value_length)
            self.new_values[value_length]          = new_values[value_length].copy()
            self.value_counts[value_length]        = value_counts[value_length].copy()
            self.value_length_counts[value_length] = len(self.new_values[value_length])
            #self.picked_values[value_length].update(self.new_values[value_length])
        
    def get_picked_range(self, value_length: int) -> ChainedRange:
        ranges           = list()
        new_value_groups = consecutive_groups(sorted(self.new_values[value_length]))
        for group in new_value_groups:
            range_values = list(group)
            start        = min(range_values)
            end          = max(range_values) + 1
            group_range  = range(start, end)
            ranges.append(group_range)
        return ChainedRange(ranges=ranges, value_length=value_length)
    
    def get_full_value_range(self, value_length: int) -> ChainedRange:
        return ChainedRange([range(0, 2**value_length)], value_length=value_length)
    
    def get_remaining_length_range(self, value_length: int) -> ChainedRange:
        full_range = self.get_full_value_range(value_length=value_length)
        #new_values = self.get_new_value_range(value_length=value_length)

#########

@dataclass()
class NestedNumberContainer:
    container_number    : NestedNumber         = field()
    parent_container_id : int | None           = field()
    container_id        : int                  = field()
    value_length        : int                  = field(init=False, default=None)
    included_values     : Set[int] | SortedSet = field(init=False, default=None)
    excluded_values     : Set[int] | SortedSet = field(init=False, default=None)
    target_values       : Set[int] | SortedSet = field(init=False, default=None)

    def __init__(self, container_number: NestedNumber, parent_container_id: int, container_id: int):
        self.container_number     = deepcopy(container_number)
        self.parent_container_id  = parent_container_id
        self.container_id         = container_id
        self.value_length         = self.container_number.get_child_length()
        self.included_values      = SortedSet(self.container_number.get_child_values())
        self.excluded_values      = SortedSet()
        self.target_values        = SortedSet()
    
    def get_value_by_id(self, value_id: int) -> int:
        all_values = list(self.container_number.get_child_values())
        return all_values[value_id]
    
    def get_included_value_by_id(self, value_id: int) -> int:
        included_list = list(self.included_values)
        return included_list[value_id]
    
    def get_excluded_value_by_id(self, value_id: int) -> int:
        excluded_list = list(self.excluded_values)
        return excluded_list[value_id]
    
    def get_target_value_by_id(self, value_id: int) -> int:
        target_list = list(self.target_values)
        return target_list[value_id]
    
    def get_included_value_id(self, value: int) -> int:
        included_list = list(self.included_values)
        return included_list.index(value)
    
    def get_excluded_value_id(self, value: int) -> int:
        excluded_list = list(self.excluded_values)
        return excluded_list.index(value)
    
    def get_target_value_id(self, value: int) -> int:
        target_list = list(self.target_values)
        return target_list.index(value)
    
    def exclude_value(self, value: int):
        self.included_values.remove(value)
        self.excluded_values.add(value)
        self.target_values.discard(value)

    def exclude_values(self, values: List[int]):
        for value in values:
            self.exclude_value(value=value)
    
    def set_target_value(self, value: int):
        if (value not in self.included_values):
            raise Exception(f"Cannot set value={value} as target for included_values={self.included_values}")
        self.target_values.add(value)

    def set_target_values(self, values: List[int]):
        for value in values:
            if (value not in self.included_values):
                continue
            self.set_target_value(value=value)
    
    def has_included_value(self, length: int, value: int):
        if (length != self.value_length):
            return False
        return (value in self.included_values)
    
    def has_excluded_value(self, length: int, value: int):
        if (length != self.value_length):
            return False
        return (value in self.excluded_values)
    
    def has_target_value(self, length: int, value: int):
        if (length != self.value_length):
            return False
        return (value in self.target_values)
    
    def count_included_values(self) -> int:
        return len(self.included_values)
    
    def count_excluded_values(self) -> int:
        return len(self.excluded_values)
    
    def count_target_values(self) -> int:
        return len(self.target_values)
    
    def create_included_nested_containers(self) -> List[NestedNumberContainer]:
        containers = list()
        for included_value in self.included_values:
            container_number = NestedNumber(length=self.container_number.get_child_length(), value=included_value)
            container_id     = self.get_included_value_id(value=included_value)
            container        = NestedNumberContainer(
                container_number    = container_number,
                parent_container_id = self.container_id,
                container_id        = container_id,
            )
            containers.append(container)
        return containers
    
    def create_excluded_nested_containers(self) -> List[NestedNumberContainer]:
        containers = list()
        for excluded_value in self.excluded_values:
            container_number = NestedNumber(length=self.container_number.get_child_length(), value=excluded_value)
            container_id     = self.get_excluded_value_id(value=excluded_value)
            container        = NestedNumberContainer(
                container_number    = container_number,
                parent_container_id = self.container_id,
                container_id        = container_id,
            )
            containers.append(container)
        return containers
    
    #def create_canonical_dict(self, values: List[int] | SortedSet[int]) -> Dict[int, int]:
    #    canonical_dict = dict()
    #    for value in values:
    #        canonical_dict[value] = 1
    #    return canonical_dict
    #
    #def create_huffman_codes(self, values: List[int] | SortedSet[int]) -> Dict[int, bitarray]:
    #    freq_map = self.create_canonical_dict(values=values)
    #    return huffman_code(freq_map, endian=DEFAULT_ENDIAN)
    
    def get_included_value_codes(self) -> Dict[int, bitarray]:
        #return self.create_huffman_codes(values=self.included_values)
        return create_canonical_codes(values=self.included_values)
    
    def get_excluded_value_codes(self) -> Dict[int, bitarray]:
        #return self.create_huffman_codes(values=self.excluded_values)
        return create_canonical_codes(values=self.excluded_values)

    def get_target_value_codes(self) -> Dict[int, bitarray]:
        #return self.create_huffman_codes(values=self.target_values)
        return create_canonical_codes(values=self.target_values)