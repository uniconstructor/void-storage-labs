# https://github.com/Textualize/rich
from rich import inspect
from rich import print
# https://rich.readthedocs.io/en/latest/pretty.html
from rich.pretty import pprint
# https://rich.readthedocs.io/en/latest/protocol.html
from rich.console import Console, OverflowMethod
console = Console()
# https://rich.readthedocs.io/en/latest/traceback.html
from rich.traceback import install
install(show_locals=True)
# https://rich.readthedocs.io/en/latest/logging.html
import logging
from rich.logging import RichHandler
logging.basicConfig(
    level="INFO",
    format="%(message)s",
    datefmt="[%X]",
    handlers=[RichHandler(rich_tracebacks=True, locals_max_length=16)]
)
log = logging.getLogger("rich")
# https://pyoxidizer.readthedocs.io/en/stable/pyoxidizer_packaging_static_linking.html
import tqdm

# https://docs.python.org/3/library/itertools.html
from itertools import accumulate, zip_longest
# https://more-itertools.readthedocs.io/en/stable/api.html
from more_itertools import windowed, sliced
# https://bitstring.readthedocs.io/en/latest/index.html
from bitstring import Bits, BitArray, BitStream, ConstBitStream
# https://pythongeeks.org/ordereddict-in-python/
# https://www.geeksforgeeks.org/counters-in-python-set-1/
# https://docs.python.org/3/library/collections.html#counter-objects
from collections import OrderedDict, Counter, defaultdict, namedtuple, deque
# https://github.com/multiformats/unsigned-varint
# https://github.com/fmoo/python-varint
import varint
# https://docs.python.org/3/library/typing.html
from typing import List, Dict, Set, Tuple, Optional, Union
# https://www.geeksforgeeks.org/floor-ceil-function-python/?ref=lbp
import math
import operator
from statistics import mean
# https://github.com/eerimoq/detools
# https://pypi.org/project/delta-of-delta/
# https://github.com/blester125/delta-of-delta
# https://github.com/blester125/delta-of-delta/blob/master/tests/test_encoders.py#L155
from delta_of_delta import delta_encode, delta_of_delta_encode, delta_of_delta_decode, delta_decode, DeltaEncoding
# https://docs.python.org/3.8/library/difflib.html#difflib.SequenceMatcher.get_opcodes
from difflib import SequenceMatcher, Match
# https://docs.python.org/3/library/base64.html
import base64
# https://stackoverflow.com/questions/55212014/python-count-copies-in-dictionary
import copy

import os
if not 'workbookDir' in globals():
    workbookDir = os.getcwd()
os.chdir(workbookDir)

# функции для работы с хеш-пространством
from hash_space_utils import HashItemAddress, \
    get_min_bit_length, \
    decode_varint_bytes, \
    value_at_position, \
    read_hash_item, \
    split_data, \
    count_split_values, \
    collect_split_positions \
# type alias
HashSpaceSegment = HashItemAddress

# Match block after delta encoding
DeltaBlock     = namedtuple('DeltaBlock', ['hash_position', 'data_position', 'size'])
# data --> hash mapping data, including blocks
SegmentOptions = namedtuple('SegmentOptions', [
    'matcher',
    'first_block',
    'blocks',
    'delta_blocks', 
    'delta_block_length', 
    'hash_position_length', 
    'data_position_length',
    'item_size_length',
    'hash_positions_dict', 
    'data_positions_dict',
    'item_sizes_dict',
    'hash_positions_counts',
    'data_positions_counts',
    'item_sizes_counts',
    'item_sizes_sum',
    'data_segment_size',
    'hash_segment_size',
])
# encoded block with the dictionary
EncodedSegment = namedtuple('EncodedSegment', [
    'data_positions_dict',
    'hash_positions_dict',
    'item_sizes_dict',
    'first_block',
    'encoded_blocks',
    'block_data_positions_bits',
    'block_hash_positions_bits',
    'block_item_sizes_bits',
    'block_bits',
    'prepend_blocks_bits',
    'segment_number',
    'segment_size',
    'segment_seed',
    'data_positions_size',
    'hash_positions_size',
    'item_sizes_size',
    'item_sizes_sum',
    'dict_size',
    'blocks_size',
    'total_size',
])

# default length of the hash segment relative to data segment
#HASH_SEGMENT_MULTIPLIER   = 4096 #16384
#HASH_SEGMENT_MULTIPLIER   = 16384
#HASH_SEGMENT_MULTIPLIER   = 2**16
HASH_SEGMENT_MULTIPLIER   = 2**16
#DEFAULT_DATA_SEGMENT_SIZE = 256
DEFAULT_DATA_SEGMENT_SIZE = 16
DEFAULT_ITEM_SIZE         = 8
ENCODED_ITEM_SIZE         = 4
TEST_IMAGE_SIZE           = 172603
# encoding functions, based on item size
ENCODING_FUNCTIONS = {
    4: base64.b16encode,
    5: base64.b32encode,
    6: base64.b64encode,
}
ENCODING_FUNCTION = ENCODING_FUNCTIONS[ENCODED_ITEM_SIZE]

def get_max_data_segment(data: ConstBitStream, segment_bit_length: int) -> int:
    """
    >>> segment_bit_length = (DEFAULT_DATA_SEGMENT_SIZE * DEFAULT_ITEM_SIZE)
    >>> data               = ConstBitStream(filename='./data/image.jpg')
    >>> max_segment        = get_max_data_segment(data, segment_bit_length)
    >>> max_segment == (len(data) // segment_bit_length)
    True
    """
    data_length = len(data)
    max_segment = data_length // segment_bit_length
    return max_segment

def create_data_segments(data: ConstBitStream, segment_bit_length: int) -> List[ConstBitStream]:
    """
    >>> segment_bit_length = (DEFAULT_DATA_SEGMENT_SIZE * DEFAULT_ITEM_SIZE)
    >>> data               = ConstBitStream(filename='./data/image.jpg')
    >>> segments           = create_data_segments(data, segment_bit_length)
    >>> len(segments) == (len(data) // segment_bit_length)
    True
    >>> (len(segments[0]) // DEFAULT_ITEM_SIZE) == DEFAULT_DATA_SEGMENT_SIZE
    True
    >>> #((len(data) % segment_bit_length) // DEFAULT_ITEM_SIZE)
    >>> #11(4) #59(32)
    """
    max_segment = get_max_data_segment(data, segment_bit_length)
    segments    = list()
    for segment_number in range(0, max_segment):
        segment = create_data_segment(data, segment_bit_length, segment_number)
        segments.append(segment)
    return segments

def create_data_segment(data: ConstBitStream, segment_bit_length: int, segment_number: int) -> ConstBitStream:
    """
    >>> segment_bit_length = (DEFAULT_DATA_SEGMENT_SIZE * DEFAULT_ITEM_SIZE)
    >>> data               = ConstBitStream(filename='./data/image.jpg')
    >>> segment_number     = 0
    >>> data_segment_0     = create_data_segment(data, segment_bit_length, segment_number)
    >>> (len(data_segment_0) // DEFAULT_ITEM_SIZE) == DEFAULT_DATA_SEGMENT_SIZE
    True
    >>> #data_segment_0[0:16]
    >>> #ConstBitStream('0xffd8')
    >>> segment_number     = 1
    >>> data_segment_1     = create_data_segment(data, segment_bit_length, segment_number)
    >>> (len(data_segment_1) // DEFAULT_ITEM_SIZE) == DEFAULT_DATA_SEGMENT_SIZE
    True
    >>> #data_segment_1[0:16]
    >>> #ConstBitStream('0x42b1')
    """
    start   = segment_bit_length * segment_number
    end     = start + segment_bit_length
    segment = data[start:end]
    return segment

def get_data_suffix(data: ConstBitStream, segment_bit_length: int) -> ConstBitStream:
    """
    >>> segment_bit_length = (DEFAULT_DATA_SEGMENT_SIZE * DEFAULT_ITEM_SIZE)
    >>> data               = ConstBitStream(filename='./data/image.jpg')
    >>> segments           = create_data_segments(data, segment_bit_length)
    >>> suffix             = get_data_suffix(data, segment_bit_length)
    >>> (len(data) % len(segments)) == len(suffix)
    True
    >>> suffix[-8:]
    ConstBitStream('0xd9')
    >>> (((len(segments) * segment_bit_length) + len(suffix)) == len(data))
    True
    """
    suffix_length = len(data) % segment_bit_length
    suffix        = data[-suffix_length:]
    return suffix

def get_hash_segment_length_by_data_segment(segment_bit_length: int) -> int:
    """
    Hash segment must be 256 times longer then data segment to make sure that we will found all data bytes
    before loaded part hash space ends (we will implement dynamic loading later)

    >>> segment_bit_length  = (DEFAULT_DATA_SEGMENT_SIZE * DEFAULT_ITEM_SIZE)
    >>> data                = ConstBitStream(filename='./data/image.jpg')
    >>> hash_segment_length = get_hash_segment_length_by_data_segment(segment_bit_length)
    >>> hash_segment_length == (segment_bit_length * HASH_SEGMENT_MULTIPLIER)
    True
    """
    return (segment_bit_length * HASH_SEGMENT_MULTIPLIER)

def load_hash_space_segment(segment_bit_length: int, segment_number: int, seed: int) -> ConstBitStream:
    """
    >>> segment_seed       = 0
    >>> segment_bit_length = (DEFAULT_DATA_SEGMENT_SIZE * DEFAULT_ITEM_SIZE)
    >>> segment_number     = 0
    >>> hash_segment_0     = load_hash_space_segment(segment_bit_length, segment_number, segment_seed)
    >>> len(hash_segment_0) == (segment_bit_length * HASH_SEGMENT_MULTIPLIER)
    True
    >>> #hash_segment_0[0:16]
    >>> #Bits('0xa6cd')
    >>> segment_number     = 1
    >>> hash_segment_1     = load_hash_space_segment(segment_bit_length, segment_number, segment_seed)
    >>> len(hash_segment_1) == (segment_bit_length * HASH_SEGMENT_MULTIPLIER)
    True
    >>> #hash_segment_1[0:16]
    >>> #Bits('0xb5d3')
    """
    hash_segment_length = get_hash_segment_length_by_data_segment(segment_bit_length)
    start_position      = (segment_number * hash_segment_length)
    segment_address     = HashItemAddress(bit_position=start_position, bit_length=hash_segment_length, seed=seed)
    return read_hash_item(segment_address)

def create_segment_matcher(data_segment: ConstBitStream, hash_segment: Bits) -> SequenceMatcher:
    """
    >>> data               = ConstBitStream(filename='./data/image.jpg')
    >>> segment_seed       = 0
    >>> segment_bit_length = (DEFAULT_DATA_SEGMENT_SIZE * DEFAULT_ITEM_SIZE)
    >>> segment_number     = 0
    >>> data_segment_0     = create_data_segment(data, segment_bit_length, segment_number)
    >>> hash_segment_0     = load_hash_space_segment(segment_bit_length, segment_number, segment_seed)
    >>> (len(data_segment_0) * HASH_SEGMENT_MULTIPLIER) == len(hash_segment_0)
    True
    >>> matcher = create_segment_matcher(data_segment_0, hash_segment_0)
    >>> blocks  = matcher.get_matching_blocks()
    >>> blocks
    >>> #len(blocks)
    >>> #55
    >>> blocks[0:2]
    [Match(a=5, b=11, size=1), Match(a=213, b=12, size=2)]
    >>> blocks[-2:]
    [Match(a=61634, b=318, size=2), Match(a=81920, b=320, size=0)]
    """
    data_string = ENCODING_FUNCTION(data_segment.tobytes())
    hash_string = ENCODING_FUNCTION(hash_segment.tobytes())
    matcher     = SequenceMatcher(None, data_string, hash_string, autojunk=False)
    return matcher

def create_segment_options(data_segment: ConstBitStream, hash_segment: Bits) -> SegmentOptions:
    """
    >>> data               = ConstBitStream(filename='./data/image.jpg')
    >>> segment_seed       = 0
    >>> segment_bit_length = (DEFAULT_DATA_SEGMENT_SIZE * DEFAULT_ITEM_SIZE)
    >>> segment_number     = 0
    >>> data_segment_0     = create_data_segment(data, segment_bit_length, segment_number)
    >>> hash_segment_0     = load_hash_space_segment(segment_bit_length, segment_number, segment_seed)
    >>> #matcher            = create_segment_matcher(data_segment_0, hash_segment_0)
    >>> #segment_blocks     = matcher.get_matching_blocks()
    >>> #segment_options    = create_segment_options(data_segment_0, hash_segment_0)
    >>> segment_options.blocks
    >>> len(segment_options.delta_blocks)
    >>> segment_options.delta_blocks
    >>> len(segment_options.delta_blocks)
    54
    >>> len(segment_options.delta_blocks) == (len(segment_blocks) - 1)
    True
    >>> segment_blocks[-2:]
    [Match(a=61634, b=318, size=2), Match(a=81920, b=320, size=0)]
    >>> segment_options.delta_blocks[-2:]
    [DeltaBlock(hash_position=23015, data_position=3, size=3), DeltaBlock(hash_position=20286, data_position=2, size=2)]
    >>> segment_blocks[0:2]
    [Match(a=5, b=11, size=1), Match(a=213, b=12, size=2)]
    >>> segment_options.delta_blocks[0:2]
    [DeltaBlock(hash_position=208, data_position=1, size=1), DeltaBlock(hash_position=3, data_position=42, size=2)]
    >>> len(segment_options.hash_positions_dict)
    23
    >>> segment_options.hash_position_length
    5
    >>> segment_options.data_position_length
    4
    >>> segment_options.item_size_length
    3
    >>> segment_options.delta_block_length
    12
    >>> segment_options.hash_positions_dict
    {0: 3, 1: 6, 2: 5, 3: 208, 4: 10, 5: 35, 6: 7, 7: 8, 8: 9, 9: 57, 10: 13, 11: 133, 12: 1, 13: 2, 14: 23, 15: 101, 16: 747, 17: 118, 18: 6026, 19: 12, 20: 31064, 21: 23015, 22: 20286}
    >>> len(segment_options.data_positions_dict)
    13
    >>> segment_options.data_positions_dict[12]
    6
    >>> segment_options.data_positions_dict
    {0: 1, 1: 7, 2: 2, 3: 3, 4: 42, 5: 22, 6: 17, 7: 5, 8: 10, 9: 91, 10: 46, 11: 11, 12: 6}
    >>> segment_options.item_sizes_dict
    {0: 1, 1: 2, 2: 3, 3: 0}
    >>> segment_options.hash_positions_counts.most_common(5)
    [(3, 4), (6, 3), (5, 2), (208, 1), (10, 1)]
    >>> segment_options.data_positions_counts.most_common(5)
    [(1, 8), (7, 5), (2, 5), (3, 2), (42, 1)]
    >>> segment_options.item_sizes_counts.most_common(5)
    [(1, 18), (2, 8), (3, 3), (0, 1)]
    >>> (segment_options.data_segment_size // 8) == DEFAULT_DATA_SEGMENT_SIZE
    True
    >>> (segment_options.hash_segment_size // 8) == (DEFAULT_DATA_SEGMENT_SIZE * HASH_SEGMENT_MULTIPLIER)
    True
    >>> segment_options.first_block
    Match(a=5, b=11, size=1)
    >>> segment_options.blocks

    """
    hash_positions = list()
    data_positions = list()
    item_sizes     = list()
    # get matching blocks for segment
    matcher        = create_segment_matcher(data_segment, hash_segment)
    blocks         = matcher.get_matching_blocks()
    first_block    = copy.copy(blocks[0])
    item_sizes_sum = 0

    # collect matched blocks values
    for block in blocks:
        hash_positions.append(block.a)
        data_positions.append(block.b)
        item_sizes.append(block.size)
        item_sizes_sum += block.size
    # converted blocks
    delta_blocks      = list()
    # delta encoded hash positions 
    de_hash_positions = delta_encode(hash_positions).values
    # delta encoded data positions
    de_data_positions = delta_encode(data_positions).values
    # item sizes not need to be encoded
    de_item_sizes     = item_sizes
    # convert matched blocks to delta encoding
    for block_number in range(0, len(blocks)-1):
        delta_block = DeltaBlock(de_hash_positions[block_number], de_data_positions[block_number], de_item_sizes[block_number])
        delta_blocks.append(delta_block)
    # unique delta position values
    unique_delta_hash_positions = set(de_hash_positions)
    unique_delta_data_positions = set(de_data_positions)
    unique_delta_item_sizes     = set(de_item_sizes)
    
    # delta block parts length
    hash_position_length = get_min_bit_length(len(unique_delta_hash_positions))
    data_position_length = get_min_bit_length(len(unique_delta_data_positions))
    item_size_length     = get_min_bit_length(len(unique_delta_item_sizes))
    # single delta block length
    delta_block_length   = hash_position_length + data_position_length + item_size_length
    
    # create length stats
    hash_positions_counts = Counter(de_hash_positions)
    data_positions_counts = Counter(de_data_positions)
    item_sizes_counts     = Counter(de_item_sizes)
    
    # create dictionary for every block parameter
    hash_positions      = [hash_position[0] for hash_position in hash_positions_counts.most_common()]
    hash_positions_dict = dict(zip(range(0, len(hash_positions)), hash_positions))
    data_positions      = [data_position[0] for data_position in data_positions_counts.most_common()]
    data_positions_dict = dict(zip(range(0, len(data_positions)), data_positions))
    item_sizes          = [item_size[0] for item_size in item_sizes_counts.most_common()]
    item_sizes_dict     = dict(zip(range(0, len(item_sizes)), item_sizes))

    # define data and hash segment sizes
    data_segment_size = len(data_segment)
    hash_segment_size = len(hash_segment)

    # creating final result object
    segment_options = SegmentOptions(
        matcher,
        first_block,
        blocks,
        delta_blocks, 
        delta_block_length,
        hash_position_length,
        data_position_length,
        item_size_length,
        hash_positions_dict,
        data_positions_dict,
        item_sizes_dict,
        hash_positions_counts,
        data_positions_counts,
        item_sizes_counts,
        item_sizes_sum,
        data_segment_size,
        hash_segment_size,
    )
    return segment_options

def encode_segment_value(data_segment: ConstBitStream, segment_number: int, segment_seed: int) -> EncodedSegment:
    """
    >>> data               = ConstBitStream(filename='./data/image.jpg')
    >>> segment_seed       = 0
    >>> segment_bit_length = (DEFAULT_DATA_SEGMENT_SIZE * DEFAULT_ITEM_SIZE)
    >>> segment_number     = 0
    >>> #data_segment       = create_data_segment(data, segment_bit_length, segment_number)
    >>> #encoded_value      = encode_segment_value(data_segment, segment_number, segment_seed)
    >>> encoded_value.data_positions_dict
    BitArray('0x010305040206071108250c610b')
    >>> encoded_value.hash_positions_dict
    BitArray('0x030605d0010a23070809390d850101021765eb05768a2f0cd8f201e7b301be9e01')
    >>> encoded_value.item_sizes_dict
    BitArray('0x01020300')
    >>> encoded_value.encoded_blocks
    BitArray('0x01800210082182803000a93b04000b94905425900496081086d0088759780800062108880911991a00a9ab11')
    >>> encoded_value.block_hash_positions_bits
    5
    >>> encoded_value.block_data_positions_bits
    4
    >>> encoded_value.block_item_sizes_bits
    3
    >>> encoded_value.block_bits
    12
    >>> encoded_value.hash_positions_size
    264
    >>> encoded_value.data_positions_size
    104
    >>> encoded_value.item_sizes_size
    32
    >>> encoded_value.dict_size
    400
    >>> encoded_value.dict_size == (encoded_value.data_positions_size + encoded_value.hash_positions_size + encoded_value.item_sizes_size)
    True
    >>> encoded_value.blocks_size
    352
    >>> encoded_value.total_size
    752
    >>> encoded_value.total_size == (encoded_value.blocks_size + encoded_value.dict_size)
    True
    >>> encoded_value.prepend_blocks_bits
    4
    >>> encoded_value.segment_number
    0
    >>> encoded_value.segment_size
    2048
    >>> encoded_value.segment_seed
    0
    >>> encoded_value.first_block
    Match(a=5, b=11, size=1)
    """
    segment_bit_length = len(data_segment)
    hash_segment       = load_hash_space_segment(segment_bit_length, segment_number, segment_seed)
    segment            = create_segment_options(data_segment, hash_segment)
    item_sizes_sum     = 0
    
    # encode data positions
    # TODO: more optimal encoding
    encoded_data_positions = BitArray()
    data_position_indexes  = dict()
    for i in range(0, len(segment.data_positions_dict)):
        data_position         = segment.data_positions_dict[i]
        encoded_data_position = varint.encode(data_position)
        encoded_data_positions.append(BitArray(bytes=encoded_data_position))
        data_position_indexes[data_position] = i
    # encode hash positions
    # TODO: more optimal encoding
    encoded_hash_positions = BitArray()
    hash_position_indexes  = dict()
    for i in range(0, len(segment.hash_positions_dict)):
        hash_position         = segment.hash_positions_dict[i]
        encoded_hash_position = varint.encode(hash_position)
        encoded_hash_positions.append(BitArray(bytes=encoded_hash_position))
        hash_position_indexes[hash_position] = i
    # encode item sizes
    # TODO: more optimal encoding
    encoded_item_sizes = BitArray()
    item_size_indexes  = dict()
    for i in range(0, len(segment.item_sizes_dict)):
        item_size          = segment.item_sizes_dict[i]
        encoded_item_size  = varint.encode(item_size)
        encoded_item_sizes.append(BitArray(bytes=encoded_item_size))
        item_size_indexes[item_size] = i
        item_sizes_sum += item_size
    
    # encode delta blocks
    encoded_blocks = BitArray()
    for delta_block in segment.delta_blocks:
        encoded_block = BitArray()
        # encode hash_position
        hash_position         = delta_block.hash_position
        encoded_hash_position = BitArray(uint=hash_position_indexes[hash_position], length=segment.hash_position_length)
        encoded_block.append(encoded_hash_position)
        # encode data position
        data_position         = delta_block.data_position
        encoded_data_position = BitArray(uint=data_position_indexes[data_position], length=segment.data_position_length)
        encoded_block.append(encoded_data_position)
        # encode item_size
        item_size = delta_block.size
        encoded_item_size = BitArray(uint=item_size_indexes[item_size], length=segment.item_size_length)
        encoded_block.append(encoded_item_size)
        # add block to block list
        encoded_blocks.append(encoded_block)
    
    # byte-align encoded blocks
    prepend_blocks_bits = len(encoded_blocks) % 8
    if (prepend_blocks_bits > 0):
        encoded_blocks.prepend(BitArray(bin='0b0'*prepend_blocks_bits))
    
    # TODO: encode data suffix

    # calculate encoded size
    segment_size        = len(data_segment)
    data_positions_size = len(encoded_data_positions)
    hash_positions_size = len(encoded_hash_positions)
    item_sizes_size     = len(encoded_item_sizes)
    dict_size           = data_positions_size + hash_positions_size + item_sizes_size
    blocks_size         = len(encoded_blocks)
    total_size          = dict_size + blocks_size

    # metadata to be appended to the encoded segment
    block_data_positions_bits = segment.data_position_length
    block_hash_positions_bits = segment.hash_position_length
    block_item_sizes_bits     = segment.item_size_length
    encoded_block_bits        = segment.delta_block_length
    first_block               = segment.first_block

    # result as varint-encoded bytes
    return EncodedSegment(
        encoded_data_positions,
        encoded_hash_positions,
        encoded_item_sizes,
        first_block,
        encoded_blocks,
        block_data_positions_bits,
        block_hash_positions_bits,
        block_item_sizes_bits,
        encoded_block_bits,
        prepend_blocks_bits,
        segment_number,
        segment_size,
        segment_seed,
        data_positions_size,
        hash_positions_size,
        item_sizes_size,
        item_sizes_sum,
        dict_size,
        blocks_size,
        total_size,
    )

def decode_segment_value(segment: EncodedSegment) -> Bits:
    # restore hash space segment
    hash_segment   = load_hash_space_segment(segment.segment_size, segment.segment_number, segment.segment_seed)
    # read & decode dicts
    # data positions
    data_positions = decode_varint_bytes(segment.data_positions_dict)
    data_positions_dict    = dict()
    data_positions_indexes = dict()
    for i in range(0, len(data_positions)):
        data_position = data_positions[i]
        data_positions_dict[i]                = data_position
        data_positions_indexes[data_position] = i
    # hash_positions
    hash_positions = decode_varint_bytes(segment.hash_positions_dict)
    hash_positions_dict    = dict()
    hash_positions_indexes = dict()
    for i in range(0, len(hash_positions)):
        hash_position = hash_positions[i]
        hash_positions_dict[i]                = hash_position
        hash_positions_indexes[hash_position] = i
    # item_sizes
    item_sizes         = decode_varint_bytes(segment.item_sizes_dict)
    item_sizes_dict    = dict()
    item_sizes_indexes = dict()
    for i in range(0, len(item_sizes)):
        item_size = item_sizes[i]
        item_sizes_dict[i]                = item_size
        item_sizes_indexes[item_size] = i
    
    # read blocks
    start_blocks   = segment.prepend_blocks_bits
    end_blocks     = len(segment.blocks)
    encoded_blocks = segment.blocks[start_blocks:end_blocks]
    block_bits     = segment.block_bits
    max_block      = len(encoded_blocks) // block_bits
    
    # decode blocks
    hash_position_bits = segment.block_hash_positions_bits
    data_position_bits = segment.block_data_positions_bits
    item_size_bits     = segment.block_item_sizes_bits
    block_format       = f"uint:{hash_position_bits},uint:{data_position_bits},uint:{item_size_bits}"
    hash_positions     = list()
    data_positions     = list()
    item_sizes         = list()
    delta_blocks       = list()
    for block_number in range(0, max_block):
        start = block_number * block_bits
        end   = start + block_bits
        block = encoded_blocks[start:end]
        # decode value ids
        hash_position_id, data_position_id, item_size_id = block.readlist(block_format)
        # get actial block values
        hash_position = hash_positions_indexes[hash_position_id]
        data_position = data_positions_indexes[data_position_id]
        item_size     = item_sizes_indexes[item_size_id]
        # collect all values "by column" for delta-decoding
        hash_positions.append(hash_position)
        data_positions.append(data_position)
        item_sizes.append(item_size)
        # collect delta blocks
        delta_blocks.append(DeltaBlock(hash_position, data_position, item_size))
    # decoding positions
    hash_positions = delta_decode(hash_positions)
    data_positions = delta_decode(data_positions)
    # create delta blocks
    blocks = list()
    
        # block = DeltaBlock(hash_position, data_position, item_size)
        # delta_blocks.append(delta_block)
    
    # return decoded segment
    #return delta_blocks