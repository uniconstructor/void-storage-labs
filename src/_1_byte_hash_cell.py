from __future__ import annotations
from rich import print
from rich.pretty import pprint
from tqdm.notebook import tqdm
from custom_counter import CustomCounter as Counter, init_byte_counter, ConsumableCounter
from collections import defaultdict, ChainMap, deque
from collections.abc import Iterable, Callable, Hashable, Generator,\
    ItemsView, KeysView, ValuesView, MappingView,\
    Mapping, MutableMapping,\
    Sequence, MutableSequence
from bitarray import bitarray, frozenbitarray
from bitarray.util import ba2int, int2ba, huffman_code, zeros, intervals
from sortedcontainers import SortedSet
from typing import List, Set, Dict, Tuple, Optional, Union, Iterable
from dataclasses import dataclass, field
from operator import attrgetter
from copy import deepcopy, copy
#from delta_of_delta import delta_encode, delta_decode, delta_of_delta_encode, delta_of_delta_decode
from lolviz import objviz, lolviz, listviz, treeviz
from cycle_gen import CMWC
import math
import xxhash
import functools
from itertools import chain
from more_itertools import split_at, mark_ends, stagger, bucket, windowed
from enum import Enum, IntEnum
# https://realpython.com/lru-cache-python/
from functools import lru_cache

DEFAULT_ENDIAN = 'big'

NEXT_BYTES_CACHE : Dict[int, Sequence[int]] = dict()
PREV_BYTES_CACHE : Dict[int, Sequence[int]] = dict()

def init_rainbow_paths() -> Dict[int]:
    generator   = CMWC(x=0)
    byte_values = range(0, 256)
    for byte_value in byte_values:
        generator.seed(seed=byte_value)
        next_bytes = generator.sample(byte_values, k=8)
        NEXT_BYTES_CACHE[byte_value] = next_bytes.copy()
    for byte_value in byte_values:
        prev_bytes = list()
        for prev_byte, next_bytes in NEXT_BYTES_CACHE.items():
            if (byte_value in next_bytes):
                prev_bytes.append(prev_byte)
        PREV_BYTES_CACHE[byte_value] = prev_bytes.copy()
#init_rainbow_paths()

def init_xor_paths() -> Dict[int, Sequence[int]]:
    distance_counts = defaultdict(Counter)
    target_counts   = Counter()
    for byte_id in range(0, 256):
        byte_data = int2ba(byte_id, length=8, endian=DEFAULT_ENDIAN, signed=False)
        for next_byte_id in range(0, 256):
            next_byte_data = int2ba(next_byte_id, length=8, endian=DEFAULT_ENDIAN, signed=False)
            xor_byte_data  = byte_data ^ next_byte_data
            distance       = xor_byte_data.count(1)
            distance_counts[byte_id].update({ next_byte_id: distance })
        inverted_byte_id = list(distance_counts[byte_id].with_count(8).keys())[0]
        #NEXT_BYTES_CACHE[byte_id] = [byte_id] + list(distance_counts[byte_id].with_count(7).keys())[0:6] + [inverted_byte_id]
        #next_bytes                = [byte_id] + list(distance_counts[byte_id].with_count(7).keys())[0:6] + [inverted_byte_id]
        #next_bytes                = list(distance_counts[byte_id].with_count(1).keys())[0:8]
        # try to pick next bytes evenly, trying to keep average number of prev items equal
        next_targets = list()
        next_targets = list(distance_counts[byte_id].with_count(1).keys())
        next_targets = sorted([(target_counts[_nt], _nt) for _nt in next_targets])
        #next_bytes   = sorted([_nt[1] for _nt in next_targets[0:8]])
        if (byte_id not in next_targets):
            next_bytes = [byte_id] + [_nt[1] for _nt in next_targets[0:7]]
        else:
            next_targets.remove(byte_id)
            next_bytes = [byte_id] + [_nt[1] for _nt in next_targets[0:7]]
        #next_targets = list()
        #next_targets = list(distance_counts[byte_id].with_count(4).keys())
        #next_targets = sorted([(target_counts[_nt], _nt) for _nt in next_targets])
        #next_bytes   = [byte_id] + sorted([_nt[1] for _nt in next_targets[0:6]]) + [inverted_byte_id]
        NEXT_BYTES_CACHE[byte_id] = next_bytes
        for nb in next_bytes:
            target_counts.update({ nb: 1 })
        #NEXT_BYTES_CACHE[byte_id] = list(distance_counts[byte_id].with_count(7).keys())[0:4] + list(distance_counts[byte_id].with_count(1).keys())[0:4]
        #start_item_id = byte_id % 62
        #end_item_id   = start_item_id + 8
        #NEXT_BYTES_CACHE[byte_id] = list(distance_counts[byte_id].with_count(4).keys())[start_item_id:end_item_id]
    for byte_value in range(0, 256):
        prev_bytes = list()
        for prev_byte, next_bytes in NEXT_BYTES_CACHE.items():
            if (byte_value in next_bytes):
                prev_bytes.append(prev_byte)
        PREV_BYTES_CACHE[byte_value] = sorted(prev_bytes.copy())
    pprint(target_counts.aggregated_counts().first_items())
init_xor_paths()

@lru_cache(maxsize=256)
def has_prev_byte(byte_value: int, prev_byte: int) -> bool:
    return (prev_byte in PREV_BYTES_CACHE[byte_value])

@lru_cache(maxsize=256)
def has_next_byte(byte_value: int, next_byte: int) -> bool:
    return (next_byte in NEXT_BYTES_CACHE[byte_value])

@lru_cache(maxsize=2**16)
def has_all_next_bytes(byte_value: int, next_bytes: frozenset[int]) -> bool:
    for next_byte in next_bytes:
        if (has_next_byte(byte_value=byte_value, next_byte=next_byte) is False):
            return False
    return True

@lru_cache(maxsize=256)
def get_prev_bytes(byte_value: int) -> Sequence[int]:
    return PREV_BYTES_CACHE[byte_value]

@lru_cache(maxsize=256)
def get_next_bytes(byte_value: int) -> Sequence[int]:
    return NEXT_BYTES_CACHE[byte_value]

@lru_cache(maxsize=2**16)
def has_next_byte_id(byte_value: int, next_byte_id: int) -> bool:
    if (next_byte_id < 0):
        raise Exception(f"Incorrect next_byte_id={next_byte_id}")
    next_bytes        = get_next_bytes(byte_value=byte_value)
    next_bytes_length = len(next_bytes)
    return (next_byte_id < next_bytes_length)

@lru_cache(maxsize=2**16)
def get_next_byte_by_id(byte_value: int, next_byte_id: int) -> int:
    if (has_next_byte_id(byte_value=byte_value, next_byte_id=next_byte_id) is False):
        raise Exception(f"byte_value={byte_value} don't have next_byte_id={next_byte_id}")
    return get_next_bytes(byte_value=byte_value)[next_byte_id]

@lru_cache(maxsize=2**16)
def get_prev_byte_id(byte_value: int, prev_byte: int) -> int:
    if (has_prev_byte(byte_value=byte_value, prev_byte=prev_byte) is False):
        raise Exception(f"byte_value={byte_value} don't have prev_byte={prev_byte}")
    prev_bytes = get_prev_bytes(byte_value=byte_value)
    for prev_byte_id in range(0, len(prev_bytes)):
        current_byte = prev_bytes[prev_byte_id]
        if (prev_byte == current_byte):
            return prev_byte_id
    raise Exception(f"byte_value={byte_value} don't have prev_byte={prev_byte}")

@lru_cache(maxsize=2**16)
def get_next_byte_id(byte_value: int, next_byte: int) -> int:
    if (has_next_byte(byte_value=byte_value, next_byte=next_byte) is False):
        raise Exception(f"byte_value={byte_value} don't have next_byte={next_byte}")
    next_bytes = get_next_bytes(byte_value=byte_value)
    for next_byte_id in range(0, len(next_bytes)):
        current_byte = next_bytes[next_byte_id]
        if (next_byte == current_byte):
            return next_byte_id
    raise Exception(f"byte_value={byte_value} don't have next_byte={next_byte}")

@lru_cache(maxsize=2**16)
def get_seeds_for_next_bytes(next_bytes: frozenset[int] | Tuple[int, int]) -> Dict[int, List[Tuple[int, int]]]:
    result = dict()
    for seed_value in range(0, 256):
        if (has_all_next_bytes(byte_value=seed_value, next_bytes=next_bytes)):
            next_byte_ids = list()
            for next_byte in next_bytes:
                next_byte_id   = get_next_byte_id(byte_value=seed_value, next_byte=next_byte)
                next_byte_item = (next_byte, next_byte_id)
                next_byte_ids.append(next_byte_item)
            result[seed_value] = next_byte_ids
    return result

@lru_cache(maxsize=2**16)
def has_common_seed(next_bytes: frozenset[int] | Tuple[int, int]) -> bool:
    for seed_value in range(0, 256):
        if (has_all_next_bytes(byte_value=seed_value, next_bytes=next_bytes)):
            return True
    return False

#@lru_cache(maxsize=2**16)
def has_common_seed_for_head_seed_pointer(seed_byte: int, pointer_byte: int) -> bool:
    return has_common_seed(next_bytes=(seed_byte, pointer_byte))

#@lru_cache(maxsize=2**16)
def get_seeds_for_head_seed_pointer(seed_byte: int, pointer_byte: int) -> Dict[int, List[Tuple[int, int]]]:
    return get_seeds_for_next_bytes(next_bytes=(seed_byte, pointer_byte))

#@lru_cache(maxsize=2**16)
def has_common_seed_for_expansion_byte_pointer(left_pointer_byte: int, right_pointer_byte: int) -> bool:
    return has_common_seed(next_bytes=(left_pointer_byte, right_pointer_byte))

#@lru_cache(maxsize=2**16)
def get_seeds_for_expansion_byte_pointer(left_pointer_byte: int, right_pointer_byte: int) -> Dict[int, List[Tuple[int, int]]]:
    return get_seeds_for_next_bytes(next_bytes=(left_pointer_byte, right_pointer_byte))

@lru_cache(maxsize=2**16)
def has_common_seed_for_tail_seed_pointer(seed_byte: int, pointer_byte: int) -> bool:
    for prev_seed_value in range(0, 256):
        if (has_next_byte(byte_value=prev_seed_value, next_byte=seed_byte)):
            if (has_next_byte(byte_value=seed_byte, next_byte=pointer_byte)):
                return True
    return False

@lru_cache(maxsize=2**16)
def get_seeds_for_tail_seed_pointer(seed_byte: int, pointer_byte: int) -> Dict[int, List[Tuple[int, int]]]:
    result = dict()
    for prev_seed_value in range(0, 256):
        if (has_next_byte(byte_value=prev_seed_value, next_byte=seed_byte)):
            if (has_next_byte(byte_value=seed_byte, next_byte=pointer_byte)):
                next_byte_ids = list()
                seed_item     = (seed_byte, get_next_byte_id(byte_value=prev_seed_value, next_byte=seed_byte))
                pointer_item  = (pointer_byte, get_next_byte_id(byte_value=seed_byte, next_byte=pointer_byte))
                next_byte_ids.append(seed_item)
                next_byte_ids.append(pointer_item)
                result[prev_seed_value] = next_byte_ids
    return result

@lru_cache(maxsize=256)
def get_seeds_for_data_pointer(data_byte: int) -> Dict[int, List[Tuple[int, int]]]:
    result = dict()
    for data_seed_value in range(0, 256):
        if (has_next_byte(byte_value=data_seed_value, next_byte=data_byte)):
            for start_seed_value in get_prev_bytes(byte_value=data_seed_value):
                next_byte_ids        = list()
                data_seed_value_item = (data_seed_value, get_next_byte_id(byte_value=start_seed_value, next_byte=data_seed_value))
                data_byte_item       = (data_byte, get_next_byte_id(byte_value=data_seed_value, next_byte=data_byte))
                next_byte_ids.append(data_seed_value_item)
                next_byte_ids.append(data_byte_item)
            result[start_seed_value] = next_byte_ids
    return result

def resolve_data_pointer(seed_byte: int, next_seed_byte_id: int, data_byte_id: int) -> Tuple[int]:
    next_seed_byte = get_next_byte_by_id(byte_value=seed_byte, next_byte_id=next_seed_byte_id)
    data_byte      = get_next_byte_by_id(byte_value=next_seed_byte, next_byte_id=data_byte_id)
    return (data_byte, )

def resolve_head_seed_pointer(seed_byte: int, next_seed_byte_id: int, next_pointer_byte_id: int) -> Tuple[int, int]:
    next_seed_byte    = get_next_byte_by_id(byte_value=seed_byte, next_byte_id=next_seed_byte_id)
    next_pointer_byte = get_next_byte_by_id(byte_value=seed_byte, next_byte_id=next_pointer_byte_id)
    return (next_seed_byte, next_pointer_byte)

def resolve_tail_seed_pointer(seed_byte: int, next_seed_byte_id: int, next_pointer_byte_id: int) -> Tuple[int, int]:
    next_seed_byte    = get_next_byte_by_id(byte_value=seed_byte, next_byte_id=next_seed_byte_id)
    next_pointer_byte = get_next_byte_by_id(byte_value=next_seed_byte, next_byte_id=next_pointer_byte_id)
    return (next_seed_byte, next_pointer_byte)

def resolve_expansion_byte_pointer(seed_byte: int, left_pointer_byte_id: int, right_pointer_byte_id: int) -> Tuple[Tuple[int, int], Tuple[int, int]]:
    left_pointer_byte  = get_next_byte_by_id(byte_value=seed_byte, next_byte_id=left_pointer_byte_id)
    right_pointer_byte = get_next_byte_by_id(byte_value=seed_byte, next_byte_id=right_pointer_byte_id)
    left_bytes         = (seed_byte, left_pointer_byte)
    right_bytes        = (seed_byte, right_pointer_byte)
    return (left_bytes, right_bytes)

