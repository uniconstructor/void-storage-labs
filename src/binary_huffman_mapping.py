from __future__ import annotations
from rich import print
from rich.pretty import pprint
from rich.text import Text
from rich.progress import track,\
    BarColumn, Progress, Task, TaskID, TextColumn, TimeElapsedColumn, TimeRemainingColumn,\
    MofNCompleteColumn, RenderableColumn, SpinnerColumn, TransferSpeedColumn, FileSizeColumn, ProgressColumn
from rich.layout import Layout
from rich.columns import Columns
from rich.text import Text
from custom_rich import CustomTaskProgressColumn as TaskProgressColumn
from tqdm.notebook import tqdm
from custom_counter import CustomCounter as Counter, init_byte_counter, ConsumableCounter
from collections import defaultdict, ChainMap, deque
from collections.abc import Iterable, Callable, Hashable, Generator,\
    ItemsView, KeysView, ValuesView, MappingView,\
    Mapping, MutableMapping,\
    Sequence, MutableSequence
from bitarray import bitarray, frozenbitarray
from bitarray.util import ba2int, int2ba, huffman_code,\
    vl_encode, vl_decode, sc_encode, sc_decode, serialize, zeros, intervals
from sortedcontainers import SortedSet, SortedDict
from typing import List, Set, Dict, Tuple, Optional, Union, Iterable
import dataclasses
from dataclasses import dataclass, field
from operator import attrgetter
from enum import Enum, IntEnum
from copy import deepcopy, copy
from delta_of_delta import delta_encode, delta_decode, delta_of_delta_encode, delta_of_delta_decode
from lolviz import objviz, lolviz, listviz, treeviz
from cycle_gen import CMWC
import math
import xxhash
import functools
from itertools import chain
from more_itertools import split_at

from hash_range_iterator import nounce_to_input_bytes
from fib_encoder import fib_encode_position, fib_decode_position, get_fib_lengths, get_fib_value_range, get_fib_position_ranges
from block_types import DEFAULT_ENDIAN, SectionType,\
    create_canonical_bitmap_groups, pick_digest_bytes_from_bitmap

INPUT_BYTES_LENGTH  = 3
OUTPUT_BYTES_LENGTH = 4

BITMAP_LENGTH_CODES = {
    1 : frozenbitarray('00'),
    2 : frozenbitarray('01'),
    3 : frozenbitarray('1'),
}

BITMAP_CODE_LENGTHS = {
    frozenbitarray('00') : 1,
    frozenbitarray('01') : 2,
    frozenbitarray('1')  : 3,
}

INPUT_BITMAPS_BY_COST = {
    0: [
        frozenbitarray('1'),
        frozenbitarray('11'),
        frozenbitarray('111'),
        #frozenbitarray('1111'),
    ],
    1: [
        frozenbitarray('0'),
        frozenbitarray('10'),
        frozenbitarray('01'),
        frozenbitarray('110'),
        frozenbitarray('101'),
        frozenbitarray('011'),
        #frozenbitarray('1110'),
        #frozenbitarray('1101'),
        #frozenbitarray('1011'),
        #frozenbitarray('0111'),
    ],
    2: [
        frozenbitarray('00'),
        frozenbitarray('100'),
        frozenbitarray('010'),
        frozenbitarray('001'),
        #frozenbitarray('1100'),
        #frozenbitarray('1010'),
        #frozenbitarray('1001'),
        #frozenbitarray('0110'),
        #frozenbitarray('0101'),
        #frozenbitarray('0011'),
    ],
    3: [
        frozenbitarray('000'),
        #frozenbitarray('1000'),
        #frozenbitarray('0100'),
        #frozenbitarray('0010'),
        #frozenbitarray('0001'),
    ],
}

INPUT_BITMAPS_BY_INPUT_LENGTH = {
    1: [
        frozenbitarray('1'),
        frozenbitarray('0'),
    ],
    2: [
        frozenbitarray('11'),
        frozenbitarray('10'),
        frozenbitarray('01'),
        frozenbitarray('00'),
    ],
    3: [
        frozenbitarray('111'),
        frozenbitarray('110'),
        frozenbitarray('101'),
        frozenbitarray('011'),
        frozenbitarray('100'),
        frozenbitarray('010'),
        frozenbitarray('001'),
        frozenbitarray('000'),
    ],
    #4: {
        #frozenbitarray('1111'),
        #frozenbitarray('1110'),
        #frozenbitarray('1101'),
        #frozenbitarray('1011'),
        #frozenbitarray('0111'),
        #frozenbitarray('1100'),
        #frozenbitarray('1010'),
        #frozenbitarray('1001'),
        #frozenbitarray('0110'),
        #frozenbitarray('0101'),
        #frozenbitarray('0011'),
        #frozenbitarray('1000'),
        #frozenbitarray('0100'),
        #frozenbitarray('0010'),
        #frozenbitarray('0001'),
        #frozenbitarray('0000'),
    #}
}

OUTPUT_LENGTH_OPTIONS_BY_INPUT_COST = {
    0: [1, 2, 3], # , 4
    1: [2, 3], #, 4
    2: [3], # , 4
    3: [4],
}

OUTPUT_BITMAP_CODES_BY_OUTPUT_LENGTH = {
    1: {
        frozenbitarray('1000'): frozenbitarray('00'), 
        frozenbitarray('0100'): frozenbitarray('01'), 
        frozenbitarray('0010'): frozenbitarray('10'), 
        frozenbitarray('0001'): frozenbitarray('11'),
    },
    2: {
        frozenbitarray('1100'): frozenbitarray('00'),
        frozenbitarray('0011'): frozenbitarray('01'),
        frozenbitarray('0110'): frozenbitarray('100'),
        frozenbitarray('1010'): frozenbitarray('101'),
        frozenbitarray('0101'): frozenbitarray('110'),
        frozenbitarray('1001'): frozenbitarray('111'),
    },
    3: {
        frozenbitarray('1110'): frozenbitarray('00'),
        frozenbitarray('1101'): frozenbitarray('01'),
        frozenbitarray('1011'): frozenbitarray('10'),
        frozenbitarray('0111'): frozenbitarray('11'),
    },
    4: {
        frozenbitarray('1111'): frozenbitarray(''),
    }
}

SEED_INPUT_LENGTH_OPTIONS_BY_MAPPING_COST = {
    1: [1, 2, 3],
    2: [1, 2],
    3: [1],
}

OUTPUT_LENGTH_OPTIONS_BY_INPUT_LENGTH = {
    1: [2, 3],
    2: [3],
}

SEED_RANGES_BY_SEED_INPUT_LENGTH = {
    1: (0, 2**8),
    2: (2**8, 2**8 + 2**16),
    3: (2**8 + 2**16, 2**8 + 2**16 + 2**24),
}


def generate_hash_input_range(input_length: int) -> Iterable[int]:
    return range(2**(8*input_length))

def get_hash_output_bitmaps() -> Dict[int, List[bitarray]]:
    return create_canonical_bitmap_groups(section_type=SectionType.POINT_SECTION)

def get_next_data_bits(data: bitarray, start_byte_id: int, output_length: int, input_length: int) -> bitarray:
    max_bytes   = output_length + input_length
    end_byte_id = start_byte_id + max_bytes
    return data[start_byte_id*8:end_byte_id*8]

def get_next_data_input_bytes(next_data_bytes: bitarray, output_length: int, input_length: int) -> bytes:
    if (len(next_data_bytes) < (output_length + input_length)*8):
        raise Exception(f"Too short next_data_bytes={next_data_bytes.to01()} ({len(next_data_bytes)} -> {len(next_data_bytes) // 8}), output_length={output_length}, input_length={input_length}")
    start_bit = 0
    end_bit   = output_length*8 + input_length*8 
    return next_data_bytes[start_bit:end_bit].tobytes()

def get_next_data_output_bytes(next_data_bytes: bitarray, output_length: int) -> bytes:
    return next_data_bytes[0:output_length*8].tobytes()

def get_output_targets(output_bytes: bytes) -> Dict[int, Dict[frozenbitarray, bytes]]:
    output_targets = defaultdict(dict)
    output_bitmaps = get_hash_output_bitmaps()
    # we must take at least 1 byte from the hash output digest
    del output_bitmaps[0]
    for output_length, length_bitmaps in output_bitmaps.items():
        for target_bitmap in length_bitmaps:
            output_targets[output_length][target_bitmap] = pick_digest_bytes_from_bitmap(digest=output_bytes, bitmap=target_bitmap)
    return output_targets

def get_input_targets(input_bytes: bytes) -> Dict[int, Dict[int, Dict[frozenbitarray, bytes]]]:
    input_targets = defaultdict(lambda: defaultdict(dict))
    for input_length in [1, 2, 3]: # , 4
        length_bitmaps = INPUT_BITMAPS_BY_INPUT_LENGTH[input_length]
        for target_bitmap in length_bitmaps:
            target_length = target_bitmap.count(1)
            input_targets[input_length][target_length][target_bitmap] = input_bytes[0:target_length]
    return input_targets

def create_hash_input_options(input_length: int, bitmap: frozenbitarray, input_bytes: bytes=bytes()) -> Dict[int, Sequence[int]]:
    if (len(input_bytes) < bitmap.count(1)):
        raise Exception(f"Not enough input_bytes={input_bytes.hex()} ({len(input_bytes)}). Target input bitmap={bitmap.to01()} requires at least {bitmap.count(1)} input bytes, but {len(input_bytes)} bytes given.")
    target_bytes         = input_bytes[0:bitmap.count(1)]
    target_byte_position = 0
    byte_options         = dict()
    for byte_position in range(input_length):
        #target_byte = input_bytes[byte_position]
        if (bitmap[byte_position] == 1):
            target_byte                 = target_bytes[target_byte_position]
            byte_options[byte_position] = [target_byte]
            target_byte_position       += 1
        else:
            #position_options            = split_at(range(256), lambda x: x == target_byte)
            #byte_options[byte_position] = list(chain(*position_options))
            byte_options[byte_position] = list(range(256))
    return byte_options

def create_hash_inputs(input_length: int, bitmap: frozenbitarray, input_bytes: bytes=bytes()) -> Generator[Sequence[bytes], None, None]: #:
    input_byte_options = create_hash_input_options(input_length=input_length, input_bytes=input_bytes, bitmap=bitmap)
    for byte_0 in input_byte_options[0]:
        if (input_length == 1):
            hash_input = bytes([byte_0])
            yield hash_input
            continue
        for byte_1 in input_byte_options[1]:
            if (input_length == 2):
                hash_input = bytes([byte_0, byte_1])
                yield hash_input
                continue
            for byte_2 in input_byte_options[2]:
                if (input_length == 3):
                    hash_input = bytes([byte_0, byte_1, byte_2])
                    yield hash_input
                    continue
                #for byte_3 in input_byte_options[3]:
                    #if (input_length == 4):
                    #hash_input = bytes([byte_0, byte_1, byte_2, byte_3])
                    #yield hash_input

def get_hash_outputs_by_output_length(hash_input: bytes, output_length: int, seed: int) -> Dict[frozenbitarray, bytes]:
    digest = xxhash.xxh32_digest(hash_input, seed=seed)
    if (output_length == 4):
        return {
            frozenbitarray('1111') : digest
        }
    outputs        = dict()
    length_bitmaps = get_output_bitmaps_by_output_length(output_length=output_length)
    for bitmap in length_bitmaps:
        bitmap_value    = pick_digest_bytes_from_bitmap(digest=digest, bitmap=bitmap)
        outputs[bitmap] = bitmap_value
    return outputs

@functools.lru_cache()
def get_output_bitmaps_by_output_length(output_length: int) -> list[frozenbitarray]:
    return list(OUTPUT_BITMAP_CODES_BY_OUTPUT_LENGTH[output_length].keys())

def get_output_bitmap_code_by_output_length(output_length: int, output_bitmap: frozenbitarray) -> frozenbitarray:
    return OUTPUT_BITMAP_CODES_BY_OUTPUT_LENGTH[output_length][output_bitmap]

class InputGroupAxis(int, Enum):
    GROUP_ID   : int = 0
    INPUT_COST : int = 1

    def __str__(self):
        return f'{self.name}({self.value})'
    
    def __repr__(self):
        return f'{self.name}({self.value})'

#@functools.lru_cache()
def init_large_number_range(item_length: int) -> SortedSet[int] | Set[int]:
    if (item_length not in [1, 2, 3]):
        raise Exception(f"item_length={item_length} not supported")
    return SortedSet(range(0, 2**(item_length*8)))

@dataclass()
class UniqueByteList:
    item_length : int            = field()
    seed        : int            = field(default=0)
    items_count : int            = field(default=None, init=False)
    items       : SortedSet[int] = field(default_factory=SortedSet, init=False, repr=False)
    #generator   : CMWC           = field(default=None, init=False, repr=False)

    def __init__(self, item_length: int, seed: int=0):
        self.seed        = seed
        self.item_length = item_length
        #self.generator   = CMWC(x=self.seed)
        self.items       = SortedSet()
        self.items       = init_large_number_range(item_length=self.item_length).copy()
        self.items_count = 2**(self.item_length*8)

    def set_seed(self, seed: int):
        self.seed = seed
        #self.generator.seed(seed=seed)
    
    def reset(self, item_length: int=None):
        self.items.clear()
        if (item_length is not None):
            self.item_length = item_length
        self.items       = init_large_number_range(item_length=self.item_length).copy()
        self.items_count = 2**(self.item_length*8)

    def pick_number(self, number_id: int) -> int | None:
        if (self.items_count == 0):
            return None
        item_id = number_id % self.items_count
        value   = self.items.pop(item_id)
        self.items.discard(value)
        self.items_count -= 1
        return value
    
    def pick_numbers(self, number_ids: Sequence[int]) -> List[int]:
        values = list()
        for number_id in number_ids:
            value = self.pick_number(number_id=number_id)
            if (value is None):
                break
            values.append(value)
        return values
    
    def pick_value(self, value: int) -> int | None:
        """Returns id of the item that has been removed"""
        if (self.items_count == 0):
            return None
        item_id    = 0
        removed_id = None
        for item_value in self.items:
            if (value == item_value):
                removed_id = item_id
                break
            else:
                item_id += 1
        if (removed_id is None):
            raise Exception(f"value={value} not found in items={self.items}")
        self.items.discard(value)
        self.items_count -= 1
        return item_id

#@functools.lru_cache()
def init_unique_byte_list_generator(item_length: int, seed: int=0) -> UniqueByteList:
    return UniqueByteList(item_length=item_length, seed=seed)

@dataclass()
class HashOutputMatch:
    seed            : int                  = field()
    score           : int                  = field()
    output_bitmap   : frozenbitarray       = field()
    input_bytes     : bytes                = field()
    output_bytes    : bytes                = field()
    output_length   : int                  = field()
    hash_outputs    : List[bitarray]       = field()
    input_target_id : Tuple[int, int, int] = field()

    def __str__(self):
        return self.__repr__()

@dataclass()
class HashInputGroup:
    input_length        : int                                             = field()
    target_input_length : int                                             = field()
    input_bitmap        : bitarray                                        = field()
    input_targets       : Dict[tuple[int, int, int], tuple[bytes, bytes]] = field()
    input_cost          : int                                             = field()
    #input_group_id      : int                                             = field()
    output_lengths      : list[int]                                       = field()
    used_output_lengths : list[int]                                       = field()
    output_match        : HashOutputMatch | None                          = field(default=None, init=False)
    output_bytes        : Dict[int, List[bytes]]                          = field(default_factory=lambda : defaultdict(list), repr=False)

    def __str__(self):
        return self.__repr__()

    # https://stackoverflow.com/questions/72161257/exclude-default-fields-from-python-dataclass-repr
    def __repr__(self):
        results        = list()
        visible_fields = (
            (f.name, attrgetter(f.name)(self))
            for f in dataclasses.fields(self)
            if f.repr is True
        )
        for name, value in visible_fields:
            if (name == 'input_targets'):
                input_targets = deepcopy(value)
                input_items   = list()
                for target_id, target_bytes in input_targets.items():
                    #input_items.append(f"(id={target_id}, out='{target_bytes}')")
                    input_items.append(f"(id={target_id}, out='{target_bytes[0].hex()}', in='{target_bytes[1].hex()}')")
                value = ', '.join(input_items)
                value = '{' + value + '}'
            results.append(f"{name}={value}")
        return f"{self.__class__.__name__}(\n            " + f",\n            ".join(results) + f"\n        )"

    def get_hash_inputs(self, target_score: int | None=None) -> Dict[int, Sequence[bytes]]:
        group_inputs = dict()
        for target_id, target_bytes in self.input_targets.items():
            if (target_score is not None) and (target_score != target_id[2]):
                continue
            input_bytes             = target_bytes[1]
            target_inputs           = create_hash_inputs(input_length=self.input_length, input_bytes=input_bytes, bitmap=self.input_bitmap)
            group_inputs[target_id] = target_inputs
        return group_inputs
    
#def get_output_match(self, output_target: bytes, hash_input: bytes, output_length: int, seed: int) -> HashOutputMatch | None:
#    hash_outputs = self.get_hash_outputs_by_output_length(hash_input=hash_input, output_length=output_length, seed=seed)
#    for output_bitmap, output_bytes in hash_outputs.items():
#        self.output_bytes[len(output_bytes)].append(output_bytes)
#        if (output_bytes == output_target):
#            output_score = (len(output_bytes) - self.input_cost) - 1
#            self.output_match = HashOutputMatch(
#                seed            = seed,
#                score           = output_score,
#                output_bitmap   = output_bitmap,
#                input_bytes     = hash_input,
#                output_bytes    = output_bytes,
#                output_length   = output_length,
#                hash_outputs    = hash_outputs,
#                input_target_id = (output_length, len(hash_input), output_score)
#            )
#            return self.output_match
#    return None

#@functools.lru_cache()
def collect_hash_input_groups(next_input_bytes: bytes, target_score: int | None=None) -> Dict[int, List[HashInputGroup]]:
    if (type(next_input_bytes) != bytes):
        raise Exception(f"next_input_bytes={next_input_bytes} (must have 'bytes' type, {type(next_input_bytes)} given)")
    hash_input_groups = defaultdict(list)
    for input_cost in [0, 1, 2, 3]:
        #input_group_id = input_cost
        cost_bitmaps   = INPUT_BITMAPS_BY_COST[input_cost]
        output_lengths = OUTPUT_LENGTH_OPTIONS_BY_INPUT_COST[input_cost]
        
        for bitmap in cost_bitmaps:
            input_targets       = dict()
            input_length        = len(bitmap)
            target_input_length = bitmap.count(1)
            for target_output_length in output_lengths:
                output_score = target_output_length - input_length + target_input_length - 1
                if (target_score is not None) and (target_score != output_score):
                    continue
                target_id                = (target_output_length, target_input_length, output_score)
                output_start_byte        = 0
                output_end_byte          = target_output_length
                target_output_bytes      = next_input_bytes[output_start_byte:output_end_byte]
                input_start_byte         = output_end_byte
                input_end_byte           = input_start_byte + target_input_length
                target_input_bytes       = next_input_bytes[input_start_byte:input_end_byte]
                input_targets[target_id] = (target_output_bytes, target_input_bytes)
            if (len(input_targets) == 0):
                continue
            used_output_lengths = [t_id[0] for t_id, _ in input_targets.items()]
            hash_input_groups[input_cost].append(HashInputGroup(
                input_length        = input_length,
                target_input_length = target_input_length,
                input_bitmap        = bitmap,
                input_targets       = input_targets,
                input_cost          = input_cost,
                #input_group_id      = input_group_id,
                output_lengths      = output_lengths,
                used_output_lengths = used_output_lengths,
                output_bytes        = defaultdict(list),
            ))
    return dict(hash_input_groups.items())

def create_seed_inputs(seed_length: int):
    min_seed = SEED_RANGES_BY_SEED_INPUT_LENGTH[seed_length][0]
    max_seed = SEED_RANGES_BY_SEED_INPUT_LENGTH[seed_length][1]
    seeds    = range(min_seed, max_seed)
    return seeds

@dataclass()
class SeedInputGroup:
    input_length        : int                                             = field()
    target_input_length : int                                             = field()
    input_targets       : Dict[tuple[int, int, int], tuple[bytes, bytes]] = field()
    mapping_cost        : int                                             = field()
    #input_group_id      : int                                             = field()
    output_lengths      : list[int]                                       = field()
    output_match        : HashOutputMatch | None                          = field(default=None, init=False)
    output_bytes        : Dict[int, List[bytes]]                          = field(default_factory=lambda : defaultdict(list), repr=False)

    def __str__(self):
        return self.__repr__()
    
    def get_seed_inputs(self, target_score: int | None=None) -> Dict[int, Sequence[bytes]]:
        group_inputs = dict()
        for target_id, target_bytes in self.input_targets.items():
            if (target_score is not None) and (target_score != target_id[2]):
                continue
            target_inputs           = create_seed_inputs(seed_length=self.target_input_length)
            group_inputs[target_id] = target_inputs
        return group_inputs
    
def collect_seed_input_groups(next_input_bytes: bytes, target_score: int | None=None) -> Dict[int, List[SeedInputGroup]]:
    if (type(next_input_bytes) != bytes):
        raise Exception(f"next_input_bytes={next_input_bytes} (must have 'bytes' type, {type(next_input_bytes)} given)")
    seed_input_groups = defaultdict(list)
    for mapping_cost in [1, 2, 3]:
        for seed_length in SEED_INPUT_LENGTH_OPTIONS_BY_MAPPING_COST[mapping_cost]:
            #input_group_id = mapping_cost
            output_lengths      = [3] #OUTPUT_LENGTH_OPTIONS_BY_INPUT_COST[seed_length]
            input_targets       = dict()
            input_length        = seed_length + mapping_cost
            target_input_length = seed_length
            for target_output_length in output_lengths:
                output_score = target_output_length - input_length
                if (target_score is not None) and (target_score != output_score):
                    continue
                target_id                = (target_output_length, target_input_length, output_score)
                output_start_byte        = 0
                output_end_byte          = target_output_length
                target_output_bytes      = next_input_bytes[output_start_byte:output_end_byte]
                input_start_byte         = output_end_byte
                input_end_byte           = input_start_byte + target_input_length
                target_input_bytes       = next_input_bytes[input_start_byte:input_end_byte]
                input_targets[target_id] = (target_output_bytes, target_input_bytes)
            if (len(input_targets) == 0):
                continue
            seed_input_groups[mapping_cost].append(SeedInputGroup(
                input_length        = input_length,
                target_input_length = target_input_length,
                input_targets       = input_targets,
                mapping_cost        = mapping_cost,
                #input_group_id      = input_group_id,
                output_lengths      = output_lengths,
                output_bytes        = defaultdict(list),
            ))
    return dict(seed_input_groups.items())