# https://github.com/Textualize/rich
import rich
from rich import print
# https://rich.readthedocs.io/en/latest/pretty.html
from rich.pretty import pprint
from rich.progress import track, Progress, \
    SpinnerColumn, BarColumn, TextColumn, TimeElapsedColumn, MofNCompleteColumn, TaskProgressColumn, TimeRemainingColumn, TransferSpeedColumn

from custom_counter import CustomCounter as Counter

# https://rosettacode.org/wiki/Pseudo-random_numbers/Xorshift_star#Python

mask64 = (1 << 64) - 1
mask32 = (1 << 32) - 1
const  = 0x2545F4914F6CDD1D

class Xorshift_star():
    
    def __init__(self, seed=0):
        self.state = seed & mask64

    def seed(self, num):
        self.state = num & mask64
    
    def next_int(self):
        "return random int between 0 and 2**32"
        x = self.state
        x = (x ^ (x >> 12)) & mask64
        x = (x ^ (x << 25)) & mask64
        x = (x ^ (x >> 27)) & mask64
        self.state = x
        answer = (((x * const) & mask64) >> 32) & mask32 
        return answer
    
    def next_float(self):
        "return random float between 0 and 1"
        return self.next_int() / (1 << 32)
    

if __name__ == '__main__':
    bits_per_position = 32
    random_gen = Xorshift_star()
    random_gen.seed(1234567)
    for i in range(5):
        print(random_gen.next_int())
        
    random_gen.seed(987654321)
    hist = {i:0 for i in range(5)}
    for i in range(100_000):
        hist[int(random_gen.next_float() *5)] += 1
    print(f"hist: {hist}")

    random_gen.seed(987654321)
    item_counts = Counter()
    progress = Progress(
        SpinnerColumn(),
        TextColumn("[progress.description]{task.description}"),
        BarColumn(),
        TimeRemainingColumn(),
        TaskProgressColumn(),
        TimeElapsedColumn(),
        MofNCompleteColumn(),
        TransferSpeedColumn(),
        refresh_per_second=1,
        speed_estimate_period=30,
    )

    with progress:
        positions    = range(0, 2**27)
        step_task_id = progress.add_task("", total=(len(positions)*bits_per_position), name="Scanning...")
        for i in positions: #progress.track(sequence=, update_period=1):
            value        = random_gen.next_int()
            value_length = value.bit_length()
            item_counts.update({ value_length: 1 })
            progress.update(step_task_id, advance=bits_per_position)
            if (progress.finished):
                break
    
    print(f"last_value={value}, l={value_length}")
    pprint(item_counts)