# https://stackoverflow.com/questions/38340808/recursive-typing-in-python-3-5
from __future__ import annotations
from rich import print
from rich.pretty import pprint
from rich.text import Text
from rich.progress import TaskID, Progress, SpinnerColumn, BarColumn, TextColumn, MofNCompleteColumn,\
    TaskProgressColumn, TimeElapsedColumn, TimeRemainingColumn, track
from custom_counter import CustomCounter as Counter
from bitarray.util import ba2int, int2ba, huffman_code, canonical_huffman, canonical_decode
from bitarray import bitarray, frozenbitarray
from collections import defaultdict, namedtuple
from dataclasses import dataclass, field
from copy import deepcopy, copy
from typing import List, Set, Dict, Tuple, Optional, Union, Iterable, Sequence
from sortedcontainers import SortedSet, SortedDict, SortedList, SortedKeyList,\
    SortedKeysView, SortedValuesView, SortedItemsView
from enum import Enum, IntEnum
from functools import lru_cache
# accumulate            : accumulate([1,2,3,4,5], operator.mul) --> 1 2 6 24 120 | accumulate([1,2,3,4,5], initial=100) --> 100 101 103 106 110 115
#                         https://docs.python.org/3/library/itertools.html#itertools.accumulate
# compress              : compress('ABCDEF', [1,0,1,0,1,1]) --> A C E F
#                         https://docs.python.org/3/library/itertools.html#itertools.compress
# count                 : count(10) --> 10 11 12 13 14 ... | count(2.5, 0.5) --> 2.5 3.0 3.5 ...
#                         https://docs.python.org/3/library/itertools.html#itertools.count
# islice                : islice(iterable, start, stop[, step])
#                       : https://docs.python.org/3/library/itertools.html#itertools.islice
# filter                : filter(pred, iterable)
# filterfalse           : filterfalse(lambda x: x%2, range(10)) --> 0 2 4 6 8
# takewhile             : takewhile(lambda x: x<5, [1,4,6,4,1]) --> 1 4
#                         https://docs.python.org/3/library/itertools.html#itertools.takewhile
# spy                   : https://more-itertools.readthedocs.io/en/stable/api.html#more_itertools.spy
# bucket                : https://more-itertools.readthedocs.io/en/stable/api.html#more_itertools.bucket
# iterate               : Return start, func(start), func(func(start))
#                         https://more-itertools.readthedocs.io/en/stable/api.html#more_itertools.iterate
# ilen                  : Return the number of items in iterable.
#                         https://more-itertools.readthedocs.io/en/stable/api.html#more_itertools.ilen
# iter_index            : list(iter_index('AABCADEAF', 'A')) --> [0, 1, 4, 7]
#                         https://more-itertools.readthedocs.io/en/stable/api.html#more_itertools.iter_index
# SequenceView          : Sequence views are useful as an alternative to copying, as they don’t require (much) extra storage
#                         https://more-itertools.readthedocs.io/en/stable/api.html#more_itertools.SequenceView
# consumer              : Decorator that automatically advances a PEP-342-style “reverse iterator” to its first yield point so you don’t have to call next() on it manually.
#                         https://more-itertools.readthedocs.io/en/stable/api.html#more_itertools.consumer
# map_if                : list(map_if(iterable, lambda x: x > 3, lambda x: 'too_big')) --> [-5, -4, -3, -2, -1, 0, 1, 2, 3, 'too_big']
#                         https://more-itertools.readthedocs.io/en/stable/api.html#more_itertools.map_if
# unique_everseen       : unique_everseen('ABBcCAD', str.lower) --> A B c D
#                         Remember that list objects are unhashable - you can use the key parameter to transform the list to a tuple (which is hashable) to avoid a slowdown.
#                         https://more-itertools.readthedocs.io/en/stable/api.html#more_itertools.unique_everseen
# locate                : list(locate(['a', 'b', 'c', 'b'], lambda x: x == 'b')) --> [1, 3]
#                         Use with seekable() to find indexes and then retrieve the associated items:
#                         https://more-itertools.readthedocs.io/en/stable/api.html#more_itertools.locate
# first_true            : first_true(range(10), default='missing', pred=lambda x: x > 9) --> 'missing'
#                         first_true(range(10), pred=lambda x: x > 5) --> 6
# longest_common_prefix : ''.join(longest_common_prefix(['abcd', 'abc', 'abf'])) --> 'ab'
#                         https://more-itertools.readthedocs.io/en/stable/api.html#more_itertools.longest_common_prefix
from more_itertools import SequenceView, spy, seekable, peekable, countable,\
    iterate, islice_extended, ilen, iter_index, locate, first_true,\
    iter_except, filter_except, map_except,\
    transpose, partition, bucket,\
    longest_common_prefix
from itertools import accumulate, count, compress, islice, filterfalse, takewhile
from bitarrayset.binaryarrayset import BinaryArraySet
from hash_range_iterator import int_bits_from_nounce, last_int_bits_from_nounce,\
    last_ba_bits_from_nounce, last_ba_bits_from_digest, last_fba_bits_from_digest, last_int_bits_from_digest, int_from_nounce

NUMBER_TYPE_LENGTH        = 1
DEFAULT_ITEM_SCORE        = 1
DEFAULT_PREFIX_LENGTH     = 4
DEFAULT_ENDIAN            = 'little'
MAX_TOP_ITEMS             = 2**16
MAX_INIT_ITEM_LENGTH      = 14
MAX_DATA_ITEM_LENGTH      = 32
# max 65534 options for remaining_number_id in seed tree
SEED_VARINT_PREFIX_BITS   = 3
# max 254 options for remaining_number_id in each nounce tree
NOUNCE_VARINT_PREFIX_BITS = 3

class VarintType(int, Enum):
    CONSUMED  : int = 0
    REMAINING : int = 1

    def __str__(self):
        return f'{self.name}'
    
    def __repr__(self):
        return f'{self.name}'

class HashIdType(int, Enum):
    OLD_NOUNCE_OLD_SEED : int = 0
    #HUFFMAN_CODE        : int = 1
    NEW_NOUNCE_OLD_SEED : int = 2
    NEW_NOUNCE_NEW_SEED : int = 3

    def __str__(self):
        return f'{self.name}'
    
    def __repr__(self):
        return f'{self.name}'

class HashIdRangeType(int, Enum):
    OLD_SEED : int = 0
    NEW_SEED : int = 1

    def __str__(self):
        return f'{self.name}'
    
    def __repr__(self):
        return f'{self.name}'

HashRangeId = namedtuple("HashRangeId", ["range_type", "seed_ids", "nounce_ids"])

def ba2tuple(data: bitarray, signed: bool=False) -> Tuple[int, int]:
    return (len(data), ba2int(data, signed=signed))

def tuple2ba(data: Tuple[int, int], endian: str=DEFAULT_ENDIAN, signed: bool=False) -> bitarray:
    bit_length = data[0]
    value      = data[1]
    return int2ba(value, length=bit_length, endian=endian, signed=signed)

# составной id хеш-значения - состоит из 2 чисел: seed (номер хеш-пространства) и nounce (позиция в хеш-пространстве)
HashId = namedtuple("HashId", ['seed', 'nounce'])

def init_batch_size_from_prefix_size(prefix_size: int) -> int:
    return (2**(2**prefix_size)) - 1   

#@lru_cache(maxsize=4)
def encode_hash_id_type(hash_id_type: HashIdType) -> frozenbitarray:
    # existing items must have shortest codes
    if (hash_id_type == HashIdType.NEW_NOUNCE_NEW_SEED):
        return frozenbitarray('0', endian=DEFAULT_ENDIAN).copy()
    #if (hash_id_type == HashIdType.HUFFMAN_CODE):
    #    return frozenbitarray('01', endian=DEFAULT_ENDIAN).copy()
    if (hash_id_type == HashIdType.NEW_NOUNCE_OLD_SEED):
        return frozenbitarray('10', endian=DEFAULT_ENDIAN).copy()
    if (hash_id_type == HashIdType.OLD_NOUNCE_OLD_SEED):
        return frozenbitarray('11', endian=DEFAULT_ENDIAN).copy()
    raise Exception(f"incorrect hash_id_type={hash_id_type}")

#@lru_cache(maxsize=4)
def decode_hash_id_type(encoded_hash_id_type: frozenbitarray) -> HashIdType:
    #first_bits = frozenbitarray(encoded_hash_id_type[0:2])
    if (encoded_hash_id_type[0:1].copy().to01() == encode_hash_id_type(HashIdType.OLD_NOUNCE_OLD_SEED).to01()):
        return HashIdType.OLD_NOUNCE_OLD_SEED
    #if (encoded_hash_id_type[0:2].copy().to01() == encode_hash_id_type(HashIdType.HUFFMAN_CODE).to01()):
    #    return HashIdType.HUFFMAN_CODE
    if (encoded_hash_id_type[0:2].copy().to01() == encode_hash_id_type(HashIdType.NEW_NOUNCE_OLD_SEED).to01()):
        return HashIdType.NEW_NOUNCE_OLD_SEED
    if (encoded_hash_id_type[0:2].copy().to01() == encode_hash_id_type(HashIdType.NEW_NOUNCE_NEW_SEED).to01()):
        return HashIdType.NEW_NOUNCE_NEW_SEED
    raise Exception(f"incorrect encoded_hash_id_type={encoded_hash_id_type}")

#@lru_cache(maxsize=4)
def get_hash_id_type_length(hash_id_type: HashIdType) -> int:
    return len(encode_hash_id_type(hash_id_type=hash_id_type).copy()) 

#@lru_cache(maxsize=2**16)
def get_number_offset(number: int, bit_length: int=None) -> int:
    if (bit_length is None):
        number_length = number.bit_length()
    else:
        number_length = bit_length
    if (number_length == 0):
        return 1
    offset = 0
    for i in range(0, number_length):
        offset += 2**i
    return offset

#@lru_cache(maxsize=32)
def create_number_ranges(max_number_length: int, offset: int=0) -> List[Tuple[int, int]]:
    ranges    = list()
    max_value = 0
    for number_length in range(0, max_number_length):
        min_value = max_value
        max_value = min_value + 2**number_length
        ranges.append((min_value + offset, max_value + offset))
        #ranges.add((min_value + offset, max_value + offset))
    return ranges.copy()

def get_number_range(number: int) -> Tuple[int, Tuple[int, int]]:
    number_ranges   = create_number_ranges(max_number_length=33, offset=0)
    range_id        = 0
    target_range_id = None
    target_start    = None
    target_end      = None
    for start, end in number_ranges:
        if (number >= start) and (number < end):
            target_range_id = range_id
            target_start    = start
            target_end      = end
            break
        else:
            range_id += 1
    if (target_range_id is None):
        raise Exception(f"number={number} range not found (start={start}, end={end})")
    return (target_range_id, (target_start, target_end))

def get_bit_length_from_range(number: int) -> int:
    number_range = get_number_range(number=number)
    bit_length   = number_range[0]
    return bit_length

def get_prefix_length_from_range(number: int) -> int:
    bit_length = get_bit_length_from_range(number=number)
    return bit_length.bit_length()

@dataclass
class VarintNumberEncoder:
    prefix_length     : int                   = field()
    default_endian    : str                   = field(default=DEFAULT_ENDIAN)
    number_offset     : int                   = field(default=0)
    max_number_length : int                   = field(default=None, init=False)
    number_ranges     : List[Tuple[int, int]] = field(default=None, init=False)

    def __post_init__(self):
        self.max_number_length = 2 ** self.prefix_length
        self.number_ranges     = create_number_ranges(max_number_length=self.max_number_length, offset=self.number_offset)
    
    def get_number_range(self, number: int) -> Tuple[int, Tuple[int, int]]:
        range_id        = 0
        target_range_id = None
        target_start    = None
        target_end      = None
        #pprint(self.number_ranges)
        for start, end in self.number_ranges:
            if (number >= start) and (number < end):
                target_range_id = range_id
                target_start    = start
                target_end      = end
                break
            else:
                range_id += 1
        if (target_range_id is None):
            raise Exception(f"number={number} range not found (start={start}, end={end})")
        return (target_range_id, (target_start, target_end))
    
    def to_relative_number(self, number: int) -> int:
        number_range = self.get_number_range(number=number)
        offset       = number_range[1][0]
        return number - offset

    def to_absolute_number(self, number: int, bit_length: int) -> int:
        number_range = self.number_ranges[bit_length]
        offset       = number_range[0]
        return offset + number
    
    def get_bit_length_from_range(self, number: int) -> int:
        number_range = self.get_number_range(number=number)
        bit_length   = number_range[0]
        return bit_length
    
    def encode_length_prefix(self, value: int, signed: bool=False) -> bitarray:
        if (self.prefix_length == 0):
            return bitarray('', endian=self.default_endian)
        return int2ba(value, length=self.prefix_length, endian=self.default_endian, signed=signed)

    def decode_length_prefix(self, encoded_prefix: bitarray, signed: bool=False) -> int:
        if (len(encoded_prefix) == 0):
            return 0
        if (len(encoded_prefix) < self.prefix_length):
            raise Exception(f"encoded_prefix={encoded_prefix} (l={len(encoded_prefix)}) is too short (target self.prefix_length={self.prefix_length})")
        return ba2int(encoded_prefix[0:self.prefix_length], signed=signed)

    def encode_relative_number(self, number: int, bit_length: int, signed: bool=False) -> bitarray:
        if (bit_length == 0):
            return bitarray('', endian=self.default_endian)
        return int2ba(number, length=bit_length, endian=self.default_endian, signed=signed)

    def decode_relative_number(self, encoded_number: bitarray, bit_length: int, signed: bool=False) -> int:
        if (bit_length == 0):
            return 0
        if (len(encoded_number) < bit_length):
            raise Exception(f"encoded_number={encoded_number} (l={len(encoded_number)}) is too short (target bit_length={bit_length})")
        return ba2int(encoded_number[0:bit_length], signed=signed)
    
    def encode_number(self, number: int) -> bitarray:
        number_range    = self.get_number_range(number=number)
        bit_length      = number_range[0]
        relative_number = self.to_relative_number(number=number)
        encoded_prefix  = self.encode_length_prefix(value=bit_length)
        encoded_number  = self.encode_relative_number(number=relative_number, bit_length=bit_length)

        return encoded_prefix + encoded_number

    def decode_number(self, encoded_number: bitarray) -> int:
        prefix_bits     = encoded_number[0:self.prefix_length]
        number_length   = self.decode_length_prefix(encoded_prefix=prefix_bits)
        number_start    = self.prefix_length
        number_end      = number_start + number_length
        number_bits     = encoded_number[number_start:number_end]
        relative_number = self.decode_relative_number(encoded_number=number_bits, bit_length=number_length)
        decoded_number  = self.to_absolute_number(number=relative_number, bit_length=number_length)

        return decoded_number

    def encode_varint_number(self, number: int) -> bitarray:
        return self.encode_number(number=number)

    def decode_varint_number(self, encoded_number: bitarray) -> int:
        return self.decode_number(encoded_number=encoded_number)
    
@dataclass
class DecodedVarint:
    bit_length  : int        = field()
    number_type : VarintType = field()
    number      : int        = field()
    number_id   : int        = field()
    data        : bitarray   = field()

@dataclass
class EncodedVarint:
    bit_length  : int        = field()
    number_type : VarintType = field()
    number      : int        = field()
    number_id   : int        = field()
    data        : bitarray   = field()

@dataclass
class ConsumableVarintSet:
    remaining_id_prefix_length : int                           = field()
    consumed_id_prefix_length  : int                           = field(init=False)
    _consumed_id_prefix_length : int                           = field(default=None, repr=False)
    max_value_id_length        : int                           = field(default=None, init=False)
    _max_value_id_length       : int                           = field(init=False, repr=False)
    min_value_id               : int                           = field(default=0, init=False)
    max_value_id               : int                           = field(default=None, init=False)
    _max_value_id              : int                           = field(default=None, repr=False)
    # список выбранных значений (в порядке добавления)
    consumed_values            : SortedDict[int, int]          = field(default_factory=SortedDict, init=False)
    _consumed_values           : SortedDict[int, int]          = field(default_factory=SortedDict, init=False, repr=False)
    _consumed_values_view      : SortedValuesView[int]         = field(default=None, init=False, repr=False)
    consumed_ids               : SequenceView[int]             = field(init=False)
    _consumed_ids              : Sequence[int]                 = field(default_factory=list, init=False, repr=False)
    remaining_ids              : SequenceView[int]             = field(init=False)
    _remaining_ids             : Sequence[int]                 = field(default=None, init=False, repr=False)
    # список оставшихся значений (которые еще не были использованы)
    remaining_values           : SortedSet                     = field(default_factory=SortedSet)
    _remaining_values          : SortedSet                     = field(default_factory=SortedSet, init=False, repr=False)
    min_remaining_value        : int                           = field(default=0, repr=False)
    default_endian             : str                           = field(default=DEFAULT_ENDIAN, repr=False)
    value_id_ranges            : SequenceView[Tuple[int, int]] = field(init=False)
    _value_id_ranges           : List[Tuple[int, int]]         = field(default=None, init=False, repr=False)
    consumed_varint_encoder    : VarintNumberEncoder           = field(init=False)
    _consumed_varint_encoder   : VarintNumberEncoder           = field(init=False, repr=False)
    remaining_varint_encoder   : VarintNumberEncoder           = field(init=False)

    def __post_init__(self):
        self._consumed_varint_encoder = VarintNumberEncoder(prefix_length=self.consumed_id_prefix_length, default_endian=self.default_endian)
        self.remaining_varint_encoder = VarintNumberEncoder(prefix_length=self.remaining_id_prefix_length, default_endian=self.default_endian)
        for remaining_id in self.remaining_ids:
            remaining_value = self.min_remaining_value + remaining_id
            self.remaining_values.add(remaining_value)
    
    @property
    def consumed_varint_encoder(self) -> VarintNumberEncoder:
        if (self._consumed_varint_encoder is None):
            self._consumed_varint_encoder = VarintNumberEncoder(prefix_length=self.consumed_id_prefix_length, default_endian=self.default_endian)
        #print(type(self._consumed_varint_encoder).__name__)
        if (type(self._consumed_varint_encoder).__name__ == 'VarintNumberEncoder') and (self._consumed_varint_encoder.prefix_length != self.consumed_id_prefix_length):
            #print(f"Updating self.consumed_varint_encoder:")
            #print(f"self._consumed_varint_encoder.prefix_length={self._consumed_varint_encoder.prefix_length}")
            #print(f"self.consumed_id_prefix_length={self.consumed_id_prefix_length}")
            self._consumed_varint_encoder = VarintNumberEncoder(prefix_length=self.consumed_id_prefix_length, default_endian=self.default_endian)
        return self._consumed_varint_encoder
    
    @consumed_varint_encoder.setter
    def consumed_varint_encoder(self, consumed_varint_encoder: VarintNumberEncoder):
        self._consumed_varint_encoder = consumed_varint_encoder
    
    @property
    def remaining_values(self) -> SortedSet:
        #print("getting remaining_values")
        return self._remaining_values

    @remaining_values.setter
    def remaining_values(self, remaining_values: SortedSet):
        #print("setting remaining_values to", remaining_values)
        self._remaining_values = remaining_values
    
    @property
    def consumed_values(self) -> Union[SortedValuesView, SortedDict]:
        if (self._consumed_values is None):
            self._consumed_values = SortedDict()
        #if (self._consumed_values_view is None):
        #    self._consumed_values_view = SortedValuesView(self._consumed_values)
        return SequenceView(list(self._consumed_values.values()))

    @consumed_values.setter
    def consumed_values(self, consumed_values: SortedDict[int, int]=None):
        if (consumed_values is None):
            consumed_values = SortedDict()
        self._consumed_values = consumed_values
        #self._consumed_values_view = self._consumed_values.values()
    
    @property
    def remaining_ids(self) -> SequenceView[int]:
        if (self._remaining_ids is None):
            self._remaining_ids = range(self.min_value_id, self.max_value_id + 1)
        return SequenceView(list(self._remaining_ids))
    
    @remaining_ids.setter
    def remaining_ids(self, remaining_ids: SortedDict[int]=SortedDict()):
        #print(f"self.min_value_id={self.min_value_id}, self._max_value_id={self._max_value_id}, self.max_value_id={self.max_value_id}")
        #self._remaining_ids = SequenceView(range(self.min_value_id, self.max_value_id + 1))
        if (self._remaining_ids is None):
            self._remaining_ids = range(self.min_value_id, self.max_value_id + 1)
    
    @property
    def consumed_ids(self) ->  SequenceView[int]:
        if (self.consumed_values is not None):
            self._consumed_ids = range(0, len(self.consumed_values))
        return SequenceView(list(self._consumed_ids))
    
    @consumed_ids.setter
    def consumed_ids(self, consumed_ids: SortedDict[int]=SortedDict()) -> SequenceView[int]:
        if (self.consumed_values is not None):
            self._consumed_ids = range(0, len(self.consumed_values))
    
    @property
    def value_id_ranges(self) -> List[Tuple[int, int]]:
        if (self._value_id_ranges is None):
            self._value_id_ranges = SequenceView(create_number_ranges(max_number_length=2**self.remaining_id_prefix_length, offset=self.min_value_id))
        #pprint(self._value_id_ranges)
        return self._value_id_ranges

    @value_id_ranges.setter
    def value_id_ranges(self, value_id_ranges: List[Tuple[int, int]]=None):
        if (value_id_ranges is None) and (self._value_id_ranges is None):
            self._value_id_ranges = SequenceView(create_number_ranges(max_number_length=2**self.remaining_id_prefix_length, offset=self.min_value_id))
    
    @property
    def max_value_id(self) -> int:
        #print("getting max_value_id")
        if (self._max_value_id is None) or (self._max_value_id == 0):
            if (self.value_id_ranges is not None):
                #pprint(self.value_id_ranges)
                self._max_value_id = self.value_id_ranges[len(self.value_id_ranges)-1][1] - 1
        return self._max_value_id
    
    @max_value_id.setter
    def max_value_id(self, max_value_id: int=0):
        self._max_value_id = max_value_id
    
    @property
    def max_value_id_length(self) -> int:
        #print("getting max_value_id_length")
        if (self.remaining_id_prefix_length is not None) and (self._max_value_id_length is None):
            self._max_value_id_length = 2 ** self.remaining_id_prefix_length - 1
        return self._max_value_id_length
    
    @max_value_id_length.setter
    def max_value_id_length(self, max_value_id_length: int=None):
        self._max_value_id_length = max_value_id_length
        if (self.remaining_id_prefix_length is not None) and (self._max_value_id_length is None):
            self._max_value_id_length = 2 ** self.remaining_id_prefix_length - 1

    @property
    def consumed_id_prefix_length(self) -> int:
        #print("getting consumed_id_prefix_length")
        if (self._consumed_id_prefix_length is None):
            self._consumed_id_prefix_length = 0
        if (self._consumed_values is not None) and (self.consumed_values is not None):
            max_consumed_value_id = len(self.consumed_values)
            if (max_consumed_value_id > 0):
                max_consumed_value_id = max_consumed_value_id - 1
            #self._consumed_id_prefix_length = get_bit_length_from_range(number=total_consumed_values)
            self._consumed_id_prefix_length = get_prefix_length_from_range(number=max_consumed_value_id)
        return self._consumed_id_prefix_length
    
    @consumed_id_prefix_length.setter
    def consumed_id_prefix_length(self, consumed_id_prefix_length: int=None):
        if (consumed_id_prefix_length is not None):
            self._consumed_id_prefix_length = consumed_id_prefix_length
    
    ### DATA API ###

    def get_consumed_varint_id(self, consumed_varint: int) -> int:
        return self.consumed_values.index(value=consumed_varint)
    
    def get_consumed_varint(self, consumed_varint_id: int) -> int:
        return self.consumed_values[consumed_varint_id]
    
    def has_consumed_varint_id(self, consumed_varint_id: int) -> bool:
        return (consumed_varint_id in self.consumed_values)
    
    def has_consumed_varint(self, consumed_varint: int) -> bool:
        return (consumed_varint in self._consumed_values.values())

    def get_remaining_varint_id(self, remaining_varint: int) -> int:
        return self.remaining_values.index(value=remaining_varint)
    
    def get_remaining_varint(self, remaining_varint_id: int) -> int:
        return self.remaining_values.__getitem__(index=remaining_varint_id)
    
    def has_remaining_varint_id(self, remaining_varint_id: int) -> bool:
        return (remaining_varint_id <= len(self.remaining_values))
    
    def has_remaining_varint(self, remaining_varint: int) -> bool:
        #remaining_varint_id = self.get_remaining_varint_id(remaining_varint=remaining_varint)
        #return self.has_remaining_varint_id(remaining_varint_id=remaining_varint_id)
        return (remaining_varint in self._remaining_values)

    def consume_value_id(self, remaining_value_id: int) -> int:
        remaining_value = self.remaining_values[remaining_value_id]
        return self.consume_value(remaining_value=remaining_value)

    def consume_value(self, remaining_value: int) -> int:
        if (remaining_value not in self.remaining_values):
            raise Exception(f"remaining_value={remaining_value} not found")
        # define id (position) for new consumed value
        consumed_value_id    = len(self._consumed_values)
        max_remaining_value  = max(self._remaining_values)
        next_remaining_value = max_remaining_value + 1
        # add new consumed value (preserving insertion order)
        self._consumed_values[consumed_value_id] = remaining_value
        # remove consumed value from remaining items list
        self._remaining_values.remove(value=remaining_value)
        # refill remaining values
        self._remaining_values.add(next_remaining_value)
        return consumed_value_id
    
    def get_consumed_varint_prefix_length(self, consumed_varint: int) -> int:
        if (self.has_consumed_varint(consumed_varint=consumed_varint) is False):
            raise Exception(f"No consumed_varint={consumed_varint} in self.consumed_values={self.consumed_values}")
        return self.consumed_id_prefix_length
    
    def get_consumed_varint_length(self, consumed_varint: int) -> int:
        if (self.has_consumed_varint(consumed_varint=consumed_varint) is False):
            raise Exception(f"No consumed_varint={consumed_varint} in self.consumed_values={self.consumed_values}")
        consumed_varint_id = self.get_consumed_varint_id(consumed_varint=consumed_varint)
        encoded_varint_id  = self.consumed_varint_encoder.encode_number(number=consumed_varint_id)
        return len(encoded_varint_id)
    
    def get_next_consumed_varint_prefix_length(self, remaining_varint: int) -> int:
        if (self.has_consumed_varint(consumed_varint=remaining_varint) is True):
            raise Exception(f"remaining_varint={remaining_varint} is already in self.consumed_values={self.consumed_values}")
        if (self.has_remaining_varint(remaining_varint=remaining_varint) is False):
            raise Exception(f"No remaining_varint={remaining_varint} in self.remaining_varint={self.remaining_varint}")
        next_consumed_varint_id = len(self.consumed_values) + 1
        if (next_consumed_varint_id > 0):
            next_consumed_varint_id = next_consumed_varint_id - 1
        return get_prefix_length_from_range(number=next_consumed_varint_id)
    
    def get_next_consumed_varint_length(self, remaining_varint: int) -> int:
        if (self.has_consumed_varint(consumed_varint=remaining_varint) is True):
            raise Exception(f"remaining_varint={remaining_varint} is already in self.consumed_values={self.consumed_values}")
        if (self.has_remaining_varint(remaining_varint=remaining_varint) is False):
            raise Exception(f"No remaining_varint={remaining_varint} in self.remaining_varint={self.remaining_varint}")
        next_consumed_varint_id = len(self.consumed_values) + 1
        if (next_consumed_varint_id > 0):
            next_consumed_varint_id = next_consumed_varint_id - 1
        return get_bit_length_from_range(number=next_consumed_varint_id)
    
    def encode_consumed_varint(self, consumed_varint: int) -> EncodedVarint:
        consumed_varint_id = self.get_consumed_varint_id(consumed_varint=consumed_varint)
        return self.encode_consumed_varint_id(consumed_varint=consumed_varint, consumed_varint_id=consumed_varint_id)

    def encode_consumed_varint_id(self, consumed_varint: int, consumed_varint_id: int) -> EncodedVarint:
        encoded_varint = self.consumed_varint_encoder.encode_number(number=consumed_varint_id)
        bit_length     = len(encoded_varint)
        return EncodedVarint(
            number_type = VarintType.CONSUMED,
            bit_length  = bit_length,
            number      = consumed_varint,
            number_id   = consumed_varint_id,
            data        = encoded_varint,
        )
    
    def encode_remaining_varint(self, remaining_varint: int) -> EncodedVarint:
        remaining_varint_id = self.get_remaining_varint_id(remaining_varint=remaining_varint)
        return self.encode_remaining_varint_id(remaining_varint=remaining_varint, remaining_varint_id=remaining_varint_id)

    def encode_remaining_varint_id(self, remaining_varint: int, remaining_varint_id: int) -> EncodedVarint:
        encoded_varint = self.remaining_varint_encoder.encode_number(number=remaining_varint_id)
        bit_length     = len(encoded_varint)
        return EncodedVarint(
            number_type = VarintType.REMAINING,
            bit_length  = bit_length,
            number      = remaining_varint,
            number_id   = remaining_varint_id,
            data        = encoded_varint,
        )
    
    def decode_consumed_varint(self, encoded_consumed_varint: bitarray) -> DecodedVarint:
        varint_id     = self.consumed_varint_encoder.decode_number(encoded_number=encoded_consumed_varint)
        prefix_bits   = encoded_consumed_varint[0:self.consumed_varint_encoder.prefix_length]
        prefix_length = self.consumed_varint_encoder.prefix_length
        number_length = self.consumed_varint_encoder.decode_length_prefix(encoded_prefix=prefix_bits)
        bit_length    = prefix_length + number_length
        number        = self.get_consumed_varint(consumed_varint_id=varint_id)
        return DecodedVarint(
            number_type = VarintType.CONSUMED,
            bit_length  = bit_length,
            number      = number,
            number_id   = varint_id,
            data        = encoded_consumed_varint,
        )
    
    def decode_remaining_varint(self, encoded_remaining_varint: bitarray) -> DecodedVarint:
        varint_id     = self.remaining_varint_encoder.decode_number(encoded_number=encoded_remaining_varint)
        prefix_bits   = encoded_remaining_varint[0:self.remaining_varint_encoder.prefix_length]
        prefix_length = self.remaining_varint_encoder.prefix_length
        number_length = self.remaining_varint_encoder.decode_length_prefix(encoded_prefix=prefix_bits)
        bit_length    = prefix_length + number_length
        number        = self.get_remaining_varint(remaining_varint_id=varint_id)
        return DecodedVarint(
            number_type = VarintType.REMAINING,
            bit_length  = bit_length,
            number      = number,
            number_id   = varint_id,
            data        = encoded_remaining_varint,
        )
    
### HASH ITEM API ###

# TODO: store init hash id length and saved hash id length
@dataclass
class InitHashItem:
    hash_id             : HashId          = field()
    hash_id_type        : HashIdType      = field()
    digest              : int             = field()
    hash_id_length      : int             = field()
    data_length         : int             = field(default=None)
    encoded_hash_id_type: frozenbitarray  = field(default=None, init=False)
    encoded_seed        : frozenbitarray  = field(default=None)
    encoded_nounce      : frozenbitarray  = field(default=None)
    encoded_hash_item   : frozenbitarray  = field(default=None, init=False)
    data                : frozenbitarray  = field(default=None, init=False)
    data_id             : Tuple[int, int] = field(default=None, init=False)
    score               : int             = field(default=DEFAULT_ITEM_SCORE)
    seed                : int             = field(default=None, init=False)
    nounce              : int             = field(default=None, init=False)
    offset              : int             = field(default=None)
    difficulty          : int             = field(default=None)
    hash_id_type_length : int             = field(init=False)

    def __post_init__(self):
        if (self.data_length is None):
            self.data_length = self.hash_id_length + DEFAULT_ITEM_SCORE
        # making search faster but allowing negative scores for the elements longer than 16 bit
        if (self.hash_id_type != HashIdType.OLD_NOUNCE_OLD_SEED) and (self.data_length > MAX_INIT_ITEM_LENGTH):
            self.data_length = MAX_INIT_ITEM_LENGTH
        
        self.score                = self.data_length - self.hash_id_length
        self.seed                 = self.hash_id.seed
        self.nounce               = self.hash_id.nounce
        
        self.data                 = last_fba_bits_from_digest(digest=self.digest, bit_length=self.data_length, endian=DEFAULT_ENDIAN)
        self.data_id              = ba2tuple(data=self.data)
        self.hash_id_type_length  = get_hash_id_type_length(hash_id_type=self.hash_id_type)
        
        self.encoded_hash_id_type = encode_hash_id_type(self.hash_id_type)
        self.encoded_hash_item    = bitarray('', endian=DEFAULT_ENDIAN)
        self.encoded_hash_item   += self.encoded_hash_id_type
        self.encoded_hash_item   += self.encoded_seed 
        self.encoded_hash_item   += self.encoded_nounce

@dataclass
class SavedHashItem:
    init_item               : InitHashItem        = field()
    seeds                   : ConsumableVarintSet = field(repr=False)
    seed_nounces            : ConsumableVarintSet = field(repr=False)
    seed_id_length          : int                 = field()
    nounce_id_length        : int                 = field()
    seed_id_prefix_length   : int                 = field()
    nounce_id_prefix_length : int                 = field()
    hash_id_type_length     : int                 = field(init=False)
    hash_id_length          : int                 = field(init=False)
    data_length             : int                 = field(init=False)
    hash_id_type            : HashIdType          = field(default=HashIdType.OLD_NOUNCE_OLD_SEED, init=False)
    hash_id                 : HashId              = field(init=False)
    digest                  : int                 = field(init=False)
    data                    : frozenbitarray      = field(init=False)
    data_id                 : Tuple[int, int]     = field(init=False)
    score                   : int                 = field(default=DEFAULT_ITEM_SCORE, init=False)
    total_score             : int                 = field(default=0, init=False)
    seed                    : int                 = field(default=None, init=False)
    nounce                  : int                 = field(default=None, init=False)
    default_endian          : str                 = field(default=DEFAULT_ENDIAN, repr=False)
    encoded_hash_id_type    : frozenbitarray      = field(default=None, init=False)
    encoded_seed            : frozenbitarray      = field(default=None, init=False)
    encoded_nounce          : frozenbitarray      = field(default=None, init=False)
    encoded_hash_item       : frozenbitarray      = field(default=None, init=False)
    huffman_code            : frozenbitarray      = field(default=None, init=False)
    huffman_data_code       : frozenbitarray      = field(default=None, init=False)
    debug                   : bool                = field(default=True, repr=False)

    def __post_init__(self):
        self.hash_id             = self.init_item.hash_id
        self.digest              = self.init_item.digest
        self.seed                = self.hash_id.seed
        self.nounce              = self.hash_id.nounce

        self.update_encoded_hash_id()

        self.hash_id_type_length = get_hash_id_type_length(hash_id_type=self.hash_id_type)
        self.hash_id_length      = self.hash_id_type_length \
            + self.seed_id_prefix_length + self.seed_id_length \
            + self.nounce_id_prefix_length + self.nounce_id_length 
        self.data_length         = self.hash_id_length + self.score
        self.data                = last_fba_bits_from_digest(digest=self.digest, bit_length=self.data_length, endian=self.default_endian)
        self.data_id             = ba2tuple(data=self.data)
        self.total_score        += self.init_item.score
    
    def get_new_hash_id_length(self, seed_id_prefix_length: int, nounce_id_prefix_length: int) -> int:
        if (self.huffman_code is None):
            return self.hash_id_type_length + seed_id_prefix_length + self.seed_id_length + nounce_id_prefix_length + self.nounce_id_length
        else:
            return self.hash_id_type_length + len(self.huffman_code)
    
    def get_new_data_length(self, seed_id_prefix_length: int, nounce_id_prefix_length: int) -> int:
        if (self.huffman_data_code is None):
            new_hash_id_length = self.get_new_hash_id_length(seed_id_prefix_length=seed_id_prefix_length, nounce_id_prefix_length=nounce_id_prefix_length)
        else:
            new_hash_id_length = len(self.huffman_data_code)
        return new_hash_id_length + self.score
    
    def get_new_data(self, seed_id_prefix_length: int, nounce_id_prefix_length: int) -> frozenbitarray:
        new_data_length = self.get_new_data_length(seed_id_prefix_length=seed_id_prefix_length, nounce_id_prefix_length=nounce_id_prefix_length)
        return self.create_data_bits(bit_length=new_data_length) #last_fba_bits_from_digest(digest=self.digest, bit_length=new_data_length, endian=self.default_endian)
    
    def get_new_data_id(self, seed_id_prefix_length: int, nounce_id_prefix_length: int) -> frozenbitarray:
        new_data = self.get_new_data(seed_id_prefix_length=seed_id_prefix_length, nounce_id_prefix_length=nounce_id_prefix_length)
        return ba2tuple(data=new_data)
    
    def create_data_bits(self, bit_length: int) -> frozenbitarray:
        return last_fba_bits_from_digest(digest=self.digest, bit_length=bit_length, endian=self.default_endian).copy()
    
    def need_updates(self, seed_id_prefix_length: int, nounce_id_prefix_length: int) -> bool:
        #new_hash_id_length = self.get_new_hash_id_length(seed_id_prefix_length=seed_id_prefix_length, nounce_id_prefix_length=nounce_id_prefix_length)
        #if (new_hash_id_length != self.hash_id_length):
        #    return True
        #new_data_length = self.get_new_data_length(seed_id_prefix_length=seed_id_prefix_length, nounce_id_prefix_length=nounce_id_prefix_length)
        #if (new_data_length != self.data_length):
        #    return True
        new_data_id = self.get_new_data_id(seed_id_prefix_length=seed_id_prefix_length, nounce_id_prefix_length=nounce_id_prefix_length)
        if (new_data_id != self.data_id):
            return True
        return False

    def update_encoded_hash_id(self):
        self.encoded_hash_id_type = encode_hash_id_type(self.hash_id_type)
        if (self.huffman_code is None):
            self.encoded_seed         = self.seeds.encode_consumed_varint(consumed_varint=self.seed).data
            self.encoded_nounce       = self.seed_nounces.encode_consumed_varint(consumed_varint=self.nounce).data
            self.encoded_hash_item    = self.encoded_hash_id_type + self.encoded_seed + self.encoded_nounce
        else:
            self.encoded_seed         = self.seeds.encode_consumed_varint(consumed_varint=self.seed).data
            self.encoded_nounce       = self.seed_nounces.encode_consumed_varint(consumed_varint=self.nounce).data
            self.encoded_hash_item    = self.encoded_hash_id_type + self.huffman_code.copy()

    def update_score(self) -> str:
        self.total_score += self.score
        return f"(+{self.score}) total_score={self.total_score} -> {self.total_score + self.score}, hash_id: {self.hash_id}, data={self.data} (l={self.data_length})" #self.total_score
    
    def update_data(self, seed_id_prefix_length: int, nounce_id_prefix_length: int) -> str:
        #if (self.need_updates(seed_id_prefix_length=seed_id_prefix_length, nounce_id_prefix_length=nounce_id_prefix_length) is False):
        new_data_id = self.get_new_data_id(seed_id_prefix_length=seed_id_prefix_length, nounce_id_prefix_length=nounce_id_prefix_length)
        if (self.data_id == new_data_id) and (self.huffman_code is None):
            return None
        old_data_length              = self.data_length
        old_data_id                  = self.data_id
        old_data                     = self.data
        self.seed_id_prefix_length   = seed_id_prefix_length
        self.nounce_id_prefix_length = nounce_id_prefix_length

        self.hash_id_length = self.get_new_hash_id_length(seed_id_prefix_length=seed_id_prefix_length, nounce_id_prefix_length=nounce_id_prefix_length)
        self.data_length    = self.get_new_data_length(seed_id_prefix_length=seed_id_prefix_length, nounce_id_prefix_length=nounce_id_prefix_length)
        self.data           = self.get_new_data(seed_id_prefix_length=seed_id_prefix_length, nounce_id_prefix_length=nounce_id_prefix_length)
        self.data_id        = self.get_new_data_id(seed_id_prefix_length=seed_id_prefix_length, nounce_id_prefix_length=nounce_id_prefix_length)
        
        self.update_encoded_hash_id()
        
        if (self.debug):
            if (self.data_length != old_data_length):
                return f"hash_id: {self.hash_id}: old_data={old_data} id={old_data_id}, ({old_data_length}) -> new_data={self.data} id={self.data_id}, ({self.data_length})" #self.data
        return None
    
def hash_item_seed_key_function(hash_item: SavedHashItem) -> int:
    return hash_item.hash_id.seed

def create_hash_item_seed_validator_function(target_seed: int):
    return lambda seed: seed == target_seed

def hash_item_data_length_key_function(hash_item: SavedHashItem) -> int:
    return hash_item.data_length

def create_hash_item_data_length_validator_function(target_data_length: int):
    return lambda data_length: data_length == target_data_length

def hash_item_data_key_function(hash_item: SavedHashItem) -> frozenbitarray:
    return hash_item.data

def create_hash_item_data_validator_function(target_data: frozenbitarray):
    return lambda data: data == target_data

def hash_item_hash_id_length_key_function(hash_item: SavedHashItem) -> int:
    return hash_item.hash_id_length

def create_hash_item_hash_id_length_validator_function(target_hash_id_length: int):
    return lambda hash_id_length: hash_id_length == target_hash_id_length

# TODO: also predict consumed_id length after saving
@dataclass
class HashIdRange:
    id                   : HashRangeId     = field(default=None, init=False)
    range_type           : HashIdRangeType = field()
    seed_ids             : Sequence[int]   = field()
    nounce_ids           : Sequence[int]   = field()
    seed_prefix_length   : int             = field()
    nounce_prefix_length : int             = field()
    hash_id_length       : int             = field(default=None, init=False)
    data_length          : int             = field(default=None, init=False)
    hash_id_type         : int             = field(default=None, init=False)
    hash_id_type_length  : int             = field(default=None, init=False)
    seed_id_length       : int             = field(default=None, init=False)
    nounce_id_length     : int             = field(default=None, init=False)
    total                : int             = field(default=None, init=False)
    score                : int             = field(default=DEFAULT_ITEM_SCORE, init=False)

    def __post_init__(self):
        if (self.range_type == HashIdRangeType.OLD_SEED):
            self.hash_id_type = HashIdType.NEW_NOUNCE_OLD_SEED
        elif (self.range_type == HashIdRangeType.NEW_SEED):
            self.hash_id_type = HashIdType.NEW_NOUNCE_NEW_SEED
        else:
            raise Exception(f"incorrect range_type={self.range_type}")
        
        if (len(self.seed_ids) == 0):
            raise Exception(f"Empty self.seed_ids={self.seed_ids}")
        if (len(self.nounce_ids) == 0):
            raise Exception(f"Empty self.nounce_ids={self.nounce_ids}")
        self.id = HashRangeId(
            range_type = self.range_type,
            seed_ids   = (min(self.seed_ids), max(self.seed_ids)+1),
            nounce_ids = (min(self.nounce_ids), max(self.nounce_ids)+1),
        )
        
        self.hash_id_type_length = get_hash_id_type_length(hash_id_type=self.hash_id_type)
        self.seed_id_length      = get_bit_length_from_range(number=max(self.seed_ids))
        self.nounce_id_length    = get_bit_length_from_range(number=max(self.nounce_ids))
        self.total               = len(self.seed_ids) * len(self.nounce_ids)

        self.hash_id_length = self.hash_id_type_length \
            + self.seed_prefix_length + self.seed_id_length \
            + self.nounce_prefix_length + self.nounce_id_length
        self.data_length = self.hash_id_length + self.score
        
        if (self.data_length > MAX_INIT_ITEM_LENGTH):
            self.data_length = MAX_INIT_ITEM_LENGTH
            self.score       = self.data_length - self.hash_id_length

ScannedHashId = namedtuple("ScannedHashId", ["digest", "data_id_value", "offset"])

class MyTaskProgressColumn(TaskProgressColumn):

    @classmethod
    def render_speed(cls, speed: Optional[float]) -> Text:
        """Render the speed in iterations per second.

        Args:
            task (Task): A Task object.

        Returns:
            Text: Text object containing the task speed.
        """
        if speed is None:
            return Text("", style="progress.percentage")
        return Text(f"{speed:#n} hashes/s", style="progress.percentage")

@dataclass
class HashItemTree:
    seed_id_prefix_length     : int                               = field(default=SEED_VARINT_PREFIX_BITS)
    nounce_id_prefix_length   : int                               = field(default=NOUNCE_VARINT_PREFIX_BITS)
    seeds                     : ConsumableVarintSet               = field(default=None)
    seed_nounces              : Dict[int, ConsumableVarintSet]    = field(default=None)
    hash_items                : Dict[HashId, SavedHashItem]       = field(default=None)
    scan_ranges               : Dict[HashRangeId, HashIdRange]    = field(default_factory=SortedDict, init=False)
    saved_data_lengths        : SortedSet[int]                    = field(default_factory=SortedSet, init=False)
    #scanned_data_lengths      : SortedSet[int]                    = field(default_factory=SortedSet, init=False)
    ranges_data_lengths       : SortedSet[int]                    = field(default_factory=SortedSet, init=False)
    #saved_hash_id_lengths     : SortedSet[int]                    = field(default_factory=SortedSet, init=False)
    scanned_hash_id_lengths   : SortedSet[int]                    = field(default_factory=SortedSet, init=False)
    ranges_hash_id_lengths    : SortedSet[int]                    = field(default_factory=SortedSet, init=False)
    saved_data_ids            : Dict[int, Set[int]]               = field(default_factory=lambda: defaultdict(set), init=False)
    scanned_data_ids          : Dict[int, Set[int]]               = field(default_factory=lambda: defaultdict(set), init=False)
    hash_id_counts            : Union[Counter, Dict[HashId, int]] = field(default_factory=Counter, init=False)
    hash_id_huffman_codes     : Dict[HashId, bitarray]            = field(default_factory=dict, init=False)
    hash_id_data_codes        : Dict[HashId, bitarray]            = field(default_factory=dict, init=False)
    data_tail                 : bitarray                          = field(default=None)
    data_tail_length          : int                               = field(default=0)
    max_data_item_length      : int                               = field(default=MAX_DATA_ITEM_LENGTH)
    max_init_item_length      : int                               = field(default=MAX_INIT_ITEM_LENGTH)
    debug                     : bool                              = field(default=True, init=False)
    layer_score               : int                               = field(default=0)
    # общее количество хеш-операций которые понадобилось выполнить чтобы найти последний элемент
    # обновляется при добавлении каждого элемента
    item_difficulty           : int                                = field(default=0)
    progress                  : Progress                           = field(default=None, init=False, repr=False)
    main_task_id              : int                                = field(default=None, init=False, repr=False)
    scan_task_id              : int                                = field(default=None, init=False, repr=False)
    fill_task_id              : int                                = field(default=None, init=False, repr=False)

    def __post_init__(self):
        self.seeds        = ConsumableVarintSet(remaining_id_prefix_length=self.seed_id_prefix_length)
        self.seed_nounces = defaultdict(lambda : ConsumableVarintSet(remaining_id_prefix_length=self.nounce_id_prefix_length))
        self.hash_items   = SortedDict()
        self.progress = Progress(MofNCompleteColumn(),
            BarColumn(),
            MyTaskProgressColumn(show_speed=True),
            TimeElapsedColumn(),
            TimeRemainingColumn(),
            TextColumn("[progress.description]{task.description}"),
            #transient=False, 
            auto_refresh=True,
            #refresh_per_second=2, 
            speed_estimate_period=1,
        )
        self.main_task_id = self.progress.add_task("Scanning...", total=None, start=True)
        self.scan_task_id = self.progress.add_task("Attempts", total=None, start=True)
        #self.fill_task_id = self.progress.add_task("Fill", total=2**MAX_INIT_ITEM_LENGTH, start=True)
        self.progress.start()
        
    
    ### SCAN RANGES ###

    def collect_scan_range_seeds(self, scan_range: HashIdRange) -> List[int]:
        seeds = list()
        if (scan_range.range_type == HashIdRangeType.OLD_SEED):
            for seed_id in scan_range.seed_ids:
                seed = self.seeds.get_consumed_varint(consumed_varint_id=seed_id)
                seeds.append(seed)
            return seeds
        elif (scan_range.range_type == HashIdRangeType.NEW_SEED):
            for seed_id in scan_range.seed_ids:
                seed = self.seeds.get_remaining_varint(remaining_varint_id=seed_id)
                seeds.append(seed)
            return seeds
        raise Exception(f"Incorrect scan_range.range_type={scan_range.range_type}")
    
    def collect_scan_range_nounces(self, scan_range: HashIdRange, seed: int) -> List[int]:
        nounces = list()
        for nounce_id in scan_range.nounce_ids:
            nounce = self.seed_nounces[seed].get_remaining_varint(remaining_varint_id=nounce_id)
            nounces.append(nounce)
        return nounces

    def collect_old_seed_ranges(self) -> SortedDict[HashRangeId, HashIdRange]:
        seed_prefix_length   = self.seeds.consumed_id_prefix_length
        nounce_prefix_length = self.nounce_id_prefix_length
        range_type           = HashIdRangeType.OLD_SEED
        if (len(self.seeds.consumed_ids) == 0):
            # no saved seeds yet - first search run s
            return
        
        max_seed_id       = max(self.seeds.consumed_ids)
        max_seed_range    = get_number_range(number=max_seed_id)
        max_seed_range_id = max_seed_range[0]
        seed_ranges       = create_number_ranges(max_number_length=max_seed_range_id+1)
        #print(f"max_seed_range={max_seed_range}, seed_ranges={seed_ranges}, self.seeds.consumed_ids={self.seeds.consumed_ids}")

        max_nounce_range_id = (2 ** self.nounce_id_prefix_length)
        nounce_ranges       = create_number_ranges(max_number_length=max_nounce_range_id)
        max_nounce_range    = nounce_ranges[max_nounce_range_id-1]
        #print(f"max_nounce_range={max_nounce_range}, nounce_ranges={nounce_ranges}")

        for seed_range in seed_ranges:
            seed_id_start = seed_range[0]
            seed_id_end   = seed_range[1]
            if (max_seed_id < seed_id_end):
                seed_id_end = max_seed_id 
            if (seed_id_start == seed_id_end):
                seed_id_end = seed_id_start + 1
            
            #pprint(f"seed_range={seed_range}, seed_id_start={seed_id_start}, seed_id_end={seed_id_end}")
            seed_ids = range(seed_id_start, seed_id_end)
            for nounce_range in nounce_ranges:
                #pprint(f"nounce_range={nounce_range}")
                nounce_id_start = nounce_range[0]
                nounce_id_end   = nounce_range[1]
                nounce_ids      = range(nounce_id_start, nounce_id_end)
                hash_id_range = HashIdRange(
                    range_type           = range_type,
                    seed_ids             = list(seed_ids).copy(),
                    nounce_ids           = list(nounce_ids).copy(),
                    seed_prefix_length   = seed_prefix_length,
                    nounce_prefix_length = nounce_prefix_length,
                )
                if (hash_id_range.id in self.scan_ranges):
                    raise Exception(f"hash_id_range.id={hash_id_range.id} already exists (self.scan_ranges.keys(): {self.scan_ranges.keys()})")
                self.scan_ranges[hash_id_range.id] = hash_id_range
                self.ranges_data_lengths.add(hash_id_range.data_length)
                self.ranges_hash_id_lengths.add(hash_id_range.hash_id_length)
            
        return self.scan_ranges
    
    def collect_new_seed_ranges(self) -> SortedDict[HashRangeId, HashIdRange]:
        seed_prefix_length   = self.seed_id_prefix_length
        nounce_prefix_length = self.nounce_id_prefix_length
        range_type           = HashIdRangeType.NEW_SEED
        
        max_seed_range_id = (2 ** self.seed_id_prefix_length)
        seed_ranges       = create_number_ranges(max_number_length=max_seed_range_id)
        max_seed_range    = seed_ranges[max_seed_range_id-1]
        #print(f"max_seed_range={max_seed_range}, seed_ranges={seed_ranges}")

        max_nounce_range_id = (2 ** self.nounce_id_prefix_length)
        nounce_ranges       = create_number_ranges(max_number_length=max_nounce_range_id)
        max_nounce_range    = nounce_ranges[max_nounce_range_id-1]
        #print(f"max_nounce_range={max_nounce_range}, nounce_ranges={nounce_ranges}")

        for seed_range in seed_ranges:
            seed_id_start = seed_range[0]
            seed_id_end   = seed_range[1]
            seed_ids      = range(seed_id_start, seed_id_end)
            for nounce_range in nounce_ranges:
                nounce_id_start = nounce_range[0]
                nounce_id_end   = nounce_range[1]
                nounce_ids      = range(nounce_id_start, nounce_id_end)
                hash_id_range = HashIdRange(
                    range_type           = range_type,
                    seed_ids             = list(seed_ids).copy(),
                    nounce_ids           = list(nounce_ids).copy(),
                    seed_prefix_length   = seed_prefix_length,
                    nounce_prefix_length = nounce_prefix_length,
                )
                if (hash_id_range.id in self.scan_ranges):
                    raise Exception(f"hash_id_range.id={hash_id_range.id} already exists (self.scan_ranges.keys(): {self.scan_ranges.keys()})")
                self.scan_ranges[hash_id_range.id] = hash_id_range
                self.ranges_data_lengths.add(hash_id_range.data_length)
                self.ranges_hash_id_lengths.add(hash_id_range.hash_id_length)
        
        return self.scan_ranges

    def collect_scan_ranges(self) -> SortedDict[HashRangeId, HashIdRange]:
        # clear scan ranges before each new search (order of scanning is extremely important)
        self.scan_ranges.clear()
        self.ranges_data_lengths.clear()
        # scan ranges with existing seeds processed first
        self.collect_old_seed_ranges()
        # searching for new values only after checking existing
        self.collect_new_seed_ranges()
        return self.scan_ranges
    
    def get_scan_ranges_by_data_length(self, data_length: int) -> SortedDict[HashRangeId, HashIdRange]:
        ranges    = SortedDict()
        key       = hash_item_data_length_key_function
        validator = create_hash_item_data_length_validator_function(target_data_length=data_length)
        items     = bucket(self.scan_ranges.values(), key=key, validator=validator)
        for hash_range in list(items[data_length]):
            ranges[hash_range.id] = hash_range
        return ranges
    
    def get_scan_ranges_by_hash_id_length(self, hash_id_length: int) -> Union[SortedDict[HashRangeId, HashIdRange], Dict[HashRangeId, HashIdRange]]:
        ranges    = SortedDict()
        key       = hash_item_hash_id_length_key_function
        validator = create_hash_item_hash_id_length_validator_function(target_hash_id_length=hash_id_length)
        items     = bucket(self.scan_ranges.values(), key=key, validator=validator)
        for hash_range in list(items[hash_id_length]):
            ranges[hash_range.id] = hash_range
        return ranges
    
    def get_hash_id_type(self, hash_id: HashId) -> HashIdType:
        if (self.seeds.has_consumed_varint(consumed_varint=hash_id.seed)):
            #if (self.has_hash_id_huffman_code(hash_id=hash_id)):
            #    return HashIdType.HUFFMAN_CODE
            if (self.seed_nounces[hash_id.seed].has_consumed_varint(consumed_varint=hash_id.nounce)):
                return HashIdType.OLD_NOUNCE_OLD_SEED
            elif (self.seed_nounces[hash_id.seed].has_remaining_varint(remaining_varint=hash_id.nounce)):
                return HashIdType.NEW_NOUNCE_OLD_SEED
        elif (self.seeds.has_remaining_varint(remaining_varint=hash_id.seed)):
            if (self.seed_nounces[hash_id.seed].has_remaining_varint(remaining_varint=hash_id.nounce)):
                return HashIdType.NEW_NOUNCE_NEW_SEED
            else:
                raise Exception(f"hash_id={hash_id}: incorrect nounce: self.seed_nounces[{hash_id.seed}].remaining_values={self.seed_nounces[hash_id.seed].remaining_values}")
        else:
            raise Exception(f"hash_id={hash_id}: incorrect seed: self.seeds.remaining_values={self.seeds.remaining_values}")

    def get_hash_id_length(self, hash_id: HashId) -> int:
        hash_id_type    = self.get_hash_id_type(hash_id=hash_id)
        encoded_seed    = None
        encoded_nounce  = None
        if (hash_id_type == HashIdType.NEW_NOUNCE_OLD_SEED):
            #seed_id_prefix_length   = self.seeds.get_consumed_varint_prefix_length(consumed_varint=hash_id.seed)
            #seed_id_length          = self.seeds.get_consumed_varint_length(consumed_varint=hash_id.seed)
            encoded_seed   = self.seeds.encode_consumed_varint(consumed_varint=hash_id.seed)
            encoded_nounce = self.seed_nounces[hash_id.seed].encode_remaining_varint(remaining_varint=hash_id.nounce)
        elif (hash_id_type == HashIdType.NEW_NOUNCE_NEW_SEED):
            #seed_id_prefix_length   = self.seeds.get_next_consumed_varint_prefix_length(remaining_varint=hash_id.seed)
            #seed_id_length          = self.seeds.get_next_consumed_varint_length(remaining_varint=hash_id.seed)
            encoded_seed   = self.seeds.encode_remaining_varint(remaining_varint=hash_id.seed)
            encoded_nounce = self.seed_nounces[hash_id.seed].encode_remaining_varint(remaining_varint=hash_id.nounce)
        elif (hash_id_type == HashIdType.OLD_NOUNCE_OLD_SEED): # or (hash_id_type == HashIdType.HUFFMAN_CODE):
            if (self.has_saved_hash_item(hash_id=hash_id) is False):
                raise Exception(f"No saved hash item for hash_id={hash_id}, hash_id_type={hash_id_type}")
            seed_id_prefix_length   = self.get_seed_id_prefix_length()
            nounce_id_prefix_length = self.get_nounce_id_prefix_length(seed=hash_id.seed)
            if (self.hash_items[hash_id].need_updates(seed_id_prefix_length=seed_id_prefix_length, nounce_id_prefix_length=nounce_id_prefix_length)):
                update_msg = self.hash_items[hash_id].update_data(
                    seed_id_prefix_length   = seed_id_prefix_length, 
                    nounce_id_prefix_length = nounce_id_prefix_length
                )
                if (update_msg is not None):
                    self.progress.print(update_msg)
            return self.hash_items[hash_id].hash_id_length
            #encoded_seed   = self.seeds.encode_consumed_varint(consumed_varint=hash_id.seed)
            #encoded_nounce = self.seed_nounces[hash_id.seed].encode_consumed_varint(consumed_varint=hash_id.nounce)
        else:
            raise Exception(f"Incorrect hash_id_type={hash_id_type}")
        #nounce_id_prefix_length = self.seed_nounces[hash_id.seed].get_next_consumed_varint_prefix_length(remaining_varint=hash_id.nounce)
        #nounce_id_length        = self.seed_nounces[hash_id.seed].get_next_consumed_varint_length(remaining_varint=hash_id.nounce)
        #hash_id_length          = hash_id_type_length + seed_id_prefix_length + seed_id_length + nounce_id_prefix_length + nounce_id_length
        hash_id_type_length     = get_hash_id_type_length(hash_id_type=hash_id_type)
        hash_id_length          = hash_id_type_length + encoded_seed.bit_length + encoded_nounce.bit_length
        return hash_id_length

    def create_init_item(self, hash_id: HashId, digest: int, data_length: int=None) -> InitHashItem:
        hash_id_type   = self.get_hash_id_type(hash_id=hash_id)
        hash_id_length = self.get_hash_id_length(hash_id=hash_id)
        
        if (hash_id_type == HashIdType.OLD_NOUNCE_OLD_SEED):
            encoded_seed         = self.seeds.encode_consumed_varint(consumed_varint=hash_id.seed).data
            encoded_nounce       = self.seed_nounces[hash_id.seed].encode_consumed_varint(consumed_varint=hash_id.nounce).data
        elif (hash_id_type == HashIdType.NEW_NOUNCE_OLD_SEED):
            encoded_seed         = self.seeds.encode_consumed_varint(consumed_varint=hash_id.seed).data
            encoded_nounce       = self.seed_nounces[hash_id.seed].encode_remaining_varint(remaining_varint=hash_id.nounce).data
        elif (hash_id_type == HashIdType.NEW_NOUNCE_NEW_SEED):
            encoded_seed         = self.seeds.encode_remaining_varint(remaining_varint=hash_id.seed).data
            encoded_nounce       = self.seed_nounces[hash_id.seed].encode_remaining_varint(remaining_varint=hash_id.nounce).data
        
        if (data_length is None):
            data_length = hash_id_length + DEFAULT_ITEM_SCORE
        if (data_length > MAX_INIT_ITEM_LENGTH):
            data_length = MAX_INIT_ITEM_LENGTH

        return InitHashItem(
            hash_id        = hash_id,
            hash_id_type   = hash_id_type,
            digest         = digest,
            hash_id_length = hash_id_length,
            data_length    = data_length,
            encoded_seed   = encoded_seed,
            encoded_nounce = encoded_nounce,
        )

    def save_new_hash_item(self, init_item: InitHashItem) -> SavedHashItem:
        if (init_item.hash_id_type == HashIdType.OLD_NOUNCE_OLD_SEED):
            return self.use_saved_hash_item(init_item=init_item)
        # store prefix length values before tree update
        old_seed_id_prefix_length   = self.get_seed_id_prefix_length()
        old_nounce_id_prefix_length = self.get_nounce_id_prefix_length(seed=init_item.seed)
        # save seed and nounce pair into tree
        if (init_item.hash_id_type == HashIdType.NEW_NOUNCE_NEW_SEED):
            self.seeds.consume_value(remaining_value=init_item.seed)
        self.seed_nounces[init_item.seed].consume_value(remaining_value=init_item.nounce)
        # check any changes after adding new values
        new_seed_id_prefix_length   = self.get_seed_id_prefix_length()
        new_nounce_id_prefix_length = self.get_nounce_id_prefix_length(seed=init_item.seed)
        # save new hash item 
        hash_item = SavedHashItem(
            init_item               = init_item, 
            seeds                   = self.seeds, #self.seeds.consumed_values, 
            seed_nounces            = self.seed_nounces[init_item.seed], #self.seed_nounces[init_item.seed].consumed_values,
            seed_id_prefix_length   = new_seed_id_prefix_length, #self.seed_id_prefix_length,
            seed_id_length          = self.seeds.encode_consumed_varint(consumed_varint=init_item.seed).bit_length,
            nounce_id_prefix_length = new_nounce_id_prefix_length, #self.nounce_id_prefix_length,
            nounce_id_length        = self.seed_nounces[init_item.seed].encode_consumed_varint(consumed_varint=init_item.nounce).bit_length,
        )
        # update saved items registry
        self.hash_items[hash_item.hash_id] = hash_item
        self.saved_data_lengths.add(hash_item.data_length)
        self.saved_data_ids[hash_item.data_length].add(hash_item.data_id[1])

        # update saved hash_item
        self.hash_items[hash_item.hash_id].update_data(seed_id_prefix_length=new_seed_id_prefix_length, nounce_id_prefix_length=new_nounce_id_prefix_length)
        
        # update global score
        self.layer_score += hash_item.init_item.score
        # update usage counts
        self.hash_id_counts.update({ hash_item.hash_id: 1 })
        self.update_huffman_codes()
        
        # TODO: hash items: update only changed
        if (old_seed_id_prefix_length != new_seed_id_prefix_length):
            self.update_hash_items()
        elif (old_nounce_id_prefix_length != new_nounce_id_prefix_length):
            seed_items = self.get_hash_items_by_seed(seed=hash_item.seed)
            self.update_hash_items(hash_items=seed_items)
        #self.update_hash_items()
        
        # return updated item
        return self.hash_items[hash_item.hash_id]
    
    def use_saved_hash_item(self, init_item: InitHashItem) -> SavedHashItem:
        hash_item = self.get_saved_hash_item(hash_id=init_item.hash_id)
        update_msg = self.hash_items[hash_item.hash_id].update_score()
        self.layer_score += hash_item.score
        self.hash_id_counts.update({ hash_item.hash_id: 1 })
        if (update_msg is not None):
            self.progress.print(update_msg)
            self.progress.print(f"layer_score={self.layer_score}")
            self.progress.print(f"hash_id_counts: {dict(self.hash_id_counts.most_common(16))}")
            self.update_hash_items()
            self.update_huffman_codes()
        return hash_item
    
    def get_seed_id_prefix_length(self) -> int:
        return self.seeds.consumed_id_prefix_length
    
    def get_nounce_id_prefix_length(self, seed: int) -> int:
        #if (self.seeds.has_consumed_varint(consumed_varint=seed) is False):
        #    raise Exception(f"Cannot find nounce prefix for seed={seed}")
        return self.seed_nounces[seed].consumed_id_prefix_length
    
    def has_saved_hash_item(self, hash_id: HashId) -> SavedHashItem:
        if (hash_id in self.hash_id_huffman_codes):
            return True
        return (hash_id in self.hash_items)

    def get_saved_hash_item(self, hash_id: HashId) -> SavedHashItem:
        if (self.has_saved_hash_item(hash_id=hash_id) is False):
            raise Exception(f"hash_id={hash_id} not registered")
        return self.hash_items[hash_id]
    
    def get_hash_items_by_seed(self, seed: int) -> List[SavedHashItem]:
        key       = hash_item_seed_key_function
        validator = create_hash_item_seed_validator_function(target_seed=seed)
        items     = bucket(self.hash_items.values(), key=key, validator=validator)
        return items[seed]
    
    def get_hash_items_by_data_length(self, data_length: int) -> List[SavedHashItem]:
        key       = hash_item_data_length_key_function
        validator = create_hash_item_data_length_validator_function(target_data_length=data_length)
        items     = bucket(self.hash_items.values(), key=key, validator=validator)
        return items[data_length]
    
    def get_hash_items_by_data_item(self, data_item: int) -> List[SavedHashItem]:
        key       = hash_item_data_key_function
        validator = create_hash_item_data_validator_function(target_data=data_item)
        items     = bucket(self.hash_items.values(), key=key, validator=validator)
        return items[data_item]
    
    def get_hash_items_by_hash_id_length(self, hash_id_length: int) -> List[SavedHashItem]:
        key       = hash_item_hash_id_length_key_function
        validator = create_hash_item_hash_id_length_validator_function(target_data_length=hash_id_length)
        items     = bucket(self.hash_items.values(), key=key, validator=validator)
        return items[hash_id_length]
    
    def has_hash_id_huffman_code(self, hash_id: HashId) -> bool:
        return (hash_id in self.hash_id_huffman_codes)
    
    def has_hash_id_huffman_code(self, hash_id: HashId) -> bool:
        return (hash_id in self.hash_id_data_codes)
    
    def get_hash_id_huffman_code(self, hash_id: HashId) -> bitarray:
        return self.hash_id_huffman_codes[hash_id]
    
    def get_hash_id_huffman_code_data(self, hash_id: HashId) -> frozenbitarray:
        return self.hash_id_data_codes[hash_id]

    def update_huffman_codes(self):
        top_hash_ids  = dict(self.hash_id_counts.most_common(MAX_TOP_ITEMS))
        hash_id_codes = huffman_code(top_hash_ids)
        self.hash_id_huffman_codes = hash_id_codes
        
        # update data codes
        for hash_id, hash_id_huffman_code in self.hash_id_huffman_codes.items():
            hash_id_code_length   = len(hash_id_huffman_code)
            hash_id_type_length   = get_hash_id_type_length(hash_id_type=HashIdType.OLD_NOUNCE_OLD_SEED)
            data_item_code_length = hash_id_type_length + hash_id_code_length + DEFAULT_ITEM_SCORE
            data_item_code        = self.hash_items[hash_id].create_data_bits(bit_length=data_item_code_length)
            self.hash_id_data_codes[hash_id]           = data_item_code.copy()
            self.hash_items[hash_id].huffman_code      = hash_id_huffman_code.copy()
            self.hash_items[hash_id].huffman_data_code = data_item_code.copy()
            self.hash_items[hash_id].update_data(
                seed_id_prefix_length   = self.hash_items[hash_id].seed_id_prefix_length, 
                nounce_id_prefix_length = self.hash_items[hash_id].nounce_id_prefix_length
            )

        # log huffman code updates
        #pprint(self.hash_id_huffman_codes, expand_all=True)
        pprint(dict(list(self.hash_id_data_codes.items())[0:8]), expand_all=True)
        self.progress.print(f"Huffman_codes: {len(self.hash_id_huffman_codes)}")

    def update_hash_items(self, hash_items: List[SavedHashItem]=None):
        if (hash_items is None):
            hash_items = self.hash_items.values()
            self.saved_data_ids.clear()
            self.scanned_data_ids.clear()
        
        seed_id_prefix_length = self.get_seed_id_prefix_length()
        for hash_item in hash_items:
            nounce_id_prefix_length = self.get_nounce_id_prefix_length(seed=hash_item.hash_id.seed)
            if (hash_item.need_updates(seed_id_prefix_length=seed_id_prefix_length, nounce_id_prefix_length=nounce_id_prefix_length)):
                old_data_length = hash_item.data_length
                old_data_id     = copy(hash_item.data_id)
                old_data        = hash_item.data.copy()
                # update related data
                update_msg = hash_item.update_data(
                    seed_id_prefix_length   = seed_id_prefix_length, 
                    nounce_id_prefix_length = nounce_id_prefix_length
                )
                if (update_msg is not None):
                    self.progress.print(update_msg)
                if (old_data_length != hash_item.data_length) or (old_data_id != hash_item.data_id) or (old_data != hash_item.data):
                    #self.saved_data_ids[old_data_length].remove(old_data_id[1])
                    self.saved_data_ids[old_data_length].discard(old_data_id[1])
                    self.progress.print(f"Removed : old_data={old_data} id={old_data_id} (l={old_data_length})", soft_wrap=True, crop=False)
                    self.progress.print(f"Added   : new_data={hash_item.data} id={hash_item.data_id} (l={hash_item.data_length})", soft_wrap=True, crop=False)
                self.saved_data_lengths.add(hash_item.data_length)
                self.saved_data_ids[hash_item.data_length].add(hash_item.data_id[1])
            #if (hash_item.data_id[1] in self.saved_data_ids[hash_item.data_length]):

    def has_saved_data_item(self, data_item: frozenbitarray) -> bool:
        item = self.get_saved_data_item(data_item=data_item)
        if (item is None):
            return False
        return True
    
    def get_saved_data_item(self, data_item: frozenbitarray) -> Union[SavedHashItem, None]:
        data_length = len(data_item)
        hash_items  = list(self.get_hash_items_by_data_length(data_length=data_length))
        return first_true(hash_items, default=None, pred=lambda hash_item: hash_item.data == data_item)
    
    def search_saved_hash_item(self, data: frozenbitarray) -> Union[InitHashItem, None]:
        saved_data_item = None
        for data_length in self.saved_data_lengths:
            data_item = data[0:data_length]
            #self.progress.print(f"data_length={data_length}, data_item={data_item}", soft_wrap=True, crop=False)
            self.progress.update(
                task_id     = self.main_task_id, 
                description = f"data_length={data_length}, data_item={data_item}"
            )
            items = list(self.get_hash_items_by_data_item(data_item=data_item))
            if (len(items) > 0):
                saved_data_item = items[0]
            else:
                saved_data_item = self.get_saved_data_item(data_item=data_item)
            #self.get_saved_data_item(data_item=data_item)
            if (saved_data_item is not None):
                #pprint(f"found existing item: {saved_data_item}")
                #return saved_data_item
                return self.create_init_item(
                    hash_id     = saved_data_item.hash_id,
                    data_length = data_length,
                    digest      = saved_data_item.digest
                )
        return saved_data_item
    
    #def has_saved_data_id(self, data_length: int, data_id_value: int) -> bool:
    #    if (data_length not in self.saved_data_ids):
    #        return False
    #    return (data_id_value in self.saved_data_ids[data_length])
    
    #def has_scanned_data_id(self, data_length: int, data_id_value: int) -> bool:
    #    if (data_length not in self.scanned_data_ids):
    #        return False
    #    return (data_id_value in self.scanned_data_ids[data_length])

    def scan_hash_id(self, seed: int, nounce: int, scan_range: HashIdRange) -> ScannedHashId: # , prev_lengths: Sequence[int]
        offset = 0

        while True:
            hash_seed          = seed + offset
            hash_digest        = last_int_bits_from_nounce(nounce=nounce, seed=hash_seed, bit_length=MAX_DATA_ITEM_LENGTH) #int_from_nounce(nounce=nounce, seed=hash_seed)
            has_prev_item      = False

            for prev_length in self.scanned_data_ids.keys():
                if (prev_length > scan_range.data_length):
                    break
                prev_item_id_value = last_int_bits_from_digest(digest=hash_digest, bit_length=prev_length)
                #prev_item          = last_fba_bits_from_digest(digest=hash_digest, bit_length=prev_length, endian=DEFAULT_ENDIAN, signed=False)
                #prev_item_id_value = ba2int(prev_item, signed=False) #ba2tuple(data=prev_item, signed=False)
                #if (self.has_scanned_data_id(data_length=prev_length, data_id_value=prev_item_id_value)):
                if (prev_item_id_value in self.scanned_data_ids[prev_length]):
                    has_prev_item = True
                    break
            if (has_prev_item is True):
                offset               += 1
                self.item_difficulty += 1
                continue

            for prev_length in self.saved_data_ids.keys():
                if (prev_length > scan_range.data_length):
                    break
                prev_item_id_value = last_int_bits_from_digest(digest=hash_digest, bit_length=prev_length)
                #prev_item          = last_fba_bits_from_digest(digest=hash_digest, bit_length=prev_length, endian=DEFAULT_ENDIAN, signed=False)
                #prev_item_id_value = ba2int(prev_item, signed=False) #ba2tuple(data=prev_item, signed=False)
                #if (self.has_saved_data_id(data_length=prev_length, data_id_value=prev_item_id_value)):
                if (prev_item_id_value in self.saved_data_ids[prev_length]):
                    has_prev_item = True
                    break
            if (has_prev_item is True):
                offset               += 1
                self.item_difficulty += 1
                continue

            # unique item found
            #prev_item          = last_fba_bits_from_digest(digest=hash_digest, bit_length=scan_range.data_length, endian=DEFAULT_ENDIAN, signed=False)
            #prev_item_id_value = ba2int(prev_item, signed=False) #ba2tuple(data=prev_item, signed=False)
            break
        
        # update scan progress
        self.progress.advance(self.main_task_id)
        self.progress.advance(self.scan_task_id, advance=offset)
        prev_item_id_value = last_int_bits_from_digest(digest=hash_digest, bit_length=scan_range.data_length)
        
        return ScannedHashId(digest=hash_digest, data_id_value=prev_item_id_value, offset=offset)
    
    def scan_hash_range_for_data_item(self, scan_range: HashIdRange, data_item: frozenbitarray) -> Union[InitHashItem, None]:
        seeds       = self.collect_scan_range_seeds(scan_range=scan_range)
        assert(len(data_item) == scan_range.data_length)
        data_length = scan_range.data_length #len(data_item)
        
        #max_length = scan_range.data_length
        if (data_length > self.max_init_item_length):
            data_length = self.max_init_item_length
        #    #data_length = data_item[0:data_length]
        data_item_id       = ba2tuple(data=data_item)
        data_item_id_value = ba2int(data_item, signed=False) #data_item_id[1]
        
        #if (min_length > data_length):
        #    raise Exception(f"Incorrect data_length: data_length={data_length} < min_length={min_length}")
        
        #prev_lengths = SortedSet()
        #for prev_length in range(1, self.max_init_item_length):
        #    prev_lengths.add(prev_length)
        #prev_lengths       = range(min_length, data_length + 1)
        #print(f"seeds: {len(seeds)}")
        #if (len(scan_range.seed_ids) * len(scan_range.nounce_ids) > 1000):
        #    seeds = track(sequence=seeds)
        for seed in seeds:
            nounces = self.collect_scan_range_nounces(scan_range=scan_range, seed=seed)
            for nounce in nounces:
                scanned_hash_id = self.scan_hash_id(
                    seed         = seed, 
                    nounce       = nounce, 
                    scan_range   = scan_range,
                    #prev_lengths = prev_lengths,
                    #data_length  = scan_range.data_length, 
                    #data_item_id = data_item_id
                )
                #print(f"nounces: {len(nounces)}")
                #print("scanned_hash_id", scanned_hash_id, data_item_id_value)
                #print(data_item)
                #print(int2ba(scanned_hash_id.data_id_value, length=scan_range.data_length, endian=DEFAULT_ENDIAN), ba2tuple(data=data_item))
                #print("self.scanned_data_ids")
                #print(self.scanned_data_ids)
                if (data_item_id_value == scanned_hash_id.data_id_value):
                    init_item = self.create_init_item(
                        hash_id     = HashId(seed=seed, nounce=nounce),
                        data_length = data_length,
                        digest      = scanned_hash_id.digest,
                    )
                    self.item_difficulty += scanned_hash_id.offset
                    init_item.difficulty = self.item_difficulty
                    init_item.offset     = scanned_hash_id.offset
                    return init_item
                else:
                    self.scanned_data_ids[data_length].add(scanned_hash_id.data_id_value)
                    #self.scanned_data_ids[scan_range.data_length].add(data_item_id_value)
                    #self.scanned_data_lengths.add(data_length)
                    #self.scanned_data_lengths.add(scan_range.data_length)
        return None
    
    def scan_hash_range_for_hash_id(self, scan_range: HashIdRange, hash_id: HashId) -> Union[InitHashItem, None]: #v , data_length: int=None
        seeds       = self.collect_scan_range_seeds(scan_range=scan_range)
        data_length = scan_range.data_length

        if (data_length > self.max_init_item_length):
            data_length = self.max_init_item_length
        
        #prev_lengths = SortedSet()
        #for prev_length in range(1, self.max_init_item_length):
        #    prev_lengths.add(prev_length)
        #prev_lengths = range(min_length, data_length + 1)
        #if (len(scan_range.seed_ids) * len(scan_range.nounce_ids) > 1000):
        #    seeds = track(sequence=seeds)
        for seed in seeds:
            nounces = self.collect_scan_range_nounces(scan_range=scan_range, seed=seed)
            for nounce in nounces:
                scanned_hash_id = self.scan_hash_id(
                    seed         = seed, 
                    nounce       = nounce, 
                    scan_range   = scan_range,
                    #prev_lengths = prev_lengths,
                )
                if (seed == hash_id.seed) and (nounce == hash_id.nounce):
                    init_item = self.create_init_item(
                        hash_id     = HashId(seed=seed, nounce=nounce),
                        data_length = scan_range.data_length,
                        digest      = scanned_hash_id.digest,
                    )
                    self.item_difficulty += scanned_hash_id.offset
                    init_item.difficulty = self.item_difficulty
                    init_item.offset     = scanned_hash_id.offset
                    return init_item
                else:
                    self.scanned_data_ids[data_length].add(scanned_hash_id.data_id_value)
                    #self.scanned_data_lengths.add(data_length)
                    #self.scanned_data_lengths.add(scan_range.data_length)
        return None
    
    def search_new_hash_item(self, data: frozenbitarray) -> InitHashItem:
        new_hash_item = None
        #for data_length in self.ranges_data_lengths:
        self.item_difficulty = 0
        self.progress.update(task_id=self.scan_task_id, total=None, completed=0)
        for hash_id_length in self.ranges_hash_id_lengths:
            #scan_ranges = self.get_scan_ranges_by_data_length(data_length=data_length)
            scan_ranges = self.get_scan_ranges_by_hash_id_length(hash_id_length=hash_id_length)
            #print(f"data_length={data_length}, data_length={data_length}, data_item={data_item}, scan_ranges: {len(scan_ranges)}")
            for _, scan_range in scan_ranges.items():
                #data_length = hash_id_length + DEFAULT_ITEM_SCORE
                data_length = scan_range.data_length
                if (data_length > MAX_INIT_ITEM_LENGTH):
                    data_length = MAX_INIT_ITEM_LENGTH
                data_item = data[0:data_length]
                self.progress.update(
                    task_id     = self.main_task_id, 
                    total       = scan_range.total, 
                    description = f"s: {min(scan_range.id.seed_ids)}-{max(scan_range.id.seed_ids)}, n: {min(scan_range.id.nounce_ids)}-{max(scan_range.id.nounce_ids)}, dl={data_length}, di={data_item}"
                )
                new_hash_item = self.scan_hash_range_for_data_item(scan_range=scan_range, data_item=data_item)
                
                self.progress.reset(task_id=self.main_task_id)
                if (new_hash_item is not None):
                    return new_hash_item
        if (new_hash_item is None):
            pprint(self.ranges_hash_id_lengths)
            pprint(scan_ranges)
            raise Exception(f"Unable to encode: data_length={len(data_item)}, data_item={data_item}, try to more seed/nounce ranges for search")
    
    def search_new_hash_id(self, hash_id: HashId) -> InitHashItem:
        new_hash_item = None
        #for data_length in self.ranges_data_lengths:
        self.item_difficulty = 0
        self.progress.update(task_id=self.scan_task_id, total=None, completed=0)
        for hash_id_length in self.ranges_hash_id_lengths:
            #scan_ranges = self.get_scan_ranges_by_data_length(data_length=data_length)
            scan_ranges = self.get_scan_ranges_by_hash_id_length(hash_id_length=hash_id_length)
            if (len(scan_ranges) == 0):
                continue
            #print(f"hash_id_length={hash_id_length}, target hash_id={hash_id}, scan_ranges: {len(scan_ranges)}") # data_length={data_length}, 
            for _, scan_range in scan_ranges.items():
                #data_length = hash_id_length + DEFAULT_ITEM_SCORE
                data_length = scan_range.data_length
                if (data_length > MAX_INIT_ITEM_LENGTH):
                    data_length = MAX_INIT_ITEM_LENGTH
                self.progress.update(
                    task_id     = self.main_task_id, 
                    total       = scan_range.total, 
                    description = f"s: {min(scan_range.id.seed_ids)}-{max(scan_range.id.seed_ids)}, n: {min(scan_range.id.nounce_ids)}-{max(scan_range.id.nounce_ids)}, dl={data_length}, di=(restoring...)"
                )
                new_hash_item = self.scan_hash_range_for_hash_id(scan_range=scan_range, hash_id=hash_id) # , data_length=data_length

                self.progress.reset(task_id=self.main_task_id)
                if (new_hash_item is not None):
                    return new_hash_item
        if (new_hash_item is None):
            pprint(self.ranges_hash_id_lengths)
            pprint(scan_ranges)
            raise Exception(f"Unable to restore: hash_id={hash_id}, decoding failed")

    def search_data_item(self, data_item: frozenbitarray) -> InitHashItem:
        # 1) search saved items
        init_item = self.search_saved_hash_item(data=data_item)
        if (init_item is not None):
            return init_item
        # 2) search new item ranges
        self.collect_scan_ranges()
        self.update_hash_items()
        self.scanned_data_ids.clear()
        #self.scanned_data_ids = deepcopy(self.saved_data_ids)
        #self.scanned_data_lengths.clear()

        return self.search_new_hash_item(data=data_item)
    
    def restore_init_item_from_hash_id(self, hash_id: HashId) -> InitHashItem: # , hash_id_type: HashIdType)
        #if (hash_id_type == HashIdType.OLD_NOUNCE_OLD_SEED):
        #    saved_item = self.hash_items[hash_id]
        #    return self.create_init_item(hash_id=hash_id, data_length=saved_item.data_length, digest=saved_item.digest)
        if (self.has_saved_hash_item(hash_id=hash_id)):
            saved_item = self.get_saved_hash_item(hash_id=hash_id)
            return self.create_init_item(hash_id=hash_id, data_length=saved_item.data_length, digest=saved_item.digests)
        # restoring hash digest by replaying all search operations
        self.collect_scan_ranges()
        self.update_hash_items()
        self.scanned_data_ids.clear()
        #self.scanned_data_ids = deepcopy(self.saved_data_ids)
        #self.scanned_data_lengths.clear()
        
        return self.search_new_hash_id(hash_id=hash_id)

    def encode_hash_id(self, hash_id: HashId) -> InitHashItem:
        if (self.has_saved_hash_item(hash_id=hash_id)):
            saved_item = self.get_saved_hash_item(hash_id=hash_id)
            return self.create_init_item(hash_id=hash_id, digest=saved_item.digest) #, data_length=saved_item.data_length)
        return self.restore_init_item_from_hash_id(hash_id=hash_id)
    
    def encode_data_item(self, data_item: frozenbitarray) -> InitHashItem:
        if (data_item.endian() != DEFAULT_ENDIAN):
            raise Exception(f"Incorrect input data endian. Use '{self.default_endian}' ('{data_item.endian()}' given)")
        return self.search_data_item(data_item=data_item)
        #return self.encode_hash_id(hash_id=init_item.hash_id)
    
    def encode_data(self, data: frozenbitarray) -> bitarray: #InitHashItem:
        item_id        = 0
        position       = 0
        remaining_data = data
        encoded_bits   = bitarray('', endian=DEFAULT_ENDIAN)
        if (data.endian() != DEFAULT_ENDIAN):
            raise Exception(f"Incorrect input data endian. Use '{self.default_endian}' ('{data.endian()}' given)")
        
        while True:
            data_item       = remaining_data[0:self.max_data_item_length]
            encoded_hash_id = self.encode_data_item(data_item=data_item)
            
            data_start       = encoded_hash_id.data_length
            remaining_data   = remaining_data[data_start:len(remaining_data)]
            # update position and element id
            item_id         += 1
            position        += encoded_hash_id.data_length
            encoded_bits    += encoded_hash_id.encoded_hash_item
            # save value to the tree, updating all related elements
            saved_hash_item = self.save_new_hash_item(init_item=encoded_hash_id)
            if (self.debug):
                self.progress.print(f"\n{item_id}: score={encoded_hash_id.score}, p={position}, ({self.layer_score}), saved_hash_item={saved_hash_item}")
                #self.progress.print(f"\n{item_id}: saved_hash_item={saved_hash_item}\n")
            if (len(remaining_data) < self.max_data_item_length):
                self.data_tail        = remaining_data.copy()
                self.data_tail_length = len(self.data_tail)
                break
        
        self.progress.stop()
        return encoded_bits + self.data_tail
    
    def decode_hash_item(self, hash_item_data: frozenbitarray) -> InitHashItem:
        if (hash_item_data.endian() != DEFAULT_ENDIAN):
            raise Exception(f"Incorrect input data endian. Use '{DEFAULT_ENDIAN}' ('{hash_item_data.endian()}' given)")
        
        bit_length          = 0
        hash_id_type        = decode_hash_id_type(encoded_hash_id_type=hash_item_data[0:2].copy())
        #print(f"hash_item_data={hash_item_data}, hash_id_type={hash_id_type}, hash_item_data[0:2]={hash_item_data[0:2]}")
        hash_id_type_length = get_hash_id_type_length(hash_id_type=hash_id_type)
        hash_id_bits        = hash_item_data[hash_id_type_length:len(hash_item_data)].copy()
        bit_length         += hash_id_type_length
        
        if (hash_id_type == HashIdType.OLD_NOUNCE_OLD_SEED):
            decoded_seed   = self.seeds.decode_consumed_varint(encoded_consumed_varint=hash_id_bits)
            seed           = decoded_seed.number
            nounce_start   = decoded_seed.bit_length
            nounce_end     = nounce_start + len(hash_item_data)
            nounce_bits    = hash_id_bits[nounce_start:nounce_end].copy()
            bit_length    += decoded_seed.bit_length
            decoded_nounce = self.seed_nounces[seed].decode_consumed_varint(encoded_consumed_varint=nounce_bits)
            bit_length    += decoded_nounce.bit_length
            nounce         = decoded_nounce.number
            hash_id        = HashId(seed=seed, nounce=nounce)
            saved_item     = self.hash_items[hash_id]
            return self.create_init_item(hash_id=hash_id, data_length=saved_item.data_length, digest=saved_item.digest)
        
        if (hash_id_type == HashIdType.NEW_NOUNCE_OLD_SEED):
            decoded_seed   = self.seeds.decode_consumed_varint(encoded_consumed_varint=hash_id_bits)
            seed           = decoded_seed.number
            nounce_start   = decoded_seed.bit_length
            nounce_end     = nounce_start + len(hash_item_data)
            nounce_bits    = hash_id_bits[nounce_start:nounce_end].copy()
            bit_length    += decoded_seed.bit_length
            decoded_nounce = self.seed_nounces[seed].decode_remaining_varint(encoded_remaining_varint=nounce_bits)
            bit_length    += decoded_nounce.bit_length
            nounce         = decoded_nounce.number
            hash_id        = HashId(seed=seed, nounce=nounce)
            return self.restore_init_item_from_hash_id(hash_id=hash_id)
        
        if (hash_id_type == HashIdType.NEW_NOUNCE_NEW_SEED):
            decoded_seed   = self.seeds.decode_remaining_varint(encoded_remaining_varint=hash_id_bits)
            seed           = decoded_seed.number
            nounce_start   = decoded_seed.bit_length
            nounce_end     = nounce_start + len(hash_item_data)
            nounce_bits    = hash_id_bits[nounce_start:nounce_end].copy()
            bit_length    += decoded_seed.bit_length
            decoded_nounce = self.seed_nounces[seed].decode_remaining_varint(encoded_remaining_varint=nounce_bits)
            bit_length    += decoded_nounce.bit_length
            nounce         = decoded_nounce.number
            hash_id        = HashId(seed=seed, nounce=nounce)
            return self.restore_init_item_from_hash_id(hash_id=hash_id)
        
        raise Exception(f"Incorrect hash_id_type={hash_id_type}")

    def decode_data(self, data: bitarray, tail_length: int) -> bitarray:
        item_id        = 0
        position       = 0
        decoded_bits   = bitarray('', endian=DEFAULT_ENDIAN)
        if (data.endian() != DEFAULT_ENDIAN):
            raise Exception(f"Incorrect input data endian. Use '{self.default_endian}' ('{data.endian()}' given)")
        
        if (tail_length > 0):
            data_start = 0
            data_end   = len(data) - tail_length
            data_bits  = data[data_start:data_end]
            tail_bits  = data[data_end:len(data)]
        else:
            data_bits = data
            tail_bits = bitarray('', endian=DEFAULT_ENDIAN)
        
        remaining_data = data_bits
        while True:
            hash_item_bits    = remaining_data[0:self.max_data_item_length]
            decoded_hash_item = self.decode_hash_item(hash_item_data=hash_item_bits)

            if (self.debug is True):
                self.progress.print(f"D {item_id} ({position}): init_hash_item={decoded_hash_item}")
            data_start      = decoded_hash_item.hash_id_length
            remaining_data  = remaining_data[data_start:]
            decoded_bits   += decoded_hash_item.data
            item_id        += 1
            position       += decoded_hash_item.data_length
            
            self.save_new_hash_item(init_item=decoded_hash_item)
            if (len(remaining_data) == 0):
                break
        if (tail_length > 0):
            decoded_bits += tail_bits
        
        self.progress.stop()
        return decoded_bits
        
    
