# https://github.com/Textualize/rich
from rich import print
# https://rich.readthedocs.io/en/latest/pretty.html
from rich.pretty import pprint
from typing import List, Set, Dict, Tuple, Optional, Union, Iterable
from bitarray import bitarray, frozenbitarray
from bitarray.util import int2ba, ba2int
from custom_counter import CustomCounter as Counter
from collections import defaultdict
from tqdm import tqdm
from hash_range_iterator import bits_at_position

# https://realpython.com/lru-cache-python/
from functools import lru_cache

# https://rich.readthedocs.io/en/latest/progress.html
from rich.progress import track, Progress, \
    SpinnerColumn, BarColumn, TextColumn, TimeElapsedColumn, MofNCompleteColumn, \
    TaskProgressColumn, TimeRemainingColumn, TransferSpeedColumn

from dataclasses import dataclass

DEFAULT_ENDIAN = 'big'

C1_COUNTS = {
    2: 1,
    3: 1,
    4: 2,
    5: 3,
    6: 5,
    7: 8,
    8: 13,
    9: 21,
    10: 34,
    11: 55,
    12: 89,
    13: 144,
    14: 233,
    15: 377,
    16: 610,
    17: 987,
    18: 1597,
    19: 2584,
    20: 4181,
    21: 6765,
    22: 10946,
    23: 17711,
    24: 28657,
    25: 46368,
    26: 75025,
    27: 121393,
    28: 196418,
    29: 317811,
    30: 514229,
    31: 832040,
}

@dataclass()
class DecodedNumber:
    value  : int
    length : int

@lru_cache(maxsize=2**17)
def encode_c1_number(number: List[int], endian: str=DEFAULT_ENDIAN) -> bitarray:
    return fib_encode_position(position=number, encoder_type='C1', endian=endian)

def decode_c1_number(bits: bitarray, endian: str=DEFAULT_ENDIAN) -> DecodedNumber:
    decoded_number = from_c1_bits(data=bits, endian=endian)
    decoded_length = get_encoded_c1_length(data=bits, endian=endian)
    return DecodedNumber(
        value  = decoded_number,
        length = decoded_length,
    )

# Python3 program for Fibonacci Encoding
# of a positive integer n
# This code is contributed by Mohit Kumar (Thank you, Mohit!)
 
# To limit on the largest
# Fibonacci number to be used
N = 2**24
 
# Array to store fibonacci numbers.
# fib[i] is going to store
# (i+2)'th Fibonacci number
fib = [0 for i in range(N)]
#for i in range(0, len())
 
# Stores values in fib and returns index of
# the largest fibonacci number smaller than n.
@lru_cache(maxsize=2**21)
def largestFiboLessOrEqual(n: int) -> int:
    fib[0] = 1 # Fib[0] stores 2nd Fibonacci No.
    fib[1] = 2 # Fib[1] stores 3rd Fibonacci No.
    # Keep Generating remaining numbers while
    # previously generated number is smaller
    i = 2
    while fib[i - 1] <= n:
        fib[i] = fib[i - 1] + fib[i - 2]
        i += 1
    # Return index of the largest fibonacci number
    # smaller than or equal to n. Note that the above
    # loop stopped when fib[i-1] became larger.
    return (i - 2)

_fibnum = largestFiboLessOrEqual(N-1)
 
# Returns pointer to the char string which
# corresponds to code for n
@lru_cache(maxsize=2**16)
def to_c1_code(n: int) -> str:
    index = largestFiboLessOrEqual(n)
    # allocate memory for codeword
    codeword = ['a' for i in range(index + 2)]
 
    # index of the largest Fibonacci f <= n
    i = index
 
    while (n):
        # Mark usage of Fibonacci f (1 bit)
        codeword[i] = '1'
        # Subtract f from n
        n = n - fib[i]
        # Move to Fibonacci just smaller than f
        i = i - 1
        # Mark all Fibonacci > n as not used (0 bit),
        # progress backwards
        while (i >= 0 and fib[i] > n):
            codeword[i] = '0'
            i = i - 1
    # additional '1' bit
    codeword[index + 1] = '1'
    # return pointer to codeword
    return "".join(codeword)

def from_c1_bits(data: Union[bitarray, frozenbitarray], endian: str='big') -> int:
    return fib_decode_position(position_bits=data, encoder_type='C1', endian=endian)

def to_c1_bits(n: int, endian: str='big') -> bitarray:
    return bitarray(to_c1_code(n), endian=endian)

def get_encoded_c1_length(data: Union[bitarray, frozenbitarray], endian: str='big') -> int:
    last_bit_position = len(data) - 1
    marker            = bitarray('11', endian=endian)
    for start_position in range(0, last_bit_position):
        end_position = start_position + 2
        suffix_bits  = data[start_position:end_position]
        if (suffix_bits == marker):
            return end_position
    #raise Exception(f"C1 encoded number not found in data (marker '11' occurred 0 times)")
    return None

def get_encoded_c1_bits(data: Union[bitarray, frozenbitarray], endian: str='big') -> int:
    item_length = get_encoded_c1_length(data=data, endian=endian)
    return data[0:item_length]

#@lru_cache(maxsize=2**14)
def fib_encode_position(position: int, encoder_type: str='C1', endian: str='big') -> frozenbitarray:
    # position 0 cannot be encoded
    position = position + 1
    if (encoder_type == 'C1'):
        return frozenbitarray(to_c1_code(position), endian=endian)
    if (encoder_type == 'C2'):
        if (position == 1):
            return frozenbitarray('1', endian=endian)
        c1_code = bitarray(to_c1_code(position-1))
        c2_code = frozenbitarray(bitarray('10') + c1_code[0:len(c1_code)-1])
        return c2_code

@lru_cache(maxsize=2**14)
def fib_decode_position(position_bits: Union[bitarray, frozenbitarray], encoder_type: str='C1', endian: str='big') -> int:
    suffix = position_bits[len(position_bits)-2:len(position_bits)]
    if (encoder_type == 'C1'):
        last_bit_number = len(position_bits) - 1
        assert(suffix == bitarray('11', endian=endian))
    if (encoder_type == 'C2'):
        if (position_bits == bitarray('1', endian=endian)):
            return 0
        position_bits   = position_bits[2:len(position_bits)] + bitarray('1', endian=endian)
        last_bit_number = len(position_bits) - 1
    # decoded position
    position = 0
    # getting sum of all fibonacci numbers with indexes that match to "1" bits
    for bit_number in range(0, last_bit_number):
        fibnum = fib[bit_number]
        if (bool(position_bits[bit_number]) is True):
            position = position + fibnum
        #print(f"{bit_number}:", bool(position_bits[bit_number]), f"fibnum={fibnum}", f"p={position}", f"endian: {position_bits.endian()}")
    # we added 1 at the encoding step - now we need to subtract it back
    if (encoder_type == 'C1'):
        position = position - 1
    if (position == -1):
        raise Exception(f"Incorrect value: position_bits={position_bits}, endian: {position_bits.endian()}")
    return position

def fib_encode_positions(positions: Iterable[int], endian: str='big', encoder_type: str='C1') -> bitarray:
    """
    Преобразовать список целых чисел в битовый массив состоящий из значений разделимого кода Фибоначчи
    Поддерживает два вида кода Фибоначчи: 
    1) C1 (с минимальной длиной значения 2 бита) 
    2) C2 (с минимальной длиной значения 1 бит)
    Поддерживает кодирование нуля путем добавления единицы к значению перед кодированием (и вычитанием после декодирования)
    Порядок бит в значении: big endian (слева направо, старшие биты идут первыми)
    TODO: настраиваемый параметр offset для указания минимального значения
    """
    result = bitarray(endian=endian)
    for position in positions:
        encoded_position = fib_encode_position(position=position, encoder_type=encoder_type, endian=endian)
        result           = result + encoded_position
    if (encoder_type == 'C2'):
        # C2 encoding scheme require adding final "1" bit to avoid special processing of "11" marker in last element
        result = result + bitarray('1', endian=endian)
    return result

def fib_decode_positions(positions_bits: Union[bitarray, frozenbitarray], encoder_type: str='C1') -> Iterable[int]:
    """
    Преобразовать битовый массив состоящий из значений разделимого кода Фибоначчи в список целых чисел 
    Поддерживает два вида кода Фибоначчи: 
    1) C1 (с минимальной длиной значения 2 бита) 
    2) C2 (с минимальной длиной значения 1 бит)
    Поддерживает кодирование нуля путем добавления единицы к значению перед кодированием (и вычитанием после декодирования)
    Порядок бит в значении: big endian (слева направо, старшие биты идут первыми)
    TODO: настраиваемый параметр offset для указания минимального значения
    """
    marker            = bitarray('11')
    start_position    = 0
    stop_position     = 0
    min_position      = 0
    decoded_positions = list()
    while True:
        marker_position = positions_bits.find(marker, min_position)
        if (marker_position == -1):
            break
        start_position = min_position
        if (encoder_type == 'C1'):
            stop_position    = marker_position + 2
            encoded_position = positions_bits[start_position:stop_position]
            decoded_position = fib_decode_position(encoded_position, encoder_type=encoder_type)
        elif (encoder_type == 'C2'):
            stop_position    = marker_position + 1
            encoded_position = positions_bits[start_position:stop_position]
            decoded_position = fib_decode_position(encoded_position, encoder_type=encoder_type)
        decoded_positions.append(decoded_position)
        #print(f"{marker_position}: {start_position}-{stop_position}, {encoded_position}, ({decoded_position}), encoder={encoder_type}")
        if (encoder_type == 'C1'):
            min_position = marker_position + 2
        elif (encoder_type == 'C2'):
            min_position = marker_position + 1
    return decoded_positions

@lru_cache(maxsize=64)
def get_fib_lengths(min_length: int=None, max_position: int=2**21, encoder_type: str='C1', use_cache:bool=True) -> Counter:
    """
    Получить словарь с соотношением количества позиций для каждой длины значения для разных вариантов кода Фибоначчи (C1 и C2)
    """
    if (use_cache is True):
        length_counts = {
            'C1': {
                3: 1,
                4: 1,
                5: 2,
                6: 3,
                7: 5,
                8: 8,
                9: 13,
                10: 21,
                11: 34,
                12: 55,
                13: 89,
                14: 144,
                15: 233,
                16: 377,
                17: 610,
                18: 987,
                19: 1597,
                20: 2584,
                21: 4181,
                22: 6765,
                23: 10946,
                24: 17711,
                25: 28657,
                26: 46368,
                27: 75025,
                28: 121393,
                29: 196418,
                30: 317811,
                31: 514229,
                32: 832040,
                33: 1346269,
                34: 2178309,
                35: 3524578,
                36: 5702887,
                37: 9227465,
                38: 14930352,
                39: 24157817,
                40: 39088169,
                41: 63245986,
                42: 102334155,
                43: 165580141,
                44: 267914296,
                45: 433494437,
                46: 701408733,
                47: 1134903170,
                48: 1836311903,
                49: 2971215073,
                50: 4807526976,
                51: 7778742049,
                52: 12586269025,
                53: 20365011074,
                54: 32951280099,
                55: 53316291173,
                56: 86267571272,
                57: 139583862445,
                58: 225851433717,
                59: 365435296162,
                60: 591286729879,
                61: 956722026041,
                62: 1548008755920,
                63: 2504730781961,
            },
            'C2': {
                2: 1,
                4: 1,
                5: 1,
                6: 2,
                7: 3,
                8: 5,
                9: 8,
                10: 13,
                11: 21,
                12: 34,
                13: 55,
                14: 89,
                15: 144,
                16: 233,
                17: 377,
                18: 610,
                19: 987,
                20: 1597,
                21: 2584,
                22: 4181,
                23: 6765,
                24: 10946,
                25: 17711,
                26: 28657,
                27: 46368,
                28: 75025,
                29: 121393,
                30: 196418,
                31: 317811,
                32: 514229,
                33: 832040,
                34: 1346269,
                35: 2178309,
                36: 3524578,
                37: 5702887,
                38: 9227465,
                39: 14930352,
                40: 24157817,
                41: 39088169,
                42: 63245986,
                43: 102334155,
                44: 165580141,
                45: 267914296,
                46: 433494437,
                47: 701408733,
                48: 1134903170,
            },
        }
        result = length_counts[encoder_type].copy()
        return result
    if (min_length is None):
        min_length = 1
    lengths = Counter()
    for position in track(range(0, max_position)):
        encoded_position = fib_encode_position(position, encoder_type=encoder_type)
        encoded_length   = len(encoded_position)
        value_length     = encoded_length + min_length
        lengths.update({ value_length: 1 })
    return lengths.copy()

@lru_cache(maxsize=64)
def get_max_values_for_position_length(position_length: int, encoder_type: str='C1') -> int:
    length_counts = get_fib_lengths(encoder_type=encoder_type).copy()
    value_length  = position_length + 1
    return length_counts[value_length]

def get_min_value_length_for_position(position: int, score: int=1, encoder_type: str='C1') -> int:
    encoded_position = fib_encode_position(position, encoder_type=encoder_type)
    return len(encoded_position) + score

@lru_cache(maxsize=64)
def get_fib_offset_for_position_length(position_length: int, encoder_type: str='C1') -> int:
    """
    Получить первую позицию диапазона значений указанной длины
    """
    offset           = 0
    length_counts    = get_fib_lengths(encoder_type=encoder_type).copy()
    position_lengths = [(value_length - 1) for value_length in sorted(length_counts.keys())]
    for current_length in position_lengths:
        if (current_length >= position_length):
            return offset
        max_values = get_max_values_for_position_length(position_length=current_length, encoder_type=encoder_type)
        offset     = offset + max_values
    raise Exception(f"Too long value: position_length={position_length}")

@lru_cache(maxsize=2**14)
def get_max_values_for_position(position: int, encoder_type: str='C1') -> int:
    encoded_position = fib_encode_position(position, encoder_type=encoder_type)
    return get_max_values_for_position_length(len(encoded_position))

@lru_cache(maxsize=64)
def get_fib_value_range(value_length: int, encoder_type: str='C1') -> Tuple[int, int]:
    """
    Получить границы диапазона значений кода Фибоначчи для указанной длины
    """
    first_value = get_fib_offset_for_position_length(position_length=value_length, encoder_type=encoder_type)
    last_value  = get_fib_offset_for_position_length(position_length=(value_length + 1), encoder_type=encoder_type)
    return (first_value, last_value)

@lru_cache(maxsize=64)
def get_fib_position_range(position_length: int, encoder_type: str='C1') -> Tuple[int, int]:
    """
    Получить границы диапазона позиций указанной длины
    """
    return get_fib_value_range(value_length=position_length, encoder_type=encoder_type)

def get_fib_position_ranges(encoder_type: str='C1', max_length: int=32) -> List[Tuple[int, int]]:
    ranges    = []
    max_value = 0
    value_lengths = list(get_fib_lengths(encoder_type=encoder_type).keys())
    for value_length_id in range(0, max_length+1):
        value_length         = value_lengths[value_length_id]
        min_value, max_value = get_fib_value_range(value_length=value_length, encoder_type=encoder_type)
        ranges.append((min_value, max_value))
    return ranges

@lru_cache(maxsize=2**20)
def fib_item_id_to_position(item_id: int, encoder_type: str='C1', score: int=1) -> int:
    """
    Получить позицию значения по id значения (с учетом смещения для непересекающихся значений)
    """
    fib_lengths     = get_fib_lengths(encoder_type=encoder_type).copy()
    position        = 0
    current_item_id = 0
    last_length     = False
    for value_length, total_values in fib_lengths.items():
        position_length = (value_length - 1)
        if (current_item_id + total_values >= item_id):
          total_values = item_id - current_item_id
          last_length  = True
        position        += (position_length + score) * total_values
        current_item_id += total_values
        if (last_length is True):
          return position
        #for _ in range(0, total_values):
        #    if (current_item_id == item_id):
        #        return position
        #    position        += position_length + score
        #    current_item_id += 1
    raise Exception(f"Too large item_id={item_id}")

@lru_cache(maxsize=2**20)
def fib_item_id_to_value_length(item_id: int, encoder_type: str='C1', score: int=1) -> int:
    """
    Получить длину значения по id значения
    """
    fib_lengths      = get_fib_lengths(encoder_type=encoder_type).copy()
    position_lengths = [(value_length - 1) for value_length in fib_lengths.keys()]
    for position_length in position_lengths:
        min_id, max_id = get_fib_position_range(position_length=position_length, encoder_type=encoder_type)
        if (item_id >= min_id) and (item_id < max_id):
            value_length = (position_length + score)
            return value_length
    raise Exception(f"Too large item_id={item_id}")

@lru_cache(maxsize=2**20)
def get_fib_non_overlapped_item(item_id: int, offset: int=0, seed: int=0, encoder_type: str='C1', score: int=1) -> frozenbitarray:
    """
    Получить непересекающееся значение из хеш-пространства по его порядковому номеру с указанным смещением 
    относительно нулевого бита хеш-пространства
    """
    position     = offset + fib_item_id_to_position(item_id=item_id, encoder_type=encoder_type, score=score)
    value_length = fib_item_id_to_value_length(item_id=item_id, encoder_type=encoder_type, score=score)
    return bits_at_position(bit_position=position, bit_length=value_length, seed=seed, frozen=True)

@lru_cache(maxsize=64)
def get_total_values_in_fib_range(position_length: int, encoder_type: str='C1') -> int:
    """
    Получить максимально доступное количество уникальных значений кода Фибоначчи 
    в интервале для указанной длины
    """
    start, end = get_fib_position_range(position_length=position_length, encoder_type=encoder_type)
    return (end - start)

@lru_cache(maxsize=64)
def get_fib_item_ids_for_value_length(value_length: int, encoder_type: str='C1', score: int=1) -> Iterable[int]:
    position_length = value_length - score
    min_id, max_id  = get_fib_position_range(position_length=position_length, encoder_type=encoder_type)
    return [item_id for item_id in range(min_id, max_id)]

def get_fib_item_lengths_by_item_ids(item_ids: Iterable[int], encoder_type: str='C1', score: int=1) -> Iterable[int]:
    """
    Получить длину значения для каждого id значения из переданного списка
    """
    return [fib_item_id_to_value_length(item_id=item_id, encoder_type=encoder_type, score=score) for item_id in item_ids]

def get_fib_items_by_item_ids(item_ids: Iterable[int], offset: int=0, seed: int=0, encoder_type: str='C1', score: int=1) -> Iterable[int]:
    """
    Получить список элементов по списку их id
    """
    return [get_fib_non_overlapped_item(item_id=item_id, offset=offset, seed=seed, encoder_type=encoder_type, score=score) for item_id in item_ids]

####################################################

def get_next_fib_for_position(position: int) -> int:
    index = largestFiboLessOrEqual(position)
    return fib[index+1]

def get_prev_fib_for_position(position: int) -> int:
    index = largestFiboLessOrEqual(position)
    return fib[index-1]

def fib_value_id(value: bitarray, position: int, score: int=1) -> int:
    value_length     = len(value)
    position_length  = value_length - score
    return int2ba(position, length=position_length)

def get_fib_prefix_options(prefix_length: int, encoder_type: str='C1') -> Dict[int, Tuple[int]]:
    prefix_options = dict()
    if (encoder_type == 'C1'):
        min_position_length = 2
        min_seed_length     = 2
        min_prefix_length   = (min_seed_length + min_position_length)
    else:
        raise Exception(f"encoder_type={encoder_type} is not implemented")
    if (prefix_length < min_prefix_length):
        raise Exception(f"prefix_length={prefix_length} is too short (min_prefix_length={min_prefix_length}, encoder_type={encoder_type})")
    # определяем максимально возможную длину позиции
    max_seed_length = prefix_length - min_position_length
    for seed_length in range(min_seed_length, (max_seed_length + 1)):
        start_seed, end_seed = get_fib_position_range(seed_length, encoder_type=encoder_type)
        seed_range           = range(start_seed, end_seed)
        position_length      = prefix_length - seed_length
        start_pos, end_pos   = get_fib_position_range(position_length, encoder_type=encoder_type)
        position_range       = (start_pos, end_pos)
        for seed in seed_range:
            prefix_options[seed] = position_range
    return prefix_options

@dataclass
class PositionMapping:
    min_open_length       : int
    open_lenghts          : Set[int]                        # {value_length1, value_length2, ...}
    closed_lenghts        : Set[int]                        # {value_length1, value_length2, ...}
    value_lenghts         : Set[int]                        # {value_length1, value_length2, ...}
    position_counts       : Counter                         # position->total_mentions (across all seeds)
    open_position_counts  : Counter                         # value_length->remaining_positions_count
    open_seeds            : Dict[int, Counter]              # value_length->seed->remaining_positions_count
    length_values         : Dict[int, Set[frozenbitarray]]  # value_length->{v1, v2, ...}
    length_positions      : Dict[int, Set[int]]             # value_length->{pos1, pos2, ...}
    length_seeds          : Dict[int, Set[int]]             # value_length->{seed1, seed2, ...}
    length_seed_positions : Dict[int, Dict[int, Set[int]]]  # value_length->seed->{pos1, pos2, ...}
    value_seeds           : Dict[frozenbitarray, int]       # value->seed
    value_positions       : Dict[frozenbitarray, int]       # value->position
    seed_positions        : Dict[int, Set[int]]             # seed->{pos1, pos2, ...}
    seed_value_lengths    : Dict[int, Set[int]]             # seed->{vl1, vl2, ...}
    score                 : int = 1
    encoder_type          : str = 'C1'

def init_position_mapping(score: int=1, encoder_type: str='C1') -> PositionMapping:
    return PositionMapping(
        min_open_length       = None,
        open_lenghts          = set(),                                  # {value_length1, value_length2, ...}
        closed_lenghts        = set(),                                  # {value_length1, value_length2, ...}
        value_lenghts         = set(),                                  # {value_length1, value_length2, ...}
        position_counts       = Counter(),                              # position->total_mentions (across all seeds)
        open_position_counts  = Counter(),                              # value_length->remaining_positions_count
        open_seeds            = defaultdict(Counter),                   # value_length->seed->remaining_positions_count
        length_values         = defaultdict(set),                       # value_length->{v1, v2, ...}
        length_positions      = defaultdict(set),                       # value_length->{pos1, pos2, ...}
        length_seeds          = defaultdict(set),                       # value_length->{seed1, seed2, ...}
        length_seed_positions = defaultdict(lambda: defaultdict(set)),  # value_length->seed->{pos1, pos2, ...}
        value_seeds           = dict(),                                 # value-seed
        value_positions       = dict(),                                 # value->position
        seed_positions        = defaultdict(set),                       # seed->{pos1, pos2, ...}
        seed_value_lengths    = defaultdict(set),                       # seed->{vl1, vl2, ...}
        score                 = score,
        encoder_type          = encoder_type,
    )

def init_value_length_mapping(value_length: int, mapping: PositionMapping) -> PositionMapping:
    return mapping

def find_priority_position(value: frozenbitarray, mapping: PositionMapping, seed: int, priority_positions: List[int]) -> Union[int, None]:
    pass

def find_seed_position(value: frozenbitarray, mapping: PositionMapping, seed: int, priority_positions: List[int]) -> Union[int, None]:
    pass

def update_position_mapping_with_value(value: frozenbitarray, mapping: PositionMapping) -> PositionMapping:
    value_length           = len(value)
    prefix_length          = (value_length - mapping.score)
    length_options         = get_fib_prefix_options(prefix_length=prefix_length, encoder_type=mapping.encoder_type)
    length_positions_count = sum([len(range(start, end)) for start, end in length_options.values()])
    length_seeds           = sorted(length_options.keys())
    length_open_seeds      = dict()
    priority_positions     = [position_item[0] for position_item in mapping.position_counts.most_common()]
    
    for length_seed in length_seeds:
        length_seed_positions_count    = len(range(length_options[length_seed][0], length_options[length_seed][1]))
        length_open_seeds[length_seed] = Counter({length_seed: length_seed_positions_count})

    if (value_length not in mapping.value_lenghts):
        mapping.value_lenghts.add(value_length)
        mapping.open_lenghts.add(value_length)
        mapping.open_position_counts[value_length] = length_positions_count
        mapping.length_values[value_length]        = set()
        mapping.length_positions[value_length]     = defaultdict(set)
        mapping.length_seeds[value_length]         = set(length_seeds)
        mapping.open_seeds[value_length]           = length_open_seeds
        mapping.length_positions[value_length]
    
    if (mapping.min_open_length is None):
        mapping.min_open_length = min(mapping.open_lenghts)
    
    if (value_length in mapping.closed_lenghts) or (mapping.open_position_counts[value_length] == 0):
        raise Exception(f"No open positions for value_length={value_length}, value={value}")
    
    # find value in open positions
    target_seeds = sorted(set(mapping.open_seeds[value_length].keys()))
    #print(target_seeds)
    for seed in target_seeds:
        if (mapping.open_seeds[value_length][seed] == 0):
            #del mapping.open_seeds[value_length][seed]
            continue
        has_prev_value = False
        position       = None
        # 1) check used positions first
        for priority_position in priority_positions:
            has_prev_value = False
            if (priority_position in mapping.seed_positions[seed]):
                continue
            # check previous values/prefixes
            for prev_length in mapping.value_lenghts:
                if (prev_length >= value_length):
                    break
                prev_value = bits_at_position(bit_position=priority_position, bit_length=prev_length, seed=seed, frozen=True)
                if (prev_value not in mapping.length_values[prev_length]):
                    has_prev_value = True
                    break
            if (has_prev_value is True):
                continue
            # check target value
            hash_value = bits_at_position(bit_position=priority_position, bit_length=value_length, seed=seed, frozen=True)
            if (hash_value != value):
                continue
            position = priority_position
            print(f"p={position}: hv={hash_value}, v={value} (priority)")
            break
        # 2) scan new positions only when all used positions are checked
        if (position is None):
            position = 0
            while (True):
                has_prev_value = False
                if (position in priority_positions):
                    position += 1
                    continue
                if (position in mapping.seed_positions[seed]):
                    position += 1
                    continue
                # check previous values/prefixes
                for prev_length in mapping.value_lenghts:
                    if (prev_length >= value_length):
                        break
                    prev_value = bits_at_position(bit_position=position, bit_length=prev_length, seed=seed, frozen=True)
                    if (prev_value not in mapping.length_values[prev_length]):
                        has_prev_value = True
                        break
                if (has_prev_value is True):
                    position += 1
                    continue
                # check target value
                hash_value = bits_at_position(bit_position=position, bit_length=value_length, seed=seed, frozen=True)
                if (hash_value != value):
                    position += 1
                    continue
                print(f"p={position}: hv={hash_value}, v={value} (new)")
                break
            # position found, update mapping with new value
            if (position is not None):
                mapping.value_positions[value] = position
                mapping.value_seeds[value]     = seed
                mapping.seed_positions[seed].add(position)
        # TODO: new seed position    
    return mapping

def init_fib_value_positions(min_prefix_length: int, max_prefix_length: int, score: int=1, encoder_type: str='C1') -> Dict[int, Dict[int, Set[int]]]:
    result = defaultdict(dict)
    if (encoder_type == 'C1'):
        if (min_prefix_length is None):
            min_prefix_length = 4
    else:
        raise Exception(f"encoder_type={encoder_type} is not implemented")
    # получаем все возможные комбинации диапазонов seed + position для указанной длины
    for prefix_length in range(min_prefix_length, (max_prefix_length + 1)):
        value_length           = prefix_length + score
        prefix_options         = get_fib_prefix_options(prefix_length=prefix_length, encoder_type=encoder_type)
        value_length_seeds     = sorted(prefix_options.keys())
        for seed in value_length_seeds:
            start_pos, end_pos         = prefix_options[seed]
            seed_positions             = range(start_pos, end_pos) #set([p for p in range(start_pos, end_pos)])
            result[value_length][seed] = seed_positions  
    return result

@dataclass
class SeedMapping:
    value_length   : int
    prev_values    : Dict[int, Set[frozenbitarray]]
    current_values : Set[frozenbitarray]
    target_seeds   : Dict[int, int]
    score          : int = 1
    encoder_type   : str = 'C1'

def get_fib_seeds_for_value_length(value_length: int, prev_values: Dict[int, Set[frozenbitarray]], score: int=1, encoder_type: str='C1') -> SeedMapping:
    target_seeds     = dict()
    prefix_length    = value_length - score
    prefix_options   = get_fib_prefix_options(prefix_length=prefix_length, encoder_type=encoder_type)
    current_values   = set()
    seed_ids         = sorted(prefix_options.keys())
    prev_lengths     = sorted(prev_values.keys())
    max_seed         = 2**16
    max_position     = 2**24
    min_duplicates   = dict()
    best_seeds       = dict()
    best_seed_values = defaultdict(set)
    progress         = tqdm(total=max_seed, smoothing=0, miniters=500, mininterval=0.5)

    #print(f"vl={value_length}, prev_lengths={prev_lengths}", prev_values)
    for seed_id in seed_ids:
        seed                    = 0
        start_pos, end_pos      = prefix_options[seed_id]
        positions               = range(start_pos, end_pos)
        min_duplicates[seed_id] = None
        best_seeds[seed_id]     = None
        while (True):
            has_duplicates  = False
            duplicate_count = 0
            seed_values     = set()
            for position in positions:
                for prev_length in prev_lengths:
                    prev_value = bits_at_position(bit_position=position, bit_length=prev_length, seed=seed, frozen=True)
                    if (prev_value in prev_values[prev_length]):
                        has_duplicates = True
                        duplicate_count += 1
                        break
                seed_value = bits_at_position(bit_position=position, bit_length=value_length, seed=seed, frozen=True)
                if (seed_value in seed_values) or (seed_value in current_values):
                    has_duplicates = True
                    duplicate_count += 1
                else:
                    seed_values.add(seed_value)    
                if (has_duplicates is True) and (min_duplicates[seed_id] is not None) and (duplicate_count >= min_duplicates[seed_id]):
                    break
                #    break
                    #if (duplicate_count > min_duplicates[seed_id]) and (min_duplicates[seed_id] is not None):
                    #    break
            if (has_duplicates is False):
                target_seeds[seed_id]     = seed
                best_seeds[seed_id]       = seed
                min_duplicates[seed_id]   = duplicate_count
                best_seed_values[seed_id] = seed_values
                for bs_value in best_seed_values[seed_id]:
                    current_values.add(bs_value)
                #print(f"{seed_id}: seed={seed}, {target_seeds}")
                progress.reset()
                break
            if (min_duplicates[seed_id] is None) or (duplicate_count < min_duplicates[seed_id]):
                if (len(positions) >= 34):
                    print(f"({value_length}) {seed_id}/{len(seed_ids)}: positions={len(positions)} duplicate_count={duplicate_count}, seed={seed}")
                min_duplicates[seed_id]   = duplicate_count
                best_seeds[seed_id]       = seed
                best_seed_values[seed_id] = seed_values
            if (seed >= max_seed):
                target_seeds[seed_id] = best_seeds[seed_id]
                for bs_value in best_seed_values[seed_id]:
                    current_values.add(bs_value)
                print(f"({value_length}) {seed_id}/{len(seed_ids)} [TIMEOUT={seed}]: seed={target_seeds[seed_id]} duplicates={min_duplicates[seed_id]}")
                progress.reset()
                break
            seed += 1
            progress.update(1)
    assert(len(target_seeds) == len(seed_ids))
    return SeedMapping(
        value_length   = value_length,
        prev_values    = prev_values.copy(),
        current_values = current_values,
        target_seeds   = target_seeds,
        score          = score,
        encoder_type   = encoder_type,
    )

@dataclass
class FibSplit:
    item_limits     : Dict[int, int]
    split_values    : List[bitarray]
    split_positions : List[bitarray]
    dict_items      : Dict[int, Dict[bitarray, bitarray]]
    value_counts    : Counter
    length_counts   : Counter

def fib_split_data(data: bitarray, encoder_type: str='C1') -> FibSplit:
    item_limits         = get_fib_lengths(encoder_type=encoder_type).copy()
    # unique items used in data split, grouped by item length: dict keys are data items, values are fibonacci C2 codes
    dict_items          = defaultdict(dict)
    value_lengths       = sorted(item_limits.keys())
    min_length          = min(value_lengths)
    max_length          = max(value_lengths)
    start_data_position = 0
    end_data_position   = 0
    split_values        = list()
    split_positions     = list()
    value_counts        = Counter()
    value_length        = min_length
    length_counts       = Counter()

    print(f"{min_length}, {max_length}, {value_lengths}")

    n   = 0
    bar = tqdm(range(0, len(data)), desc="Splitting...", mininterval=0.5)
    while (end_data_position <= len(data)):
        n += 1
        bar.update(value_length)
        for value_length in value_lengths:
            start_data_position = end_data_position
            data_value          = data[start_data_position:start_data_position+value_length]
            dict_item_position  = len(value_counts.keys())
            length_values       = dict_items[value_length]
            encoded_position    = None
            
            if (len(data_value) < value_length):
                break
            if data_value not in length_values:
                if (item_limits[value_length] == 0):
                    continue
                encoded_position                     = fib_encode_position(dict_item_position, encoder_type=encoder_type)
                dict_items[value_length][data_value] = encoded_position
                item_limits[value_length]            = item_limits[value_length] - 1
                length_counts.update({ value_length: 1 })
                #print(f"{n}, l={value_length}, ({start_data_position}-{start_data_position+value_length}): p={encoded_position}={dict_item_position}:{decoded_position}, pl={len(encoded_position)}, v={data_value}, vl={len(data_value)} (NEW)")
            else:
                encoded_position = length_values[data_value]
                #print(f"{n}, l={value_length}, ({start_data_position}-{start_data_position+value_length}): p={encoded_position}={dict_item_position}:{decoded_position}, pl={len(encoded_position)}, v={data_value}, vl={len(data_value)} (CACHE)")
            
            split_values.append(data_value)
            split_positions.append(encoded_position)
            value_counts.update({ data_value: 1 })
            end_data_position = start_data_position + value_length
            break
        if (start_data_position + value_length) >= len(data):
            break
    bar.close()

    return FibSplit(
        item_limits     = item_limits.copy(),
        split_values    = split_values.copy(),
        split_positions = split_positions.copy(),
        dict_items      = dict_items.copy(),
        value_counts    = value_counts.copy(),
        length_counts   = length_counts.copy()
    )