from __future__ import annotations
#from rich import print
#from rich.pretty import pprint
#from tqdm.notebook import tqdm
from custom_counter import CustomCounter as Counter
from collections.abc import Iterable
from bitarray import bitarray #, frozenbitarray
from bitarray.util import canonical_huffman # ba2int, int2ba, huffman_code, zeros, intervals, 
from sortedcontainers import SortedSet
from typing import List, Set, Dict, Tuple, Optional, Union, Iterable
from dataclasses import dataclass, field

@dataclass()
class CanonicalHuffmanDict:
    """All code bitarrays have big endian by default"""
    values        : List[int]|Iterable[int] = field()
    value_codes   : Dict[int, bitarray]     = field(init=False, default=None)
    id_codes      : Dict[int, bitarray]     = field(init=False, default=None)
    min_length    : int                     = field(init=None, default=None)
    max_length    : int                     = field(init=None, default=None)
    length_counts : Counter                 = field(init=None, default=None)
    last_min_id   : int                     = field(init=None, default=None)
    length        : int                     = field(init=None, default=None)

    def __init__(self, values: List[int]|Iterable[int]):
        frequencies = dict()
        self.values = sorted(list(values))
        self.length = len(self.values)
        if (self.length != len(SortedSet(self.values))):
            raise Exception(f"Value list must be unique")
        self.value_codes   = dict()
        self.id_codes      = dict()
        self.length_counts = Counter()
        
        for i in range(0, self.length):
            frequencies[i] = 1
        canonical_codes = list(canonical_huffman(frequencies)[0].values())

        if (len(canonical_codes) != self.length):
            raise Exception(f"Canonical codes dict length ({len(canonical_codes)}) don't match value list length={self.length}")
        self.min_length = len(canonical_codes[0])
        self.max_length = len(canonical_codes[self.length-1])
        
        for item_id in range(0, self.length):
            value       = self.values[item_id]
            code        = canonical_codes[item_id]
            code_length = len(code)
            if (code_length == self.min_length):
                self.last_min_id = item_id
            self.length_counts.update({ code_length: 1 })
            self.value_codes[value] = code.copy()
            self.id_codes[item_id]  = code.copy()
    
    def get_value_id(self, value: int) -> int:
        return self.values.index(value)
    
    def get_value_id_code(self, value_id: int) -> bitarray:
        if (value_id not in self.id_codes):
            raise Exception(f"value_id={value_id} not found: cannot find code for it")
        return self.id_codes[value_id]
    
    def get_code_value_id(self, code: bitarray) -> int:
        for value_id, value_code in self.id_codes.items():
            if (code == value_code):
                return value_id
        raise Exception(f"code={code} not found: cannot find value_id for it")
    
    def get_code_value(self, code: bitarray) -> int:
        for value, value_code in self.value_codes.items():
            if (code == value_code):
                return value
        raise Exception(f"code={code} not found: cannot find value for it")
    
    def get_value_code(self, value: int) -> bitarray:
        value_id = self.get_value_id(value=value)
        return self.get_value_id_code(value_id=value_id)

def create_canonical_codes(values: Iterable) -> CanonicalHuffmanDict:
    return CanonicalHuffmanDict(values=values)