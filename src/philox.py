from rich import print
from rich.pretty import pprint

import tensorflow as tf

# Creates some virtual devices (cpu:0, cpu:1, etc.) for using distribution strategy
"""
physical_devices = tf.config.list_physical_devices("CPU")
tf.config.experimental.set_virtual_device_configuration(
    physical_devices[0], [
        tf.config.experimental.VirtualDeviceConfiguration(),
        tf.config.experimental.VirtualDeviceConfiguration(),
        tf.config.experimental.VirtualDeviceConfiguration()
    ])
"""

g = tf.random.Generator.from_seed(seed=1, alg="philox")

print(f"\ng.key: {g.key}, g.state: {g.state}")
print(f"g.key: {g.key}, g.state: {g.state}")

gg   = tf.random.get_global_generator()
tf.random.set_seed(1)
nums = gg.from_key_counter(key=1, counter=range(0, 2), alg="philox")

print(f"\ngg.key: {gg.key}, gg.state: {gg.state}")
print(f"nums: {nums.__dict__}")

nums = gg.uniform([0, 2],
    minval=0,
    maxval=100,
    dtype=tf.dtypes.float32,
)
print(f"\ngg.key: {gg.key}, gg.state: {gg.state}")
print(f"nums: {nums}")
#(key=1, counter=range(0, 2), alg="philox")



#nums = gg.from_key_counter (shape=[2, 2], minval=0, maxval=100, dtype=tf.dtypes.float32)

#print(f"nums:")
#pprint(nums)
