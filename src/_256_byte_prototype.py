from __future__ import annotations
from rich import print
from rich.pretty import pprint
from rich.text import Text
from rich.progress import track,\
    BarColumn, Progress, Task, TaskID, TextColumn, TimeElapsedColumn, TimeRemainingColumn,\
    MofNCompleteColumn, RenderableColumn, SpinnerColumn, TransferSpeedColumn, FileSizeColumn, ProgressColumn
from rich.layout import Layout
from rich.columns import Columns
from rich.text import Text
from custom_rich import CustomTaskProgressColumn as TaskProgressColumn
from custom_counter import init_byte_counter, CustomCounter as Counter
from collections import defaultdict, namedtuple
from bitarray import bitarray, frozenbitarray
from bitarray.util import ba2int, int2ba, canonical_huffman, huffman_code, zeros,\
    vl_encode, vl_decode, sc_encode, sc_decode, serialize, deserialize
from sortedcontainers import SortedSet, SortedList, SortedKeyList, SortedListWithKey,\
    SortedKeysView, SortedValuesView, SortedItemsView
from typing import List, Set, Dict, Tuple, Optional, Union, Iterable
from dataclasses import dataclass, field
from enum import Enum, IntEnum
from copy import deepcopy, copy
import operator
from delta_of_delta import delta_encode, delta_decode
from itertools import accumulate
from more_itertools import nth, countable
import math

from hash_range_iterator import DEFAULT_ENDIAN, HashPositionBitmap, \
    int_from_nounce, int_bits_from_nounce, last_int_bits_from_nounce,\
    last_ba_bits_from_nounce, last_ba_bits_from_digest, last_fba_bits_from_digest, last_int_bits_from_digest,\
    split_data, count_data_items, create_value_bitmap, collect_missing_positions, delta_to_list, \
    get_target_position_bitmap, encode_position_bitmap, decode_position_bitmap
from cycle_gen import CMWC

# all data processed as 256-byte (2048-bit) blocks
DEFAULT_BLOCK_LENGTH  = 256
DEFAULT_BLOCK_SEED    = 1
#DEFAULT_SEED_INTERVAL = 2**8

class CycleType(int, Enum):
    """Cycles can include or exclude byte values into/from block"""
    EXCLUSIVE : int = 0
    INCLUSIVE : int = 1

    def __str__(self):
        return f'{self.name}({self.value})'
    
    def __repr__(self):
        return f'{self.name}({self.value})'

class MappingMode(int, Enum):
    # all positions except given
    EXCLUDE : int = 0
    # only given positions
    INCLUDE : int = 1

    def __str__(self):
        return f'{self.name}({self.value})'
    
    def __repr__(self):
        return f'{self.name}({self.value})'

class MappingDirection(int, Enum):
    # mapping starts from left to right (from min to max)
    ASC : int = 0
    # reversed mapping direction
    DESC : int = 1

    def __str__(self):
        return f'{self.name}({self.value})'
    
    def __repr__(self):
        return f'{self.name}({self.value})'

@dataclass
class ValuePath:
    cycle_id   : int
    bit_length : int
    distance   : int

@dataclass
class IsolatedCycle:
    id                 : int             = field(init=False, default=None)
    # position-value pairs, started from smallest id
    items              : Dict[int, int]  = field()
    item_counts        : Counter         = field()
    # full_length, start_id - for total ordering cycles inside target set
    sort_id            : Tuple[int, int] = field(init=False)
    # total items in cycle at creation, immutable
    length             : int             = field(default=None, init=False)
    start_id           : int             = field(default=None, init=False)
    start_value        : int             = field(default=None, init=False)
    # all values from initial mapping: modifies every time when value collected from the cycle
    # all collected values moved to "excluded values"
    included_values    : SortedSet[int]  = field(default_factory=SortedSet, init=False)
    excluded_values    : SortedSet[int]  = field(default_factory=SortedSet, init=False)
    included_ids       : SortedSet[int]  = field(default_factory=SortedSet, init=False)
    excluded_ids       : SortedSet[int]  = field(default_factory=SortedSet, init=False)
    #next_lengths       : List[int]       = field(default_factory=list, init=False)
    #next_length_counts : Counter         = field(default_factory=Counter, init=False)
    
    def __init__(self, items: Dict[int, int], item_counts: Counter):
        if (len(item_counts) == 0):
            raise Exception(f"Cycle must have at least 1 item, 0 given: items={items} ({len(items)})")
        if (len(item_counts) != len(items)):
            raise Exception(f"Cycle: items and item_counts does not match: items={items} ({len(items)}),\n item_counts={item_counts} ({len(item_counts)})")
        self.items              = items.copy()
        self.item_counts        = Counter()
        for item_key, item_value in self.items.items():
            self.item_counts.update({ item_value: item_counts[item_value] })
        self.length             = len(self.items)
        self.start_id           = min(list(self.items.keys()))
        self.start_value        = self.items[self.start_id]
        self.sort_id            = (self.length, -self.start_id)
        self.included_values    = SortedSet(list(self.items.values()))
        self.excluded_values    = SortedSet()
        self.included_ids       = SortedSet(list(self.items.keys()))
        self.excluded_ids       = SortedSet()
        #self.next_lengths       = self.get_next_item_lengths()
        #self.next_length_counts = Counter(self.next_lengths)
    
    def set_cycle_id(self, cycle_id: int):
        self.id = cycle_id
    
    def get_item_value_id(self, item_value: int) -> int:
        if (self.has_item_value(item_value=item_value) is False):
            raise Exception(f"item_value={item_value} does not exists in this cycle: {self.items}")
        for key, value in self.items.items():
            if (value == item_value):
                return key
        raise Exception(f"item_value={item_value} id not found")
    
    def exclude_value(self, value: int):
        if (value in self.excluded_values):
            raise Exception(f"value={value} already excluded: \nincluded={self.included_values}, \nexcluded={self.excluded_values}, items={self.items}")
        if (value not in self.included_values):
            raise Exception(f"value={value} is not included, nothing to exclude: \nincluded={self.included_values}, \nexcluded={self.excluded_values}, items={self.items}")
        
        self.item_counts.update({ value: -1 })
        #print(f"cid={self.id}, v={value}, ({len(self.item_counts)}) ic={self.item_counts.first_items()}")
        if (self.item_counts[value] > 0):
            return
        else:
            del self.item_counts[value]

        value_id = self.get_item_value_id(item_value=value)
        self.included_ids.remove(value=value_id)
        self.excluded_ids.add(value=value_id)
        
        self.included_values.remove(value=value)
        self.excluded_values.add(value=value)

        #value_length = self.get_next_item_length()
        #self.next_lengths.remove(value_length)
        #self.next_length_counts.update({ value_length: -1 })
    
    def has_item_id(self, item_id: int) -> bool:
        if (item_id not in self.included_ids):
            return False
        item_value = self.items[item_id]
        if (item_value in self.excluded_values):
            return False
        return True
    
    def has_item_value(self, item_value: int) -> bool:
        if (item_value not in SortedSet(self.items.values())):
            return False
        if (item_value in self.excluded_values):
            return False
        return True
    
    def get_next_item_length(self) -> int:
        return (len(self.item_counts.keys())-1).bit_length()
    
    #def get_next_item_lengths(self) -> List[int]:
    #    item_lengths = list()
    #    #while
    #    for max_path in range(len(self.included_values)-1, -1, -1):
    #        item_lengths.append(max_path.bit_length())
    #    return item_lengths
    
    def find_start_item(self) -> Tuple[int, int]:
        start_id    = self.start_id
        start_value = self.items[start_id]
        for _ in range(0, len(self.items)):
            if (start_value not in self.excluded_values):
                return (start_id, start_value)
            start_id    = start_value
            start_value = self.items[start_id]
        raise Exception(f"Cannot find start item: cycle_id={self.id} is empty: \nitems={self.items}, \nexc={self.excluded_values}")
    
    def get_value_path(self, item_value: int) -> ValuePath:
        """
        Получить путь к указанному значению внутри этого цикла. Путь указывается как расстояние 
        от изначальной точки (start_id, start_value) до элемента содержащего переданное число (item_value).
        Этот метод всегда возвращает результат если цикл содержит переданное значение.
        """
        if (self.has_item_value(item_value=item_value) is False):
            raise Exception(f"item_value={item_value} not found inside cycle_id={self.id}, \nitems={self.items}")
        
        start_item       = self.find_start_item()
        cycle_item_id    = start_item[0]
        cycle_item_value = start_item[1]
        distance         = 0

        for i in range(0, self.length):
            if (cycle_item_value == item_value):
                return ValuePath(cycle_id=self.id, bit_length=self.get_next_item_length(), distance=distance)
            cycle_item_id    = cycle_item_value
            cycle_item_value = self.items[cycle_item_id]
            # пропускаем ранее полученные значения чтобы каждый следующий путь был короче и требовал меньше бит
            if (cycle_item_value in self.excluded_values):
                continue
            else:
                distance += 1
        raise Exception(f"Cannot find item_value={item_value}: \ncycle_id={self.id} does not contain this value: \nitems={self.items}, \nexc={self.excluded_values}")

    def read_value_path(self, item_path: ValuePath) -> int:
        start_item       = self.find_start_item()
        cycle_item_id    = start_item[0]
        cycle_item_value = start_item[1]
        distance         = 0

        for i in range(0, self.length):
            if (item_path.distance == distance):
                return cycle_item_value
            cycle_item_id    = cycle_item_value
            cycle_item_value = self.items[cycle_item_id]
            # пропускаем ранее полученные значения чтобы каждый следующий путь был короче и требовал меньше бит
            if (cycle_item_value in self.excluded_values):
                continue
            else:
                distance += 1
        raise Exception(f"Cannot find item_path={item_path}: cycle_id={self.id} does not contain this value: \nitems={self.items}, \nexc={self.excluded_values}")
    
    def has_path_over_values(self, allowed_values: Set[int], min_length: int) -> bool:
        if (min_length > self.length):
            min_length = self.length
        start_item       = self.find_start_item()
        cycle_item_id    = start_item[0]
        cycle_item_value = start_item[1]

        for i in range(0, min_length):
            if (cycle_item_value not in allowed_values):
                return False
            cycle_item_id    = cycle_item_value
            cycle_item_value = self.items[cycle_item_id]
        return True
    
    def calculate_cycle_score(self, included_values: Set[int], excluded_values: Set[int]) -> int:
        included_count = 0
        excluded_count = 0
        default_type   = None
        pass

########################
### PermutationBlock ###
########################

@dataclass
class PermutationBlock:
    id                  : int                      = field(default=None)
    seed                : int                      = field(default=DEFAULT_BLOCK_SEED)
    items               : Dict[int]                = field(default_factory=dict, init=False, repr=False)
    target_values       : Counter                  = field(default=None)
    block_length        : int                      = field(default=DEFAULT_BLOCK_LENGTH, init=False)
    generator           : CMWC                     = field(default=None, init=False, repr=False)
    cycles              : Dict[int, IsolatedCycle] = field(default=None, init=False)
    exclude_values      : bool                     = field(default=True)

    def __init__(self, id: int=None, seed: int=DEFAULT_BLOCK_SEED, target_values: Counter=None):
        if (target_values is None) or (len(target_values) == 0):
            target_values = Counter([x for x in range(256)])
        self.id            = id
        self.seed          = seed
        self.generator     = CMWC(x=self.seed)
        self.target_values = target_values.copy()
        self.block_length  = len(self.target_values)
        self.items         = self.generate_items().copy()
        self.cycles        = self.extract_cycles().copy()
    
    #def __post_init__(self):

    def get_target_keys(self) -> List[int]:
        return list(sorted(list(self.target_values.copy().keys())))

    def generate_items(self, seed: int=None) -> List[int]:
        if (seed is not None):
            self.seed = seed
            self.generator.seed(seed=self.seed)
        items  = dict()
        keys   = self.get_target_keys()
        values = self.generator.sample(keys, len(keys))
        #assert(len(keys) == len(values))

        for i in range(0, len(values)):
            key   = keys[i]
            value = values[i]
            items[key] = value
        return items
    
    def reload_seed(self, seed: int, target_values: Counter=None):
        """Clean and re-create all block contents with provided seed: old block data will be erased"""
        if (target_values is not None):
            self.target_values.clear()
            self.target_values = target_values.copy()
            self.block_length  = len(self.target_values)
        self.items.clear()
        self.items  = self.generate_items(seed=seed).copy()
        self.cycles.clear()
        self.cycles = self.extract_cycles().copy()

    def extract_cycles(self) -> Dict[int, IsolatedCycle]:
        cycles          = dict()
        cycle_sort_ids  = SortedSet()
        excluded_values = SortedSet()
        
        while (True):
            if (len(excluded_values) == len(self.target_values)):
                break
            cycle_items  = dict()
            cycle_keys   = self.get_target_keys()
            cycle_counts = Counter()
            for index in cycle_keys: #range(0, len(self.target_values)):
                item_value = self.items[index]
                if (item_value not in excluded_values):
                    break
            while True:
                if (index not in self.items):
                    print(index, self.items)
                    print(self.target_values)
                    raise(f"Cannot extract cycles: index={index} not found")
                item_value         = self.items[index]
                cycle_items[index] = item_value
                cycle_counts.update({ item_value: self.target_values[item_value] })
                excluded_values.add(item_value)
                # check link between first and last element of the cycle
                next_index = item_value
                if (next_index in cycle_items.keys()):
                    break
                index = next_index
            # init cycle with dict mapping
            cycle = IsolatedCycle(items=cycle_items, item_counts=cycle_counts.copy())
            cycle_sort_ids.add(cycle.sort_id)
            cycles[cycle.sort_id] = cycle
        # sort cycles starting from longer to shorter
        cycle_id      = 0
        sorted_cycles = dict()
        for cycle_sort_id in reversed(cycle_sort_ids):
            sorted_cycles[cycle_id] = cycles[cycle_sort_id]
            sorted_cycles[cycle_id].set_cycle_id(cycle_id=cycle_id)
            cycle_id += 1
        return sorted_cycles
    
    def find_cycle_containing_value(self, item_value: int) -> IsolatedCycle | None:
        for cycle_id, cycle in self.cycles.items():
            if (cycle.has_item_value(item_value=item_value)):
                return cycle
        return None
    
    def get_value_path(self, item_value: int) -> ValuePath | None:
        #if (data_value in excluded_values):
        #    raise Exception(f"item_value={data_value} found in excluded_values={excluded_values}")
        cycle = self.find_cycle_containing_value(item_value=item_value)
        if (cycle is None):
            return None
        cycle_path = cycle.get_value_path(item_value=item_value)
        if (self.exclude_values):
            self.target_values.update({ item_value: -1 })
            cycle.exclude_value(value=item_value)
            if (self.target_values[item_value] == 0):
                del self.target_values[item_value]
        
        return cycle_path
    
    def read_value_path(self, item_path: ValuePath) -> int:
        cycle      = self.cycles[item_path.cycle_id]
        item_value = cycle.read_value_path(item_path=item_path)
        if (self.exclude_values):
            self.target_values.update({ item_value: -1 })
            cycle.exclude_value(value=item_value)
            if (self.target_values[item_value] == 0):
                del self.target_values[item_value]
        return item_value
    
    def get_cycle_counts(self) -> Counter:
        cycle_counts = Counter()
        for _, cycle in self.cycles.items():
            cycle_counts.update({ cycle.id: cycle.length })
        return cycle_counts
    
    def restore_data_block(self, seed: int, item_paths: Dict[int, ValuePath], target_values: Counter) -> List[int]:
        self.reload_seed(seed=seed, target_values=target_values)
        items = list()
        for item_path in item_paths:
            #print(f"{item_path}")
            item = self.read_value_path(item_path=item_path)
            items.append(item)
        return items

@dataclass
class EncodedOrderedSet:
    items          : List[int]             = field(default_factory=list)
    encoded_items  : List[Tuple[int, int]] = field(default_factory=list)
    value_lengths  : List[int]             = field(default_factory=list)
    encoded_length : int                   = field(default=0, init=False)

def encode_item_order(items: List[int]) -> EncodedOrderedSet:
    encoded_set       = EncodedOrderedSet()
    encoded_set.items = items.copy()
    remaining_items   = set(items.copy())
    #encoded_items   = list()
    for item in items:
        item_length  = (len(remaining_items)-1).bit_length()
        encoded_item = (item_length, item)
        encoded_set.encoded_items.append(encoded_item)
        encoded_set.value_lengths.append(item_length)
        remaining_items.remove(item)
        encoded_set.encoded_length += item_length
    return encoded_set

@dataclass
class SequenceItem:
    position   : int
    item_value : int
    bit_length : int

@dataclass
class EncodedItemSequence:
    items           : List[int]      = field()
    item_counts     : Counter        = field()
    start_positions : Dict[int, int] = field()
    encoded_length  : int            = field(default=0, init=False)
    length_counts   : Counter        = field(default_factory=Counter, init=False)
    encoded_items   : List[SequenceItem] = field(default_factory=list)

    def __init__(self, items: List[int], item_counts: Counter, start_positions: Dict[int, int]):
        self.items           = items.copy()
        self.item_counts     = item_counts.copy()
        self.start_positions = start_positions.copy()
        self.length_counts   = Counter()
        self.encoded_items   = list()
    
    def encode_items(self) -> List[SequenceItem]:
        score             = 0
        positions_bitmap  = get_target_position_bitmap(positions=self.start_positions) #, length_offset=4)
        encoded_positions = encode_position_bitmap(bitmap=positions_bitmap)
        score            += len(encoded_positions)

        start_items         = list(self.start_positions.values())
        encoded_start_items = encode_item_order(items=start_items)
        score              += encoded_start_items.encoded_length

        print(f"positions={self.start_positions}, l={len(self.start_positions)}, enc_p={encoded_positions}, l={len(encoded_positions)}", positions_bitmap)
        print(f"cycle_ids={start_items}, l={len(start_items)}, enc_c={encoded_start_items}")
        print(f"score={score}")

        active_items  = SortedSet()
        encoded_items = list()

        for position in range(len(self.items)):
            #skip_encoding   = False
            position_counts = Counter()
            
            if (position in self.start_positions):
                item_value = self.start_positions[position]
                if(item_value != self.items[position]):
                    raise Exception(f"item_value={item_value} != self.items [ position ] = {self.items[position]}")
                #skip_encoding = True
                active_items.add(item_value)
                item_length = 0
                #print(f"{position}: score={score:4}, new item={item_value} ({item_length}), active_items={active_items} ({len(active_items)}), start_position value={self.start_positions[position]} removed")
                #print(f"{position}: start_position value={self.start_positions[position]} removed")
                del self.start_positions[position]
            else:
                item_value = self.items[position]
                for active_item in active_items:
                    position_counts[active_item] = self.item_counts[active_item]
                #print(f"{position}: score={score:4}, old item={item_value} ({item_length}), active_items={active_items} ({len(active_items)}), counts={self.item_counts.first_items()}")
                #item_codes  = huffman_code(self.item_counts)
                if (item_value in self.item_counts.keys()) and (self.item_counts[item_value] > 0):
                    item_codes  = huffman_code(self.item_counts)
                    #print(f"v={item_value}, counts={self.item_counts}, hc={item_codes}")
                    item_length = len(item_codes[item_value])
                #print(f"{position}: ")
            
            encoded_items.append(SequenceItem(
                position   = position,
                item_value = item_value,
                bit_length = item_length,
            ))

            self.item_counts.update({ item_value: -1 })
            if (self.item_counts[item_value] == 0):
                del self.item_counts[item_value]
                active_items = SortedSet(list(self.item_counts.keys()))
            self.length_counts.update({ item_length: 1 })
            score += item_length
        
        self.encoded_length = score
        self.encoded_items  = encoded_items.copy()

        #print(f"{position}: score={score:4}, old item={item_value} ({item_length}), active_items={active_items} ({len(active_items)}), counts={self.item_counts.first_items()}")
        return encoded_items

@dataclass
class ExcludedItem:
    excluded_value: int
    target_counter: int
    
@dataclass
class DataBlock:
    id                  : int                      = field()
    excluded_values     : SortedSet[int]           = field()
    items               : Dict[int, int]           = field(default_factory=dict, repr=False)
    block_length        : int                      = field(default=DEFAULT_BLOCK_LENGTH, init=False)
    permutation_block   : PermutationBlock         = field(default=None, init=False)
    item_cycle_ids      : Dict[int, int]           = field(default_factory=dict, init=False)
    item_paths          : Dict[int, ValuePath]     = field(default_factory=dict, init=False)
    score               : int                      = field(default=0, init=False)
    final_seed          : int                      = field(default=None)
    item_value_counts   : Counter                  = field(default_factory=Counter, init=False)
    path_value_counts   : Counter                  = field(default_factory=Counter, init=False)
    cycle_path_counts   : Counter                  = field(default_factory=lambda: defaultdict(Counter), init=False)

    def __init__(self, id: int, items: Dict[int, int]=None, seed: int=None):
        if (items is not None) and (len(items) != self.block_length):
            raise Exception(f"Incorrect block length: items={items}")
        self.id                = id
        self.items             = dict()
        for i in range(0, len(items)):
            self.items[i] = items[i]
        self.item_paths        = dict()
        self.item_cycle_ids    = dict()
        self.item_value_counts = Counter()
        self.path_value_counts = Counter()
        self.cycle_path_counts = defaultdict(Counter)
        self.excluded_values   = SortedSet()
        # scan block item values, save existing/missing value distribution
        self.collect_excluded_values()
        if (seed is not None):
            self.final_seed = seed
        self.permutation_block = PermutationBlock(id=self.id, seed=self.final_seed, target_values=self.item_value_counts.copy())
    
    def collect_excluded_values(self):
        #self.item_value_counts.clear()
        if (len(self.excluded_values) > 0):
            return
        block_values = SortedSet(list(self.items.values()))
        for item_id, item_value in self.items.items():
            self.item_value_counts.update({ item_value: 1 })
        
        for byte_value in range(0, 256):
            if (byte_value not in block_values):
                self.excluded_values.add(byte_value)
        #print(f"self.excluded_count={len(self.excluded_values)}")

    def locate_data_items(self, seed: int=None) -> Dict[int, ValuePath] | None:
        #if (len(self.item_paths) > 0):
        #    return
        if (seed is not None):
            self.final_seed = seed
            self.permutation_block.reload_seed(seed=seed, target_values=self.item_value_counts.copy())
        self.item_paths.clear()
        self.item_cycle_ids.clear()
        self.path_value_counts.clear()
        self.cycle_path_counts.clear()
        # TODO: implement scenario when not all byte values presented inside block
        #for item_value in SortedSet(list(self.items.values())):
        for byte_id in range(self.block_length):
            item_value = self.items[byte_id]
            item_path  = self.permutation_block.get_value_path(item_value=item_value)
            if (item_path is None):
                return None
            
            self.item_paths[byte_id]     = item_path
            self.item_cycle_ids[byte_id] = item_path.cycle_id
            self.path_value_counts.update({ item_path.distance: 1 })
            self.cycle_path_counts[item_path.cycle_id].update({ item_path.distance: 1 })
        
        return self.item_paths

    def find_permutation_block(self, min_seed: int=1, max_seed: int=2**16, target_cycles: int=None, force_update: bool=False):
        if (self.final_seed is not None) and (force_update is False):
            return
        for seed in range(min_seed, max_seed):
            # fill block with new items from seed
            self.permutation_block.reload_seed(seed=seed, target_values=self.item_value_counts.copy())
            if (target_cycles is not None) and (len(self.permutation_block.cycles) != target_cycles):
                continue
            item_paths = self.locate_data_items()
            if (item_paths is None):
                continue
            break
        self.final_seed = seed
    
    def restore_data_block(self, seed: int, item_paths: Dict[int, ValuePath]) -> List[int]:
        self.permutation_block.reload_seed(seed=seed, target_values=self.item_value_counts.copy())
        items = list()
        for item_path in item_paths:
            #print(f"{item_path}")
            item = self.permutation_block.read_value_path(item_path=item_path)
            items.append(item)
        return items
    
    def count_block_score(self) -> int:
        score  = 0
        score += 5
        score += self.permutation_block.seed.bit_length()

        start_sequence    = self.collect_start_cycle_positions()
        positions         = list(start_sequence.keys())
        #length_offset     = 0 #len(positions)
        positions_bitmap  = get_target_position_bitmap(positions=positions)
        encoded_positions = encode_position_bitmap(bitmap=positions_bitmap)
        score            += len(encoded_positions)

        cycle_ids       = list(start_sequence.values())
        encoded_cycles  = encode_item_order(items=cycle_ids)
        score          += encoded_cycles.encoded_length

        print(f"positions={positions}, l={len(positions)}, enc_p={encoded_positions}, l={len(encoded_positions)}", positions_bitmap)
        print(f"cycle_ids={cycle_ids}, l={len(cycle_ids)}, enc_c={encoded_cycles}")
        #print(f"pd={delta_to_list(position_deltas)}, l={len(delta_to_list(position_deltas))}")
        print(f"score={score}")

        cycle_id_sequence = [c_obj.cycle_id for cid, c_obj in self.item_paths.items()]
        cycle_id_counts   = self.permutation_block.get_cycle_counts()
        encoded_cycle_ids = EncodedItemSequence(items=cycle_id_sequence, item_counts=cycle_id_counts, start_positions=start_sequence)
        all_cycle_ids     = encoded_cycle_ids.encode_items()

        for position, item_path in self.item_paths.items():
            cycle_id_length  = all_cycle_ids[position].bit_length
            item_path_length = item_path.bit_length
            score           += cycle_id_length
            score           += item_path_length
            #print(f"{position}: score={score} ({cycle_id_length+item_path_length}), cid={item_path.cycle_id} ({cycle_id_length:+}), itp={item_path.distance} ({item_path_length:+})")
        
        print(f"final_score={score}")
        return score
    
    def collect_start_cycle_positions(self) -> Dict[int, int]:
        cycle_ids       = SortedSet(list(self.permutation_block.cycles.keys()))
        start_positions = dict()
        for position, item_path in self.item_paths.items():
            if (item_path.cycle_id in cycle_ids):
                start_positions[position] = item_path.cycle_id
                cycle_ids.remove(item_path.cycle_id)
        return start_positions
    
    def encode_excluded_values(self):
        if (len(self.excluded_values) == 0):
            return
        excluded_sequence = list()
        target_values     = self.item_value_counts.most_common_above(1).copy()
        # visit each excluded value and increase item counter for each duplicate value
        # block size is always 256 bytes, each excluded value will match some duplicated value
        for excluded_value in self.excluded_values:
            closest_duplicate = min(list(target_values.keys()))
            target_values.update({ closest_duplicate: -1 })
            if (target_values[closest_duplicate] == 0):
                del target_values[closest_duplicate]
                excluded_value    = ExcludedItem(excluded_value=excluded_value, target_counter=closest_duplicate)
                excluded_sequence.append(excluded_value)
        
        print(f"{len(excluded_sequence)}")

@dataclass
class PBlockPosition:
    """Permutation block position"""
    seed         : int
    block_number : int
    cycle_id_bits: int
    target_values: Set[int]         = field(default_factory=SortedSet)
    block        : PermutationBlock = field(default=None, repr=False)

def find_block_position(cycle_id_bits: int, target_values: Counter, target_block_number: int, block_id: int=None) -> PBlockPosition:
    seed          = DEFAULT_BLOCK_SEED
    block         = PermutationBlock(id=block_id, seed=DEFAULT_BLOCK_SEED, target_values=target_values)
    target_cycles = 2**cycle_id_bits
    block_number  = 0
    while True:
        block.reload_seed(seed=seed, target_values=target_values)
        if (len(block.cycles) == target_cycles):
            if (block_number == target_block_number):
                return PBlockPosition(
                    seed          = seed,
                    block_number  = block_number,
                    cycle_id_bits = cycle_id_bits,
                    target_values = SortedSet(target_values.keys()),
                    block         = block,
                )
            else:
                block_number += 1
        seed += 1

@dataclass
class BlockBytes:
    items                      : Dict[int, int] = field(default_factory=dict)
    included_values            : Set[int]       = field(default_factory=SortedSet, init=False)
    excluded_values            : Set[int]       = field(default_factory=SortedSet, init=False)
    duplicated_values          : Set[int]       = field(default_factory=SortedSet, init=False)
    value_counts               : Counter        = field(default_factory=Counter, init=False)
    value_mapping_mode         : MappingMode    = field(default=None, init=False)
    included_distances         : List[int]      = field(default_factory=list, init=False)
    excluded_distances         : List[int]      = field(default_factory=list, init=False)
    duplicated_distances       : List[int]      = field(default_factory=list, init=False)
    included_distance_counts   : Counter        = field(default_factory=Counter, init=False)
    excluded_distance_counts   : Counter        = field(default_factory=Counter, init=False)
    included_mapping           : Dict[int, int] = field(default_factory=dict, init=False)
    duplicate_mapping_mode     : MappingMode    = field(default=None, init=False)
    mapped_duplicate_ids       : Set[int]       = field(default_factory=SortedSet, init=False)
    mapped_duplicate_distances : List[int]      = field(default_factory=list, init=False)
    mapped_dd_counts           : Counter        = field(default_factory=Counter, init=False)

    def __init__(self, items: Dict[int, int]):
        if (items is not None) and (len(items) != DEFAULT_BLOCK_LENGTH):
            raise Exception(f"Incorrect block length={len(items)}: items={items}")
        self.items        = dict()
        self.value_counts = init_byte_counter()
        for i in range(0, len(items)):
            byte_value    = items[i]
            if (byte_value < 0) or (byte_value > 255):
                raise Exception(f"Incorrect byte_value={byte_value}, items={items}")
            self.items[i] = byte_value
            self.value_counts.update({ byte_value: 1 })
        
        self.included_values   = SortedSet(self.value_counts.with_count_above(1).keys())
        self.excluded_values   = SortedSet(self.value_counts.with_count(0).keys())
        self.duplicated_values = SortedSet(self.value_counts.with_count_above(2).keys())

        if (len(self.excluded_values) <= len(self.included_values)):
            self.value_mapping_mode = MappingMode.EXCLUDE
        else:
            self.value_mapping_mode = MappingMode.INCLUDE
        
        self.included_distances   = delta_to_list(delta_encode(list(self.included_values)), no_zero_delta=True)
        self.excluded_distances   = delta_to_list(delta_encode(list(self.excluded_values)), no_zero_delta=True)
        self.duplicated_distances = delta_to_list(delta_encode(list(self.duplicated_values)), no_zero_delta=True)

        self.included_distance_counts = Counter(self.included_distances)
        self.excluded_distance_counts = Counter(self.excluded_distances)

        if (len(self.duplicated_values) <= (len(self.included_values) // 2)):
            self.duplicate_mapping_mode = MappingMode.EXCLUDE
        else:
            self.duplicate_mapping_mode = MappingMode.INCLUDE

        self.included_mapping     = dict()
        self.mapped_duplicate_ids = SortedSet()
        iv_id = 0
        for iv in self.included_values:
            self.included_mapping[iv_id] = iv
            iv_id += 1
            if (iv in self.duplicated_values):
                self.mapped_duplicate_ids.add(iv_id)
        
        self.mapped_duplicate_distances = delta_to_list(delta_encode(list(self.mapped_duplicate_ids)), no_zero_delta=True)
        self.mapped_dd_counts           = Counter(self.mapped_duplicate_distances)
    
# number of bits used to define length of maximum positive number in delta values sequence
DEFAULT_DISTANCE_DELTA_BITS = 3

@dataclass
class DistanceMapping:
    # total items in mapping
    mapping_size            : int            = field()
    raw_distances           : List[int]      = field()
    mode                    : MappingMode    = field()
    chain_size              : int            = field(default=None, init=False) # total distances in the chain
    ground_distance         : int            = field(default=None, init=False)
    distances               : List[int]      = field(default_factory=list, init=False) # raw_distances moved down to ground level (distance[i]=raw_distance[i]-ground_distance)
    distance_counts         : Counter        = field(default_factory=Counter, init=False)
    unique_distances        : Set[int]       = field(default_factory=SortedSet, init=False)
    unique_distance_count   : int            = field(default=None, init=False)
    unique_distance_bitmap  : bitarray       = field(default_factory=bitarray, init=False)
    unique_distance_deltas  : List[int]      = field(default_factory=list, init=False)
    sorted_distance_counts  : List[int]      = field(default_factory=list, init=False)
    unique_counts_deltas    : List[int]      = field(default_factory=list, init=False)

    def __init__(self, mapping_size: int, raw_distances: List[int], mode: MappingMode):
        if (mapping_size > DEFAULT_BLOCK_LENGTH):
            raise Exception(f"Incorrect mapping_size={mapping_size}")
        if (len(raw_distances) > mapping_size):
            raise Exception(f"Incorrect mapping_size={mapping_size} for {len(raw_distances)} raw_distances: {raw_distances}")
        self.mapping_size    = mapping_size
        self.mode            = mode
        self.raw_distances   = raw_distances.copy()
        self.chain_size      = len(raw_distances)
        self.ground_distance = min(raw_distances)

        self.distances       = list()
        self.distance_counts = Counter()
        for raw_distance in self.raw_distances:
            distance = raw_distance - self.ground_distance
            self.distances.append(distance)
            self.distance_counts.update({ distance: 1 })
        
        self.unique_distances       = SortedSet(list(self.distance_counts.keys()))
        self.unique_distance_count  = len(self.unique_distances)
        self.unique_distance_bitmap = zeros(max(self.unique_distances)+1, endian=DEFAULT_ENDIAN)
        for i in range(0, max(self.unique_distances)+1):
            if (i in self.unique_distances):
                self.unique_distance_bitmap[i] = 1
        
        self.unique_distance_deltas = delta_to_list(delta_encode(list(self.unique_distances)), no_zero_delta=True)
        self.sorted_distance_counts = [udc for udv, udc in self.distance_counts.last_items()]
        self.unique_counts_deltas   = delta_to_list(delta_encode(self.sorted_distance_counts), no_zero_delta=False)
        
        #self.unique_distance_bits   = max(self.unique_distance_deltas).bit_length()
        #self.unique_distance_mapping = dict()
        #ud_id = 0
        #for unique_distance in self.unique_distances:
        #    self.unique_distance_mapping[ud_id] = unique_distance
        #    ud_id += 1

class CounterSortAxis(int, Enum):
    """Order, used for storing counter key-value pairs"""
    VALUES : int = 0
    COUNTS : int = 1

    def __str__(self):
        return f'{self.name}({self.value})'
    
    def __repr__(self):
        return f'{self.name}({self.value})'

class CounterSortDirection(int, Enum):
    """Sorting direction, used for storing counter key-value pairs"""
    ASC  : int = 0
    DESC : int = 1

    def __str__(self):
        return f'{self.name}({self.value})'
    
    def __repr__(self):
        return f'{self.name}({self.value})'

class SeedTargetType(int, Enum):
    MAX_NEW_ITEMS     : int = 0
    MIN_CHANGES_COUNT : int = 1

    def __str__(self):
        return f'{self.name}({self.value})'
    
    def __repr__(self):
        return f'{self.name}({self.value})'

ENCODED_COUNTER_CHAIN_SIZE_BITS = 8

@dataclass
class EncodedNumberCounter:
    value_counts              : Counter              = field()
    chain_size                : int                  = field(default=None, init=False)
    unique_values             : Set[int]             = field(default_factory=SortedSet, init=False)
    unique_counts             : Set[int]             = field(default_factory=SortedSet, init=False)
    sorted_values             : List[int]            = field(default_factory=list, init=False)
    sorted_counts             : List[int]            = field(default_factory=list, init=False)
    value_distances           : List[int]            = field(default_factory=list, init=False)
    count_distances           : List[int]            = field(default_factory=list, init=False)
    sort_axis                 : CounterSortAxis      = field(default=CounterSortAxis.VALUES, init=False)
    axis_2_sort_direction     : CounterSortDirection = field(default=CounterSortDirection.ASC, init=False)
    
    axis_1_items         : List[int]            = field(default_factory=list, init=False)
    axis_1_distances     : List[int]            = field(default_factory=list, init=False)
    axis_2_items         : List[int]            = field(default_factory=list, init=False)
    axis_2_distances     : List[int]            = field(default_factory=list, init=False)
    axis_1_value_bits    : int                  = field(default=None, init=False)
    axis_2_pos_bits      : int                  = field(default=None, init=False)
    axis_2_neg_bits      : int                  = field(default=None, init=False)

    encoded_bit_length   : int                  = field(default=0, init=False)

    def __init__(self, value_counts: Counter, axis_2_sort_direction: CounterSortDirection=CounterSortDirection.DESC):
        if (len(value_counts) == 0):
            raise Exception(f"Unable to encode empty counter")
        self.axis_2_sort_direction = axis_2_sort_direction
        self.value_counts          = value_counts.copy()
        self.chain_size            = len(self.value_counts)
        self.unique_values         = SortedSet(self.value_counts.keys())
        self.unique_counts         = SortedSet(self.value_counts.values())
        self.sorted_values         = sorted(self.value_counts.keys()).copy()
        self.sorted_counts         = sorted(self.value_counts.values()).copy()
        self.value_distances       = delta_to_list(delta_encode(self.sorted_values), no_zero_delta=False)
        self.count_distances       = delta_to_list(delta_encode(self.sorted_counts), no_zero_delta=False)

        self.axis_1_items     = self.get_axis_1_items(sort_axis=self.sort_axis)
        self.axis_1_distances = self.get_axis_1_distances(sort_axis=self.sort_axis)
        self.axis_2_items     = self.get_axis_2_items(sort_axis=self.sort_axis)
        self.axis_2_distances = self.get_axis_2_distances(sort_axis=self.sort_axis)

        self.axis_1_value_bits = max(self.axis_1_distances).bit_length()
        
        max_axis_2_distance = max(self.axis_2_distances)
        if (max_axis_2_distance > 0):
            self.axis_2_pos_bits = max_axis_2_distance.bit_length()
        else:
            self.axis_2_pos_bits = 0
        
        min_axis_2_distance = min(self.axis_2_distances)
        if (min_axis_2_distance < 0):
            if (self.axis_2_pos_bits > 0) and (min_axis_2_distance > 1):
                self.axis_2_neg_bits = (abs(min_axis_2_distance)-1).bit_length()
            else:
                self.axis_2_neg_bits = abs(min_axis_2_distance).bit_length()
        
        self.calculate_bit_length()
        
    def get_axis_1_distances(self, sort_axis: CounterSortAxis) -> List[int]:
        if (sort_axis == CounterSortAxis.VALUES):
            return self.value_distances
        if (sort_axis == CounterSortAxis.COUNTS):
            return self.count_distances
        raise Exception(f"Incorrect sort_axis={sort_axis}")

    def get_axis_1_items(self, sort_axis: CounterSortAxis) -> List[int]:
        if (sort_axis == CounterSortAxis.VALUES):
            return self.sorted_values
        if (sort_axis == CounterSortAxis.COUNTS):
            return self.sorted_counts
        raise Exception(f"Incorrect sort_axis={sort_axis}")
    
    def get_axis_2_items(self, sort_axis: CounterSortAxis) -> List[int]:
        if (sort_axis == CounterSortAxis.VALUES):
            items = list()
            for value in self.get_axis_1_items(sort_axis=sort_axis):
                items.append(self.value_counts[value])
            if (self.axis_2_sort_direction == CounterSortDirection.ASC):
                return items
            if (self.axis_2_sort_direction == CounterSortDirection.DESC):
                items.reverse()
                return items
            raise Exception(f"Incorrect sort_direction={self.axis_2_sort_direction}")
        raise Exception(f"NOT IMPLEMENTED sort_axis={sort_axis}")
    
    def get_axis_2_distances(self, sort_axis: CounterSortAxis) -> List[int]:
        if (sort_axis == CounterSortAxis.VALUES):
            items = self.get_axis_2_items(sort_axis=sort_axis)
            return delta_to_list(delta_encode(items), no_zero_delta=False)
        raise Exception(f"NOT IMPLEMENTED sort_axis={sort_axis}")
    
    def calculate_bit_length(self) -> int:
        self.encoded_bit_length += ENCODED_COUNTER_CHAIN_SIZE_BITS
        # sort direction for axis 2
        self.encoded_bit_length += 1
        # encoded axis 1 values
        self.encoded_bit_length += self.axis_1_value_bits * self.chain_size
        # use sign bit only if we have values with both signs
        axis_2_sign_bits = 0
        if (self.axis_2_neg_bits > 0) and (self.axis_2_pos_bits > 0):
            axis_2_sign_bits = 1
        
        # encoded values for axis 2
        for distance_value in self.axis_2_distances:
            self.encoded_bit_length += axis_2_sign_bits
            if (distance_value < 0):
                self.encoded_bit_length += self.axis_2_neg_bits
            else:
                self.encoded_bit_length += self.axis_2_pos_bits
        return self.encoded_bit_length
            
@dataclass
class SeedBlockCycle:
    length                 : int            = field()
    type                   : CycleType      = field()
    seed                   : int            = field()
    cycle_id               : int            = field()
    values                 : Set[int]       = field()
    included_bytes         : Set[int]       = field(default_factory=SortedSet)
    excluded_bytes         : Set[int]       = field(default_factory=SortedSet)
    cycle                  : IsolatedCycle  = field(default=None, repr=None)
    included_count         : int            = field(default=None, init=False)
    excluded_count         : int            = field(default=None, init=False)
    inclusive_changes      : Set[int]       = field(default_factory=SortedSet, init=False)
    exclusive_changes      : Set[int]       = field(default_factory=SortedSet, init=False)
    min_changes_count      : int            = field(default=None, init=False)
    min_changes_direction  : CycleType      = field(default=None, init=False)
    new_included_count     : int            = field(default=0)
    new_excluded_count     : int            = field(default=0)
    new_item_count         : int            = field(default=0, init=False)

    def __post_init__(self):
        self.included_count    = len(self.included_bytes)
        self.excluded_count    = len(self.excluded_bytes)
        self.inclusive_changes = self.excluded_bytes.copy()
        self.exclusive_changes = self.included_bytes.copy()
        
        # calculate transformation direction with minimum changes
        if (len(self.exclusive_changes) < len(self.inclusive_changes)):
            self.min_changes_count     = len(self.exclusive_changes)
            self.min_changes_direction = CycleType.EXCLUSIVE
        else:
            self.min_changes_count     = len(self.inclusive_changes)
            self.min_changes_direction = CycleType.INCLUSIVE
        
        self.new_item_count = self.new_included_count + self.new_excluded_count

def collect_best_seed_block_cycles(
        block_bytes: BlockBytes, target_type: SeedTargetType, min_seed: int=1, max_seed: int=2**16
    ) -> List[SeedBlockCycle]:
    block = PermutationBlock(seed=min_seed)
    # init progress UI
    progress = Progress(
        MofNCompleteColumn(),
        BarColumn(),
        TaskProgressColumn(show_speed=True),
        TimeElapsedColumn(),
        TimeRemainingColumn(),
        TextColumn("[progress.description]{task.description}"),
        refresh_per_second=4,
    )
    data_task_id  = progress.add_task("Total", total=max_seed-min_seed, start=True)
    spd_task_id   = progress.add_task("Speed", total=None, start=True)
    progress.start()
    
    best_min_seed_changes    = DEFAULT_BLOCK_LENGTH
    best_seed                = min_seed
    best_seed_cycles         = list()
    best_seed_item_count     = 0
    best_seed_included_bytes = SortedSet()
    best_seed_excluded_bytes = SortedSet()
    for seed in range(min_seed, max_seed):
        # generate new permutation from 256 numbers: it will have from 1 to 128 isolated cycles within it
        block.reload_seed(seed=seed)
        progress.advance(data_task_id)
        progress.advance(spd_task_id)

        seed_cycles         = list()
        seed_changes_count  = 0
        seed_item_count     = 0
        seed_included_count = 0
        seed_excluded_count = 0
        included_seed_bytes = SortedSet()
        excluded_seed_bytes = SortedSet()
        # scan all cycles within the block
        for cycle_id, cycle in block.cycles.items():
            cycle_values         = SortedSet(cycle.items.values())
            cycle_type           = CycleType.INCLUSIVE
            included_cycle_bytes = SortedSet()
            excluded_cycle_bytes = SortedSet()
            
            for value_id, value in cycle.items.items():
                if (value in block_bytes.included_values):
                    included_cycle_bytes.add(value)
                    included_seed_bytes.add(value)
                elif (value in block_bytes.excluded_values):
                    excluded_cycle_bytes.add(value)
                    excluded_seed_bytes.add(value)
                else:
                     raise Exception(f"Incorrect cycle value={value} for cycle_id={cycle_id} in seed={seed}: value not found in included or excluded values") 
                # TODO: save related seed cycles for replaced values (if any)
            # we define cycle type as type of largest part of items within it
            if (len(excluded_cycle_bytes) > len(included_cycle_bytes)):
                cycle_type = CycleType.EXCLUSIVE
            
            # init block_cycle
            seed_cycle = SeedBlockCycle(
                length         = cycle.length,
                type           = cycle_type,
                seed           = seed,
                cycle_id       = cycle.id,
                values         = cycle_values,
                cycle          = cycle,
                included_bytes = included_cycle_bytes.copy(),
                excluded_bytes = excluded_cycle_bytes.copy(),
            )
            seed_changes_count += seed_cycle.min_changes_count

            seed_cycle.new_included_count = 0
            seed_cycle.new_excluded_count = 0
            seed_cycle.new_item_count     = 0
            # search for "pure cycles": all items within them have same type
            if (seed_cycle.min_changes_count == 0):
                if (seed_cycle.min_changes_direction == CycleType.INCLUSIVE):
                    seed_cycle.new_included_count = seed_cycle.length
                    seed_included_count          += seed_cycle.new_included_count
                elif (seed_cycle.min_changes_direction == CycleType.EXCLUSIVE):
                    seed_cycle.new_excluded_count = seed_cycle.length
                    seed_excluded_count          += seed_cycle.new_excluded_count
                else:
                    raise Exception(f"Incorrect min_changes_direction={seed_cycle.min_changes_direction}, for seed_cycle={seed_cycle}")
                seed_cycle.new_item_count = seed_cycle.new_included_count + seed_cycle.new_excluded_count
            # save cycle with other seed cycles
            seed_cycles.append(seed_cycle)
                
        # each change is a permutation - so its actually makes 2 changes at each operation
        seed_changes_count = seed_changes_count // 2
        seed_item_count    = seed_included_count + seed_excluded_count
        if (target_type == SeedTargetType.MIN_CHANGES_COUNT) and (seed_changes_count < best_min_seed_changes):
            best_min_seed_changes = seed_changes_count
            best_seed_cycles      = seed_cycles.copy()
            best_seed             = seed
            print(f"New best_min_seed_changes={best_min_seed_changes}, seed={seed}, s_itc={seed_item_count}, s_inc_c={seed_included_count}, s_exc_c={seed_excluded_count})")
        elif (target_type == SeedTargetType.MAX_NEW_ITEMS) and (seed_item_count > best_seed_item_count):
            best_seed_item_count     = seed_item_count
            best_seed_included_bytes = included_seed_bytes.copy()
            best_seed_excluded_bytes = excluded_seed_bytes.copy()
            best_seed_cycles         = seed_cycles.copy()
            best_seed                = seed
            print(f"New best_seed_item_count={best_seed_item_count}, seed={seed}, s_itc={seed_item_count}, s_inc_c={seed_included_count}, s_exc_c={seed_excluded_count})")
        #block_cycles.append((seed, seed_cycles))
    progress.stop()

    if (target_type == SeedTargetType.MIN_CHANGES_COUNT):
        return (best_min_seed_changes, best_seed, best_seed_cycles)
    elif (target_type == SeedTargetType.MAX_NEW_ITEMS):
        return (best_seed_item_count, best_seed, best_seed_included_bytes, best_seed_excluded_bytes, best_seed_cycles)
    else:
        raise Exception(f"Incorrect target_type={target_type}, for seed={best_seed}")
    
def find_best_cycles_for_block_bytes(block_bytes: BlockBytes, target_values: Counter, min_seed: int=1, max_seed: int=2**16) -> Tuple[SeedBlockCycle, SeedBlockCycle]:
    block                = PermutationBlock(seed=min_seed, target_values=target_values)
    best_included_length = 0
    best_included_cycle  = None
    max_included_length  = len(block_bytes.included_values)
    best_excluded_length = 0
    best_excluded_cycle  = None
    max_excluded_length  = len(block_bytes.excluded_values)

    progress = Progress(
        MofNCompleteColumn(),
        BarColumn(),
        TaskProgressColumn(show_speed=True),
        TimeElapsedColumn(),
        TimeRemainingColumn(),
        TextColumn("[progress.description]{task.description}"),
    )
    data_task_id  = progress.add_task("Total", total=max_seed-min_seed, start=True)
    spd_task_id   = progress.add_task("Speed", total=None, start=True)
    progress.start()
    
    #for seed in track(range(1, 2**16)):
    for seed in range(min_seed, max_seed):
        progress.advance(data_task_id)
        progress.advance(spd_task_id)

        block.reload_seed(seed=seed, target_values=target_values)

        for cycle_id, cycle in block.cycles.items():
            start_value = cycle.start_value
            cycle_type  = None
            if (start_value in block_bytes.included_values):
                cycle_type       = CycleType.INCLUSIVE
                new_cycle_length = min(cycle.length, max_included_length)
                if (new_cycle_length <= best_included_length):
                    continue
                allowed_values = block_bytes.included_values
            elif (start_value in block_bytes.excluded_values):
                cycle_type       = CycleType.EXCLUSIVE
                new_cycle_length = min(cycle.length, max_excluded_length)
                if (new_cycle_length <= best_excluded_length):
                    continue
                allowed_values = block_bytes.excluded_values
            else:
                raise Exception(f"Incorrect start_item={start_value} for cycle_id={cycle_id} in seed={seed}: not found in included or excluded values")
            # check if cycle has path via required values
            if (cycle.has_path_over_values(allowed_values=allowed_values,min_length=new_cycle_length) is False):
                continue
            # new best cycle found
            if (cycle_type == CycleType.INCLUSIVE):
                #print(f"\nNew best inclusive cycle: best_included_length={best_included_length}, new_cycle_length={new_cycle_length}")
                best_included_length = new_cycle_length
                best_included_cycle  = SeedBlockCycle(
                    length   = new_cycle_length,
                    type     = CycleType.INCLUSIVE,
                    seed     = seed,
                    cycle_id = cycle.id,
                    values   = list(cycle.items.values())[0:new_cycle_length],
                    cycle    = cycle,
                )
                progress.update(data_task_id, description=f"best_il={best_included_length}, best_el={best_excluded_length}")
                #pprint(best_included_cycle, max_length=12)
            elif (cycle_type == CycleType.EXCLUSIVE):
                #print(f"\nNew best exclusive cycle: best_excluded_length={best_excluded_length}, new_cycle_length={new_cycle_length}")
                best_excluded_length = new_cycle_length
                best_excluded_cycle  = SeedBlockCycle(
                    length   = new_cycle_length,
                    type     = CycleType.EXCLUSIVE,
                    seed     = seed,
                    cycle_id = cycle.id,
                    values   = list(cycle.items.values())[0:new_cycle_length],
                    cycle    = cycle,
                )
                progress.update(data_task_id, description=f"best_il={best_included_length}, best_el={best_excluded_length}")
                #pprint(best_excluded_cycle, max_length=12)
            else:
                raise Exception(f"Incorrect cycle_type={cycle_type}")
    
    progress.update(data_task_id, description=f"best_il={best_included_length}, best_el={best_excluded_length}")
    progress.update(spd_task_id, description=f"Speed")
    progress.stop()

    print(f"\n")
    print(f"best_included_cycle: ", best_included_cycle)
    print(f"best_excluded_cycle: ", best_excluded_cycle)

    return (best_included_cycle, best_excluded_cycle)

def collect_extra_block_seeds(block_bytes: BlockBytes, max_items: int=128, min_seed: int=1, max_seed: int=2**16) -> List[SeedBlockCycle]:
    block_seeds     = list()
    block_bytes     = deepcopy(block_bytes)
    layer_id        = 0
    total_items     = 0

    while True:
        best_seed = collect_best_seed_block_cycles(
            block_bytes=block_bytes, target_type=SeedTargetType.MAX_NEW_ITEMS, min_seed=min_seed, max_seed=max_seed
        )
        block_seeds.append(best_seed)
        total_items += best_seed[0]
        layer_id    += 1
        print(f"layer_id={layer_id}: best_seed={best_seed}, total_items={total_items}")

        included_bytes = best_seed[2]
        excluded_bytes = best_seed[3]
        for included_byte in included_bytes:
            block_bytes.included_values.remove(included_byte)
            block_bytes.excluded_values.add(excluded_byte)
        for excluded_byte in excluded_bytes:
            block_bytes.excluded_values.add(excluded_byte)
        
        if (total_items >= max_items):
            break
        
    return block_seeds

@dataclass
class DuplicateNode:
    byte_value     : int      = field()
    byte_position  : int      = field()
    prev_position  : int      = field(default=None)
    next_position  : int      = field(default=None)
    distance       : int      = field(default=None, init=False)
    bit_length     : int      = field(default=None, init=False)

@dataclass
class DuplicateChain:
    byte_value       : int       = field()
    byte_position    : int       = field()
    block_bytes      : int       = field(repr=False)
    block_mapping    : bitarray  = field(repr=False)
    next_positions   : List[int] = field(default_factory=list, init=False)
    next_distances   : List[int] = field(default_factory=list, init=False)
    next_bit_lengths : List[int] = field(default_factory=list, init=False)
    chain_bit_length : int       = field(default=None, init=False)

@dataclass
class DuplicateMapping:
    """
    Mapping of repeated byte values for 256-byte block
    Each byte can be marked as:
     - "unique" (repeats 0 times after this position) 
     - "duplicated" (repeats 1 or more times after this position)
    Each duplicated byte contain list of positions of byte copies within the block
    Each position within this list encoded as distance from previous position
    Mapping is dynamic: positions of past duplicate values are excluded from the numeration of next items:
    that allow us to use less bits for encoding future values because total number of potential next position
    in the block will decrease after each mentioned duplicate
    """
    items        : Dict[int, int] = field()
    bitmap       : bitarray       = field(default_factory=lambda: zeros(length=DEFAULT_BLOCK_LENGTH, endian=DEFAULT_ENDIAN), init=False)
    #duplicates   : Dict[int, ]

def create_duplicate_mapping(self):
    duplicate_mapping = zeros(length=DEFAULT_BLOCK_LENGTH, endian=DEFAULT_ENDIAN)
    duplicate_chains  = defaultdict(list)
    




