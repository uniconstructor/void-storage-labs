from __future__ import annotations
from rich import print
from rich.pretty import pprint
from rich.text import Text
from rich.progress import track,\
    BarColumn, Progress, Task, TaskID, TextColumn, TimeElapsedColumn, TimeRemainingColumn,\
    MofNCompleteColumn, RenderableColumn, SpinnerColumn, TransferSpeedColumn, FileSizeColumn, ProgressColumn
from custom_rich import CustomTaskProgressColumn as TaskProgressColumn
from tqdm.notebook import tqdm
from custom_counter import CustomCounter as Counter, ConsumableCounter, init_byte_counter
from collections import defaultdict, ChainMap, deque
from collections.abc import Iterable, Callable, Hashable, Generator,\
    ItemsView, KeysView, ValuesView, MappingView,\
    Mapping, MutableMapping,\
    Sequence, MutableSequence
from bitarray import bitarray, frozenbitarray
from bitarray.util import ba2int, int2ba, canonical_huffman, canonical_decode, huffman_code,\
    vl_encode, vl_decode, sc_encode, sc_decode, serialize, zeros, intervals
from sortedcontainers import SortedSet, SortedDict, SortedList, SortedKeyList, SortedListWithKey,\
    SortedKeysView, SortedValuesView, SortedItemsView
from typing import List, Set, Dict, Tuple, Optional, Union, DefaultDict,\
    TypeAlias, TypeVar, NewType, Annotated, Match, ParamSpec # , ChainMap, Iterable, 
from dataclasses import dataclass, field
from enum import Enum, IntEnum
from copy import deepcopy, copy
from delta_of_delta import delta_encode, delta_decode
# https://github.com/Cyan4973/xxHash
# https://github.com/ifduyue/python-xxhash
import xxhash
import math
import functools
import weakref

from hash_range_iterator import DEFAULT_ENDIAN, nounce_to_input_bytes
from cycle_gen import CMWC
from fib_encoder import fib_encode_position, fib_decode_position, get_fib_lengths, get_fib_value_range, get_fib_position_ranges

# @see https://stackoverflow.com/questions/33672412/python-functools-lru-cache-with-instance-methods-release-object
def memoized_method(*lru_args, **lru_kwargs):
    def decorator(func):
        @functools.wraps(func)
        def wrapped_func(self, *args, **kwargs):
            # We're storing the wrapped method inside the instance. If we had
            # a strong reference to self the instance would never die.
            self_weak = weakref.ref(self)
            @functools.wraps(func)
            @functools.lru_cache(*lru_args, **lru_kwargs)
            def cached_method(*args, **kwargs):
                return func(self_weak(), *args, **kwargs)
            setattr(self, func.__name__, cached_method)
            return cached_method(*args, **kwargs)
        return wrapped_func
    return decorator

# bytes per block
DEFAULT_BLOCK_LENGTH = 256

CANONICAL_COUNT_ID_RANGES_ASC = {
    0: [0, 1],
    1: [1, 9],
    2: [9, 37],
    3: [37, 93],
    4: [93, 163],
    5: [163, 219],
    6: [219, 247],
    7: [247, 255],
    8: [255, 256],
}

CANONICAL_COUNT_ID_RANGES_DESC = {
    8: [0, 1],
    7: [1, 9],
    6: [9, 37],
    5: [37, 93],
    4: [93, 163],
    3: [163, 219],
    2: [219, 247],
    1: [247, 255],
    0: [255, 256],
}

CANONICAL_GROUP_COUNTS = {
    0: 1,
    1: 8,
    2: 28,
    3: 56,
    4: 70,
    5: 56,
    6: 28,
    7: 8,
    8: 1,
}

UNIVERSAL_COUNT_ID_RANGES_ASC = {
    0: [0, 1],
    1: [1, 9],
    2: [9, 37],
    3: [37, 93],
    4: [93, 128],
}

UNIVERSAL_COUNT_ID_RANGES_DESC = {
    8: [0, 1],
    7: [1, 9],
    6: [9, 37],
    5: [37, 93],
    4: [93, 128],
}

UNIVERSAL_GROUP_COUNTS = {
    0: 1,
    1: 8,
    2: 28,
    3: 56,
    4: 45,
    5: 56,
    6: 28,
    7: 8,
    8: 1,
}

class BlockType(int, Enum):
    POINT  : int = 0
    LINE   : int = 1
    SQUARE : int = 2
    CUBE   : int = 3

    def __str__(self):
        return f'{self.name}({self.value})'
    
    def __repr__(self):
        return f'{self.name}({self.value})'

class SectionType(int, Enum):
    POINT_SECTION  : int = 0
    LINE_SECTION   : int = 1
    SQUARE_SECTION : int = 2
    CUBE_SECTION   : int = 3

    def __str__(self):
        return f'{self.name}({self.value})'
    
    def __repr__(self):
        return f'{self.name}({self.value})'

class MappingMode(int, Enum):
    # all positions except given
    EXCLUDE : int = 0
    # only given positions
    INCLUDE : int = 1

    def __str__(self):
        return f'{self.name}({self.value})'
    
    def __repr__(self):
        return f'{self.name}({self.value})'

def get_file_bytes_by_line_id(line_id: int, data: bitarray) -> List[int]:
    line_bytes = list()
    for i in range(DEFAULT_BLOCK_LENGTH*line_id, DEFAULT_BLOCK_LENGTH*(line_id+1)):
        file_byte = ba2int(data[i*8:i*8+8], signed=False)
        line_bytes.append(file_byte)
    return line_bytes

def get_file_bytes_by_square_id(square_id: int, data: bitarray) -> List[int]:
    square_bytes    = list()
    start_line_id = DEFAULT_BLOCK_LENGTH * square_id
    end_line_id    = start_line_id + DEFAULT_BLOCK_LENGTH
    for line_id in range(start_line_id, end_line_id):
        line_bytes = get_file_bytes_by_line_id(line_id=line_id, data=data)
        square_bytes += line_bytes
    return square_bytes

def get_first_bit_position(bitmap: bitarray, target_bit: int) -> int:
    return bitmap.index(target_bit)

def unary_encode(n: int, min_value: int=1, last_bit: int=0, endian: str=DEFAULT_ENDIAN) -> bitarray:
    if (n < min_value):
        raise Exception(f"n={n} must be greater than min_value={min_value}")
    space_bit   = 1 - last_bit
    result      = bitarray('', endian=endian)
    for i in range(min_value, n):
        result.append(space_bit)
    result.append(last_bit)
    return result

def unary_decode(bits: bitarray, min_value: int=1, last_bit: int=0) -> int:
    #space_bit    = 1 - last_bit
    result       = min_value
    if (bits.count(last_bit) == 0):
        # all bits are spacers - last bit is omitted
        result += len(bits)
    else:
        # count all bits before end marker bit
        last_index = bits.index(last_bit)
        result    += last_index
    return result

def make_shifted_list(values: List[int], distance: int) -> List[int]:
    if (distance == 0):
        return values.copy()
    if (distance > 0):
        left_start  = len(values)-distance
        left_end    = len(values)
        right_start = 0
        right_end   = len(values)-distance
    else:
        left_start  = abs(distance)
        left_end    = len(values)
        right_start = 0
        right_end   = abs(distance)
    left_part  = values[left_start:left_end]
    right_part = values[right_start:right_end]
    result     = left_part + right_part
    return result

def encode_item_order(items: List[int], max_values: int=None, endian: str=DEFAULT_ENDIAN) -> bitarray:
    item_count  = len(items)
    item_counts = Counter()
    for v in range(item_count):
        if (v not in items):
            raise Exception(f"Incorrect item order: v={v} is missing. Sorted items={items}")
        item_counts.update({ v: 1 })  
    item_bits = bitarray("", endian=endian)
    for item in items:
        item_codes = huffman_code(item_counts, endian=endian)
        item_code  = item_codes[item].copy()
        item_bits += item_code
        #print(f"item={item}: code={item_code}, item_bits={item_bits} ({len(item_bits)})")
        del item_counts[item]
    return item_bits

def decode_item_order(item_bits: bitarray, length: int, max_values: int=None, endian: str=DEFAULT_ENDIAN) -> List[int]:
    remaining_counts = Counter([x for x in range(length)])
    values           = list()
    if (max_values is None):
        max_values = length
    
    for i in range(0, max_values):
        codes        = huffman_code(remaining_counts, endian=endian)
        code_lengths = Counter()
        for v, vc in codes.items():
            code_lengths.update({ v: len(vc) })
        # start from shortest codes
        decoded_value = None
        for value, code_length in code_lengths.least_common():
            code         = codes[value]
            encoded_bits = item_bits[0:code_length]
            if (code == encoded_bits):
                decoded_value = value
                item_bits     = item_bits[code_length:len(item_bits)].copy()
                values.append(decoded_value)
                del remaining_counts[decoded_value]
                break
        if (decoded_value is None) and (code_length > 0):
            raise Exception(f"Step {i} ({max_values}): No value found for huffman code (encoded_bits={encoded_bits.to01()} ({len(encoded_bits)}), item_bits={item_bits.to01()} ({len(item_bits)})")
        #else:
        #    print(f"{i}: value={decoded_value}, code={code.to01()}, lvb={len(item_bits)}")
    return values

@dataclass
class CounterSelection:
    values         : List[int]
    matches_count  : int
    match_map      : List[bool]
    mirror_mapping : bool

def select_bytes_by_counter(generator: CMWC, k: int, value_counts: Counter, target_values: SortedSet[int]) -> CounterSelection:
    values           = list()
    remaining_counts = value_counts.copy()
    matches_count    = 0
    match_map        = list()
    mirror_mapping   = True
    mirror_distance  = k // 2 
    if (k % 2) == 1:
        mirror_mapping = False
    
    for i in range(0, k):
        weights = [c for _v, c in remaining_counts.first_items()]
        value   = generator.choices(population=range(256), weights=weights, k=1)[0]
        values.append(value)
        # never emit value again if there is no occurrences at this interval
        if (value in target_values):
            remaining_counts.update({ value: -1 })
            matches_count += 1
            match_map.append(True)
        else:
            remaining_counts[value] = 0
            match_map.append(False)
        # sometimes we can use 8 bit to save 16-bit mapping (when first 8 bits are equal inverted last 8 bits)
        if (mirror_mapping is True) and (i >= mirror_distance):
            if (match_map[i] == match_map[i-mirror_distance]):
                mirror_mapping = False
    
    return CounterSelection(
        values         = values,
        matches_count  = matches_count,
        match_map      = match_map,
        mirror_mapping = mirror_mapping,
    )

def count_block_bytes(block_bytes: List[int]) -> Counter:
    counter = init_byte_counter()
    for byte_value in block_bytes:
        counter.update({ byte_value: 1 })
    return counter

def get_block_section_size(section_type: SectionType) -> int:
    if (section_type == SectionType.POINT_SECTION):
        return 4
    if (section_type == SectionType.LINE_SECTION):
        return 8
    if (section_type == SectionType.SQUARE_SECTION):
        return 64
    if (section_type == SectionType.CUBE_SECTION):
        return 512
    raise Exception(f"Incorrect section_type={section_type}")

def get_bitmap_sections(bitmap: bitarray, section_type: SectionType) -> List[bitarray]:
    sections           = list()
    block_section_size = get_block_section_size(section_type=section_type)
    total_sections     = len(bitmap) // block_section_size
    tail_section_size  = len(bitmap) % block_section_size
    if (tail_section_size > 0):
        total_sections = total_sections + 1
    for section_id in range(total_sections):
        start   = section_id * block_section_size
        end     = start + block_section_size
        section = bitmap[start:end]
        sections.append(section)
    return sections

### CANONICAL BITMAPS ###

def create_canonical_section_bitmap_count_codes() -> Dict[int, bitarray]:
    # 0-8 (9 total)
    block_section_size = get_block_section_size(section_type=SectionType.LINE_SECTION)
    canonical_section_bitmap_counts = list(range(0, block_section_size+1))
    count_groups = dict()
    for count_id in canonical_section_bitmap_counts:
        count_groups[count_id] = 1
    canonical_count_codes = dict(SortedDict(huffman_code(count_groups, endian=DEFAULT_ENDIAN)))
    return canonical_count_codes

def create_canonical_section_bitmap_ids(endian: str=DEFAULT_ENDIAN) -> Dict[int, bitarray]:
    result             = dict()
    block_section_size = get_block_section_size(section_type=SectionType.LINE_SECTION)
    bitmap_ids         = list(range(DEFAULT_BLOCK_LENGTH))
    for bitmap_id in bitmap_ids:
        bitmap            = int2ba(bitmap_id, length=block_section_size, endian=endian, signed=False)
        result[bitmap_id] = bitmap
    return result

def create_canonical_bitmap_groups(reverse: bool=True, section_type: SectionType=SectionType.LINE_SECTION) -> Dict[int, List[bitarray]]:
    result              = dict()
    section_size        = get_block_section_size(section_type=section_type)
    bitmaps_by_count_id = defaultdict(list)
    bitmap_values       = list(range(2**section_size))
    group_ids           = range(0, section_size+1)
    for bitmap_value in bitmap_values:
        bitmap            = frozenbitarray(int2ba(bitmap_value, length=section_size, endian=DEFAULT_ENDIAN, signed=False))
        bitmap_count_id   = bitmap.count(1)
        bitmaps_by_count_id[bitmap_count_id].append(bitmap)
    for count_group_id in group_ids:
        result[count_group_id] = sorted(bitmaps_by_count_id[count_group_id], reverse=reverse) # 
    return result

def create_canonical_section_bitmap_positions(reverse: bool=False) -> Dict[int, bitarray]:
    result              = dict()
    block_section_size  = get_block_section_size(section_type=SectionType.LINE_SECTION)
    bitmaps_by_count_id = defaultdict(list)
    bitmap_values       = list(range(DEFAULT_BLOCK_LENGTH))
    for bitmap_value in bitmap_values:
        bitmap            = frozenbitarray(int2ba(bitmap_value, length=block_section_size, endian=DEFAULT_ENDIAN, signed=False))
        bitmap_count_id   = bitmap.count(1)
        bitmaps_by_count_id[bitmap_count_id].append(bitmap)
    
    count_group_ids = sorted(create_canonical_section_bitmap_count_codes().keys(), reverse=reverse)
    bitmap_position = 0
    for count_group_id in count_group_ids:
        sorted_bitmaps = sorted(bitmaps_by_count_id[count_group_id], reverse=(not reverse)) # 
        for sorted_bitmap in sorted_bitmaps:
            result[bitmap_position] = sorted_bitmap
            bitmap_position += 1
    return result

def create_universal_section_bitmap_count_codes() -> Dict[int, bitarray]:
    # 0-4 (5 total)
    block_section_size              = get_block_section_size(section_type=SectionType.LINE_SECTION)
    universal_section_bitmap_counts = list(range(0, (block_section_size // 2) + 1))
    count_groups = dict()
    for count_id in universal_section_bitmap_counts:
        count_groups[count_id] = 1
    universal_count_codes = dict(SortedDict(huffman_code(count_groups, endian=DEFAULT_ENDIAN)))
    return universal_count_codes

def create_canonical_section_bitmap_group_codes() -> Dict[int, bitarray]:
    # 1-7 (7 total)
    block_section_size              = get_block_section_size(section_type=SectionType.LINE_SECTION)
    canonical_section_bitmap_groups = list(range(1, block_section_size))
    group_counts = dict()
    for group_id in canonical_section_bitmap_groups:
        group_counts[group_id] = 1
    canonical_group_codes = dict(SortedDict(huffman_code(group_counts, endian=DEFAULT_ENDIAN)))
    return canonical_group_codes

def create_universal_section_bitmap_group_codes() -> Dict[int, bitarray]:
    # 1-4 (4 total)
    block_section_size              = get_block_section_size(section_type=SectionType.LINE_SECTION)
    universal_section_bitmap_groups = list(range(1, (block_section_size // 2) + 1))
    group_counts = dict()
    for group_id in universal_section_bitmap_groups:
        group_counts[group_id] = 1
    universal_group_codes = dict(SortedDict(huffman_code(group_counts, endian=DEFAULT_ENDIAN)))
    return universal_group_codes

def get_canonical_section_bitmap_by_position(target_position: int, reverse: bool=False) -> bitarray:
    positions = create_canonical_section_bitmap_positions(reverse=reverse)
    return positions[target_position]

def get_canonical_section_bitmap_position(target_bitmap: frozenbitarray, reverse: bool=False) -> int:
    target_bitmap = frozenbitarray(target_bitmap)
    positions     = create_canonical_section_bitmap_positions(reverse=reverse)
    for position, bitmap in positions.items():
        if (target_bitmap == bitmap):
            return position
    raise Exception(f"Incorrect bitmap={target_bitmap.to01()}")

def get_inverted_section_bitmap_position(target_bitmap: frozenbitarray, reverse: bool=False) -> int:
    inverted_bitmap = bitarray(target_bitmap.copy())
    inverted_bitmap.invert()
    return get_canonical_section_bitmap_position(target_bitmap=inverted_bitmap, reverse=reverse)

def is_universal_section_bitmap(bitmap: frozenbitarray, reverse: bool=False) -> bool:
    position = get_canonical_section_bitmap_position(target_bitmap=bitmap, reverse=reverse)
    if (position < 128):
        return True
    return False

def get_universal_section_bitmap_position(bitmap: frozenbitarray, reverse: bool=False) -> int:
    if (is_universal_section_bitmap(bitmap=bitmap, reverse=reverse)):
        return get_canonical_section_bitmap_position(target_bitmap=bitmap, reverse=reverse)
    else:
        return get_inverted_section_bitmap_position(target_bitmap=bitmap, reverse=reverse)

def get_universal_section_bitmap_group_id(bitmap: bitarray, reverse: bool=False) -> int:
    position = get_universal_section_bitmap_position(bitmap=bitmap, reverse=reverse)
    if (reverse is True):
        group_ranges = UNIVERSAL_COUNT_ID_RANGES_DESC
    else:
        group_ranges = UNIVERSAL_COUNT_ID_RANGES_ASC
    for group_id, group_positions in group_ranges.items():
        group_range = range(group_positions[0], group_positions[1])
        if (position in group_range):
            return group_id
    raise Exception(f"Incorrect position={position}")

def get_canonical_section_bitmap_group_id(bitmap: bitarray, reverse: bool=False) -> int:
    position = get_canonical_section_bitmap_position(target_bitmap=bitmap, reverse=reverse)
    if (reverse is True):
        group_ranges = CANONICAL_COUNT_ID_RANGES_DESC
    else:
        group_ranges = CANONICAL_COUNT_ID_RANGES_ASC
    for group_id, group_positions in group_ranges.items():
        group_range = range(group_positions[0], group_positions[1])
        if (position in group_range):
            return group_id
    raise Exception(f"Incorrect position={position}")

def get_group_id_bitmaps(group_id: int, reverse: bool=False) -> List[bitarray]:
    bitmap_groups = create_canonical_bitmap_groups(reverse=(not reverse))
    return bitmap_groups[group_id]

def get_group_id_bitmap(group_id: int, bitmap_id: int, reverse: bool=False) -> bitarray:
    bitmap_groups = create_canonical_bitmap_groups(reverse=(not reverse))
    return bitmap_groups[group_id][bitmap_id]

def get_group_id_bitmap_id(group_id: int, bitmap: int, reverse: bool=False) -> int:
    bitmap_groups = create_canonical_bitmap_groups(reverse=(not reverse))
    return bitmap_groups[group_id].index(bitmap)

### HASH BYTES PICKER ###

@dataclass()
class LocatedSeedValue:
    value              : int          = field()
    data_item_id       : int          = field()
    seed               : int          = field()
    init_item          : int          = field()
    bitmap             : bitarray     = field()
    value_bit_length   : int          = field(default=16, repr=False)

@dataclass()
class AllowedSeedValues:
    seed                : int                       = field()
    target_values       : Dict[int, int]            = field(default=None)
    bitmap_group_id     : int                       = field(default=2)
    group_id_bitmaps    : List[bitarray]            = field(default=None, init=False, repr=False)
    init_items          : List[int]                 = field(default=None, init=False, repr=False)
    seed_values         : Set[int]                  = field(default=None, init=False, repr=False)
    seed_value_counts   : Counter                   = field(default=None, init=False, repr=False)
    seed_bitmaps        : Set[bitarray]             = field(default=None, init=False)
    seed_bitmap_counts  : Counter                   = field(default=None, init=False)
    seed_bitmap_values  : Dict[bitarray, Set[int]]  = field(default=None, init=False)
    init_item_values    : Dict[int, Set[int]]       = field(default=None, init=False)
    init_item_bitmaps   : Dict[int, Set[bitarray]]  = field(default=None, init=False)
    bitmap_item_values  : Dict[bitarray, Dict[int, int]] = field(default=None, init=False, repr=False)
    value_bitmap_items  : Dict[int, Dict[bitarray, int]] = field(default=None, init=False, repr=False)
    digest_input_length : int                       = field(default=None, init=False)

    def __init__(self, seed: int, target_values: Dict[int, int]=dict(), bitmap_group_id: int=2): # , target_values: List[int]=None
        self.seed                = seed
        self.target_values       = target_values.copy()
        self.bitmap_group_id     = bitmap_group_id
        self.group_id_bitmaps    = get_group_id_bitmaps(group_id=self.bitmap_group_id)
        self.init_items          = list(range(256))
        self.seed_values         = SortedSet()
        self.seed_value_counts   = Counter()
        self.seed_bitmaps        = SortedSet()
        self.seed_bitmap_counts  = Counter()
        self.seed_bitmap_values  = defaultdict(SortedSet)
        self.init_item_values    = defaultdict(SortedSet)
        self.init_item_bitmaps   = defaultdict(SortedSet)
        self.bitmap_item_values  = defaultdict(dict)
        self.value_bitmap_items  = defaultdict(dict)
        self.digest_input_length = math.ceil((len(self.init_items) - 1).bit_length() / 8)
        # init seed values
        self.get_available_values(seed=self.seed)
    
    def set_seed(self, seed: int):
        self.seed = seed
        self.seed_values.clear()
        self.seed_value_counts.clear()
        self.seed_bitmaps.clear()
        self.seed_bitmap_counts.clear()
        self.seed_bitmap_values.clear()
        #self.init_item_values.clear()
        #self.init_item_bitmaps.clear()
        #self.bitmap_item_values.clear()
        self.value_bitmap_items.clear()
    
    def set_target_values(self, target_values: Dict[int, int]):
        self.target_values.clear()
        self.target_values = target_values.copy()

    def get_available_values(self, seed: int) -> Set[int]:
        self.set_seed(seed=seed)
        #bitmaps             = get_group_id_bitmaps(group_id=self.bitmap_group_id)
        #digest_input_length = math.ceil((len(self.init_items) - 1).bit_length() / 8)
        
        for init_item in self.init_items:
            digest_input = nounce_to_input_bytes(nounce=init_item, byte_length=self.digest_input_length)
            digest       = xxhash.xxh3_64_digest(digest_input, seed=self.seed)
            for bitmap in self.group_id_bitmaps:
                picked_seed_value = pick_int_digest_bytes_from_bitmap(digest=digest, bitmap=bitmap)
                self.seed_value_counts.update({ picked_seed_value: 1 })
                if (picked_seed_value in self.seed_values):
                    continue
                #if (picked_seed_value not in self.seed_values):
                self.seed_values.add(picked_seed_value)
                self.seed_bitmap_counts.update({ bitmap: 1 })
                #self.seed_bitmaps.add(bitmap)
                #self.seed_bitmap_values[bitmap].add(picked_seed_value)
                #self.init_item_values[init_item].add(picked_seed_value)
                #self.init_item_bitmaps[init_item].add(bitmap)
                #self.bitmap_item_values[bitmap][init_item] = picked_seed_value
                self.value_bitmap_items[picked_seed_value][bitmap] = init_item
        self.seed_bitmaps = SortedSet(self.seed_bitmap_counts.keys())
        return self.seed_values
    
    def locate_target_values(self, seed: int=None, allow_skips: bool=False, target_values: Dict[int, int]=None) -> Dict[int, LocatedSeedValue]:
        located_values = dict()
        if (seed is not None) and (seed != self.seed):
            self.get_available_values(seed=seed)
        if (target_values is not None):
            self.set_target_values(target_values=target_values)
        
        for data_item_id, target_value in self.target_values.items():
            if (target_value in self.seed_values):
                target_value_bitmap = list(self.value_bitmap_items[target_value].keys())[0]
                target_init_item    = self.value_bitmap_items[target_value][target_value_bitmap]
                located_value = LocatedSeedValue(
                    value        = target_value,
                    data_item_id = data_item_id,
                    seed         = self.seed,
                    init_item    = target_init_item,
                    bitmap       = target_value_bitmap,
                )
                located_values[data_item_id] = located_value
            else:
                if (allow_skips is True):
                    # maximize number of located values
                    continue
                else:
                    # maximize sequence length - strictly search target values from list, do not allow missing values
                    break
        return located_values

def get_target_data_values(data: bitarray, start_item_id: int=0, max_items: int=16, item_bit_length: int=16) -> Dict[int, int]:
    target_values = dict()
    end_item_id   = start_item_id + max_items
    item_ids      = range(start_item_id, end_item_id)
    for item_id in item_ids:
        start_bit    = item_id * item_bit_length
        end_bit      = start_bit + item_bit_length
        target_bits  = data[start_bit:end_bit]
        target_value = ba2int(target_bits, signed=False)
        target_values[item_id] = target_value
    return target_values

def pick_digest_bytes_from_bitmap(digest: bytes, bitmap: bitarray) -> bytes:
    return bytes([digest[bp] for bp in bitmap.search(bitarray('1'))])

def pick_int_digest_bytes_from_bitmap(digest: bytes, bitmap: bitarray, endian: str=DEFAULT_ENDIAN) -> int:
    picked_bytes = pick_digest_bytes_from_bitmap(digest=digest, bitmap=bitmap, endian=endian) #bytes([digest[bp] for bp in bitmap.search(bitarray('1'))])
    return int.from_bytes(bytes=picked_bytes, byteorder=endian, signed=False)

def get_data_numbers(item_bits: bitarray, split_size: int) -> List[int]:
    item_bytes = list()
    for byte_id in range(0, len(item_bits) // split_size):
        item_bytes.append(ba2int(item_bits[byte_id*split_size:byte_id*split_size+split_size], signed=False))
    return item_bytes

def get_data_bytes(item_bits: bitarray) -> List[int]:
    return get_data_numbers(item_bits=item_bits, split_size=8)

### PERMUTATIONS

def create_permutation(values: List[int], positions: List[int]) -> List[int]:
    new_values = list()
    if (len(values) != len(positions)):
        raise Exception(f"Number of values must be equal to number of positions")
    for position in positions:
        value    = values[position]
        new_values.append(value)
    return new_values

def restore_permutation(values: List[int], positions: List[int]) -> List[int]:
    new_values = list()
    for _ in range(len(values)):
        new_values.append(None)
    if (len(values) != len(positions)):
        raise Exception(f"Number of values must be equal to number of positions")
    i = 0
    for value in values:
        position             = positions[i]
        new_values[position] = value
        i += 1
    return new_values

### GENERATORS

@dataclass()
class CounterGenerator:
    seed            : int       = field()
    value_counts    : Counter   = field(default=None, init=False)
    default_length  : int       = field(default=None, init=False)
    generator       : CMWC      = field(default=None, init=False, repr=False)
    static_sequence : List[int] = field(default=None, init=False)

    def __init__(self, seed: int, value_counts: Counter):
        self.seed            = seed
        self.value_counts    = value_counts.copy()
        self.default_length  = sum(list(value_counts.values()))
        self.generator       = CMWC(x=self.seed)
        self.static_sequence = list()
        self.init_static_sequence()

    def set_seed(self, seed: int):
        self.seed = seed
        self.generator.seed(seed=seed)
        self.init_static_sequence(length=self.default_length)

    def get_sequence(self, length: int=None):
        if (length is None):
            length = self.default_length
        target_values = list(self.value_counts.with_count_above(target_count=1).keys())
        target_counts = list(self.value_counts.with_count_above(target_count=1).values())
        return self.generator.sample(target_values, k=length, counts=target_counts)
    
    def init_static_sequence(self) -> List[int]:
        self.static_sequence = list()
        self.static_sequence = self.get_sequence().copy()
        return self.static_sequence

    def get_static_sequence(self, length: int=None, start: int=0) -> List[int]:
        if (length is None):
            length = self.default_length
        return self.static_sequence[start:start+length]

@dataclass()
class PermutationGenerator:
    seed            : int       = field()
    default_length  : int       = field(default=256)
    generator       : CMWC      = field(default=None, init=False)#field(default_factory=lambda : CMWC(), init=False)
    static_sequence : List[int] = field(default=None, init=False)

    def __init__(self, seed: int, default_length: int=256):
        self.seed            = seed
        self.default_length  = default_length
        self.generator       = CMWC(x=self.seed)
        self.static_sequence = list()
        self.init_static_sequence()

    def set_seed(self, seed: int):
        self.seed = seed
        self.generator.seed(seed=seed)
        self.init_static_sequence(length=self.default_length)

    def get_sequence(self, length: int=None, seed: int=None):
        if (seed is not None):
            self.set_seed(seed=seed)
        if (length is None):
            length = self.default_length
        return self.generator.sample(range(length), length)
    
    def init_static_sequence(self, length: int=None) -> List[int]:
        if (length is None):
            length = self.default_length
        self.static_sequence = list()
        self.static_sequence = self.get_sequence(length=length).copy()
        return self.static_sequence

    def get_static_sequence(self, length: int=None, start: int=0) -> List[int]:
        if (length is None):
            length = self.default_length
        return self.static_sequence[start:start+length]
    
    def shuffle_bitmap(self, seed: int, bitmap: bitarray) -> bitarray:
        positions  = self.get_sequence(length=len(bitmap), seed=seed)
        old_values = bitmap.tolist().copy()
        new_values = create_permutation(values=old_values, positions=positions)
        return bitarray(new_values)
    
    def restore_bitmap(self, seed: int, bitmap: bitarray) -> bitarray:
        positions  = self.get_sequence(length=len(bitmap), seed=seed)
        new_values = bitmap.tolist().copy()
        old_values = restore_permutation(values=new_values, positions=positions)
        return bitarray(old_values)

@dataclass()
class BitmapStats:
    bitmap                 : bitarray                  = field()
    sections               : Dict[int, bitarray]       = field(default_factory=dict, init=False, repr=False)
    total_sections         : int                       = field(default=None, init=False)
    total_mirrors          : int                       = field(default=0, init=False)
    total_copies           : int                       = field(default=0, init=False)
    tail_section_size      : int                       = field(default=0, init=False)
    section_counts         : Counter                   = field(default_factory=Counter, init=False)
    section_bitmap_counts  : Counter                   = field(default_factory=Counter, init=False)
    used_counts            : Set[int]                  = field(default_factory=SortedSet, init=False)
    count_sections         : Dict[int, Set[int]]       = field(default=None, init=False)
    sorted_section_bitmaps : Dict[int, List[bitarray]] = field(default_factory=lambda: defaultdict(list), init=False)
    section_mirrors        : Dict[int, Set[bitarray]]  = field(default=None, init=False)
    section_copies         : Dict[int, Set[bitarray]]  = field(default=None, init=False)

    def __init__(self, bitmap: bitarray, section_type: SectionType=SectionType.LINE_SECTION):
        block_section_size          = get_block_section_size(section_type=section_type)
        self.bitmap                 = bitmap.copy()
        self.sections               = dict()
        sections                    = get_bitmap_sections(self.bitmap)
        self.total_sections         = len(sections) #len(bitmap) // BLOCK_SECTION_SIZE
        self.tail_section_size      = len(self.bitmap) % block_section_size
        self.section_counts         = Counter()
        self.section_bitmap_counts  = Counter()
        self.used_counts            = SortedSet()
        self.count_sections         = defaultdict(SortedSet)
        self.sorted_section_bitmaps = defaultdict(list)
        self.section_mirrors        = defaultdict(SortedSet)
        self.section_copies         = defaultdict(SortedSet)

        target_copies  = defaultdict(SortedSet)
        target_mirrors = defaultdict(SortedSet)

        for section_id in range(self.total_sections):
            self.sections[section_id] = sections[section_id]
            frozen_section            = frozenbitarray(sections[section_id])
            section_count             = self.sections[section_id].count(1)
            self.section_counts.update({ section_id: section_count })
            self.section_bitmap_counts.update({ frozen_section: 1 })
            self.count_sections[section_count].add(section_id)

            if (section_count > 0) and (section_count < 8):
                if (frozen_section in target_copies[section_count]):
                    self.total_copies += 1
                    self.section_copies[section_count].add(frozen_section)
                if (frozen_section in target_mirrors[section_count]):
                    self.total_mirrors += 1
                    self.section_mirrors[section_count].add(frozen_section)
                inverted_section = ~(sections[section_id].copy())
                target_mirrors[inverted_section.count(1)].add(frozenbitarray(inverted_section))
                target_copies[section_count].add(frozen_section)
        
        self.used_counts = SortedSet(self.count_sections.keys())
    
    def get_margin_sections_count(self) -> int:
        block_section_size = get_block_section_size(section_type=SectionType.LINE_SECTION)
        counts             = self.section_counts.aggregated_counts()
        result             = 0
        if (counts[0] is not None):
            result += counts[0]
        if (counts[block_section_size] is not None):
            result += counts[block_section_size]
        return result
    
    def sort_section_bitmaps(self):
        for section_bitmap in self.sections.values():
            section_count = section_bitmap.count(1)
            self.sorted_section_bitmaps[section_count].append(section_bitmap)
            self.sorted_section_bitmaps[section_count] = sorted(
                self.sorted_section_bitmaps[section_count], 
                key=lambda x: frozenbitarray(x),
                reverse=False
            )
        self.sorted_section_bitmaps = dict(reversed(SortedDict(self.sorted_section_bitmaps).items()))
        # collect section ids - we will encode them to restore original section order
        #for group_id, group_sections in self.sorted_section_bitmaps.items():
        #    for group_section in group_sections:
        #        self.sorted_section_ids.append(group_section.section_id)
        return self.sorted_section_bitmaps


@dataclass()
class SquareStats:
    values            : List[int]            = field(init=False, repr=False)
    line_value_counts : Dict[int, Counter]   = field(init=False, repr=False)
    generator         : PermutationGenerator = field(default_factory=lambda : PermutationGenerator(seed=0), init=False)

    def __init__(self, square_id: int, data: bitarray):
        self.values            = get_file_bytes_by_square_id(square_id=square_id, data=data)
        self.line_value_counts = dict()
        for line_id in range(DEFAULT_BLOCK_LENGTH):
            start_byte_id = line_id * DEFAULT_BLOCK_LENGTH
            end_byte_id   = start_byte_id + DEFAULT_BLOCK_LENGTH
            line_bytes    = self.values[start_byte_id:end_byte_id]
            self.line_value_counts[line_id] = count_block_bytes(block_bytes=line_bytes)
        self.generator = PermutationGenerator(seed=0)
    
    @memoized_method()
    def get_line_ids_with_exact_value_count(self, value: int, count: int) -> List[int]:
        """All line ids where this value has target count"""
        line_ids = list()
        for line_id, line_counts in self.line_value_counts.items():
            if (line_counts[value] == count):
                line_ids.append(line_id)
        return line_ids
    
    @memoized_method()
    def get_line_ids_with_exact_value_count_bitmap(self, value: int, count: int) -> bitarray:
        line_ids       = self.get_line_ids_with_exact_value_count(value=value, count=count)
        line_id_bitmap = zeros(DEFAULT_BLOCK_LENGTH, endian=DEFAULT_ENDIAN)
        for line_id in range(DEFAULT_BLOCK_LENGTH):
            if (line_id in line_ids):
                line_id_bitmap[line_id] = 1
        return line_id_bitmap
    
    @memoized_method()
    def get_values_with_exact_count_in_line_id(self, line_id: int, count: int) -> List[int]:
        """All line values with target count"""
        return list(self.line_value_counts[line_id].with_count(target_count=count).keys())
    
    @memoized_method()
    def get_values_with_exact_count_in_line_id_bitmap(self, line_id: int, count: int) -> bitarray:
        line_values  = self.get_values_with_exact_count_in_line_id(line_id=line_id, count=count)
        value_bitmap = zeros(DEFAULT_BLOCK_LENGTH, endian=DEFAULT_ENDIAN)
        for byte_value in range(256):
            if (byte_value in line_values):
                value_bitmap[byte_value] = 1
        return value_bitmap
    
    @memoized_method()
    def get_max_count_for_value(self, value: int) -> int:
        """Max number of instances of the same value within one line across all blocks"""
        max_count = 0
        for line_id in range(DEFAULT_BLOCK_LENGTH):
            line_value_count = self.line_value_counts[line_id][value]
            if (line_value_count > max_count):
                max_count = line_value_count
        return max_count
    
    @memoized_method()
    def count_used_counts_for_value(self, value: int) -> Counter:
        used_value_counts = Counter()
        for line_id in range(DEFAULT_BLOCK_LENGTH):
            line_value_count = self.line_value_counts[line_id][value]
            if (line_value_count > 0):
                used_value_counts.update({ line_value_count: 1 })
        return used_value_counts
    
    @memoized_method()
    def get_used_counts_for_value(self, value: int) -> Set[int]:
        used_value_counts = self.count_used_counts_for_value(value=value)
        return SortedSet(used_value_counts.keys())
    
    @memoized_method()
    def count_used_counts_for_values(self) -> Counter:
        used_value_counts = Counter()
        for byte_value in range(256):
            byte_value_counts = self.count_used_counts_for_value(value=byte_value)
            count_values      = byte_value_counts.keys()
            for count in count_values:
                used_value_counts.update({ count: 1 })
        return used_value_counts
    
    @memoized_method()
    def get_used_counts_for_values(self) -> Set[int]:
        used_value_counts = self.count_used_counts_for_values()
        return SortedSet(used_value_counts.keys())
    
    @memoized_method()
    def get_used_values_for_count(self, value_count: int) -> Set[int]:
        used_values = SortedSet()
        for byte_value in range(256):
            byte_value_counts = self.count_used_counts_for_value(value=byte_value)
            if (value_count in byte_value_counts):
                used_values.add(byte_value)
        return used_values
    
    @memoized_method()
    def get_total_used_counts_for_value(self, value: int) -> int:
        return len(self.get_used_counts_for_value(value=value))
    
    @memoized_method()
    def get_max_value_count_for_line(self, line_id: int) -> int:
        """Max number of instances of the same value within specific line"""
        return max(self.line_value_counts[line_id].values())
    
    @memoized_method()
    def get_used_value_counts_for_line(self, line_id: int) -> Set[int]:
        """All byte counter values used within specific line (example: [1, 2, 3, 5, 6] - no values repeated 4 times)"""
        return SortedSet(self.line_value_counts[line_id].with_count_above(1).values())
    
    @memoized_method()
    def count_used_value_counts_for_line(self, line_id: int) -> Counter:
        """Counter with all value counts used within specific line"""
        return self.line_value_counts[line_id].with_count_above(1).aggregated_counts()
    
    @memoized_method()
    def get_total_value_counts_for_line(self, line_id: int) -> int:
        """Number of different groups of value counts used by all lines in this square"""
        return len(self.get_used_value_counts_for_line(line_id=line_id))
    
    @memoized_method()
    def get_used_line_ids_for_count(self, value_count: int) -> Set[int]:
        used_lines = SortedSet()
        for line_id in range(DEFAULT_BLOCK_LENGTH):
            line_counts = self.get_used_value_counts_for_line(line_id=line_id)
            if (value_count in line_counts):
                used_lines.add(line_id)
        return used_lines
    
    @memoized_method()
    def count_square_used_line_count_groups(self) -> Counter:
        counter = Counter()
        for line_id in range(DEFAULT_BLOCK_LENGTH):
            used_counts = tuple(self.get_used_value_counts_for_line(line_id=line_id))
            counter.update({ used_counts: 1 })
        return counter

    @memoized_method()
    def count_square_used_line_count_group_lengths(self) -> Counter:
        counter     = Counter()
        used_counts = self.count_square_used_line_count_groups()
        for count_group, num_instances in used_counts.most_common():
            group_length = len(count_group)
            counter.update({ group_length: num_instances })
        return counter
    
    @memoized_method()
    def collect_square_used_line_count_groups_line_ids(self) -> Dict[int, Set[int]]:
        line_ids = defaultdict(SortedSet)
        for line_id in range(DEFAULT_BLOCK_LENGTH):
            used_counts = tuple(self.get_used_value_counts_for_line(line_id=line_id))
            line_ids[used_counts].add(line_id)
        return line_ids
    
    @memoized_method()
    def count_square_used_value_count_groups(self) -> Counter:
        counter = Counter()
        for byte_value in range(256):
            used_counts = tuple(self.get_used_counts_for_value(value=byte_value))
            counter.update({ used_counts: 1 })
        return counter

    @memoized_method()
    def count_square_used_value_count_group_lengths(self) -> Counter:
        counter     = Counter()
        used_counts = self.count_square_used_value_count_groups()
        for count_group, num_instances in used_counts.most_common():
            group_length = len(count_group)
            counter.update({ group_length: num_instances })
        return counter
    
    @memoized_method()
    def collect_square_used_value_count_groups_values(self) -> Dict[int, Set[int]]:
        byte_values = defaultdict(SortedSet)
        for byte_value in range(256):
            used_counts = tuple(self.get_used_counts_for_value(value=byte_value))
            byte_values[used_counts].add(byte_value)
        return byte_values
    
    @memoized_method()
    def collect_byte_line_ids_by_value_count(self, value_count: int) -> Dict[int, Dict[int, Set[int]]]:
        byte_values = defaultdict(lambda: defaultdict(SortedSet))
        for byte_value in range(256):
            byte_line_ids   = self.get_line_ids_with_exact_value_count(value=byte_value, count=value_count)
            byte_line_count = len(byte_line_ids)
            if (byte_line_count > 0):
                for line_id in byte_line_ids:
                    byte_values[byte_line_count][byte_value].add(line_id)
        return dict(SortedDict(byte_values))

    @memoized_method()
    def collect_byte_line_ids_by_value_counts(self) -> Dict[int, Dict[int, Dict[int, Set[int]]]]:
        count_groups = dict()
        used_counts  = self.get_used_counts_for_values()
        for value_count in used_counts:
            count_groups[value_count] = self.collect_byte_line_ids_by_value_count(value_count=value_count)
        return count_groups

    @memoized_method()
    def collect_line_byte_ids_by_value_count(self, value_count: int) -> Dict[int, Dict[int, Set[int]]]:
        line_bytes = defaultdict(lambda: defaultdict(SortedSet))
        for line_id in range(DEFAULT_BLOCK_LENGTH):
            line_id_bytes    = self.get_values_with_exact_count_in_line_id(line_id=line_id, count=value_count)
            line_bytes_count = len(line_id_bytes)
            if (line_bytes_count > 0):
                for byte_value in line_id_bytes:
                    byte_value_id = self.get_byte_id_for_count_group(value_count=value_count, byte_value=byte_value)
                    line_bytes[line_bytes_count][line_id].add(byte_value_id)
                    #line_bytes[line_bytes_count][line_id].add(byte_value)
        return dict(SortedDict(line_bytes))
    
    @memoized_method()
    def collect_line_byte_ids_by_value_counts(self) -> Dict[int, Dict[int, Dict[int, Set[int]]]]:
        count_groups = dict()
        used_counts  = self.get_used_counts_for_values()
        for value_count in used_counts:
            count_groups[value_count] = self.collect_line_byte_ids_by_value_count(value_count=value_count)
        return count_groups
    
    #@memoized_method()
    def collect_byte_values_by_tree_depth(self, tree_depth: int) -> Dict[int, Dict[int, Set[int]]]:
        byte_values = defaultdict(lambda: defaultdict(SortedSet))
        used_counts = self.get_used_counts_for_values()
        for value_count in used_counts:
            count_group_byte_ids = self.collect_line_byte_ids_by_value_count(value_count=value_count)
            count_group_depths    = SortedSet(count_group_byte_ids.keys())
            if (tree_depth not in count_group_depths):
                continue
            count_group_lines = count_group_byte_ids[tree_depth]
            for line_id, line_id_byte_ids in count_group_lines.items():
                if (len(line_id_byte_ids) == 0):
                    continue
                #for line_byte_value in line_id_bytes:
                #    byte_value_id = self.get_byte_id_for_count_group(value_count=value_count, byte_value=line_byte_value)
                #    byte_values[value_count][line_id].add(byte_value_id)
                byte_values[value_count][line_id].update(line_id_byte_ids)
        return byte_values
    
    @memoized_method()
    def get_byte_value_tree_depths(self) -> Set[int]:
        line_bytes  = self.collect_line_byte_ids_by_value_counts()
        tree_depths = SortedSet()
        for value_count, count_lines in line_bytes.items():
            tree_depths.update(list(count_lines.keys()))
        return tree_depths
    
    @memoized_method()
    def get_max_byte_value_tree_depth(self) -> int:
        tree_depths = self.get_byte_value_tree_depths()
        return max(tree_depths)
    
    @memoized_method()
    def collect_byte_values_by_tree_depths(self) -> Dict[int, Dict[int, Dict[int, Set[int]]]]:
        tree_depths  = self.get_byte_value_tree_depths()
        depth_groups = dict()
        for tree_depth in tree_depths:
            depth_groups[tree_depth] = self.collect_byte_values_by_tree_depth(tree_depth=tree_depth)
        return depth_groups
    
    #@memoized_method()
    def get_byte_id_for_count_group(self, value_count: int, byte_value: int) -> int:
        group_values = self.get_used_values_for_count(value_count=value_count)
        if (byte_value not in group_values):
            raise Exception(f"byte_value={byte_value} not found for value_count={value_count}, group_values={group_values}")
        return list(group_values).index(byte_value)
    
    #@memoized_method()
    def get_byte_value_for_count_group(self, value_count: int, byte_id: int) -> int:
        group_values = list(self.get_used_values_for_count(value_count=value_count))
        if (len(group_values) <= byte_id):
            raise Exception(f"byte_id={byte_id} not found for value_count={value_count}, group_values={group_values}")
        return group_values[byte_id]

### DATA TREE ###

@dataclass()
class SquareDataTree:
    stats                : SquareStats        = field(repr=False)
    used_levels          : Set[int]           = field(default_factory=SortedSet(), init=False)
    used_level_counts    : Counter            = field(default_factory=Counter(), init=False)
    level_byte_id_counts : Dict[int, Counter] = field(default=None, init=False, repr=False)

    def __init__(self, stats: SquareStats):
        self.stats                = stats
        self.used_levels          = SortedSet([ul-1 for ul in self.stats.get_byte_value_tree_depths()])
        self.used_level_counts    = Counter()
        self.level_byte_id_counts = defaultdict(Counter)
        #for level_id in self.used_levels:
        #    self.level_byte_id_counts[level_id] = Counter()
    
    def has_level_byte_id(self, level_id: int, byte_id: int) -> bool:
        if (level_id not in self.used_levels):
            return False
        #if (level_id not in self.level_byte_id_counts.keys()):
        #    return False
        if (byte_id in self.level_byte_id_counts[level_id].keys()):
            return True
        return False
    
    def update_level_id_byte_ids(self, target_level_id: int):
        #if (tree_depth <= 0):
        #    raise Exception(f"Tree depth must be > 0 (tree_depth={tree_depth} given)")
        tree_depth  = target_level_id + 1
        byte_values = self.stats.collect_byte_values_by_tree_depth(tree_depth=tree_depth)
        if (len(byte_values) == 0):
            return
        updated_counts           = defaultdict(Counter)
        new_level_byte_ids       = defaultdict(SortedSet)
        new_level_byte_id_counts = Counter()
        new_byte_ids_count       = 0
        level_ids                = SortedSet()
        
        for used_level in self.used_levels:
            level_ids.add(used_level)
            if (used_level == target_level_id):
                break
        
        for value_count, value_count_lines in byte_values.items():
            for line_id, line_byte_ids in value_count_lines.items():
                byte_ids = list(line_byte_ids)
                for level_id in level_ids:
                    byte_id = byte_ids[level_id]
                    updated_counts[level_id].update({ byte_id: 1 })
                    has_level_byte_id = self.has_level_byte_id(level_id=level_id, byte_id=byte_id)
                    #if (level_id not in self.level_byte_id_counts.keys()):
                    #    self.level_byte_id_counts[level_id] = Counter()
                    if (has_level_byte_id is False) and (byte_id not in new_level_byte_ids[level_id]):
                        new_level_byte_ids[level_id].add(byte_id)
                        new_byte_ids_count += 1
                        new_level_byte_id_counts.update({ level_id: 1 })
        
        for level_id in level_ids:
            self.level_byte_id_counts[level_id].update(updated_counts[level_id])
            sorted_level_counter = Counter()
            for byte_value_id, byte_value_count in sorted(self.level_byte_id_counts[level_id].items()):
                sorted_level_counter[byte_value_id] = byte_value_count
            self.level_byte_id_counts[level_id] = sorted_level_counter
            self.used_level_counts[level_id]    = len(self.level_byte_id_counts[level_id])
        #print(f"updated_counts={updated_counts}")
        #print(f"new_level_byte_ids={new_level_byte_ids}")
        print(f"tl_id={target_level_id}: new_level_byte_ids_count={new_byte_ids_count}, new_level_byte_id_counts={new_level_byte_id_counts.first_items()}")
    
    def load_tree_byte_ids(self, max_level: int=None):
        for level_id in self.used_levels:
            self.update_level_id_byte_ids(target_level_id=level_id)
            if (max_level is not None) and (max_level == level_id):
                break