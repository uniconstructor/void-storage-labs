# https://stackoverflow.com/questions/38340808/recursive-typing-in-python-3-5
from __future__ import annotations
# https://github.com/Textualize/rich
from rich import print
# https://rich.readthedocs.io/en/latest/pretty.html
from rich.pretty import pprint
from tqdm import tqdm

from bitarray.util import ba2int, int2ba, ba2hex, hex2ba, make_endian, vl_encode, vl_decode, huffman_code
from bitarray import bitarray, frozenbitarray
from custom_counter import CustomCounter as Counter
from collections import defaultdict, deque
from delta_of_delta import delta_encode, delta_decode, delta_of_delta_encode, delta_of_delta_decode

from typing import List, Set, Dict, Tuple, Optional, Union

from fib_encoder import fib_encode_position, fib_decode_position, fib_decode_positions, fib_encode_positions, \
  get_max_values_for_position, get_fib_lengths, get_next_fib_for_position, \
  get_max_values_for_position_length, get_fib_position_range, get_fib_prefix_options, init_fib_value_positions, \
  get_total_values_in_fib_range
from hash_space_utils import get_min_bit_length
from canonical_huffman_encoder import create_canonical_codes

from dataclasses import dataclass, field

@dataclass
class FibSplit:
    length_counts       : Counter
    value_counts        : Counter
    split_items         : Tuple[frozenbitarray]
    data_values         : Dict[int, Set[frozenbitarray]]
    #encoded_data        : frozenbitarray
    #remaining_data      : frozenbitarray
    capacity_overflow   : bool
    encoded_items_count : int
    unique_values_count : int
    end_bit             : int
    data_length         : int
    items_length        : int

@dataclass
class BackwardFibSplit:
    length_counts       : Counter
    value_counts        : Counter
    split_items         : Tuple[frozenbitarray]
    data_values         : Dict[int, Set[frozenbitarray]]
    capacity_overflow   : bool
    encoded_items_count : int
    unique_values_count : int
    data_values_order   : Dict[int, Dict[frozenbitarray, int]]
    end_bit             : int
    data_length         : int
    items_length        : int
    item_limits         : Dict[int, int]
    prefix_length       : int           
    prefix_bits         : frozenbitarray

def create_fib_split(data: frozenbitarray, encoder_type: str='C1', score: int=1, max_value_length: int=None, prefer_existing: bool=False) -> FibSplit:
    """
    Разбить данные на элементы разной длины, начиная с указанной минимальной. Если значение ранее было упомянуто
    в данных - то оно используется повторно. Если значение встречается в первый раз - оно добавляется в словарь.
    Количество значений в словаре ограничено таким образом чтобы позиция значения всегда была короче чем само значение
    """
    data_length         = len(data)
    items_length        = 0
    max_seeds           = 2**(score - 1)
    #print(f"score={score}, max_seeds={max_seeds}")
    # define maximum values for each value length
    max_items_by_length = get_fib_lengths(encoder_type=encoder_type).copy()
    # все возможные варианты длины значения
    position_lengths    = [(vl - 1) for vl in sorted(max_items_by_length.keys())]
    value_lengths       = [(pl + score) for pl in position_lengths]
    max_items_by_value_length = {}
    for pl in position_lengths:
        max_items_by_value_length[pl + score] = (max_items_by_length[pl + 1] * max_seeds)
    min_value_length    = min(value_lengths)
    # варианты длины значений, для которых доступны свободные позиции
    open_lengths        = value_lengths.copy()
    # варианты длины значений, использованные хотя бы 1 раз
    used_lengths        = list()
    # варианты длины значений, для которых закончились свободные позиции
    closed_lengths      = list()
    # индикатор того что в процессе разбиения закончились слоты значений
    capacity_overflow   = False
    # уникальные значения, сгруппированные по длине
    data_values         = defaultdict(set)
    restricted_values   = defaultdict(set)
    split_items         = list()
    length_counts       = Counter()
    value_counts        = Counter()
    start_bit           = 0
    end_bit             = 0
    progress            = tqdm(total=data_length, smoothing=0, mininterval=0.5, miniters=10000)
    # read data using variable length items
    while (True):
        item_value = None
        for value_length in value_lengths:
            if ((start_bit + value_length) > data_length):
                value_length = value_length - 1
                break
            # try to read and use short values first, use long values only if we dont have short ones
            end_bit    = start_bit + value_length
            scan_value = data[start_bit:end_bit]
            if (scan_value not in data_values[value_length]):
                # new (unique) value received
                if (scan_value in restricted_values[value_length]):
                    # value must not be prefix of another (already used) value
                    continue
                if length_counts[value_length] >= max_items_by_value_length[value_length]:
                    # reached maximum capacity for values with given length
                    if (value_length == max_value_length):
                        # maximum unique values received - dictionary overflow
                        capacity_overflow = True
                        break
                    else:
                        # use capacity from next length tier
                        continue
                else:
                    # value length has open values
                    has_next_value  = False
                    next_start_bit  = 0
                    next_end_bit    = 0
                    next_scan_value = None
                    # check possible next value
                    if (prefer_existing is True) and (length_counts[value_length] == max_items_by_value_length[min_value_length]) and (len(used_lengths) > 0) and (end_bit <= data_length):
                        next_start_bit = end_bit
                        for next_value_length in used_lengths:
                            next_scan_value = None
                            next_end_bit    = next_start_bit + next_value_length
                            if (next_end_bit >= data_length):
                                has_next_value = True
                                break
                            next_scan_value = data[next_start_bit:next_end_bit]
                            if (next_scan_value in data_values[next_value_length]):
                                has_next_value = True
                                break
                    else:
                        has_next_value = True
                    # prefer items with existing next values (when possible)
                    if (has_next_value is False):
                        continue
                    # read new item value
                    item_value = data[start_bit:end_bit]
                    # add new value to dictionary
                    data_values[value_length].add(item_value)
                    # update restricted value list
                    for restricted_length in open_lengths:
                        if (restricted_length >= value_length):
                            break
                        restricted_value = item_value[0:restricted_length]
                        restricted_values[restricted_length].add(restricted_value)
                    # updating item counter (and decrease tier capacity by 1 value)
                    length_counts.update({value_length : 1})
                    # update used lengths list
                    if (length_counts[value_length] == 1):
                        used_lengths = sorted(length_counts.keys())
                    # update open/closed lengths list
                    if (length_counts[value_length] >= max_items_by_value_length[value_length]):
                        open_lengths.remove(value_length)
                        closed_lengths.append(value_length)
                        restricted_values[value_length].clear()
                    break
            else:
                # existing (not unique) value recieved - do not modify our dictionary, but save item value to split result
                item_value = data[start_bit:end_bit]
                break
        if capacity_overflow == True:
            # input max_value_length is not enough to create a dictionary
            #raise Exception(f"Length capacity reached: {length_counts} (items_processed={len(split_items)}, max_value_length={max_value_length})")
            break
        # update usage counter of the dictionary value
        value_counts.update({item_value : 1})
        # append item value to final result
        split_items.append(item_value)
        # update total length of all encoded items
        items_length = items_length + (value_length - score)
        # update progress
        progress.update(value_length)
        progress.set_description(f"{len(value_counts)} ({len(split_items)}), il={items_length}", refresh=False)
        # move to next item
        start_bit = end_bit
        # checking end of data
        if (end_bit >= data_length) or (start_bit >= data_length) or (item_value is None):
            # TODO: save and return last item if any
            #if (item_value is not None):
            break
    return FibSplit(
        length_counts       = length_counts,
        value_counts        = value_counts,
        split_items         = tuple(split_items),
        data_values         = data_values,
        #encoded_data        = data[0:end_bit],
        #remaining_data      = data[end_bit:data_length],
        capacity_overflow   = capacity_overflow,
        encoded_items_count = len(split_items),
        unique_values_count = len(value_counts),
        end_bit             = end_bit,
        data_length         = data_length,
        items_length        = items_length,
        item_limits         = max_items_by_value_length,
    )

@dataclass
class FibMapping:
    fib_split : FibSplit
    max_seeds : int

def prepare_fib_mapping(data: frozenbitarray, score: int=1, encoder_type: str='C1', prefer_existing: bool=False) -> FibMapping:
    max_seeds = 2**(score - 1)
    #print(f"score={score}, max_seeds={max_seeds}")
    fib_split = create_fib_split(data=data, encoder_type=encoder_type, score=score, prefer_existing=prefer_existing)
    return FibMapping(
        fib_split = fib_split,
        max_seeds = max_seeds,
    )

def create_backward_fib_split(data: frozenbitarray, score: int=1, min_position_length: int = 0, max_position_length: int=21) -> BackwardFibSplit:
    """
    Разбить данные на элементы разной длины, начиная с указанной минимальной. Если значение ранее было упомянуто
    в данных - то оно используется повторно. Если значение встречается в первый раз - оно добавляется в словарь.
    Количество значений в словаре ограничено таким образом чтобы позиция значения всегда была короче чем само значение.
    Разбиение происходит от конца к началу. Если последний элемент не присутствует в словаре и недостаточно длинный для
    создания нового значения то он сохраняется отдельно.
    """
    data_length         = len(data)
    #processed_length    = 0
    items_length        = 0
    #max_seeds           = 2**(score - 1)
    #print(f"score={score}, max_seeds={max_seeds}")
    # define maximum values for each value length
    #max_items_by_length = get_fib_lengths(encoder_type='C1').copy()
    #max_items_by_length = Counter()
    length_id_dict      = create_canonical_codes(values=list(range(min_position_length, max_position_length)))
    # все возможные варианты длины значения
    position_lengths = []
    value_lengths    = []
    for pl, length_id_code in length_id_dict.id_codes.items():
        vl = pl + len(length_id_code) + score
        #max_items_by_length[vl] = 2**vl
        position_lengths.append(pl)
        value_lengths.append(vl)
    max_value_length = max(value_lengths)

    max_items_by_value_length = {}
    for pl, length_id_code in length_id_dict.id_codes.items():
        vl = pl + len(length_id_code) + score
        max_items_by_value_length[vl] = 2**pl
    
    print(f'max_items_by_value_length:', max_items_by_value_length)
    print(f'value_lengths: {value_lengths}')

    # варианты длины значений, для которых доступны свободные позиции
    open_lengths        = value_lengths.copy()
    # варианты длины значений, для которых закончились свободные позиции
    closed_lengths      = list()
    # индикатор того что в процессе разбиения закончились слоты значений
    capacity_overflow   = False
    # уникальные значения, сгруппированные по длине
    data_values         = defaultdict(set)
    # порядок в котором были добавлены уникальные значения (сгруппировано по длине)
    data_values_order   = defaultdict(dict)
    restricted_values   = defaultdict(set)
    split_items         = list()
    length_counts       = Counter()
    value_counts        = Counter()
    item_count          = 0
    unique_count        = 0
    start_bit           = data_length
    end_bit             = data_length
    progress            = tqdm(total=data_length, smoothing=0, mininterval=0.5, miniters=10000)
    # read data using variable length items
    while (True):
        item_value = None
        for value_length in value_lengths:
            if ((end_bit - value_length) < 0):
                value_length = end_bit #value_length - 1
                start_bit    = 0 #end_bit - value_length
                item_value   = data[start_bit:end_bit]
                break
            # try to read and use short values first, use long values only if we dont have short ones
            start_bit  = end_bit - value_length
            scan_value = data[start_bit:end_bit]

            if (scan_value not in data_values[value_length]):
                # new (unique) value received
                if (scan_value in restricted_values[value_length]):
                    # value must not be prefix of another (already used) value
                    continue
                if length_counts[value_length] >= max_items_by_value_length[value_length]:
                    # reached maximum capacity for values with given length
                    if (value_length >= max_value_length):
                        # maximum unique values received - dictionary overflow
                        capacity_overflow = True
                        break
                    else:
                        # use capacity from next length tier
                        continue
                else:
                    # value length has open values
                    # read new item value
                    item_value       = data[start_bit:end_bit]
                    item_value_order = len(data_values[value_length])
                    # save order of adding for unique items
                    data_values_order[value_length][item_value] = item_value_order
                    # add new value to dictionary
                    data_values[value_length].add(item_value)
                    # updating item counter (and decrease tier capacity by 1 value)
                    length_counts.update({ value_length : 1 })
                    unique_count += 1
                    # update restricted value list
                    for restricted_length in open_lengths:
                        if (restricted_length >= value_length):
                            break
                        restricted_value = item_value[0:restricted_length]
                        restricted_values[restricted_length].add(restricted_value)
                    # update open/closed lengths list
                    if (length_counts[value_length] >= max_items_by_value_length[value_length]):
                        open_lengths.remove(value_length)
                        closed_lengths.append(value_length)
                        restricted_values[value_length].clear()
                    break
            else:
                # existing (not unique) value received - do not modify our dictionary, but save item value to split result
                item_value = data[start_bit:end_bit]
                break
        if capacity_overflow == True:
            # input max_value_length is not enough to create a dictionary
            #raise Exception(f"Length capacity reached: {length_counts} (items_processed={len(split_items)}, max_value_length={max_value_length})")
            break
        # update usage counter of the dictionary value
        value_counts.update({ item_value : 1 })
        # append item value to final result
        #split_items.append(item_value)
        #split_items.insert(0, item_value)
        split_items.append(item_value)
        item_count += 1
        # update total length of all encoded items
        items_length = items_length + (value_length - score)
        # update progress
        progress.update(value_length)
        progress.set_description(f"uc={unique_count} (ic={item_count}), til={items_length}, vl={value_length}", refresh=False)
        #progress.set_postfix({
        #    'vlc': f"{value_counts.most_common(5)}", 
        #}, refresh=False)
        # checking end of data
        prefix_length = 0
        prefix_bits   = data[0:end_bit]
        if (end_bit <= 0) or (start_bit <= 0) or (item_value is None):
            # save and return prefix item if any
            if (item_value is not None):
                prefix_length = len(item_value)
                prefix_bits   = frozenbitarray(item_value)
            else:
                prefix_length = 0
                prefix_bits   = data[0:end_bit]
            # stop split process: end of data
            break
        # move to next item
        end_bit = start_bit
    
    split_items.reverse()
    return BackwardFibSplit(
        length_counts       = length_counts,
        value_counts        = value_counts,
        split_items         = tuple(split_items),
        data_values         = data_values,
        capacity_overflow   = capacity_overflow,
        encoded_items_count = len(split_items),
        unique_values_count = len(value_counts),
        data_values_order   = data_values_order,
        end_bit             = end_bit,
        data_length         = data_length,
        items_length        = items_length,
        item_limits         = max_items_by_value_length,
        prefix_length       = prefix_length,
        prefix_bits         = prefix_bits,
    )


@dataclass
class ItemValueMapper:
    pass