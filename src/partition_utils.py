# https://stackoverflow.com/questions/38340808/recursive-typing-in-python-3-5
from __future__ import annotations

from enum import unique
from rich import print
from rich.pretty import pprint

# https://pyoxidizer.readthedocs.io/en/stable/pyoxidizer_packaging_static_linking.html
from tqdm import tqdm as tqdm_notebook

# https://bitstring.readthedocs.io/en/latest/index.html
from bitstring import Bits, BitArray, BitStream, ConstBitStream
from collections import (
    OrderedDict,
    Counter as BaseCounter,
    defaultdict,
    namedtuple,
    deque,
)

# https://docs.python.org/3/library/typing.html
from typing import List, Dict, Tuple, Optional, Union, Set, Iterable

# https://realpython.com/python-data-classes/#advanced-default-values
from dataclasses import dataclass
import os, io, leb128, json, copy

if not "workbookDir" in globals():
    workbookDir = os.getcwd()
os.chdir(workbookDir)

from hash_space_utils import (
    HashItemAddress,
    HashSegmentAddress,
    count_split_values,
    get_min_bit_length,
    get_aligned_bit_length,
    bytes_at_position,
    value_at_position,
)

from custom_counter import CustomCounter as Counter

# maximum number of seeds, used to allocate 256 2-byte values
DEFAULT_PARTITION_SEEDS_1BYTE = 60

# max values/positions: 256, 
# max seeds: 60, 
# min values per seed: 1
# max values per seed: 8
DEFAULT_TARGET_SCORES_1BYTE = [
    1, 1, 1, 1, 1, 1, 1, 1,
    2, 2, 2, 2, 2, 2, 2, 2,
    3, 3, 3, 3, 3, 3, 3, 3,
    4, 4, 4, 4, 4, 4, 4, 4,
    5, 5, 5, 5, 5, 5, 5, 5,
    6, 6, 6, 6, 6, 6, 6, 6,
    7, 7, 7, 7, 7, 7, 7, 7,
    8, 8, 8, 8,
]

# score order, used for filling target scores for partition
DEFAULT_SCORE_DISTRIBUTION_1BYTE = [
    1, 1, 1, 1, 1, 1, 1, 1,
    2, 2, 3, 4, 2, 2, 3, 4,
    2, 2, 2, 2, 3, 3, 4, 4,
    3, 3, 3, 3, 4, 4, 4, 4, 
    5, 6, 7, 8, 5, 6, 7, 8,
    5, 5, 6, 6, 5, 5, 5, 5,
    6, 6, 6, 6, 7, 7, 8, 8,
    7, 7, 7, 7,
]

@dataclass()
class DataPartitions:
    """
    >>> value_length    = 2
    >>> data_length     = 256 * 8 * value_length
    >>> test_data       = ConstBitStream(value_at_position(512*8, length=data_length, seed=2**31))
    >>> partitions_data = DataPartitions(data=test_data, total_partitions=0, value_length=2)
    >>> partitions_data.total_partitions
    1
    >>> len(partitions_data.partition_values) == partitions_data.total_partitions
    True
    >>> len(partitions_data.partition_values[0])
    256
    >>> partitions_data.min_values
    256
    >>> partitions_data.max_values
    256
    >>> partitions_data.capacity_overflow
    False
    >>> partitions_data.data_tail_length
    0
    >>> partitions_data.data_tail
    ConstBitStream('')
    >>> len(partitions_data.data_tail)
    0
    >>> partitions_data.agregated_counts
    CustomCounter({256: 1})
    >>> data_length     = (256 * 8 * value_length) - 8
    >>> test_data       = ConstBitStream(value_at_position(512*8, length=data_length, seed=2**31))
    >>> partitions_data = DataPartitions(data=test_data, total_partitions=1, value_length=2)
    >>> partitions_data.data_tail_length
    8
    >>> partitions_data.data_tail
    ConstBitStream('0x45')
    >>> len(partitions_data.data_tail)
    8
    >>> partitions_data.min_values
    255
    >>> partitions_data.max_values
    255
    """
    min_values         : int
    max_values         : int
    partition_values   : Dict[int, Set[str]]
    data_values        : Dict[int, str]
    partition_counters : Counter
    total_partitions   : int
    agregated_counts   : Counter
    data_length        : int
    data_tail_length   : int
    data               : ConstBitStream
    data_tail          : ConstBitStream
    value_length       : int
    capacity_overflow  : bool

    def __init__(self, data: ConstBitStream, total_partitions: int, value_length: int=2):
        if (total_partitions == 0):
            total_partitions = 1
        self.partition_counters = defaultdict(Counter)
        self.partition_values   = defaultdict(set)
        self.agregated_counts   = Counter()
        self.data_values        = dict()
        self.value_length       = value_length
        self.total_partitions   = total_partitions
        self.data               = data[:]
        self.data_length        = len(self.data)
        # max values per each partition
        self.max_values         = (2 ** (8 * (self.value_length - 1)))
        # True, when number of partitions is too small to all allocate data values
        self.capacity_overflow  = False
        
        # cut "data tail" (last data bytes that shorter than value length)
        self.data_tail_length = len(self.data) % (self.value_length * 8)
        self.data_tail        = self.data[len(self.data) - self.data_tail_length : len(self.data)]
        self.data             = self.data[0 : len(self.data) - self.data_tail_length]
        self.data_length      = len(self.data)

        # init min and max values
        max_partition_values = 0
        min_partition_values = self.max_values
        # split data to chunks, group chinks into partitions
        value_number = 0
        for value_bits in self.data.cut(8 * self.value_length):
            # partition depends on chunk number - this help to spread values evenly
            partition_id = value_number % self.total_partitions
            value        = value_bits.hex
            # save global value mapping
            self.data_values[value_number] = value
            # count occurences of each value in each partition
            self.partition_counters[partition_id].update({value: 1})
            self.partition_values[partition_id].add(value)
            # check total number of unique values in partition
            partition_values_count = len(self.partition_values[partition_id])
            # check partition capacity overflow
            if partition_values_count > self.max_values:
                self.capacity_overflow = True
                break
            # each value chunk must have unique number
            value_number += 1

        for partition_id in range(0, self.total_partitions):
            current_partition_values = len(self.partition_values[partition_id])
            # update maximum and minimum values in partition
            max_partition_values = max(max_partition_values, current_partition_values)
            min_partition_values = min(min_partition_values, current_partition_values)
            # count total number partitions for each number of values
            self.agregated_counts.update({current_partition_values: 1})
        
        # final min and max values of each partition
        self.min_values = min_partition_values
        self.max_values = max_partition_values

def define_min_partitions(data: ConstBitStream, min_partitions: int = None, value_length: int=2) -> DataPartitions:
    """
    Define and make data split having minimum number of partitions (each partition should have maximum number of values)

    >>> value_length    = 2
    >>> test_data       = ConstBitStream(value_at_position(512*8, length=(2**15)*8, seed=2**31))
    >>> partitions_data = define_min_partitions(data=test_data, min_partitions=62, value_length=value_length)
    >>> partitions_data.capacity_overflow
    False
    >>> partitions_data.total_partitions
    64
    >>> partitions_data.min_values
    253
    >>> partitions_data.max_values
    256
    >>> partitions_data.data_tail_length
    0
    >>> partitions_data.data_tail
    ConstBitStream('')
    >>> len(partitions_data.data_tail)
    0
    >>> partitions_data.agregated_counts
    CustomCounter({256: 32, 255: 23, 254: 8, 253: 1})
    """
    max_partition_values = (2 ** ((value_length - 1) * 8))
    
    # get default minimum partitions for current data
    if min_partitions is None:
        # cut "data tail" (last data bytes that shorter than value length)
        data_tail_length = len(data) % (value_length * 8)
        aligned_data     = data[0 : len(data) - data_tail_length]
        # get total number of unique values in split
        value_counts   = count_split_values(aligned_data, value_length * 8, "hex")
        min_partitions = len(value_counts) // max_partition_values
    # minimum number of partitions is 1 
    if min_partitions == 0:
        min_partitions = 1
    total_partitions = min_partitions

    # test different split options
    partitions = None
    while (True):
        partitions = DataPartitions(data=data, total_partitions=total_partitions, value_length=value_length)
        #print(f"data_length: {len(partitions.data)} ({len(data)}), total_partitions: {partitions.total_partitions}, overflow: {partitions.capacity_overflow}, max: {partitions.max_values}, min: {partitions.min_values}, {partitions.agregated_counts}")
        # check capacity overflow
        if (partitions.capacity_overflow == False) and (partitions.max_values <= max_partition_values):
            break
        # increase number of total partitions if partition capacity is not enough
        total_partitions += 1
    
    return partitions

@dataclass()
class PartitionScoreData:
    """
    Seed values distribution pattern

    >>> total_values = 256
    >>> score_data   = PartitionScoreData(total_values=total_values)
    >>> score_data.total_scores
    60
    >>> score_data.max_scores
    60
    >>> score_data.total_seeds
    60
    >>> score_data.max_seeds
    60
    >>> score_data.min_score
    1
    >>> score_data.max_score
    8
    >>> score_data.total_values == total_values
    True
    >>> sum(score_data.scores) == total_values
    True
    >>> score_data.score_counts
    CustomCounter({1: 8, 2: 8, 3: 8, 4: 8, 5: 8, 6: 8, 7: 8, 8: 4})
    >>> score_data.unique_scores
    {1, 2, 3, 4, 5, 6, 7, 8}
    >>> total_values = 255
    >>> score_data   = PartitionScoreData(total_values=total_values)
    >>> score_data.total_values == total_values
    True
    >>> sum(score_data.scores) == total_values
    True
    >>> score_data.score_counts
    CustomCounter({2: 8, 3: 8, 4: 8, 5: 8, 6: 8, 7: 8, 1: 7, 8: 4})
    >>> total_values = 249
    >>> score_data   = PartitionScoreData(total_values=total_values)
    >>> score_data.total_values == total_values
    True
    >>> sum(score_data.scores) == total_values
    True
    >>> score_data.score_counts
    CustomCounter({1: 8, 2: 8, 3: 8, 4: 8, 5: 8, 6: 8, 7: 7, 8: 4})
    >>> total_values = 248
    >>> score_data   = PartitionScoreData(total_values=total_values)
    >>> score_data.total_values == total_values
    True
    >>> sum(score_data.scores) == total_values
    True
    >>> score_data.score_counts
    CustomCounter({2: 8, 3: 8, 4: 8, 5: 8, 6: 8, 1: 7, 7: 7, 8: 4})
    """
    scores           : List[int]
    target_scores    : List[int]
    total_values     : int
    max_values       : int
    total_positions  : int
    max_positions    : int
    total_seeds      : int
    max_seeds        : int
    total_scores     : int
    max_scores       : int
    score_counts     : Counter
    max_score_counts : Counter
    unique_scores    : Set[int]
    min_score        : int
    max_score        : int
    value_length     : int

    def __init__(self, total_values: int, value_length: int=2):
        self.value_length    = value_length
        self.max_values      = (2 ** ((self.value_length - 1) * 8))
        if (total_values > self.max_values):
            raise Exception(f"Max values for current length ({value_length}) is {self.max_values}. Input {total_values}  is too large.")
        # score items will be added to the result list in order defined in this list
        self.target_scores   = DEFAULT_SCORE_DISTRIBUTION_1BYTE.copy()
        # define max number items and actual (used) number of items
        self.total_values    = total_values
        self.total_positions = self.total_values
        self.max_positions   = self.max_values
        self.total_seeds     = 0
        self.max_seeds       = len(self.target_scores)
        self.total_scores    = 0
        self.max_scores      = len(self.target_scores)
        # result score list
        self.scores           = list()
        self.score_counts     = Counter()
        self.max_score_counts = Counter(self.target_scores)
        self.unique_scores    = set()
    
        # init remaining/allocated value counters
        allocated_items = 0
        remaining_items = total_values
        # starting allocation
        while (self.total_values > allocated_items):
            #allocated_score = max(target_scores)
            allocated_score = self.target_scores[0]
            # check size of next allocation: make it fit to number of remaining non-allocated elements
            if (allocated_score > remaining_items):
                for _ in range(0, allocated_score):
                    self.scores.remove(1)
                    self.target_scores.append(1)
                    self.score_counts.update({ 1: -1 })
                    allocated_items = allocated_items - 1
                for _ in range(0, remaining_items):
                    self.scores.append(1)
                    self.target_scores.remove(1)
                    self.score_counts.update({ 1: 1 })
                    allocated_items = allocated_items + 1
            # tracking total number of allocated/remaining values
            remaining_items = remaining_items - allocated_score
            allocated_items = allocated_items + allocated_score
            # move score value from allocation queue to final score list
            self.target_scores.remove(allocated_score)
            self.scores.append(allocated_score)
            self.unique_scores.add(allocated_score)
            self.score_counts.update({ allocated_score: 1 })

        #print(f"{self.score_counts}, {sum(self.scores)}")
        # count final number of scores in allocation
        self.total_scores = len(self.scores)
        self.total_seeds  = self.total_scores
        # save max and min scores
        self.min_score = min(self.scores)
        self.max_score = max(self.scores)

def count_seed_score(seed: int, max_score: int, target_values: set, target_positions: Union[set, Iterable], value_length: int=2) -> int:
    """
    Count score of a given seed for a given set of target values at available positions

    >>> seed             = 0
    >>> max_score        = 8
    >>> value_length     = 2
    >>> test_mapping     = {0: 'c44b', 1: 'dff4', 2: '074e', 4: 'e12e', 8: 'c9f4', 16: '73b2', 32: 'dc4d', 64: 'ea3c'}
    >>> target_values    = set(test_mapping.values())
    >>> target_positions = set(test_mapping.keys())
    >>> score            = count_seed_score(seed, max_score, target_values, target_positions, value_length)
    >>> score
    8
    >>> target_values.remove('c44b')
    >>> score = count_seed_score(seed, max_score, target_values, target_positions, value_length)
    >>> score
    7
    >>> target_positions.remove(0)
    >>> score = count_seed_score(seed, max_score, target_values, target_positions, value_length)
    >>> score
    7
    >>> target_values.remove('dff4')
    >>> score = count_seed_score(seed, max_score, target_values, target_positions, value_length)
    >>> score
    6
    >>> target_positions.remove(2)
    >>> score = count_seed_score(seed, max_score, target_values, target_positions, value_length)
    >>> score
    5
    """
    score          = 0
    located_values = set()
    # scan each available position
    for position in target_positions:
        #value = bytes_at_position(position=position*8, length=value_length*8, seed=seed, use_bytearray=False).hex()
        value = bytes_at_position(position=position*value_length*8, length=value_length*8, seed=seed, use_bytearray=False).hex()
        if value not in target_values:
            continue
        if value in located_values:
            continue
        score = score + 1
        located_values.add(value)
        # return target score even if current seed score is greater
        if (score >= max_score):
            return score
    # final score
    return score

SeedData = namedtuple('SeedData', [
    'partition_id',
    'seed',
    'score',
    'target_values',
    'target_positions',
    'located_values',
    'located_positions',
    'value_positions',
    'position_values',
    'value_length',
])

SeedMapping = namedtuple('SeedMapping', [
    'seed',
    'score',
    # Dict[position, value]
    'mapping',
])

def score_seed_data(partition_id: int, seed: int, max_score: int, target_values: Set[str], 
        target_positions: Union[set, Iterable], value_length: int=2) -> SeedData:
    """
    Calculate score of current seed for a given partition

    >>> partition_id     = 0
    >>> seed             = 0
    >>> max_score        = 8
    >>> value_length     = 2
    >>> test_mapping     = {0: 'c44b', 1: 'dff4', 2: '074e', 4: 'e12e', 8: 'c9f4', 16: '73b2', 32: 'dc4d', 64: 'ea3c'}
    >>> target_values    = set(test_mapping.values())
    >>> target_positions = set(test_mapping.keys())
    >>> seed_data = score_seed_data(partition_id, seed, max_score, target_values, target_positions, value_length)
    >>> seed_data.partition_id == partition_id
    True
    >>> seed_data.seed == seed
    True
    >>> seed_data.score
    8
    >>> len(seed_data.target_positions)
    8
    >>> len(seed_data.target_values)
    8
    >>> sorted(seed_data.target_values)
    ['074e', '73b2', 'c44b', 'c9f4', 'dc4d', 'dff4', 'e12e', 'ea3c']
    >>> sorted(seed_data.target_positions)
    [0, 1, 2, 4, 8, 16, 32, 64]
    >>> sorted(seed_data.located_values)
    ['074e', '73b2', 'c44b', 'c9f4', 'dc4d', 'dff4', 'e12e', 'ea3c']
    >>> sorted(seed_data.located_positions)
    [0, 1, 2, 4, 8, 16, 32, 64]
    >>> seed_data.value_positions
    {'c44b': 0, 'dff4': 1, '074e': 2, 'dc4d': 32, 'e12e': 4, 'ea3c': 64, 'c9f4': 8, '73b2': 16}
    >>> seed_data.position_values
    {0: 'c44b', 1: 'dff4', 2: '074e', 32: 'dc4d', 4: 'e12e', 64: 'ea3c', 8: 'c9f4', 16: '73b2'}
    >>> target_positions.remove(0)
    >>> seed_data = score_seed_data(partition_id, seed, max_score, target_values, target_positions, value_length)
    >>> seed_data.score
    7
    >>> len(seed_data.target_positions)
    7
    >>> len(seed_data.target_values)
    8
    >>> sorted(seed_data.target_values)
    ['074e', '73b2', 'c44b', 'c9f4', 'dc4d', 'dff4', 'e12e', 'ea3c']
    >>> sorted(seed_data.target_positions)
    [1, 2, 4, 8, 16, 32, 64]
    >>> sorted(seed_data.located_values)
    ['074e', '73b2', 'c9f4', 'dc4d', 'dff4', 'e12e', 'ea3c']
    >>> sorted(seed_data.located_positions)
    [1, 2, 4, 8, 16, 32, 64]
    >>> seed_data.value_positions
    {'dff4': 1, '074e': 2, 'dc4d': 32, 'e12e': 4, 'ea3c': 64, 'c9f4': 8, '73b2': 16}
    >>> seed_data.position_values
    {1: 'dff4', 2: '074e', 32: 'dc4d', 4: 'e12e', 64: 'ea3c', 8: 'c9f4', 16: '73b2'}
    >>> target_values.remove('dff4')
    >>> seed_data = score_seed_data(partition_id, seed, max_score, target_values, target_positions, value_length)
    >>> seed_data.score
    6
    >>> len(seed_data.target_positions)
    7
    >>> len(seed_data.target_values)
    7
    >>> sorted(seed_data.target_values)
    ['074e', '73b2', 'c44b', 'c9f4', 'dc4d', 'e12e', 'ea3c']
    >>> sorted(seed_data.target_positions)
    [1, 2, 4, 8, 16, 32, 64]
    >>> sorted(seed_data.located_values)
    ['074e', '73b2', 'c9f4', 'dc4d', 'e12e', 'ea3c']
    >>> sorted(seed_data.located_positions)
    [2, 4, 8, 16, 32, 64]
    >>> seed_data.value_positions
    {'074e': 2, 'dc4d': 32, 'e12e': 4, 'ea3c': 64, 'c9f4': 8, '73b2': 16}
    >>> seed_data.position_values
    {2: '074e', 32: 'dc4d', 4: 'e12e', 64: 'ea3c', 8: 'c9f4', 16: '73b2'}
    """
    located_values    = set()
    located_positions = set()
    value_positions   = dict()
    position_values   = dict()
    score             = 0

    if (len(target_values) == 0):
        raise Exception(f"No target_values to search")
    if (len(target_positions) == 0):
        raise Exception(f"No target target_positions to search")
    if (max_score == 0):
        raise Exception(f"Given max_score=0")
    
    for position in target_positions:
        #value = bytes_at_position(position=position*8, length=value_length*8, seed=seed, use_bytearray=False).hex()
        value = bytes_at_position(position=position*value_length*8, length=value_length*8, seed=seed, use_bytearray=False).hex()
        # value must be one of the partition values
        if value not in target_values:
            continue
        # each partition value can be included only once
        if value in located_values:
            continue
        # update seed score
        score = score + 1
        # collect located values and positions
        located_values.add(value)
        located_positions.add(position)
        # save value/position mapping
        value_positions[value]    = position
        position_values[position] = value
        # target score reached - stop scanning
        if (score >= max_score):
            break
    
    return SeedData(
        partition_id,
        seed,
        score,
        target_values,
        target_positions,
        located_values,
        located_positions,
        value_positions,
        position_values,
        value_length,
    )

def update_score_seeds(score_seeds: Dict[int, set], partition_values: set, partition_positions: set, value_length: int=2) -> Dict[int, set]:
    """
    Update score of each seed for a given partition
    """
    updated_score_seeds = defaultdict(set)
    scores              = sorted(set(score_seeds.keys()))
    # update score of each seed
    for score in scores:
        seeds = score_seeds[score]
        for seed in seeds:
            #seed_data = score_seed_mapping(partition_id, seed, score, partition_values, partition_positions, {}, value_length)
            seed_score = count_seed_score(
                seed             = seed, 
                target_score     = score, 
                target_values    = partition_values, 
                target_positions = partition_positions, 
                value_length     = value_length
            )
            # TODO: exclude duplicate 1-score items
            # TODO: prefer previous seeds
            if (seed_score > 0):
                updated_score_seeds[seed_score].add(seed)
    # return filtered seeds with updated score
    return updated_score_seeds

def update_partition_scores(score_seeds: Dict[int, set]) -> Counter:
    """
    Update number of seeds for each score for a single partition
    """
    partition_scores = Counter()
    scores           = set(score_seeds.keys())
    for score in scores:
        partition_scores.update({ score: len(score_seeds[score]) })
    return partition_scores

@dataclass()
class NestedSeedNode:
    """
    Partition seeds score tree: builds from long to short scores, appending each next score to previous
    This allow us to search ALL possible position/value allocations diring single seed range scan
    Tree build several different branches until the longest branch, that covers all partition values
    will be found
    """
    # last seed number in partition
    max_seed_number   : int
    # max number of items for each score in partition
    max_score_numbers : Dict[int, int]
    # prev level
    prev_score        : int
    prev_score_number : int
    prev_seed_number  : int
    prev_seed         : int
    # current level
    score            : int
    score_number     : int
    seed_number      : int
    seed             : int
    # current level mapping
    seed_positions   : Set[int]
    seed_values      : Set[str]
    seed_mapping     : Dict[int, str]
    # next level
    next_score        : int
    next_score_number : int
    next_seed_number  : int
    next_seeds        : Dict[int, NestedSeedNode]
    # current level target values/positions (excluding all previous levels value/positions)
    # current seed value/position pairs also excluded from this lists
    target_positions : Set[int]
    target_values    : Set[str]
    # current level mapping (includes all mapping above and current seed values/positions)
    current_mapping  : Dict[int, str]
    is_root          : bool = False

    def __init__(self, 
            score: int, score_number: int, seed_number: int, seed_data: SeedData,
            prev_score: int, prev_score_number: int, prev_seed_number: int, prev_seed: int,
            max_seed_number: int, max_score_numbers: Dict[int, int],
            parent_values: Set[str], parent_positions: Set[int], parent_mapping: Dict[int, str]
        ):
        # partition options
        self.max_seed_number   = max_seed_number
        self.max_score_numbers = max_score_numbers
        # set current level data
        self.score        = score
        self.score_number = score_number 
        self.seed_number  = seed_number 
        self.seed         = seed_data.seed
        # set current level mapping
        self.seed_values    = seed_data.located_values
        self.seed_positions = seed_data.located_positions
        self.seed_mapping   = seed_data.position_values
        
        # set target value/position mapping
        self.current_mapping = parent_mapping.copy()
        # remove current seed values from search (because they already allocated)
        self.target_values = parent_values.copy()
        for seed_value in self.seed_values:
            self.target_values.remove(seed_value)
        # remove current seed positions from search (because they already used)
        self.target_positions = parent_positions.copy()
        for seed_position in self.seed_positions:
            self.target_positions.remove(seed_position)
        # add seed mapping to current mapping
        for position, seed in self.seed_mapping.items():
            self.current_mapping[position] = seed
        
        # set prev node values
        if (self.seed_number is None):
            # root node check
            self.prev_score        = None
            self.prev_score_number = None
            self.prev_seed_number  = None
            self.prev_seed         = None
            self.is_root           = True
        else:
            self.prev_score        = prev_score
            self.prev_score_number = prev_score_number
            self.prev_seed_number  = prev_seed_number
            self.prev_seed         = prev_seed
            self.is_root           = False
        
        # set next node values
        if (self.seed_number == self.max_seed_number):
            # last seed node check
            self.next_score        = None
            self.next_score_number = None
            self.next_seed_number  = None
            self.next_seeds        = None
        else:
            if (self.max_score_numbers[self.score] == self.score_number):
                next_score        = self.score + 1
                next_score_number = 0
            else:
                next_score        = self.score
                next_score_number = self.score_number + 1
            # root not initialization
            if (self.is_root is True):
                next_seed_number  = 0
                next_score_number = 0
            else:
                next_seed_number = (self.seed_number + 1)
            self.next_score        = next_score
            self.next_score_number = next_score_number
            self.next_seed_number  = next_seed_number
            self.next_seeds        = dict()
    
    def check_next_seed(self, seed_data: SeedData) -> bool:
        return True
    
    def apend_next_seed(self, seed_data: SeedData):
        if (seed_data.score != self.score):
            raise Exception(f"Incorrect seed score: {seed_data.score} (required {self.score})")
        if (self.next_seeds is None):
            raise Exception(f"Unable to add next seed to {self.seed} [seed_number={self.seed_number}]: maximum seed number reached")
        
        # check that next seed data is correct before appending it to the tree
        self.check_next_seed(seed_data=seed_data)
        
        # create next score seed from current
        self.next_seeds[seed_data.seed] = NestedSeedNode(
            score             = seed_data.score,
            score_number      = self.next_score_number,
            seed_number       = self.next_seed_number,
            seed_data         = seed_data,
            prev_score        = self.score,
            prev_score_number = self.score_number,
            prev_seed_number  = self.seed_number,
            prev_seed         = self.seed,
            max_seed_number   = self.max_seed_number,
            max_score_numbers = self.max_score_numbers,
            parent_values     = self.target_values,
            parent_positions  = self.target_positions,
            parent_mapping    = self.current_mapping,
        )

@dataclass()
class ScoreLayerMapping:
    """
    Mapping for several seeds with same score

    #>>> score_mapping = PartitionScoreMapping()
    #>>> score_mapping
    #ScoreMapping()
    #>>> score_mapping.asdict()
    #{'value_length': 2, 'values': [38, 42]}
    """
    # partition, witch values will to be allocated using given score rules
    partition_id         : int
    # score value (number of partition values located inside single seed)
    target_score         : int
    # total number of items (seeds) for allocation in this layer
    total_scores         : int
    # maximum order number available for each partition score
    max_score_numbers    : Dict[int, int]

    # all partition seeds with this score
    score_seeds          : Set[int]
    # mapping for each seed (seed.position.value)
    score_seed_mappings  : Dict[int, Dict[int, str]]
    
    # all partition values that not allocated yet
    partition_values     : Set[str]
    # all positions, currently available for value allocation
    partition_positions  : Set[int]
    # global seed mapping (not allocated)
    seed_values          : Dict[int, Set[str]]
    seed_positions       : Dict[int, Set[int]]
    # global value mapping (not allocated)
    value_seeds          : Dict[str, Set[int]]
    value_positions      : Dict[str, Set[str]]
    # global position mapping (not allocated)
    position_seeds       : Dict[int, Set[int]]
    position_values      : Dict[int, Set[str]]
    # unallocated seed value and position usage counters
    seed_value_counts    : Counter
    seed_position_counts : Counter
    # layer seeds, positions and values, after allocation (order of seeds is important)
    mapped_seeds         : Tuple[int]
    mapped_values        : Set[str]
    mapped_positions     : Set[int]
    # score mapping is open for appending seed values
    is_open              : bool
    # root score mapping group: first open score by default, no "entry points" (parent trees) needed to add value here
    is_root              : bool

    # prev/next score links
    prev_score           : int
    next_score           : int
    # seed chains from parent score mapping, the only available places for adding new seeds
    root_seeds           : Dict[int, NestedSeedNode]
    # original value length (default to 2)
    value_length         : int = 2

    def __init__(self, partition_id: int, score: int, total_scores: int, max_score_numbers: int,
            partition_values: Set[str], partition_positions: Set[int], 
            is_root: bool, is_open: bool, value_length: int=2):
        # set basic options
        self.is_open      = is_open
        self.is_root      = is_root
        self.value_length = value_length
        self.partition_id = partition_id
        self.target_score = score
        self.total_scores = total_scores
        
        # init scores list
        self.max_score_numbers   = max_score_numbers
        # set values/positions available for mapping
        self.partition_values    = partition_values.copy()
        self.partition_positions = partition_positions.copy()
        self.score_seeds         = set()
        
        # init value/position counters
        self.seed_value_counts    = Counter()
        self.seed_position_counts = Counter()
        
        # init global mappings
        self.mapped_seeds     = tuple()
        self.mapped_values    = set()
        self.mapped_positions = set()
        
        # init item mappings
        self.seed_values     = defaultdict(set)
        self.seed_positions  = defaultdict(set)
        self.value_seeds     = defaultdict(set)
        self.value_positions = defaultdict(set)
        self.position_seeds  = defaultdict(set)
        self.position_values = defaultdict(set)

    def append_seed_data(self, seed_data: SeedData):
        """
        Add new seed value, that can be used to allocate values with given score
        Updates all internal mappings and counters (they will be used to choose best seed combination)
        This method only adds new seed into collection, but do not changes value/position mapping
        """
        if (seed_data.score != self.target_score):
            raise Exception(f"Incorrect seed_data.score={seed_data.score} (expected seed_data.score={self.target_score})")
        # load new seed
        seed = seed_data.seed

        # update partition seed list
        self.score_seeds.add(seed)
        
        # update seed position mapping
        for position in seed_data.located_positions:
            # check position before import
            if (position not in self.partition_positions):
                raise Exception(f"seed_position={position} not found in self.partition_positions={self.partition_positions}")
            self.seed_positions[seed].add(position)
            self.position_seeds[position].add(seed)
            self.position_values[position].add(seed_data.position_values[position])
            self.seed_position_counts.update({ position: 1 })
        
        # update seed value mapping
        for value in seed_data.located_values:
            # check value before import
            if (value not in self.partition_values):
                raise Exception(f"seed_value={value} not found in self.partition_values={self.partition_values}")
            self.seed_values[seed].add(value)
            self.value_seeds[value].add(seed)
            self.value_positions[value].add(seed_data.value_positions[value])
            self.seed_value_counts.update({ value: 1 })
    
    def add_chain_root(self, seed_node: NestedSeedNode):
        """
        Add new seed chain by creating new root node for this score
        """
        if (seed_node.score != self.target_score):
            raise Exception(f"Incorrect seed_node.score: {seed_node.score} (required {self.target_score})")
        
        # add new root to score mapping
        self.root_seeds[seed_node.seed] = seed_node
        
        # mark score mapping as open for new values
        if (self.is_open is False):
            self.is_open = True
        
        return {
            'score'     : self.target_score,
            'root_seeds': self.root_seeds,
            'is_open'   : self.is_open,
        }
    
    def set_seed_mapping(self, seeds: Tuple[int]):
        """
        Save value/position allocation, mark given seed layer as "mapped", mark all seed values and
        positions as mapped, exclude seed values and positions from available/unallocated partition items
        """
        pass

    def update_score_seeds(self, partition_values: Set[str], partition_positions: Set[int]):
        """
        Check all seeds of this score, update score of each seed
        """
        pass

    def build_seed_chain(self):
        """
        Create and apply all available seed combinations to find the longest chain
        Target chain length = self.total_scores
        """
        pass

@dataclass()
class PartitionSeedTree:
    """
    Sequence of seeds that covers all partition values
    """
    root_node     : NestedSeedNode
    total_seeds   : int
    max_seeds     : int
    length_counts : Counter

    def __init__(self, partition_id: int, root_values: Set[str], root_positions: Set[int], root_score: int, 
            max_seed_number: int, max_score_numbers: Dict[int, int]):
        # create root node (seed=None)
        root_seed_data = SeedData(
            partition_id      = partition_id,
            seed              = None,
            score             = root_score,
            target_values     = root_values,
            target_positions  = root_positions,
            located_values    = set(),
            located_positions = set(),
            value_positions   = dict(),
            position_values   = dict(),
        )
        # init root node
        # TODO: no nodes without seed, move start values, positions and mapping into tree options, use 1 node for each seed
        self.root_node = NestedSeedNode(
            score             =root_score,
            score_number      = None,
            seed_number       = None,
            seed_data         = root_seed_data,
            prev_score        = None,
            prev_score_number = None,
            prev_seed_number  = None,
            prev_seed         = None,
            max_seed_number   = max_seed_number,
            max_score_numbers = max_score_numbers,
            parent_values     = root_values,
            parent_positions  = root_positions,
            parent_mapping    = dict(),
        )
        # init counters
        self.total_seeds   = 0
        self.max_seeds     = max_seed_number + 1
        self.length_counts = Counter()

@dataclass()
class PartitionItem:
    """
    Single data partition

    >>> value_length    = 2
    >>> workbookDir     = os.getcwd()
    >>> file_path       = f"{workbookDir}/src/data/image-36kb.jpg"
    >>> test_data       = ConstBitStream(filename=file_path)
    >>> data_partitions = define_min_partitions(data=test_data, min_partitions=70, value_length=value_length)
    >>> partition_id    = 0
    >>> seed_range      = range(0, 2**10)
    >>> prev_seeds      = set()
    >>> min_seed_score  = 2
    >>> partition_item  = PartitionItem(partition_id, data_partitions, seed_range, prev_seeds, value_length, min_seed_score)
    >>> len(partition_item.values)
    251
    >>> partition_item.total_values
    251
    >>> len(partition_item.positions)
    251
    >>> partition_item.total_positions
    251
    >>> sum(partition_item.target_scores)
    251
    >>> sorted(partition_item.target_scores)
    [1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4, 4, 4, 4, 5, 5, 5, 5, 5, 5, 5, 5, 6, 6, 6, 6, 6, 6, 6, 6, 7, 7, 7, 7, 7, 7, 7, 7, 8, 8, 8, 8]
    >>> sum(partition_item.target_scores) == partition_item.total_values
    True
    >>> min(partition_item.positions)
    0
    >>> max(partition_item.positions)
    250
    >>> partition_item.target_score_layers
    [8, 7, 6, 5, 4, 3, 2, 1]
    >>> partition_item.open_score_layers
    [8]
    >>> partition_item.collect_partition_seeds()
    """
    partition_id    : int
    # inital partition values, waiting for allocation
    values          : Set[str]
    total_values    : int
    # empty partition positions, waiting to be filled with one of partition values 
    positions       : Set[int]
    total_positions : int
    # default seed range for search
    seed_range      : range
    # last scanned seed from partition seed range
    last_range_seed : int
    
    # score mapping config (common for all scores)
    score_data           : PartitionScoreData
    # score mapping data (one mapping object for each score size: 1-8)
    score_mapping        : Dict[int, ScoreLayerMapping]
    # all unallocated score layers
    target_score_layers  : List[int]
    # minimal score layer opened for value allocation
    open_score_layers    : List[int]
    # remaining unallocated target scores
    target_scores        : List[int]
    # currently collected seeds (grouped by score)
    score_seeds          : Dict[int, Set[int]]
    # total number of collected seeds (grouped by score)
    score_seed_counts    : Counter
    # total number of collected seeds
    total_seeds          : int
    
    # seeds, used in previous partitions (not scored, not mapped): 
    # one seed used in several partitions will minify number of total seeds used (useful when you have many small partitions)
    prev_score_seeds       : Dict[int, Set[int]]
    prev_score_seed_counts : Counter
    prev_seeds             : Set[int]
    prev_seed_count        : int

    # partition seed chains: list of all possible combinations of seeds for partition covering (order of seed is important)
    seed_chains            : Dict[int, NestedSeedNode]
    # partition mapping
    partition_mapping      : Dict[int, str]
    
    # original value length (default to 2)
    value_length    : int = 2
    # minimum score, required for new seed
    min_seed_score  : int = 2

    def __init__(self, partition_id: int, data_partitions: DataPartitions, seed_range: range(0, 2**28), 
            prev_seeds: Set[int]=set(), value_length: int=2, min_seed_score: int=2):
        # set partition options
        self.partition_id   = partition_id
        self.value_length   = value_length
        self.min_seed_score = min_seed_score
        # set partition values and partition positions
        self.values          = data_partitions.partition_values[partition_id].copy()
        self.total_values    = len(self.values)
        # set position/value counters
        self.positions       = set(range(0, self.total_values))
        self.total_positions = len(self.positions)
        # each partition can have a dedicated seed range in order to perform parallel search
        self.seed_range      = seed_range
        self.last_range_seed = min(self.seed_range)
        
        # init score data
        self.score_data          = PartitionScoreData(self.total_values, value_length=self.value_length)
        self.target_scores       = self.score_data.scores.copy()
        self.target_score_layers = sorted(self.score_data.unique_scores, reverse=True)
        self.open_score_layers   = [self.score_data.max_score]
        self.score_seeds         = defaultdict(set)
        self.score_seed_counts   = Counter()
        self.total_seeds         = 0
        
        # collect prev seeds
        self.prev_score_seed_counts = Counter()
        self.prev_score_seeds       = defaultdict(set)
        self.prev_seeds             = set()
        self.prev_seed_count        = 0
        self.collect_prev_seeds(prev_seeds)

        # init chain tree and final mapping
        self.seed_chains       = dict()
        self.partition_mapping = dict()
        
        # init score mapping
        self.score_mapping = dict()
        for score in sorted(self.score_data.unique_scores):
            # define and mark root nodes
            if (score == self.score_data.max_score):
                is_root = True
                is_open = True
            else:
                is_root = False
                is_open = False
            
            # max available number for each score item
            max_score_numbers = dict()
            for score, score_count in self.score_data.score_counts.items():
                max_score_numbers[score] = (score_count - 1)
            
            # init mapping of each score
            self.score_mapping[score] = ScoreLayerMapping(
                partition_id        = self.partition_id,
                score               = score,
                total_scores        = self.score_data.score_counts[score],
                max_score_numbers   = max_score_numbers,
                partition_values    = self.values,
                partition_positions = self.positions,
                is_root             = is_root,
                is_open             = is_open,
                value_length        = self.value_length,
            )
        
    def collect_prev_seeds(self, prev_seeds: Set[int]):
        for seed in sorted(prev_seeds):
            # scan seed positions
            seed_data = score_seed_data(self.partition_id, seed, self.score_data.max_score, self.values, self.positions)
            if (seed_data.score == 0):
                continue
            # save seed data to current seed list + update score mapping
            self.add_seed_data(seed_data)
            # store previously used seeds separatley
            self.prev_score_seeds[seed_data.score].add(seed_data.seed)
            self.prev_score_seed_counts.update({ seed_data.score: 1 })
            self.prev_seeds.add(seed_data.seed)
            self.prev_seed_count += 1
    
    def get_priority_seed(self) -> int:
        return None

    def add_seed_data(self, seed_data: SeedData):
        # update seeds, values and positions
        if (seed_data.score == 0):
            return
        # update score seeds
        self.score_seeds[seed_data.score].add(seed_data.seed)
        self.score_seed_counts.update({ seed_data.score: 1 })
        self.total_seeds += 1
        # update seed score mapping
        # TODO: update score mapping only when seed score is unlocked
        #self.score_mapping[seed_data.score].append_seed_data(seed_data)
    
    def update_score_mapping(self, seed_data: SeedData):
        self.score_mapping[seed_data.score].append_seed_data(seed_data)

    def add_score_seed(self, seed_data: SeedData):
        score_mapping = self.score_mapping[seed_data.score]
        # add seed to seed chains
        if (score_mapping.is_root and score_mapping.is_open):
            score_number      = 0
            seed_number       = 0
            prev_score        = None
            prev_score_number = None
            prev_seed_number  = None
            prev_seed         = None
            # max seed/score numbers
            max_seed_number   = (self.score_data.total_seeds - 1)
            max_score_numbers = dict()
            for score, score_count in self.score_data.score_counts.items():
                max_score_numbers[score] = (score_count - 1)
            # mapping
            parent_values    = self.values
            parent_positions = self.positions
            parent_mapping   = self.partition_mapping
            # create root seed node
            root_node = NestedSeedNode(
                score             = seed_data.score,
                score_number      = score_number,
                seed_number       = seed_number,
                seed_data         = seed_data,
                prev_score        = prev_score,
                prev_score_number = prev_score_number,
                prev_seed_number  = prev_seed_number,
                prev_seed         = prev_seed,
                max_seed_number   = max_seed_number,
                max_score_numbers = max_score_numbers,
                parent_values     = parent_values,
                parent_positions  = parent_positions,
                parent_mapping    = parent_mapping,
            )
            # start new seed chain
            self.seed_chains[seed_data.seed] = root_node
            print(f"seed: {root_node.seed}, node: {root_node.current_mapping}, max_score_numbers: {root_node.max_score_numbers}")
            # add new seed to the root
            #score_mapping.add_chain_root(seed_node)
            
    def process_partition_seed(self, seed: int) -> SeedData:
        # scan all positions for this seed
        seed_data = score_seed_data(
            partition_id     = self.partition_id, 
            seed             = seed, 
            max_score        = self.score_data.max_score, 
            target_values    = self.values,
            target_positions = self.positions,
            value_length     = self.value_length
        )
        # check seed score: required minimum for new value
        if (seed_data.score >= self.min_seed_score):
            # add seed data to partition mapping
            self.add_seed_data(seed_data)
        else:
            return seed_data

        # check seed score: is it an open score layer?
        if (seed_data.score in self.open_score_layers):
            #print(f"(seed_data.score in self.open_score_layers)")
            # (updating score mapping only when seed score is unlocked)
            self.update_score_mapping(seed_data)
            # add new seed to score
            self.add_score_seed(seed_data)
        
        return seed_data

    def collect_partition_seeds(self):
        seed_progress = tqdm_notebook(self.seed_range, mininterval=1, smoothing=0)
        seed_progress.set_description_str(f"partition_id={self.partition_id}", refresh=False)
        # scan seed range
        max_seed = max(self.seed_range)
        
        while (self.last_range_seed < max_seed):
            # check already used seeds first
            priority_seed = self.get_priority_seed()
            if (priority_seed is not None):
                # try to use old seeds twice before adding new seeds
                seed = priority_seed
            else:
                # no priority seed available: continue to scan seed range
                seed                 = self.last_range_seed
                self.last_range_seed += 1
            # process single seed
            seed_data = self.process_partition_seed(seed)
            
            # skip progress display if no new seeds added
            if (seed_data.score < self.min_seed_score):
                continue
            #print(f"{seed}: seeds: {self.total_seeds} positions: {seed_data.located_positions}, values: {seed_data.located_values}")
            # display progress
            #if (self.total_seeds % 2 == 1):
            if (self.total_seeds % 500 == 1):
                seed_progress.n = seed
                seed_progress.set_postfix({
                    'total_seeds'   : f"{self.total_seeds}",
                    'values'        : f"{len(self.values)}",
                    'positions'     : f"{len(self.positions)}",
                    'scores'        : f"{self.score_seed_counts.most_common()}",
                    'targets[max]'  : f"{sorted(self.target_scores, reverse=True)} ({len(self.target_scores)}) [{self.score_data.max_score}]",
                    'target_layers' : f"{self.target_score_layers} ",
                    'open_layers'   : f"{self.open_score_layers} ",
                    'prev_values'   : f"{len(self.prev_seeds)} [{self.prev_seed_count}]",
                    'seed_chains'   : f"({len(self.seed_chains)})" #{self.seed_chains}
                })    