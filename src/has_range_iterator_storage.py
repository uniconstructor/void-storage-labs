# https://stackoverflow.com/questions/38340808/recursive-typing-in-python-3-5
from __future__ import annotations
# https://realpython.com/lru-cache-python/
from functools import lru_cache, cached_property

# https://github.com/Textualize/rich
from rich import print
# https://rich.readthedocs.io/en/latest/pretty.html
from rich.pretty import pprint
from rich.progress import track, Progress, \
    SpinnerColumn, BarColumn, TextColumn, TimeElapsedColumn, MofNCompleteColumn, \
    TaskProgressColumn, TimeRemainingColumn, TransferSpeedColumn

# https://realpython.com/python-data-classes/
# https://realpython.com/python-data-classes/#advanced-default-values
from dataclasses import dataclass, make_dataclass

# https://github.com/Cyan4973/xxHash
# https://github.com/ifduyue/python-xxhash
import xxhash
# https://realpython.com/python-namedtuple/
from collections import defaultdict, namedtuple
from custom_counter import CustomCounter as Counter
# https://realpython.com/linked-lists-python/
from collections import deque
# https://docs.python.org/3/library/typing.html
# https://realpython.com/python-data-classes/#more-flexible-data-classes
from typing import List, Set, Dict, Tuple, Optional, Union
# https://www.geeksforgeeks.org/floor-ceil-function-python/?ref=lbp
import math
from bitarray import bitarray, bits2bytes, frozenbitarray, get_default_endian
from bitarray.util import ba2hex, hex2ba, ba2int, int2ba, ba2base, base2ba, \
    serialize, deserialize, vl_encode, vl_decode, make_endian
# https://docs.mongoengine.org/
from mongoengine import Document, QuerySet, LongField, IntField, \
    BinaryField, StringField, DictField, ListField, BooleanField, queryset_manager

from hash_range_iterator import HashIterator, bits_at_position

# different hash digest formats
BYTES_HASH_CALLBACK : bytes = xxhash.xxh32_digest
HEX_HASH_CALLBACK   : str   = xxhash.xxh32_hexdigest
INT_HASH_CALLBACK   : int   = xxhash.xxh32_intdigest
# хеш-функция используемая для порождения всего бесконечного хеш-пространства
HASH_CALLBACK       : bytes = BYTES_HASH_CALLBACK
# длина получаемого хеша по умолчанию, в битах (используется для всех расчетов)
HASH_DIGEST_BITS    : int   = 32
# длина получаемого хеша по умолчанию, в байтах
HASH_DIGEST_BYTES   : int   = HASH_DIGEST_BITS // 8
# хеш-пространство по умолчанию
DEFAULT_SEED        : int = 0

# минимальное количество значений сохраняемых в базу одним запросом при заполнении базы значений
DEFAULT_BATCH_SIZE  : int = 4096

class NumberValueQuerySet(QuerySet):

    def starts_from_value(self, value: bitarray) -> NumberValueQuerySet:
        """
        Найти значение начинающееся с указанных битов
        """
        min_length = len(value)
        if (min_length < 24):
            min_length = 24
        # ищем по младшим битам числа - через деление с остатком
        value   = ba2int(value, signed=False)
        divisor = 2 ** min_length
        return self.filter(value__mod=(divisor, value))
    
    def with_value(self, value: bitarray) -> NumberValueQuerySet:
        return self.filter(value=ba2int(value, signed=False))
    
    def with_prefix(self, prefix: bitarray) -> NumberValueQuerySet:
        return self.filter(prefix=ba2int(prefix, signed=False))
    
    def with_length(self, length: int) -> NumberValueQuerySet:
        return self.filter(length=length)
    
    def from_position(self, position: int) -> NumberValueQuerySet:
        return self.filter(position__gte=position)
    
    def needs_upgrade(self, prefix: bitarray, value: bitarray, position: int, length: int) -> Union[bool, tuple]:
        """
        Определить нужно ли обновлять значение, заменяя его на более длинное: позволяет найти и заменить
        старые значения, для которых уже заданы все возможные варианты более длинных хначений на новые
        сохранив при этом позицию значения и увеличив его длину до первого уникального значения
        """
        next_value_0 = bitarray(endian='little')
        next_value_1 = bitarray(endian='little')
        next_length  = length + 1
        # полностью копируем переданное значение, дополняя его нулями
        for bit_position in range(0, length):
            if (bit_position < len(value)):
                next_value_0[bit_position] = value[bit_position]
                next_value_1[bit_position] = value[bit_position]
            else:
                next_value_0[bit_position] = 0
                next_value_1[bit_position] = 0
        # получаем 2 возможных варианта более длинного значения дополняя их единицей или нулем на конце
        next_value_0.append(0)
        next_value_1.append(1)
        # проверяем значение с последним битом 0
        item_0 = self.with_prefix(prefix=prefix)\
            .with_length(length=next_length)\
            .from_position(position=position)\
            .with_value(value=next_value_0)\
            .order_by('position')\
            .limit(1)\
            .first()
        if (item_0 is None):
            return False
        # проверяем значение с последним битом 1
        item_1 = self.with_prefix(prefix=prefix)\
            .with_length(length=next_length)\
            .from_position(position=position)\
            .with_value(value=next_value_1)\
            .order_by('position')\
            .limit(1)\
            .first()
        if (item_1 is None):
            return False
        # если оба значения есть - то текущее можно заменить на более длинное потому что у нас уже есть 2
        # заменяющих его варианта, которые будут всегда выбраны вместо него, потому что они более длинные
        return (item_0, item_1)

    def has_saved_value(self, value: bitarray) -> bool:
        value_count = self.starts_from_value(value=value).limit(1).count(with_limit_and_skip=True)
        return (value_count > 0)
    
    def get_first_value(self, value: bitarray) -> Int64ValueS0:
        """
        Найти первое упоминание значения
        """
        item = self.starts_from_value(value=value).limit(1).order_by('position').first()
        if (item is None):
            return None
        return item
    
    def get_last_value(self, value: bitarray) -> Int64ValueS0:
        """
        Найти последнее упоминание значения
        """
        item = self.starts_from_value(value=value).limit(1).order_by('-position').first()
        if (item is None):
            return None
        return item
    
    def get_last_value_position(self, value: bitarray) -> int:
        """
        Найти позицию последнего известного упоминания значения
        """
        item = self.get_last_value(value=value)
        if (item is None):
            return None
        return item.position
    
    def get_first_value_position(self, value: bitarray) -> int:
        """
        Найти позицию первого известного упоминания значения
        """
        item = self.get_first_value(value=value)
        if (item is None):
            return None
        return item.position
    
    def get_last_prefix_position(self, prefix: bitarray) -> int:
        """
        Найти последнюю известную позицию префикса
        """
        item = self.with_prefix(prefix=prefix).order_by('-position').first()
        if (item is None):
            return None
        return item.position
    
    def get_first_prefix_position(self, prefix: bitarray) -> int:
        """
        Найти первую известную позицию префикса
        """
        item = self.with_prefix(prefix=prefix).order_by('position').first()
        if (item is None):
            return None
        return item.position
    
    def get_prefix_positions(self, prefix: bitarray) -> QuerySet:
        return self.with_prefix(prefix=prefix).order_by('position')
    
    def get_last_saved_position(self) -> int:
        """
        Найти последнюю сохраненную позицию при сканировании
        """
        item = self.filter().order_by('-position').limit(1).first()
        if (item is None):
            return None
        return item.position

class Int64ValueS0(Document):
    """
    Сохраненное значение: непрерывный участок бит произвольной длины из хеш-пространства
    """
    # номер бита от начала пространства хешей
    position      = LongField(primary_key=True)
    # префикс значения - всегда фиксированной длины, минимум 24 бита
    prefix        = IntField()
    # значение: количество бит начиная с указанной позиции (обновляется чтобы всегда содержать наиболее длинные значения)
    value         = LongField(unique_with='length')
    # длина значения в битах (обновляется чтобы всегда содержать наиболее длинные значения)
    length        = IntField(min_value=24, max_value=64)
    
    meta = {
        'queryset_class': NumberValueQuerySet,
        'shard_key': 'prefix',
        'indexes': [
            'prefix',
            'value',
            'length',
        ],
    }

class Int64PositionS0(Document):
    """
    Unique values
    """
    value    = LongField(min_value=0, primary_key=True)
    length   = IntField(min_value=16, max_value=64)
    position = LongField(min_value=0)
    meta = {
        'indexes': [
            'length',
            'position',
        ],
    }

    @queryset_manager
    def get_max_length(doc_cls, queryset: QuerySet) -> QuerySet:
        return queryset.limit(1).order_by('-length')
    
    @queryset_manager
    def get_min_length(doc_cls, queryset: QuerySet) -> QuerySet:
        return queryset.limit(1).order_by('length')
    
    @queryset_manager
    def get_max_position(doc_cls, queryset: QuerySet) -> QuerySet:
        return queryset.limit(1).order_by('-position')

class Int32ValueS0(Document):
    """
    Значения, маркированные 32-битным префиксом
    Префикс присутствует везде и сохраняется для каждой записи как обязательное поле
    Минимальный размер блока, на который делятся данные для этой модели - 32 бита
    """
    value    = LongField(primary_key=True)
    # длина значения в битах
    length   = IntField(min_value=1, max_value=64)
    # первые 32 бита значения с этой позиции. Префикс нужен для отсечения сохранения в базу простых лоя поиска значений
    # (они заполняют коллекцию целиком и после 500 млн записей становится невозможно искать по условию деления с остатком
    # при запросе записей по id в mongodb)
    prefix   = IntField()
    # пока вместо seed-параметра используем просто большой диапазон position, не разделяя пространства на независисые области
    # а производя весь поиск на одной оси - позиций (вместо комбинации position + seed как раньше)
    position = LongField()
    meta = {
        'indexes': [
            'prefix',
            'length',
            'position',
        ],
    }

class LengthBasedValue(Document):
    """
    Уникальное значение в пространстве хешей (64 бита для номера позиции)
    """
    id       = LongField(min_value=0, primary_key=True)
    position = LongField(min_value=0) #, unique=True)
    value    = ListField(BooleanField(), null=False, required=True)
    # collection metadata
    meta = {
        #'queryset_class': SeedValueQuerySet,
        'abstract': True,
        'index_background': True,
        'auto_create_index': True,
        'indexes': [
            'position',
            'value',
        ],
    }

    def to_bitarray(self, frozen: bool=True) -> Union[bitarray, frozenbitarray]: 
        value = bitarray(self.value, endian='little')
        if frozen:
            return frozenbitarray(value, endian='little')
        return value

class BlackListedValue(Document):
    """
    Не найденное в диапазоне значение
    """
    # минимальная позиция для начала поиска
    value_id   = LongField(min_value=0)
    position   = LongField(min_value=0)
    value      = ListField(BooleanField(), null=False, required=True)
    bit_length = IntField(min_value=0)
    # collection metadata
    meta = {
        'indexes': [
            'value_id',
            'position',
            'value',
            'bit_length',
        ],
    }

    def to_bitarray(self, frozen: bool=True) -> Union[bitarray, frozenbitarray]: 
        if frozen:
            return frozenbitarray(self.value, endian='little')
        return bitarray(self.value, endian='little')

### CACHE ###

CACHED_CLASSES     = dict()
CACHED_CLASS_NAMES = set()

def has_cached_class(class_name: str) -> bool:
    return (class_name in CACHED_CLASS_NAMES)

def get_cached_class(class_name: str) -> Union[LengthBasedValue, Document, QuerySet]:
    return CACHED_CLASSES.get(class_name)

### COLLECTION FACTORIES ###

def get_value_class_name(value_length: int, seed: int=0) -> str:
    return f"ValueL{value_length}S{seed}"

def get_value_collection_name(value_length: int, seed: int=0) -> str:
    return f"value_l{value_length}_s{seed}"

@lru_cache(maxsize=2**8)
def make_value_class(value_length: int, seed: int=0) -> LengthBasedValue:
    # creating class dynamically (OMG, Python, you are AWESOME!)
    # https://www.geeksforgeeks.org/create-classes-dynamically-in-python/
    return type(get_value_class_name(value_length=value_length, seed=seed), (LengthBasedValue, ), {
        "meta" : {
            'collection': get_value_collection_name(value_length=value_length, seed=seed),
        }
    })

@lru_cache(maxsize=2**8)
def get_value_class(value_length: int, seed: int=0) -> Union[LengthBasedValue, Document, QuerySet]:
    class_name = get_value_class_name(value_length=value_length, seed=seed)
    if (has_cached_class(class_name) is False):
        # cache constructed class
        value_class = make_value_class(value_length=value_length, seed=seed)
        CACHED_CLASSES[class_name] = value_class
        CACHED_CLASS_NAMES.add(class_name)
    return get_cached_class(class_name)

### HASH RANGE BUCKETS ###

@dataclass()
class HashRangeBucket:
    __slots__ =(
        'bucket_id',
        'item_bit_length', 
        'bucket_bit_length', 
        'bucket_size', 
        #'max_bucket_items',
        'type',
        #'range',
        'min_id',
        'max_id',
        'included_ids',
        'excluded_ids',
    )

    #included_ids: Set[int] = set()
    #excluded_ids: Set[int] = set()

    """
    Оптимизированный кеш интервала знчений хеш-пространства
    """
    def __init__(self, item_bit_length: int, bucket_id: int, bucket_bit_length: int):
        self.bucket_id         = bucket_id
        self.item_bit_length   = item_bit_length
        self.bucket_bit_length = bucket_bit_length
        
        self.bucket_size      = 2**self.bucket_bit_length
        #self.max_bucket_items = (self.bucket_size // 2)
        self.min_id           = self.bucket_id * self.bucket_size
        self.max_id           = self.min_id + self.bucket_size - 1

        self.type         = 'INCLUDE'
        #self.range        = range(self.min_id, self.max_id + 1)

        self.included_ids = set()
        self.excluded_ids = set()

    def get_local_id(self, item_id: int) -> int:
        return item_id #(item_id % self.bucket_size)
  
    def has_id(self, item_id: int) -> bool:
        if (item_id < self.min_id) or (item_id > self.max_id):
            return Exception(f"Incorrect bucket {self.bucket_id} for item_id {item_id}")
        local_id = item_id#self.get_local_id(item_id=item_id)
        if (self.type == 'INCLUDE'):
            return (local_id in self.included_ids)
        if (self.type == 'EXCLUDE'):
            return (local_id not in self.excluded_ids)
  
    def add_id(self, item_id: int):
        if (item_id < self.min_id) or (item_id > self.max_id):
            raise Exception(f"Incorrect bucket {self.bucket_id} for item_id {item_id}")
        local_id = item_id#self.get_local_id(item_id=item_id)
        if (self.type == 'INCLUDE'):
            self.included_ids.add(local_id)
            self.update_type()
        if (self.type == 'EXCLUDE'):
            self.excluded_ids.discard(local_id)
  
    def update_type(self):
        if (self.type == 'INCLUDE') and (len(self.included_ids) >= (self.bucket_size // 2)):
            for item_id in range(self.min_id, self.max_id + 1):
                local_id = item_id#self.get_local_id(item_id=item_id)
                if (local_id in self.included_ids):
                    self.included_ids.remove(local_id)
                else:
                    self.excluded_ids.add(local_id)
            self.type = 'EXCLUDE'
            self.included_ids.clear()
            #del self.included_ids
            #if ((self.bucket_id % 128) == 0) or ((self.bucket_id % 128) == 1):
            #    print(f"Bucket {self.bucket_id} ({self.min_id}-{self.max_id}): changed type to EXCLUDE, ids: {len(self.included_ids)}/{len(self.excluded_ids)}, {list(self.excluded_ids)[0:16]}")
  
    def load_items(self):
        ValueClass = get_value_class(value_length=self.item_bit_length)
        item_ids   = ValueClass.objects(id__gte=self.min_id, id__lte=self.max_id).scalar('id')
        for item_id in item_ids:
            self.add_id(item_id=item_id)
        self.update_type()
        del ValueClass

@dataclass()
class HashRangeSegment:
    __slots__ = (
        'item_bit_length', 
        'bucket_bit_length', 
        'init_buckets',
        #'determinant', 
        'bucket_size', 
        'max_bucket_items', 
        'total_items', 
        'buckets',
        'total_buckets',
        'bucket_bitmap',
    )
    #bucket_bitmap: Dict[HashRangeBucket] = dict()

    """
    Оптимизированный класс для поиска значений в хеш-пространстве
    """
    def __init__(self, item_bit_length: int, bucket_bit_length: int, init_buckets: bool=False):
        self.item_bit_length   = item_bit_length
        self.bucket_bit_length = bucket_bit_length
        self.init_buckets      = init_buckets
        
        if (2**item_bit_length < bucket_bit_length):
            raise Exception("Bucket bit length is too large")
        
        # число по которому элементы разделяются на группы
        self.bucket_size      = 2**self.bucket_bit_length
        #self.determinant      = self.bucket_size

        self.max_bucket_items = (self.bucket_size // 2)
        self.total_items      = 2**self.item_bit_length
        self.total_buckets    = (self.total_items // self.bucket_size) + 1
        #self.min_id           = 0
        #self.max_id           = self.total_items - 1
        self.buckets          = dict()
        self.bucket_bitmap    = bitarray('0' * (self.total_buckets))
        #if (init_buckets is True):
        #    for bucket_id in range(0, self.total_buckets):
        #        self.get_bucket(bucket_id * self.bucket_size)
    
    def get_bucket_id(self, item_id: int) -> int:
        return (item_id // self.bucket_size)
    
    def is_filled_bucket(self, bucket_id: int) -> bool:
        return bool(self.bucket_bitmap[bucket_id])
    
    def update_filled(self, bucket_id: int) -> bool:
        if (self.buckets[bucket_id].type == 'EXCLUDE') and (len(self.buckets[bucket_id].excluded_ids) == 0):
            self.bucket_bitmap[bucket_id] = True
            del self.buckets[bucket_id]
            #print(f"Filled bucket_id={bucket_id}: remaining buckets {len(self.buckets)}")
    
    def get_bucket(self, item_id: int) -> HashRangeBucket:
        bucket_id = self.get_bucket_id(item_id=item_id)
        if bucket_id not in self.buckets:
            bucket = HashRangeBucket(self.item_bit_length, bucket_id, self.bucket_bit_length)
            if (self.init_buckets is True):
                bucket.load_items()
                #if ((bucket_id % 128) == 0) or ((bucket_id % 128) == 1):
                #    print(f"Loaded bucket #{bucket_id}/{self.total_buckets}: type={bucket.type}, {len(bucket.included_ids)}/{len(bucket.excluded_ids)}, {list(bucket.included_ids)[0:16]}, {list(bucket.excluded_ids)[0:16]}") #{len(bucket.included_ids)}
            #if (bucket.type == 'EXCLUDE') and (len(bucket.excluded_ids) == 0):
            #    self.bucket_bitmap[bucket_id] = True
            #    return bucket
            self.buckets[bucket_id] = bucket
        return self.buckets[bucket_id]
    
    def has_id(self, item_id: int) -> bool:
        bucket_id = self.get_bucket_id(item_id=item_id)
        if (self.is_filled_bucket(bucket_id=bucket_id)):
            return True
        bucket = self.get_bucket(item_id=item_id)
        return bucket.has_id(item_id)
    
    def add_id(self, item_id: int):
        bucket_id = self.get_bucket_id(item_id=item_id)
        if (self.is_filled_bucket(bucket_id=bucket_id)):
            return
        bucket = self.get_bucket(item_id=item_id)
        bucket.add_id(item_id)
        self.update_filled(bucket.bucket_id)


### ITEM VALUE HELPERS ###

def value2id(value: Union[bitarray, frozenbitarray]) -> int:
    return ba2int(value, signed=False)

def id2value(value_id: int, value_length: int, frozen: bool=True) -> Union[bitarray, frozenbitarray]:
    value = int2ba(value_id, length=value_length, endian='little', signed=False)
    if frozen:
        return frozenbitarray(value, endian='little')
    return value

@lru_cache(maxsize=2**21)
def is_blacklisted(value: Union[bitarray, frozenbitarray], max_position: int, seed: int=0) -> bool:
    blacklisted_count = BlackListedValue.objects(value_id=value2id(value), bit_length=len(value), position__gte=max_position).limit(1).count(with_limit_and_skip=True)
    if (blacklisted_count > 0):
        return True
    ValueClass   = get_value_class(value_length=len(value), seed=seed)
    saved_count  = ValueClass.objects(id=value2id(value), position__gt=max_position).limit(1).count(with_limit_and_skip=True)
    if (saved_count > 0):
        return True
    return False

def get_saved_value(value: Union[bitarray, frozenbitarray], max_position: int=None, seed: int=0) -> Union[LengthBasedValue, Document, QuerySet]:
    ValueClass = get_value_class(value_length=len(value), seed=seed)
    if (max_position is None):
        return ValueClass.objects(id=value2id(value)).first()
    return ValueClass.objects(id=value2id(value), position__lte=max_position).first()

@lru_cache(maxsize=2**21)
def get_saved_value_position(value: Union[bitarray, frozenbitarray], max_position: int=None, seed: int=0) -> Union[int, None]:
    if (max_position is None):
        saved_value = get_saved_value(value=value, seed=seed)
    else:
        saved_value = get_saved_value(value=value, max_position=max_position, seed=seed)
    if saved_value:
        return saved_value.position
    return None

def save_item_value(value: Union[bitarray, frozenbitarray], position: int, seed: int=0, check_existing: bool=True) -> Union[LengthBasedValue, Document, QuerySet]:
    if check_existing:
        saved_value_position = get_saved_value_position(value=value, seed=seed)
        if (saved_value_position is not None):
            return get_saved_value(value=value, seed=seed)
    # create new value only when it is unique
    ValueClass = get_value_class(value_length=len(value), seed=seed)
    new_value  = ValueClass(
        id=value2id(value),
        position=position,
        value=value.tolist(),
    )
    new_value.save(force_insert=True)
    # clear cache every time when saving new value to db
    get_saved_value_position.cache_clear()
    count_saved_values.cache_clear()

    return new_value

def save_item_values(new_items: List[Union[LengthBasedValue, Document, QuerySet]], ValueClass: Union[LengthBasedValue, Document, QuerySet]) -> List[int]:
    if (len(new_items) == 0):
        return
    new_item_ids = QuerySet(ValueClass, collection=ValueClass._get_collection()).insert(new_items, load_bulk=False)
    get_saved_value_position.cache_clear()
    count_saved_values.cache_clear()
    return new_item_ids

def create_item_document(value: Union[bitarray, frozenbitarray], position: int, seed: int=0) -> Union[LengthBasedValue, Document, QuerySet, None]:
    ValueClass = get_value_class(value_length=len(value), seed=seed)
    new_item = ValueClass(
        id=value2id(value),
        position=position,
        value=value.tolist(),
    )
    return new_item

def discard_existing_ids(value_ids: Set[int], ValueClass: Union[LengthBasedValue, Document, QuerySet], positions: Set[int]=None) -> Set[int]:
    min_id = min(value_ids)
    max_id = max(value_ids)
    existing_ids = ValueClass.objects(
        id__in=value_ids, 
        id__gte=min_id, 
        id__lte=max_id, 
    ).scalar('id')
    
    #if (len(existing_ids) == 0):
    #    return value_ids
    
    existing_ids = set(existing_ids)
    for value_id in value_ids.copy():
        if (value_id in existing_ids):
            value_ids.remove(value_id)
            existing_ids.remove(value_id)
    return value_ids

def discard_existing_values(values: Set[frozenbitarray], ValueClass: Union[LengthBasedValue, Document, QuerySet]) -> Set[frozenbitarray]:
    value_ids       = set()
    for value in values:
        value_ids.add(value2id(value=value))
    filtered_ids = discard_existing_ids(value_ids=value_ids, ValueClass=ValueClass)

    #if (len(values) == len(filtered_ids)):
    #    return values

    filtered_values = set()
    for filtered_id in filtered_ids:
        filtered_values.add(id2value(filtered_id))
    return filtered_values

def discard_existing_items(items: Dict[frozenbitarray, int], ValueClass: Union[LengthBasedValue, Document, QuerySet]) -> Dict[frozenbitarray, int]:
    value_length    = len(list(items.keys())[0])
    value_ids       = set()
    value_positions = set()
    for value, position in items.items():
        value_ids.add(value2id(value=value))
        value_positions.add(position)
    filtered_ids = discard_existing_ids(value_ids=value_ids, ValueClass=ValueClass, positions=value_positions)
    
    # no changes after filtering - return original dict
    #if (len(items) == len(filtered_ids)):
    #    return items
    
    # reconstruct original dict and return it
    filtered_items = dict()
    for filtered_id in filtered_ids:
        value = id2value(filtered_id, value_length=value_length)
        filtered_items[value] = items[value]
    return filtered_items

def save_blacklisted_value(value: Union[bitarray, frozenbitarray], max_position: int, seed: int=0) -> None:
    if (is_blacklisted(value=value, max_position=max_position, seed=seed)):
        return
    new_value = BlackListedValue(
        value_id=value2id(value),
        position=max_position,
        value=value.tolist(),
        bit_length=len(value),
    )
    new_value.save(force_insert=True)
    # new value added - cache invalidated
    is_blacklisted.cache_clear()

@lru_cache(maxsize=2**8)
def count_saved_values(value_length: int) -> int:
    ValueClass = get_value_class(value_length=value_length)
    if (value_length <= 21):
        total_count  = ValueClass.objects.count()
    else:
        count_batches      = 2**14
        count_segment_size = (2**value_length) // count_batches
        total_count   = 0
        for count_segment_id in track(range(0, count_batches), description=f"Counting {value_length}-bit values (segment_size={count_segment_size})..."):
            start_count_segment = count_segment_id * count_segment_size
            end_count_segment   = start_count_segment + count_segment_size
            total_count = total_count + ValueClass.objects(id__gte=start_count_segment, id__lt=end_count_segment).count(with_limit_and_skip=True)
    return total_count

def has_max_saved_values(value_length: int, seed: int=0) -> bool:
    ValueClass    = get_value_class(value_length=value_length, seed=seed)
    max_count     = 2**value_length
    current_count = count_saved_values(value_length=value_length) #ValueClass.objects.count(with_limit_and_skip=True)
    return (current_count == max_count)

def get_max_position(value_length: int, seed: int=0) -> int:
    ValueClass  = get_value_class(value_length=value_length, seed=seed)
    saved_value = ValueClass.objects.order_by('-position').first()
    if (saved_value is None):
        return 0
    return saved_value.position

def get_batch_size(value_length: int) -> int:
    batch_size  = DEFAULT_BATCH_SIZE
    ValueClass  = get_value_class(value_length=value_length)
    saved_count = ValueClass.objects.count(with_limit_and_skip=True)
    if value_length <= 16:
      return 1
    if (2**value_length - saved_count) <= batch_size:
        batch_size = 1 #(2**value_length - saved_count)
    return batch_size

def discover_values(value_length: int, start_position: int=0, bucket_bit_length: int=13, init_buckets: bool=False, 
        input_batch_size: int=None, input_filter_batch_size: int=None, stop_position=None, seed: int=0) -> None:
    """
    Find first occurance of each value for the given length
    """
    if (has_max_saved_values(value_length=value_length, seed=seed) is True):
        print(f"value_length={value_length} has max values")
        return
    else:
        print(f"value_length={value_length}")
    ValueClass   = get_value_class(value_length=value_length, seed=seed)
    batch_size   = get_batch_size(value_length=value_length)
    
    filter_batch         = dict()
    save_batch           = dict()
    filter_batch_size    = batch_size
    loaded_buckets_count = 0
    if (stop_position is None):
        # stop scan when position length is equal to value length
        if (value_length <= 21):
          stop_position = (2**26)
        else:
          stop_position = (2**26)
    #if (init_buckets is False) and (value_length >= 2**24):
    #    filter_batch_size = DEFAULT_BATCH_SIZE
    # override batch size
    if (input_batch_size is not None):
        batch_size        = input_batch_size
    if (input_filter_batch_size is not None):
        filter_batch_size = input_filter_batch_size
    #if start_position == 0:
    #    start_position = total_count
    hash_range_tail_started = False

    total_count = count_saved_values(value_length=value_length)

    print(f"total_count={total_count}, batch_size={batch_size}, filter_batch_size={filter_batch_size}")
    if (value_length <= 16):
        bucket_bit_length = value_length
    else:
        bucket_bit_length = 16
    item_cache = HashRangeSegment(item_bit_length=value_length, bucket_bit_length=bucket_bit_length, init_buckets=init_buckets)
    bucket_ids = set([bucket_id for bucket_id in range(0, item_cache.total_buckets)])
    
    with Progress(
        TextColumn("[progress.description]{task.description}"),
        MofNCompleteColumn(),
        BarColumn(),
        TaskProgressColumn(text_format="[progress.percentage]{task.percentage:>3.2f}%", show_speed=True),
        TimeRemainingColumn(elapsed_when_finished=True),
        TransferSpeedColumn(),
        #auto_refresh=False,
        refresh_per_second=2,
        speed_estimate_period=120,
        expand=True,
    ) as progress:
        main_task_id   = progress.add_task(f"Discovering {value_length}-bit values...", total=2**value_length)
        range_task_id  = progress.add_task(f"Range", total=(stop_position // 8))
        bucket_task_id = progress.add_task(f"Buckets", total=len(bucket_ids)-1)
        filter_task_id = progress.add_task(f"Filter batch", total=filter_batch_size)
        save_task_id   = progress.add_task(f"Save batch", total=batch_size)

        progress.advance(main_task_id, total_count)
        progress.advance(range_task_id, (start_position // 8))

        hash_range = HashIterator(item_bit_length=value_length, start_position=start_position, stop_position=stop_position, seed=seed, frozen=True)
        for hash_value in hash_range:
            hash_value_id = value2id(hash_value)
            # measure scan progress in bits, convert value to bytes to display correct value in "TransferSpeedColumn" inside progressbar
            if (hash_range.bit_position % 20000) == 0:
                progress.advance(range_task_id, 2500)
            
            if (loaded_buckets_count < item_cache.total_buckets):
                bucket_id = item_cache.get_bucket_id(hash_value_id)
                if (bucket_id in bucket_ids):
                    bucket_ids.discard(bucket_id)
                    loaded_buckets_count += 1
                    progress.advance(bucket_task_id, 1)

            if item_cache.has_id(hash_value_id):
                continue

            item_cache.add_id(hash_value_id)
            filter_batch[hash_value] = hash_range.bit_position
            progress.advance(filter_task_id, 1)

            # flag defining last positions within hash range
            if ((hash_range.bit_position + (DEFAULT_BATCH_SIZE // 4)) >= stop_position):
                hash_range_tail_started = True
            
            # collect non-checked new values in buffer to efficently use db 
            # (1 query checking 4096 ids is better than 4096 queries checking 1 id each)
            if (len(filter_batch) < filter_batch_size) and (hash_range_tail_started is False):
                continue
            
            # filter potential new items
            filter_batch = discard_existing_items(filter_batch, ValueClass)
            for value, position in filter_batch.items():
                progress.advance(save_task_id, 1)
                save_batch[value] = position
            # remove processed items from filter batch
            filter_batch.clear()
            # reset filter batch progress
            progress.update(filter_task_id, completed=0, refresh=False)
            
            # (1 query saving 4096 values is better than 4096 queries savibg 1 value each)
            if (len(save_batch) < batch_size) and (hash_range_tail_started is False):
                continue
            
            # save new items in batch
            new_items = []
            for value, position in save_batch.items():
                new_item = create_item_document(value=value, position=position)
                new_items.append(new_item)
                total_count += 1
            # TODO: check save result
            saved_ids = save_item_values(new_items=new_items, ValueClass=ValueClass)
            progress.advance(main_task_id, len(save_batch))
            # remove processed items from batch
            save_batch.clear()
            # reset save batch progress
            progress.update(save_task_id, completed=0, refresh=False)

            # update batch size 
            if (batch_size == DEFAULT_BATCH_SIZE) and ((2**value_length - total_count) < (DEFAULT_BATCH_SIZE * 2)):
                batch_size = get_batch_size(value_length=value_length)
                #if (2**value_length < max_cache_size):
                    # srink filter batch size only if cache can cover all possible values
                filter_batch_size = 1
                print(f"total_count={total_count}, batch_size={batch_size}, filter_batch_size={filter_batch_size}")
            # all values found
            if (total_count == 2**value_length):
                # (has_max_saved_values(value_length=value_length, seed=seed) is True)
                progress.stop_task(range_task_id)
                progress.update(save_task_id, refresh=False)
                progress.refresh()
                break
    # no return value expected as a result - we just need to run a process itself
    return None

def get_max_filled_length(max_length: int=32, seed: int=0) -> int:
    for value_length in range(1, max_length+1):
        if (has_max_saved_values(value_length=value_length, seed=seed) is False):
            return value_length - 1
    return max_length

def update_blacklisted_values() -> None:
    max_length   = get_max_filled_length()
    max_position = get_max_position(value_length=max_length)
    values_count = BlackListedValue.objects(position__lte=max_position, bit_length__lte=max_length).count()
    if (values_count == 0):
        return
    with Progress(
        TextColumn("[progress.description]{task.description}"),
        MofNCompleteColumn(),
        BarColumn(),
        TaskProgressColumn(text_format="[progress.percentage]{task.percentage:>3.2f}%", show_speed=True),
        TimeElapsedColumn(),
        TimeRemainingColumn(),
        auto_refresh=False,
        refresh_per_second=2, 
        speed_estimate_period=120
    ) as progress:
        task_id = progress.add_task(f'Blacklist cleanup (max_bits={max_length})...', total=values_count)
        #for item in track(BlackListedValue.objects(position__lte=max_position, bit_length__lte=max_length), refresh_per_second=1):
        for item in BlackListedValue.objects(position__lte=max_position, bit_length__lte=max_length):
            progress.advance(task_id=task_id, advance=1)
            #progress.refresh()
            if (item.bit_length <= max_length):
                item.delete()
            else:
                position = get_saved_value_position(item.to_bitarray())
                if (position is not None):
                    item.delete()
        # final update before finish
        progress.refresh()

NextItemOption = namedtuple("NextItemOption", [
    "value_length",
    "item",
    "position",
    "prev_distance",
    "prev_distance_length",
    "start_position",
    "stop_position",
    "score",
    "position_length",
])

def load_hash_segment(start_positiom: int, bit_length: int) -> frozenbitarray:
    return bits_at_position(bit_position=start_positiom, bit_length=bit_length, frozen=True)

def get_max_value_id(value_length: int, seed: int=0) -> int:
    ValueClass   = get_value_class(value_length=value_length, seed=seed)
    min_id       = ((2**value_length) // 2)
    #max_id       = (2**value_length)
    total_count  = ValueClass.objects(id__gte=min_id).count()
    
    pass