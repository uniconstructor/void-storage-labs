# https://stackoverflow.com/questions/38340808/recursive-typing-in-python-3-5
from __future__ import annotations
from rich import print
from rich.pretty import pprint
from custom_counter import CustomCounter as Counter
from tqdm import tqdm
from bitarray.util import ba2int, int2ba
from bitarray import bitarray, frozenbitarray
from collections import defaultdict, namedtuple
from dataclasses import dataclass
from copy import deepcopy, copy
from typing import List, Set, Dict, Tuple, Optional, Union, Iterable
from functools import lru_cache
import xxhash
import math
from bitarrayset.binaryarrayset import BinaryArraySet
#from mongoengine import register_connection
#sregister_connection('default', db=f'256_byte_segments', host='127.0.0.1', port=27017, maxPoolSize=300)
from mongoengine import Document, QuerySet, LongField, IntField, \
    BinaryField, StringField, ListField, SortedListField, BooleanField, queryset_manager

#from hash_space_utils import get_min_bit_length
from hash_range_iterator import HASH_DIGEST_BITS, bits_at_position, int_bytes_from_digest, int_from_nounce,\
    bytes_from_nounce, get_min_bit_length, nounce_to_input
from fib_encoder import fib_encode_position

def get_number_offset(number: int) -> int:
    number_length = number.bit_length()
    if (number_length == 0):
        return 0
    offset = 0
    for i in range(0, number_length):
        offset += 2**i
    return offset

def encode_prefixed_varint(number: int, prefix_value: int, prefix_length: int) -> bitarray:
    prefix = int2ba(prefix_value, length=prefix_length, endian='big', signed=False)
    if (prefix_value > 0):
        value = int2ba(number, length=prefix_value, endian='big', signed=False)
    else:
        value = bitarray()
    return prefix + value

def create_number_ranges(max_length: int) -> List[Tuple[int, int]]:
    ranges    = []
    max_value = 0
    for number_length in range(0, max_length):
        min_value = max_value
        max_value = min_value + 2**number_length
        ranges.append((min_value, max_value))
    return ranges

SEED_PREFIX_BITS   = 4
NOUNCE_PREFIX_BITS = 4
HASH_INPUT_BYTES   = 4
MIN_ITEM_LENGTH    = SEED_PREFIX_BITS + NOUNCE_PREFIX_BITS + 1
MAX_ITEM_LENGTH    = 64
SAVE_BATCH_SIZE    = 16384
MAX_CACHED_LENGTH  = 31

seed_lengths      = [sl for sl in range(0, 2**SEED_PREFIX_BITS)]
SEED_RANGES       = create_number_ranges(max_length=2**SEED_PREFIX_BITS)
min_seed_length   = min(seed_lengths)
max_seed_length   = max(seed_lengths)

nounce_lengths    = [nl for nl in range(0, 2**NOUNCE_PREFIX_BITS)]
NOUNCE_RANGES     = create_number_ranges(max_length=2**NOUNCE_PREFIX_BITS)
min_nounce_length = min(nounce_lengths)
max_nounce_length = max(nounce_lengths)

LengthRange = namedtuple('LengthRange', [
    'seed_length',
    'nounce_length',
    'item_length',
    'total_seeds',
    'total_nounces',
    'total_items',
    'value_length',
    'item_score',
])

item_length_counts = Counter()
item_length_limits = Counter()
item_length_ranges = defaultdict(list)
total_available_items = 0

for seed_length in seed_lengths:
    min_seed    = SEED_RANGES[seed_length][0]
    max_seed    = SEED_RANGES[seed_length][1]
    total_seeds = max_seed - min_seed
    for nounce_length in nounce_lengths:
        min_nounce    = NOUNCE_RANGES[nounce_length][0]
        max_nounce    = NOUNCE_RANGES[nounce_length][1]
        total_nounces = max_nounce - min_nounce
        item_length   = SEED_PREFIX_BITS + NOUNCE_PREFIX_BITS + seed_length + nounce_length
        total_items   = total_seeds * total_nounces
        value_length  = item_length + 1
        if (value_length > 32):
            value_length = 32
        item_score = value_length - item_length
        # убираем из области поиска все диапазоны пораждающие значения с длиной больше разрешенной 
        if (item_length > MAX_ITEM_LENGTH):
            continue
        
        item_length_counts.update({ item_length: 1 })
        item_length_limits.update({ item_length: total_items })
        total_available_items += total_items

        item_length_ranges[item_length].append(LengthRange(
            seed_length   = seed_length,
            nounce_length = nounce_length,
            item_length   = item_length,
            total_seeds   = total_seeds,
            total_nounces = total_nounces,
            total_items   = total_items,
            value_length  = value_length,
            item_score    = item_score,
        ))
ITEM_LENGTH_RANGES = item_length_ranges
ITEM_LENGTH_COUNTS = item_length_counts
ITEM_LENGTH_LIMITS = item_length_limits

@dataclass(init=True)
class DuplicateSeedValue:
    seed             : int
    seed_length      : int
    nounce           : int
    nounce_length    : int
    nounce_distance  : int
    value            : int
    value_length     : int
    item_length      : int
    canonical_seed   : int
    canonical_nounce : int
    canonical_length : int
    canonical_value  : int
    nounce_shift     : int
    init_value       : int

@dataclass(init=True)
class SeedDictionary:
    seed             : int
    seed_length      : int
    # value length for seed items
    min_value_length : int
    # value_length / value
    item_values      : Dict[int, Set[int]]
    # value_length / nounce -> value
    nounce_values    : Dict[int, Dict[int, int]]
    # value_length / value -> nounce
    value_nounces    : Dict[int, Dict[int, int]]
    duplicates       : Dict[int, Dict[int, DuplicateSeedValue]]
    duplicate_counts : Counter

    def __init__(self, seed: int, seed_length: int):
        self.seed             = seed
        self.seed_length      = seed_length
        self.min_value_length = MIN_ITEM_LENGTH + seed_length
        self.item_values      = defaultdict(set)
        self.nounce_values    = defaultdict(dict)
        self.value_nounces    = defaultdict(dict)
        self.duplicates       = defaultdict(dict)
        self.duplicate_counts = Counter()
    
    def has_prefix_item(self, item_value: int, item_length: int) -> bool:
        for value_length in range(self.min_value_length, item_length+1):
            prev_value = item_value % (2**value_length)
            if (prev_value in self.item_values[value_length]):
                return True
        return False
    
    def get_prefix_item(self, item_value: int, item_nounce: int, item_length: int, nounce_length: int) -> DuplicateSeedValue:
        if (self.has_prefix_item(item_value=item_value, item_length=item_length) is False):
            return None
        if (item_length <= 32):
            value_length = item_length
        else:
            value_length = 32
        canonical_nounce = None
        for prev_length in range(self.min_value_length, item_length+1):
            if (prev_length > 32):
                prev_length = 32
            prev_value = item_value % (2**prev_length)
            if (prev_value in self.item_values[prev_length]):
                canonical_nounce = self.value_nounces[prev_length][prev_value]
                canonical_value  = prev_value
                break
        return DuplicateSeedValue(
            seed             = self.seed,
            seed_length      = self.seed_length,
            nounce           = item_nounce,
            nounce_length    = nounce_length,
            nounce_distance  = 2**nounce_length,
            value            = item_value,
            value_length     = value_length,
            item_length      = item_length,
            canonical_seed   = self.seed,
            canonical_nounce = canonical_nounce,
            canonical_length = prev_length,
            canonical_value  = canonical_value,
            nounce_shift     = 0,
            init_value       = item_value,
        )

    def load_length_values(self, nounce_length: int):
        min_nounce      = NOUNCE_RANGES[nounce_length][0]
        max_nounce      = NOUNCE_RANGES[nounce_length][1]
        nounce_range    = range(min_nounce, max_nounce)
        item_length     = self.min_value_length + nounce_length
        nounce_distance = max_nounce - min_nounce
        if (item_length <= 32):
            value_length = item_length
        else:
            value_length = 32
        #print(f"item_length={item_length}, value_length={value_length}")
        if (nounce_distance.bit_length() >= 24):
            progress = tqdm(nounce_range, mininterval=0.5, smoothing=0)
        else:
            progress = nounce_range
        for nounce in progress:
            hash_input = nounce.to_bytes(length=HASH_INPUT_BYTES, byteorder='little', signed=False)
            digest     = xxhash.xxh32_intdigest(input=hash_input, seed=self.seed)
            item_value = digest % (2**value_length)
            if (self.has_prefix_item(item_value=item_value, item_length=item_length) is False):
                # save unique value to dict
                self.item_values[value_length].add(item_value)
                self.nounce_values[value_length][nounce]     = item_value
                self.value_nounces[value_length][item_value] = nounce
            else:
                duplicate = self.get_prefix_item(item_value=item_value, item_nounce=nounce, item_length=item_length, nounce_length=nounce_length)
                while (True):
                    new_nounce = duplicate.nounce + duplicate.nounce_distance * duplicate.nounce_shift
                    hash_input = new_nounce.to_bytes(length=HASH_INPUT_BYTES, byteorder='little', signed=False)
                    new_digest = xxhash.xxh32_intdigest(input=hash_input, seed=duplicate.seed)
                    new_value  = new_digest % (2**duplicate.value_length)
                    if (self.has_prefix_item(item_value=new_value, item_length=duplicate.value_length) is False):
                        duplicate.nounce_shift = duplicate.nounce_shift
                        duplicate.value        = new_value
                        self.item_values[value_length].add(new_value)
                        self.nounce_values[value_length][new_nounce] = new_value
                        self.value_nounces[value_length][new_value]  = new_nounce
                        self.duplicates[item_length][new_value]      = duplicate
                        break
                    else:
                        duplicate.nounce_shift += 1
                self.duplicates[value_length][item_value] = duplicate
                self.duplicate_counts.update({ duplicate.value_length: 1 })

TargetItem = namedtuple('TargetItem', [
    'seed',
    'seed_length',
    'nounce',
    'nounce_length',
    'value',
    'value_length',
    'score',
])

def scan_length_ranges(data_item: bitarray, length_ranges: List[LengthRange], seed_offset: int=0) -> Union[TargetItem, None]:
    total_range_items = sum([lr.total_items for lr in length_ranges])
    target_value      = ba2int(data_item, signed=False)
    value_length      = len(data_item)
    length_range_id   = 0
    
    show_progress = False
    if (total_range_items.bit_length() >= 24):
        show_progress = True
        progress      = tqdm(total=total_range_items, mininterval=0.5, smoothing=0)

    for length_range in length_ranges:
        if (show_progress):
            progress_info = f"range={length_range_id+1}/{len(length_ranges)} (s:{length_range.seed_length}|n:{length_range.nounce_length}|i:{length_range.item_length}), target_value={target_value} ({value_length})"
            progress.set_description_str(progress_info)
        min_seed   = SEED_RANGES[length_range.seed_length][0] + seed_offset
        max_seed   = SEED_RANGES[length_range.seed_length][1] + seed_offset
        seed_range = range(min_seed, max_seed)
        for seed in seed_range:
            min_nounce   = NOUNCE_RANGES[length_range.nounce_length][0]
            max_nounce   = NOUNCE_RANGES[length_range.nounce_length][1]
            nounce_range = range(min_nounce, max_nounce)
            for nounce in nounce_range:
                if (show_progress):
                    progress.update(1)
                hash_input = nounce.to_bytes(length=HASH_INPUT_BYTES, byteorder='little', signed=False)
                digest     = xxhash.xxh32_intdigest(input=hash_input, seed=seed)
                if (value_length < 32):
                    item_value = digest % (2**value_length)
                else:
                    item_value = digest
                if (item_value == target_value):
                    score = value_length - SEED_PREFIX_BITS - NOUNCE_PREFIX_BITS - length_range.seed_length - length_range.nounce_length
                    return TargetItem(
                        seed          = seed,
                        seed_length   = length_range.seed_length,
                        nounce        = nounce,
                        nounce_length = length_range.nounce_length,
                        value         = item_value,
                        value_length  = value_length,
                        score         = score,
                    )
        if (show_progress):
            progress.refresh()
        length_range_id += 1
    return None

### BASE CLASSES ###

class BaseSeedItemValue(Document):
    id             = IntField(primary_key=True)
    seed           = IntField(null=False, required=True)
    seed_length    = IntField(null=False, required=True)
    seed_shift     = IntField(null=False, required=True)
    shifted_seed   = IntField(null=False, required=True)
    nounce         = IntField(null=False, required=True)
    nounce_length  = IntField(null=False, required=True)
    #nounce_shift   = IntField(null=False, required=True)
    #shifted_nounce = LongField(null=False, required=True)
    value_length   = IntField(null=False, required=True)
    item_length    = IntField(null=False, required=True)
    score          = IntField(null=False, required=True)
    # collection metadata
    meta = {
        'abstract': True,
        'indexes': [
            'seed',
            'seed_length',
            'seed_shift',
            'nounce',
            'nounce_length',
            #'value_length',
            'item_length',
        ],
    }

class BaseSeedItemRange(Document):
    id             = IntField(primary_key=True)
    seed_length    = IntField(null=False, required=True)
    nounce_length  = IntField(null=False, required=True)
    value_length   = IntField(null=False, required=True)
    item_length    = IntField(null=False, required=True)
    item_score     = IntField(null=False, required=True)
    total_seeds    = IntField(null=False, required=True)
    total_nounces  = IntField(null=False, required=True)
    total_items    = IntField(null=False, required=True)
    saved_items    = IntField(null=False, required=True)
    completed      = BooleanField(null=False, required=True)
    # collection metadata
    meta = {
        'abstract': True,
        'indexes': [
            'seed_length',
            'nounce_length',
            'value_length',
            'item_length',
            'item_score',
            'total_seeds',
            'total_nounces',
            'total_items',
            'saved_items',
            'completed',
        ],
    }

class BaseDataItemValue(Document):
    """
    Значение фрагмента данных при разбиении фиксированной длины (одна коллекция - одно разбиение)
    """
    # id=position
    id      = IntField(primary_key=True)
    value   = IntField(null=False, required=True)
    # collection metadata
    meta = {
        'abstract': True,
        'indexes': [
            'value',
        ],
    }

### CACHE ###

CACHED_CLASSES     = dict()
CACHED_CLASS_NAMES = set()

def has_cached_class(class_name: str) -> bool:
    return (class_name in CACHED_CLASS_NAMES)

def get_cached_class(class_name: str) -> Document:
    return CACHED_CLASSES.get(class_name)

### COLLECTION FACTORIES ###

def get_seed_item_class_name(seed_bits: int, nounce_bits: int, value_length: int) -> str:
    return f"SeedItemValueS{seed_bits:02}N{nounce_bits:02}L{value_length:02}"

def get_seed_item_range_class_name(seed_bits: int, nounce_bits: int) -> str:
    return f"SeedItemRangeS{seed_bits:02}N{nounce_bits:02}"

def get_data_item_class_name(data_bits: int) -> str:
    return f"DataItemValueL{data_bits:02}"
    
def get_seed_item_collection_name(seed_bits: int, nounce_bits: int, value_length: int) -> str:
    return f"seed_item_value_s{seed_bits:02}_n{nounce_bits:02}_l{value_length:02}"

def get_item_range_collection_name(seed_bits: int, nounce_bits: int) -> str:
    return f"seed_item_range_s{seed_bits:02}_n{nounce_bits:02}"

def get_data_item_collection_name(data_bits: int) -> str:
    return f"data_item_value_l{data_bits}"

def make_seed_item_value_class(seed_bits: int, nounce_bits: int, value_length: int) -> BaseSeedItemValue:
    return type(get_seed_item_class_name(seed_bits=seed_bits, nounce_bits=nounce_bits, value_length=value_length), (BaseSeedItemValue, ), {
        "meta" : {
            'collection': get_seed_item_collection_name(seed_bits=seed_bits, nounce_bits=nounce_bits, value_length=value_length),
        }
    })

def make_seed_item_range_class(seed_bits: int, nounce_bits: int) -> BaseSeedItemRange:
    return type(get_seed_item_range_class_name(seed_bits=seed_bits, nounce_bits=nounce_bits), (BaseSeedItemRange, ), {
        "meta" : {
            'collection': get_item_range_collection_name(seed_bits=seed_bits, nounce_bits=nounce_bits),
        }
    })

def make_data_item_value_class(data_bits: int) -> BaseDataItemValue:
    return type(get_data_item_class_name(data_bits=data_bits), (BaseDataItemValue, ), {
        "meta" : {
            'collection': get_data_item_collection_name(data_bits=data_bits),
        }
    })

def get_seed_item_value_class(seed_bits: int, nounce_bits: int, value_length: int) -> Union[BaseSeedItemValue, Document]:
    class_name = get_seed_item_class_name(seed_bits=seed_bits, nounce_bits=nounce_bits, value_length=value_length)
    if (has_cached_class(class_name) is False):
        CACHED_CLASSES[class_name] = make_seed_item_value_class(seed_bits=seed_bits, nounce_bits=nounce_bits, value_length=value_length)
        CACHED_CLASS_NAMES.add(class_name)
    return get_cached_class(class_name)

def get_item_range_class(seed_bits: int, nounce_bits: int) -> Union[BaseSeedItemRange, Document]:
    class_name = get_seed_item_range_class_name(seed_bits=seed_bits, nounce_bits=nounce_bits)
    if (has_cached_class(class_name) is False):
        CACHED_CLASSES[class_name] = make_seed_item_range_class(seed_bits=seed_bits, nounce_bits=nounce_bits)
        CACHED_CLASS_NAMES.add(class_name)
    return get_cached_class(class_name)

def get_data_item_value_class(data_bits: int) -> Union[BaseDataItemValue, Document]:
    class_name = get_data_item_class_name(data_bits=data_bits)
    if (has_cached_class(class_name) is False):
        CACHED_CLASSES[class_name] = make_data_item_value_class(data_bits=data_bits)
        CACHED_CLASS_NAMES.add(class_name)
    return get_cached_class(class_name)

### END OF METACLASSES ###

@lru_cache(maxsize=64)
def get_item_length_limits(seed_bits: int, nounce_bits: int) -> Dict[int, int]:
    seed_ranges    = create_number_ranges(2**seed_bits)
    nounce_ranges  = create_number_ranges(2**nounce_bits)
    seed_lengths   = [sl for sl in range(0, 2**seed_bits)]
    nounce_lengths = [nl for nl in range(0, 2**nounce_bits)]
    limits         = defaultdict(lambda: defaultdict(Counter))
    for seed_length in seed_lengths:
        min_seed    = seed_ranges[seed_length][0]
        max_seed    = seed_ranges[seed_length][1]
        total_seeds = max_seed - min_seed
        for nounce_length in nounce_lengths:
            min_nounce    = nounce_ranges[nounce_length][0]
            max_nounce    = nounce_ranges[nounce_length][1]
            total_nounces = max_nounce - min_nounce
            item_length   = seed_bits + nounce_bits + seed_length + nounce_length
            value_length  = item_length + 1
            seed_nounces  = total_seeds * total_nounces
            limits[value_length][seed_length].update({ nounce_length: seed_nounces })
    return limits

def has_prefix_item(item_value: int, value_length: int, seed_bits: int, nounce_bits: int, min_value_length: int) -> bool:
    for prev_length in range(min_value_length, (value_length + 1)):
        ValueClass  = get_seed_item_value_class(seed_bits=seed_bits, nounce_bits=nounce_bits, value_length=prev_length)
        prev_value  = item_value % (2**prev_length)
        saved_count = ValueClass.objects(id=prev_value).limit(1).count(with_limit_and_skip=True)
        if (saved_count > 0):
            return True
    return False

def save_seed_item_values(seed_bits: int, nounce_bits: int, value_length: int, item_values: List[BaseSeedItemValue]):
    ValueClass = get_seed_item_value_class(seed_bits=seed_bits, nounce_bits=nounce_bits, value_length=value_length)
    if (len(item_values) > 0):
        QuerySet(ValueClass, collection=ValueClass._get_collection()).insert(item_values, load_bulk=False)
    #total_batches = math.ceil(len(item_values) / SAVE_BATCH_SIZE)
    #for batch_id in range(0, total_batches):
    #    batch_items = item_values[batch_id*SAVE_BATCH_SIZE:batch_id*SAVE_BATCH_SIZE+SAVE_BATCH_SIZE]
    #    if (len(batch_items) > 0):
    #        QuerySet(ValueClass, collection=ValueClass._get_collection()).insert(batch_items, load_bulk=False)
    #    else:
    #        return

def save_data_item_values(data_bits: int, item_values: List[BaseDataItemValue]):
    ValueClass    = get_data_item_value_class(data_bits=data_bits)
    total_batches = math.ceil(len(item_values) / SAVE_BATCH_SIZE)
    for batch_id in range(0, total_batches):
        batch_items = item_values[batch_id*SAVE_BATCH_SIZE:batch_id*SAVE_BATCH_SIZE+SAVE_BATCH_SIZE]
        if (len(batch_items) > 0):
            QuerySet(ValueClass, collection=ValueClass._get_collection()).insert(batch_items, load_bulk=False)
        else:
            return

#@lru_cache(maxsize=2048)
def has_completed_range(seed_bits: int, nounce_bits: int, length_range: LengthRange) -> bool:
    ValueClass        = get_seed_item_value_class(seed_bits=seed_bits, nounce_bits=nounce_bits, value_length=length_range.value_length)
    total_saved_items = ValueClass.objects(
        seed_length   = length_range.seed_length, 
        nounce_length = length_range.nounce_length,
    ).count()
    item_length_limits = get_item_length_limits(seed_bits=seed_bits, nounce_bits=nounce_bits)
    #length_limits   = get_item_length_limits(seed_bits=seed_bits, nounce_bits=nounce_bits)
    #max_range_items = length_limits[length_range.value_length][length_range.seed_length][length_range.nounce_length]
    #pprint(length_range)
    #print(f"total_saved_items={total_saved_items}, length_range.total_items={length_range.total_items}, length_range.item_length={length_range.item_length}, length_range.value_length={length_range.value_length}, limits={item_length_limits[length_range.value_length][length_range.seed_length][length_range.nounce_length]}")
    if (total_saved_items > 0) and (total_saved_items == item_length_limits[length_range.value_length][length_range.seed_length][length_range.nounce_length]):
        #print(f"total_saved_items={total_saved_items}, length_range.total_items={length_range.total_items}, length_range.item_length={length_range.item_length}, length_range.value_length={length_range.value_length}, limits={item_length_limits[length_range.value_length][length_range.seed_length][length_range.nounce_length]}")
        return True
    return False

#@lru_cache(maxsize=2048)
def has_partial_range(seed_bits: int, nounce_bits: int, length_range: LengthRange) -> bool:
    #length_limits     = get_item_length_limits(seed_bits=seed_bits, nounce_bits=nounce_bits)
    #max_range_items   = length_limits[length_range.item_length]
    if (has_completed_range(seed_bits=seed_bits, nounce_bits=nounce_bits, length_range=length_range)):
        return False
    ValueClass        = get_seed_item_value_class(seed_bits=seed_bits, nounce_bits=nounce_bits, value_length=length_range.value_length)
    total_saved_items = ValueClass.objects(
        seed_length   = length_range.seed_length, 
        nounce_length = length_range.nounce_length,
    ).count()
    if (total_saved_items > 0):
        return True
    #print(f"total_saved_items={total_saved_items}, length_range.total_items={length_range.total_items}")
    return False

def drop_partial_range(seed_bits: int, nounce_bits: int, length_range: LengthRange):
    ValueClass = get_seed_item_value_class(seed_bits=seed_bits, nounce_bits=nounce_bits, value_length=length_range.value_length)
    #if (has_partial_range(seed_bits=seed_bits, nounce_bits=nounce_bits, length_range=length_range) is False):
    #    pprint(length_range)
    #    raise Exception("Item range is not partial: deleting aborted")
    deleted_values    = defaultdict(set)
    batch_size        = 100000
    total_saved_items = ValueClass.objects(
        seed_length   = length_range.seed_length, 
        nounce_length = length_range.nounce_length,
    ).count()
    if (total_saved_items == 0):
        return deleted_values
    total_batches = math.ceil(total_saved_items / batch_size) + 1
    progress      = tqdm(range(0, total_batches), miniters=1, smoothing=0)
    progress.set_description_str(f"Deleting: s={seed_bits}, n={nounce_bits}, value_length={length_range.value_length} ({total_saved_items} items)")
    for batch_id in progress:
        total_items_left = ValueClass.objects(
            seed_length   = length_range.seed_length, 
            nounce_length = length_range.nounce_length,
        ).count()
        progress.set_description_str(f"Deleting: s={seed_bits}, n={nounce_bits}, value_length={length_range.value_length} ({total_items_left} of {total_saved_items} items left)")
        saved_items = ValueClass.objects(
            seed_length   = length_range.seed_length, 
            nounce_length = length_range.nounce_length,
        ).only('id').limit(batch_size)
        for saved_item in saved_items:
            deleted_values[length_range.value_length].add(saved_item.id)
            saved_item.delete()
    return deleted_values
    

def sync_item_ranges(seed_bits: int, nounce_bits: int, value_length: int):
    RangeClass     = get_item_range_class(seed_bits=seed_bits, nounce_bits=nounce_bits)
    seed_ranges    = create_number_ranges(2**seed_bits)
    nounce_ranges  = create_number_ranges(2**nounce_bits)
    seed_lengths   = [sl for sl in range(0, 2**seed_bits)]
    nounce_lengths = [nl for nl in range(0, 2**nounce_bits)]
    item_range_id  = 0

    for seed_length in seed_lengths:
        min_seed    = seed_ranges[seed_length][0]
        max_seed    = seed_ranges[seed_length][1]
        total_seeds = max_seed - min_seed
        for nounce_length in nounce_lengths:
            min_nounce    = nounce_ranges[nounce_length][0]
            max_nounce    = nounce_ranges[nounce_length][1]
            total_nounces = max_nounce - min_nounce
            item_length   = seed_bits + nounce_bits + seed_length + nounce_length
            total_items   = total_seeds * total_nounces
            value_length  = item_length + 1
            if (value_length > 32):
                value_length = 32
            item_score = value_length - item_length
            ValueClass = get_seed_item_value_class(seed_bits=seed_bits, nounce_bits=nounce_bits, value_length=value_length)
            item_range = {
                'id'             : item_range_id,
                'seed_length'    : seed_length,
                'nounce_length'  : nounce_length,
                'value_length'   : value_length,
                'item_length'    : item_length,
                'item_score'     : item_score,
                'min_seed'       : min_seed,
                'max_seed'       : max_seed,
                'total_seeds'    : total_seeds,
                'min_nounce'     : min_nounce,
                'max_nounce'     : max_nounce,
                'total_nounces'  : total_nounces,
                'total_items'    : total_items,
                'saved_items'    : 0,
                'completed'      : False,
            }
            pprint(item_range)
            item_range_id += 1

def load_item_cache(seed_bits: int, nounce_bits: int, max_cached_length: int=MAX_CACHED_LENGTH) -> Dict[int, BinaryArraySet]:
    min_cached_length = seed_bits + nounce_bits + 1
    cached_values     = defaultdict(BinaryArraySet)
    batch_size        = 100000
    for value_length in range(min_cached_length, max_cached_length+1):
        ValueClass  = get_seed_item_value_class(seed_bits=seed_bits, nounce_bits=nounce_bits, value_length=value_length)
        total_items = ValueClass.objects.count()
        if (total_items == 0):
            continue
        total_batches = math.ceil(total_items / batch_size)
        progress      = tqdm(range(0, total_batches), smoothing=0)
        for batch_id in progress:
            skip        = batch_id * batch_size
            #saved_items = ValueClass.objects.only('id').skip(skip).limit(batch_size)
            saved_item_ids = ValueClass.objects.scalar('id').skip(skip).limit(batch_size)
            for saved_item_id in saved_item_ids:
                cached_values[value_length].add_unique(saved_item_id)
            progress.set_description_str(f"value_length={value_length}/{max_cached_length} (loaded {len(cached_values[value_length])} of {total_items} items: {list(cached_values[value_length])[0:4]}...)")
    return cached_values

def discover_seed_item_values(seed_bits: int, nounce_bits: int, min_item_length: int=MIN_ITEM_LENGTH, max_item_length: int=MAX_ITEM_LENGTH):
    seed_ranges    = create_number_ranges(2**seed_bits)
    nounce_ranges  = create_number_ranges(2**nounce_bits)
    seed_lengths   = [sl for sl in range(0, 2**seed_bits)]
    nounce_lengths = [nl for nl in range(0, 2**nounce_bits)]

    item_length_counts  = Counter()
    item_length_limits  = Counter()
    cache_length_counts = Counter()
    item_length_ranges  = defaultdict(list)
    max_item_values     = 0
    #min_item_length    = seed_bits + nounce_bits + 1
    
    for seed_length in seed_lengths:
        min_seed    = seed_ranges[seed_length][0]
        max_seed    = seed_ranges[seed_length][1]
        total_seeds = max_seed - min_seed
        for nounce_length in nounce_lengths:
            min_nounce    = nounce_ranges[nounce_length][0]
            max_nounce    = nounce_ranges[nounce_length][1]
            total_nounces = max_nounce - min_nounce
            item_length   = seed_bits + nounce_bits + seed_length + nounce_length
            total_items   = total_seeds * total_nounces
            value_length  = item_length + 1
            if (value_length > 32):
                value_length = 32
            item_score = value_length - item_length
            # убираем из области поиска все диапазоны порождающие значения с длиной больше разрешенной 
            if (item_length > max_item_length):
                continue
            item_length_counts.update({ item_length: 1 })
            item_length_limits.update({ item_length: total_items })
            max_item_values += total_items
            
            item_length_ranges[item_length].append(LengthRange(
                seed_length   = seed_length,
                nounce_length = nounce_length,
                item_length   = item_length,
                total_seeds   = total_seeds,
                total_nounces = total_nounces,
                total_items   = total_items,
                value_length  = value_length,
                item_score    = item_score,
            ))
    
    item_lengths      = list(item_length_ranges.keys())
    total_item_values = max_item_values
    duplicate_matches = 0
    unique_count      = 0
    saved_count       = 0
    cache_hits        = 0
    if (total_item_values > 2**32):
        total_item_values = 2**32
    pending_items  = list()
    pending_values = set()
    progress       = tqdm(total=2**32, initial=0, mininterval=0.5, smoothing=0.05, dynamic_ncols=True, nrows=40)

    for item_length in item_lengths:
        if (item_length < min_item_length):
            continue
        for length_range in item_length_ranges[item_length]:
            if (has_completed_range(seed_bits=seed_bits, nounce_bits=nounce_bits, length_range=length_range)):
                print(f"skipped value_length={length_range.value_length}: seed_length={length_range.seed_length}, nounce_length={length_range.nounce_length}")
                continue
            if (has_partial_range(seed_bits=seed_bits, nounce_bits=nounce_bits, length_range=length_range)):
                print(f"clearing value_length={length_range.value_length}: seed_length={length_range.seed_length}, nounce_length={length_range.nounce_length}  (partial)")
                dropped_values  = drop_partial_range(seed_bits=seed_bits, nounce_bits=nounce_bits, length_range=length_range)
                #dropped_lengths = list(dropped_values.keys())
                #for dropped_length in dropped_lengths:
                #    print(f"clearing value_length={length_range.value_length}: removed {len(dropped_values[dropped_length])} items from cache")
                #    for dropped_value in dropped_values[dropped_length]:
                #        cached_values[dropped_length].remove(dropped_value)
                dropped_values.clear()
    
    # load cache
    cached_values  = load_item_cache(seed_bits=seed_bits, nounce_bits=nounce_bits, max_cached_length=MAX_CACHED_LENGTH) #defaultdict(set)
    cached_lengths = sorted(list(cached_values.keys()))

    # start scan
    for item_length in item_lengths:
        if (item_length < min_item_length):
            print(f"Skipping item_length={item_length} (min_item_length={min_item_length})")
            continue
        length_range_id     = 0
        max_length_range_id = len(item_length_ranges[item_length]) - 1
        if (len(pending_items) > 0):
            save_seed_item_values(seed_bits=seed_bits, nounce_bits=nounce_bits, value_length=length_range.value_length, item_values=pending_items)
            pending_items.clear()
            pending_values.clear()
        for length_range in item_length_ranges[item_length]:
            ValueClass = get_seed_item_value_class(seed_bits=seed_bits, nounce_bits=nounce_bits, value_length=length_range.value_length)
            if (has_completed_range(seed_bits=seed_bits, nounce_bits=nounce_bits, length_range=length_range)):
                unique_item_values = 2**(32 - length_range.value_length) * length_range.total_items
                print(f"skipped item_length={item_length}: value_length={length_range.value_length}, seed_length={length_range.seed_length}, nounce_length={length_range.nounce_length} (total_items={length_range.total_items})")
                progress.update(unique_item_values)
                length_range_id += 1
                unique_count    += unique_item_values
                saved_count     += length_range.total_items
                continue
            min_seed   = seed_ranges[length_range.seed_length][0]
            max_seed   = seed_ranges[length_range.seed_length][1]
            seeds      = range(min_seed, max_seed)
            progress.set_description_str(f"il={item_length} ({length_range_id}/{max_length_range_id}), vl={length_range.value_length}, sl={length_range.seed_length}, nl={length_range.nounce_length}")
            for seed in seeds:
                min_nounce = nounce_ranges[length_range.nounce_length][0]
                max_nounce = nounce_ranges[length_range.nounce_length][1]
                nounces    = range(min_nounce, max_nounce)
                for nounce in nounces:
                    shift_size = 0
                    while (True):
                        if (shift_size >= ((2**32 / (2**(2**seed_bits))) - 1)):
                            save_seed_item_values(seed_bits=seed_bits, nounce_bits=nounce_bits, value_length=length_range.value_length, item_values=pending_items.copy())
                            pending_items.clear()
                            pending_values.clear()
                            raise Exception(f"Shift limit exeeded: shift_size={shift_size}")
                        #hash_nounce = nounce + (2**(2**nounce_bits)) * nounce_shift
                        #hash_input  = hash_nounce.to_bytes(length=HASH_INPUT_BYTES, byteorder='little', signed=False)
                        #digest      = xxhash.xxh32_intdigest(input=hash_input, seed=seed)
                        hash_seed   = seed + (2**(2**seed_bits)) * shift_size
                        hash_input  = nounce.to_bytes(length=HASH_INPUT_BYTES, byteorder='little', signed=False)
                        digest      = xxhash.xxh32_intdigest(input=hash_input, seed=hash_seed)
                        if (length_range.value_length < 32):
                            item_value = digest % (2**length_range.value_length)
                        else:
                            item_value = digest
                        has_cached_value = False
                        for cached_length in cached_lengths:
                            cached_value = item_value % (2**cached_length)
                            if (cached_value in cached_values[cached_length]):
                                has_cached_value = True
                                break
                        if (has_cached_value):
                            shift_size        += 1
                            duplicate_matches += 1
                            cache_hits        += 1
                            cache_length_counts.update({ cached_length: 1 })
                            continue
                        if (has_cached_value is False) and (length_range.value_length <= MAX_CACHED_LENGTH):
                            break
                        if (item_value in pending_values):
                            shift_size        += 1
                            duplicate_matches += 1
                            cache_hits        += 1
                            continue
                        has_prefix = has_prefix_item(
                            item_value       = item_value, 
                            value_length     = length_range.value_length, 
                            seed_bits        = seed_bits, 
                            nounce_bits      = nounce_bits, 
                            min_value_length = MAX_CACHED_LENGTH + 1, #min(item_lengths)
                        )
                        if (has_prefix is False):
                            break
                        shift_size        += 1
                        duplicate_matches += 1
                    # save unique item
                    seed_item_value = ValueClass(
                        id             = item_value,
                        seed           = seed,
                        seed_length    = length_range.seed_length,
                        seed_shift     = shift_size,
                        shifted_seed   = hash_seed,
                        nounce         = nounce,
                        nounce_length  = length_range.nounce_length,
                        #nounce_shift   = 0,
                        #shifted_nounce = nounce,
                        value_length   = length_range.value_length,
                        item_length    = item_length,
                        score          = length_range.item_score,
                    )
                    if (length_range.value_length <= MAX_CACHED_LENGTH):
                        cached_values[length_range.value_length].add_unique(item_value)
                        if (length_range.value_length not in cached_lengths):
                            #cached_lengths.append(length_range.value_length)
                            cached_lengths = sorted(list(cached_values.keys()))
                    # add new value to save queue
                    pending_items.append(seed_item_value)
                    pending_values.add(item_value)
                    # save pending items in batch
                    if (len(pending_items) >= SAVE_BATCH_SIZE):
                        save_seed_item_values(seed_bits=seed_bits, nounce_bits=nounce_bits, value_length=length_range.value_length, item_values=pending_items)
                        pending_items.clear()
                        pending_values.clear()
                        progress.set_postfix({
                            'dp'          : f"{duplicate_matches}",
                            'uq'          : f"{unique_count} ({(unique_count/2**32)*100:2.5f}%)",
                            'sav'         : f"{saved_count}",
                            's'           : f"{seed}/{max_seed}",
                            'n'           : f"{nounce}/{max_nounce}",
                            #'shift_size'         : f"{shift_size}",
                            #'pending_items'      : f"{len(pending_items)}",
                            'cc'          : f"{cache_hits}",
                            'clc'         : f"{cache_length_counts.first_items()}",
                            'sc'          : f"{length_range.item_score}",
                        }, refresh=True)
                    unique_item_values = 2**(32 - length_range.value_length)
                    progress.update(unique_item_values)
                    unique_count += unique_item_values
                    saved_count  += 1
                    if (unique_count >= (2**32 - 8)):
                        save_seed_item_values(seed_bits=seed_bits, nounce_bits=nounce_bits, value_length=length_range.value_length, item_values=pending_items)
                        pending_items.clear()
                        pending_values.clear()
                        raise Exception(f"SCAN FINISHED, unique_count={unique_count}")
                        #break
            # save remaining range values if any
            if (len(pending_items) > 0):
                save_seed_item_values(seed_bits=seed_bits, nounce_bits=nounce_bits, value_length=length_range.value_length, item_values=pending_items)
                pending_items.clear()
                pending_values.clear()
                progress.set_postfix({
                    'dp'          : f"{duplicate_matches}",
                    'uq'          : f"{unique_count} ({(unique_count/2**32)*100:2.5f}%)",
                    'sav'         : f"{saved_count}",
                    's'           : f"{seed}/{max_seed}",
                    'n'           : f"{nounce}/{max_nounce}",
                    'cc'          : f"{cache_hits}",
                    'clc'         : f"{cache_length_counts.first_items()}",
                    'sc'          : f"{length_range.item_score}",
                }, refresh=True)
            length_range_id += 1
            progress.refresh()
        if (len(pending_items) > 0):
            save_seed_item_values(seed_bits=seed_bits, nounce_bits=nounce_bits, value_length=length_range.value_length, item_values=pending_items)
            pending_items.clear()
            pending_values.clear()

def get_remaining_data_items(data_bits: int, start_position: int, target_values: Set[int]=None) -> Union[BaseDataItemValue, Document]:
    ValueClass = get_data_item_value_class(data_bits=data_bits)
    if (target_values is None):
        return ValueClass.objects(
            id__gte = start_position,
        )
    else:
        return ValueClass.objects(
            id__gte   = start_position,
            value__in = target_values,
        )

def count_remaining_data_items(data_bits: int, start_position: int, target_values: Set[int]=None) -> Counter:
    ValueClass = get_data_item_value_class(data_bits=data_bits)
    if (target_values is None):
        data_items = ValueClass.objects(
            id__gte = start_position,
        ).item_frequencies('value')
    else:
        data_items = ValueClass.objects(
            id__gte   = start_position,
            value__in = target_values,
        ).item_frequencies('value')
    return Counter(data_items)

def fill_data_items(data: frozenbitarray, min_data_item_length: int, max_data_item_length: int):
    for item_length in range(min_data_item_length, max_data_item_length):
        print(f"item_length={item_length} bits, ({(item_length/(max_data_item_length-1))*100:2.4f}% of {(max_data_item_length-1)} bits)")
        pending_items = list()
        ValueClass    = get_data_item_value_class(data_bits=item_length)
        item_counter  = Counter()
        progress      = tqdm(range(0, len(data)-item_length), mininterval=0.5, smoothing=0)
        for item_id in progress:
            start_bit  = item_id
            end_bit    = item_id + item_length
            data_item  = data[start_bit:end_bit]
            item_value = ba2int(data_item, signed=False)
            # create data item
            data_item_value = ValueClass(
                id    = item_id,
                value = item_value,
            )
            item_counter.update({ item_value: 1 })
            if (len(pending_items) < SAVE_BATCH_SIZE):
                pending_items.append(data_item_value)
            else:
                save_data_item_values(data_bits=item_length, item_values=pending_items.copy())
                pending_items.clear()
                progress.set_postfix({
                    'counts': f"{item_counter.most_common(8)} ({len(item_counter)})"
                }, refresh=False)
        # save remaining items
        if (len(pending_items) > 0):
            print(f"Saving remaining items: {len(pending_items)} (item_length={item_length} bits)")
            save_data_item_values(data_bits=item_length, item_values=pending_items.copy())
            pending_items.clear()
            progress.set_postfix({
                'counts': f"{item_counter.most_common(8)} ({len(item_counter)})"
            }, refresh=True)
    # save remaining items
    if (len(pending_items) > 0):
        print(f"Saving remaining items: {len(pending_items)} (item_length={item_length} bits)")
        save_data_item_values(data_bits=item_length, item_values=pending_items.copy())
        pending_items.clear()
        progress.set_postfix({
            'counts': f"{item_counter.most_common(8)} ({len(item_counter)})"
        }, refresh=True)