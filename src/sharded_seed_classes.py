# https://docs.mongoengine.org/
from mongoengine import Document, QuerySet, LongField, IntField, BinaryField, StringField, DictField, ListField, BooleanField
#connect(db='blib', host='127.0.0.1', port=27017, maxPoolSize=300)
from typing import List, Dict, Set, Tuple, Optional, Union, Iterable
# https://docs.python.org/3/library/dataclasses.html
from dataclasses import dataclass
from custom_counter import CustomCounter as Counter

### DOCUMENT COLLECTIONS ###

class SeedValueQuerySet(QuerySet):
    """
    Дополнительные методы для поиска значений в базе
    """
    
    def get_last_position(self, value_id: str) -> int:
        """
        Найти первое упоминание значения
        """
        item = self.filter(id__startswith=value_id).order_by('-position').limit(1).first()
        if (item is None):
            return 0
        return item.position
    
    def get_first_position(self, value_id: str) -> int:
        """
        Найти последнее известное упоминание значения
        """
        item = self.filter(id__startswith=value_id).order_by('position').limit(1).first()
        if (item is None):
            return 0
        return item.position
    
    def get_longest_parent(self, value_id: str, min_length: int=2) -> Document:
        """
        Найти самое длинное предшествующее значение
        """
        parents = self.parent_values(value_id=value_id, min_length=min_length).all()
        if (len(parents) == 0):
            return None
        best_parent = None
        best_length = None
        for parent in parents:
            parent_length = len(parent.id)
            if (best_length is None) or (parent_length > best_length):
                best_length = parent_length
                best_parent = parent
        return best_parent
    
    def get_shortest_parent(self, value_id: str, min_length: int=2) -> Document:
        """
        Найти самое короткое предшествующее значение
        """
        parents = self.parent_values(value_id=value_id, min_length=min_length).all()
        if (len(parents) == 0):
            return None
        best_parent = None
        best_length = None
        for parent in parents:
            parent_length = len(parent.id)
            if (parent_length < best_length) or (best_length is None):
                best_length = parent_length
                best_parent = parent
        return best_parent
    
    def is_completed(self, value_id: str) -> bool:
        """
        Определить, найдены ли все потомки для указаннного значения
        """
        children_count = self.count_children_values(value_id=value_id)
        if (children_count < 256):
            return False
        return True
    
    def get_children_ids(self, value_id: str) -> Set[str]:
        """
        Получить id всех прямых потомков указанного значения (всегда возвращет 256 элементов)
        """
        children_ids = set()
        for i in range(0, 256):
            next_byte_id   = i.to_bytes(length=1, byteorder='big', signed=False).hex()
            child_value_id = str().join([value_id, next_byte_id])
            children_ids.add(child_value_id)
        return children_ids

    def get_parent_id(self, value_id: str, parent_length: int=None)-> str:
        value_length = (len(value_id) // 2)
        if (value_length == 0) or (value_length == 1):
            parent_value_id = ''
        elif (value_length == 2):
            parent_value_id = value_id[0:2]
        else:
            if (parent_length is None):
                parent_length    = value_length - 1
                parent_id_length = parent_length * 2
            parent_value_id = value_id[0:parent_id_length]
        return parent_value_id
    
    def get_parent_ids(self, value_id: str, min_length: int=2)-> List[str]:
        parent_ids   = list()
        value        = bytes().fromhex(value_id)
        value_length = len(value)
        if (value_length <= 2):
            parent_ids.append(self.get_parent_id(value_id=value_id))
            return parent_ids
        for parent_length in range(min_length, value_length):
            parent_value = value[0:parent_length]
            parent_id    = parent_value.hex()
            parent_ids.append(parent_id)
        return parent_ids
    
    def parent_value(self, value_id: str, parent_length: int=None) -> QuerySet:
        """
        Find parent for given value
        """
        parent_value_id = self.get_parent_id(value_id=value_id, parent_length=parent_length)
        return self.filter(id=parent_value_id)
    
    def parent_values(self, value_id: str, min_length: int=1) -> QuerySet:
        """
        Find all parents for given value
        """
        parent_ids = self.get_parent_ids(value_id=value_id, min_length=min_length)
        return self.filter(id__in=parent_ids)
    
    def children_values(self, value_id: str) -> QuerySet:
        """
        Find all located children for given value
        """
        children_ids = self.get_children_ids(value_id=value_id)
        return self.filter(id__in=children_ids)
    
    def value_instances(self, value_id: str, min_value_length: int=None, max_value_length: int=None) -> QuerySet:
        """
        Найти все известные упоминания этого значения (и его потомков) в пространстве хешей
        """
        if (min_value_length is None):
            min_value_length = (len(value_id) // 2)
        if (max_value_length is None):
            return self.filter(id__startswith=value_id, value_length__gte=min_value_length)
        return self.filter(id__startswith=value_id, value_length__gte=min_value_length, value_length__lte=max_value_length)
    
    def basis_values(self, value_id: str) -> QuerySet:
        """
        Value itself + all children
        """
        basis_value_ids = set()
        children_ids    = self.get_children_ids(value_id=value_id)
        basis_value_ids.add(value_id)
        basis_value_ids.update(children_ids)
        return self.filter(id__in=basis_value_ids)
    
    def count_children_values(self, value_id: str) -> int:
        """
        Подсчитать количество найденных прямых потомков указанного значения
        """
        return self.children_values(value_id=value_id).limit(256).count(with_limit_and_skip=True)
    
class BaseSeedValue(Document):
    """
    Уникальное значение в пространстве хешей
    """
    #id           = BinaryField(max_bytes=8, primary_key=True)
    id           = StringField(max_length=16, primary_key=True)
    position     = LongField(min_value=0)
    value_length = IntField(min_value=1, max_value=16, choices=(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16))
    meta = {
        'queryset_class': SeedValueQuerySet,
        'abstract': True,
        'indexes': [
            'position',
            'value_length',
        ],
    }

class BaseSeedValuePosition(Document):
    """
    Position inside seed hash space: stores maximum value length that has been saved at this position
    """
    id   = LongField(min_value=0, primary_key=True)
    tier = IntField(min_value=0, max_value=16)
    meta = {
        'abstract': True,
        'indexes': [
            'tier',
        ],
    }

class BaseValueInstance(Document):
    # using value position as unique instance id
    id       = LongField(min_value=0, primary_key=True)
    #value_id = BinaryField(max_bytes=8)
    value_id = StringField(max_bytes=8)
    meta = {
        'abstract': True,
        'indexes': [
            'value_id',
        ],
    }

class ShardedStorageTask(Document):
    id           = LongField(min_value=0, primary_key=True)
    parent_id    = LongField(min_value=0)
    name         = StringField()
    handler_type = StringField()
    payload      = DictField()
    total        = LongField()
    completed    = LongField()
    status       = StringField()
    meta = {
        'indexes': [
            'parent_id', 
            'name', 
            'handler_type',
            'status',
        ],
    }

### DATACLASSES ###

@dataclass()
class ValueStats:
    seed_id: int
    value_id: bytes
    parent_value_id: int
    is_root: bool
    value_length: int
    address_length: int
    parent_length: int
    children_length: int
    # first occurance of this value (position of the first known instance)
    min_position: int
    # last known occurance of this value (position of the last known instance)
    max_position: int
    total_instances: int
    # distance: number of positions between 2 value instances
    min_distance: int
    max_distance: int
    avg_distance: int
    # expected distance (probability of getting this value)
    base_distance: int
    located_children: List[bytes]
    remaining_children: Set[bytes]
    # 0-256 (first child allways have same position)
    located_children_count: int
    remaining_children_count: int
    last_child_position: int
    last_child_value_id: bytes
    # value score: difference (in bytes) between address length and value length
    score: int
    parent_score: int
    min_child_score: int
    max_child_score: int
    child_scores: Dict[bytes, int]
    child_score_counts: Counter
    # value cost (volume of calculations required to locate this value)
    cost: int
    # tiers, containing this value
    min_instance_tier: int
    max_instance_tier: int
    instance_tiers: Dict[int, int]
    instance_tier_counts: Counter
    # tier usage data
    min_child_tier: int
    max_child_tier: int
    child_tiers: Dict[bytes, int]
    child_tier_counts: Counter

@dataclass()
class TierStats:
    # s0a1v2
    tier_id: str
    seed_id: int
    value_length: int
    address_length: int
    # tier boundaries (end=start+size)
    start_position: int
    end_position: int
    # score required for all values
    target_score: int
    # values for search
    target_value_ids: Set[bytes]
    target_parent_value_ids: Set[bytes]
    # number of values located during scan
    total_values: int
    min_value_position: int
    max_value_position: int
    # value_length:total_instances
    total_instances: Counter
    min_instance_position: int
    max_instance_position: int
    # value_id:total_instances
    value_counts: Counter
    # value_length[value_id:total_instances]
    instance_counts: Dict[int, Counter]
    # entire segment status: 
    # - draft (0 processed segments)
    # - processing (1 or more segments processed)
    # - finished (all segments completed)
    status: str
    # total number of positions in tier
    tier_size: int
    # total number of positions in one segment (for parallel processing)
    segment_size: int
    # tier_size/segment_size
    total_segments: int
    # segment_number:is_done
    segment_bitmap: Dict[int, bool]

@dataclass()
class SeedStats:
    seed_id: int
    # value_length: value count
    total_values: Dict[int, int]
    # value_length[value_score:value_ids]
    value_scores: Dict[int, Dict[int, Set[bytes]]]
    # value_length[value_score:total_values]
    value_score_counts: Dict[int, Dict[int, int]]

### CACHE ###

CACHED_CLASSES     = dict()
CACHED_CLASS_NAMES = set()

def has_cached_class(class_name: str) -> bool:
    return (class_name in CACHED_CLASS_NAMES)

def get_cached_class(class_name: str) -> Union[BaseSeedValue, BaseValueInstance, Document]:
    return CACHED_CLASSES.get(class_name)

### COLLECTION FACTORIES ###

def get_value_class_name(prefix_id: str, seed: int=0) -> str:
    return f"SeedValue0x{prefix_id}S{seed}"
    
def get_position_class_name(prefix_id: str, seed: int=0) -> str:
    return f"SeedValue0x{prefix_id}S{seed}Position"

def get_instance_class_name(value_length: int, prefix_id: str, seed: int=0) -> str:
    return f"ValueInstance0x{prefix_id}L{value_length}S{seed}"

def get_value_collection_name(prefix_id: str, seed: int=0) -> str:
    return f"value_0x{prefix_id}_s{seed}"

def get_position_collection_name(prefix_id: str, seed: int=0) -> str:
    return f"value_0x{prefix_id}_s{seed}_position"

def get_instance_collection_name(value_length: int, prefix_id: str, seed: int=0) -> str:
    return f"instance_0x{prefix_id}_l{value_length}_s{seed}"

def make_value_class(prefix_id: str, seed: int=0) -> BaseSeedValue:
    # creating class dynamically (OMG, Python, you are AWESOME!)
    # https://www.geeksforgeeks.org/create-classes-dynamically-in-python/
    return type(get_value_class_name(prefix_id, seed), (BaseSeedValue, ), {
        "meta" : {
            'queryset_class': SeedValueQuerySet,
            'collection': get_value_collection_name(prefix_id, seed),
        }
    })

def make_position_class(prefix_id: str, seed: int=0) -> BaseSeedValue:
    return type(get_position_class_name(prefix_id, seed), (BaseSeedValuePosition, ), {
        "meta" : {
            'collection': get_position_collection_name(prefix_id, seed),
        }
    })

def make_instance_class(value_length: int, prefix_id: str, seed: int=0) -> BaseValueInstance:
    return type(get_instance_class_name(value_length, prefix_id, seed), (BaseValueInstance, ), {
        "meta" : {
            'collection': get_instance_collection_name(value_length, prefix_id, seed),
        }
    })

def get_seed_value_class(prefix_id: str, seed: int=0) -> BaseSeedValue:
    class_name = get_value_class_name(prefix_id, seed)
    if (has_cached_class(class_name) is False):
        # cache constructed class
        value_class = make_value_class(prefix_id=prefix_id, seed=seed)
        CACHED_CLASSES[class_name] = value_class
        CACHED_CLASS_NAMES.add(class_name)
    return get_cached_class(class_name)

def get_seed_value_position_class(prefix_id: str, seed: int=0) -> BaseSeedValuePosition:
    class_name = get_position_class_name(prefix_id, seed)
    if (has_cached_class(class_name) is False):
        # cache constructed class
        position_class = make_position_class(prefix_id=prefix_id, seed=seed)
        CACHED_CLASSES[class_name] = position_class
        CACHED_CLASS_NAMES.add(class_name)
    return get_cached_class(class_name)

def get_value_instance_class(value_length: int, prefix_id: str, seed: int=0) -> BaseValueInstance:
    class_name = get_instance_class_name(value_length, prefix_id, seed)
    if (has_cached_class(class_name) is False):
        # cache constructed class
        instance_class = make_instance_class(value_length=value_length, prefix_id=prefix_id, seed=seed)
        CACHED_CLASSES[class_name] = instance_class
        CACHED_CLASS_NAMES.add(class_name)
    return get_cached_class(class_name)

### CURRENT STATE INFO COLLECTION AND TASK PLANNING ###



### TASK HANDLERS ###

class InitSeedHandler():
    pass

class InitValueHandler():
    pass

class PrepareTaskHandler():
    pass

class CompleteTaskHandler():
    pass

class ProcessSeeedHandler():
    pass

class ProcessTierHandler():
    pass

class ProcessPositionRangeHandler():
    pass

class ProcessValueHandler():
    pass

class ProcessInstanceHandler():
    pass

class CollectRootValuesHandler():
    pass

class CollectSeedValuesHandler():
    pass

class CollectTierValuesHandler():
    pass

class CollectNestedValuesHandler():
    pass

class CollectValueInstancesHandler():
    pass

class ProvideMinSeedScoreHandler():
    pass

class ProvideMinTierScoreHandler():
    pass

class ProvideMinValueScoreHandler():
    pass

class UpdateSeedStatsHandler():
    pass

class UpdateTierStatsHandler():
    pass

class UpdateValueStatsHandler():
    pass