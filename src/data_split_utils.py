# https://github.com/Textualize/rich
from rich import print, inspect
# https://rich.readthedocs.io/en/latest/pretty.html
from rich.pretty import pprint
# https://rich.readthedocs.io/en/latest/traceback.html
from rich.traceback import install
install(show_locals=True)
# https://rich.readthedocs.io/en/latest/logging.html
import logging
from rich.logging import RichHandler
logging.basicConfig(
    level="INFO",
    format="%(message)s",
    datefmt="[%X]",
    handlers=[RichHandler(rich_tracebacks=True, locals_max_length=16)]
)
log = logging.getLogger("rich")
# https://github.com/tqdm/tqdm
import tqdm
# https://realpython.com/python-data-classes/
# https://realpython.com/python-data-classes/#advanced-default-values
from dataclasses import dataclass, make_dataclass, field
# https://bitstring.readthedocs.io/en/latest/index.output_item_idhtml
from bitstring import Bits, BitArray, BitStream, ConstBitStream
# https://pythongeeks.org/ordereddict-in-python/
# https://www.geeksforgeeks.org/counters-in-python-set-1/
# https://docs.python.org/3/library/collections.html#counter-objects
# https://realpython.com/python-namedtuple/
# https://realpython.com/linked-lists-python/
from collections import OrderedDict, Counter, defaultdict, namedtuple, deque
# https://docs.python.org/3/library/typing.html
# https://realpython.com/python-data-classes/#more-flexible-data-classes
from typing import List, Set, Dict, Tuple, Optional, Union, Deque, Literal
# https://www.geeksforgeeks.org/floor-ceil-function-python/?ref=lbp
import math
# https://docs.python.org/3/library/operator.html#module-operator
import operator
# https://habr.com/ru/company/timeweb/blog/564826/
# https://docs.python.org/3/library/enum.html
import enum
from enum import Enum
import os, sys
if not 'workbookDir' in globals():
    workbookDir = os.getcwd()
os.chdir(workbookDir)

# функции для работы с хеш-пространством
from hash_space_utils import HashItemAddress, HashItemPosition, HashSegmentAddress, PositionSeed, \
    DEFAULT_VALUE_STEP, DEFAULT_POSITION_STEP, HASH_DIGEST_BITS, \
    split_data, count_segment_items, count_split_values, collect_split_positions, \
    get_min_bit_length, get_aligned_bit_length, \
    read_hash_item, read_hash_segment, \
    get_item_based_data_length_from_number, \
    find_value_in_segment
# функции преобразования в varint и обратно
from archive.custom_varint_encoding import ItemLengthOptions, create_value_length_options, \
    get_mapping_from_value_length_to_prefix, get_prefix_length_for_options
# результат разбиения данных на элементы с одновременным заполнением словаря
VarLengthSplitResult   = namedtuple('VarLengthSplitResult', ['data_items', 'data_dict', 'suffix_item'])
FixedLengthSplitResult = namedtuple('FixedLengthSplitResult', ['data_items', 'data_dict', 'suffix_item', 'value_length'])

class SplitMode(Enum):
    """Режим разбиения данных на элементы"""
    # стандартное разбиение: все элементы одинаковой длины
    SAME_LENGTH : 0
    # разбиение на элементы с использованием обратных ссылок: новые элементы добавляются как комбинация любых двух старых
    # элементов которые находятся в этих же данных
    SAME_ORIGIN : 1

class SplitType(Enum):
    """Тип разбиения на элементы"""
    # все элементы одной длины
    FIXED_LENGTH    = 0
    # элементы разной длины
    VARIABLE_LENGTH = 1

class NewItemLengthSplitStrategy(Enum):
    """
    Стратегия выбора длины нового элемента при разбиении данных на элементы (если используются элементы с разной длиной)
    """
    # сначала использовать длинные элементы, а если их нет - искать более короткие
    SHORT_ITEMS_FIRST : 0
    # сначала использовать самые короткие элементы и только если их нет - переходить к боле длинным
    LONG_ITEMS_FIRST  : 1

class NewItemLengthMappingStrategy(Enum):
    """
    Стратегия выбора длины элементов для заполнения словаря уникальными значениями
    """
    # сначала заполнять словарь длинных значений, переходить к заполнению словарей с более короткими значениями
    # только после того как будет полностью заполнен предыдущий словарь (с более длинными значениями)
    SHORT_ITEMS_FIRST : 0
    # сначала заполнять словарь коротких значений, переходить к заполнению словарей с более короткими значениями
    # только если словарь более коротких уже заполнен
    LONG_ITEMS_FIRST  : 1

class NewItemValueStrategy(Enum):
    """
    Стратегия использования ранее найденных элементов при разбиении данных на элементы
    """
    # сначала использовать ранее найденные элементы из словаря, 
    # добавлять в словарь новые элементы только если в словаре не нашлось нужного значения
    PREFER_EXISTING : 0
    # сначала выбирать новые элементы и дополнять словарь, 
    # использовать ранее найденные элементы только когда словарь уже заполнен
    PREFER_NEW      : 1
    # только существующие элементы
    # использовать только элементы из словаря, запретить добавление новых элементов,
    # генерировать исключение если значения в словаре нет, а в данных оно есть
    FORCE_EXISTING  : 2
    # только новые элементы
    # разбивать данные на элементы так, чтобы каждый последующий элемент был уникальным: в этом режиме каждый
    # элемент полученный из данных всегда дополняет словарь новым значением
    # никогда не использовать элементы по второму разу (то есть если он уже есть в словаре - ищем более длинный)
    # генерировать исключение если словарь уже заполнен а данные еще остались
    FORCE_NEW       : 3

class NewItemLengthStrategy(Enum):
    """
    Стратегия выбора длины новых значений при заполнении словаря
    """
    # сначала заполнять словари коротких значений, затем словари более длинных
    SHORT_ITEMS_FIRST : 0
    # сначала заполнять словари длинных значений, затем словари коротких
    LONG_ITEMS_FIRST  : 1

class ItemLengthMappingType(Enum):
    """
    Тип разбиения исходных данных на словари по длине значений
    """
    # один словарь для значений одной длины
    # если в данных содержится больше уникальных значений чем может вместить словарь - то создается следующий словарь
    # с более длинными значениями и заполняется уже он
    # гарантирует использование ровно одного словаря для всех значений одной длины
    SINGLE_MAP : 0
    # несколько словарей для значений одной длины
    # если в данных содержится больше уникальных значений чем может вместить словарь - то создается следующий словарь
    # со значениями такой же длины, а все данные разделяются на непересекающиеся множества: 
    # все элементы относящиеся к первому словарю отделяются от данных второго словаря
    # процесс разделения продолжается до тех пор пока все данные не будут разделены на непересекающиеся области,
    # каждая из областей будет иметь свой словарь
    # гарантирует что все уникальные значения данных будут представлены элементами одинаковой длины
    MULTI_MAP  : 1

@dataclass()
class DataSplit:
    """
    Итоговый результат разбиения, содержащий словарь, адреса элементов и метаданные разбиения
    """

    __slots__ = [
        'split_type',
        'item_mapping',
        'seed_mapping',
        'data_items',
        'item_addresses',
        'item_counts',
        'seed_counts',
        'length_options', 
        'data_length',
        'prefix_length',
        'suffix_item',
        'show_progress',
    ]

    split_type     : SplitType
    item_mapping   : Dict[Bits, HashItemAddress]
    seed_mapping   : Dict[int, int]
    data_items     : List[Bits]
    item_addresses : List[Bits]
    item_counts    : Counter
    seed_counts    : Counter
    length_options : Dict[str, ItemLengthOptions]
    data_length    : int
    prefix_length  : int
    suffix_item    : Bits
    show_progress  : bool

    def __init__(self, data: ConstBitStream, length_options: Dict[str, ItemLengthOptions], show_progress=False):
        split_result        = get_split_result(data, length_options, show_progress=show_progress)
        # fill default values
        self.data_length    = len(data)
        self.length_options = length_options
        self.show_progress  = show_progress
        self.split_type     = get_split_type(length_options)
        self.data_items     = split_result.data_items
        self.item_counts    = count_split_items(split_result.data_items)
        self.item_mapping   = create_data_item_mapping(split_result, length_options, show_progress=show_progress)
        self.item_addresses = collect_item_addresses(split_result, self.item_mapping)
        self.seed_mapping   = create_seed_mapping(self.item_addresses)
        self.seed_counts    = Counter(self.seed_mapping.values())
        self.suffix_item    = split_result.suffix_item
        self.prefix_length  = get_prefix_length_for_options(self.length_options)
        # TODO: reduce prefix length after removing unused options
        #self.filter_length_options()
    
    def filter_length_options(self):
        filtered_keys = set()
        for dict_item in self.item_mapping.keys():
            item_options = get_item_length_options(dict_item, self.length_options)
            filtered_keys.add(item_options.option_index)
        filtered_options = dict()
        for key in filtered_keys:
            filtered_options[key] = self.length_options[key]
        self.length_options = filtered_options
        self.prefix_length  = get_prefix_length_for_options(filtered_options)

def run_fixed_length_split(data: ConstBitStream, item_options: ItemLengthOptions) -> List[BitArray]: # FixedLengthSplitResult
    return split_data(data, item_options.value_length)

def get_max_value_length(length_options: Dict[str, ItemLengthOptions]) -> int:
    return max([item_options.value_length for item_options in length_options.values()])

def get_min_value_length(length_options: Dict[str, ItemLengthOptions]) -> int:
    return min([item_options.value_length for item_options in length_options.values()])

def run_variable_length_split(data: ConstBitStream, length_options: Dict[str, ItemLengthOptions], \
        value_step: int=DEFAULT_VALUE_STEP, show_progress: bool=False) -> VarLengthSplitResult:
    """
    >>> free_space_bits = 2
    >>> min_length      = 8
    >>> max_length      = 32
    >>> position_step   = 8
    >>> value_step      = 8
    >>> length_options  = create_value_length_options(min_length, max_length, free_space_bits, position_step, value_step)
    >>> data            = BitStream(hex='0xdeadbeef0123456789abcdefdeadbeef00112233445566778899aabbccddeeff')
    >>> result          = run_variable_length_split(data, length_options, value_step)
    >>> len(result.data_dict[8])
    16
    >>> len(result.data_dict[16])
    5
    >>> result.data_dict[8]
    {Bits('0x00'), Bits('0x01'), Bits('0x22'), Bits('0x23'), Bits('0x44'), Bits('0x45'), Bits('0x67'), Bits('0x89'), Bits('0xab'), Bits('0xad'), Bits('0xcd'), Bits('0xef'), Bits('0x11'), Bits('0x33'), Bits('0xde'), Bits('0xbe')}
    >>> result.data_dict[16]
    {Bits('0x5566'), Bits('0x7788'), Bits('0x99aa'), Bits('0xbbcc'), Bits('0xddee')}
    >>> len(result.data_items)
    26
    >>> items_data = BitStream().join([item for item in result.data_items])
    >>> items_data
    BitStream('0xdeadbeef0123456789abcdefdeadbeef00112233445566778899aabbccddee')
    >>> result.suffix_item
    BitArray('0xff')
    >>> items_data.append(result.suffix_item)
    >>> items_data == data
    True
    >>> data       = BitStream(hex='0xdeadbeef0123456789abcdefdeadbeef00112233445566778899aabbccddeeff42')
    >>> result     = run_variable_length_split(data, length_options, value_step)
    >>> result.data_items[0:4]
    [Bits('0xde'), Bits('0xad'), Bits('0xbe'), Bits('0xef')]
    >>> items_data = BitStream().join([item for item in result.data_items])
    >>> items_data
    BitStream('0xdeadbeef0123456789abcdefdeadbeef00112233445566778899aabbccddeeff42')
    >>> items_data == data
    True
    >>> result.suffix_item == None
    True
    """
    data_items      = list()
    data_length     = len(data)
    min_length      = get_min_value_length(length_options)
    max_length      = get_max_value_length(length_options)
    search_item     = BitArray()
    data_dict       = defaultdict(set)
    item_number     = 0
    suffix_item     = None
    if (show_progress):
        pbar = tqdm.tqdm(total=data_length, miniters=50000)
    # split data to items, filling dictionary in process
    while ((data_length - data.bitpos) >= value_step):
        item_part           = data.read(f"bits:{value_step}")
        search_item.append(item_part)
        search_item_length  = len(search_item)
        #length_options_key  = options_mapping[search_item_length]
        search_item_options = get_item_length_options(search_item, length_options) #length_options[length_options_key]
        if (show_progress):
            pbar.update(value_step)
        if ((data_length - data.bitpos) == 0):
            # this is the last data item
            suffix_item = search_item
        if (len(search_item) < min_length):
            # item is too short
            continue
        if (len(search_item) > max_length):
            # too long item (not enough space in dictionaries)
            raise Exception(f"Max length reached: search_item={search_item} (not enough space in dictionaries)")
        if (len(data_dict[search_item_length]) == search_item_options.max_items):
            # dictionary for this length is full
            continue
        dict_item = Bits(search_item)
        if (dict_item not in data_dict[search_item_length]):
            # new item for dictionary
            data_dict[search_item_length].add(dict_item)
        # append item to result
        data_items.append(dict_item)
        search_item = BitArray()
        suffix_item = None
        item_number += 1
    if (show_progress):
        pbar.close()
    return VarLengthSplitResult(data_items, data_dict, suffix_item)

def count_split_items(data_items: List[Bits]) -> Counter:
    """
    >>> free_space_bits = 2
    >>> min_length      = 8
    >>> max_length      = 32
    >>> position_step   = 8
    >>> value_step      = 8
    >>> length_options  = create_value_length_options(min_length, max_length, free_space_bits, position_step, value_step)
    >>> data            = BitStream(hex='0xdeadbeef0123456789abcdefdeadbeef00112233445566778899aabbccddeeff')
    >>> result          = run_variable_length_split(data, length_options, value_step)
    >>> count_split_items(result.data_items).most_common(4)
    [(Bits('0xef'), 3), (Bits('0xde'), 2), (Bits('0xad'), 2), (Bits('0xbe'), 2)]
    """
    return Counter(data_items)

def get_item_length_options(data_item: Union[Bits, BitArray, int], length_options: Dict[str, ItemLengthOptions]) -> ItemLengthOptions:
    """
    >>> free_space_bits = 2
    >>> min_length      = 8
    >>> max_length      = 32
    >>> position_step   = 8
    >>> value_step      = 8
    >>> length_options = create_value_length_options(min_length, max_length, free_space_bits, position_step, value_step)
    >>> item_options   = get_item_length_options(Bits('0xde'), length_options)
    >>> item_options.value_length == 8
    True
    >>> item_options   = get_item_length_options(Bits('0xdead'), length_options)
    >>> item_options.value_length == 16
    True
    >>> item_options   = get_item_length_options(8, length_options)
    >>> item_options.value_length == 8
    True
    """
    if type(data_item) is int:
        item_length = data_item
    else:
        item_length = len(data_item)
    options_mapping    = get_mapping_from_value_length_to_prefix(length_options)
    length_options_key = options_mapping[item_length]
    return length_options[length_options_key]

def locate_data_item(item: Bits, item_options: ItemLengthOptions, skip_positions: set) -> HashItemAddress:
    """
    >>> free_space_bits = 2
    >>> min_length      = 8
    >>> max_length      = 32
    >>> position_step   = 8
    >>> value_step      = 8
    >>> length_options  = create_value_length_options(min_length, max_length, free_space_bits, position_step, value_step)
    >>> data            = BitStream(hex='0xdeadbeef0123456789abcdefdeadbeef00112233445566778899aabbccddeeff')
    >>> result          = run_variable_length_split(data, length_options, value_step)
    >>> result.data_items[0:4]
    [Bits('0xde'), Bits('0xad'), Bits('0xbe'), Bits('0xef')]
    >>> item_options   = get_item_length_options(result.data_items[0], length_options)
    >>> skip_positions = set()
    >>> item_address   = locate_data_item(result.data_items[0], item_options, skip_positions)
    >>> item_address
    HashItemAddress(bit_position=120, bit_length=8, seed=12)
    >>> read_hash_item(item_address) == result.data_items[0]
    True
    >>> skip_positions = {120}
    >>> item_address   = locate_data_item(result.data_items[0], item_options, skip_positions)
    >>> item_address
    HashItemAddress(bit_position=8, bit_length=8, seed=18)
    >>> read_hash_item(item_address) == result.data_items[0]
    True
    >>> result.data_items[25]
    Bits('0xddee')
    >>> skip_positions = set()
    >>> item_options   = get_item_length_options(result.data_items[25], length_options)
    >>> item_address   = locate_data_item(result.data_items[25], item_options, skip_positions)
    >>> item_address
    HashItemAddress(bit_position=31248, bit_length=16, seed=32)
    >>> read_hash_item(item_address) == result.data_items[25]
    True
    """
    segment_length = math.ceil((item_options.end_search_position - item_options.start_search_position) / HASH_DIGEST_BITS) * HASH_DIGEST_BITS
    seed_range     = range(0, 2**24)
    for seed in seed_range:
        segment_address = HashSegmentAddress(item_options.start_search_position, segment_length, seed)
        # TODO: check values database before scanning hashes
        item_address    = find_value_in_segment(item, segment_address, 
            value_step=item_options.value_step, 
            #chunk_length=(HASH_DIGEST_BITS * len(item)),
            position_step=item_options.position_step,
            skip_positions=skip_positions)
        if (item_address is not None):
            return item_address
    raise Exception(f"Seed range exeeded: item={item}, item_options: {item_options}")

def create_data_item_mapping(split_result: Union[List[Bits], VarLengthSplitResult], \
        length_options: Dict[str, ItemLengthOptions], show_progress=False) -> Dict[Bits, HashItemAddress]:
    """
    >>> free_space_bits = 2
    >>> min_length      = 8
    >>> max_length      = 32
    >>> position_step   = 8
    >>> value_step      = 8
    >>> length_options  = create_value_length_options(min_length, max_length, free_space_bits, position_step, value_step)
    >>> data            = BitStream(hex='0xdeadbeef0123456789abcdefdeadbeef00112233445566778899aabbccddeeff')
    >>> result          = run_variable_length_split(data, length_options, value_step)
    >>> item_mapping    = create_data_item_mapping(result, length_options)
    >>> len(item_mapping)
    21
    >>> item_mapping[Bits('0x00')]
    HashItemAddress(bit_position=40, bit_length=8, seed=0)
    >>> read_hash_item(item_mapping[Bits('0x00')]) == Bits('0x00')
    True
    >>> item_mapping[Bits('0xde')]
    HashItemAddress(bit_position=64, bit_length=8, seed=29)
    >>> read_hash_item(item_mapping[Bits('0xde')]) == Bits('0xde')
    True
    >>> item_mapping[Bits('0xddee')]
    HashItemAddress(bit_position=31248, bit_length=16, seed=32)
    >>> result.data_items[25] == Bits('0xddee')
    True
    >>> locate_data_item(result.data_items[25], get_item_length_options(result.data_items[25], length_options), {})
    HashItemAddress(bit_position=31248, bit_length=16, seed=32)
    >>> read_hash_item(item_mapping[Bits('0xddee')])
    Bits('0xddee')
    """
    item_mapping = dict()
    item_lengths = list(split_result.data_dict.keys())
    for item_length in item_lengths:
        items          = split_result.data_dict[item_length]
        skip_positions = set()
        if (show_progress):
            item_range = tqdm.tqdm(items)
        else:
            item_range = items
        prev_item_position = None
        for item in item_range:
            item_options = get_item_length_options(item, length_options)
            # the main operation here: locate data position inside hash space
            item_address = locate_data_item(item, item_options, skip_positions)
            #item_address = locate_data_item(item_values, item_options, skip_positions)
            #item_values.discard(item)
            if (prev_item_position is not None):
                skip_positions.add(prev_item_position)
            item_mapping[item] = item_address
            prev_item_position = item_address.bit_position
    return item_mapping

def collect_item_addresses(split_result: Union[List[Bits], VarLengthSplitResult], item_mapping: Dict[Bits, HashItemAddress]) -> List[HashItemAddress]:
    """
    >>> free_space_bits = 2
    >>> min_length      = 8
    >>> max_length      = 32
    >>> position_step   = 8
    >>> value_step      = 8
    >>> length_options  = create_value_length_options(min_length, max_length, free_space_bits, position_step, value_step)
    >>> data            = BitStream(hex='0xdeadbeef0123456789abcdefdeadbeef00112233445566778899aabbccddeeff')
    >>> result          = run_variable_length_split(data, length_options, value_step)
    >>> item_mapping    = create_data_item_mapping(result, length_options)
    >>> addresses       = collect_item_addresses(result, item_mapping)
    >>> len(addresses) == len(result.data_items)
    True
    >>> len(addresses)
    26
    >>> addresses[0]
    HashItemAddress(bit_position=64, bit_length=8, seed=29)
    >>> read_hash_item(addresses[0]) == result.data_items[0]
    True
    >>> read_hash_item(addresses[25]) == result.data_items[25]
    True
    """
    addresses = list()
    for item in split_result.data_items:
        address  = item_mapping[item]
        addresses.append(address)
    return addresses

def create_seed_mapping(data_item_addresses: List[HashItemAddress]) -> Dict[int, int]:
    """
    >>> free_space_bits = 2
    >>> min_length      = 8
    >>> max_length      = 32
    >>> position_step   = 8
    >>> value_step      = 8
    >>> length_options  = create_value_length_options(min_length, max_length, free_space_bits, position_step, value_step)
    >>> data            = BitStream(hex='0xdeadbeef0123456789abcdefdeadbeef00112233445566778899aabbccddeeff')
    >>> result          = run_variable_length_split(data, length_options, value_step)
    >>> item_mapping    = create_data_item_mapping(result, length_options)
    >>> addresses       = collect_item_addresses(result, item_mapping)
    >>> seed_mapping    = create_seed_mapping(addresses)
    >>> len(seed_mapping) == len(addresses)
    True
    >>> seed_mapping
    {0: 29, 1: 34, 2: 117, 3: 5, 4: 13, 5: 39, 6: 7, 7: 3, 8: 13, 9: 12, 10: 0, 11: 5, 12: 29, 13: 34, 14: 117, 15: 5, 16: 0, 17: 27, 18: 26, 19: 24, 20: 3, 21: 33, 22: 8, 23: 14, 24: 0, 25: 32}
    >>> seed_mapping[0] == addresses[0].seed
    True
    >>> seed_mapping[25] == addresses[25].seed
    True
    """
    seed_mapping = dict()
    item_number  = 0
    for address in data_item_addresses:
        seed_mapping[item_number] = address.seed
        item_number += 1
    return seed_mapping

def get_split_type(length_options: Dict[str, ItemLengthOptions]) -> SplitType:
    if (len(length_options) == 1):
        return SplitType.FIXED_LENGTH
    return SplitType.VARIABLE_LENGTH

def get_split_result(data: ConstBitStream, length_options: Dict[str, ItemLengthOptions], show_progress=False) -> Union[List[Bits], VarLengthSplitResult]:
    split_type = get_split_type(length_options)
    if (split_type == SplitType.VARIABLE_LENGTH):
        return run_variable_length_split(data, length_options, show_progress=show_progress)
    elif (split_type == SplitType.FIXED_LENGTH):
        item_options = list(length_options.values()).pop()
        return run_fixed_length_split(data, item_options)
    raise Exception(f"Incorrect split type: split_type={split_type}, length_options: {length_options}")

def create_data_split(data: ConstBitStream, length_options: Dict[str, ItemLengthOptions], show_progress=False) -> DataSplit:
    """
    >>> free_space_bits = 2
    >>> min_length      = 8
    >>> max_length      = 32
    >>> position_step   = 8
    >>> value_step      = 8
    >>> length_options  = create_value_length_options(min_length, max_length, free_space_bits, position_step, value_step)
    >>> data            = BitStream(hex='0xdeadbeef0123456789abcdefdeadbeef00112233445566778899aabbccddeeff')
    >>> data_split      = create_data_split(data, length_options)
    >>> data_split.split_type == SplitType.VARIABLE_LENGTH
    True
    >>> len(data_split.item_mapping)
    21
    >>> len(data_split.seed_mapping)
    26
    >>> len(data_split.data_items)
    26
    >>> len(data_split.item_addresses)
    26
    >>> len(data_split.item_counts)
    21
    >>> len(data_split.seed_counts)
    17
    >>> data_split.prefix_length == get_prefix_length_for_options(data_split.length_options)
    True
    >>> #data_split.length_options != length_options
    >>> #True
    >>> #len(data_split.length_options)
    >>> #2
    >>> #list(data_split.length_options.keys())
    >>> #['00', '01']
    >>> #[item_option.value_length for item_option in data_split.length_options.values()]
    >>> #[8, 16]
    >>> data_split.data_length == len(data)
    True
    >>> data_split.suffix_item == Bits('0xff')
    True
    """
    return DataSplit(data, length_options, show_progress=show_progress)