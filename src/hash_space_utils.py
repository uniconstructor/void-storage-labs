# https://github.com/Textualize/rich
from rich import print
# https://rich.readthedocs.io/en/latest/pretty.html
from rich.pretty import pprint
# https://rich.readthedocs.io/en/latest/traceback.html
from rich.traceback import install
# https://rich.readthedocs.io/en/latest/logging.html
import logging
"""
from rich.logging import RichHandler
logging.basicConfig(
    level="INFO",
    format="%(message)s",
    datefmt="[%X]",
    handlers=[RichHandler(rich_tracebacks=True)]
)
"""
install(show_locals=True)
log = logging.getLogger("rich")
# https://github.com/tqdm/tqdm
import tqdm

# https://realpython.com/python-data-classes/
# https://realpython.com/python-data-classes/#advanced-default-values
from dataclasses import dataclass, make_dataclass, field
# https://docs.python.org/3/library/enum.html
from enum import Enum

# https://bitstring.readthedocs.io/en/latest/index.output_item_idhtml
from bitstring import Bits, BitArray, BitStream, ConstBitStream
# https://github.com/Cyan4973/xxHash
# https://github.com/ifduyue/python-xxhash
import xxhash
# https://pythongeeks.org/ordereddict-in-python/
# https://www.geeksforgeeks.org/counters-in-python-set-1/
# https://docs.python.org/3/library/collections.html#counter-objects
# https://realpython.com/python-namedtuple/
from collections import OrderedDict, defaultdict, namedtuple
from custom_counter import CustomCounter as Counter
# https://realpython.com/linked-lists-python/
from collections import deque
# https://github.com/multiformats/unsigned-varint
# https://github.com/fmoo/python-varint
import varint
# https://docs.python.org/3/library/typing.html
# https://realpython.com/python-data-classes/#more-flexible-data-classes
from typing import List, Set, Dict, Tuple, Optional, Union, Deque
# https://www.geeksforgeeks.org/floor-ceil-function-python/?ref=lbp
import math
# https://docs.python.org/3/library/operator.html#module-operator
import operator

HASH_DIGEST_BITS  : int = 64
HASH_DIGEST_BYTES : int = HASH_DIGEST_BITS // 8
# different hash digest formats
BYTES_HASH_CALLBACK : bytes = xxhash.xxh3_64_digest
HEX_HASH_CALLBACK   : str   = xxhash.xxh3_64_hexdigest
INT_HASH_CALLBACK   : int   = xxhash.xxh3_64_intdigest
HASH_CALLBACK       : bytes = BYTES_HASH_CALLBACK
# default item processing options - used for reading seed/item value from hash space
DEFAULT_SEED_ITEM_LENGTH  : int = 24
DEFAULT_VALUE_ITEM_LENGTH : int = 8
SEED_VALUE_START_POSITION : int = 0
DEFAULT_POSITION_STEP     : int = 8
DEFAULT_VALUE_STEP        : int = 8
# стандартный размер блока кеша при поиске значений в хеш-пространстве (по умолчанию 4 kb)
DEFAULT_SEARCH_CHUNK_SIZE : int = 1024 * HASH_DIGEST_BITS
DEFAULT_MULTIPLIER        : int = 2 ** DEFAULT_VALUE_ITEM_LENGTH
DEFAULT_SEGMENT_POSITION  : int = 0
FORMAT_PREFIXES = {
    'bin': '0b',
    'hex': '0x',
    'oct': '0o',
}

class DataUnit(str, Enum):
    """Единица измерения объема данных"""
    BYTES : str = 'bytes'
    BITS  : str = 'bits'

DataUnitScale = {
    f"{DataUnit.BITS}"  : 1,
    f"{DataUnit.BYTES}" : 8,
}

# адрес произвольного значения в пространстве хешей: низкоуровневый указатель,
# который не связан ни с элементами ни с сегментами значений
HashItemAddress    = namedtuple('HashItemAddress', ['bit_position', 'bit_length', 'seed'])
# позиция элемента данных в пространстве хешей: другой формат адреса, в котором длина и позиция указываются с учетом
# длины значения элемента и размера шага поиска для позиции
HashItemPosition   = namedtuple('HashItemPosition', ['item_position', 'item_length', 'seed', 'position_step', 'value_step'])
# сегмент пространства хешей
HashSegmentAddress = namedtuple('HashSegmentAddress', ['bit_position', 'bit_length', 'seed'])
# таблица настройки сегмента хеш-пространства под данные
AdjustmentTable    = namedtuple('AdjustmentTable', [
    'data_items',
    'segment_items',
    'data_length',
    'item_length',
    'segment_length',
    'segment_address',
    'data_counts',
    'segment_counts',
    'segment_items_in_data',
    'data_items_in_segment',
    'missing_data_items_in_segment',
    'missing_segment_items_in_data',
    'segment_items_map',
    'multiplier',
])
# пара соответствия позиции элемента в файле и seed-числа которое используется для вычисления значения элемента
PositionSeed = namedtuple('PositionSeed', ['position', 'seed'])

# данные подготовленные к разбиению на отрезки указанной длины
@dataclass()
class AlignedData:
    # выровненные данные 
    data         : Union[ConstBitStream, BitArray]
    # отрезанная при вывавнивании часть (в конце)
    tail         : Union[ConstBitStream, BitArray]
    # длина выровненных данных (в байтах)
    data_length  : int
    # длина отрезанного остатка (в байтах)
    tail_length  : int
    # длина элемента для разбиения (в байтах)
    value_length : int
    # количество частей после разбиения
    total_items  : int

    def __init__(self, data: Union[ConstBitStream, BitArray], value_length : int=2):
        self.value_length = value_length
        self.tail_length  = (len(data) // 8) % self.value_length
        # вычисляем начало и окончание сегментов для выравнивания
        start_tail        = len(data) - (8 * self.tail_length)
        end_tail          = len(data)
        start_data        = 0
        end_data          = start_tail
        # выравниваем данные
        self.data         = data[start_data:end_data]
        self.tail         = data[start_tail:end_tail]
        
        self.data_length  = len(self.data) // 8 
        self.tail_length  = len(self.tail) // 8 
        
        self.total_items  = (self.data_length // self.value_length)

def bit_position_to_nounce(position: int) -> int:
    return position // HASH_DIGEST_BITS

def bit_position_to_block_position(position: int) -> int:
    return position % HASH_DIGEST_BITS

def byte_position_to_nounce(byte_position: int) -> int:
    return (byte_position * 8) // HASH_DIGEST_BITS

def nounce_to_input(nounce: int) -> bytes:
    byte_length  = (nounce.bit_length() // 8) + 1
    nounce_bytes = nounce.to_bytes(byte_length, byteorder='big', signed=False)
    return nounce_bytes
    #if (padding is None):
    #    return nounce_bytes
    #if (len(padding) == 0):
    #    return nounce_bytes
    #return padding + nounce_bytes

def hash_from_nounce(nounce: int, seed: Optional[int]=0, padding: bytearray=None, format: str='bytes') -> Union[bytes, int, str]:
    if (format == 'bytes'):
        return bytes_from_nounce(nounce=nounce, seed=seed)
    if (format == 'hex'):
        return hex_from_nounce(nounce=nounce, seed=seed)
    if (format == 'int'):
        return int_from_nounce(nounce=nounce, seed=seed)
    input = nounce_to_input(nounce, padding=padding)
    return HASH_CALLBACK(input, seed=seed)

def bytes_from_nounce(nounce: int, seed: Optional[int]=0) -> bytes:
    return BYTES_HASH_CALLBACK(input=nounce_to_input(nounce), seed=seed)

def hex_from_nounce(nounce: int, seed: Optional[int]=0) -> str:
    return HEX_HASH_CALLBACK(input=nounce_to_input(nounce), seed=seed)

def int_from_nounce(nounce: int, seed: Optional[int]=0) -> int:
    return INT_HASH_CALLBACK(input=nounce_to_input(nounce), seed=seed)

def normalize_length_value(value: int, unit: DataUnit) -> int:
    if (unit is DataUnit.BYTES):
        return (value << 3)
    else:
        return value

def bytes_at_position(position: int, length: int, seed: Optional[int]=0, padding: bytearray=None,
        use_bytearray: Optional[bool]=True, unit: DataUnit=DataUnit.BITS) -> Union[bytearray, bytes]:
    """
    Получить из указанного пространства хешей байты значения заданной длины с указанной позиции
    (низкоуровневая функция, использует bytearray вместо Bits)

    Parameters
    ----------
    position: int
        позиция значения - номер бита от начала (от нулевого бита) хеш-пространства
    length: int
        длина значения - количество считываемых бит начиная с указанной позиции
    seed: int
        номер пространства хешей из которого происходит чтение, задачется параметром seed в модуле xxhash:
        https://github.com/ifduyue/python-xxhash#seed-overflow
    use_bytearray: bool
        возвращать bytearray (по умолчанию) или bytes
    unit: DataUnit
        единица измерения данных (биты или байты)
    
    >>> hash_bytes = bytes_at_position(0, 8 * 8)
    >>> hash_bytes.hex()
    'a6cd5e9392000f6a'
    >>> len(hash_bytes) == 8
    True
    >>> hash_bytes = bytes_at_position(8, 7 * 8)
    >>> hash_bytes.hex()
    'cd5e9392000f6a'
    >>> len(hash_bytes) == 7
    True
    >>> hash_bytes_0 = bytes_at_position(0, HASH_DIGEST_BITS)
    >>> hash_bytes_0.hex()
    'a6cd5e9392000f6ac44bdff4074eecdb'
    >>> hash_bytes_1 = bytes_at_position(HASH_DIGEST_BITS, HASH_DIGEST_BITS)
    >>> hash_bytes_1.hex()
    '51025a4491835505e12ef9d2eb86ceeb'
    >>> hash_bytes_2 = bytes_at_position(0, HASH_DIGEST_BITS * 2)
    >>> hash_bytes_2.hex()
    'a6cd5e9392000f6ac44bdff4074eecdb51025a4491835505e12ef9d2eb86ceeb'
    >>> (hash_bytes_0 + hash_bytes_1) == hash_bytes_2
    True
    >>> hash_bytes = bytes_at_position(HASH_DIGEST_BITS - 16, 32)
    >>> hash_bytes.hex()
    'ecdb5102'
    >>> hash_bytes == (hash_bytes_0[-2:] + hash_bytes_1[:2])
    True
    >>> hash_bytes_0 = bytes_at_position(0, 128 * 8 + 2 * 8)
    >>> len(hash_bytes_0)
    130
    >>> hash_bytes_1 = bytes_at_position(128 * 8, 128 * 8 + 2 * 8)
    >>> len(hash_bytes_0)
    130
    >>> hash_bytes_0[-2:] == hash_bytes_1[:2]
    True
    """
    # пересчитываем рассиояеие и пощициб если они переданы в битах
    #if (unit is DataUnit.BYTES):
    position = normalize_length_value(position, unit=unit)
    length   = normalize_length_value(length, unit=unit)
    # определяем порядковый номер хеша в пространстве: он кратен длине выходного значения хеш-функции
    start_nounce = bit_position_to_nounce(position)
    end_nounce   = bit_position_to_nounce(position + length - 1)
    # определяем первый и последний байт для чтения:
    # вычисляем из абсолютной позиции относительную (от начала блока) чтобы понять откуда читать данные
    start_byte = (position % HASH_DIGEST_BITS) // 8
    end_byte   = start_byte + math.ceil(length / 8)
    # определяем сколько хешей необходимо вычислить чтобы получить значение указанной длины
    if (start_nounce == end_nounce):
        # нужен один хеш - зная его порядковый номер вычисляем хеш от этого номера и берем значение целиком
        if use_bytearray:
            hash_bytes = bytearray().join([hash_from_nounce(start_nounce, seed=seed, padding=padding)])
        else:
            hash_bytes = hash_from_nounce(start_nounce, seed=seed, padding=padding)
    else:
        # значение лежит на стыке двух хешей или слишком длинное чтобы поместиться в один хеш:
        # вычисляем несколько последовательно идущих хешей из одного пространства, а затем склеиваем их
        hashes = [hash_from_nounce(nounce, seed=seed, padding=padding) for nounce in range(start_nounce, end_nounce + 1)]
        # хеши генерируются как поток байт
        if use_bytearray:
            hash_bytes = bytearray().join(hashes)
        else:
            hash_bytes = bytes().join(hashes)
    return hash_bytes[start_byte:end_byte]

def hex_at_position(position: int, length: int, seed: Optional[int]=0, unit: DataUnit=DataUnit.BITS) -> str: #, unit: DataUnit=DataUnit.BYTES) -> str:
    """
    Получить из указанного пространства хешей hex-строку байтов значения заданной длины с указанной позиции
    (низкоуровневая функция, использует str вместо Bits)

    Parameters
    ----------
    position: int
        позиция значения - номер бита от начала (от нулевого бита) хеш-пространства
    length: int
        длина значения - количество считываемых бит начиная с указанной позиции
    seed: int
        номер пространства хешей из которого происходит чтение, задачется параметром seed в модуле xxhash:
        https://github.com/ifduyue/python-xxhash#seed-overflow
    unit: DataUnit
        единица измерения данных (биты или байты)
    """
    position = normalize_length_value(position, unit=unit)
    length   = normalize_length_value(length, unit=unit)
    # определяем порядковый номер хеша в пространстве: он кратен длине выходного значения хеш-функции
    start_nounce = bit_position_to_nounce(position)
    end_nounce   = bit_position_to_nounce(position + length - 1)
    # определяем первый и последний байт для чтения:
    # вычисляем из абсолютной позиции относительную (от начала блока) чтобы понять откуда читать данные
    start_byte = (position % HASH_DIGEST_BITS) // 8
    end_byte   = start_byte + math.ceil(length / 8)
    start_char = start_byte * 2
    end_char   = end_byte * 2
    # определяем сколько хешей необходимо вычислить чтобы получить значение указанной длины
    if (start_nounce == end_nounce):
        # нужен один хеш - зная его порядковый номер вычисляем хеш от этого номера и берем значение целиком
        hex_digest = hex_from_nounce(start_nounce, seed)
    else:
        # значение лежит на стыке двух хешей или слишком длинное чтобы поместиться в один хеш:
        # вычисляем несколько последовательно идущих хешей из одного пространства, а затем склеиваем их
        # хеши генерируются как поток байт
        hex_digest = ''
        for nounce in range(start_nounce, end_nounce + 1):
            hex_digest += hex_from_nounce(nounce, seed=seed)
    return hex_digest[start_char:end_char]

def value_at_position(position: int, length: int, seed: Optional[int]=0, padding: bytearray=None, unit: DataUnit=DataUnit.BITS) -> Bits:
    """
    Получить из указанного пространства хешей значение заданной длины с указанной позиции

    Parameters
    ----------
    position: int
        позиция значения - номер бита от начала (от нулевого бита) хеш-пространства
    length: int
        длина значения - количество считываемых бит начиная с указанной позиции
    seed: int
        номер пространства хешей из которого происходит чтение, задачется параметром seed в модуле xxhash:
        https://github.com/ifduyue/python-xxhash#seed-overflow
    
    >>> item_length = HASH_DIGEST_BITS
    >>> seed        = 0
    >>> position    = 0
    >>> value_at_position(position, item_length, seed)
    Bits('0xa6cd5e9392000f6ac44bdff4074eecdb')
    >>> position = HASH_DIGEST_BITS
    >>> value_at_position(position, item_length, seed)
    Bits('0x51025a4491835505e12ef9d2eb86ceeb')
    >>> position    = (HASH_DIGEST_BITS - 16)
    >>> item_length = 16
    >>> value_at_position(position, item_length, seed)
    Bits('0xecdb')
    >>> position    = HASH_DIGEST_BITS
    >>> item_length = 16
    >>> value_at_position(position, item_length, seed)
    Bits('0x5102')
    >>> position    = (HASH_DIGEST_BITS - 16)
    >>> item_length = 32
    >>> value_at_position(position, item_length, seed)
    Bits('0xecdb5102')
    """
    hash_bytes   = bytearray()
    position     = normalize_length_value(position, unit=unit)
    length       = normalize_length_value(length, unit=unit)
    # определяем порядковый номер хеша в пространстве: он кратен длине выходного значения хеш-функции
    start_nounce = bit_position_to_nounce(position)
    end_nounce   = bit_position_to_nounce(position + length - 1)
    # получаем блок байт хеша целиком
    if (start_nounce == end_nounce):
        # нужен один хеш - зная его порядковый номер вычисляем хеш от этого номера и берем значение целиком
        hash_bytes = hash_bytes.join([hash_from_nounce(start_nounce, seed=seed, padding=padding)])
    else:
        # значение лежит на стыке двух хешей или слишком длинное чтобы поместиться в один хеш:
        # вычисляем несколько последовательно идущих хешей из одного пространства, а затем склеиваем их
        hashes     = [hash_from_nounce(nounce, seed=seed, padding=padding) for nounce in range(start_nounce, end_nounce + 1)]
        # хеши генерируются как поток байт
        hash_bytes = hash_bytes.join(hashes)
    # создаем объект для работы с битами
    hash_bits = Bits(bytes=hash_bytes)
    # вычисляем из абсолютной позиции относительную (от начала блока) чтобы понять откуда читать данные
    start = bit_position_to_block_position(position)
    # определяем диапазон бит в полученном отрезке пространства хешей
    end   = start + length
    # возвращаем заданный диапазон пространства хешей
    return hash_bits[start:end]

def read_hash_item(address: HashItemAddress) -> Bits:
    """
    Прочитать данные из пространства хешей (alias для value_at_position)

    >>> item_length   = HASH_DIGEST_BITS
    >>> seed          = 0
    >>> position      = 0
    >>> position_step = DEFAULT_POSITION_STEP
    >>> value_step    = DEFAULT_VALUE_STEP
    >>> address       = HashItemAddress(position, item_length, seed)
    >>> read_hash_item(address) == value_at_position(position, item_length, seed)
    True
    >>> position = HASH_DIGEST_BITS
    >>> address  = HashItemAddress(position, item_length, seed)
    >>> read_hash_item(address) == value_at_position(position, item_length, seed)
    True
    >>> position    = (HASH_DIGEST_BITS - 16)
    >>> item_length = 16
    >>> address     = HashItemAddress(position, item_length, seed)
    >>> read_hash_item(address) == value_at_position(position, item_length, seed)
    True
    >>> position    = HASH_DIGEST_BITS
    >>> item_length = 16
    >>> address     = HashItemAddress(position, item_length, seed)
    >>> read_hash_item(address) == value_at_position(position, item_length, seed)
    True
    >>> position    = (HASH_DIGEST_BITS - 16)
    >>> item_length = 32
    >>> address     = HashItemAddress(position, item_length, seed)
    >>> read_hash_item(address) == value_at_position(position, item_length, seed)
    True
    """
    return value_at_position(position=address.bit_position, length=address.bit_length, seed=address.seed)

def read_hash_segment(address: HashSegmentAddress) -> ConstBitStream:
    """
    Прочитать участок из пространства хешей (пока что alias read_hash_item)
    Функция работает как read_hash_item, но предназначена для чтения больших сегментов данных
    """
    # TODO: использовать генератор, загружать в память только 2 соседних читаемых хеша
    segment_bits = read_hash_item(address)
    return ConstBitStream(segment_bits)

def get_format_prefix(format: str) -> str:
    if format not in FORMAT_PREFIXES:
        return ''
    return FORMAT_PREFIXES[format]

def get_min_bit_length(value: int) -> int:
    """
    >>> get_min_bit_length(0)
    1
    >>> get_min_bit_length(1)
    1
    >>> get_min_bit_length(2)
    2
    >>> get_min_bit_length(3)
    2
    >>> get_min_bit_length(4)
    3
    >>> get_min_bit_length(5)
    3
    >>> get_min_bit_length(7)
    3
    >>> get_min_bit_length(8)
    4
    """
    length = int(value).bit_length()
    if (length == 0):
        length = 1
    return length

def get_aligned_bit_length(value: int) -> int:
    """
    Получить длину числа (в битах, округление до целого байта)

    >>> get_aligned_bit_length(0)
    8
    >>> get_aligned_bit_length(1)
    8
    >>> get_aligned_bit_length(127) 
    8
    >>> get_aligned_bit_length(128)
    8
    >>> get_aligned_bit_length(255)
    8
    >>> get_aligned_bit_length(256)
    16
    >>> get_aligned_bit_length(2**14-1) 
    16
    >>> get_aligned_bit_length(2**14)
    16
    >>> get_aligned_bit_length(2**16-1)
    16
    >>> get_aligned_bit_length(2**16)
    24
    >>> get_aligned_bit_length(2**21-1)
    24
    >>> get_aligned_bit_length(2**21)
    24
    """
    byte_length = math.ceil(get_min_bit_length(value) / 8)
    return (byte_length * 8)

def get_aligned_byte_length(value: int) -> int:
    return (get_aligned_bit_length(value) // 8)

def get_varint_bit_length(value: int) -> int:
    """
    Получить длину значения в varint-кодировке (в битах): https://github.com/multiformats/unsigned-varint

    >>> get_varint_bit_length(0)
    8
    >>> get_varint_bit_length(1)
    8
    >>> get_varint_bit_length(127) 
    8
    >>> get_varint_bit_length(128)
    16
    >>> get_varint_bit_length(255)
    16
    >>> get_varint_bit_length(256)
    16
    >>> get_varint_bit_length(2**14-1) 
    16
    >>> get_varint_bit_length(2**14)
    24
    >>> get_varint_bit_length(2**16-1)
    24
    >>> get_varint_bit_length(2**16)
    24
    >>> get_varint_bit_length(2**21-1)
    24
    >>> get_varint_bit_length(2**21)
    32
    """
    return (len(varint.encode(value)) * 8)

def decode_varint_bytes(data: bytes) -> List[int]:
    """
    >>> numbers = BitArray('0x010702032a1611050a5b2e0b06').tobytes()
    >>> decode_varint_bytes(numbers)
    [1, 7, 2, 3, 42, 22, 17, 5, 10, 91, 46, 11, 6]
    """
    data_bits = BitArray(bytes=data)
    numbers   = list()
    while len(data_bits) > 0:
        varint_bytes         = data_bits.tobytes()
        number               = varint.decode_bytes(varint_bytes)
        number_varint_length = get_varint_bit_length(number)
        numbers.append(number)
        # prepare to read next varint value (clear decoded bytes)
        start     = number_varint_length
        end       = len(data_bits)
        data_bits = data_bits[start:end]
    return numbers

# TODO: use Enum: https://docs.python.org/3/library/enum.html#timeperiod
def get_item_based_data_length_from_number(data_length: int, item_length: int = DEFAULT_VALUE_ITEM_LENGTH) -> int:
    """
    Получить длину исходных данных как количество элементов списка, составленного из отрезков указанной длины
    """
    data_length_in_bits  = data_length
    data_length_in_items = (data_length_in_bits // item_length)
    if (data_length_in_bits % item_length) > 0:
        data_length_in_items += 1
    return data_length_in_items

def get_item_based_data_length(data: ConstBitStream, item_length: int = DEFAULT_VALUE_ITEM_LENGTH) -> int:
    """
    Получить длину исходных данных как количество элементов списка, составленного из отрезков указанной длины
    """
    return get_item_based_data_length_from_number(len(data), item_length)

def get_data_item_position_from_data_length(data_length: int, number: int, item_length: int = DEFAULT_VALUE_ITEM_LENGTH) -> int:
    data_items_length = get_item_based_data_length_from_number(data_length, item_length)
    if (number >= data_items_length):
        item_position = number % data_items_length
    else:
        item_position = number
    return item_position

def get_data_item_position_from_number(data: ConstBitStream, number: int, item_length: int = DEFAULT_VALUE_ITEM_LENGTH) -> int:
    """
    Получить позицию элемента данных (обычно 1 байта) в исходных данных из любого числа
    """
    return get_data_item_position_from_data_length(len(data), number, item_length)

def align_data_before_split(data: ConstBitStream, length_bytes: int=2, unit: DataUnit=DataUnit.BYTES) -> AlignedData:
    """
    Подготовить данные для разбиения

    Parameters
    ----------
    data: ConstBitStream
        Итератор для чтения данных
    length_bytes: int
        Длина элемента при разбиении (в байтах)
    """
    length_bytes = normalize_length_value(length_bytes, unit)
    return AlignedData(data=data, value_length=length_bytes)

def split_data(data: ConstBitStream, length: int, format: str='bin', show_progress: bool=False, unit: DataUnit=DataUnit.BITS):
    """
    Разбить данные файла на равные части указанной длины

    Parameters
    ----------
    data: ConstBitStream
        Итератор для чтения данных
    length: int
        Длина элемента при разбиении (в битах)
    format: str
        Формат элемента при разбиении:
        https://bitstring.readthedocs.io/en/latest/constbitstream.html#bitstring.ConstBitStream.read
    
    >>> item_length  = 8
    >>> seed         = 999999
    >>> data_length  = 4 * item_length
    >>> data         = ConstBitStream(value_at_position(0, data_length, seed=seed))
    >>> bin_items    = list(split_data(data, length=item_length, format='bin'))
    >>> bin_items
    ['10001111', '11111010', '00001011', '01101001']
    >>> hex_items    = list(split_data(data, length=item_length, format='hex'))
    >>> hex_items
    ['8f', 'fa', '0b', '69']
    >>> bin_items_4    = list(split_data(data, length=4, format='bin'))
    >>> bin_items_4
    ['1000', '1111', '1111', '1010', '0000', '1011', '0110', '1001']
    >>> hex_items_4    = list(split_data(data, length=4, format='hex'))
    >>> hex_items_4
    ['8', 'f', 'f', 'a', '0', 'b', '6', '9']
    """
    length = normalize_length_value(length, unit=unit)
    if (len(data) % length) != 0:
        raise Exception(f"Incorrect length={length} for data_length={len(data)} (not divisible)")
    start    = 0
    end      = (len(data) // length)
    data.pos = start
    if (show_progress):
        range_object = tqdm.tqdm(range(start, end), miniters=50000)
    else:
        range_object = range(start, end)
    for item_position in range_object:
        value = data.read(f"{format}:{length}")
        yield value  

def count_split_values(data: ConstBitStream, length: int, format: str='bin', show_progress: bool=False, unit: DataUnit=DataUnit.BITS) -> Counter:
    """
    Подсчитать количество использований каждого значения при разбиении

    Parameters
    ----------
    data: ConstBitStream
        Итератор для чтения данных
    length: int
        Длина элемента при разбиении (в битах)
    format: str
        Формат элемента при разбиении:
        https://bitstring.readthedocs.io/en/latest/constbitstream.html#bitstring.ConstBitStream.read
    
    >>> item_length  = 8
    >>> seed         = 999999
    >>> data_length  = 16 * item_length
    >>> data         = ConstBitStream(value_at_position(0, data_length, seed=seed))
    >>> bin_counts   = count_split_values(data, length=item_length, format='bin')
    >>> bin_counts.most_common()[0:4]
    [('11011111', 2), ('10001111', 1), ('11111010', 1), ('00001011', 1)]
    >>> hex_counts   = count_split_values(data, length=item_length, format='hex')
    >>> hex_counts.most_common()[0:4]
    [('df', 2), ('8f', 1), ('fa', 1), ('0b', 1)]
    >>> uint_counts   = count_split_values(data, length=item_length, format='uint')
    >>> uint_counts.most_common()[0:4]
    [(223, 2), (143, 1), (250, 1), (11, 1)]
    >>> item_length  = 16
    >>> data_length  = 256 * item_length
    >>> data         = ConstBitStream(value_at_position(0, data_length, seed=seed))
    >>> hex_counts   = count_split_values(data, length=item_length, format='hex')
    >>> hex_counts.most_common()[0:4]
    [('6969', 2), ('8ffa', 1), ('0b69', 1), ('74df', 1)]
    """
    old_data_pos = data.pos
    tail_length = len(data) % length
    if (tail_length > 0):
        data = data[0:len(data)-tail_length]
        print(f"removed last {tail_length} bits: {data[len(data)-tail_length:len(data)]}")
    counts       = Counter(split_data(data, length, format, show_progress, unit))
    data.pos     = old_data_pos
    return counts

def collect_split_positions(data: ConstBitStream, length: int, format: str='bin', show_progress: bool=False, unit: DataUnit=DataUnit.BITS) -> dict:
    """
    Разбить данные на элементы указанной длины и получить словарь, в котором ключом является уникальное значение
    элемента при разбиении а значением - список позиций, в которых он встречается

    Parameters
    ----------
    data: ConstBitStream
        Итератор для чтения данных
    length: int
        Длина элемента при разбиении (в битах)
    format: str
        Формат элемента при разбиении: 
        https://bitstring.readthedocs.io/en/latest/constbitstream.html#bitstring.ConstBitStream.read
    
    >>> item_length   = 8
    >>> seed          = 999999
    >>> data_length   = 8 * item_length
    >>> position      = 40
    >>> data          = ConstBitStream(value_at_position(position, data_length, seed))
    >>> hex_positions = dict(collect_split_positions(data, item_length, format='hex'))
    >>> hex_positions 
    {'df': [0, 56], '08': [8], '7c': [16], 'a4': [24], 'c4': [32], '1f': [40], 'c7': [48]}
    >>> uint_positions = dict(collect_split_positions(data, item_length, format='uint'))
    >>> uint_positions 
    {223: [0, 56], 8: [8], 124: [16], 164: [24], 196: [32], 31: [40], 199: [48]}
    >>> item_length     = 4
    >>> data_length     = 8 * item_length
    >>> position        = 0
    >>> data            = ConstBitStream(value_at_position(position, data_length, seed))
    >>> hex_positions_4 = dict(collect_split_positions(data, item_length, format='hex'))
    >>> hex_positions_4 
    {'8': [0], 'f': [4, 8], 'a': [12], '0': [16], 'b': [20], '6': [24], '9': [28]}
    """
    length          = normalize_length_value(length, unit=unit)
    old_data_pos    = data.pos
    value_positions = defaultdict(list)
    data_values     = split_data(data, length, format, show_progress, unit=DataUnit.BITS)
    position        = data.pos
    for value in data_values:
        value_positions[value].append(position)
        position = position + length    
    data.pos = old_data_pos
    return value_positions

def collect_split_values(data: ConstBitStream, length: int, format: str='bin', show_progress: bool=False, unit: DataUnit=DataUnit.BITS) -> dict:
    length          = normalize_length_value(length, unit=unit)
    old_data_pos    = data.pos
    position_values = dict()
    data_values     = split_data(data, length, format, show_progress, unit=DataUnit.BITS)
    position        = 0
    for value in data_values:
        position_values[position] = value
        position = position + 1  
    data.pos = old_data_pos
    return position_values

def count_segment_items(address: HashSegmentAddress, item_length: int, format: str='bin', show_progress: bool=False, unit: DataUnit=DataUnit.BITS) -> Counter:
    """
    Подсчитать количество использований уникальных элементов в указанном сегменте пространства хешей при заданном разбиении

    Parameters
    ----------
    address: HashSegmentAddress
        Адрес и длина сегмента пространства хешей
    item_length: int
        Длина элемента при разбиении (в битах)
    format: str
        Формат элемента при разбиении: 
        https://bitstring.readthedocs.io/en/latest/constbitstream.html#bitstring.ConstBitStream.read
    
    >>> item_length         = 4
    >>> segment_length      = 12 * item_length
    >>> seed                = 0
    >>> position            = 0
    >>> address             = HashSegmentAddress(position, segment_length, seed)
    >>> hex_segment_stats_4 = count_segment_items(address, item_length, format='hex')
    >>> hex_segment_stats_4
    Counter({'9': 2, '0': 2, 'a': 1, '6': 1, 'c': 1, 'd': 1, '5': 1, 'e': 1, '3': 1, '2': 1})
    >>> item_length         = 8
    >>> segment_length      = 256 * item_length
    >>> address             = HashSegmentAddress(position, segment_length, seed)
    >>> hex_segment_stats_8 = count_segment_items(address, item_length, format='hex')
    >>> hex_segment_stats_8.most_common()[0:4]
    [('62', 5), ('f4', 4), ('3e', 4), ('81', 4)]
    >>> item_length             = 4
    >>> segment_length          = 256 * item_length
    >>> address                 = HashSegmentAddress(position, segment_length, seed)
    >>> hex_segment_stats_4_256 = count_segment_items(address, item_length, format='hex')
    >>> hex_segment_stats_4_256.most_common()[0:8]
    [('2', 25), ('e', 22), ('4', 20), ('f', 19), ('b', 19), ('5', 18), ('c', 17), ('3', 16)]
    """
    segment_data = read_hash_segment(address)
    return count_split_values(segment_data, item_length, format, show_progress, unit=unit)

### да, вот на этом моменте надо было остановиться, потому что дальше уровень безумия в коде будет только возрастать.
### Пожалуй оставим тут все как есть - некоторые решения могут стать нужны в дальнейшем

def create_data_bitmap(data: ConstBitStream, item_length: int=DEFAULT_VALUE_ITEM_LENGTH, format: str='bin', unit: DataUnit=DataUnit.BITS) -> Bits:
    """
    Создать карту использованных значений в переданных данных

    >>> item_length   = 8
    >>> bitmap_length = item_length * item_length
    >>> seed          = 999999
    >>> bitmap_data   = ConstBitStream(value_at_position(0, bitmap_length, seed=seed))
    >>> bin_bitmap    = create_data_bitmap(bitmap_data, item_length, format='bin')
    >>> bin_bitmap.count(1)
    8
    >>> bin_bitmap.hex
    '0090000000000000000000000040080800010000000000000000000100000020'
    >>> len(bin_bitmap) == (2 ** item_length)
    True
    >>> hex_bitmap = create_data_bitmap(bitmap_data, item_length, format='hex')
    >>> bin_bitmap.count(1) == hex_bitmap.count(1)
    True
    >>> bin_bitmap.hex == hex_bitmap.hex
    True
    >>> len(bin_bitmap) == len(hex_bitmap)
    True
    >>> item_length   = 16
    >>> bitmap_length = item_length * item_length
    >>> bitmap_data   = ConstBitStream(value_at_position(0, bitmap_length, seed=seed))
    >>> hex_bitmap_16 = create_data_bitmap(bitmap_data, item_length, format='hex')
    >>> len(hex_bitmap_16) == (2 ** item_length)
    True
    >>> hex_bitmap_16.count(1)
    16
    >>> hex_bitmap_16.hex[64:128]
    '0000000000000000000000000000000000000000000000000001000000000000'
    """
    item_length = normalize_length_value(item_length, unit=unit)
    if (item_length < 1):
        raise Exception(f"Incorrect item_length: {item_length}")
    if (len(data) % item_length != 0):
        raise Exception(f"Incorrect item_length: {item_length} (data_length={len(data)})")
    value_counts  = count_split_values(data, length=item_length, format=format)
    bitmap_length = 2 ** item_length
    bitmap        = BitArray(length=bitmap_length)
    # проходя по всем возможным значениям указанной длины помечаем те из них, 
    # которые были использованы хотя бы раз (1 бит = 1 уникальное значение значение)
    # TODO: заполнять карту не последовательно а в том же порядке в котором значения встречаются 
    #       в данных (сохраняя для каждого значимого бита порядковый номер - когда по счету он встретился): 
    #       это позволит каждому файлу данных иметь свою bitmap-сигнатуру (даже если используются все возможные варианты значений)
    #       а также даст возможность использовать начало битовой карты как начало файла (и не хранить их при сжатии)
    #       если дополнить такую карту списком пропусков - то можно все значения словаря исключить из списка позиций для поиска
    #       (так как мы уже указали их) и использовать эти позиции как соединительные элементы для стыковки нескольких списков
    #       также есть вариант на последнем этапе при склейке списков использовать только позиции с уникальными элементами
    #       и уже из них составлять словарь
    for bit_position in range(0, bitmap_length):
        bit_value  = False
        value      = ConstBitStream(uint=bit_position, length=item_length)
        value_key  = value.read(f"{format}:{item_length}")
        if (value_counts[value_key] > 0):
            bit_value = True
        bitmap[bit_position] = bit_value
    return bitmap

def collect_bitmap_keys(bitmap: Bits, target_value: bool=True) -> set:
    """
    Получить индексы всех используемых (или всех неиспользуемых) значений как целые числа

    >>> item_length   = 8
    >>> bitmap_length = item_length * item_length
    >>> seed          = 999999
    >>> bitmap_data   = ConstBitStream(value_at_position(0, bitmap_length, seed))
    >>> bin_bitmap    = create_data_bitmap(bitmap_data, item_length, format='bin')
    >>> bin_keys_1    = collect_bitmap_keys(bin_bitmap)
    >>> bin_keys_0    = collect_bitmap_keys(bin_bitmap, False)
    >>> bin_keys_1
    {8, 105, 11, 143, 116, 250, 124, 223}
    >>> len(bin_keys_1)
    8
    >>> len(bin_keys_0)
    248
    >>> (len(bin_keys_1) + len(bin_keys_0)) == (2 ** item_length)
    True
    >>> len(bin_keys_1.intersection(bin_keys_0))
    0
    >>> item_length   = 16
    >>> bitmap_length = item_length * item_length
    >>> bitmap_data   = ConstBitStream(value_at_position(0, bitmap_length, seed))
    >>> hex_bitmap_16 = create_data_bitmap(bitmap_data, item_length, format='hex')
    >>> hex_16_keys_1 = collect_bitmap_keys(hex_bitmap_16)
    >>> hex_16_keys_0 = collect_bitmap_keys(hex_bitmap_16, False)
    >>> hex_16_keys_1
    {42401, 57313, 42180, 8135, 37864, 2921, 62024, 28269, 463, 60085, 17081, 36858, 16763, 2172, 43005, 29919}
    >>> len(hex_16_keys_1)
    16
    >>> len(hex_16_keys_0)
    65520
    >>> (len(hex_16_keys_1) + len(hex_16_keys_0)) == (2 ** item_length)
    True
    >>> len(hex_16_keys_1.intersection(hex_16_keys_0))
    0
    """
    keys = set()
    for key in range(0, len(bitmap)):
        if (bitmap[key] == target_value):
            keys.add(key)
    return keys

def load_chunk_from_segment(address: HashSegmentAddress, chunk_number: int, chunk_length: int, value_step: int, \
        items_per_value: int, chunk_type: Union[ConstBitStream, Bits]=ConstBitStream) -> ConstBitStream:
    """
    Загрузить один блок из указанного сегмента хеш-пространства

    >>> value_step         = 8
    >>> segment_length      = (32 * HASH_DIGEST_BITS)
    >>> seed                = 0
    >>> position            = 0
    >>> address             = HashSegmentAddress(position, segment_length, seed)
    >>> chunk_number        = 0
    >>> chunk_length        = (2 * HASH_DIGEST_BITS)
    >>> items_per_value     = 3
    >>> chunk_0             = load_chunk_from_segment(address, chunk_number, chunk_length, value_step, items_per_value)
    >>> len(chunk_0) == (chunk_length + (items_per_value * value_step) - value_step) == 272
    True
    >>> chunk_0
    ConstBitStream('0xa6cd5e9392000f6ac44bdff4074eecdb51025a4491835505e12ef9d2eb86ceebe3ab')
    >>> chunk_number        = 1
    >>> chunk_1             = load_chunk_from_segment(address, chunk_number, chunk_length, value_step, items_per_value)
    >>> len(chunk_1) == len(chunk_0)
    True
    >>> chunk_0[-16:] == chunk_1[0:16]
    True
    >>> chunk_1
    ConstBitStream('0xe3ab152fcb1762bfc9f42e6c9e93dfff22bbb76b211a39ba13e608bc156defed2ff3')
    >>> chunk_number        = 15
    >>> chunk_15            = load_chunk_from_segment(address, chunk_number, chunk_length, value_step, items_per_value)
    >>> len(chunk_15) == (len(chunk_0) - 16) == 256
    True
    >>> chunk_15
    ConstBitStream('0x3195a37edc06e26b313c36c6889b8aaa153283be9f2a26d405087bbed866d0de')
    """
    chunk_start     = (address.bit_position + (chunk_number * chunk_length))
    # вычисляем оставшуюся длину сегмента
    segment_length = address.bit_length - (chunk_number * chunk_length)
    # если данных осталось меньше чем на 1 значение - просто останавливаем процесс
    if (segment_length < value_step):
        return ConstBitStream()
    # определяем сколько хеш-пространства будем подгружать
    read_length = chunk_length
    if (read_length >= segment_length):
        # если в сегменте осталось меньше 1 блока данных - то это последний блок, загружаем все что есть
        read_length = segment_length
    else:
        # если есть следующий блок - берем из него первые несколько элементов чтобы не потерять значения на стыке
        next_chunk_length = address.bit_length - ((chunk_number + 1) * chunk_length)
        extra_read_length = ((items_per_value - 1) * value_step)
        if (next_chunk_length >= extra_read_length):
            read_length = read_length + extra_read_length
    # получаем адрес блока из хеш-сегмента, который будем читать
    chunk_address  = HashSegmentAddress(chunk_start, read_length, address.seed)
    if (chunk_type is ConstBitStream):
        chunk = read_hash_segment(chunk_address)
    else:
        chunk = read_hash_item(chunk_address)
    return chunk

def find_one_value_in_chunk(chunk: ConstBitStream, value, position_step: int, skip_positions: set, \
        address: HashItemAddress, chunk_number: int, chunk_length: int, byte_aligned: bool) -> HashItemAddress:
    search_result = chunk.find(value, bytealigned=byte_aligned)
    if (len(search_result) == 0):
        return None
    # проверяем найдена ли позиция в этом блоке - вычисляем ее абсолютный адрес
    value_position = address.bit_position + (chunk_number * chunk_length) + search_result[0]
    while (len(search_result) == 1) and (value_position in skip_positions):
        # проверяем что найденная позиция не присутствует в списке игнорируемых
        search_result  = chunk.find(value, start=(search_result[0] + position_step), bytealigned=byte_aligned)
        if (len(search_result) == 0):
            break
        if ((chunk_length - search_result[0]) < len(value)):
            break
        value_position = address.bit_position + (chunk_number * chunk_length) + search_result[0]
    if (len(search_result) == 1) and (value_position not in skip_positions):
        # если найдена возвращаем ссылку на элемент как HashItemAddress
        value_address = HashItemAddress(value_position, len(value), address.seed)
        return value_address
    return search_result

def find_many_values_in_chunk(chunk: Bits, values: Set[Bits], value_length: int, position_step: int, skip_positions: set, \
        address: HashItemAddress, chunk_number: int, chunk_length: int, \
        byte_aligned: bool, value_format:str='bits') -> HashItemAddress:
    #search_result = chunk.find(value, bytealigned=byte_aligned)
    # проверяем найдена ли позиция в этом блоке - вычисляем ее абсолютный адрес
    #value_position = address.bit_position + (chunk_number * chunk_length) + search_result[0]
    chunk_value          = BitArray()
    chunk_value_position = 0
    prev_value_position  = 0
    for chunk_item in chunk.cut(position_step):
        chunk_value.append(chunk_item)
        if (len(chunk_value) < value_length):
            continue
        if (len(chunk_value) > value_length):
            chunk_value.clear()
            continue
        chunk_value_position = prev_value_position + len(chunk_value)
        value_position       = (address.bit_position + (chunk_number * chunk_length) + chunk_value_position) - len(chunk_value)
        if (value_position in skip_positions):
            chunk_value.clear()
            prev_value_position = chunk_value_position
            continue
        if (Bits(chunk_value) in values):
            value_address = HashItemAddress(value_position, len(chunk_value), address.seed)
            return value_address
        if (len(chunk_value) == value_length):
            chunk_value.clear()
        prev_value_position = chunk_value_position
    return None

def find_value_in_segment(value: Union[Bits, Set[Bits]], address: HashSegmentAddress, value_step: int=DEFAULT_VALUE_STEP, \
        chunk_length: int=DEFAULT_SEARCH_CHUNK_SIZE, byte_aligned: int=True, \
        skip_positions: set=set(), position_step:int=DEFAULT_POSITION_STEP,
        value_length: int=None) -> HashItemAddress:
    """
    Выполнить поиск значения в указанном сегменте пространства хешей: ищет указанное значение просматривая сегмент 
    полным перебором всех его значений.
    Если длина шага поиска (value_step) меньше чем длина значения (value) - то значение ищется методом 
    "sliding window" (сегмент разбивается на элементы и затем просматривается группами со сдвигом на 1 элемент)
    Пример: https://more-itertools.readthedocs.io/en/stable/api.html#more_itertools.sliding_window

    Parameters
    ----------
    value: Union[Bits, Set[Bits]]
        Значение которое надо найти (либо несколько значений одинаковой длины как множество)
    address: HashSegmentAddress
        Адрес и длина сегмента пространства хешей в котором производится поиск
    value_step: int
        Минимальная длина "одного кванта" значения (value) в битах, то есть длина 1 элемента из которых состоит значение: 
        - длина значения (value) должна без остатка делиться на это число
        - длина сегмента (address.bit_length) должна без остатка делиться на это число
        - должен быть совместим с форматом элемента (format)
        По умолчанию 8 бит
    chunk_length: int
        Размер блока памяти для кеширования значений хеш-пространства в процессе поиска (в битах)
        Должен быть кратным константе HASH_DIGEST_BITS
        По умолчанию равен 4 kb
    position_step: int
        Длина шага поиска (в битах), по умолчанию 8 бит
    
    >>> value_step          = DEFAULT_VALUE_STEP
    >>> segment_length      = (32 * HASH_DIGEST_BITS)
    >>> seed                = 0
    >>> position            = 0
    >>> address             = HashSegmentAddress(position, segment_length, seed)
    >>> chunk_length        = (2 * HASH_DIGEST_BITS)
    >>> value               = Bits('0xcd5e93')
    >>> item_address        = find_value_in_segment(value, address, value_step, chunk_length)
    >>> item_address
    HashItemAddress(bit_position=8, bit_length=24, seed=0)
    >>> read_hash_item(item_address) == value
    True
    >>> value               = Bits('0xefed2f')
    >>> item_address        = find_value_in_segment(value, address, value_step, chunk_length)
    >>> item_address
    HashItemAddress(bit_position=496, bit_length=24, seed=0)
    >>> read_hash_item(item_address) == value
    True
    >>> value               = Bits('0xefed2ff3')
    >>> item_address        = find_value_in_segment(value, address, value_step, chunk_length)
    >>> item_address
    HashItemAddress(bit_position=496, bit_length=32, seed=0)
    >>> read_hash_item(item_address) == value
    True
    >>> value               = Bits('0xe3ab')
    >>> item_address        = find_value_in_segment(value, address, value_step, chunk_length)
    >>> item_address
    HashItemAddress(bit_position=256, bit_length=16, seed=0)
    >>> read_hash_item(item_address) == value
    True
    >>> segment_length      = (128 * HASH_DIGEST_BITS)
    >>> address             = HashSegmentAddress(position, segment_length, seed)
    >>> value               = Bits('0x00')
    >>> item_address        = find_value_in_segment(value, address, value_step, chunk_length)
    >>> item_address
    HashItemAddress(bit_position=40, bit_length=8, seed=0)
    >>> read_hash_item(item_address) == value
    True
    >>> skip_positions      = {40}
    >>> item_address        = find_value_in_segment(value, address, value_step, chunk_length, skip_positions=skip_positions)
    >>> item_address
    HashItemAddress(bit_position=1104, bit_length=8, seed=0)
    >>> read_hash_item(item_address) == value
    True
    >>> skip_positions      = {40, 1104}
    >>> item_address        = find_value_in_segment(value, address, value_step, chunk_length, skip_positions=skip_positions)
    >>> item_address
    HashItemAddress(bit_position=2888, bit_length=8, seed=0)
    >>> read_hash_item(item_address) == value
    True
    >>> skip_positions      = set()
    >>> segment_length      = (2**13) * 8
    >>> seed                = 32
    >>> address             = HashSegmentAddress((2**4) * 8, segment_length, seed)
    >>> chunk_length        = segment_length
    >>> value               = Bits('0xddee')
    >>> item_address        = find_value_in_segment(value, address, value_step, chunk_length, skip_positions=skip_positions)
    >>> item_address
    HashItemAddress(bit_position=31248, bit_length=16, seed=32)
    >>> read_hash_item(item_address)
    Bits('0xddee')
    >>> skip_positions      = set()
    >>> segment_length      = (2**13) * 8
    >>> seed                = 0
    >>> address             = HashSegmentAddress(0, segment_length, seed)
    >>> chunk_length        = segment_length
    >>> values              = set([Bits('0xde'), Bits('0x00'), Bits('0xad')])
    >>> item_address = find_value_in_segment(values, address, value_step, chunk_length, skip_positions=skip_positions)
    >>> item_address
    HashItemAddress(bit_position=40, bit_length=8, seed=0)
    >>> read_hash_item(item_address) == Bits('0x00')
    True
    >>> skip_positions      = {40}
    >>> values              = set([Bits('0xde'), Bits('0x00'), Bits('0xad')])
    >>> item_address = find_value_in_segment(values, address, value_step, chunk_length, skip_positions=skip_positions)
    >>> item_address
    HashItemAddress(bit_position=1104, bit_length=8, seed=0)
    >>> read_hash_item(item_address) == Bits('0x00')
    True
    >>> skip_positions      = {40, 1104}
    >>> values              = set([Bits('0xde'), Bits('0x00'), Bits('0xad')])
    >>> item_address = find_value_in_segment(values, address, value_step, chunk_length, skip_positions=skip_positions)
    >>> item_address
    HashItemAddress(bit_position=1216, bit_length=8, seed=0)
    >>> read_hash_item(item_address)
    Bits('0xde')
    >>> read_hash_item(item_address) in values
    True
    >>> skip_positions      = set()
    >>> segment_length      = (2**13) * 8
    >>> seed                = 0
    >>> address             = HashSegmentAddress(0, segment_length, seed)
    >>> values              = set([Bits('0x5e93'), Bits('0x9200'), Bits('0x000f'), Bits('0xcd5e'), Bits('0x5e93'), Bits('0xefed'), Bits('0xe3ab'), Bits('0xe3ab')])
    >>> item_address        = find_value_in_segment(values, address, value_step, chunk_length, value_length=16, skip_positions=skip_positions)
    >>> item_address
    HashItemAddress(bit_position=16, bit_length=16, seed=0)
    >>> read_hash_item(item_address) in values
    True
    """
    if (address.bit_length % value_step != 0):
        raise Exception(f"Incorrect search step: (value_step={value_step}) for address.bit_length={address.bit_length} (address={address})")
    if (type(value) is set):
        if (value_length is None):
            value_length = len(list(value)[0])
    else:
        if (len(value) % value_step != 0):
            raise Exception(f"Incorrect search step: (value_step={value_step}) for value.length={len(value)} (value={value})")

    # TODO: определяем режим поиска в объекте Bits: только значения в начале байта или без ограничений:
    #       https://bitstring.readthedocs.io/en/latest/constbitarray.html#bitstring.Bits.find
    # byte_aligned       = (value_step % 8 == 0)
    # определяем количество элементов в значении
    items_per_value    = (len(value) // value_step)
    # определяем размещение сегмента в кеше
    if (address.bit_length < chunk_length):
        # если сегмент целиком помещается в кеш - то загружаем его целиком
        chunk_length = address.bit_length
    # определяем количество блоков кеша в сегменте
    chunks_per_segment = (address.bit_length // chunk_length)
    # проверяем что блок кеша нормально разделяется на элементы
    if (chunk_length % value_step != 0):
        raise Exception(f"Incorrect search step: (value_step={value_step}) for chunk_length={chunk_length} (address={address})")

    # вычисляем хеши блоками, затем сканируем каждый блок скользящим окном с шагом в 1 элемент в поисках значения
    for chunk_number in range(0, chunks_per_segment):
        if (type(value) in [Bits, BitArray, BitStream, ConstBitStream]):
            chunk  = load_chunk_from_segment(address, chunk_number, chunk_length, value_step, items_per_value, chunk_type=ConstBitStream)
            result = find_one_value_in_chunk(chunk, value, position_step, skip_positions, address, \
                chunk_number, chunk_length, byte_aligned=byte_aligned)
        elif (type(value) is set):
            chunk  = load_chunk_from_segment(address, chunk_number, chunk_length, value_step, items_per_value, chunk_type=Bits)
            result = find_many_values_in_chunk(chunk, value, value_length, \
                position_step=position_step, 
                skip_positions=skip_positions, 
                address=address, 
                chunk_number=chunk_number, 
                chunk_length=chunk_length, 
                byte_aligned=byte_aligned)
        else:
            raise Exception(f"incorrect value type: {type(value)}, value={value}")
        if (result):
            return result
    return None

def create_hash_segment_mapping(data_items: set, segment_items: set) -> dict:
    """
    Создать словарь замен, по которому значения из хеш-пространства заменяются в процессе чтения данных
    Содержит список значений, которые не встречаются в исходных данных и замену для каждого значения
    TODO: пока работает только для случаев когда в хеш-пространстве встречаются все значения

    >>> data_items    = set({'a', 'b', 'c', '0'})
    >>> segment_items = set({'a', 'b', 'c', 'd', 'e', 'f', '0', '1', '2', '3'})
    >>> mapping       = create_hash_segment_mapping(data_items, segment_items)
    >>> mapping
    {'c': 'c', 'b': 'b', 'a': 'a', '0': '0', 'f': 'c', 'e': 'b', 'd': 'a', '3': '0', '2': 'c', '1': 'b'}
    """
    data_items_list = sorted(list(data_items), reverse=True)
    # изначально запоминаем все случае когда замена НЕ происходит 
    mapping         = dict(zip(data_items_list, data_items_list))
    # список неиспользуемых значений которые заменяются при чтении данных из пространства хешей
    missing_items      = segment_items.difference(data_items)
    missing_items_list = sorted(list(missing_items), reverse=True)
    # заменяем все неиспользуемые значения используемыми
    for i in range(0, len(missing_items_list)):
        segment_item          = missing_items_list[i]
        target_data_item_id   = (i % len(data_items))
        target_data_item      = data_items_list[target_data_item_id]
        mapping[segment_item] = target_data_item
    return mapping
    
def create_adjustment_table_for_data(data: ConstBitStream, item_length: int, seed: int, \
        multiplier: int=DEFAULT_MULTIPLIER, format: str='hex', segment_position: int=DEFAULT_SEGMENT_POSITION) -> AdjustmentTable:
    """
    Создать таблицу для преобразования сегмента хеш-пространства под переданные данные с указанным разбиением:
    - определяет размер сегмента хеш-пространства (по множителю)
    - разбивает данные и сегмент хеш-пространства на элементы одинаковой длины
    - определяет количество уникальных элементов в данных и в хеш-пространстве и их частоту
    - определяет общие элементы и разницу в частоте использования каждого из них
    - определяет элементы, которые есть в хеш-пространстве, но не встречаются в данных
    - определяет элементы, которые есть в данных, но не встречаются в хеш-пространстве
    - составляет таблицу преобразования элементов сегмента хеш-пространства в элеметы данных
    
    Parameters
    ----------
    data: ConstBitStream
        Итератор для чтения данных
    item_length: int
        Длина элемента при разбиении (в битах)
        Исходные данные должны без остатка делиться на элементы указанной длины
    seed: int
        Seed-число для инициализации хеш-функции (номер пространства хешей: от 0 до 2**64)
    multiplier: int
        Множитель: количество раз которое данные должны покрыть хеш-пространство
        Определяет длину сегмента хеш-пространства
        Рекомендуемое значение 2 ** item_length
    format: str
        Формат элемента при разбиении: 
        https://bitstring.readthedocs.io/en/latest/constbitstream.html#bitstring.ConstBitStream.read
    segment_position: int
        Позиция начала сегмента (номер бита) в пространстве хешей (по умолчанию 0)
    
    >>> item_length         = 8
    >>> data_length         = (item_length * 64)
    >>> data_seed           = 999999
    >>> data_position       = 0
    >>> data_address        = HashSegmentAddress(data_position, data_length, data_seed)
    >>> data                = read_hash_segment(data_address)
    >>> hash_seed           = 0
    >>> multiplier          = (2 ** item_length)
    >>> adjustment_table    = create_adjustment_table_for_data(data, item_length, hash_seed, multiplier, format='hex')
    >>> adjustment_table.data_length == len(data)
    True
    >>> adjustment_table.segment_address.bit_length == (len(data) * multiplier)
    True
    >>> adjustment_table.segment_address.bit_length == adjustment_table.segment_length
    True
    >>> len(adjustment_table.data_counts)
    59
    >>> len(adjustment_table.data_items) == len(adjustment_table.data_counts)
    True
    >>> len(adjustment_table.segment_counts)
    256
    >>> len(adjustment_table.segment_items) == len(adjustment_table.segment_counts)
    True
    >>> len(adjustment_table.missing_data_items_in_segment) == 0
    True
    >>> len(adjustment_table.missing_segment_items_in_data) == (len(adjustment_table.segment_items) - len(adjustment_table.data_items))
    True
    >>> adjustment_table.data_counts.most_common()[0:8]
    [('8f', 2), ('df', 2), ('b9', 2), ('6d', 2), ('7b', 2), ('fa', 1), ('0b', 1), ('69', 1)]
    >>> adjustment_table.segment_counts.most_common()[0:8]
    [('19', 88), ('5b', 84), ('e8', 84), ('78', 83), ('3e', 81), ('c0', 81), ('7c', 81), ('bf', 80)]
    >>> adjustment_table.multiplier == multiplier
    True
    >>> len(adjustment_table.data_items_in_segment) == len(adjustment_table.data_items)
    True
    >>> len(adjustment_table.segment_items_in_data) == len(adjustment_table.data_items)
    True
    >>> sorted(list(adjustment_table.segment_items_in_data))[0:8]
    ['01', '08', '0b', '0c', '0d', '12', '1c', '1f']
    >>> len(adjustment_table.segment_items_map) == len(adjustment_table.segment_items)
    True
    >>> list(adjustment_table.segment_items_map.items())[0:4]
    [('fe', 'fe'), ('fd', 'fd'), ('fc', 'fc'), ('fa', 'fa')]
    """
    data_length = len(data)
    if (item_length == 0):
        raise Exception(f"Incorrect item_length={item_length}")
    if (data_length % item_length != 0):
        msg = f"Incorrect item_length=item_length for data_length={data_length} (try another item_length or add padding ti data)"
        raise Exception(msg)
    
    # определяем длину сегмента данных
    segment_length  = data_length * multiplier
    # создаем сегмент
    segment_address = HashSegmentAddress(segment_position, segment_length, seed)

    # вычисляем статистику использования значений элементов для данных
    data_item_counts    = count_split_values(data, item_length, format)
    # вычисляем статистику использования значений элементов для сегмента хеш-пространства
    segment_item_counts = count_segment_items(segment_address, item_length, format)
    
    # все уникальные значения из данных
    data_items    = set(data_item_counts.keys())
    # все уникальные значения из хеш-пространства
    segment_items = set(segment_item_counts.keys())

    # значения данных, которые не встречаются в сегменте хеш-пространства
    missing_data_items_in_segment = data_items.difference(segment_items)
    # значения хеш-апрстранства, которые не встречаются в данных
    missing_segment_items_in_data = segment_items.difference(data_items)

    # все значения элемнтов из хеш-пространства, которые присутствуют в данных
    segment_items_in_data = data_items.intersection(segment_items)
    # все значения элемнтов данных, которые присутствуют из хеш-пространства
    data_items_in_segment = segment_items.intersection(data_items)

    # основная таблица для замен значений: ключи - элементы хешей, значения - элементы данных
    # TODO: создавать таблицу с учетом частоты упоминания каждого значения
    segment_items_map = create_hash_segment_mapping(data_items, segment_items)
    
    # создаем таблицу для изменения сегмента данных под данные
    adjustment_table = AdjustmentTable(
        data_items,
        segment_items,
        data_length,
        item_length,
        segment_length,
        segment_address,
        data_item_counts,
        segment_item_counts,
        segment_items_in_data,
        data_items_in_segment,
        missing_data_items_in_segment,
        missing_segment_items_in_data,
        segment_items_map,
        multiplier
    )
    return adjustment_table

def adjust_hash_segment_for_data(data: ConstBitStream, item_length: int, seed: int, \
        multiplier: int=DEFAULT_MULTIPLIER, format: str='hex') -> ConstBitStream:
    """
    Преобразовать сегмент пространства хешей под переданные данные:
    - разбивает данные и сегмент хеш-пространства на элементы одинаковой длины
    - определяет количество уникальных элементов в данных и в хеш-пространстве и их частоту
    - определяет общие элементы и разницу в частоте использования каждого из них
    - определяет элементы, которые есть в хеш-пространстве, но не встречаются в данных
    - определяет элементы, которые есть в данных, но не встречаются в хеш-пространстве
    - составляет таблицу преобразования элементов сегмента хеш-пространства в элеметы данных
    - определяет размер сегмента хеш-пространства (по множителю)
    - циклически применяет таблицу замены ко всему хеш-пространству чтобы уменьшить 
      Хемминговское расстояние между данными и сегментом хеш-пространства (very dirty hack)
    
    Parameters
    ----------
    data: ConstBitStream
        Итератор для чтения данных
    item_length: int
        Длина элемента при разбиении (в битах)
    seed: int
        Seed-число для инициализации хеш-функции (номер пространства хешей: от 0 до 2**64)
    multiplier: int
        Множитель: количество раз которое данные должны покрыть хеш-пространство
        Определяет длину сегмента хеш-пространства
    format: str
        Формат элемента при разбиении: 
        https://bitstring.readthedocs.io/en/latest/constbitstream.html#bitstring.ConstBitStream.read
    
    
    """
    table            = create_adjustment_table_for_data(data, item_length, seed, multiplier, format)
    hash_segment     = read_hash_segment(table.segment_address)
    adjusted_segment = BitStream()
    total_items      = table.segment_length // table.item_length
    format_prefix    = get_format_prefix(format)
    
    for item_number in range(0, total_items):
        item_start = item_number * table.item_length
        item_end   = item_start + table.item_length
        item_key   = hash_segment.read(f"{format}:{table.item_length}")
        # заменяем элементы в хеш-пространстве по таблице
        if (item_key in table.segment_items_map):
            if (format == 'uint'):
                item_value = table.segment_items_map[item_key]
                item       = BitArray(uint=item_value, length=item_length)
            else:
                item_value = format_prefix + f"{table.segment_items_map[item_key]}"
                item       = BitArray(item_value)
        else:
            item = hash_segment[item_start:item_end]
        adjusted_segment.append(item)
    # возвращаем измененный сегмент в котором все неиспользуемые элементы заменены на используемые
    return adjusted_segment