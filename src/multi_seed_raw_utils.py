# https://stackoverflow.com/questions/38340808/recursive-typing-in-python-3-5
from __future__ import annotations
from rich import print
from rich.pretty import pprint
from custom_counter import CustomCounter as Counter
from tqdm import tqdm
from bitarray.util import ba2int, int2ba
from bitarray import bitarray, frozenbitarray
from collections import defaultdict, namedtuple
from dataclasses import dataclass
from copy import deepcopy, copy
from typing import List, Set, Dict, Tuple, Optional, Union, Iterable
from functools import lru_cache
import xxhash
import math
from bitarrayset.binaryarrayset import BinaryArraySet
#from mongoengine import register_connection
#sregister_connection('default', db=f'256_byte_segments', host='127.0.0.1', port=27017, maxPoolSize=300)
from mongoengine import Document, QuerySet, LongField, IntField, \
    BinaryField, StringField, ListField, SortedListField, BooleanField, queryset_manager

#from hash_space_utils import get_min_bit_length
from hash_range_iterator import HASH_DIGEST_BITS, bits_at_position, int_bytes_from_digest, int_from_nounce,\
    bytes_from_nounce, get_min_bit_length, nounce_to_input
from fib_encoder import fib_encode_position, get_fib_lengths, get_fib_value_range, get_fib_position_ranges


def get_number_offset(number: int) -> int:
    number_length = number.bit_length()
    if (number_length == 0):
        return 0
    offset = 0
    for i in range(0, number_length):
        offset += 2**i
    return offset

def encode_prefixed_varint(number: int, prefix_value: int, prefix_length: int) -> bitarray:
    prefix = int2ba(prefix_value, length=prefix_length, endian='big', signed=False)
    if (prefix_value > 0):
        value = int2ba(number, length=prefix_value, endian='big', signed=False)
    else:
        value = bitarray()
    return prefix + value

def create_number_ranges(max_length: int) -> List[Tuple[int, int]]:
    ranges    = []
    max_value = 0
    for number_length in range(0, max_length):
        min_value = max_value
        max_value = min_value + 2**number_length
        ranges.append((min_value, max_value))
    return ranges

SEED_PREFIX_BITS   = 4
NOUNCE_PREFIX_BITS = 4
HASH_INPUT_BYTES   = 4
MIN_ITEM_LENGTH    = SEED_PREFIX_BITS + NOUNCE_PREFIX_BITS + 1
MAX_ITEM_LENGTH    = 64
SAVE_BATCH_SIZE    = 16384
MAX_CACHED_LENGTH  = 31

LengthRange = namedtuple('LengthRange', [
    'seed_length',
    'nounce_length',
    'item_length',
    'total_seeds',
    'total_nounces',
    'total_items',
    'value_length',
    'item_score',
])

seed_lengths      = [sl for sl in range(0, 2**SEED_PREFIX_BITS)]
SEED_RANGES       = create_number_ranges(max_length=2**SEED_PREFIX_BITS)
min_seed_length   = min(seed_lengths)
max_seed_length   = max(seed_lengths)

nounce_lengths    = [nl for nl in range(0, 2**NOUNCE_PREFIX_BITS)]
NOUNCE_RANGES     = create_number_ranges(max_length=2**NOUNCE_PREFIX_BITS)
min_nounce_length = min(nounce_lengths)
max_nounce_length = max(nounce_lengths)

item_length_counts = Counter()
item_length_limits = Counter()
item_length_ranges = defaultdict(list)
total_available_items = 0

for seed_length in seed_lengths:
    min_seed    = SEED_RANGES[seed_length][0]
    max_seed    = SEED_RANGES[seed_length][1]
    total_seeds = max_seed - min_seed
    for nounce_length in nounce_lengths:
        min_nounce    = NOUNCE_RANGES[nounce_length][0]
        max_nounce    = NOUNCE_RANGES[nounce_length][1]
        total_nounces = max_nounce - min_nounce
        item_length   = SEED_PREFIX_BITS + NOUNCE_PREFIX_BITS + seed_length + nounce_length
        total_items   = total_seeds * total_nounces
        value_length  = item_length + 1
        if (value_length > 32):
            value_length = 32
        item_score = value_length - item_length
        # убираем из области поиска все диапазоны пораждающие значения с длиной больше разрешенной 
        if (item_length > MAX_ITEM_LENGTH):
            continue
        
        item_length_counts.update({ item_length: 1 })
        item_length_limits.update({ item_length: total_items })
        total_available_items += total_items

        item_length_ranges[item_length].append(LengthRange(
            seed_length   = seed_length,
            nounce_length = nounce_length,
            item_length   = item_length,
            total_seeds   = total_seeds,
            total_nounces = total_nounces,
            total_items   = total_items,
            value_length  = value_length,
            item_score    = item_score,
        ))
ITEM_LENGTH_RANGES = item_length_ranges
ITEM_LENGTH_COUNTS = item_length_counts
ITEM_LENGTH_LIMITS = item_length_limits

### BASE CLASSES ###

class BaseRawItemValue(Document):
    id             = IntField(primary_key=True)
    seed           = IntField(null=False, required=True)
    seed_length    = IntField(null=False, required=True)
    nounce         = IntField(null=False, required=True)
    nounce_length  = IntField(null=False, required=True)
    value          = IntField(null=False, required=True)
    value_length   = IntField(null=False, required=True)
    item_length    = IntField(null=False, required=True)
    # collection metadata
    meta = {
        'abstract': True,
        'indexes': [
            'seed',
            #'seed_length',
            'nounce',
            #'nounce_length',
            'value',
            #'value_length',
            #'item_length',
        ],
    }

class BaseEncodedDataItem(Document):
    id             = IntField(primary_key=True)
    seed           = IntField(null=False, required=True)
    seed_length    = IntField(null=False, required=True)
    nounce         = IntField(null=False, required=True)
    nounce_length  = IntField(null=False, required=True)
    value          = IntField(null=False, required=True)
    value_length   = IntField(null=False, required=True)
    bit_position   = IntField(null=False, required=True)
    item_score     = IntField(null=False, required=True)
    total_score    = IntField(null=False, required=True)
    #file_name      = StringField(null=False, required=True)
    # collection metadata
    meta = {
        'abstract': True,
        'indexes': [
            'seed',
            'seed_length',
            'nounce',
            'nounce_length',
            'value',
            'value_length',
            'bit_position',
            'item_score',
            #'total_score',
            #'file_name',
        ],
    }

### CACHE ###

CACHED_CLASSES     = dict()
CACHED_CLASS_NAMES = set()

def has_cached_class(class_name: str) -> bool:
    return (class_name in CACHED_CLASS_NAMES)

def get_cached_class(class_name: str) -> Document:
    return CACHED_CLASSES.get(class_name)

### COLLECTION FACTORIES ###

def get_raw_item_class_name(seed_bits: int, nounce_bits: int, seed_length: int, nounce_length: int, value_length: int, item_length: int) -> str:
    return f"RawItemValueSB{seed_bits}NB{nounce_bits}VL{value_length:02}SL{seed_length:02}NL{nounce_length:02}IL{item_length:02}"

def get_encoded_data_item_class_name(encoder_type: str, start_max_value_length: int, expand_distance: int) -> str:
    return f"EncodedDataItem{encoder_type}SMVL{start_max_value_length:02}ED{expand_distance:02}"

def get_raw_item_collection_name(seed_bits: int, nounce_bits: int, seed_length: int, nounce_length: int, value_length: int, item_length: int) -> str:
    return f"raw_item_value_s{seed_bits}_n{nounce_bits}_vl{value_length:02}_sl{seed_length:02}_nl{nounce_length:02}_il{item_length:02}"

def get_encoded_data_item_collection_name(encoder_type: str, start_max_value_length: int, expand_distance: int) -> str:
    return f"encoded_data_item_{encoder_type.lower()}_smvl{start_max_value_length:02}_ed{expand_distance:02}"

def make_raw_item_value_class(seed_bits: int, nounce_bits: int, seed_length: int, nounce_length: int, value_length: int, item_length: int) -> BaseRawItemValue:
    return type(get_raw_item_class_name(
        seed_bits     = seed_bits, 
        nounce_bits   = nounce_bits, 
        seed_length   = seed_length,
        nounce_length = nounce_length,
        value_length  = value_length,
        item_length   = item_length), (BaseRawItemValue, ), {
        "meta" : {
            'collection': get_raw_item_collection_name(
                seed_bits     = seed_bits, 
                nounce_bits   = nounce_bits, 
                seed_length   = seed_length,
                nounce_length = nounce_length,
                value_length  = value_length,
                item_length   = item_length
            ),
        }
    })

def make_encoded_data_item_value_class(encoder_type: str, start_max_value_length: int, expand_distance: int) -> BaseEncodedDataItem:
    return type(get_encoded_data_item_class_name(
            encoder_type           = encoder_type,
            start_max_value_length = start_max_value_length,
            expand_distance        = expand_distance
        ), (BaseEncodedDataItem, ), {
        "meta" : {
            'collection': get_encoded_data_item_collection_name(
                encoder_type           = encoder_type,
                start_max_value_length = start_max_value_length,
                expand_distance        = expand_distance
            ),
        }
    })

def get_raw_item_value_class(seed_bits: int, nounce_bits: int, seed_length: int, nounce_length: int, value_length: int, item_length: int) -> Union[BaseRawItemValue, Document]:
    class_name = get_raw_item_class_name(
        seed_bits     = seed_bits, 
        nounce_bits   = nounce_bits, 
        seed_length   = seed_length,
        nounce_length = nounce_length,
        value_length  = value_length,
        item_length   = item_length
    )
    if (has_cached_class(class_name) is False):
        CACHED_CLASSES[class_name] = make_raw_item_value_class(
                seed_bits     = seed_bits, 
                nounce_bits   = nounce_bits, 
                seed_length   = seed_length,
                nounce_length = nounce_length,
                value_length  = value_length,
                item_length   = item_length
            )
        CACHED_CLASS_NAMES.add(class_name)
    return get_cached_class(class_name)

def get_encoded_data_item_value_class(encoder_type: str, start_max_value_length: int, expand_distance: int) -> Union[BaseEncodedDataItem, Document]:
    class_name = get_encoded_data_item_class_name(
        encoder_type           = encoder_type,
        start_max_value_length = start_max_value_length,
        expand_distance        = expand_distance
    )
    if (has_cached_class(class_name) is False):
        CACHED_CLASSES[class_name] = make_encoded_data_item_value_class(
                encoder_type           = encoder_type,
                start_max_value_length = start_max_value_length,
                expand_distance        = expand_distance
            )
        CACHED_CLASS_NAMES.add(class_name)
    return get_cached_class(class_name)

### END OF METACLASSES ###

def get_all_value_classes(seed_bits: int, nounce_bits: int, seed_length: int, nounce_length: int, value_length: int, item_length: int):
    pass

# result generated by create_contant_based_split()
ContentBasedSplit = namedtuple('ContentBasedSplit', [
    # split result (list of items of different length)
    'split_items',
    # unique elements only (data dictionary)
    'data_values',
    'value_ids',
    'unique_values',
    # this flag is True when split process was not finished because all tier dictionaries are full
    'capacity_overflow',
    # total number of number of unique values, grouped by length
    'length_counts',
    # number of usages of each dict value
    'value_counts',
    # popularity of values for each value length
    'length_usage_counts',
    # length of the result list
    'encoded_items_count',
    # number of unique values in dict
    'unique_values_count',
    # (required to continue split process with several iterations, used when capacity_overflow=True)
    'position',
])

def create_content_based_split(data: frozenbitarray, seed_bits: int, nounce_bits: int) -> ContentBasedSplit:
    """
    Split data to short items (several byte each), each item value should be unique.
    Process goes from short to values. Number of items of each length is defined separatley (based on leb_128 encoding). 
    """
    # sets to True when all dictionaries are full, but there is still more unique data items to process
    capacity_overflow   = False
    # unique fragments of data
    data_values         = set()
    unique_values       = defaultdict(list)
    value_ids           = list()
    # oridinal data, splitted into items according our dictionary
    split_items         = list()
    # number of usages for each byte value
    value_counts        = Counter()
    length_usage_counts = Counter()
    # combined usage of dictionary values, grouped by item length
    length_counts       = Counter()
    position            = 0
    # set up maximum values of each length
    fib_lengths         = get_fib_lengths(encoder_type='C2')
    max_fib_length      = 32
    max_items_by_length = dict()
    value_lengths       = list()
    for v_length, max_items in fib_lengths.items():
        max_items_by_length[v_length] = max_items
        value_lengths.append(v_length)
        if (v_length >= max_fib_length):
            break
    max_value_length = max(value_lengths)
    # read data using variable length items
    while (True):
        item_value = None
        # length of next value depends on current dictionary state: short values collected first,
        # new values will be added only if we don't meet them previously
        for value_length in value_lengths: #range(min_value_length, 32 + 1):
            # try to read and use short values first, use long values only if we dont have short ones
            v_start    = position
            v_end      = position + value_length
            scan_value = data[v_start:v_end]
            if (scan_value not in data_values):
                # new (unique) value recieved
                if length_counts[value_length] >= max_items_by_length[value_length]:
                    # reached maximum capacity for values with given length
                    if (value_length >= max_value_length):
                        # maximum unique values recieved - dictionary overflow
                        capacity_overflow = True
                        break
                    else:
                        # use capacity from next length tier
                        continue
                else:
                    # add new value to dictionary, updating item counter (and decreace tier capacity by 1 value)
                    length_counts.update({value_length : 1})
                    item_value = scan_value
                    position  += value_length
                    break
            else:
                # existing (not unique) value recieved - do not modify our dictionary, but save item value to split result
                item_value = scan_value
                position  += value_length
                break
        if capacity_overflow == True:
            # input max_value_length is not enough to create a dictionary
            # raise Exception(f"Length capacity reached: {length_counts} (items_processed={len(split_items)}, max_value_length={max_value_length})")
            break
        # update usage counter of the dictionary value
        item_value_int = ba2int(item_value, signed=False)
        value_counts.update({(value_length, item_value_int) : 1})
        # append item value to final result
        split_items.append(item_value)
        # add item value to data dictionary
        data_values.add(item_value)
        # group int values by length
        if (item_value_int not in unique_values[value_length]):
            unique_values[value_length].append(item_value_int)
            value_ids.append((len(value_ids), value_length, item_value_int))
        if ((len(data) - position) <= max_value_length):
            # all data processed:
            # last item will be saved and returned in 'remaining_data' field inside result dict (if any)
            break
    for v_item, v_count in value_counts.items():
        v_length = v_item[0]
        length_usage_counts.update({(v_count, v_length): 1})
    # final result
    return ContentBasedSplit(
        split_items         = tuple(split_items),
        data_values         = data_values,
        value_ids           = value_ids,
        unique_values       = unique_values,
        capacity_overflow   = capacity_overflow,
        length_counts       = length_counts,
        value_counts        = value_counts,
        length_usage_counts = length_usage_counts,
        encoded_items_count = len(split_items),
        unique_values_count = len(data_values),
        position            = position,
    )

def create_l_limits(prefix_bits: int, total_tiers: int) -> Dict(int, int):
    l_limits        = dict()
    reserved_tiers  = 32 - total_tiers
    reserved_length = prefix_bits + 1
    
    for p_length in range(prefix_bits, prefix_bits + total_tiers):
        v_length           = p_length + 1
        max_values         = 2**(p_length - prefix_bits)
        l_limits[v_length] = max_values
    
    #l_limits[reserved_length] += reserved_tiers
    return l_limits

@dataclass(init=True)
class DictPrefixTree:
    value_lengths  : List[int]
    encoded_values : Dict[int, Set[int]]
    target_nounces : Dict[int, List[int]] 
    used_nounces   : Dict[int, Set[int]]
    value_nounces  : Dict[int, Dict[int, int]]
    value_seeds    : Dict[int, Dict[int, int]]
    nounce_seeds   : Dict[int, int]

    excluded_lengths : List[int]
    excluded_values  : Dict[int, Set[int]]
    restored_nounces : Dict[int, List[int]]

    #new_value        : int
    #new_value_length : int
    #new_nounce       : int
    #new_seed         : int
    #new_seed_shift   : int
    removed_count    : int = 0

    def __init__(self, value_lengths=None, encoded_values=None, target_nounces=None, used_nounces=None, 
            value_nounces=None, value_seeds=None, nounce_seeds=None,
            excluded_lengths: List[int]=None, excluded_values: Dict[int, Set[int]]=None, 
            restored_nounces: Dict[int, List[int]]=None):
        self.value_lengths  = value_lengths  
        self.encoded_values = encoded_values 
        self.target_nounces = target_nounces 
        self.used_nounces   = used_nounces   
        self.value_nounces  = value_nounces  
        self.value_seeds    = value_seeds
        self.nounce_seeds   = nounce_seeds
        
        self.excluded_lengths = set()
        self.excluded_values  = defaultdict(set)
        self.restored_nounces = defaultdict(list)
        self.replaced_seeds   = defaultdict(dict)
        if (excluded_lengths is not None):
            self.excluded_lengths = excluded_lengths
        if (excluded_values is not None):
            self.excluded_values = excluded_values
        if (restored_nounces is not None):
            self.restored_nounces = restored_nounces

def shake_prefix_tree(prefix_tree: DictPrefixTree, value_length: int, new_value: int) -> DictPrefixTree:
    target_divider = 2**value_length
    removed_values = list()
    removed_seeds  = list()
    enc_nv         = int2ba(new_value, length=value_length, endian='little', signed=False)

    for tier_value_length in prefix_tree.value_lengths:
        if (tier_value_length <= value_length):
            continue
        for tier_value in prefix_tree.encoded_values[tier_value_length]:
            tier_value_prefix = tier_value % target_divider
            if (tier_value_prefix == new_value):
                prefix_tree.removed_count += 1
                tier_value_nounce          = prefix_tree.value_nounces[tier_value_length][tier_value]
                enc_tv                     = int2ba(tier_value, length=tier_value_length, endian='little', signed=False)
                removed_values.append((tier_value_length, enc_tv)) # , tier_value_nounce
                removed_seeds.append((tier_value_nounce, prefix_tree.nounce_seeds[tier_value_nounce]))
                prefix_tree.excluded_lengths.add(tier_value_length)
                #suffix_tree.encoded_values[tier_value_length].remove(tier_value)
                #suffix_tree.target_nounces[tier_value_length].add(tier_value_nounce)
                prefix_tree.excluded_values[tier_value_length].add(tier_value)
                prefix_tree.restored_nounces[tier_value_length].append(tier_value_nounce)
    
    prefix_tree.excluded_lengths = sorted(list(prefix_tree.excluded_lengths))

    if (prefix_tree.removed_count > 0):
        print(f"ts ({value_length}): {enc_nv}, ({prefix_tree.removed_count}): {sorted(removed_values)}, l={prefix_tree.excluded_lengths}, rs={removed_seeds}")

    return prefix_tree

def load_seed_values(seed: int, target_nounces: Dict[int, List[int]], value_lengths: List[int]) -> Dict[int, Set[int]]: # , encoded_values: Dict[int, Set[int]]
    seed_values = defaultdict(set)
    for value_length in value_lengths:
        for nounce in target_nounces[value_length]:
            hash_input = nounce.to_bytes(length=4, byteorder='little', signed=False)
            digest     = xxhash.xxh32_intdigest(input=hash_input, seed=seed)
            item_value = digest % (2**value_length)
            seed_values[value_length].add(item_value)
    return seed_values

def make_content_based_split(data: frozenbitarray, start_max_value_length: int, expand_distance: int):
    nounce_encoder_type = 'C2'
    seed_encoder_type   = 'C1'
    EDVClass            = get_encoded_data_item_value_class(
        encoder_type           = nounce_encoder_type, 
        start_max_value_length = start_max_value_length,
        expand_distance        = expand_distance
    )
    EDVClass.drop_collection()
    #prefix_bits         = 0 #5
    #total_tiers         = 0 #14
    #reserved_tiers      = 0 #(2**prefix_bits) - total_tiers
    #reserved_length     = 0 #prefix_bits + 1

    #l_limits   = create_l_limits(prefix_bits=prefix_bits, total_tiers=total_tiers)
    all_limits  = get_fib_lengths(encoder_type=nounce_encoder_type)
    l_limits    = dict()
    for fib_vl, fib_max_items in all_limits.items():
        l_limits[fib_vl] = fib_max_items
        if (fib_vl >= start_max_value_length):
            break
    max_value_length                = start_max_value_length
    min_items_for_next_value_length = all_limits[max_value_length-expand_distance]
    #nounce_ranges       = create_number_ranges(max_length=(2**prefix_bits))
    #nounce_ranges       = get_fib_position_ranges(encoder_type=nounce_encoder_type, max_length=len(l_limits)) #create_number_ranges(max_length=(2**prefix_bits))
    #max_fib_length      = total_tiers + prefix_bits
    max_items_by_length = dict()
    value_lengths       = list()
    # сохранённая связь между добавленными в словарь значениями и исключенными из поиска позициями
    # используется в процессе заполнения словаря для его оптимизации: более короткие значения постепенно 
    # вытесняют более длинные
    # все длинные значения которые оказываются суффиксами более коротких должны быть удалены из словаря,
    # а их nounce-позиции должны быть обратно добавлены в список для сканирования
    # данные при этом не теряются, потому что при восстановлении словарь строиться в той же последовательности,
    # теми же шагами, с теми же заменами и при вычислении следующей позиции опирается
    # только на ранее добавленные в словарь элементы, а их состав и последовательность замен всегда
    # полностью совпадает для операций построения словаря при упаковке и при восстанослении словаря при распаковке
    value_nounces       = defaultdict(dict)
    # то же самое соотношение, только для сидов
    value_seeds         = defaultdict(set)
    # какой сид для какого nounce применять
    nounce_seeds        = dict()
    item_use_counts     = defaultdict(Counter)
    unique_item_counts  = Counter()
    total_score         = 0 #reserved_tiers * reserved_length * (-1)
    min_value_length    = min(list(l_limits.keys()))
    max_value_length    = max(list(l_limits.keys()))
    encoded_values      = defaultdict(set)
    target_nounces      = defaultdict(list)
    used_nounces        = defaultdict(set)
    used_nounce_counts  = Counter()
    free_nounce_counts  = Counter()
    cached_seed_values  = defaultdict(lambda: defaultdict(set))
    #cached_seed_value_nounces = defaultdict(lambda: defaultdict(dict))
    #cached_seed_nounce_values = defaultdict(lambda: defaultdict(dict))

    print(f"nounce_encoder={nounce_encoder_type}, seed_encoder={seed_encoder_type}, start_max_length={start_max_value_length}:")
    print(f"expand_distance={expand_distance} (expansion after {min_items_for_next_value_length} items)")
    print(f"l_limits:")
    pprint(l_limits)
    
    for v_length, max_items in l_limits.items():
        max_items_by_length[v_length] = max_items
        value_lengths.append(v_length)
        #p_length                 = v_length - reserved_length
        nounce_range             = get_fib_value_range(value_length=(v_length - 1), encoder_type=nounce_encoder_type)
        #min_nounce               = nounce_ranges[p_length][0]
        #max_nounce               = nounce_ranges[p_length][1]
        #min_nounce               = nounce_ranges[p_length][0]
        #max_nounce               = nounce_ranges[p_length][1]
        #nounce_range             = range(min_nounce, max_nounce)
        nounce_range             = range(nounce_range[0], nounce_range[1])
        print(f"v_length={v_length}, nounce_range={nounce_range}")
        target_nounces[v_length]     = [n for n in nounce_range]
        free_nounce_counts.update({ v_length: len(target_nounces[v_length]) })
        #if (v_length == min_value_length) and (reserved_tiers > 0):
        #    # добавляем в область самых коротких значений словаря столько же уникальных nounce-значений сколько было
        #    # исключено из списка отвечающего за длину адреса (схема мутная, но похоже работает)
        #    for i in range(0, reserved_tiers):
        #        target_nounces[v_length].append(0)
        #        max_items_by_length[v_length] += 0 #1
    
    print(f"target_nounces (nounce_encoder={nounce_encoder_type}, seed_encoder={seed_encoder_type}): {len(target_nounces)}")
    print(f"value_lengths (start_max_length={start_max_value_length}): {value_lengths}")
    print(f"min_vl={min_value_length}: {len(target_nounces[min_value_length])}")
    print(f"max_vl={max_value_length}: {len(target_nounces[max_value_length])}")
    print(f"max_it={min_value_length}: {max_items_by_length[min_value_length]}")
    
    value_lengths        = sorted(value_lengths)
    # original data, splitted into items according our dictionary
    split_items          = list()
    # number of usages for each byte value
    value_counts         = Counter()
    base_shift_counts    = Counter()
    extra_shift_counts   = Counter()
    encoded_value_counts = Counter()
    seed_counts          = Counter()
    # init scan counters
    progress              = tqdm(total=len(data), mininterval=0.5, smoothing=0)
    seed                  = 0
    position              = 0
    extra_seed_offset     = 0
    base_seed_offset      = 0
    value_found           = False
    #cached_seed_values[0] = load_seed_values(seed=0, target_nounces=target_nounces, value_lengths=value_lengths)

    while (True):
        if ((len(data) - position) <= 32):
            break
        value_found = False
        for value_length in value_lengths:
            prev_lengths = list()
            for p_length in value_lengths:
                if (p_length > value_length):
                    break
                prev_lengths.append(p_length)
            # load target item value
            tv_start          = position
            tv_end            = tv_start + value_length
            data_item_value   = data[tv_start:tv_end]
            target_value      = ba2int(data_item_value, signed=False)

            if (target_value in encoded_values[value_length]):
                value_found  = True
                position    += value_length
                total_score += 1
                item_use_counts[value_length].update({target_value: 1})
                value_counts.update({ data_item_value.to01(): 1 })
                split_items.append(data_item_value)
                canonical_seed      = nounce_seeds[value_nounces[value_length][target_value]]
                canonical_seed_cost = len(fib_encode_position(position=canonical_seed, encoder_type=seed_encoder_type))
                # update total progress
                print(f"{None} ({(value_counts[data_item_value.to01()]-canonical_seed_cost):2}={value_counts[data_item_value.to01()]}-{canonical_seed_cost}), vl={value_length} ({encoded_value_counts[value_length]}/{max_items_by_length[value_length]}), p={position} ({len(split_items)}/{len(value_counts)}), tv={target_value} ('{data_item_value.to01()}': {value_counts[data_item_value.to01()]}), total_score={total_score} (+1)")
                progress.update(value_length)
                progress.set_description_str(f"vl={value_length}, o=(+{extra_seed_offset}, +{base_seed_offset}), tv={target_value}, ncs=({len(target_nounces[value_length])}), seed={seed}", refresh=True)
                # saving item to db
                encoded_data_value = EDVClass(
                    id             = len(split_items),
                    seed           = canonical_seed,
                    seed_length    = 0,
                    nounce         = value_nounces[value_length][target_value],
                    nounce_length  = (value_length - 1),
                    value          = target_value,
                    value_length   = value_length,
                    bit_position   = (position - value_length),
                    item_score     = 1,
                    total_score    = total_score,
                )
                encoded_data_value.save()
                break
            
            # starting value length nounces scan
            for nounce in target_nounces[value_length]:
                base_seed_offset = 0
                # stop when maximum unique values of current length is found (infinity loop prevention)
                if (unique_item_counts[value_length] >= 2**value_length):
                    break
                while (True):
                    seed       = extra_seed_offset + base_seed_offset
                    hash_input = nounce.to_bytes(length=4, byteorder='little', signed=False)
                    digest     = xxhash.xxh32_intdigest(input=hash_input, seed=seed)
                    has_prefix = False
                    is_unique  = True
                    
                    current_seed_value = digest % (2**value_length)
                    for prev_seed in range(0, extra_seed_offset+1):
                        if (current_seed_value in cached_seed_values[prev_seed][value_length]):
                            is_unique = False
                            break
                    if (is_unique is False):
                        base_seed_offset += 1
                        continue

                    for prev_length in prev_lengths:
                        prev_value = digest % (2**prev_length)
                        if (prev_value in encoded_values[prev_length]):
                            has_prefix = True
                            break
                    if (has_prefix is True):
                        base_seed_offset += 1
                        continue
                    
                    if (has_prefix is False) and (is_unique is True):
                        cached_seed_values[extra_seed_offset][value_length].add(current_seed_value)
                        unique_item_counts.update({ value_length: 1 })
                        #cached_seed_value_nounces[extra_seed_offset][value_length][current_seed_value] = nounce
                        #cached_seed_nounce_values[extra_seed_offset][value_length][nounce] = current_seed_value
                        break
                # check new value against target
                if (value_length < 32):
                    item_value = digest % (2**value_length)
                else:
                    item_value = digest
                if (item_value == target_value):
                    value_found = True
                    break
            if (value_found is True):
                value_nounces[value_length][target_value] = nounce
                nounce_seeds[nounce]                      = extra_seed_offset
                encoded_values[value_length].add(target_value)
                target_nounces[value_length].remove(nounce)
                used_nounces[value_length].add(nounce)
                base_shift_counts.update({ base_seed_offset: 1 })
                extra_shift_counts.update({ extra_seed_offset: 1 })
                encoded_value_counts.update({ value_length: 1 })
                item_use_counts[value_length].update({ target_value: 1 })
                value_counts.update({ data_item_value.to01(): 1 })
                used_nounce_counts.update({ value_length: 1 })
                free_nounce_counts.update({ value_length: -1 })
                seed_counts.update({ seed: 1 })
                split_items.append(data_item_value)
                
                target_points = item_use_counts[value_length][target_value]
                seed_cost     = len(fib_encode_position(position=extra_seed_offset, encoder_type=seed_encoder_type))
                target_score  = target_points - seed_cost
                total_score   = total_score + target_score
                position      = position + value_length

                # saving item to db
                encoded_data_value = EDVClass(
                    id             = len(split_items),
                    seed           = extra_seed_offset,
                    seed_length    = seed_cost,
                    nounce         = nounce,
                    nounce_length  = (value_length - 1),
                    value          = target_value,
                    value_length   = value_length,
                    bit_position   = (position - value_length),
                    item_score     = target_score,
                    total_score    = total_score,
                )
                encoded_data_value.save()

                # tree shaking
                dpt = DictPrefixTree(
                    value_lengths     = value_lengths,
                    encoded_values    = encoded_values,
                    target_nounces    = target_nounces,
                    used_nounces      = used_nounces,
                    value_nounces     = value_nounces,
                    value_seeds       = value_seeds,
                    nounce_seeds      = nounce_seeds,
                )
                prefix_tree = shake_prefix_tree(prefix_tree=dpt, value_length=value_length, new_value=target_value)
                if (prefix_tree.removed_count > 0):
                    for pt_excluded_length in prefix_tree.excluded_lengths:
                        for pt_excluded_value in prefix_tree.excluded_values[pt_excluded_length]:
                            encoded_values[pt_excluded_length].remove(pt_excluded_value)
                            del value_nounces[pt_excluded_length][pt_excluded_value]
                        for pt_excluded_nounce in prefix_tree.restored_nounces[pt_excluded_length]:
                            used_nounces[pt_excluded_length].remove(pt_excluded_nounce)
                            target_nounces[pt_excluded_length].append(pt_excluded_nounce)
                            target_nounces[pt_excluded_length] = sorted(target_nounces[pt_excluded_length])
                            used_nounce_counts.update({ pt_excluded_length: -1 })
                            free_nounce_counts.update({ pt_excluded_length: 1 })
                            del nounce_seeds[pt_excluded_nounce]
                # end of tree shaking
                
                progress.update(value_length)
                progress.set_description_str(f"vl={value_length:02}, o=(+{base_seed_offset}, +{extra_seed_offset}), tv={data_item_value.to01()} (s: {target_points}-{seed_cost}={target_score}), ncs=({len(target_nounces[value_length])}), seed={seed}, n={nounce}", refresh=True)
                print(f"s={extra_seed_offset:02} ({target_score:2}=1-{seed_cost}), vl={value_length} ({encoded_value_counts[value_length]}/{max_items_by_length[value_length]}), offset=({extra_seed_offset}+{base_seed_offset}={seed}), p={position} ({len(split_items)}/{len(value_counts)}), tv={target_value} ('{data_item_value.to01()}': {value_counts[data_item_value.to01()]}) (s: {target_points}-{seed_cost}={target_score}), score={total_score}, n={nounce}, uc={unique_item_counts.first_items(3)}")
                progress.set_postfix({
                    #'tns': f"{len(target_nounces[value_length])}",
                    #'uns': f"{len(used_nounces[value_length])}",
                    #'unc': f"{used_nounce_counts.first_items(10)}",
                    'fnc': f"{free_nounce_counts.with_count_above(1).last_items(10)}",
                    #'ts':  f"{total_score}",
                    #'vc':  f"{value_counts.most_common(8)} ({len(value_counts)})",
                    #'bs_counts': f"{base_shift_counts.last_items(8)} ({len(base_shift_counts)})",
                    #'es_counts': f"{extra_shift_counts.first_items(8)} ({len(extra_shift_counts)})",
                    #'enc_counts': f"{encoded_value_counts.last_items(8)}",
                    #'split_items': f"{len(split_items)}",
                }, refresh=True)
                break
        if (value_found is True):
            seed              = 0
            extra_seed_offset = 0
            base_seed_offset  = 0
            cached_seed_values.clear()
            unique_item_counts.clear()
            
            for vl_id in range(0, len(value_lengths)):
                u_vl                     = value_lengths[vl_id]
                unique_item_counts[u_vl] = len(used_nounces[u_vl])
                if (vl_id > 0):
                    for prev_vl_id in range(0, vl_id):
                        prev_vl         = value_lengths[prev_vl_id]
                        prev_multiplier = 2**(u_vl-prev_vl)
                        extra_count     = len(used_nounces[prev_vl]) * prev_multiplier
                        unique_item_counts.update({ u_vl: extra_count })
            
            # expand search space during the scan
            if (used_nounce_counts[max_value_length] >= min_items_for_next_value_length):
                next_max_value_length = max_value_length + 1
                next_total_items      = all_limits[next_max_value_length]
                next_min_items        = all_limits[next_max_value_length-expand_distance]
                max_items_by_length[next_max_value_length] = next_total_items
                value_lengths.append(next_max_value_length)
                value_lengths          = sorted(value_lengths)
                next_nounce_range      = get_fib_value_range(value_length=(next_max_value_length - 1), encoder_type=nounce_encoder_type)
                next_nounce_range      = range(next_nounce_range[0], next_nounce_range[1])
                target_nounces[next_max_value_length] = [n for n in next_nounce_range]
                free_nounce_counts.update({ next_max_value_length: len(target_nounces[next_max_value_length]) })
                print(f"\n")
                print(f"SEARCH SPACE EXPANDED: new_length={next_max_value_length}, nounces: {min(next_nounce_range)}-{max(next_nounce_range)} ({next_total_items} items), next_item_limit={next_min_items}")
                print(f"value_lengths={value_lengths}")
                print(f"max_items_by_length:")
                pprint(max_items_by_length)
                print(f"free_nounce_counts:")
                pprint(free_nounce_counts)
                print(f"used_nounce_counts:")
                pprint(used_nounce_counts)
                print(f"extra_shift_counts={extra_shift_counts.last_items(40)}")
                print(f"")
                # update item limits
                max_value_length                = next_max_value_length
                min_items_for_next_value_length = next_min_items

        else:
            progress.set_description_str(f"vl={value_length}, o=(+{extra_seed_offset}, +{base_seed_offset}), tv={data_item_value.to01()}, ncs=({len(target_nounces[value_length])}), seed={seed}", refresh=True)
            extra_seed_offset += 1