if __name__ == "__main__":
    # поддержка импорта по относительному пути
    import os, sys
    if not 'workbookDir' in globals():
        workbookDir = os.getcwd()
    os.chdir(workbookDir)

# https://github.com/Textualize/rich
from rich import print, inspect
# https://rich.readthedocs.io/en/latest/pretty.html
from rich.pretty import pprint
# https://rich.readthedocs.io/en/latest/traceback.html
from rich.traceback import install
install(show_locals=True)
# https://rich.readthedocs.io/en/latest/logging.html
import logging
from rich.logging import RichHandler
logging.basicConfig(
    level="INFO",
    format="%(message)s",
    datefmt="[%X]",
    handlers=[RichHandler(rich_tracebacks=True, locals_max_length=16)]
)
log = logging.getLogger("rich")
# https://github.com/tqdm/tqdm
import tqdm

# https://realpython.com/python-data-classes/
# https://realpython.com/python-data-classes/#advanced-default-values
from dataclasses import dataclass, make_dataclass, field
# https://github.com/keon/algorithms
# https://github.com/keon/algorithms/blob/master/algorithms/compression/rle_compression.py
from algorithms.compression.rle_compression import encode_rle, decode_rle

# https://bitstring.readthedocs.io/en/latest/index.output_item_idhtml
from bitstring import Bits, BitArray, BitStream, ConstBitStream
# https://pythongeeks.org/ordereddict-in-python/
# https://www.geeksforgeeks.org/counters-in-python-set-1/
# https://docs.python.org/3/library/collections.html#counter-objects
# https://realpython.com/python-namedtuple/
# https://realpython.com/linked-lists-python/
from collections import OrderedDict, Counter, defaultdict, namedtuple, deque
# https://github.com/multiformats/unsigned-varint
# https://github.com/fmoo/python-varint
import varint
# https://docs.python.org/3/library/typing.html
# https://realpython.com/python-data-classes/#more-flexible-data-classes
from typing import List, Set, Dict, Tuple, Optional, Union, Deque
# https://www.geeksforgeeks.org/floor-ceil-function-python/?ref=lbp
import math
# https://docs.python.org/3/library/operator.html#module-operator
import operator
# https://stackoverflow.com/questions/55212014/python-count-copies-in-dictionary
import copy

# методы для работы с пространством хешей
from hash_space_utils import HashItemAddress, \
    value_at_position, \
    read_hash_item, \
    split_data, \
    count_split_values, \
    collect_split_positions

# соотношение длины адреса и длины данных (в байтах)
DEFAULT_TIER_VALUE_LENGTH = {
    1: 2,
    2: 3,
    3: 4,
}

def get_max_varint_value(byte_length: int) -> int:
    """
    Максимальное varint-значение при заданном числе байт
    """
    if (byte_length < 1) or (byte_length > 9):
        raise Exception(f"Incorrect byte length: {byte_length}")
    VALUE_BITS = 7
    return (2 ** (VALUE_BITS * byte_length) - 1)

def get_min_varint_value(byte_length: int) -> int:
    """
    Минимальное varint-значение при заданном числе байт
    """
    if (byte_length < 1):
        raise Exception(f"Incorrect byte length: {byte_length}")
    if (byte_length == 1):
        return 0
    prev_length    = byte_length - 1
    prev_max_value = get_max_varint_value(prev_length)
    # диапазон значений следующего уровня начинается сразу после окончания предыдущего, без пропусков
    return (prev_max_value + 1)

def get_max_varint_items(byte_length: int) -> int:
    """
    Максимальное количество элементов описываемое varint-значением фиксированной длины

    >>> get_max_varint_items(1)
    128
    >>> get_max_varint_items(2)
    16256
    >>> get_max_varint_items(3)
    2080768
    >>> get_max_varint_items(4)
    266338304
    >>> get_max_varint_items(5)
    34091302912
    """
    if (byte_length < 1):
        raise Exception(f"Incorrect byte length: {byte_length}")
    max_value = get_max_varint_value(byte_length)
    size      = max_value + 1
    if (byte_length == 1):
        return size
    prev_max_value = get_max_varint_value(byte_length - 1)
    prev_size      = prev_max_value + 1
    return (size - prev_size)

def get_varint_length(value: int) -> int:
    """
    Получить количество байт для varint-кодирования указанного значения
    """
    if (value < 0):
        raise Exception(f"Incorrect value: {value}")
    for byte_length in range(1, 9):
        max_value = get_max_varint_value(byte_length)
        if (max_value >= value):
            return byte_length
    raise Exception(f"Incorrect value: {value}")

def get_varint_position_tier(position: int) -> int:
    """
    Получить уровень позиции значения в пространстве хешей по ее значению 
    используя кодирование в формате varint 
    """
    position_size = get_varint_length(position)
    return position_size

def get_tier_position_bit_length(tier: int) -> int:
    """
    Получить длину номера позиции уровня в битах
    """
    return tier * 8

def get_position_tier(position: int) -> int:
    """
    Получить уровень позиции в хеш-пространстве по ее значению

    Parameters
    ----------
    position: int
        позиция значения - номер байта от начала (т. е. нулевого байта) хеш-пространства
    """
    # пока только varint-формат
    return get_varint_position_tier(position)

def get_max_tier_position(tier: int) -> int:
    """
    Получить конечную позицию уровня в хеш-пространстве
    """
    return get_max_varint_value(byte_length=tier)

def get_min_tier_position(tier: int) -> int:
    """
    Получить начальную позицию уровня в хеш-пространстве
    """
    return get_min_varint_value(byte_length=tier)

def get_tier_size(tier: int) -> int:
    """
    Получить размер уровня значений: размером считается количество позиций внутри переданного уровня
    (позиции предыдущих не прибавляются к размеру следующих - считаются только свои позиции)
    """
    return get_max_varint_items(byte_length=tier)

def get_max_seed_value(max_data_tier: int) -> int:
    """
    Получить максимальное значение seed-значения при поиске значений для текущего файла
    """
    # максимальное значение seed не должно превышать максимальное колиичество элементов в последнем уровне значений
    max_position = get_max_tier_position(max_data_tier)
    return max_position

def get_max_position_tier(total_data_items: int) -> int:
    """
    Определить максимальный уровень позиции значения используемый при поиске значений в пространствах хешей
    Уровень определяется как минимально необходимое количество байт для того чтобы представить в словаре
    все значения полученные при разбиении файла
    """
    # определяем минимально необходимое количество бит для того чтобы предстваить весь словарь фрагментов данных
    max_position  = total_data_items + 1
    position_tier = get_position_tier(position=max_position)
    return position_tier

def get_min_tier_count(tier_counts: Counter) -> int:
    if (len(list(tier_counts.keys())) == 0):
        return 1
    return min(list(tier_counts.keys()))

def get_max_tier_count(tier_counts: Counter) -> int:
    if (len(list(tier_counts.keys())) == 0):
        return get_min_tier_count(tier_counts)
    return max(list(tier_counts.keys()))

def get_tier_counts(value_counts: Counter, tier: int) -> Counter:
    """
    Получить количество использований каждого значения из словаря для выбранного уровня позиции в хеш-пространстве

    Parameters
    ----------
    value_counts: Counter
        словарь с количеством упоминаний каждого значения, из счетчиков которого вычли все значений прошлых уровней
    tier: int
        уровень позиции внутри пространства хешей
    """
    local_counts = value_counts.copy()
    # вычитаем элементы предыдущих уровней, начиная с первого но не доходя до текущего
    for prev_tier in range(1, tier):
        # определяем размер уровня
        prev_tier_size = get_tier_size(prev_tier)
        # берем из словаря наиболее популярные элементы: столько чтобы хватило заполнить уровень
        for i in range(0, prev_tier_size):
            if (len(local_counts) == 0):
                # значения словаря уже закончились а свободные позиции в слое еще остались - возвращаем все что набрали
                break
            max_item_count = max(list(local_counts.keys()))
            # удаляем элементы которые были использованы предыдущими уровнями
            local_counts.update({max_item_count: -1})
            if (local_counts[max_item_count] == 0):
                del local_counts[max_item_count]
    
    # создаем словарь, который будет хранить количество упоминаний каждого значения в слое
    tier_counts = Counter()
    # определяем количество элементов, которые нужно разместить на этом уровне
    tier_size   = get_tier_size(tier)
    for i in range(0, tier_size):
        if (len(local_counts) == 0):
            # значения словаря уже закончились а свободные позиции в слое еще остались - возвращаем все что набрали
            break
        max_item_count = max(list(local_counts.keys()))
        # добавляем использование значения указанной длины в список слоя
        tier_counts.update({max_item_count: 1})
        # вычитаем его же из общего количества
        local_counts.update({max_item_count: -1})
        if (local_counts[max_item_count] == 0):
            del local_counts[max_item_count]
    return tier_counts

#################################################
# Основной класс для работы с уровнями значений #
#################################################

@dataclass()
class Tier:
    """
    Метаданные одного уровня значений

    >>> tier1 = Tier(1)
    >>> tier1.tier_done
    False
    >>> tier1.skip_positions
    set()
    >>> tier1.seeds
    set()
    >>> tier1.tier_values
    set()
    >>> tier1.position_values
    {}
    >>> tier1.value_positions
    {}
    >>> tier1.value_seeds
    {}
    >>> tier1.position_seeds
    {}
    """

    # номер (глубина) уровня: чем она больше, тем больше в нем значений
    tier             : int
    # найдены ли все значения уровня
    tier_done        : bool
    # длина позиции (в битах)
    position_length  : int
    # длина значения (в битах)
    value_length     : int
    # доступные позиции
    positions        : set
    # максимально возможное количество заполненных позиций в этом уровне
    max_positions    : int
    # использованные (найденные) позиции
    skip_positions   : set
    # список позиций которые осталось заполнить (на используются пока не найдено 50% позиций)
    scan_positions   : set
    # режим сканирования последних позиций: ускоряет поиск последних элементов
    long_tail_scan   : bool
    # seed-значения (номера хеш-пространств) в которых содержатся значения этого уровня
    seeds            : set
    # значения этого уровня (поисковый индекс)
    tier_values      : set
    # значения, найденные для этого уровня (соопоставленные с позициями)
    position_values  : dict
    # обратный индекс для тех же значений
    value_positions  : dict
    # seed-значения слоя, распределенные по значениям (ключи - uint-значения, значения - seed-числа)
    value_seeds      : dict
    # seed-значения слоя, распределенные по позициям
    position_seeds   : dict

    def __init__(self, tier: int):
        self.tier             = tier
        self.tier_done        = False
        self.positions        = self.create_tier_positions()
        self.max_positions    = get_tier_size(tier)
        self.position_length  = self.create_position_length()
        self.value_length     = self.create_value_length()
        self.skip_positions   = set()
        self.scan_positions   = set(),
        self.long_tail_scan   = False,
        self.seeds            = set()
        self.tier_values      = set()
        self.position_values  = dict()
        self.value_positions  = dict()
        self.value_seeds      = dict()
        self.position_seeds   = dict()
        #self.tier_counts     = get_tier_counts(number_of_options_with_same_count, tier),
        #self.max_values      = tier_sum,
        #self.min_count       = get_min_tier_count(tier_counts),
        #self.max_count       = get_max_tier_count(tier_counts),
    
    def create_tier_positions(self, tier: int = None) -> range:
        """
        Получить список позиций для расположения значений одного уровня. Начало и окончание соответствуют границам уровня,
        нумерация полностью совпадает с номерами позиций внутри уровня пространства хешей

        >>> tier1 = Tier(1)
        >>> tier1.positions
        range(0, 128)
        >>> tier2 = Tier(2)
        >>> tier2.positions
        range(128, 16384)
        >>> tier3 = Tier(3)
        >>> tier3.positions
        range(16384, 2097152)
        """
        if (tier is None):
            tier = self.tier
        tier_size      = self.max_positions
        start_position = get_min_tier_position(tier)
        end_position   = start_position + tier_size
        return range(start_position, end_position)
    
    def create_position_length(self):
        """
        >>> tier1 = Tier(1)
        >>> tier1.position_length
        8
        """
        return (self.tier * 8)
    
    def create_value_length(self):
        """
        >>> tier1 = Tier(1)
        >>> tier1.value_length
        16
        >>> tier2 = Tier(2)
        >>> tier2.value_length
        24
        >>> tier3 = Tier(3)
        >>> tier3.value_length
        32
        """
        return (DEFAULT_TIER_VALUE_LENGTH[self.tier] * 8)

#########################################
# Методы для работы с уровнями значений #
#########################################

def init_search_tiers(start_tier: int=1, end_tier: int=3) -> Dict[int, Tier]:
    """
    Создать объекты уровней значений в пространстве хешей

    >>> tiers = init_search_tiers(1, 3)
    >>> len(tiers) == 3
    True
    >>> type(tiers[1])
    <class 'hash_item_tiers.Tier'>
    >>> tiers[1].tier == 1
    True
    >>> tiers[2].tier == 2
    True
    >>> tiers[3].tier == 3
    True
    """
    tiers = dict()
    for tier in range(start_tier, (end_tier + 1)):
        tiers[tier] = Tier(tier)
    return tiers

def has_tier_value(tier_data: Tier, value: int) -> bool:
    """
    Определить есть ли указанное значение внутри ранее найденных значений уровня
    """
    return (value in tier_data.tier_values)

def has_tiers_value(tiers: dict, tier: int, value: int) -> bool:
    """
    Определить есть ли указанное значение внутри ранее найденных значений уровня
    """
    return has_tier_value(tiers[tier], value)

def get_next_available_tier(tiers: dict) -> int:
    """
    Получить первый доступный (не заполненный) уровень для следующего элемента
    """
    tier_keys = list(tiers.keys())
    for tier in tier_keys:
        tier_data = tiers[tier]
        if (tier_data['tier_done'] == False):
            return tier

def get_new_item_tier(tiers: dict) -> int:
    return get_next_available_tier(tiers)

def get_tier_position_length(tiers: dict, tier: int) -> int:
    return tiers[tier]['position_length']

def get_tier_value_length(tiers: dict, tier: int) -> int:
    return tiers[tier]['value_length']

def get_new_item_position_length(tiers: dict) -> int:
    tier = get_new_item_tier(tiers)
    return get_tier_position_length(tiers, tier)

def get_new_item_value_length(tiers: dict) -> int:
    tier = get_new_item_tier(tiers)
    return get_tier_value_length(tiers, tier)

def peek_new_value_from_data(data: ConstBitStream, tiers: dict, tier: int = None) -> Bits:
    if (tier is None):
        tier = get_new_item_tier(tiers)
    value_length = get_tier_value_length(tiers, tier)
    return Bits(data.peek(value_length))

def read_new_value_from_data(data: ConstBitStream, tiers: dict, tier: int = None) -> Bits:
    if (tier is None):
        tier = get_new_item_tier(tiers)
    value_length = get_tier_value_length(tiers, tier)
    value_fmt    = f"bin:{value_length}"
    value_data   = data.read(value_fmt)
    return Bits(auto=f"0b{value_data}")