from rich import print
# https://rich.readthedocs.io/en/latest/pretty.html
from rich.pretty import pprint
from rich.text import Text
from rich.progress import track,\
    BarColumn, Progress, Task, TaskID, TextColumn, TimeElapsedColumn, TimeRemainingColumn,\
    MofNCompleteColumn, RenderableColumn, SpinnerColumn, TransferSpeedColumn, FileSizeColumn, ProgressColumn
from rich.layout import Layout
from rich.columns import Columns
from rich.text import Text
from rich.console import Console
from custom_rich import CustomTaskProgressColumn as TaskProgressColumn

from bitarray import bitarray, frozenbitarray
from bitarray.util import int2ba, ba2int
from custom_counter import CustomCounter as Counter
from collections import defaultdict, ChainMap, deque, namedtuple
from sortedcontainers import SortedSet
from typing import List, Set, Dict, Tuple, Optional, Union, Iterable
from dataclasses import dataclass, field
from copy import copy, deepcopy
import json

from custom_nested_range import ChainedRange, NestedRange

DEFAULT_ENDIAN    = 'big'
DEFAULT_STEP_BITS = 8

def create_value_ranges(max_ranges: int,  min_index_bits: int):
    value_ranges   = list()
    max_index_bits = min_index_bits + max_ranges
    offset         = 0
    for value_length in range(min_index_bits, max_index_bits):
        range_id    = len(value_ranges)
        start_value = offset
        end_value   = offset + 2**value_length
        value_range = range(start_value, end_value)
        value_ranges.append(value_range)
        offset += len(value_range)
        print(f"range_id={range_id}: ({value_range.start}, {value_range.stop})")
    return value_ranges

@dataclass()
class CbsValue:
    id            : Tuple[int, int] = field(init=False, default=None)
    length        : int             = field()
    value         : int             = field()
    first_byte_id : int             = field()
    last_byte_id  : int             = field()
    step          : int             = field(default=DEFAULT_STEP_BITS)
    count         : int             = field(init=False, default=None, repr=False)
    value_bits    : bitarray        = field(init=False, default=None)
    final_byte_id : int             = field(init=False, default=None)
    byte_ids      : List[int]       = field(init=False, default=None)
    
    def __init__(self, length: int, value: int, first_byte_id: int, last_byte_id: int, step: int=DEFAULT_STEP_BITS):
        if (value.bit_length() > length):
            raise Exception(f"length={length} is too short for value={value}")
        self.id            = (length, value)
        self.length        = length
        self.value         = value
        self.first_byte_id = first_byte_id
        self.last_byte_id  = last_byte_id
        self.step          = step
        self.final_byte_id = None
        self.count         = None
        self.byte_ids      = list()
        self.value_bits    = frozenbitarray(int2ba(self.value, length=self.length, endian=DEFAULT_ENDIAN, signed=False))
        if (self.first_byte_id == self.last_byte_id):
            self.final_byte_id = self.last_byte_id
        for byte_id in range(self.first_byte_id, self.first_byte_id + (self.length // self.step)):
            self.byte_ids.append(byte_id)

#@dataclass()
#class CbsItemData:
#    id       : int  = field()
#    is_new   : bool = field()
#    length   : int  = field()
#    value    : int  = field()
#    position : int  = field()

CbsItemData = namedtuple('CbsItemData', ['id', 'is_new', 'length', 'value', 'position'])

@dataclass()
class CbsItem:
    id            : int             = field()
    is_new        : bool            = field()
    length        : int             = field()
    value         : int             = field()
    byte_id       : int             = field()
    last_byte_id  : int             = field(default=None)
    step          : int             = field(default=DEFAULT_STEP_BITS)
    cbs_value_id  : Tuple[int, int] = field(init=False, default=None)
    byte_ids      : List[int]       = field(init=False, default=None)

    def __init__(self, item_id: int, is_new: bool, length: int, value: int, byte_id: int, last_byte_id: int=None, step: int=DEFAULT_STEP_BITS):
        if (value.bit_length() > length):
            raise Exception(f"length={length} is too short for value={value}")
        self.id           = item_id
        self.is_new       = is_new
        self.length       = length
        self.value        = value
        self.byte_id      = byte_id
        self.last_byte_id = last_byte_id
        self.step         = step
        self.cbs_value_id = (length, value)
        self.byte_ids     = list()
        for byte_id in range(self.byte_id, self.byte_id + (self.length // self.step)):
            self.byte_ids.append(byte_id)
    
    def get_byte_length(self) -> int:
        return (self.length // self.step)
    
    def get_suffix_values(self, suffix_length: int) -> range:
        if (suffix_length <= self.length):
            raise Exception(f"No suffix values with length={suffix_length} for item={self}")
        length_diff = suffix_length - self.length
        multiplier  = 2 ** length_diff
        start_value = self.value * multiplier
        end_value   = start_value + multiplier
        return range(start_value, end_value)
    
    def to_json(self) -> CbsItemData:
        return CbsItemData(
            id       = self.id,
            is_new   = self.is_new,
            length   = self.length,
            value    = self.value,
            position = self.byte_id,
        )


@dataclass()
class ValueSequence:
    item_id : int = field()
    value   : int = field()
    count   : int = field(default=1)

    def __repr__(self):
        return f"VS(p={self.item_id}, v={self.value}, c={self.count})"

    def __init__(self, item_id: int, value: int, count: int = 1):
        self.item_id = item_id
        self.value   = value
        self.count   = count
    
    def advance(self, advance: int = 1) -> int:
        self.count += advance
        return self.count

@dataclass()
class ContentBasedSplit:
    data                          : bitarray                  = field(repr=False)
    max_value_slots               : Counter                   = field()
    max_new_value_counts          : Counter                   = field(default=None)
    step                          : int                       = field(default=DEFAULT_STEP_BITS)
    # allow dynamic index reassigning (this require to store counter with number of usages with each new value)
    allow_index_reuse             : bool                      = field(default=True)
    # allow to add new values shorter that current global value length
    allow_custom_new_value_length : bool                      = field(default=False)
    byte_id                       : int                       = field(init=False, default=0)
    item_id                       : int                       = field(init=False, default=0)
    data_length                   : int                       = field(init=False, default=0)
    open_slot_counts              : Counter                   = field(init=False, default=None)
    removed_suffix_counts         : Counter                   = field(init=False, default=None)
    reused_index_counts           : Counter                   = field(init=False, default=None)
    value_counts_by_length        : Dict[int, Counter]        = field(init=False, default=None)
    new_values_by_length          : Dict[int, List[int]]      = field(init=False, default=None)
    target_values_by_length       : Dict[int, SortedSet[int]] = field(init=False, default=None)
    target_byte_ids_by_length     : Dict[int, SortedSet[int]] = field(init=False, default=None)
    min_target_byte_ids_length    : Dict[int, int]            = field(init=False, default=None)
    next_target_byte_id_length    : int                       = field(init=False, default=None)
    last_byte_ids_by_length       : Dict[int, Dict[int, int]] = field(init=False, default=None)
    #last_value_byte_ids_by_length : Dict[int, Dict[int, int]] = field(init=False, default=None)
    current_length                : int                       = field(init=False, default=None)
    active_lengths                : List[int]                 = field(init=False, default=None)
    #current_values                : List[CbsValue]            = field(init=False, default=None)
    data_items                    : List[CbsItem]             = field(init=False, default=None)
    tail_length                   : int                       = field(init=False, default=0)
    tail                          : bitarray                  = field(init=False, default=None)
    progress                      : Progress                  = field(init=False, default=None, repr=False)
    data_task_id                  : TaskID                    = field(init=False, default=None, repr=False)
    length_task_ids               : Dict[int, TaskID]         = field(init=False, default=None, repr=False)

    def __init__(self, data: bitarray, max_value_slots: Counter, max_new_value_counts: Counter=None, step: int=DEFAULT_STEP_BITS,
                 allow_index_reuse: bool=True, allow_custom_new_value_length: bool=False):
        self.data                          = data
        self.step                          = step
        self.allow_index_reuse             = allow_index_reuse
        self.allow_custom_new_value_length = allow_custom_new_value_length
        self.byte_id                       = 0
        self.item_id                       = 0
        self.data_length                   = len(self.data)
        self.max_value_slots               = max_value_slots.copy()
        self.open_slot_counts              = max_value_slots.copy()
        self.removed_suffix_counts         = Counter()
        self.reused_index_counts           = Counter()
        self.max_new_value_counts          = max_new_value_counts
        self.value_counts_by_length        = defaultdict(Counter)
        self.new_values_by_length          = defaultdict(list)
        self.target_values_by_length       = defaultdict(SortedSet)
        self.target_byte_ids_by_length     = defaultdict(SortedSet)
        self.last_byte_ids_by_length       = defaultdict(dict)
        #self.last_value_byte_ids_by_length = defaultdict(dict)
        self.current_length                = self.get_all_value_lengths()[0]
        self.active_lengths                = [self.current_length]
        #self.current_values                = list()
        self.data_items                    = list()
        self.tail_length                   = 0
        self.tail                          = bitarray('')
        self.progress                      = None
        self.data_task_id                  = None
        self.length_task_ids               = dict()
        self.min_target_byte_ids_length    = dict()
        self.next_target_byte_id_length    = self.current_length

        for value_length in self.get_all_value_lengths():
            self.min_target_byte_ids_length[value_length] = None
        
    def get_last_byte_id_for_length(self, length: int) -> int:
        return (len(self.data) // self.step) - (length // self.step)
    
    def get_reverse_byte_id_range_for_length(self, length: int) -> range:
        start_byte_id = self.get_last_byte_id_for_length(length=length)
        end_byte_id   = self.byte_id - 1
        return range(start_byte_id, end_byte_id, -1)

    def get_last_byte_id_for_value(self, length: int, value: int) -> int:
        return 2**32
        
        #start_bit   = self.byte_id * self.step
        #target_bits = int2ba(value, length=length, endian=DEFAULT_ENDIAN, signed=False)
        #all_lengths = list()
        #for l in self.get_all_value_lengths():
        #    if (self.open_slot_counts[l] == self.max_value_slots[l]) and (len(self.last_byte_ids_by_length[l]) == 0):
        #        continue
        #    all_lengths.append(l)
        #
        #for bit_id in self.data.itersearch(target_bits, start_bit, right=1):
        #    if (self.step > 1):
        #        if ((bit_id % self.step) > 0):
        #            continue
        #        byte_id = bit_id // self.step
        #    else:
        #        byte_id = bit_id
        #    skip_byte_id = False
        #    for vl in all_lengths:
        #        #if (self.open_slot_counts[vl] == self.max_value_slots[vl]):
        #        #    break
        #        #if (vl > length):
        #        #    continue
        #        if (byte_id in self.target_byte_ids_by_length[vl]):
        #            skip_byte_id = True
        #            break
        #    if (skip_byte_id is True):
        #        continue
        #    return byte_id
        #raise Exception(f"self.byte_id={self.byte_id}: length={length}, value={value} - last byte id not found")
    
    def has_existing_value(self, length: int, value: int) -> bool:
        #if (self.value_counts_by_length[length][value] > 0):
        if (value in self.target_values_by_length[length]):
            return True
        return False
    
    def can_add_data_item(self, item: CbsItem) -> bool:
        if (item.is_new is False):
            return True
        if (self.open_slot_counts[item.length] > 0):
            return True
        #return False
        #if (self.open_slot_counts[item.length] > 0):
        #if (item.is_new is True) and (self.open_slot_counts[item.length] > 0):
        #if (item.is_new is True) and (self.open_slot_counts[self.current_length] > 0):
        if (self.open_slot_counts[self.current_length] > 0):
            return True
        else:
            return False
    
    def update_current_length(self, item: CbsItem):
        if (self.can_add_data_item(item=item)):
            return
        new_length = None
        #for next_length in self.get_all_value_lengths():
        for next_length in self.get_open_value_lengths():
            if (next_length not in self.active_lengths):
                new_length = next_length
                break
        if (new_length is None):
            raise Exception(f"Unable to update self.current_length={self.current_length} - next_length is not available: active_lengths={self.active_lengths}, max_value_slots={self.max_value_slots}")
        
        if (self.current_length not in self.length_task_ids):
            length_task_description = self.get_length_task_description(length=self.current_length)
            current_length_task_id  = self.progress.add_task(length_task_description, total=None)
            self.length_task_ids[self.current_length] = current_length_task_id

        self.current_length = new_length
        self.active_lengths.append(new_length)
        #self.current_values = list()
        
        self.progress.log(f"New current_length={self.current_length} ({self.open_slot_counts[self.current_length]} slots), active_lengths={self.active_lengths}")

        data_task_description = self.get_data_task_description()
        self.progress.update(task_id=self.data_task_id, description=data_task_description, refresh=True)
    
    def get_length_task_description(self, length: int) -> str:
        ac             = self.value_counts_by_length[length].aggregated_counts()
        ac_sum         = 0
        ac_list        = list()
        ac_length      = len(ac)
        if (self.allow_custom_new_value_length is True):
            rs_count = self.removed_suffix_counts[length]
        else:
            rs_count = self.reused_index_counts[length]
        vc_length      = len(self.value_counts_by_length[length])
        tv_length      = len(self.target_values_by_length[length])
        ac_first_items = ac.first_items()
        first_items    = ac_first_items[0:4]
        last_items     = []
        if (len(ac_first_items) > 4):
            max_last_items = min(4, len(ac_first_items)-4)
            last_items     = ac_first_items[ac_length-max_last_items:ac_length]
        for value_count, total_values in ac.items():
            ac_sum += value_count * total_values
        for value_count, total_values in first_items:
            ac_list.append(f"{value_count}:{total_values}")
        skip_length = len(ac_first_items) - (len(first_items) + len(last_items))
        if (skip_length > 0):
            ac_list.append(f"({skip_length})")
        for value_count, total_values in last_items:
            ac_list.append(f"{value_count}:{total_values}")
        ac_items = '{' + ",".join(ac_list) + '}'
        
        return f"{length:2} {vc_length}/{ac_length}/{rs_count},{tv_length},s={ac_sum}:{ac_items}"
    
    def get_data_task_description(self) -> str:
        ac             = self.value_counts_by_length[self.current_length].aggregated_counts()
        ac_sum         = 0
        ac_list        = list()
        ac_length      = len(ac)
        if (self.allow_custom_new_value_length is True):
            rs_count = self.removed_suffix_counts[self.current_length]
        else:
            rs_count = self.reused_index_counts[self.current_length]
        #vc_length      = len(self.value_counts_by_length[self.current_length])
        #tv_length      = len(self.target_values_by_length[self.current_length])
        ac_first_items = ac.first_items()
        first_items    = ac_first_items[0:4]
        last_items     = []
        if (len(ac_first_items) > 4):
            max_last_items = min(4, len(ac_first_items)-4)
            last_items     = ac_first_items[ac_length-max_last_items:ac_length]
        for value_count, total_values in ac.items():
            ac_sum += value_count * total_values
        for value_count, total_values in first_items:
            ac_list.append(f"{value_count}:{total_values}")
        skip_length = len(ac_first_items) - (len(first_items) + len(last_items))
        if (skip_length > 0):
            ac_list.append(f"({skip_length})")
        for value_count, total_values in last_items:
            ac_list.append(f"{value_count}:{total_values}")
        ac_items = '{' + ",".join(ac_list) + '}'

        return f"{self.current_length:2} {self.open_slot_counts[self.current_length]}/{self.max_value_slots[self.current_length]}/{rs_count},{len(self.target_values_by_length[self.current_length])},s={ac_sum}:{ac_items}"
    
    def make_split(self):
        data_length   = len(self.data)
        end_of_data   = False
        self.progress = Progress(
            TextColumn("[progress.description]{task.description}"),
            MofNCompleteColumn(),
            #BarColumn(),
            TaskProgressColumn(text_format="[progress.percentage]{task.percentage:>3.2f}%", show_speed=True),
            TimeRemainingColumn(elapsed_when_finished=True),
            TransferSpeedColumn(),
            #auto_refresh=False,
            refresh_per_second=1,
            #speed_estimate_period=120,
            expand=True,
            #disable=True,
            console=Console(tab_size=2, log_time=False, log_path=False),
            #redirect_stdout=False,
            #redirect_stderr=False,
        )
        data_task_description = self.get_data_task_description()
        self.data_task_id     = self.progress.add_task(data_task_description, total=(self.data_length // self.step))
        self.progress.start()

        scan_values = dict()
        while True:
            data_item = None
            scan_values.clear()
            for active_length in self.active_lengths:
                start_bit  = self.byte_id * self.step
                end_bit    = start_bit + active_length
                scan_value = None
                is_new     = True
                if (end_bit > data_length):
                    self.tail_length = data_length - start_bit
                    self.tail        = self.data[start_bit:data_length]
                    if (self.tail_length > 0):
                        self.progress.advance(task_id=self.data_task_id, advance=(self.tail_length // self.step))
                    end_of_data = True
                    break
                scan_value                 = ba2int(self.data[start_bit:end_bit], signed=False)
                scan_values[active_length] = scan_value
                if (self.has_existing_value(length=active_length, value=scan_value)):
                    is_new = False
                    break
            if (end_of_data is True):
                self.progress.log(f"END OF DATA")
                break
            # existing items can be added without additional conditions
            if (is_new is False):
                # generating new data item
                self.add_data_item(item=CbsItem(
                    item_id = self.item_id, 
                    is_new  = is_new, 
                    length  = active_length, 
                    value   = scan_value, 
                    byte_id = self.byte_id,
                    step    = self.step
                ))
                continue

            data_item_length = active_length
            data_item_value  = scan_values[data_item_length]

            if (self.allow_custom_new_value_length is True):
                # always try to fill dict for a lowest tier
                min_open_value_length = self.get_min_open_value_length()
                if (min_open_value_length < active_length):
                    data_item_length = min_open_value_length
                    data_item_value  = scan_values[data_item_length]
            #else:
            #    data_item_length = active_length
            #    data_item_value  = scan_values[data_item_length]
            #    # if lowest tier dict value is available - take only items that can be repeated
            #    #last_byte_id = self.get_last_byte_id_for_value(length=min_open_value_length, value=scan_values[min_open_value_length])
            #    #if (last_byte_id > self.byte_id):
            #    #    data_item_length = min_open_value_length
            #    #    data_item_value  = scan_values[data_item_length]
            #    #else:
            #    #    last_byte_id     = None
            #    #    data_item_length = active_length
            #    #    data_item_value  = scan_values[data_item_length]
            #else:
            #    data_item_length = active_length
            #    data_item_value  = scan_values[data_item_length]
            
            # generating new data item
            data_item = CbsItem(
                item_id      = self.item_id, 
                is_new       = is_new, 
                length       = data_item_length, 
                value        = data_item_value, 
                byte_id      = self.byte_id,
                last_byte_id = None, #last_byte_id,
                step         = self.step,
            )
            # if no open slots available for new items - move to next value length
            #if (self.can_add_data_item(item=CbsItem(item_id=self.item_id, is_new=is_new, length=active_length, value=scan_value, byte_id=self.byte_id, step=self.step)) is False):
            if (self.can_add_data_item(item=data_item) is False):
                self.progress.log(f"(byte_id={self.byte_id}, item_id={self.item_id}): cannot add active_item={data_item} - updating current length...")
                self.update_current_length(item=data_item)
                continue
            # add new item and move forward to next data bytes
            self.add_data_item(item=data_item)
        
        # finish progress display
        data_task_description = self.get_data_task_description()
        self.progress.update(task_id=self.data_task_id, description=data_task_description, refresh=True)
        # update counters for all tiers before end
        for lgth, lt_id in self.length_task_ids.items():
            length_task_description = self.get_length_task_description(length=lgth)
            self.progress.update(task_id=lt_id, description=length_task_description, refresh=True)
            self.progress.update(task_id=lt_id, refresh=True)
        self.progress.stop()

        return self.data_items

    def clear_suffix_data_items(self, item: CbsItem):
        if (item.length == self.current_length):
            return
        suffix_lengths = list()
        for suffix_length in self.get_all_value_lengths():
            if (suffix_length <= item.length):
                continue
            if (len(self.target_values_by_length[suffix_length]) == 0):
                continue
            suffix_lengths.append(suffix_length)
        if (len(suffix_lengths) == 0):
            return
        for suffix_length in suffix_lengths:
            suffix_values = item.get_suffix_values(suffix_length=suffix_length)
            for suffix_value in suffix_values:
                if (suffix_value in self.target_values_by_length[suffix_length]):
                    for last_byte_id, suffix_byte_id_value in self.last_byte_ids_by_length[suffix_length].items():
                        if (suffix_byte_id_value == suffix_value):
                            #print(f"Removing suffix item l={suffix_length}, v={suffix_value}")
                            self.remove_target_byte_id(target_length=suffix_length, byte_id=last_byte_id)
                            self.removed_suffix_counts.update({ suffix_length : 1 })
                            #del self.last_byte_ids_by_length[suffix_length][last_byte_id]
                            # update last byte id
                            #self.get_last_byte_id_for_value(length=suffix_length, value=suffix_value)
                            #last_byte_id = self.get_last_byte_id_for_value(length=suffix_length, value=suffix_value)
                            #self.last_byte_ids_by_length[suffix_length][last_byte_id] = suffix_value

    def remove_target_byte_id(self, target_length: int, byte_id: int):
        self.target_byte_ids_by_length[target_length].remove(byte_id)
        self.target_values_by_length[target_length].remove(self.last_byte_ids_by_length[target_length][byte_id])
        self.open_slot_counts.update({ target_length: 1 })
        
        if (len(self.target_byte_ids_by_length[target_length]) > 0):
            self.min_target_byte_ids_length[target_length] = min(self.target_byte_ids_by_length[target_length])
        else:
            self.min_target_byte_ids_length[target_length] = self.data_length
        
        for tb_id_length in self.active_lengths:
            tb_id_min_value = self.min_target_byte_ids_length[tb_id_length]
            if (tb_id_min_value is None):
                continue
            if (tb_id_min_value < self.min_target_byte_ids_length[self.next_target_byte_id_length]):
                self.next_target_byte_id_length = tb_id_length

    def add_data_item(self, item: CbsItem):
        # registering new value
        if (item.is_new is True):
            last_byte_id = item.last_byte_id
            # clear suffix items if any
            #if (item.length < self.current_length):
            #    self.clear_suffix_data_items(item=item)
            #if (item.last_byte_id is None):
            #    last_byte_id      = self.get_last_byte_id_for_value(length=item.length, value=item.value)
            #    item.last_byte_id = last_byte_id
            cbs_value = CbsValue(length=item.length, value=item.value, first_byte_id=self.byte_id, last_byte_id=last_byte_id, step=item.step)
            self.new_values_by_length[item.length].append(item.value)
            self.last_byte_ids_by_length[item.length][last_byte_id] = item.value
            #self.last_value_byte_ids_by_length[item.length][item.value] = last_byte_id
            # update target values only if new value has more 1 occurrences 
            if (cbs_value.first_byte_id != cbs_value.last_byte_id):
                #self.current_values.append(cbs_value)
                self.target_values_by_length[item.length].add(item.value)
                self.target_byte_ids_by_length[item.length].add(last_byte_id)
                self.min_target_byte_ids_length[item.length] = min(self.target_byte_ids_by_length[item.length])
                self.open_slot_counts.update({ item.length: -1 })

                if (self.allow_custom_new_value_length is False) and (self.allow_index_reuse is True):
                    min_open_length = self.get_min_open_value_length()
                    if (min_open_length < item.length): #  and (self.open_slot_counts[min_open_length] < self.max_value_slots[min_open_length])
                        # take open slot from previous value length if any
                        self.open_slot_counts.update({ min_open_length: -1 })
                        self.open_slot_counts.update({ item.length: 1 })
                        self.reused_index_counts.update({ min_open_length: 1 })
                
                for tb_id_length in self.active_lengths:
                    tb_id_min_value = self.min_target_byte_ids_length[tb_id_length]
                    if (tb_id_min_value is None):
                        continue
                    if (tb_id_min_value < self.min_target_byte_ids_length[self.next_target_byte_id_length]):
                        self.next_target_byte_id_length = tb_id_length
                #print(f"(+)Added new cbs_value={cbs_value}, open_slots={self.open_slot_counts[item.length]}"
                # update progress
                if (self.item_id % 10) == 0:
                    data_task_description = self.get_data_task_description()
                    self.progress.update(task_id=self.data_task_id, description=data_task_description)
        # removing value from active list
        #for byte_id in item.byte_ids:
            #for target_length in self.active_lengths:
            #target_byte_id = min(self.target_byte_ids_by_length[target_length])
        #    target_length = self.next_target_byte_id_length
        #    if (byte_id == self.min_target_byte_ids_length[target_length]):
        #        self.remove_target_byte_id(target_length=target_length, byte_id=byte_id)
                #self.target_byte_ids_by_length[target_length].remove(byte_id)
                #self.target_values_by_length[target_length].remove(self.last_byte_ids_by_length[target_length][byte_id])
                #self.open_slot_counts.update({ target_length: 1 })
                #
                #if (len(self.target_byte_ids_by_length[target_length]) > 0):
                #    self.min_target_byte_ids_length[target_length] = min(self.target_byte_ids_by_length[target_length])
                #else:
                #    self.min_target_byte_ids_length[target_length] = self.data_length
                #
                #for tb_id_length in self.active_lengths:
                #    tb_id_min_value = self.min_target_byte_ids_length[tb_id_length]
                #    if (tb_id_min_value is None):
                #        continue
                #    if (tb_id_min_value < self.min_target_byte_ids_length[self.next_target_byte_id_length]):
                #        self.next_target_byte_id_length = tb_id_length
                #if (target_length == self.current_length):
                    # remove item from current values
                    # update progress
                    #if (self.item_id % 10) == 0:
                    #data_task_description = self.get_data_task_description()
                    #self.progress.update(task_id=self.data_task_id, description=data_task_description, refresh=False)
                    #for current_value_id in range(0, len(self.current_values)):
                    #    current_value = self.current_values[current_value_id]
                    #    if (current_value.last_byte_id == byte_id):
                    #        #self.current_values.pop(current_value_id)
                    #        self.current_values[current_value_id].final_byte_id = self.byte_id
                    #        #print(f"(-)Removed current_value={self.current_values[current_value_id]}, open_slots={self.open_slot_counts[item.length]}")
                    #        del self.current_values[current_value_id]
                    #        break
        # add new item to split
        self.data_items.append(item)
        # update counters
        self.value_counts_by_length[item.length].update({ item.value : 1 })
        self.item_id  = len(self.data_items)
        self.byte_id += item.get_byte_length()
        self.progress.advance(task_id=self.data_task_id, advance=item.get_byte_length())
        # update previous length counters
        if ((item.length < self.current_length) and (self.item_id % 5) == 0):
            length_task_description = self.get_length_task_description(length=item.length)
            if (item.length in self.length_task_ids):
                length_task_id = self.length_task_ids[item.length]
                self.progress.update(task_id=length_task_id, description=length_task_description)

    def get_all_value_lengths(self) -> List[int]:
        return sorted(list(self.max_value_slots.keys()))

    def get_open_value_lengths(self) -> List[int]:
        value_lengths = list()
        for value_length, max_slots in self.max_value_slots.items():
            if (self.open_slot_counts[value_length] == 0):
                continue
            if (len(self.value_counts_by_length[value_length]) >= self.max_new_value_counts[value_length]):
                continue
            value_lengths.append(value_length)
        return value_lengths
    
    def get_min_open_value_length(self) -> int | None:
        #return self.current_length
        open_value_lengths = self.get_open_value_lengths()
        if (len(open_value_lengths) == 0):
            return None
        return min(open_value_lengths)
    
    def get_final_length_counts(self) -> Dict[str, Counter]:
        value_counts = Counter()
        item_counts  = Counter()
        for value_length in self.get_all_value_lengths():
            item_counts.update({ 'total' : sum(self.value_counts_by_length[value_length].values()) })
            item_counts.update({ value_length: sum(self.value_counts_by_length[value_length].values()) })
            value_counts.update({ 'total' : len(self.value_counts_by_length[value_length]) })
            value_counts.update({ value_length : len(self.value_counts_by_length[value_length]) })
        return {
            'item_counts'  : item_counts,
            'value_counts' : value_counts,
        }
    
    def get_new_value_counts_sequence(self, length: int) -> List[int]:
        return [self.value_counts_by_length[length][value] for value in self.new_values_by_length[length]]
    
    def get_is_new_item_sequence(self) -> List[bool]:
        return [item.is_new for item in self.data_items]
    
    def create_aggregated_new_value_counts_sequence(self, length: int) -> List[Tuple[int, int]]:
        aggregated_sequence   = list()
        value_counts_sequence = self.get_new_value_counts_sequence(length=length)
        if (len(value_counts_sequence) == 0):
            return aggregated_sequence
        if (len(value_counts_sequence) == 1):
            return [(1, value_counts_sequence[0])]
        # init first sequence item
        count_sequence = ValueSequence(item_id=0, value=value_counts_sequence[0])
        for new_value_id in range(1, len(value_counts_sequence)):
            new_value_count  = value_counts_sequence[new_value_id]
            prev_value_count = count_sequence.value
            if (prev_value_count == new_value_count):
                count_sequence.advance()
            else:
                aggregated_sequence.append(copy(count_sequence))
                count_sequence = ValueSequence(item_id=len(aggregated_sequence), value=new_value_count)
        # add last sequence item
        aggregated_sequence.append(count_sequence)
        return aggregated_sequence
    
    def get_new_value_sequence(self, length: int) -> List[int]:
        return self.new_values_by_length[length]
    
    def to_json(self) -> str:
        return {
            'open_slot_counts'              : self.max_value_slots,
            'max_new_value_counts'          : self.max_new_value_counts,
            'allow_index_reuse'             : self.allow_index_reuse,
            'allow_custom_new_value_length' : self.allow_custom_new_value_length,
            'active_lengths'                : self.active_lengths,
            'new_values'                    : self.new_values_by_length,
            'value_counts'                  : self.value_counts_by_length,
            'final_counts'                  : self.get_final_length_counts(),
            'data_items'                    : [data_item.to_json() for data_item in self.data_items],
            'tail_length'                   : self.tail_length,
            'tail'                          : self.tail.to01(),
        };

    def dump_to_file(self):
        slot_names = "_".join([f"l{slot_length}c{slot_count.bit_length()}" for slot_length, slot_count in self.max_value_slots.items()][0:4])
        data_file  = open(f'./data/cbs_data_l{self.data_length}_{slot_names}.json', 'w')
        json.dump(self.to_json(), data_file, indent=None, default=tuple)
    
    def load_from_file(self) -> Dict:
        slot_names = "_".join([f"l{slot_length}c{slot_count.bit_length()}" for slot_length, slot_count in self.max_value_slots.items()][0:4])
        data_file  = open(f'./data/cbs_data_l{self.data_length}_{slot_names}.json', 'r')
        return json.load(data_file)



# TODO: collect new item sequences by value count
# TODO: group ValueSequence by value count, apply additional aggregation on sequence lengths
# TODO: create generator for each value_count (yields new values in correct order)