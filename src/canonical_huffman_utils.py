# https://stackoverflow.com/questions/38340808/recursive-typing-in-python-3-5
from __future__ import annotations
from rich import print
from rich.pretty import pprint
from rich.live import Live
from rich.panel import Panel
from rich.table import Table, Column
from rich.progress import track,\
    BarColumn, Progress, Task, TaskID, TextColumn, TimeElapsedColumn, TimeRemainingColumn,\
    MofNCompleteColumn, RenderableColumn, SpinnerColumn, TransferSpeedColumn, FileSizeColumn, ProgressColumn
from rich.layout import Layout
from rich.columns import Columns
from rich.text import Text
from custom_rich import CustomTaskProgressColumn as TaskProgressColumn
from custom_counter import CustomCounter as Counter
#from tqdm import tqdm
from bitarray.util import ba2int, int2ba, canonical_huffman, huffman_code
from bitarray import bitarray, frozenbitarray
from collections import defaultdict, namedtuple
from dataclasses import dataclass, field
from copy import deepcopy, copy
from typing import List, Set, Dict, Tuple, Optional, Union, Iterable
from functools import lru_cache
import xxhash
#import math
from bitarrayset.binaryarrayset import BinaryArraySet
from enum import Enum
from sortedcontainers import SortedSet, SortedDict, SortedList

from mongoengine import Document, QuerySet, LongField, IntField, \
    BinaryField, StringField, ListField, SortedListField, BooleanField

from fib_encoder import fib_encode_position #, get_fib_lengths, get_fib_value_range, get_fib_position_ranges

VARINT_PREFIX_LENGTH = 4
DEFAULT_ENDIAN       = 'little'
from custom_varint import encode_varint_number

class ItemType(Enum):
    STEP   = "step"
    SEED   = "seed"
    NOUNCE = "nounce"
    RAW    = "raw"

@dataclass
class EncodedItem:
    id           : int
    data_item_id : int
    is_new       : bool
    type         : ItemType
    code         : bitarray
    length       : int
    value        : int

@dataclass
class DataItem:
    id                : int
    seed              : int
    nounce            : int
    value             : int
    seed_length       : int
    nounce_length     : int
    value_length      : int
    score             : int
    seed_step         : int            = field(default=None)
    nounce_step       : int            = field(default=None)
    seed_codes        : List[bitarray] = field(default_factory=list)
    nounce_codes      : List[bitarray] = field(default_factory=list)
    #step_codes        : List[bitarray] = field(default_factory=list)
    value_item        : bitarray       = field(default=None)
    #seed_step_codes   : bitarray = field(default_factory=list)
    #nounce_step_codes : bitarray = field(default_factory=list)

@dataclass
class HuffmanPrefixTree:
    data                   : frozenbitarray                  = field(repr=False)
    default_score          : int                             = field(default=1)
    data_size              : int                             = field(default=0, init=False)
    data_position          : int                             = field(default=0, init=False)
    data_item_id           : int                             = field(default=0, init=False)
    encoded_item_id        : int                             = field(default=0, init=False)
    # seed->nounces
    # value_length->values
    steps                  : Set[int]            = field(default_factory=SortedSet, init=False)
    seeds                  : Set[int]            = field(default_factory=SortedSet, init=False)
    nounces                : Dict[int, Set[int]] = field(default_factory=SortedDict, init=False)
    values                 : Dict[int, Set[int]] = field(default_factory=SortedDict, init=False)
    
    seed_codes             : Dict[int, bitarray]            = field(default_factory=SortedDict, init=False)
    step_codes             : Dict[int, bitarray]            = field(default_factory=SortedDict, init=False)
    nounce_codes           : Dict[int, Dict[int, bitarray]] = field(default_factory=SortedDict, init=False)
    
    step_counts            : Counter                   = field(default_factory=Counter, init=False)
    seed_counts            : Counter                   = field(default_factory=Counter, init=False)
    nounce_counts          : Dict[int, Counter]        = field(default_factory=lambda: defaultdict(Counter), init=False)
    value_counts           : Dict[int, Counter]        = field(default_factory=lambda: defaultdict(Counter), init=False)
    item_counts            : Counter                   = field(default_factory=Counter, init=False)
    
    step_length_counts     : Counter                   = field(default_factory=Counter, init=False)
    seed_length_counts     : Counter                   = field(default_factory=Counter, init=False)
    nounce_length_counts   : Dict[int, Counter]        = field(default_factory=lambda: defaultdict(Counter), init=False)
    value_length_counts    : Counter                   = field(default_factory=Counter, init=False)

    step_lengths           : Dict[int, int]                  = field(default_factory=SortedDict, init=False)
    seed_lengths           : Dict[int, int]                  = field(default_factory=SortedDict, init=False)
    nounce_lengths         : Dict[int, Dict[int, int]] = field(default_factory=SortedDict, init=False)
    nounce_values          : Dict[int, Dict[int, int]] = field(default_factory=SortedDict, init=False)
    value_lengths          : Dict[int, Dict[int, int]] = field(default_factory=SortedDict, init=False)
    value_seeds            : Dict[int, Dict[int, int]]       = field(default_factory=lambda: defaultdict(dict), init=False)
    value_nounces          : Dict[int, Dict[int, int]]       = field(default_factory=lambda: defaultdict(dict), init=False)

    active_step_lengths    : Set[int]                        = field(default_factory=SortedSet, init=False)
    active_seed_lengths    : Set[int]                        = field(default_factory=SortedSet, init=False)
    active_nounce_lengths  : Dict[int, Dict[int, Set[int]]]  = field(default_factory=SortedDict, init=False)
    active_value_lengths   : Set[int]                        = field(default_factory=SortedSet, init=False)
    
    length_steps           : Dict[int, SortedSet[int]]      = field(default_factory=lambda : defaultdict(SortedSet), init=False)
    length_seeds           : Dict[int, SortedSet[int]]      = field(default_factory=lambda : defaultdict(SortedSet), init=False)
    length_values          : Dict[int, SortedSet[int]]      = field(default_factory=lambda : defaultdict(SortedSet), init=False)
    # Запоминаем координаты последних значений для того чтобы добавлять новые путём прибавления значений к существующим
    # последним сидом или позицией считается поседний элемент последовательности диапазона от 0
    # То есть для последовательности (0, 1, 5) последним элементом будет считаться 1, для (0, 3) будет 0, а для (0, 1, 2) будет 2
    # Для каждого сида последний элемент хранится и обновляется отдельно
    start_step             : int                                   = field(default=0, init=False)
    start_seed             : int                                   = field(default=0, init=False)
    start_nounce           : Dict[int, int]                        = field(default_factory=SortedDict, init=False)
    total_score            : int                                   = field(default=0, init=False)
    data_items             : List[DataItem]                        = field(default_factory=list, init=False)
    encoded_items          : List[EncodedItem]                     = field(default_factory=list, init=False)
    data_item_seed_codes   : Dict[int, List[bitarray]]             = field(default_factory=lambda: defaultdict(list), init=False)
    data_item_nounce_codes : Dict[int, List[bitarray]]             = field(default_factory=lambda: defaultdict(list), init=False)
    # next item options
    target_values          : Dict[int, int]                        = field(default_factory=SortedDict, init=False)
    target_items           : Dict[int, bitarray]                   = field(default_factory=SortedDict, init=False)
    # current seed+nounce results value lengths: seed->nounce->value_length
    target_value_lengths   : Dict[int, Dict[int, int]]             = field(default_factory=lambda : defaultdict(dict), init=False)
    prev_values            : Dict[int, Set[int]]                   = field(default_factory=lambda : defaultdict(set), init=False)
    value_tree             : Dict[int, Set[int]]                   = field(default_factory=lambda : defaultdict(set), init=False)
    # console ui/progress data
    progress               : Progress                              = field(default=None, init=False, repr=False)
    data_task_id           : TaskID                                = field(default=None, init=False, repr=False)
    speed_task_id          : TaskID                                = field(default=None, init=False, repr=False)
    scan_task_id           : TaskID                                = field(default=None, init=False, repr=False)
    item_task_id           : TaskID                                = field(default=None, init=False, repr=False)


    def __post_init__(self):
        self.data_size = len(self.data)

        self.init_step_code_tree()
        self.register_step(1)
        #self.register_step(2)

        #self.init_seed_code_tree()
        #self.register_seed(1)
        #self.register_seed(2)

        #self.register_nounce(1, 3)
        #self.register_nounce(1, 2)
        #self.register_nounce(1, 1)
        #self.update_seed_code_tree()
        #self.update_seed_nounce_code_tree(seed=1)

        self.update_value_code_tree()
        self.active_value_lengths.add(3)

        #print(f"position={self.data_position}...{self.data_size}")

        self.progress = Progress(
            MofNCompleteColumn(),
            BarColumn(),
            TaskProgressColumn(show_speed=True),
            TimeElapsedColumn(),
            TimeRemainingColumn(),
            TextColumn("[progress.description]{task.description}"),
            #transient=False, 
            auto_refresh=True,
            #refresh_per_second=2, 
            #speed_estimate_period=30,
        )
        self.data_task_id  = self.progress.add_task("Total progress", total=self.data_size, start=True)
        self.speed_task_id = self.progress.add_task("Encoding speed", total=None, start=True)
        self.item_task_id  = self.progress.add_task("Item search", total=None, start=True)
        self.scan_task_id  = self.progress.add_task("Scan speed", total=None, start=True)
        self.progress.start()
    
    def has_existing_step(self, step: int) -> bool:
        if (step in self.steps):
            return True
        return False

    def has_existing_seed(self, seed: int) -> bool:
        if (seed in self.seeds):
            return True
        return False
    
    def has_existing_nounce(self, seed: int, nounce: int) -> bool:
        if (self.has_existing_seed(seed=seed) is False):
            #raise Exception(f"No ex seed={seed} (nounce={nounce})")
            return False
        if (self.nounce_counts[seed][nounce] > 0):
            return True
        return False
    
    def has_existing_value(self, value: int, value_length: int) -> bool:
        if (value_length not in list(self.values.keys())):
            #raise Exception(f"Incorrect value_length={value_length} (value={value})")
            return False
        if (value not in self.values[value_length]):
            return False
        return True
    
    def update_step_code_tree(self) -> SortedDict[int, bitarray]:
        new_codes = SortedDict(huffman_code(self.step_counts, endian=DEFAULT_ENDIAN).items())
        for step_id, new_code in new_codes.items():
            new_length = len(new_code)
            if (step_id in self.step_lengths.keys()):
                old_length = self.step_lengths[step_id]
            else:
                old_length = None
            if (step_id in self.step_codes.keys()):
                old_code = self.step_codes[step_id]
            else:
                old_code = None
            if (old_code != new_code):
                #print(f"step_id={step_id}, code=({old_code}->{new_code}), length=({old_length}->{new_length})")
                self.step_codes[step_id] = new_code
            if (old_length != new_length):
                self.step_lengths[step_id] = new_length
                if (self.step_length_counts[old_length] > 0):
                    self.step_length_counts.update({ old_length: -1 })
                self.step_length_counts.update({ new_length: 1 })
                #print(f"step_id={step_id}, length_counts=({self.step_length_counts[old_length]}->{self.step_length_counts[new_length]})")
            if (old_length in self.length_steps.keys()) and (step_id in self.length_steps[old_length]):
                self.length_steps[old_length].remove(step_id)
                if (len(self.length_steps[old_length]) == 0):
                    del self.length_steps[old_length]
            if (new_length is not None):
                if (new_length not in list(self.length_steps.keys())):
                    self.length_steps[new_length] = SortedSet()
                if (step_id not in self.length_steps[new_length]):
                    self.length_steps[new_length].add(step_id)
        self.step_codes.clear()
        self.step_codes = new_codes
    
    def update_seed_code_tree(self) -> SortedDict[int, bitarray]:
        new_codes = SortedDict(huffman_code(self.seed_counts, endian='little').items())
        for seed_id, new_code in new_codes.items():
            new_length = len(new_code)
            if (seed_id in self.seed_lengths):
                old_length = self.seed_lengths[seed_id]
            else:
                old_length = None
            if (seed_id in self.seed_codes):
                old_code = self.seed_codes[seed_id]
            else:
                old_code = None
            
            if (old_code != new_code):
                #print(f"seed_id={seed_id}, code=({old_code}->{new_code}), length=({old_length}->{new_length})")
                self.seed_codes[seed_id] = new_code
            if (old_length != new_length):
                self.seed_lengths[seed_id] = new_length
                if (self.seed_length_counts[old_length] > 0):
                    self.seed_length_counts.update({ old_length: -1 })
                #self.seed_length_counts.update({ new_length: 1 })
                #print(f"seed_id={seed_id}, length_counts=({self.seed_length_counts[old_length]}->{self.seed_length_counts[new_length]})")
            if (old_length in self.length_seeds) and (seed_id in self.length_seeds[old_length]):
                self.length_seeds[old_length].remove(seed_id)
                if (len(self.length_seeds[old_length]) == 0):
                    del self.length_seeds[old_length]
            if (new_length is not None):
                if (new_length not in list(self.length_seeds.keys())):
                    self.length_seeds[new_length] = SortedSet()
                if (seed_id not in self.length_seeds[new_length]):
                    self.length_seeds[new_length].add(seed_id)
            
            if (seed_id not in list(self.nounce_length_counts.keys())):
                self.nounce_length_counts[seed_id] = Counter()
            if (seed_id not in list(self.nounce_counts.keys())):
                self.nounce_counts[seed_id] = Counter()
            if (seed_id not in list(self.nounce_lengths.keys())):
                self.nounce_lengths[seed_id] = SortedDict()
            if (seed_id not in list(self.nounce_values.keys())):
                self.nounce_values[seed_id] = SortedDict()
            if (seed_id not in list(self.start_nounce.keys())):
                self.start_nounce[seed_id] = 0
            if (seed_id not in list(self.active_nounce_lengths.keys())):
                self.active_nounce_lengths[seed_id] = SortedDict()
            if (seed_id not in list(self.nounces.keys())):
                self.nounces[seed_id] = SortedSet()
            if (seed_id not in list(self.nounce_codes.keys())):
                self.nounce_codes[seed_id] = SortedDict()
        
        self.seed_codes.clear()
        self.seed_codes = new_codes
    
    def update_nounce_code_trees(self, seed: int):
        for seed in self.seeds:
            self.update_seed_nounce_code_tree(seed=seed)

    def update_seed_nounce_code_tree(self, seed: int) -> SortedDict[int, bitarray]:
        if (self.has_existing_seed(seed=seed) is False):
            raise Exception(f"seed={seed} not registered, cannot update nounce tree")
        # require to do it once for every new seed
        if (self.nounce_counts[seed][0] == 0):
            self.nounce_counts[seed].update({ 0: 1 })
        
        new_codes = SortedDict(huffman_code(self.nounce_counts[seed], endian='little').items())
        for nounce_id, new_code in new_codes.items():
            new_length = len(new_code)
            if (seed in list(self.nounce_lengths.keys())) and (nounce_id in self.nounce_lengths[seed]):
                old_length = self.nounce_lengths[seed][nounce_id]
            else:
                old_length = None
            if (seed in list(self.nounce_codes.keys())) and (nounce_id in self.nounce_codes[seed]):
                old_code = self.nounce_codes[seed][nounce_id]
            else:
                old_code = None
            if (old_code != new_code):
                #print(f"nounce_id={nounce_id}, code=({old_code}->{new_code}), length=({old_length}->{new_length})")
                if (seed not in list(self.nounce_codes.keys())):
                    self.nounce_codes[seed] = SortedDict()
                self.nounce_codes[seed][nounce_id] = new_code
            if (old_length != new_length):
                if (seed not in list(self.nounce_lengths.keys())):
                    self.nounce_lengths[seed] = SortedDict()
                self.nounce_lengths[seed][nounce_id] = new_length
                if (self.nounce_length_counts[seed][old_length] > 0):
                    self.nounce_length_counts[seed].update({ old_length: -1 })
                #self.nounce_length_counts[seed].update({ new_length: 1 })
                #print(f"nounce_id={nounce_id}, length_counts=({self.nounce_length_counts[seed][old_length]}->{self.nounce_length_counts[seed][new_length]})")
            if (seed in list(self.active_nounce_lengths.keys())) and (old_length in list(self.active_nounce_lengths[seed].keys())) and (nounce_id in self.active_nounce_lengths[seed][old_length]):
                self.active_nounce_lengths[seed][old_length].remove(nounce_id)
                if (len(self.active_nounce_lengths[seed][old_length]) == 0):
                    del self.active_nounce_lengths[seed][old_length]
            if (new_length is not None):
                if (seed not in list(self.active_nounce_lengths.keys())):
                    self.active_nounce_lengths[seed] = SortedDict()
                if (new_length not in list(self.active_nounce_lengths[seed].keys())):
                    self.active_nounce_lengths[seed][new_length] = SortedSet()
                if (nounce_id not in self.active_nounce_lengths[seed][new_length]):
                    self.active_nounce_lengths[seed][new_length].add(nounce_id)
                
                if (seed not in list(self.target_value_lengths.keys())):
                    self.target_value_lengths[seed] = SortedDict()
                self.target_value_lengths[seed][nounce_id] = new_length + new_length + self.default_score
        
        self.nounce_codes[seed].clear()
        self.nounce_codes[seed] = new_codes
    
    def get_nounce_value(self, seed: int, nounce: bytes, value_length: int) -> int:
        digest = xxhash.xxh32_intdigest(input=nounce, seed=seed)
        return digest % (2**value_length)
    
    def update_value_code_tree(self):
        updates         = SortedDict()
        prev_values     = SortedDict()
        #updated_lengths = defaultdict(lambda: defaultdict(SortedSet))
        
        for seed_id, seed_code in self.seed_codes.items():
            if (seed_id == 0):
                continue
            seed_length     = len(seed_code)
            #old_seed_length = self.seed_lengths[seed_id]
            for nounce_id, nounce_code in self.nounce_codes[seed_id].items():
                if (nounce_id == 0):
                    continue
                nounce_length     = len(nounce_code)
                #old_nounce_length = self.nounce_lengths[seed_id][nounce_id]
                value_length     = seed_length + nounce_length + self.default_score
                old_value_length = self.target_value_lengths[seed_id][nounce_id]
                if (value_length not in prev_values.keys()):
                    prev_values[value_length] = SortedSet()
                if (value_length not in updates.keys()):
                    updates[value_length] = SortedDict()
                if (seed_id not in updates[value_length].keys()):
                    updates[value_length][seed_id] = SortedSet()
                if (old_value_length != value_length):
                    #updated_lengths[value_length][seed_id].add(nounce_id)
                    updates[value_length][seed_id].add(nounce_id)
                    self.value_length_counts.update({ old_value_length: -1 })
                    self.value_length_counts.update({ value_length: 1 })
                    #del self.value_seeds[old_value_length][value]
                    #del self.value_nounces[old_value_length][value]
                    if (self.value_length_counts[old_value_length] == 0):
                        del self.value_length_counts[old_value_length]
                    self.target_value_lengths[seed_id][nounce_id] = value_length
                #if (old_seed_length != seed_length) or (old_nounce_length != nounce_length):
                #    updates[value_length][seed_id].add(nounce_id)
                updates[value_length][seed_id].add(nounce_id)
        
        #pprint(updated_lengths, max_length=16)

        for value_length, seeds in updates.items():
            for seed, nounces in seeds.items():
                if (seed == 0):
                    continue
                for nounce in nounces:
                    if (nounce == 0):
                        continue
                    seed_offset    = 0
                    input_nounce   = nounce.to_bytes(length=4, byteorder='little', signed=False)
                    #prefix_lengths = list(prev_values.keys())
                    while (True):
                        input_seed   = seed + seed_offset
                        value        = self.get_nounce_value(seed=input_seed, nounce=input_nounce, value_length=value_length)
                        prefix_found = False
                        #for prefix_length in prefix_lengths:
                        for prefix_length in prev_values.keys():
                            if (prefix_length > value_length):
                                break
                            prefix_values = prev_values[prefix_length]
                            prefix_value  = value % (2**prefix_length)
                            if (prefix_value in prefix_values):
                                prefix_found = True
                                break
                        if (prefix_found is False):
                            break
                        seed_offset += 1
                    prev_values[value_length].add(value)
                    self.value_seeds[value_length][value]   = seed
                    self.value_nounces[value_length][value] = nounce
                    self.target_value_lengths[seed][nounce] = value_length
                    self.active_value_lengths.add(value_length)
                    #if (value_length not in list(self.values.keys())):
                    if (value_length not in self.values.keys()):
                        self.values[value_length] = SortedSet()
                    #if (value_length not in list(self.value_counts.keys())):
                    if (value_length not in self.value_counts.keys()):
                        self.value_counts[value_length] = Counter()
                    #print(f"l={value_length}, v={value}, o={seed_offset}")
        
        self.values.clear()
        self.values = deepcopy(prev_values)
        #if (len(self.value_length_counts.keys()) > 0):
        #    self.active_value_lengths.clear()
        #    self.active_value_lengths = SortedSet(self.value_length_counts.keys())
    
    def rebuild_value_tree(self, min_seed: int=1):
        prev_seeds  = SortedSet()
        for seed_id in self.seeds:
            if (seed_id == 0):
                continue
            if (seed_id < min_seed):
                prev_seeds.add(seed_id)
                continue
            
            self.prev_values[seed_id].clear()
            for nounce_id in self.nounces[seed_id]:
                if (nounce_id == 0):
                    continue
                seed_code     = self.seed_codes[seed_id]
                nounce_code   = self.nounce_codes[seed_id][nounce_id]
                seed_length   = len(seed_code)
                nounce_length = len(nounce_code)
                value_length  = seed_length + nounce_length + self.default_score
                seed_offset   = 0
                input_nounce  = nounce_id.to_bytes(length=4, byteorder='little', signed=False)
                
                while (True):
                    input_seed       = seed_id + seed_offset
                    value            = self.get_nounce_value(seed=input_seed, nounce=input_nounce, value_length=value_length)
                    prev_value_found = False
                    for prev_seed_id in prev_seeds:
                        #prev_seed_lengths = sorted(list(self.prev_values[prev_seed_id].keys()))
                        prev_seed_lengths = self.prev_values[prev_seed_id].keys()
                        for prev_seed_length in prev_seed_lengths:
                            if (prev_seed_length > value_length):
                                prev_seed_values = self.prev_values[prev_seed_id][prev_seed_length]
                                value_prefix     = value % (2**prev_seed_length)
                                if (value_prefix in prev_seed_values):
                                    prev_value_found = True
                                    break
                        if (prev_value_found is True):
                            break
                    if (prev_value_found is False):
                        break
                    seed_offset += 1
                self.prev_values[seed_id][value_length].add(value)
    
    def update_active_step_lengths(self):
        self.active_step_lengths.clear()
        for step_id, step_code in self.step_codes.items():
            step_length = len(step_code)
            self.active_step_lengths.add(step_length)
    
    def update_active_seed_lengths(self):
        self.active_seed_lengths.clear()
        for seed_id, seed_code in self.seed_codes.items():
            seed_length = len(seed_code)
            self.active_seed_lengths.add(seed_length)
    
    def update_start_step(self):
        new_start_step = self.start_step
        for step_id in range(0, (max(self.steps) + 1)):
            if (step_id not in self.steps):
                break
            new_start_step = step_id
        if (new_start_step != self.start_step):
            #print(f"new_start_step={new_start_step} (old={self.start_step})")
            self.start_step = new_start_step
    
    def update_start_seed(self):
        new_start_seed = self.start_seed
        for seed_id in range(0, (max(self.seeds) + 1)):
            if (seed_id not in self.seeds):
                break
            new_start_seed = seed_id
        if (new_start_seed != self.start_seed):
            #print(f"new_start_seed={new_start_seed} (old={self.start_seed})")
            self.start_seed = new_start_seed
    
    def update_start_nounce(self, seed: int):
        if (self.has_existing_seed(seed=seed) is False):
            raise Exception(f"seed={seed} not registered, cannot update start_nounce")
        
        new_start_nounce = self.start_nounce[seed]
        for nounce_id in range(0, (max(self.nounces[seed]) + 1)):
            if (nounce_id not in self.nounces[seed]):
                break
            new_start_nounce = nounce_id
        if (new_start_nounce != self.start_nounce[seed]):
            #print(f"new_start_nounce={new_start_nounce} (old={self.start_nounce[seed]})")
            self.start_nounce[seed] = new_start_nounce
    
    def get_distance_to_new_step(self, new_step: int) -> int:
        distance = new_step - self.start_step
        return distance
    
    def get_distance_to_new_seed(self, new_seed: int) -> int:
        distance = new_seed - self.start_seed
        return distance
    
    def get_distance_to_new_nounce(self, seed: int, new_nounce: int) -> int:
        if (self.has_existing_seed(seed=seed) is False):
            raise Exception(f"seed={seed} not registered, cannot get distance for new new_nounce={new_nounce}")
        if (seed not in list(self.start_nounce.keys())):
            self.start_nounce[seed] = 0
        distance = new_nounce - self.start_nounce[seed]
        return distance
    
    def get_code_for_new_step(self, new_step: int) -> bitarray:
        if (new_step is self.step_codes):
            return self.step_codes[new_step]
        else:
            # return fib_encode_position(position=new_step, encoder_type='C1')
            return self.encode_raw_number(number=new_step)
    
    def get_code_for_new_seed(self, new_seed: int) -> bitarray:
        if (new_seed is self.seed_codes):
            return self.seed_codes[new_seed]
        seed_step = self.get_distance_to_new_seed(new_seed=new_seed)
        if (seed_step in self.steps):
            return self.step_codes[seed_step]
        else:
            self.register_step(step=seed_step, seed=new_seed)
            return self.step_codes[seed_step]
    
    def get_code_for_new_nounce(self, seed: int, new_nounce: int) -> bitarray:
        if (self.has_existing_seed(seed=seed) is False):
            raise Exception(f"seed={seed} not registered, cannot get code for new new_nounce={new_nounce}")
        if (new_nounce is self.nounce_codes[seed]):
            return self.nounce_codes[seed]
        nounce_step = self.get_distance_to_new_nounce(seed=seed, new_nounce=new_nounce)
        if (nounce_step in self.steps):
            return self.step_codes[nounce_step]
        else:
            self.register_step(step=nounce_step, seed=seed, nounce=new_nounce)
            return self.step_codes[nounce_step]

    def add_encoded_item(self, item: EncodedItem) -> EncodedItem:
        if (item.id is None):
            item.id = self.encoded_item_id
        if (item.data_item_id is None):
            item.data_item_id = self.data_item_id
        self.encoded_items.append(item)
        self.encoded_item_id = len(self.encoded_items)
        return item
    
    def add_data_item(self, item: DataItem) -> DataItem:
        if (item.id is None):
            item.id = self.data_item_id
        if (item.value_item is None):
            item.value_item = frozenbitarray(int2ba(item.value, length=item.value_length, endian='little', signed=False))
        
        self.data_items.append(item)
        self.data_item_id = len(self.data_items)

        return item
    
    def append_data_item_code(self, encoded_item: EncodedItem, seed: int=None, nounce: int=None):
        if (seed is not None):
            if (nounce is None):
                if (encoded_item.data_item_id not in self.data_item_seed_codes):
                    self.data_item_seed_codes[encoded_item.data_item_id] = list()
                self.data_item_seed_codes[encoded_item.data_item_id].append(encoded_item.code)
            else:
                if (encoded_item.data_item_id not in self.data_item_nounce_codes):
                    self.data_item_nounce_codes[encoded_item.data_item_id] = list()
                self.data_item_nounce_codes[encoded_item.data_item_id].append(encoded_item.code)
    
    def register_step(self, step: int, seed: int=None, nounce: int=None) -> EncodedItem:
        if (self.has_existing_step(step=step) is True) or (step == 0):
            raise Exception(f"step={step} already registered: code={self.step_codes[step]}")
        
        new_step_code = self.step_codes[0]
        new_step_item = EncodedItem(
            id           = self.encoded_item_id,
            data_item_id = self.data_item_id,
            is_new       = False,
            type         = ItemType.STEP,
            code         = new_step_code.to01(),
            length       = len(new_step_code),
            value        = 0,
        )
        self.add_encoded_item(new_step_item)
        self.append_data_item_code(encoded_item=new_step_item, seed=seed, nounce=nounce)
        self.step_counts.update({ 0 : 1 })
        self.update_step_code_tree()
        self.total_score -= new_step_item.length
        
        #step_code = fib_encode_position(position=step, encoder_type='C1')
        step_code = self.encode_raw_number(number=step)
        step_item = EncodedItem(
            id           = self.encoded_item_id,
            data_item_id = self.data_item_id,
            is_new       = True,
            type         = ItemType.STEP,
            code         = step_code.to01(),
            length       = len(step_code),
            value        = step,
        )
        self.add_encoded_item(step_item)
        self.append_data_item_code(encoded_item=step_item, seed=seed, nounce=nounce)
        # все коды шагов смещения считаем равновероятными и не перестраиваем их в зависимости от частоты использования:
        # эксперименты дали плохой результат в этом случае
        if (self.step_counts[step] == 0):
            self.step_counts.update({ step : 1 })
        self.update_step_code_tree()
        self.total_score -= len(step_code)

        print(f"\nNEW_STEP: {step} (-{len(step_code)}): {step_code}")
        
        self.steps.add(step)
        self.update_active_step_lengths()
        self.update_start_step()
        return step_item
    
    def register_seed(self, seed: int) -> EncodedItem:
        if (self.has_existing_seed(seed=seed) is True):
            raise Exception(f"seed={seed} already registered: code={self.seed_codes[seed]}")
        #if (seed == 0):
        if (self.seed_counts[0] == 0):
            self.seed_counts.update({ 0 : 1 })
            if (0 not in list(self.seed_codes.keys())):
                inital_codes       = SortedDict(huffman_code(self.seed_counts, endian='little').items())
                self.seed_codes[0] = inital_codes[0]
        
        new_seed_code = self.seed_codes[0]
        new_seed_item = EncodedItem(
            id           = self.encoded_item_id,
            data_item_id = self.data_item_id,
            is_new       = False,
            type         = ItemType.SEED,
            code         = new_seed_code.to01(),
            length       = len(new_seed_code),
            value        = 0,
        )
        self.add_encoded_item(new_seed_item)
        self.append_data_item_code(encoded_item=new_seed_item, seed=seed)
        self.seed_counts.update({ 0 : 1 })
        self.update_seed_code_tree()
        
        seed_code = self.get_code_for_new_seed(new_seed=seed)
        seed_item = EncodedItem(
            id           = self.encoded_item_id,
            data_item_id = self.data_item_id,
            is_new       = True,
            type         = ItemType.SEED,
            code         = seed_code.to01(),
            length       = len(seed_code),
            value        = seed,
        )
        self.add_encoded_item(seed_item)
        self.append_data_item_code(encoded_item=seed_item, seed=seed)
        #self.seed_counts.update({ seed : 1 })
        self.update_seed_code_tree()
        
        self.seeds.add(seed)
        self.update_active_seed_lengths()
        self.update_start_seed()

        if (seed > 0):
            self.update_seed_nounce_code_tree(seed=seed)
        return seed_item
    
    def register_nounce(self, seed: int, nounce: int) -> EncodedItem:
        if (self.has_existing_seed(seed=seed) is False):
            self.register_seed(seed=seed)
        if (self.has_existing_nounce(seed=seed, nounce=nounce) is True):
            raise Exception(f"nounce={nounce} already registered: code={self.nounce_codes[seed][nounce]}")
        if (seed == 0):
            raise Exception(f"nounce={nounce} cannot be registered for seed=0")
        if (nounce == 0):
            raise Exception(f"nounce={nounce} cannot be registered")
        
        new_nounce_code = self.nounce_codes[seed][0]
        new_nounce_item = EncodedItem(
            id           = self.encoded_item_id,
            data_item_id = self.data_item_id,
            is_new       = False,
            type         = ItemType.NOUNCE,
            code         = new_nounce_code.to01(),
            length       = len(new_nounce_code),
            value        = 0,
        )
        self.add_encoded_item(new_nounce_item)
        self.append_data_item_code(encoded_item=new_nounce_item, seed=seed, nounce=nounce)
        self.nounce_counts[seed].update({ 0 : 1 })
        self.update_seed_nounce_code_tree(seed=seed)
        
        nounce_code = self.get_code_for_new_nounce(seed=seed, new_nounce=nounce)
        nounce_item = EncodedItem(
            id           = self.encoded_item_id,
            data_item_id = self.data_item_id,
            is_new       = True,
            type         = ItemType.NOUNCE,
            code         = nounce_code.to01(),
            length       = len(nounce_code),
            value        = seed,
        )
        self.add_encoded_item(nounce_item)
        self.append_data_item_code(encoded_item=nounce_item, seed=seed, nounce=nounce)
        #self.nounce_counts[seed].update({ nounce : 1 })
        self.update_seed_nounce_code_tree(seed=seed)
        
        if (seed not in list(self.nounces.keys())):
            self.nounces[seed] = SortedSet()    
        self.nounces[seed].add(nounce)
        self.update_start_nounce(seed=seed)

        return nounce_item
    
    def register_value(self, seed: int, nounce: int, value_length: int, value: int):
        if (seed == 0):
            raise Exception(f"value={value} ({value_length}) cannot be registered for seed=0")
        if (nounce == 0):
            raise Exception(f"value={value} ({value_length}) cannot be registered for nounce=0")
        if (self.has_existing_seed(seed=seed) is False):
            self.register_seed(seed=seed)
        if (self.has_existing_nounce(seed=seed, nounce=nounce) is False):
            self.register_nounce(seed=seed, nounce=nounce)
        if (value_length not in self.values):
            self.values[value_length] = SortedSet()
        if (self.has_existing_value(value=value, value_length=value_length) is True):
            raise Exception(f"value={value} ({value_length}) already registered for seed={seed}, nounce={nounce}")
        self.values[value_length].add(value)
        self.value_seeds[value_length][value]   = seed
        self.value_nounces[value_length][value] = nounce
        self.target_value_lengths[seed][nounce] = value_length
        self.active_value_lengths.add(value_length)
    
    def register_data_item(self, data_item: DataItem) -> DataItem:
        if (self.has_existing_seed(seed=data_item.seed)):
            self.data_item_seed_codes[self.data_item_id] = data_item.seed_codes # [self.seed_codes[data_item.seed]]
        else:
            self.register_seed(seed=data_item.seed)
        
        if (self.has_existing_nounce(seed=data_item.seed, nounce=data_item.nounce)):
            self.data_item_nounce_codes[self.data_item_id] = data_item.nounce_codes #[self.nounce_codes[data_item.seed][data_item.nounce]]
        else:
            self.register_nounce(seed=data_item.seed, nounce=data_item.nounce)
        
        if (self.has_existing_value(value=data_item.value, value_length=data_item.value_length) is False):
            self.register_value(seed=data_item.seed, nounce=data_item.nounce, value_length=data_item.value_length, value=data_item.value)

        return self.add_data_item(item=data_item)

    def init_step_code_tree(self):
        if (len(self.step_counts) > 0):
            raise Exception("Step code tree is not empty")
        self.step_counts.update({ 0: 1 })
        self.update_step_code_tree()
        step_code = self.step_codes[0] #fib_encode_position(position=0, encoder_type='C1') #
        new_step_item = EncodedItem(
            id           = self.encoded_item_id,
            data_item_id = self.data_item_id,
            is_new       = True,
            type         = ItemType.STEP,
            code         = step_code.to01(),
            length       = len(step_code),
            value        = 0,
        )
        self.add_encoded_item(new_step_item)
        self.steps.add(0)
    
    def init_seed_code_tree(self):
        if (len(self.seed_counts) > 0):
            raise Exception("Seed code tree is not empty")
        self.seed_counts.update({ 0: 1 })
        self.update_seed_code_tree()
        seed_code = self.seed_codes[0] #fib_encode_position(position=0, encoder_type='C1') #
        new_seed_item = EncodedItem(
            id           = self.encoded_item_id,
            data_item_id = self.data_item_id,
            is_new       = True,
            type         = ItemType.SEED,
            code         = seed_code.to01(),
            length       = len(seed_code),
            value        = 0,
        )
        self.add_encoded_item(new_seed_item)
        self.seeds.add(0)

    def init_target_value(self, value_length: int):
        item_start                       = self.data_position
        item_end                         = item_start + value_length
        target_data_item                 = self.data[item_start:item_end]
        if (value_length not in list(self.target_values.keys())):
            self.target_values[value_length] = ba2int(target_data_item, signed=False)
            #print(f"+init: {self.target_values[value_length]} ({value_length})")
        if (value_length not in list(self.target_items.keys())):
            self.target_items[value_length] = target_data_item
            #print(f"+init: {self.target_items[value_length]} ({value_length})")
    
    def init_target_values(self):
        if (len(self.active_value_lengths) == 0):
            raise Exception(f"No active value lengths - cannot create targets")
        self.target_values.clear()
        self.target_items.clear()
        # собираем все варианты следующего значения
        for value_length in self.active_value_lengths:
            self.init_target_value(value_length=value_length)
    
    def find_existing_item(self) -> Union[None, DataItem]:
        for target_length, target_value in self.target_values.items():
            if (target_length not in self.values.keys()):
                continue
            if (target_value in self.values[target_length]):
                seed_id       = self.value_seeds[target_length][target_value]
                nounce_id     = self.value_nounces[target_length][target_value]
                seed_length   = len(self.seed_codes[seed_id])
                nounce_length = len(self.nounce_codes[seed_id][nounce_id])
                value_length  = seed_length + nounce_length + self.default_score
                score         = value_length - seed_length - nounce_length
                if (score < self.default_score):
                    raise Exception(f"incorrect score={score}, value_length={value_length}, target_length={target_length}, seed_length={seed_length}, nounce_length={nounce_length}")
                    #continue
                if (target_length != value_length):
                    raise Exception(f"incorrect value_length={value_length}, target_length={target_length}, seed_length={seed_length}, nounce_length={nounce_length}")
                data_item = DataItem(
                    id            = self.data_item_id,
                    seed          = seed_id,
                    nounce        = nounce_id,
                    value         = target_value,
                    seed_length   = seed_length,
                    nounce_length = nounce_length,
                    value_length  = value_length,
                    score         = score,
                    seed_codes    = [
                        self.seed_codes[seed_id].to01()
                    ],
                    nounce_codes  = [
                        self.nounce_codes[seed_id][nounce_id].to01()
                    ],
                )
                return data_item
        return None

    def get_new_nounce_item(self, step: int) -> Union[DataItem, None]:
        if (step == 0):
            return None
        for seed, seed_counts in self.seed_counts.most_common():
            if (seed == 0):
                continue
            if (self.seed_counts[seed] == 0) or (seed not in self.seed_codes.keys()):
                self.update_seed_code_tree()
                self.update_nounce_code_trees(seed=seed)
            #    next_seed_counts = self.seed_counts.copy()
            #    next_seed_counts.update({ seed : 1 })
            #    #next_seed_codes = SortedDict(huffman_code(next_seed_counts, endian='little').items())
            #    #start_nounce    = 0
            #    #seed_length     = len(next_seed_codes[seed])
            #else:
            #    next_seed_codes = SortedDict(huffman_code(next_seed_counts, endian='little').items())
            #next_seed_codes = SortedDict(huffman_code(next_seed_counts, endian='little').items())
            start_nounce    = self.start_nounce[seed]
            seed_length     = len(self.seed_codes[seed])

            nounce = start_nounce + step
            while (True):
                if (nounce in self.nounces[seed]) or (nounce == 0):
                    nounce += 1
                    continue
                break
            next_nounce_counts = self.nounce_counts[seed].copy()
            if (next_nounce_counts[0] == 0):
                next_nounce_counts.update({ 0 : 1 })
            
            next_nounce_counts.update({ nounce : 1 })
            next_nounce_codes = SortedDict(huffman_code(next_nounce_counts, endian='little').items())
            nounce_length     = len(next_nounce_codes[nounce])
            value_length      = seed_length + nounce_length + self.default_score
            
            if (value_length not in list(self.target_values.keys())):
                self.init_target_value(value_length=value_length)
            seed_offset    = 0
            input_nounce   = nounce.to_bytes(length=4, byteorder='little', signed=False)
            #prefix_lengths = sorted(list(self.values.keys()))
            prefix_lengths = SortedSet(self.prev_values.keys())

            while (True):
                input_seed   = seed + seed_offset
                value        = self.get_nounce_value(seed=input_seed, nounce=input_nounce, value_length=value_length)
                prefix_found = False
                for prefix_length in prefix_lengths:
                    if (prefix_length > value_length):
                        break
                    prefix_value = value % (2**prefix_length)
                    #if (prefix_value in self.values[prefix_length]):
                    if (prefix_value in self.prev_values[prefix_length]):
                        prefix_found = True
                        break
                if (prefix_found is False):
                    break
                seed_offset += 1
            
            self.progress.advance(self.scan_task_id, advance=seed_offset)
            if (value == self.target_values[value_length]):
                score = value_length - seed_length - len(next_nounce_codes[0]) - nounce_length
                self.progress.advance(self.scan_task_id, advance=seed_offset)
                return DataItem(
                    id            = self.data_item_id,
                    seed          = seed,
                    nounce        = nounce,
                    value         = value,
                    seed_length   = seed_length,
                    nounce_length = nounce_length,
                    value_length  = value_length,
                    seed_step     = None,
                    nounce_step   = step,
                    seed_codes    = [
                        self.seed_codes[seed].to01(), #next_seed_codes[seed].to01(),
                    ],
                    nounce_codes  = [
                        next_nounce_codes[0].to01(),
                        next_nounce_codes[nounce].to01(),
                    ],
                    score = score,
                )
            else:
                if (value_length not in self.prev_values):
                    self.prev_values[value_length] = set()
                self.prev_values[value_length].add(value)
        return None

    def get_new_seed_item(self, step: int) -> Union[DataItem, None]:
        if (step == 0):
            return None
        seed = self.start_seed + step
        while (True):
            if (seed in self.seeds) or (seed == 0):
                seed += 1
                continue
            break
        next_seed_counts = self.seed_counts.copy()
        next_seed_counts.update({ 0 : 1 })
        next_seed_counts.update({ seed : 1 })
        next_seed_codes = SortedDict(huffman_code(next_seed_counts, endian='little').items())
        seed_length     = len(next_seed_codes[seed])
        
        next_nounce_counts = Counter()
        next_nounce_counts.update({ 0 : 1 })
        next_nounce_counts.update({ 1 : 1 })
        next_nounce_codes = SortedDict(huffman_code(next_nounce_counts, endian='little').items())

        nounce        = 1
        nounce_length = len(next_nounce_codes[nounce])
        value_length  = seed_length + nounce_length + self.default_score
        #target_length = value_length + len(next_nounce_codes[0])
        if (value_length not in list(self.target_values.keys())):
            self.init_target_value(value_length=value_length)
        #if (target_length not in list(self.target_values.keys())):
        #    self.init_target_value(value_length=target_length)
        seed_offset    = 0
        input_nounce   = nounce.to_bytes(length=4, byteorder='little', signed=False)
        #prefix_lengths = sorted(list(self.values.keys()))
        prefix_lengths = SortedSet(self.prev_values.keys())
        
        while (True):
            input_seed   = seed + seed_offset
            value        = self.get_nounce_value(seed=input_seed, nounce=input_nounce, value_length=value_length)
            prefix_found = False
            for prefix_length in prefix_lengths:
                if (prefix_length > value_length):
                    break
                prefix_value = value % (2**prefix_length)
                #if (prefix_value in self.values[prefix_length]):
                if (prefix_value in self.prev_values[prefix_length]):
                    prefix_found = True
                    break
            if (prefix_found is False):
                break
            seed_offset += 1
        
        self.progress.advance(self.scan_task_id, advance=seed_offset)
        if (value == self.target_values[value_length]):
            score = value_length - len(next_seed_codes[0]) - len(next_seed_codes[seed]) #- len(next_nounce_codes[0]) - len(next_nounce_codes[nounce])
            return DataItem(
                id            = self.data_item_id,
                seed          = seed,
                nounce        = nounce,
                value         = value,
                seed_length   = seed_length,
                nounce_length = nounce_length,
                value_length  = value_length,
                seed_step     = step,
                nounce_step   = 1,
                seed_codes    = [
                    next_seed_codes[0].to01(),
                    next_seed_codes[seed].to01(),
                ],
                nounce_codes  = [
                    next_nounce_codes[0].to01(),
                    next_nounce_codes[nounce].to01(),
                ],
                score = score,
            )
        else:
            if (value_length not in self.prev_values):
                self.prev_values[value_length] = set()
            self.prev_values[value_length].add(value)
        return None

    def find_new_item(self) -> DataItem:
        self.prev_values.clear()
        self.prev_values = deepcopy(self.values)
        new_item   = None
        used_steps = self.steps.copy()
        self.progress.update(task_id=self.scan_task_id, completed=0, total=None)

        # 1: searching new seed with nounce=1
        self.progress.update(task_id=self.item_task_id, completed=0, total=len(used_steps), description="(1/3): new seed...")
        for used_step in used_steps:
            self.progress.advance(task_id=self.item_task_id)
            if (used_step == 0):
                continue
            #used_steps.add(used_step)
            new_item = self.get_new_seed_item(step=used_step)
            if (new_item is not None):
                break
            #new_item = self.get_new_nounce_item(step=used_step)
            #if (new_item is not None):
            #    break
        if (new_item is not None):
            return new_item
        
        # 2: searching new nounces in existing seeds
        self.progress.update(task_id=self.item_task_id, completed=0, total=len(used_steps), description="(2/3): new nounce...")
        for used_step in used_steps:
            self.progress.advance(task_id=self.item_task_id)
            new_item = self.get_new_nounce_item(step=used_step)
            if (new_item is not None):
                break
        if (new_item is not None):
            return new_item
        
        # 3: need a new distance step option
        self.progress.remove_task(task_id=self.item_task_id)
        self.item_task_id = self.progress.add_task(completed=0, total=None, description="(3/3): new step...")
        step = 1
        while (True):
            if (step in used_steps):
                used_steps.discard(step)
                step += 1
                continue
            new_item = self.get_new_seed_item(step=step)
            if (new_item is not None):
                break
            #new_item = self.get_new_nounce_item(step=step)
            #if (new_item is not None):
            #    break
            step += 1
            self.progress.advance(task_id=self.item_task_id)
        return new_item

    def get_next_data_item(self) -> DataItem:
        self.init_target_values()
        # сначала проверяем существующий словарь
        existing_item = self.find_existing_item()
        if (existing_item is not None):
            return existing_item
        # пробуем найти нужное значение в хеш-пространстве и дополнить им словарь
        # перебор сидов и позиций осуществляется сдвигом от последнего значения, чтобы максимизировать повторное использование
        # одних и тех же кодов Хаффмана, отвечающих за дистанцию сдвига
        return self.find_new_item()
    
    def has_next_data_item(self) -> bool:
        remaining_bits = self.data_size - self.data_position
        if (remaining_bits > max(self.active_value_lengths)):
            return True
        return False
    
    def encode_raw_number(self, number: int) -> bitarray:
        return encode_varint_number(number=number, prefix_length=VARINT_PREFIX_LENGTH, endian=DEFAULT_ENDIAN)
    
    def encode_data(self):
        while (True):
            next_data_item = self.get_next_data_item()
            data_item      = self.register_data_item(data_item=next_data_item)
            # rebuild tree after each new item
            self.update_value_code_tree()

            if (data_item.seed_step is not None):
                self.step_counts.update({ data_item.seed_step : 1 })
            if (data_item.nounce_step is not None):
                self.step_counts.update({ data_item.nounce_step : 1 })
            if (data_item.value_length not in list(self.values.keys())):
                self.values[data_item.value_length] = SortedSet()
            self.seed_counts.update({ data_item.seed : 1 })
            self.nounce_counts[data_item.seed].update({ data_item.nounce : 1 })
            self.value_counts[data_item.value_length].update({ data_item.value : 1 })
            self.seed_length_counts.update({ data_item.seed_length : 1 })
            self.nounce_length_counts[data_item.seed].update({ data_item.nounce_length : 1 })
            self.value_length_counts[data_item.value_length] = len(self.values[data_item.value_length])

            self.data_position += data_item.value_length
            self.total_score   += data_item.score
            self.item_counts.update({ (data_item.seed, data_item.nounce) : 1 })

            self.progress.advance(self.data_task_id, data_item.value_length)
            self.progress.advance(self.speed_task_id, data_item.value_length)
            self.progress.update(task_id=self.data_task_id, description=f"{sorted(self.item_counts.aggregated_counts().most_common())}")
            
            enc_value = data_item.value_item
            if (self.data_position >= 0):
                sgn = ' '
                if (data_item.score > 0):
                    sgn = '+'
                total_values = len(self.values[data_item.value_length])
                print(f"\nsc=({sgn}{data_item.score:2}), vl={data_item.value_length:2} ({total_values}), steps=({len(self.steps)}), seed={data_item.seed} ({self.seed_counts[data_item.seed]}/{len(self.seeds)}), i=('{enc_value.to01()}': {self.item_counts[(data_item.seed, data_item.nounce)]}), id={data_item.id} ({len(self.item_counts)}), p={self.data_position}, score={self.total_score}")
                print(f"                 {data_item}")
                
            
            if (self.has_next_data_item() is False):
                print(f"position={self.data_position} | {self.data_size}")
                print(f"total_score={self.total_score}")
                print(f"END OF DATA\n")
                break