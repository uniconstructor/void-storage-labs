# https://github.com/Textualize/rich
from rich import print, inspect
# https://rich.readthedocs.io/en/latest/pretty.html
from rich.pretty import pprint
# https://rich.readthedocs.io/en/latest/traceback.html
from rich.traceback import install
install(show_locals=True)
# https://rich.readthedocs.io/en/latest/logging.html
import logging
from rich.logging import RichHandler
logging.basicConfig(
    level="INFO",
    format="%(message)s",
    datefmt="[%X]",
    handlers=[RichHandler(rich_tracebacks=True, locals_max_length=16)]
)
log = logging.getLogger("rich")
# https://github.com/tqdm/tqdm
#import tqdm
# https://github.com/tqdm/tqdm#nested-progress-bars
from tqdm import tqdm as tqdm_notebook
from tqdm.auto import trange
#from tqdm.notebook import tqdm as tqdm_notebook, trange

import shutil
# https://docs.python.org/3/library/itertools.html
# https://more-itertools.readthedocs.io/en/stable/api.html
from itertools import count, accumulate
# https://more-itertools.readthedocs.io/en/stable/api.html#more_itertools.windowed
from more_itertools import adjacent, mark_ends, pairwise, windowed, seekable, peekable, spy, \
    bucket, split_when, split_before, split_at, roundrobin, \
    consecutive_groups, run_length, first_true, islice_extended, first, last, rstrip, tail, \
    unique_everseen, locate, replace, time_limited
# https://bitstring.readthedocs.io/en/latest/index.html
from bitstring import Bits, BitArray, BitStream, ConstBitStream
# https://pythongeeks.org/ordereddict-in-python/
# https://www.geeksforgeeks.org/counters-in-python-set-1/
# https://docs.python.org/3/library/collections.html#counter-objects
from collections import OrderedDict, Counter, defaultdict, namedtuple, deque
# https://github.com/multiformats/unsigned-varint
# https://github.com/fmoo/python-varint
import varint
# https://docs.python.org/3/library/typing.html
from typing import List, Dict, Set, Tuple, Optional, Union
# https://docs.python.org/3.6/library/random.html#module-random
import random
# https://www.geeksforgeeks.org/floor-ceil-function-python/?ref=lbp
import math
import operator
# https://stackoverflow.com/questions/55212014/python-count-copies-in-dictionary
# https://docs.python.org/3/library/copyreg.html#module-copyreg
# https://docs.python.org/3/library/shelve.html#module-shelve
# https://docs.python.org/3/library/pickle.html#persistence-of-external-objects
import copy, pickle, json
# https://github.com/mohanson/leb128
# https://en.wikipedia.org/wiki/LEB128
import io, leb128
import os, sys
if not 'workbookDir' in globals():
    workbookDir = os.getcwd()
os.chdir(workbookDir)
# функции для работы с хеш-пространством
from hash_space_utils import HashItemAddress, HashItemPosition, HashSegmentAddress, PositionSeed, \
    DEFAULT_VALUE_STEP, DEFAULT_POSITION_STEP, HASH_DIGEST_BITS, \
    split_data, count_segment_items, count_split_values, collect_split_positions, \
    get_min_bit_length, get_aligned_bit_length, \
    bytes_at_position, read_hash_item, read_hash_segment, \
    get_item_based_data_length_from_number, \
    find_value_in_segment

# https://www.attrs.org/en/stable/
# https://www.attrs.org/en/latest/api.html#attr.ib
import attr
# from attr import field, fields, define, make_class
# https://realpython.com/python-data-classes/
# https://realpython.com/python-data-classes/#advanced-default-values
from dataclasses import dataclass, make_dataclass

# https://docs.sqlalchemy.org/en/14/core/engines.html#sqlite
from sqlalchemy import create_engine, orm, Table, Column, Index

# https://docs.sqlalchemy.org/en/14/orm/query.html?highlight=exists#sqlalchemy.orm.Query.exists
# https://docs.sqlalchemy.org/en/14/core/sqlelement.html#sqlalchemy.sql.expression.literal
from sqlalchemy import literal

# https://docs.sqlalchemy.org/en/14/dialects/sqlite.html
# https://docs.sqlalchemy.org/en/14/dialects/sqlite.html#sqlite-data-types
from sqlalchemy.dialects.sqlite import BLOB, BOOLEAN, INTEGER, NUMERIC, JSON, SMALLINT, VARCHAR
# https://docs.sqlalchemy.org/en/14/orm/tutorial.html#declare-a-mapping
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm.query import Query
from sqlalchemy.ext.declarative import declarative_base
# https://docs.sqlalchemy.org/en/14/orm/tutorial.html#using-exists
from sqlalchemy.sql import exists, func, asc, desc
from sqlalchemy.orm import registry
# https://docs.sqlalchemy.org/en/14/orm/mapped_sql_expr.html#using-a-hybrid
# https://docs.sqlalchemy.org/en/14/orm/extensions/hybrid.html
from sqlalchemy.ext.hybrid import hybrid_property
# https://docs.sqlalchemy.org/en/14/orm/mapping_columns.html#sqlalchemy.orm.column_property
from sqlalchemy.orm import column_property
# https://docs.sqlalchemy.org/en/14/core/sqlelement.html#sqlalchemy.sql.expression.or_
from sqlalchemy import and_, or_
# https://docs.sqlalchemy.org/en/14/orm/extensions/asyncio.html?highlight=add_all#preventing-implicit-io-when-using-asyncsession
import asyncio
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy.ext.asyncio import create_async_engine

# https://docs.sqlalchemy.org/en/14/orm/declarative_styles.html#example-three-attrs-with-imperative-table
mapper_registry = registry()
#engine          = create_engine('sqlite:////media/frost/storage/values_leb_128_fixed.local.db', echo=False, query_cache_size=1000)
#engine          = create_engine('sqlite:////media/frost/storage/values_leb_128_fixed.1266.local.db', echo=False, query_cache_size=1000)
#engine          = create_engine('sqlite:///data/values_leb_128_fixed.local.db', echo=False, query_cache_size=1000)
engine          = create_engine('sqlite:///data/cache/values_leb_128_fixed.vl_3.local.db', echo=False, query_cache_size=1000)
#engine          = create_engine('sqlite:///data/values_leb_128_fixed.vl_3.local.db', echo=False) #, query_cache_size=500)
#engine          = create_engine('sqlite:///data/cache/values_leb_128_fixed.local.db', echo=False)
#engine          = create_engine('sqlite:///data/cache/values_leb_128_fixed.1266.local.db', echo=False)
# engine = create_engine('sqlite:///:memory:', echo=True)
# https://docs.sqlalchemy.org/en/14/orm/tutorial.html#creating-a-session
Session = sessionmaker(bind=engine)
session = Session()
#Base    = mapper_registry.generate_base()

# TODO: https://docs.sqlalchemy.org/en/14/orm/queryguide.html#yield-per
# TODO: use positive and negative integers to switch between LSB/MSB byte order:
#       allow negative indexes in hash space and create a "mirror space" containing identical
#       bytes, but their order is reserved: 
#       hash_bytes[0:3] = {'0x00', '0x01', 0x02}
#       hash_bytes[-3:0] = {'0x02', '0x01', 0x00}
#       hash_bytes[-1:1] = {'0x00', 0x00}
# TODO: sql bitmask: http://dataeducation.com/bitmask-handling-part-3-logical-operators/
# TODO: NUMBERS TABLE: http://dataeducation.com/you-require-a-numbers-table/
#       https://stackoverflow.com/questions/9035649/bitmask-grouping-using-sqlite
#       http://dataeducation.com/dealing-with-very-large-bitmasks/
#       http://dataeducation.com/correction-on-bitmask-handling/

# minimum item size (in bytes)
MIN_VALUE_LENGTH = 2

@dataclass()
class TierCondition:
    """
    >>> condition = TierCondition(2, [42, 4242, 38])
    >>> condition
    TierCondition(value_length=2, seed=None, value={42, 38, 4242}, position=set())
    >>> condition.asdict()
    {'value_length': 2, 'value': [38, 42, 4242]}
    """
    value_length : int
    seed         : int
    value        : Set[int]
    position     : Set[int]

    def __init__(self, value_length: int, value: Union[List[int], Set[int]], position: Union[List[int], Set[int]]=set(), seed: int=None):
        self.value_length = value_length
        self.seed         = seed
        self.value        = set(value)
        self.position     = set(position)
    
    def asdict(self) -> dict:
        result = {
            'value_length' : self.value_length,
            'value'        : sorted(list(self.value)),
        }
        if len(self.position) > 0:
            result['position'] = self.position
        if self.seed is not None:
            result['seed'] = self.seed
        return result
    
    def exclude_positions(self, excluded_positions: Set[int]):
        for excluded_position in excluded_positions:
            self.position.discard(excluded_position)
    
    def exclude_values(self, excluded_values: Set[int]):
        for excluded_value in excluded_values:
            self.value.discard(excluded_value)

# db table config for mapper
fixed_item_value_table = Table(
    'fixed_item_values',
    mapper_registry.metadata,
    # id записи (сквозная нумерация)
    Column("id", INTEGER, primary_key=True),
    # относительная позиция значения (с учетом длины адреса)
    Column("position", INTEGER, nullable=False, index=False),
    # значение в виде положительных целых чисел
    Column("value", INTEGER, nullable=False, index=False),
    # значение в виде байтов
    # Column("byte_value", IntegerColumn, nullable=False, index=True),
    Column("byte_value", BLOB(length=8), nullable=False, index=False),
    # seed-значение (инициализация хеш-функции) в виде целого числа (0 -> (2**64)-1)
    Column("seed", INTEGER, nullable=False, index=True),
    # длина значения в байтах
    Column("value_length", SMALLINT, nullable=False, index=False),
    # длина адреса в байтах
    # Column("address_length", SMALLINT, nullable=False, index=False),
    # позиция предыдущего значения, более короткого и начинающегося с тех же значений байт
    # Column("parent_position", IntegerColumn, nullable=True, index=True),
    # indexes
    Index('idx_vl_v_p_s', 'value_length', 'value', 'position', 'seed')
)
"""
class FixedItemValue:
    # таблица в базе
    __table__       = fixed_item_value_table
    id              = attr.ib(init=False)
    position        = attr.ib()
    value           = attr.ib()
    byte_value      = attr.ib()
    seed            = attr.ib()
    value_length    = attr.ib()
    #address_length  = attr.ib()
    #parent_position = attr.ib()
    #@hybrid_property
"""

def get_address_length_from_number(number: int) -> int:
    """
    >>> get_address_length_from_number(127)
    1
    >>> get_address_length_from_number(128)
    2
    >>> get_address_length_from_number(0)
    1
    >>> get_address_length_from_number(2**14-1)
    2
    >>> get_address_length_from_number(2**14)
    3
    """
    return len(leb128.u.encode(number))

def get_value_length_from_number(number: int) -> int:
    return get_address_length_from_number(number) + 1

def address_length_to_offset(address_length: int) -> int:
    """
    >>> address_length_to_offset(0)
    0
    >>> address_length_to_offset(1)
    0
    >>> address_length_to_offset(2)
    128
    >>> address_length_to_offset(3) == (2**7 + 2**14)
    True
    """
    if (address_length == 0):
        return 0
    if (address_length == 1):
        return 0
    prev_address_length = address_length - 1
    prev_address_offset = address_length_to_offset(prev_address_length)
    return prev_address_offset + (2 ** (7 * (address_length - 1)))

def address_length_to_capacity(address_length: int) -> int:
    """fixed_item_values
    >>> address_length_to_capacity(0)
    0
    >>> address_length_to_capacity(1)
    128
    >>> address_length_to_capacity(2)
    16256
    >>> address_length_to_capacity(3)
    2080640
    """
    if (address_length == 0):
        return 0
    total_address_capacity = (2 ** (7 * address_length))
    address_offset         = address_length_to_offset(address_length)
    return total_address_capacity - address_offset

def position_to_id(position: int, address_length: int) -> int:
    offset = address_length_to_offset(address_length)
    return (offset + position)

def id_to_position(id: int, address_length: int) -> int:
    offset = address_length_to_offset(address_length)
    return (id - offset)

def id_to_address_length(id: int) -> int:
    return get_address_length_from_number(id)

def address_length_to_value_length(address_length: int) -> int:
    return (address_length + 1)

def value_length_to_address_length(value_length: int) -> int:
    if (value_length == 0):
        return 0
    return (value_length - 1)

def create_values_table(model_class, file_path: str):
    """
    Создать таблицу для хранения значений: вызывается 1 раз в начале работы
    """
    local_engine  = create_engine(f"sqlite:///{file_path}", echo=False)
    #LocalSession = sessionmaker(bind=local_engine)
    #local_session = Session()
    mapper_registry.metadata.create_all(local_engine)
    inspect(model_class.__table__)

def create_values_table_indexes(model_class, file_path: str):
    local_engine  = create_engine(f"sqlite:///{file_path}", echo=False)
    print(f"file: {file_path}")
    print(f"Creating seed index...")
    seed_index = Index('ix_fixed_item_values_seed', fixed_item_value_table.c.seed)
    seed_index.create(local_engine)
    print(f"Done.")
    print(f"Creating composite index...")
    composite_index = Index('idx_vl_v_p_s', 'value_length', 'value', 'position', 'seed')
    composite_index.create(local_engine)
    print(f"Done.")

def create_query_condition(query: Query, conditions: dict, model_class=FixedItemValue) -> Query:
    for field, value in conditions.items():
        if (type(value) is set):
            value = list(value.copy())
        if (type(value) is list):
            field_getter        = operator.attrgetter(field)
            model_field         = field_getter(model_class)
            in_values           = list()
            between_expressions = list()
            # construct IN and BETWEEN condition groups
            for value_item in value:
                if (type(value_item) is range):
                    between_args        = [min(value_item), max(value_item)]
                    # https://docs.sqlalchemy.org/en/14/core/sqlelement.html#sqlalchemy.sql.expression.ColumnOperators.in_
                    # https://docs.python.org/3/library/operator.html#operator.methodcaller
                    method_caller       = operator.methodcaller('between', between_args)
                    between_expression  = method_caller(model_field)
                    between_expressions.append(between_expression)
                else:
                    in_values.append(value_item)
            # create conditions for intervals
            between_stmt = None
            if (len(between_expressions) > 0):
                between_condition = or_(False, *between_expressions)
                method_caller     = operator.methodcaller('between', between_condition)
                between_stmt      = method_caller(model_field)
            in_stmt = None
            if (len(in_values) > 0):
                method_caller    = operator.methodcaller('in_', in_values)
                in_stmt          = method_caller(model_field)
            
            if (between_stmt is not None) and (in_stmt is not None):
                field_condition = or_(
                    between_stmt, 
                    in_stmt
                )
            elif (between_stmt is not None):
                field_condition = between_stmt
            elif (in_stmt is not None):
                field_condition = in_stmt    
            else:
                continue
            query = query.where(field_condition)
        else:
            field_getter = operator.attrgetter(field)
            model_field  = field_getter(model_class)
            query        = query.filter(operator.eq(model_field, value))
    return query

def has_saved_value(conditions: Dict[str, Union[int, bool, str, list, bytes]], model_class=FixedItemValue, local_session=None) -> bool:
    """
    Определить, есть ли уже в базе сохраненное значение
    """
    if (local_session is None):
        local_session = session
    q = create_query_condition(local_session.query(model_class), conditions, model_class)
    result = local_session.query(literal(True)).filter(q.exists()).scalar()
    if result is not True:
        return False
    return True

def save_item_value(item_value: FixedItemValue, commit: bool=True, max_check_position: int=0, max_check_seed: int=0, local_session=None) -> FixedItemValue:
    """
    Сохранить значение в базу
    """
    if (local_session is None):
        local_session = session
    if (item_value.position <= max_check_position) and (item_value.seed <= max_check_seed):
        # проверяем существует ли уже такое значение
        value_exists = has_saved_value({
            'value'          : item_value.value,
            #'address_length' : item_value.address_length,
            'value_length'   : item_value.value_length,
            # одно и то же значение может одновременно существовать в базе в одном или нескольких seed-пространствах 
            # если каждая копия значения занимает позицию которая еще не была занята другими копиями этого же значения
            'position'       : item_value.position,
            # 'seed'           : item_value.seed,
        }, local_session=local_session)
        if (value_exists):
            return True
    # сохраняем только после того как всё проставлено
    result = local_session.add(item_value)
    if (commit):
        local_session.commit()
    return result

def find_all_values(conditions: Dict[str, Union[int, bool]], limit: int=None, model_class=FixedItemValue, file_path: str=None) -> List[FixedItemValue]:
    """
    Найти в базе все сохраненные значения
    """
    if (file_path is None):
        local_session = session
    else:
        local_engine  = create_engine(f"sqlite:///{file_path}", echo=False)
        LocalSession  = sessionmaker(bind=local_engine)
        local_session = LocalSession()
    q = create_query_condition(local_session.query(model_class), conditions, model_class)
    if (limit is None):
        saved_values = q.order_by(model_class.id.asc())
    else:
        saved_values = q.order_by(model_class.id.asc()).limit(limit)
    return saved_values.all()

def find_one_value(conditions: Dict[str, Union[int, bool]], model_class=FixedItemValue, file_path: str=None) -> FixedItemValue:
    """
    Найти в базе одно сохраненное значение
    """
    if (file_path is not None):
        local_engine  = create_engine(f"sqlite:///{file_path}", echo=False)
        LocalSession  = sessionmaker(bind=local_engine)
        local_session = LocalSession()
    else:
        local_session = session
    q = create_query_condition(local_session.query(model_class), conditions, model_class)
    return q.order_by(model_class.id.asc()).first()

def count_saved_values(conditions: Dict[str, Union[int, bool]], model_class=FixedItemValue, file_path: str=None) -> int:
    """
    Подсчитать количество записей
    """
    if (file_path is not None):
        local_engine  = create_engine(f"sqlite:///{file_path}", echo=False)
        LocalSession  = sessionmaker(bind=local_engine)
        local_session = LocalSession()
    else:
        local_session = session
    q = create_query_condition(local_session.query(func.count(model_class.id)), conditions, model_class)
    return q.scalar()

def get_last_seed(conditions: Dict[str, Union[int, bool]]=dict(), model_class=FixedItemValue, file_path: str=None) -> FixedItemValue:
    """
    Получить максимальный созжанный seed полученный при генерации базы значений
    """
    if (file_path is not None):
        local_engine  = create_engine(f"sqlite:///{file_path}", echo=False)
        LocalSession  = sessionmaker(bind=local_engine)
        local_session = LocalSession()
    else:
        local_session = session
    q = create_query_condition(local_session.query(model_class), conditions, model_class)
    value = q.order_by(model_class.seed.desc()).first()
    if (value is None):
        return 0
    return value.seed

def get_parent_value(item: FixedItemValue, min_value_length:int=MIN_VALUE_LENGTH) -> FixedItemValue:
    for value_length in range(min_value_length, item.value_length):
        #address_length = value_length - 1
        byte_value     = item.byte_value[:value_length]
        int_value      = int.from_bytes(byte_value, byteorder='little', signed=False)
        parent_value_conditions = {
            'value_length'   : value_length,
            #'address_length' : address_length,
            'seed'           : item.seed,
            'value'          : int_value,
        }
        parent_value = find_one_value(parent_value_conditions)
        if (parent_value):
            return parent_value
    return None

def get_items_for_byte_value(byte_value: Union[bytes, bytearray]) -> List[FixedItemValue]:
    int_value      = int.from_bytes(byte_value, byteorder='little', signed=False)
    value_length   = len(byte_value)
    #address_length = value_length - 1
    conditions     = {
        'value'          : int_value,
        'value_length'   : value_length,
        #'address_length' : address_length,
    }
    return find_all_values(conditions)

def count_items_for_byte_value(byte_value: Union[bytes, bytearray]) -> int:
    int_value      = int.from_bytes(byte_value, byteorder='little', signed=False)
    value_length   = len(byte_value)
    #address_length = value_length - 1
    conditions     = {
        'value'          : int_value,
        'value_length'   : value_length,
        #'address_length' : address_length,
    }
    return count_saved_values(conditions)

def create_length_limits(min_value_length: int = 2, max_value_length: int = 4) -> dict:
    """
    >>> create_length_limits(2, 3)
    {2: 128, 3: 16256}
    >>> create_length_limits(2, 4)
    {2: 128, 3: 16256, 4: 2080640}
    """
    max_items_by_length = dict()
    for value_length in range(min_value_length, max_value_length + 1):
        address_length        = value_length - 1
        value_length_capacity = address_length_to_capacity(address_length)
        max_items_by_length[value_length] = value_length_capacity
    return max_items_by_length

def create_content_based_split(data: ConstBitStream, min_value_length: int = 2, max_value_length: int = 4) -> dict:
    """
    Разбить данные на элементы разной длины, начиная с указанной минимальной. Если значение ранее было упомянуто
    в данных - то оно используется повторно. Если значение встречается в первый раз - оно добавляется в словарь.
    Количество значений в словаре ограничено таким образом чтобы позиция значения всегда была короче чем само значение

    Эта функция работает только с позициями в формате leb128 (нельзя указать длину в битах, только в байтах)
    """
    capacity_overflow   = False
    byte_counts         = Counter()
    data_values         = set()
    split_items         = list()
    # set up maximum values of each length
    max_items_by_length = create_length_limits(min_value_length, max_value_length)
    length_counts       = Counter()
    # read data using variable length items
    while (True):
        item_value = None
        for value_length in range(min_value_length, max_value_length + 1):
            # try to read and use short values first, use long values only if we dont have short ones
            scan_value = data.peek(f"bits:{value_length * 8}")
            if (scan_value not in data_values):
                # new (unique) value recieved
                if length_counts[value_length] >= max_items_by_length[value_length]:
                    # reached maximum capacity for values with given length
                    if (value_length == max_value_length):
                        # maximum unique values recieved - dictionary overflow
                        capacity_overflow = True
                        break
                    else:
                        # use capacity from next length tier
                        continue
                else:
                    # add new value to dictionary, updating item counter (and decreace tier capacity by 1 value)
                    length_counts.update({value_length : 1})
                    item_value = data.read(f"bits:{value_length * 8}")
                    break
            else:
                # existing (not unique) value recieved - do not modify our dictionary, but save item value to split result
                item_value = data.read(f"bits:{value_length * 8}")
                break
        if capacity_overflow == True:
            # input max_value_length is not enough to create a dictionary
            #raise Exception(f"Length capacity reached: {length_counts} (items_processed={len(split_items)}, max_value_length={max_value_length})")
            break
        # update usage counter of the dictionary value
        byte_counts.update({item_value.hex : 1})
        # append item value to final result
        split_items.append(item_value.hex)
        # add item value to data dictionary
        data_values.add(item_value)
        if ((len(data) - data.bitpos) <= (min_value_length * 8)):
            # all data processed
            # TODO: save and return last item if any
            break
    return {
        'length_counts'       : length_counts,
        'byte_counts'         : byte_counts,
        'split_items'         : tuple(split_items),
        'data_values'         : data_values,
        'encoded_data'        : data[0:data.bitpos],
        'remaining_data'      : data[data.bitpos:len(data)],
        'capacity_overflow'   : capacity_overflow,
        'encoded_items_count' : len(split_items),
        'unique_values_count' : len(data_values),
        'bytepos'             : data.bytepos
    }

def create_tier_conditions(dict_values: Set[ConstBitStream]) -> List[TierCondition]:
    """
    Create list of conditions to search values in db
    """
    conditions      = list()
    int_item_values = defaultdict(set)
    for item_value in dict_values:
        byte_value     = item_value.tobytes()
        value_length   = len(byte_value)
        int_value      = int.from_bytes(byte_value, byteorder='little', signed=False)
        int_item_values[value_length].add(int_value)
    for value_length, int_values in int_item_values.items():
        condition = TierCondition(value_length, int_values)
        conditions.append(condition)
    return conditions

def find_best_seeds_for_tier(file_path: str, condition: TierCondition, skip_positions: Set[int]=set(), skip_values: Set[int]=set()) -> Counter:
    """
    Find seed values containing maximum values from condition, excluding values that already been found (skip values),
    and exclude positions that already beeb used (skip_positions), maintaining "one seed - one position" correspondence
    """
    seed_counts = Counter()
    seed_values = defaultdict(lambda : defaultdict(set))
    if len(skip_positions) > 0:
        condition.exclude_positions(skip_positions)
    if len(skip_values) > 0:
        condition.exclude_values(skip_values)
    db_items = find_all_values(condition.asdict(), file_path=file_path)
    for item in db_items:
        item_value    = item.value
        item_seed     = item.seed
        item_position = item.position
        item_length   = item.value_length
        if item_value in skip_values:
            continue
        if item_position in skip_positions:
            continue
        # do not allow two different positions containing same value
        seed_item_values = seed_values[item_seed][item_length]
        if item_value in seed_item_values:
            continue
        seed_values[item_seed][item_length].add(item_value)
        seed_counts.update({item_seed : 1})
    return seed_counts

def create_number_ranges(numbers: List[int]) -> List[range]:
    """
    >>> create_number_ranges([1, 3, 4, 8, 11])
    [range(1, 2), range(3, 5), range(8, 9), range(11, 12)]
    """
    groups  = []
    numbers = sorted(numbers)
    for group in consecutive_groups(numbers):
        group = list(group)
        groups.append(range(min(group), max(group) + 1))
    return groups

def allocate_tier_values(file_path: str, condition: TierCondition):
    # number of values that needs to be allocated
    target_value_count  = len(condition.value)
    # item positions and item values, grouped by seed
    seed_positions      = defaultdict(set)
    seed_values         = defaultdict(set)
    # seeds, grouped by position and item value
    position_seeds      = dict()
    value_seeds         = dict()
    # values and positions as sets, without grouping
    allocated_positions = set()
    allocated_values    = set()
    # seed values used in final allocation
    allocation_seeds    = set()
    # seed values excluded from search (containing duplicate values)
    excluded_seeds      = set()
    # allocate values: find seed value for every tier value, having address (position number) length shorter than value length
    while (len(allocated_values) < target_value_count):
        # find best seeds for remaining set of values, excluding already used positions
        best_seeds = find_best_seeds_for_tier(file_path, condition, allocated_positions, allocated_values)
        # remove seeds containing incorrect values
        for excluded_seed in list(excluded_seeds):
            if excluded_seed in best_seeds:
                del best_seeds[excluded_seed]
        # check database to ensure that we have enough values and can continue search
        if (len(best_seeds) == 0):
            pprint(condition, max_length=8)
            print(len(allocated_positions), f"{allocated_positions}")
            print(len(allocated_values), f"{allocated_values}")
            raise Exception(f"Value database is too small for this tier")
        # get best available seed
        # we know how many values we can get, so we can use limit to speed up query
        for top_seed, top_seed_limit in best_seeds.most_common():
            if (top_seed not in excluded_seeds):
                break
        # create condition to find all values using same seed
        top_seed_condition      = copy.deepcopy(condition)
        # exclude allocated values and positions from search condition
        if len(allocated_positions) > 0:
            top_seed_condition.exclude_positions(allocated_positions)
        if len(allocated_values) > 0:
            top_seed_condition.exclude_values(allocated_values)
        # search inside top seed only
        top_seed_condition.seed = top_seed
        # find values sharing same seed
        top_seed_items     = find_all_values(top_seed_condition.asdict(), limit=top_seed_limit, file_path=file_path)
        # extract item positions and values
        top_seed_positions = set()
        top_seed_values    = set()
        for top_seed_item in top_seed_items:
            if top_seed_item.position in allocated_positions:
                #excluded_seeds.add(top_seed_item.seed)
                continue
            if top_seed_item.value in allocated_values:
                #excluded_seeds.add(top_seed_item.seed)
                continue
            # update allocated positions and values lists
            top_seed_positions.add(top_seed_item.position)
            top_seed_values.add(top_seed_item.value)
            allocated_positions.add(top_seed_item.position)
            allocated_values.add(top_seed_item.value)
        if len(top_seed_values) == 0:
            excluded_seeds.add(top_seed)
        # save allocated positions and values
        if (top_seed not in excluded_seeds):
            seed_positions[top_seed].update(top_seed_positions)
            seed_values[top_seed].update(top_seed_values)
            allocation_seeds.add(top_seed)
            print(f"top_seed={top_seed} {best_seeds.most_common(5)}")
        else:
            print(f"top_seed={top_seed} (EXCLUDED) {best_seeds.most_common(5)}")
        print(f"(+{len(top_seed_values)}) positions (allocated/total): {len(allocated_values)}/{target_value_count}, seeds (available/used): {len(best_seeds)}/{len(allocation_seeds)}")
        print(f"positions: {top_seed_positions},\nvalues: {top_seed_values},\nexcluded_seeds({len(excluded_seeds)})[0:8]: {list(excluded_seeds)[0:8]}")
    # create inverted indexes
    for seed, positions in seed_positions.items():
        for position in positions:
            position_seeds[position] = seed
    for seed, values in seed_values.items():
        for value in values:
            value_seeds[value] = seed
    return {
        'seed_positions'  : seed_positions,
        'seed_values'     : seed_values,
        'position_seeds'  : OrderedDict(sorted(position_seeds.items())),
        'value_seeds'     : OrderedDict(sorted(value_seeds.items())),
        'seeds'           : allocation_seeds,
    }

async def async_save_item_values(async_engine, item_values):
    async with AsyncSession(async_engine) as async_session:
        print(f"Async saving started...")
        async with async_session.begin():
            async_session.add_all(item_values)
            print('Committing...')
        await async_session.commit()
        print(f"Done.")

async def fill_database(file_path: str, start_seed: int, end_seed: int, block_length: int, min_address_length: int, max_address_length: int, commit_interval: int=100000):
    #local_engine  = create_engine(f"sqlite:///{file_path}", echo=False)
    #LocalSession  = sessionmaker(bind=local_engine)
    #local_session = LocalSession()
    async_engine = create_async_engine(
        f"sqlite+aiosqlite:///{file_path}", echo=False
    )
    start_async_session = sessionmaker(
        async_engine, expire_on_commit=False, class_=AsyncSession
    )

    min_value_length   = address_length_to_value_length(min_address_length)
    max_value_length   = address_length_to_value_length(max_address_length)
    max_check_position = 0
    max_check_seed     = start_seed - 1

    start_byte = address_length_to_offset(min_address_length)
    end_byte   = 2**(7*max_address_length) - 1 #address_length_to_offset(max_address_length + 1) - 1
    print(start_byte, end_byte)

    # starting to discard duplicate values from this byte position:
    min_skip_byte_number = 0
    min_skip_count       = end_byte
    # locate first and last block
    first_block = start_byte // block_length
    last_block  = (end_byte // block_length) + 1 #first_block + 4 #
    # number of records created in database
    created_item_count = 0
    item_values        = list()
    print(f"Start seed: {start_seed} (max_check_seed={max_check_seed}, max_check_position={max_check_position})")

    main_progress = tqdm_notebook(iterable=range(start_seed, end_seed), position=0, desc=f"seed:{start_seed}/{end_seed}", leave=True, smoothing=0)
    for item_seed in main_progress:
    #for item_seed in range(start_seed, end_seed):
        main_progress.set_description(f"seed: {item_seed}/{end_seed}", refresh=True)
        seed_skip_count = 0
        seed_values     = set()
        seed_postfix    = {
            'seed_skips' : seed_skip_count,
            'min_skips'  : min_skip_count,
        }
        #seed_progress = trange(first_block, last_block, position=1, desc=f"seed:{item_seed}/{end_seed}", leave=False)
        #seed_progress.set_postfix(seed_postfix)
        # small hack to stop database filling process gracefully
        if os.path.exists("data/stop.local.env"):
            await async_save_item_values(async_engine, item_values)
            #local_session.commit()
            #seed_progress.close()
            main_progress.close()
            await async_engine.dispose()
            print(f"Interrupted with latest completed seed = {item_seed - 1} (use start_seed={start_seed} to continue)")
            break
        # processing hash blocks
        #for block_number in seed_progress:
        for block_number in range(first_block, last_block):
            block_start = start_byte + (block_number * block_length)
            block_end   = block_start + block_length
            # define address and value length
            start_address_length = id_to_address_length(block_start)
            end_address_length   = id_to_address_length(block_end - 1)
            start_value_length   = address_length_to_value_length(start_address_length)
            end_value_length     = address_length_to_value_length(end_address_length)
            # take first bits from next block (for sliding window)
            window_size       = end_value_length
            suffix_length     = (end_value_length - 1)
            real_block_length = block_length + suffix_length
            # load bytes from virtual hash space
            block_bytes       = bytes_at_position(block_start * 8, real_block_length * 8, seed=item_seed)
            # scan all values in the block
            for byte_number in range(block_start, block_end):
                address_length  = id_to_address_length(byte_number)
                value_length    = address_length_to_value_length(address_length)
                value_start     = byte_number % block_length
                value_end       = value_start + value_length
                item_byte_value = bytes(block_bytes[value_start:value_end])
                item_int_value  = int.from_bytes(item_byte_value, byteorder='little', signed=False)
                # collect shortest values first
                prev_byte_values = set()
                for prev_value_length in range(min_value_length, max_value_length):
                    prev_byte_values.add(item_byte_value[0:prev_value_length])
                # 1 seed - 1 value
                prev_byte_values.add(item_byte_value)
                # do not save duplicated values
                have_duplicates = False
                for prev_byte_value in prev_byte_values:
                    if (prev_byte_value in seed_values):
                        #print(f"[{item_seed}/{block_number}] duplicate: {item_byte_value.hex()} ({len(seed_values)}) prev={prev_byte_value.hex()}")
                        have_duplicates = True
                        break
                # duplicate/substring value found - skip it
                if (have_duplicates == True) and (byte_number >= min_skip_byte_number):
                    seed_skip_count += 1
                    continue
                else:
                    seed_values.add(item_byte_value)
                # create new value
                item_value = FixedItemValue(
                    position=byte_number,
                    value=item_int_value,
                    byte_value=item_byte_value,
                    #address_length=address_length,
                    value_length=value_length,
                    seed=item_seed,
                    #parent_position=None,
                )
                item_values.append(item_value)
                # save item to database
                #save_result = save_item_value(item_value, False, max_check_position=max_check_position, max_check_seed=max_check_seed, local_session=local_session)
                created_item_count += 1
                if (created_item_count % commit_interval) == 0:
                    #local_session.add_all(item_values)
                    #await async_save_item_values(async_engine, item_values)
                    #local_session.commit()
                    #async with AsyncSession(async_engine) as async_session:
                    async with start_async_session() as async_session:
                        main_progress.set_description(f"seed: {item_seed}/{end_seed} (adding...)", refresh=True)
                        async with async_session.begin():
                            async_session.add_all(item_values)
                            main_progress.set_description(f"seed: {item_seed}/{end_seed} (committing...)", refresh=True)
                        await async_session.commit()
                    main_progress.set_description(f"seed: {item_seed}/{end_seed}", refresh=True)
                    item_values.clear()
                # find parent value (if any)
                #parent_value = get_parent_value(item_value)
                #if (parent_value):
                #    #raise Exception(f"({parent_value.seed})[{parent_value.position}]{parent_value.byte_value.hex()}")
                #    print(f"{item_seed}:[{block_number}:{last_block}] parent found: ({item_value.seed})[{item_value.position}]{item_value.byte_value.hex()} -> ({parent_value.seed})[{parent_value.position}]{parent_value.byte_value.hex()}")
                #    item_value.parent_position = parent_value.position
                #    seed_values.add(parent_value.byte_value)
                #    continue
                #else:
                #    save_result = save_item_value(item_value, False, max_check_position=max_check_position, max_check_seed=max_check_seed)
            # update progress info
            seed_postfix['seed_skips'] = seed_skip_count
            seed_postfix['min_skips']  = min_skip_count
            #seed_progress.set_postfix(seed_postfix)
        min_skip_count = min(seed_skip_count, min_skip_count)
        #seed_progress.close()
    await async_engine.dispose()

