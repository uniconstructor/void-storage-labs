# https://stackoverflow.com/questions/38340808/recursive-typing-in-python-3-5
from __future__ import annotations
# https://realpython.com/lru-cache-python/
from functools import lru_cache, cached_property

# https://github.com/Textualize/rich
from rich import print
# https://rich.readthedocs.io/en/latest/pretty.html
from rich.pretty import pprint
#from rich.progress import track, Progress, \
#    SpinnerColumn, BarColumn, TextColumn, TimeElapsedColumn, MofNCompleteColumn, \
#    TaskProgressColumn, TimeRemainingColumn, TransferSpeedColumn

# https://realpython.com/python-data-classes/
# https://realpython.com/python-data-classes/#advanced-default-values
from dataclasses import dataclass, make_dataclass

# https://github.com/Cyan4973/xxHash
# https://github.com/ifduyue/python-xxhash
import xxhash
# https://realpython.com/python-namedtuple/
from collections import defaultdict, namedtuple
from custom_counter import CustomCounter as Counter
# https://realpython.com/linked-lists-python/
from copy import deepcopy, copy
# https://docs.python.org/3/library/typing.html
# https://realpython.com/python-data-classes/#more-flexible-data-classes
from typing import List, Set, Dict, Tuple, Optional, Union
# https://www.geeksforgeeks.org/floor-ceil-function-python/?ref=lbp
import math
from bitarray import bitarray, bits2bytes, frozenbitarray, get_default_endian
from bitarray.util import ba2hex, hex2ba, ba2int, int2ba, ba2base, base2ba, \
    serialize, deserialize, vl_encode, vl_decode, zeros#, pprint as baprint

from delta_of_delta import delta_encode, delta_decode, DeltaEncoding

#from itertools import islice, chain, tee
#from more_itertools import windowed, pairwise, roundrobin, circular_shifts, replace, time_limited

# different hash digest formats
BYTES_HASH_CALLBACK : bytes = xxhash.xxh32_digest
HEX_HASH_CALLBACK   : str   = xxhash.xxh32_hexdigest
INT_HASH_CALLBACK   : int   = xxhash.xxh32_intdigest
# хеш-функция используемая для порождения всего бесконечного хеш-пространства
HASH_CALLBACK       : bytes = BYTES_HASH_CALLBACK
# длина получаемого хеша по умолчанию, в битах (используется для всех расчетов)
HASH_DIGEST_BITS    : int   = 32
# длина получаемого хеша по умолчанию, в байтах
HASH_DIGEST_BYTES   : int   = HASH_DIGEST_BITS // 8
# хеш-пространство по умолчанию
DEFAULT_SEED        : int = 0

# минимальное количество значений сохраняемых в базу одним запросом при заполнении базы значений
DEFAULT_BATCH_SIZE  : int = 4096

DEFAULT_ENDIAN      : str = 'little'

@lru_cache(maxsize=16)
def get_min_bit_length(value: int) -> int:
    """
    >>> get_min_bit_length(0)
    1
    >>> get_min_bit_length(1)
    1
    >>> get_min_bit_length(2)
    2
    >>> get_min_bit_length(3)
    2
    >>> get_min_bit_length(4)
    3
    >>> get_min_bit_length(5)
    3
    >>> get_min_bit_length(7)
    3
    >>> get_min_bit_length(8)
    4
    """
    length = int(value).bit_length()
    if (length == 0):
        length = 1
    return length

def bit_position_to_nounce(bit_position: int) -> int:
    return bit_position // HASH_DIGEST_BITS

def bit_position_to_block_position(bit_position: int) -> int:
    return bit_position % HASH_DIGEST_BITS

def byte_position_to_nounce(byte_position: int) -> int:
    return (byte_position * 8) // HASH_DIGEST_BITS

def byte_position_to_block_position(byte_position: int) -> int:
    return (byte_position * 8) % HASH_DIGEST_BITS

def nounce_to_bit_position(nounce: int) -> int:
    return nounce * HASH_DIGEST_BITS

def start_block_bit_position(bit_position: int) -> int:
    nounce = bit_position_to_nounce(bit_position=bit_position)
    return nounce_to_bit_position(nounce=nounce)

def end_block_bit_position(bit_position: int) -> int:
    next_nounce = bit_position_to_nounce(bit_position=bit_position) + 1
    return nounce_to_bit_position(nounce=next_nounce)

@lru_cache(maxsize=2**24)
def nounce_to_input(nounce: int, byteorder: str='big', signed=False) -> bytes:
    """
    Преобразовать число во входные байты для хеш-функции
    """
    byte_length  = (nounce.bit_length() // 8)
    if (byte_length == 0):
        byte_length = 1
    nounce_bytes = nounce.to_bytes(length=byte_length, byteorder=byteorder, signed=signed)
    return nounce_bytes

#@lru_cache(maxsize=2**16)
def nounce_to_input_bytes(nounce: int, byte_length: int, endian: str=DEFAULT_ENDIAN, signed=False) -> bytes:
    """
    Преобразовать число во входные байты для хеш-функции с фиксированной длиной
    """
    #if (byte_length is None):
    #    byte_length  = (nounce.bit_length() // 8) + 1
    nounce_bytes = nounce.to_bytes(length=byte_length, byteorder=endian, signed=signed)
    return nounce_bytes

#@lru_cache(maxsize=2**8)
def bytes_from_nounce(nounce: int, seed: Optional[int]=0) -> bytes:
    return BYTES_HASH_CALLBACK(input=nounce_to_input(nounce), seed=seed)

def hex_from_nounce(nounce: int, seed: Optional[int]=0) -> str:
    return HEX_HASH_CALLBACK(input=nounce_to_input(nounce), seed=seed)

def int_from_nounce(nounce: int, seed: Optional[int]=0) -> int:
    return INT_HASH_CALLBACK(input=nounce_to_input(nounce), seed=seed)

def int_bytes_from_digest(digest: int, byte_length: int=1, start_byte: int=0) -> int:
    # количество байт после последнего байта значения
    bytes_after   = (HASH_DIGEST_BYTES - start_byte) - byte_length
    # делитель отбрасывающий байты после значения
    right_divider = 256 ** bytes_after
    # делитель отбрасывающий байты до значения
    left_divider  = 256 ** byte_length
    # отбрасываем правые и левые байты, оставляя только значение
    return (digest // right_divider) % left_divider

def bytes_from_digest(digest: int, byte_length: int, start_byte: int=0, byteorder='big', signed: bool=False) -> bytes:
    #int_value = int_bytes_from_digest(digest=digest, byte_length=byte_length, start_byte=start_byte)
    bytes_value = digest.to_bytes(length=byte_length, byteorder=byteorder, signed=signed)
    return copy(bytes_value)

def int_bits_from_digest(digest: int, bit_length: int=8, start_bit: int=0) -> int:
    # количество байт после последнего байта значения
    bits_after    = (HASH_DIGEST_BITS - start_bit) - bit_length
    # делитель отбрасывающий байты после значения
    right_divider = 2 ** bits_after
    # делитель отбрасывающий байты до значения
    left_divider  = 2 ** bit_length
    # отбрасываем правые и левые байты, оставляя только значение
    return (digest // right_divider) % left_divider

def int_bytes_from_nounce(nounce: int, seed: int, byte_length: int=1, start_byte: int=0) -> int:
    """
    Получить один или несколько байт в виде беззнакового целого числа из результата хеш-функции
    Используется для быстрого получения значений, поддерживает только одно направление чтения байт (big endian)
    Нумерация байт начинается с нуля, слева направо
    Не содержит никаких проверок входных параметров или получаемого значения, так что аккуратней с этим дерьмом
    """
    # вычисляем полное значение хеша (32, 64, или 128 бит в виде беззнакового целого)
    digest = int_from_nounce(nounce=nounce, seed=seed)
    # отбрасываем правые и левые байты, оставляя только значение
    return int_bytes_from_digest(digest=digest, start_byte=start_byte, byte_length=byte_length)

def int_bits_from_nounce(nounce: int, seed: int, start_bit: int, bit_length: int) -> int:
    """
    Вариант int_bytes_from_nounce но чтение с точностью до битов
    """
    # вычисляем полное значение хеша (32, 64, или 128 бит в виде беззнакового целого)
    digest = int_from_nounce(nounce=nounce, seed=seed)
    # отбрасываем правые и левые байты, оставляя только значение
    return int_bits_from_digest(digest=digest, start_bit=start_bit, bit_length=bit_length)

def ba_bits_from_nounce(nounce: int, seed: int, start_bit: int, bit_length: int, endian: str='big', signed: bool=False) -> bitarray:
    int_bits = int_bits_from_nounce(nounce=nounce, seed=seed, start_bit=start_bit, bit_length=bit_length)
    return int2ba(int_bits, length=bit_length, endian=endian, signed=signed)

def last_int_bits_from_nounce(nounce: int, seed: int, bit_length: int) -> int:
    start_bit = 32 - bit_length
    return int_bits_from_nounce(nounce=nounce, seed=seed, start_bit=start_bit, bit_length=bit_length)

def last_int_bits_from_digest(digest: int, bit_length: int) -> int:
    start_bit = 32 - bit_length
    return int_bits_from_digest(digest=digest, start_bit=start_bit, bit_length=bit_length)

def last_ba_bits_from_nounce(nounce: int, seed: int, bit_length: int, endian: str='big', signed: bool=False) -> bitarray:
    start_bit = 32 - bit_length
    return ba_bits_from_nounce(nounce=nounce, seed=seed, start_bit=start_bit, bit_length=bit_length, endian=endian, signed=signed)

def last_ba_bits_from_digest(digest: int, bit_length: int, endian='big', signed=False) -> bitarray:
    start_bit = 32 - bit_length
    int_bits  = int_bits_from_digest(digest=digest, start_bit=start_bit, bit_length=bit_length)
    return int2ba(int_bits, length=bit_length, endian=endian, signed=signed)

def last_fba_bits_from_digest(digest: int, bit_length: int, endian='big', signed=False) -> frozenbitarray:
    return frozenbitarray(last_ba_bits_from_digest(digest=digest, bit_length=bit_length, endian=endian, signed=signed).copy(), endian=endian)

def bytes_at_position(byte_position: int, byte_length: int, seed: Optional[int]=0) -> bytes:
    """
    Получить из указанного пространства хешей байты значения заданной длины с указанной позиции
    (низкоуровневая функция, использует bitarray)

    Parameters
    ----------
    bit_position: int
        позиция значения - номер бита от начала (от нулевого бита) хеш-пространства
    bit_length: int
        длина значения - количество считываемых бит начиная с указанной позиции
    seed: int
        номер пространства хешей из которого происходит чтение, задачется параметром seed в модуле xxhash:
        https://github.com/ifduyue/python-xxhash#seed-overflow
    
    >>> seed          = 0
    >>> hash_bytes_0_64 = bytes_at_position(byte_position=0, byte_length=8, seed=seed)
    >>> hash_bytes_0_64.hex() # 'a6cd5e9392000f6a' 
    'c44bdff4074eecdb'
    >>> len(hash_bytes_0_64) == 8
    True
    >>> hash_bytes_64_120 = bytes_at_position(byte_position=1, byte_length=7, seed=seed)
    >>> hash_bytes_64_120.hex() # 'cd5e9392000f6a'
    '4bdff4074eecdb'
    >>> len(hash_bytes_64_120) == 7
    True
    >>> hash_bytes_0 = bytes_at_position(byte_position=0, byte_length=8, seed=seed)
    >>> len(hash_bytes_0.hex()) == (8 * 2)
    True
    >>> len(str().join([hash_bytes_0_64.hex(), hash_bytes_0.hex()])) # 'a6cd5e9392000f6ac44bdff4074eecdb'
    32
    >>> hash_bytes_0.hex()
    'c44bdff4074eecdb'
    >>> hash_bytes_1 = bytes_at_position(byte_position=8, byte_length=8, seed=seed)
    >>> hash_bytes_0.hex() + hash_bytes_1.hex() # '51025a4491835505e12ef9d2eb86ceeb'
    'c44bdff4074eecdbe12ef9d2eb86ceeb'
    >>> hash_bytes_2 = bytes_at_position(byte_position=0, byte_length=8*2, seed=seed)
    >>> hash_bytes_2.hex()
    'c44bdff4074eecdbe12ef9d2eb86ceeb'
    >>> (hash_bytes_0 + hash_bytes_1) == hash_bytes_2
    True
    >>> hash_bytes = bytes_at_position(byte_position=6, byte_length=4, seed=seed)
    >>> hash_bytes.hex() #'ecdb5102'
    'ecdbe12e' 
    >>> hash_bytes == (hash_bytes_0[-2:] + hash_bytes_1[:2])
    True
    >>> hash_bytes_0 = bytes_at_position(byte_position=0, byte_length=128 + 2, seed=seed)
    >>> len(hash_bytes_0)
    130
    >>> hash_bytes_1 = bytes_at_position(byte_position=128, byte_length=128 + 2, seed=seed)
    >>> len(hash_bytes_0)
    130
    >>> hash_bytes_0[-2:] == hash_bytes_1[:2]
    True
    """
    #hash_bits  = bits_at_position(bit_position=byte_position*8, bit_length=byte_length*8, seed=seed)
    #hash_bytes = hash_bits.tobytes()
    position = byte_position * 8
    length   = byte_length * 8
    # определяем порядковый номер хеша в пространстве: он кратен длине выходного значения хеш-функции
    start_nounce = bit_position_to_nounce(position)
    end_nounce   = bit_position_to_nounce(position + length - 1)
    # определяем первый и последний байт для чтения:
    # вычисляем из абсолютной позиции относительную (от начала блока) чтобы понять откуда читать данные
    start_byte = (position % HASH_DIGEST_BITS) // 8
    end_byte   = start_byte + math.ceil(length / 8)
    # определяем сколько хешей необходимо вычислить чтобы получить значение указанной длины
    if (start_nounce == end_nounce):
        # нужен один хеш - зная его порядковый номер вычисляем хеш от этого номера и берем значение целиком
        return bytes_from_nounce(start_nounce, seed=seed)
    else:
        # значение лежит на стыке двух хешей или слишком длинное чтобы поместиться в один хеш:
        # вычисляем несколько последовательно идущих хешей из одного пространства, а затем склеиваем их
        hashes     = [bytes_from_nounce(nounce, seed=seed) for nounce in range(start_nounce, end_nounce + 1)]
        hash_bytes = bytes().join(hashes)
    return hash_bytes[start_byte:end_byte]
    # return hash_bytes

#@lru_cache(maxsize=2**8)
def bits_at_position(bit_position: int, bit_length: int, seed: Optional[int]=0,
        frozen: Optional[bool]=False, endian: str='little') -> Union[bitarray, frozenbitarray]:
    # определяем порядковый номер хеша в пространстве: он кратен длине выходного значения хеш-функции
    start_nounce = bit_position_to_nounce(bit_position=bit_position)
    end_nounce   = bit_position_to_nounce(bit_position=((bit_position + bit_length) - 1))
    # определяем первый и последний байт для чтения:
    # вычисляем положение считываемых данных то есть от начала блока, чтобы понять откуда читать хеш
    start_bit = bit_position % HASH_DIGEST_BITS
    end_bit   = start_bit + bit_length
    # определяем сколько хешей необходимо вычислить чтобы получить значение указанной длины
    #hash_bits = bitarray(endian='little')
    if (start_nounce == end_nounce):
        # нужен один хеш - зная его порядковый номер вычисляем хеш от этого номера и берем значение целиком
        hash_bytes = bytes_from_nounce(nounce=start_nounce, seed=seed)
        # всегда читаем биты и байты из пространства хешей слква направо: 
        # нулевой байт от которого считается расстояние располагается слева, нумерация идет от него
        # используемый порядок чтения ("little endian") позволяет нам всегда находить сначала самые короткие префиксы
        # двоичных последовательностей а после них - все более и более длиные, постоянно ссылаясь назад на ранее
        # обнаруженные значения
        # В результате из полученных значений у нас естественным образом формируется префиксное дерево,
        # значения в котором гарантированно будут всегда самыми первыми вхождениями уникальных значений
        # То есть встречая новое уникальное значение при таком поиске мы можем быть уверены что 
        # эта самая ранняя точка этого значения во всем хеш-пространстве
        # Если сохранять все найденные значения в базе, то это также позволяит каждый раз начинать 
        # поиск все дальше от начала координат - в этом случае поиск каждого нового значения 
        # будет будет начинаться с переиспользования ранее вычисленных значений а не поиска их с нуля
        # и позволит нам аккумулировать все ранее затраченные вычисления
        hash_bits = bitarray(endian=endian)
        hash_bits.frombytes(hash_bytes)
        if frozen:
            hash_bits = frozenbitarray(hash_bits, endian=endian)
        return hash_bits[start_bit:end_bit]
    # значение лежит на стыке между хешами или оно очень длинное: нужно несколько значений
    # вычисляем несколько последовательно идущих хешей из одного пространства, а затем склеиваем их
    hash_bytes = bytes().join([bytes_from_nounce(nounce=nounce, seed=seed) for nounce in range(start_nounce, end_nounce + 1)])
    hash_bits  = bitarray(endian=endian)
    hash_bits.frombytes(hash_bytes)
    if frozen:
        hash_bits = frozenbitarray(hash_bits, endian=endian)
    # оставляем только нужные биты из полученного хеша
    return hash_bits[start_bit:end_bit]

@lru_cache(maxsize=2**7)
def get_position_offset(bit_length: int, min_bit_length: int=1) -> int:
    max_bit_length = (min_bit_length + 2**7)
    offset         = 0
    for position_bit_length in range(min_bit_length, max_bit_length):
        if (bit_length == position_bit_length):
            #print(f"{position_bit_length}: {offset} ({position_bit_length-min_bit_length}) = {(offset)}")
            return offset
        offset = offset + 2**position_bit_length
    return offset

def get_absolute_position(bit_length: int, bit_position: int, min_bit_length: int=5) -> int:
    if (bit_position.bit_length() > bit_length):
        raise Exception(f"Incorrect bit_length: {bit_length} (bit_position: {bit_position} length is {bit_position.bit_length()})")
    offset = get_position_offset(bit_length=bit_length, min_bit_length=min_bit_length)
    #print(f"{bit_length}: ({bit_length-min_bit_length}) {offset} + {bit_position} = {(offset + bit_position)}")
    return (offset + bit_position)

def get_relative_position(bit_length: int, bit_position: int, min_bit_length: int=5) -> int:
    offset = get_position_offset(bit_length=bit_length, min_bit_length=min_bit_length)
    #print(f"{bit_length}: ({bit_length-min_bit_length}) {bit_position} - {offset} = {(bit_position - offset)}")
    position = (bit_position - offset)
    if (position.bit_length() > bit_length):
        raise Exception(f"Incorrect bit_length: {bit_length} (position: {position} length is {position.bit_length()})")
    if (position < 0):
        raise Exception(f"Incorrect bit_length: {bit_length} (bit_position: {bit_position} length is {bit_position.bit_length()})")
    return position

### ITERATOR ###

class HashIterator:
    """
    Виртуальная последовательность для перебора пространства хешей: выполнена как генератор, поэтому создает 
    байты пространства в момент когда к ним обращается, но при этом снаружи всё выглядит так как будто мы работаем
    с обычной последовательностью (перебор через for..in, slice, проверка "a in range", количество элементов и все что положено)
    """
    def __init__(self, item_bit_length: int, start_position: int, stop_position: int, step: int=1, seed: int=0, frozen: bool=False):
        self.frozen          = frozen
        self.seed            = seed
        self.step            = step
        self.item_bit_length = item_bit_length
        # битовая позиция значения
        self.start_position = start_position
        self.stop_position  = stop_position
        self.bit_position   = start_position
        self.bit_length     = self.stop_position - self.start_position
        self.byte_length    = (self.bit_length // 8) + 1 * (self.bit_length % 8)
        self.length         = self.bit_length // self.step
        # next_position нужен только для того чтобы можно было вывести нормально диапазон хешей начиная со стартовой позиции
        self.next_position  = self.bit_position
        # определяем из какого диапазона брать входные данные для хеша
        #self.nounce         = bit_position_to_nounce(bit_position=self.bit_position)
        #self.next_nounce    = bit_position_to_nounce(bit_position=((self.bit_position + self.item_bit_length) - 1))
        # кешируем вызовы хеш-функции при вычислении рядом стоящих значений
        self.update_cache()
    
    def __getitem__(self, bit_position: int):
        return bits_at_position(bit_position=bit_position, bit_length=self.item_bit_length, seed=self.seed, frozen=self.frozen)
    
    def __len__(self):
        return self.length
    
    def __iter__(self):
        return self
    
    def __next__(self) -> bitarray:
        if (self.next_position >= self.stop_position):
            raise StopIteration
        # нужно чтобы после вызова итератора поле self.bit_position хранило позицию от которой вычислен хеш а не следующую
        self.next_position = self.next_position + self.step
        self.bit_position  = self.next_position - self.step
        # проверяем достаточно ли кеша для чтения хеша :)
        if ((self.bit_position + self.item_bit_length) >= self.end_cache_position) or (self.bit_position < self.start_cache_position):
            self.update_cache()
        # по абсолютной позиции значения определяем позиции бит которые надо прочитать из кеша
        start_bit = bit_position_to_block_position(self.bit_position)
        end_bit   = start_bit + self.item_bit_length
        # в итоге при последовательном переборе скользящим окном читаем хеш из памяти вместо того чтобы каждый раз его вычислять
        return self.cache[start_bit:end_bit]
    
    ### CLASS METHODS ###

    def update_cache(self):
        """
        Обновить данные полученные из хеш-функции вместо того чтобы вычислять его заново - нужно для множественных
        подряд идущих запросов к хеш-пространству когда идет поиск со сдвигом на 1 бит
        TODO: попробовать выдать хеши как поток и просматривать его как mmap: 
              https://realpython.com/python-mmap/#reading-a-memory-mapped-file-with-pythons-mmap
        """
        self.start_cache_position = start_block_bit_position(self.bit_position)
        self.end_cache_position   = end_block_bit_position(self.bit_position)

        if (self.item_bit_length <= HASH_DIGEST_BITS):
            self.cache_size = HASH_DIGEST_BITS * 2
        else:
            self.cache_size = ((self.item_bit_length // HASH_DIGEST_BITS) * HASH_DIGEST_BITS) + HASH_DIGEST_BITS
        self.cache = None
        self.cache = bits_at_position(
            bit_position = self.start_cache_position, 
            bit_length   = self.cache_size, 
            seed         = self.seed,
            frozen       = self.frozen,
        )
        #print(f"new_cache: {self.cache} ({self.cache_size}) pos: {self.start_cache_position}-{self.end_cache_position}")

    def get_bits(self, bit_position:int, bit_length: int, seed: int=None, frozen: bool=None):
        if (seed is None):
            seed = self.seed
        if (frozen is None):
            frozen = self.frozen
        return bits_at_position(bit_position=bit_position, bit_length=bit_length, seed=seed, frozen=frozen)


def split_data(data: bitarray, item_length: int) -> List[bitarray]:
    tail_length = len(data) % item_length
    if (tail_length > 0):
        data = data[0:len(data)-tail_length]
        tail = data[len(data)-tail_length:len(data)]
        print(f"removed last {tail_length} bits: tail={tail}")
    start_id = 0
    end_id   = (len(data) // item_length)
    items    = list()
    for item_id in range(start_id, end_id):
        start = item_id * item_length
        end   = start + item_length
        item  = ba2int(data[start:end], signed=False)
        items.append(item)
    return items

def count_data_items(data: bitarray, item_length: int, endian: str=DEFAULT_ENDIAN) -> Counter:
    # init counter with all possible values
    item_counts = Counter()
    for i in range(0, 2**item_length):
        item_counts.update({ i : 0 })
    # define tail length
    tail_length = len(data) % item_length
    if (tail_length > 0):
        data = data[0:len(data)-tail_length]
        tail = data[len(data)-tail_length:len(data)]
        print(f"removed last {tail_length} bits: tail={tail}")
    start_id = 0
    end_id   = (len(data) // item_length)
    for item_id in range(start_id, end_id):
        start = item_id * item_length
        end   = start + item_length
        item  = ba2int(data[start:end], signed=False)
        item_counts.update({ item: 1 })
    return item_counts

def create_value_bitmap(item_counts: Counter, item_length: int) -> bitarray:
    bitmap = zeros(2**item_length)
    for i in range(0, 2**item_length):
        #value_id = frozenbitarray(int2ba(i, length=item_length, endian=DEFAULT_ENDIAN, signed=False))
        value_id = i
        if (item_counts[value_id] == 0):
            bitmap[i] = 1
    return bitmap

def collect_missing_positions(position_bitmap: bitarray, item_length: int) -> List[int]:
    missing_positions = []
    for i in range(0, 2**item_length):
        if (position_bitmap[i] == 1):
            missing_positions.append(i)
    return missing_positions

def delta_to_list(encoded_items: DeltaEncoding, no_zero_delta: bool=False) -> List[int]:
    first_value = encoded_items.initial_timestamp
    if (no_zero_delta is True):
        first_value += 1
        if (0 in encoded_items.timestamps):
            raise Exception(f"Cannot apply 'no_zero_delta' mode: more than one zero delta in values: {encoded_items.initial_timestamp}, {encoded_items.timestamps} ({len(encoded_items.timestamps)})")
    return [first_value] + encoded_items.timestamps

# 2**7=128 позиций максимум
BITMAP_TOTAL_POSITION_BITS = 7
# максимальное расстояние между элементами: 2**(2**3)=256 позиций
BITMAP_ITEM_LENGTH_BITS    = 4
BITMAP_MIN_DISTANCE_BITS   = 3

@dataclass(init=True)
class HashPositionBitmap:
    # полная длина в битах
    bit_length           : int
    byte_length          : int
    # список переданных позиций
    positions            : Set[int]
    # список расстояний между позициями
    distances            : List[int]
    adjusted_distances   : List[int]
    item_bit_length      : int
    total_items          : int
    max_items            : int
    # минимальное расстояние между соседними позициями
    min_distance         : int
    max_distance         : int
    distance_diff        : int
    total_positions_bits : int = BITMAP_TOTAL_POSITION_BITS
    item_length_bits     : int = BITMAP_ITEM_LENGTH_BITS

def get_target_position_bitmap(positions: Set[int], length_offset: int=None, sort_positions: bool=True) -> HashPositionBitmap:
    #total_positions_bits = BITMAP_TOTAL_POSITION_BITS
    #item_length_bits     = BITMAP_ITEM_LENGTH_BITS
    if (length_offset is None):
        length_offset = 0 #len(positions)
    if (len(positions) == 0):
        raise Exception(f"No positions given: positions={positions} ({len(positions)})")
    if (len(positions) < length_offset):
        raise Exception(f"Number of positions is too low: {positions} ({len(positions)}), min={length_offset}")
    
    if (sort_positions):
      bitmap_positions = positions.copy()
      bitmap_positions = sorted(bitmap_positions)
    else:
      bitmap_positions = positions.copy()
    
    distances          = list()
    adjusted_distances = list()
    bit_lengths        = set()
    bit_length_counts  = Counter()
    item_bit_length    = 0
    total_items        = 0
    bitmap_bit_length  = 0
    # считаем позиции от -1 чтобы минимальное расстояние между ними всегда равнялось 1 даже если задействована нулевая позиция
    prev_position      = -1
    
    # вычисляем расстояние между каждой парой позиций
    for position_id in range(0, len(bitmap_positions)):
        position      = bitmap_positions[position_id]
        distance      = position - prev_position
        distances.append(distance)
        prev_position = position
    
    # определяем минимально необходимое количество бит для сохранения количества элементов
    #total_positions_bits = len(distances).bit_length()
    # вычисляем минимальное расстояние между позициями и вычитаем его из всех значений чтобы уменьшить длину каждого значения
    min_distance  = min(distances)
    max_distance  = max(distances)
    distance_diff = max_distance - min_distance
    for full_distance in distances:
        adjusted_distance = full_distance - min_distance
        bit_length        = get_min_bit_length(adjusted_distance)
        adjusted_distances.append(adjusted_distance)
        bit_lengths.add(bit_length)
        bit_length_counts.update({ bit_length: 1 })
    
    item_bit_length    = max(bit_lengths)
    bitmap_bit_length  = (item_bit_length * len(distances)) + BITMAP_MIN_DISTANCE_BITS # total_positions_bits + item_length_bits + 
    bitmap_byte_length = math.ceil(bitmap_bit_length / 8)
    total_items        = len(distances) - length_offset
    
    return HashPositionBitmap(
        bit_length           = bitmap_bit_length,
        byte_length          = bitmap_byte_length,
        positions            = positions,
        distances            = distances,
        adjusted_distances   = adjusted_distances,
        item_bit_length      = item_bit_length,
        total_items          = total_items,
        max_items            = len(distances),
        min_distance         = min_distance,
        max_distance         = max_distance,
        distance_diff        = distance_diff,
        #total_positions_bits = total_positions_bits,
        #item_length_bits     = item_bit_length,
    )

def encode_position_bitmap(bitmap: HashPositionBitmap, endian: str='big') -> bitarray:
    encoded_bitmap  = bitarray(endian=endian)
    encoded_bitmap += int2ba(bitmap.total_items, length=bitmap.total_positions_bits, endian=endian, signed=False)
    encoded_bitmap += int2ba(bitmap.item_bit_length, length=bitmap.item_length_bits, endian=endian, signed=False)
    #encoded_bitmap += int2ba(bitmap.total_items, length=BITMAP_TOTAL_POSITION_BITS, endian=endian, signed=False)
    #encoded_bitmap += int2ba(bitmap.item_bit_length, length=BITMAP_ITEM_LENGTH_BITS, endian=endian, signed=False)
    for distance in bitmap.distances:
        encoded_bitmap += int2ba(distance, length=bitmap.item_bit_length, endian=endian, signed=False)
    return encoded_bitmap

def decode_position_bitmap(data: bitarray, start_byte_id: int, seed_length: int) -> HashPositionBitmap:
    segment_bits     = data[start_byte_id*8:start_byte_id*8 + 8*64]
    total_items      = ba2int(segment_bits[0:BITMAP_TOTAL_POSITION_BITS], signed=False)
    item_bit_length  = ba2int(segment_bits[BITMAP_TOTAL_POSITION_BITS:BITMAP_TOTAL_POSITION_BITS + BITMAP_ITEM_LENGTH_BITS], signed=False)
    distances        = list()
    start_bit        = BITMAP_TOTAL_POSITION_BITS + BITMAP_ITEM_LENGTH_BITS
    positions        = set()
    position         = 0
    bit_length       = start_bit
    item_list_length = total_items + seed_length + 2
  
    for i in range(0, item_list_length):
        item_start  = start_bit  + i * item_bit_length
        item_end    = item_start + item_bit_length
        distance    = ba2int(segment_bits[item_start:item_end], signed=False)
        position   += distance
        bit_length += item_bit_length
        distances.append(distance)
        positions.add(position)

    return HashPositionBitmap(
        bit_length      = bit_length,
        byte_length     = math.ceil(bit_length / 8),
        positions       = positions,
        distances       = distances,
        item_bit_length = item_bit_length,
        total_items     = total_items,
        max_items       = len(distances),
    )