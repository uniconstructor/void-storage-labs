```js

db.hash_value_seed.aggregate(
  [
    {
      $project:
        { 
          oid_str: 
          {
            $convert: { 
              input: "$_id", 
              to: "string",
              onError: "An error occurred",
              onNull: "Input was null or empty" 
            }
          }
        }
    }
  ]
).pretty()

{
  oid_str: {
    $convert: {
      input: "$_id",
      to: "string",
      onError: "An error occurred",
      onNull: "Input was null or empty"
    }
  }
}

{
  pos_str: { $substrBytes: [ "$oid_str", 0, 10 ] },
  value_str: { $substrBytes: [ "$oid_str", 10, 24 ] },
}

db.system.js.insertOne(
  {
    _id: "decimalToBase",
    value: function (input, base) {
       // works up to 72057594037927928 / FFFFFFFFFFFFF8
       var field = "$" + input;
       return {
          $let: {
             vars: {
                bits: {
                   $reduce: {
                      input: { $range: [0, 56] },
                      initialValue: [{ dec: field }],
                      in: {
                         $cond: {
                            if: { $gt: [{ $last: "$$value.dec" }, 0] },
                            then: {
                               $concatArrays: ["$$value",
                                  [{
                                     b: { $substrBytes: ["0123456789ABCDEF", { $mod: [{ $last: "$$value.dec" }, base] }, 1] },
                                     dec: { $trunc: { $divide: [{ $last: "$$value.dec" }, base] } }
                                  }]
                               ]
                            },
                            else: "$$value"
                         }
                      }
                   }
                }
             },
             in: {
                $reduce: {
                   input: { $reverseArray: "$$bits.b" },
                   initialValue: "",
                   in: { $concat: ["$$value", "$$this"] }
                }
             }
          }
       }
    }
  }
);


db.system.js.insertOne(
  {
    _id: "baseToDecimal",
    value: function (input, base) {
       // works up to 72057594037927928 / FFFFFFFFFFFFF8
       var field = "$" + input;
       return {
          $sum: {
             $map: {
                input: { $range: [0, { $strLenBytes: field }] },
                in: {
                   $multiply: [
                      { $pow: [base, { $subtract: [{ $strLenBytes: field }, { $add: ["$$this", 1] }] }] },
                      { $indexOfBytes: ["0123456789ABCDEF", { $toUpper: { $substrBytes: [field, "$$this", 1] } }] }
                   ]
                }
             }
          }
       }
    }
  }
);

{
pos_int: 
  { 
    $function:
      {
        body: function (input, base) {
          // works up to 72057594037927928 / FFFFFFFFFFFFF8
          var field = "$" + input;
          return {
            $sum: {
               $map: {
                  input: { $range: [0, { $strLenBytes: field }] },
                  in: {
                     $multiply: [
                        { $pow: [base, { $subtract: [{ $strLenBytes: field }, { $add: ["$$this", 1] }] }] },
                        { $indexOfBytes: ["0123456789ABCDEF", { $toUpper: { $substrBytes: [field, "$$this", 1] } }] }
                     ]
                  }
               }
            }
          }
          },
        args: [ "pos_str", 16 ],
        lang: "js"
     }
  }
}

{
  $addFields: {
    pos_int: { 
        $function: {
            body: function (input) {
              //var hex_input = `0x${input}`;
              //return parseInt(Number(hex_input), 16),
              return input
            },
            args: [ "$pos_str" ],
            lang: "js"
         }
      }
    }
  }
}


db.createView('hash_value_seed_view', 'hash_value_seed', [{$project: {
 seed: '$seed',
 oid_str: {
  $convert: {
   input: '$_id',
   to: 'string',
   onError: 'An error occurred',
   onNull: 'Input was null or empty'
  }
 }
}}, {$project: {
 seed: '$seed',
 pos_str: {
  $substrBytes: [
   '$oid_str',
   0,
   12
  ]
 },
 value_str: {
  $substrBytes: [
   '$oid_str',
   12,
   24
  ]
 }
}}, {$project: {
 seed: '$seed',
 position: {
  $function: {
   body: 'function (input) {\n          var hex_input = `0x${input}`;\n          return Number(hex_input);\n        }',
   args: [
    '$pos_str'
   ],
   lang: 'js'
  }
 },
 value: {
  $function: {
   body: 'function (input) {\n          var hex_input = `0x${input}`;\n          return Number(hex_input);\n        }',
   args: [
    '$value_str'
   ],
   lang: 'js'
  }
 }
}}, {$addFields: {
 position: '$position',
 value: '$value',
 seed: '$seed',
 address_length: {
  $function: {
   body: 'function (position_int) {\n          var max_value = 0\n          for (var address_length = 1; address_length <=6; address_length++) {\n            max_value = Math.pow(2, (7 * address_length));\n            if (position_int < max_value) {\n              return address_length;\n            }\n          }\n        }',
   args: [
    '$position'
   ],
   lang: 'js'
  }
 }
}}, {$addFields: {
 value_length: {
  $function: {
   body: 'function (address_length) {\n          return address_length + 1\n        }',
   args: [
    '$address_length'
   ],
   lang: 'js'
  }
 }
}}]
);

{
  position: "$position",
  value: "$value",
  position_length: { 
    $function: {
        body: function (position_int) {
          if (posit)
        },
        args: [ "$position" ],
        lang: "js"
     }
  },
}

{
  position: "$position",
  value: "$value",
  address_length: { 
    $function: {
        body: function (position_int) {
          var max_value = 0
          for (var address_length = 1; address_length <=6; address_length++) {
            max_value = Math.pow(2, (7 * address_length));
            if (position_int < max_value) {
              return address_length;
            }
          }
        },
        args: [ "$position" ],
        lang: "js"
     }
  },
  value_length: { 
    $function: {
        body: function (value_int) {
          var max_value = 0
          for (var value_length = 1; value_length <=6; value_length++) {
            max_value = Math.pow(2, (8 * value_length));
            if (value_int < max_value) {
              return address_length;
            }
          }
        },
        args: [ "$value" ],
        lang: "js"
     }
  }
}


max_value = 0
for (let value_length = 1; value_length <=6; value_length++) {
  max_value = Math.pow(2 ** (8 * value_length));
  console.log(value_length);
  console.log(max_value);
}

```